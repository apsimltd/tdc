<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php
if (get('file') && get('file') != "") {
    
    $file = PDF_URL . '/' . get('file');
    
    // make the file downloadable    
	header("Pragma: public"); // required
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false); // required for certain browsers 
	header("Content-Type: application/pdf");
	// change, added quotes to allow spaces in filenames
	header("Content-Disposition: attachment; filename=\"".basename($file)."\";" );
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: ".filesize($file));
	readfile($file);
	exit();
    
} else {
    header('Location:' . BASE_URL);
}
?>

