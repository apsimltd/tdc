<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::KPI Clients" ?>
<?php $page = "kpi_clients"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can('read_client_kpi')) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>

	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base">

        <div id="contentWrap">
        
			<section class="title block">

                <h1 class="pull-left">KPI Clients <span style="color:#ff9800;">[A noter que le SI mets à jours les donnéés Temps de réactivité du client chaque heure.]</span></h1>
								
			</section>
			
            <?php require_once './views/kpi-clients.php'; ?>
                        
            <div class="row kpi_clients_results">
				
                <div class="col col-6">
                    <div class="block nopadding">

                        <div class="block-head with-border">
                            <header><i class="os-icon os-icon-bar-chart-stats-up"></i><span>Synthèse Globale</span></header>
                        </div>

                        <div class="block-body">

                            <table class="table-list compact">
                                <tbody>
                                    <tr>
                                        <td>Postes Confiés</td>
                                        <td>
                                            <?php $rows = KPI::getNumPosteConfies(); ?>
                                            <?php $statusMission = Control::getControlListByType(9); ?>
                                            <table class="table-list compact">
                                                <thead>
                                                    <tr>
                                                        <th>Statut Mission</th>
                                                        <th>Nombre Postes</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php $sum = 0; ?>
                                                <?php foreach($rows as $row): ?>
                                                    <tr>
                                                        <td><?php echo ucfirst($statusMission[$row['status']]) ?></td>
                                                        <td><?php echo $row['num_postes'] ?></td>
                                                    </tr>
                                                    <?php $sum += $row['num_postes'] ?>
                                                <?php endforeach; ?>
                                                    <tr class="total">
                                                        <td><strong>Total</strong></td>
                                                        <td><strong><?php echo $sum ?></strong></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Candidats Présentés</td>
                                        <td>
                                            <?php $presents = KPI::numCandidatsPresenter(); // debug($presents); ?>
                                            <?php $status_tdc = Control::getControlListByType(8); ?>
                                        
                                            <table class="table-list compact">
                                                <thead>
                                                    <tr>
                                                        <th>Statut TDC</th>
                                                        <th>Nombre Candidats</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php $sum = 0; ?>
                                                <?php foreach($presents as $present): ?>
                                                    <tr>
                                                        <td><?php echo ucfirst($status_tdc[$present['control_statut_tdc']]) ?></td>
                                                        <td><?php echo $present['num_candidats_presenter'] ?></td>
                                                    </tr>
                                                    <?php $sum += $present['num_candidats_presenter'] ?>
                                                <?php endforeach; ?>
                                                    <tr class="total">
                                                        <td><strong>Total</strong></td>
                                                        <td><strong><?php echo $sum ?></strong></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Candidats Rencontrés (Short-listé)</td>
                                        <td><?php echo KPI::getNumCandidatsRencontrer() ?></td>
                                    </tr>
                                    <tr>
                                        <td>Candidats Placés</td>
                                        <td><?php echo KPI::getNumCandidatPlace() ?></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div><!-- / block-body -->

                    </div>
                </div><!-- /col -->
                
                <div class="col col-6">
                    <div class="block nopadding">
                        
                        <div class="block-head with-border with-top-border">
                            <header><i class="os-icon os-icon-bar-chart-stats-up"></i><span>Temps de réactivité du client</span></header>
                        </div>
                        
                        <div class="block-body">

                            <?php $time_ins_op = KPI::tempsReactiviteVsCandidatsRencontreOP(); //debug($time_ins_op); ?>
                            <?php $time_ins_hf = KPI::tempsReactiviteVsCandidatsRencontreHF(); //debug($time_ins_hf); ?>
                            
                            <table class="table-list compact">
                                <thead>
                                    <tr>
                                        <th>Prestataire</th>
                                        <th>Total Candidats</th>
                                        <th>Présentation >> Rencontre</th>
                                        <th>Rencontre Consultant >> Rencontre Client</th>
                                        <th>Présentation >> Retour Après Rencontre</th>
                                    </tr>
                                </thead>
                                <tbody>
                                                                       
                                    <tr>
                                        <td>OPSEARCH</td>
                                        <td><?php echo $time_ins_op['num_candidats'] ?></td>
                                        <td>
                                            <?php echo seconds_to_hours_minutes_noS($time_ins_op['t_presentation_rencontre']) ?> / <?php echo $time_ins_op['num_candidats'] ?> = <strong>
                                                <?php if ($time_ins_op['t_presentation_rencontre'] > 0 && $time_ins_op['num_candidats']): ?>
                                                    <?php $val = round($time_ins_op['t_presentation_rencontre']/$time_ins_op['num_candidats'], 0) ?>
                                                    <?php echo seconds_to_hours_minutes_noS($val) ?>
                                                <?php else: ?>
                                                    0
                                                <?php endif; ?>
                                            </strong>
                                        </td>
                                        <td>                                            
                                            <?php echo seconds_to_hours_minutes_noS($time_ins_op['t_rencontre_consultant_rencontre_client']) ?> / <?php echo $time_ins_op['num_candidats'] ?> = <strong>
                                                <?php if ($time_ins_op['t_rencontre_consultant_rencontre_client'] > 0 && $time_ins_op['num_candidats'] > 0): ?>
                                                    <?php $val = round($time_ins_op['t_rencontre_consultant_rencontre_client']/$time_ins_op['num_candidats'], 0) ?>
                                                    <?php echo seconds_to_hours_minutes_noS($val) ?>
                                                <?php else: ?>
                                                    0
                                                <?php endif; ?>
                                            </strong>
                                        </td>
                                        <td>                                            
                                            <?php echo seconds_to_hours_minutes_noS($time_ins_op['t_reactivite_client']) ?> / <?php echo $time_ins_op['num_candidats'] ?> = <strong>
                                                <?php if ($time_ins_op['t_reactivite_client'] > 0 && $time_ins_op['num_candidats'] > 0): ?>
                                                    <?php $val = round($time_ins_op['t_reactivite_client']/$time_ins_op['num_candidats'], 0) ?>
                                                    <?php echo seconds_to_hours_minutes_noS($val) ?>
                                                <?php else: ?>
                                                    0
                                                <?php endif; ?>
                                            </strong>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>HF</td>
                                        <td><?php echo $time_ins_hf['num_candidats'] ?></td>
                                        <td>
                                            <?php echo seconds_to_hours_minutes_noS($time_ins_hf['t_presentation_rencontre']) ?> / <?php echo $time_ins_hf['num_candidats'] ?> = <strong>
                                                <?php if ($time_ins_hf['t_presentation_rencontre'] > 0 && $time_ins_hf['num_candidats'] > 0): ?>
                                                    <?php $val = round($time_ins_hf['t_presentation_rencontre']/$time_ins_hf['num_candidats'], 0) ?>
                                                    <?php echo seconds_to_hours_minutes_noS($val) ?>
                                                <?php else: ?>
                                                    0
                                                <?php endif; ?>
                                            </strong>
                                        </td>
                                        <td>N/A</td>
                                        <td>
                                            <?php echo seconds_to_hours_minutes_noS($time_ins_hf['t_reactivite_client']) ?> / <?php echo $time_ins_hf['num_candidats'] ?> = <strong>
                                                <?php if ($time_ins_hf['t_reactivite_client'] > 0 && $time_ins_hf['num_candidats'] > 0): ?>
                                                    <?php $val = round($time_ins_hf['t_reactivite_client']/$time_ins_hf['num_candidats'], 0) ?>
                                                    <?php echo seconds_to_hours_minutes_noS($val) ?>
                                                <?php else: ?>
                                                    0
                                                <?php endif; ?>
                                            </strong>
                                        </td>
                                    </tr>
                                    
                                    
                                </tbody>
                            </table>
                            
                            <p style="color: #090;">Voir les détails dans les tableaux ci-dessous</p>
                            
                        </div><!-- / block-body -->

                    </div>
                </div><!-- /col -->
                
                <div class="col col-12">
                    <div class="block nopadding">
                        
                        <div class="block-head with-border with-top-border">
                            <header><i class="os-icon os-icon-bar-chart-stats-up"></i><span>Détail des données prises en comptes</span></header>
                        </div>
                        
                        <div class="block-body">
                            
                            <?php $statusTDC = Control::getControlListByType(8); ?>
                            
                            <h3 class="kpi_h3">OPSEARCH</h3>
                            
                            <div class="nano" style="height:500px;">
                                <div class="nano-content">
                                    
                                    <?php $time_ops = KPI::tempsReactiviteVsCandidatsRencontre_OP(); // debug($time_ops); ?>
                                    
                                    <table class="table-list compact sticky-table-header">
                                        <thead>
                                            <tr>
                                                <th>Mission</th>
                                                <th>Poste</th>
                                                <th>Client</th>
                                                <th>Candidat</th>
                                                <th>Statut TDC</th>
                                                <th>Date Prise De Contact</th>
                                                <th>Date Présentation</th>
                                                <th>Date Rencontre Client</th>
                                                <th>Date Rencontre Consultant</th>
                                                <th>Date Retour Après Rencontre</th>
                                                <th>Présentation >> Rencontre <br>[Date Rencontre Client - Date Présentation]</th>
                                                <th>Rencontre Consultant >> Rencontre Client <br>[Date Rencontre Client - Date Rencontre Consultant]</th>
                                                <th>Présentation >> Retour Après Rencontre <br>[Date Retour Après Rencontre - Date Présentation]</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $sum_t_presentation_rencontre = 0;  
                                            $sum_t_rencontre_consultant_rencontre_client = 0;
                                            $sum_t_reactivite_client = 0;
                                            ?>
                                            <?php foreach($time_ops as $time_op): ?>
                                            <tr>
                                                <td align="center">
                                                    <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $time_op['mission_id'] ?>&tdc_id=<?php echo $time_op['tdc_suivi_id'] ?>" target="_blank">
                                                        <?php echo $time_op['mission_id'] ?>
                                                    </a>
                                                </td>
                                                <td><?php echo $time_op['poste'] ?></td>
                                                <td><?php echo $time_op['client'] ?></td>
                                                <td><?php echo mb_strtoupper($time_op['cand_nom']) . ' ' . mb_ucfirst($time_op['cand_prenom']) ?></td>
                                                <td>
                                                    <?php if ($time_op['control_statut_tdc'] > 0): ?>
                                                    
                                                        <span class="status <?php echo getClassStatusTDC($time_op['control_statut_tdc']) ?>">
                                                            <?php echo $statusTDC[$time_op['control_statut_tdc']] ?>
                                                        </span>
                                                    
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($time_op['date_prise_de_contact'] <> ""): ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_prise_de_contact'])) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($time_op['date_presentation'] <> ""): ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_presentation'])) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($time_op['date_rencontre_client'] <> ""): ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_rencontre_client'])) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($time_op['date_rencontre_consultant'] <> ""): ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_rencontre_consultant'])) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($time_op['date_retour_apres_rencontre'] <> ""): ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_retour_apres_rencontre'])) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($time_op['t_presentation_rencontre'] > 0): ?>
                                                        <?php $sum_t_presentation_rencontre += $time_op['t_presentation_rencontre']; ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_rencontre_client'])) ?> - <?php echo date('Y-m-d', strtotime($time_op['date_presentation'])) ?>
                                                        = <?php echo seconds_to_hours_minutes_noS($time_op['t_presentation_rencontre']) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                    
                                                </td>
                                                <td>
                                                    <?php if ($time_op['t_rencontre_consultant_rencontre_client'] > 0): ?>
                                                        <?php $sum_t_rencontre_consultant_rencontre_client += $time_op['t_rencontre_consultant_rencontre_client']; ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_rencontre_client'])) ?> - <?php echo date('Y-m-d', strtotime($time_op['date_rencontre_consultant'])) ?>
                                                        = <?php echo seconds_to_hours_minutes_noS($time_op['t_rencontre_consultant_rencontre_client']) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($time_op['t_reactivite_client'] > 0): ?>
                                                        <?php $sum_t_reactivite_client += $time_op['t_reactivite_client']; ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_retour_apres_rencontre'])) ?> - <?php echo date('Y-m-d', strtotime($time_op['date_presentation'])) ?>
                                                        = <?php echo seconds_to_hours_minutes_noS($time_op['t_reactivite_client']) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                    
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                            <tr class="total" style="background-color: #92D050;color: #fff;">
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td align="center"><strong>Total Candidats = <?php echo count($time_ops); ?></strong></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td align="center"><strong>Total = <?php echo seconds_to_hours_minutes_noS($sum_t_presentation_rencontre) ?></strong></td>
                                                <td align="center"><strong>Total = <?php echo seconds_to_hours_minutes_noS($sum_t_rencontre_consultant_rencontre_client) ?></strong></td>
                                                <td align="center"><strong>Total = <?php echo seconds_to_hours_minutes_noS($sum_t_reactivite_client) ?></strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                </div><!-- / nano-content -->
                            </div><!-- / nano -->
                            
                            <h3 class="kpi_h3">HF</h3>
                            
                            <div class="nano" style="height:500px;">
                                <div class="nano-content">
                                    
                                    <?php $time_ops = KPI::tempsReactiviteVsCandidatsRencontre_HF(); // debug($time_ops); ?>
                                    
                                    <table class="table-list compact sticky-table-header">
                                        <thead>
                                            <tr>
                                                <th>Mission</th>
                                                <th>Poste</th>
                                                <th>Client</th>
                                                <th>Candidat</th>
                                                <th>Statut TDC</th>
                                                <th>Date Prise De Contact</th>
                                                <th>Date Présentation</th>
                                                <th>Date Rencontre Client</th>
                                                <th>Date Retour Après Rencontre</th>
                                                <th>Présentation >> Rencontre <br>[Date Rencontre Client - Date Présentation]</th>
                                                <th>Présentation >> Retour Après Rencontre <br>[Date Retour Après Rencontre - Date Présentation]</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $sum_t_presentation_rencontre = 0;  
                                            $sum_t_reactivite_client = 0;
                                            ?>
                                            <?php foreach($time_ops as $time_op): ?>
                                            <tr>
                                                <td align="center">
                                                    <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $time_op['mission_id'] ?>&tdc_id=<?php echo $time_op['tdc_suivi_id'] ?>" target="_blank">
                                                        <?php echo $time_op['mission_id'] ?>
                                                    </a>
                                                </td>
                                                <td><?php echo $time_op['poste'] ?></td>
                                                <td><?php echo $time_op['client'] ?></td>
                                                <td><?php echo mb_strtoupper($time_op['cand_nom']) . ' ' . mb_ucfirst($time_op['cand_prenom']) ?></td>
                                                <td>
                                                    <?php if ($time_op['control_statut_tdc'] > 0): ?>
                                                    
                                                        <span class="status <?php echo getClassStatusTDC($time_op['control_statut_tdc']) ?>">
                                                            <?php echo $statusTDC[$time_op['control_statut_tdc']] ?>
                                                        </span>
                                                    
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($time_op['date_prise_de_contact'] <> ""): ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_prise_de_contact'])) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($time_op['date_presentation'] <> ""): ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_presentation'])) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($time_op['date_rencontre_client'] <> ""): ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_rencontre_client'])) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($time_op['date_retour_apres_rencontre'] <> ""): ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_retour_apres_rencontre'])) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($time_op['t_presentation_rencontre'] > 0): ?>
                                                        <?php $sum_t_presentation_rencontre += $time_op['t_presentation_rencontre']; ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_rencontre_client'])) ?> - <?php echo date('Y-m-d', strtotime($time_op['date_presentation'])) ?>
                                                        = <?php echo seconds_to_hours_minutes_noS($time_op['t_presentation_rencontre']) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                    
                                                </td>
                                                <td>
                                                    <?php if ($time_op['t_reactivite_client'] > 0): ?>
                                                        <?php $sum_t_reactivite_client += $time_op['t_reactivite_client']; ?>
                                                        <?php echo date('Y-m-d', strtotime($time_op['date_retour_apres_rencontre'])) ?> - <?php echo date('Y-m-d', strtotime($time_op['date_presentation'])) ?>
                                                        = <?php echo seconds_to_hours_minutes_noS($time_op['t_reactivite_client']) ?>
                                                    <?php else: ?>
                                                        -
                                                    <?php endif; ?>
                                                    
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                            <tr class="total" style="background-color: #92D050;color: #fff;">
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td align="center"><strong>Total Candidats = <?php echo count($time_ops); ?></strong></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td align="center"><strong>Total = <?php echo seconds_to_hours_minutes_noS($sum_t_presentation_rencontre) ?></strong></td>
                                                <td align="center"><strong>Total = <?php echo seconds_to_hours_minutes_noS($sum_t_reactivite_client) ?></strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                </div><!-- / nano-content -->
                            </div><!-- / nano -->
                            
                        </div><!-- / block-body -->

                    </div>
                </div><!-- /col -->
                
            </div><!-- / row -->
                    
        </div><!-- /contentWrap -->
        
    </div><!-- /base -->
    
	<?php require_once './views/debug.php'; ?>
    
</body>
</html>
<script>
function frm_kpi_clients_ajax_success (result, status) {
    $('.kpi_clients_results').html(result);
}
</script>