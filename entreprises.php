<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Entreprises" ?>
<?php $page = "entreprises"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('create_candidat', 'read_candidat', 'edit_candidat', 'tdc'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>

	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base">

        <div id="contentWrap">
        
			<section class="title block">

				<h1 class="pull-left">Entreprises</h1>
				
                <?php if (User::can('create_candidat')): ?>
				<div class="btn-box pull-right">
                    <button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page précédente" onClick="window.history.back();"><span class="glyphicon glyphicon-menu-left"></span></button>
                    
                    <?php $url = BASE_URL . '/ajoute-entreprise' ?>
                    <?php if (get('tdc_id') && get('tdc_id') != ""): ?>
                        <?php $url = BASE_URL . '/ajoute-entreprise?tdc_id=' . get('tdc_id') ?>
                    <?php endif; ?>
					<a href="<?php echo $url ?>">
                        <button type="button" class="btn btn-icon btn-primary pull-right tooltips" title="Ajouter Entreprise"><span class="glyphicon glyphicon-plus"></span></button>
                    </a>
                    
				</div>
                <?php endif; ?>
				
			</section>
			
            <?php if (User::can('read_candidat')): ?>
				<?php require_once './views/entreprise-search.php'; ?>
			<?php endif; ?>
            
            <div class="entreprises-listing">
                <!-- load by ajax search form -->
            </div>
            
        </div><!-- /contentWrap -->
        
    </div><!-- /base --> 
	
	<?php require_once './views/debug.php'; ?>  
    
</body>
</html>
<script src="<?php echo JS_URL ?>/entreprise.js"></script>