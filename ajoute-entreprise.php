<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Ajouter Entreprise" ?>
<?php $page = "add-entreprise"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('create_candidat'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>

	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base" class="AddEntreprise">

        <div id="contentWrap">					
            
			<section class="title block">

				<h1 class="pull-left"><span class="glyphicon glyphicon-tower"></span>Ajouter Entreprise</h1>
				
				<div class="btn-box pull-right">
                    <?php if (get('tdc_id') && get('tdc_id') != ""): ?>
                    <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo get('tdc_id') ?>">
                        <button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page précédente"><span class="glyphicon glyphicon-menu-left"></span></button>
                    </a>
                    <?php else: ?>
					<button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page précédente" onClick="window.history.back();"><span class="glyphicon glyphicon-menu-left"></span></button>
                    <?php endif; ?>
				</div>
				
			</section>
			
            <div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
												
						<div class="block-body">
							
                            <form class="frm_frm" name="frm_create_entreprise" id="frm_create_entreprise" method="POST" action="<?php echo AJAX_HANDLER ?>/add-entreprise" enctype="multipart/form-data">
                                
                                <?php if (get('tdc_id') && get('tdc_id') != ""): ?>
                                <input type="hidden" name="mission_id" value="<?php echo get('tdc_id') ?>">
                                <?php endif; ?>
                                
								<div class="row">
				
									<div class="col col-6">
										<div class="block nopadding">
																	
											<div class="block-body">
												
                                                <fieldset class="mission-checkbox">
                                                    <input type="checkbox" class="frm_checkbox" name="is_mother_company" id="is_mother_company">
                                                    <label class="pointer" for="is_mother_company">Est Une Entreprise Mère</label>
                                                </fieldset>
                                                <input type="hidden" name="is_mother" id="is_mother" value="null">
                                                
												<fieldset>
													<label>Nom</label>
													<input class="frm_text caps must checkSocieteBlackliste" name="name" placeholder="Nom" type="text" autocomplete="off" data-validation="val_blank">
                                                    
                                                    <div class="msgBlackliste" style="display:none;">
                                                        <div class="clearboth notif warning pull-right" style="background-color:#ff9800; color: #fff; width: 70%;line-height: 24px; padding: 0px 10px;"><strong>Attention :</strong> La Société est blacklistée</div>
                                                    </div><!-- / msgBlackliste -->
                                                    
												</fieldset>
                                                
<!--                                                <fieldset class="mother_company">
													<label>Entreprise Mère</label>
                                                    <input class="frm_text caps must" id="mother_autocomplete" placeholder="Entreprise Mère" type="text" data-validation="val_blank">
                                                    <input type="hidden" name="parent_id" id="mother_autocomplete_id">
												</fieldset>-->
                                                
                                                <fieldset class="mother_company">
													<label>Entreprise Mère</label>
													<select class="frm_chosen" name="parent_id" id="parent_id" data-validation="val_blank">
														<option value="">Choisir Entreprise Mère</option>
														<?php $meres = Candidat::getEntrepriseMeres() ?>
                                                        <?php foreach($meres as $mere): ?>
                                                        <option value="<?php echo $mere['id'] ?>">
                                                            <?php echo $mere['name'] ?>
                                                            <?php if ($mere['blacklist'] == 1) :?>
                                                             - [Blacklistée]
                                                            <?php endif;?>
                                                        </option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
                                                
                                                <fieldset>
													<label>Localisation</label>
													<select class="frm_chosen" name="control_localisation_id" data-validation="val_blank">
														<option value="">Choisir Localisation</option>
														<?php $controls = Control::getControlByType(6, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
                                                
                                                <fieldset>
													<label>Précision adresse</label>
                                                    <textarea name="addresse" class="frm_textarea" placeholder="Précision adresse" data-validation="val_blank" maxlength="255"></textarea>                                
												</fieldset>

                                                <fieldset>
													<label>Secteur</label>
													<select class="frm_chosen" name="secteur_activite_id" data-validation="val_blank">
														<option value="">Choisir Secteur</option>
														<?php $controls = Control::getControlByType(2, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
                                                
                                                <fieldset>
													<label>Sous Secteur Activité</label>
													<input class="frm_text" name="sous_secteur_activite" placeholder="Sous Secteur Activité" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
                                                
												<fieldset>
													<label>Téléphone</label>
													<input class="frm_text tel_fr" name="telephone" placeholder="Téléphone" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
												<fieldset>
													<label>Email</label>
													<input class="frm_text" name="email" placeholder="Email" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
                                                
                                                
                                                
<!--												<fieldset>
													<label>Région</label>
													<input class="frm_text" name="region" placeholder="Région" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>-->
												

                                                <fieldset class="client-checkbox">
                                                    <input type="checkbox" class="frm_checkbox" id="blacklist" name="blacklist">
                                                    <label class="pointer" for="blacklist">Blacklister</label>
                                                </fieldset>
                                                                                                
											</div><!-- / block-body -->
											
										</div>
									</div><!-- /col -->
									
									<div class="col col-6">
										<div class="block nopadding">
																	
											<div class="block-body">

												<fieldset>
													<label>Commentaire</label>
                                                    <textarea name="commentaire" class="frm_textarea" placeholder="Commentaire" data-validation="val_blank" style="height:169px;"></textarea>                                
												</fieldset>
                                                <fieldset class="uploadMultiple">
                                                    <div class="clearfix">
                                                        <label class="pull-left">Documents <span class="glyphicon glyphicon-info-sign tooltips" title="Les fichiers autorisés sont : .doc, .docx, .xls, .xlsx, .pdf, .jpg, .jpeg, .png ; Taille max : 10 Mo"></span></label>
                                                        <input type="hidden" name="uploadFlag" id="uploadFlag" value="1">
                                                        <div class="clearfix add_field_wrap_documents pull-left" style="width: 60%;">
                                                            <div class="add_field_row pull-left">
                                                                <input type="file" name="file[]" id="file_1" data-number="1" class="inputfile inputfile-1">
                                                                <label id="uploadLabel_1" for="file_1" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span></label>
                                                                <button type="button" class="clear_field_upload btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt6 clearfix">
                                                        <button type="button" class="add_field_upload btn btn-icon btn-success pull-right tooltips" title="Ajouter un nouveau fichier"><span class="glyphicon glyphicon-plus"></span></button>
                                                    </div>                              
												</fieldset>
												<fieldset>
													<button type="button" class="btn btn-primary frm_submit frm_before_submit frm_notif pull-right" data-form="6"><i class="ico-txt fa fa-plus"></i> Ajouter Entreprise</button>
												</fieldset>
												
											</div><!-- / block-body -->
											
										</div>
									</div><!-- /col -->
									
								</div><!-- / row -->	

							</form>
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
        
        </div><!-- /contentWrap -->
        
    </div><!-- /base --> 
	
	<?php require_once './views/debug.php'; ?>  
    
</body>
</html>
<script>
$(document).ready(function(){
    
    $(document).on('ifUnchecked', '#is_mother_company', function(){
        $('#is_mother').val('null');
        //$('#mother_autocomplete').addClass('must').removeAttr('disabled');
        $('#parent_id').addClass('must');
        $('fieldset.mother_company').fadeIn(100);
        
    });
    $(document).on('ifChecked', '#is_mother_company', function(){
        $('#is_mother').val('1');
        //$('#mother_autocomplete').removeClass('must').attr('disabled', 'disabled');
        $('#parent_id').removeClass('must');
        $('fieldset.mother_company').fadeOut(100);
    });
    
//    $("#mother_autocomplete").autocomplete({
//        minLength: 3,
//        source: function(request, response) {
//            // Fetch data
//            $.ajax({
//                url: AJAX_HANDLER + "/search-candidat-companies-mother",
//                type: 'post',
//                dataType: "json",
//                data: {
//                    search: request.term
//                },
//                // response must be {value: id of the data, label: name to display}
//                success: function( data ) {
//                    response( data );
//                }
//            });
//        },
//        select: function (event, ui) {
//            // Set selection
//            $('#mother_autocomplete').val(ui.item.label); // display the selected text
//            $('#mother_autocomplete_id').val(ui.item.value); // save selected id to input
//            return false;
//        },
//        focus: function(event, ui){
//            $('#mother_autocomplete').val(ui.item.label); // display the selected text
//            $('#mother_autocomplete_id').val(ui.item.value); // save selected id to input
//            return false;
//        },
//    });
    
    // check candidat societe blacklister
    $(document).on('blur', '.checkSocieteBlackliste', function(){
        if ($(this).val() != "") {
            
            ajaxBusy = true;
            var nom_societe = $(this).val();
            
            // check if the company already exist and add
            $.ajax({
                url: AJAX_HANDLER+'/check-societe-candidat-if-exist?nom_societe='+nom_societe,
                dataType : 'json',
                cache: false,
                success: function (result) {
                    if (result.status === "NOK") {
                        $('.frm_text.checkSocieteBlackliste').removeClass('ok').addClass('error').val("");
                        Notif.Show('Entreprise ' + nom_societe + ' existe déjà', 'error', false);
                    } else {
                        $('.frm_text.checkSocieteBlackliste').removeClass('error').addClass('ok');
                        
                        var url = AJAX_HANDLER+'/check-societe-candidat-blackliste?nom_societe='+nom_societe;
                        $.ajax({
                            url: url,
                            dataType : 'json',
                            cache: false,
                            success: function (result, status) {
                                if (status === "success" && result.status === "OK") {
                                    $('.msgBlackliste').find('.notif').html(result.msg);
                                    $('.msgBlackliste').fadeIn();
                                } else if (status === "success" && result.status === "NOK") {
                                    $('.msgBlackliste').fadeOut();
                                }
                            }
                        });
                        
                    }
                }
            });
            
            
            
        } else {
            $('.msgBlackliste').fadeOut();
        }
            
    });
    
    <?php if (get('msg') && get('msg') != "" && get('tdc_id') == ""): ?>
        Notif.Show('<?php echo $error_codes[get('msg')] ?>', 'error', true, 3000, 'reloadpagenoparam');
    <?php elseif (get('msg') && get('msg') != "" && get('tdc_id') && get('tdc_id') != ""): ?>
        var param = {'querystring': 'tdc_id', 'value': '<?php echo get('tdc_id') ?>'};
        Notif.Show('<?php echo $error_codes[get('msg')] ?>', 'error', true, 3000, 'reloadpageparam', param);
    <?php endif; ?>  
    
    Upload.Init();
    Upload.Validate();
        
    // add a new field for upload
    $(document).on('click', 'button.add_field_upload', function(){
        
        // get the last input number
        var number = parseInt($('.add_field_wrap_documents .add_field_row:last input.inputfile').attr('data-number')) + 1;
        
        var inputHtml = '<input type="file" name="file[]" id="file_'+number+'" data-number="'+number+'" class="inputfile inputfile-1"><label id="uploadLabel_'+number+'" for="file_'+number+'" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span></label><button type="button" class="clear_field_upload btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>';
        var newFieldHtml = '<div class="add_field_row clearfix">'+inputHtml+'</div>';
        $('.add_field_wrap_documents').append(newFieldHtml);
        
        var flagNumber = parseInt($('#uploadFlag').val()) + 1;
        $('#uploadFlag').val(flagNumber);
        
        Upload.Init();
    });
    
    $(document).on('click', '.clear_field_upload', function(){
        
        var numRows = parseInt($('fieldset.uploadMultiple .add_field_wrap_documents .add_field_row:last').index()) + 1;
        
        if (numRows >= 2) {
            $(this).parent('.add_field_row').remove();
        } else if (numRows == 1) {
            $(this).parent('.add_field_row').find('input.inputfile').val('');
            $(this).parent('.add_field_row').find('label').html('<span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span>');
        }
        
    }); 
    
}); // end document ready function

var Upload = {
    Init: function (){
        $('.inputfile').each(function(){
		
            var $input	 = $( this ),
                $label	 = $input.next( 'label' ),
                labelVal = $label.html();

            $input.on( 'change', function( e )
            {
                var fileName = '';

                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else if( e.target.value )
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    $label.find( 'span.filename' ).html( fileName );
                else
                    $label.html( labelVal );
            });

            // Firefox bug fix
            $input
            .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
            .on( 'blur', function(){ 
                $input.removeClass( 'has-focus' ); 
            });

        });
    },
    Validate: function (){
        $(document).on('change', '.inputfile', function(){
              Upload.ValidateUpload($(this));            
        });
    },
    ValidateUpload: function (input) {
        var file_data = input.prop("files")[0];
        // start validate file extension
        var validExtensions = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx', 'xls', 'xlsx']; //array of valid extensions
        var filename = file_data.name;
        var ext = filename.substr(filename.lastIndexOf('.') + 1);
        if ($.inArray(ext, validExtensions) == -1){
            Notif.Show('Fichier non valide', 'error', true, 4000);
            input.val("");
            input.next('label').removeClass('ok').addClass('error');
            return false;
        } else {
            // check file size
            // 4MB allowed
            var filesize = file_data.size;
            if (filesize > 10485760) {
                Notif.Show('La taille du fichier est supérieure à celle autorisée. Télécharger moins de 10 Mo.', 'error', true, 4000);
                input.val("");
                input.next('label').removeClass('ok').addClass('error');
                return false;
            } else {
                input.next('label').removeClass('error').addClass('ok');
                return true;
            }
        }
        // end validation file extension

    },
}

function frm_create_entreprise_before_submit(obj) {  
    
    // verify upload
    var uploadFlag = true;
    var numLabel = parseInt($('#uploadFlag').val());
    
    for(var i = 1; i <= numLabel; i++) {
        if ($('#uploadLabel_'+numLabel).length) {
            if ($('#uploadLabel_'+numLabel).hasClass('error')) {
                uploadFlag = false;
            }
        }
    }
    
    return uploadFlag;
    
}
</script>