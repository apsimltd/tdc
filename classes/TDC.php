<?php
class TDC {
    
    public static function getTotalCandidatInIleDeFrance($mission_id) {
        global $con;
        $sql = "SELECT COUNT(*) as num_candidates "
                . "FROM tdc T "
                . "LEFT JOIN candidats C ON C.id = T.candidat_id "
                . "WHERE T.mission_id = {$mission_id} AND T.candidat_id > 0 AND C.control_localisation_id = 221";
        $result = $con->fetchAssoc($sql);
        return $result['num_candidates'];
    }
    
    public static function getTDCCandidatAssignedUsers($mission_id) {
        
        global $con;
        
        $sql = "SELECT U.id, U.firstName, U.lastName, R.name as role_name 
FROM missionsusers M
LEFT JOIN users U ON U.id = M.user_id
LEFT JOIN roles R ON R.id = U.role_id 
WHERE M.mission_id = {$mission_id}
UNION
SELECT U.id, U.firstName, U.lastName, R.name as role_name 
FROM missions M
LEFT JOIN users U ON U.id = M.manager_id
LEFT JOIN roles R ON R.id = U.role_id 
WHERE M.id = {$mission_id}";

        return $con->fetchAll($sql);
        
    }
    
    public static function updateColorsLegend($data) {
        global $con;
        
        foreach($data as $color):
            $con->update('tdccolorslegend', array('description' => $color['desc'], 'updated' => time()), array('id' => $color['id']));
        endforeach;
        
        return true;
    }
    
    public static function updateColorsLegendEntreprise($data) {
        global $con;
        
        foreach($data as $color):
            $con->update('tdccolorslegendentreprise', array('description' => $color['desc'], 'updated' => time()), array('id' => $color['id']));
        endforeach;
        
        return true;
    }
    
    public static function AddColorsLegend($mission_id) {
        global $con;
        $sql = "SELECT * "
            . "FROM colorstdc c ORDER BY id ASC ";
            
        $colors = $con->fetchAll($sql);
        foreach($colors as $color) :
            $data = array(
                'mission_id' => $mission_id,
                'colorstdc_id' => $color['id'],
                'updated' => time(),
            );
            $con->insert('tdccolorslegend', $data);
        endforeach;
    }
    
    public static function AddColorsLegendEntreprise($mission_id) {
        global $con;
        $sql = "SELECT * "
            . "FROM colorstdcentreprise c ORDER BY id ASC ";
            
        $colors = $con->fetchAll($sql);
        foreach($colors as $color) :
            $data = array(
                'mission_id' => $mission_id,
                'colorstdc_id' => $color['id'],
                'updated' => time(),
            );
            $con->insert('tdccolorslegendentreprise', $data);
        endforeach;
    }
    
    public static function getColorsLegend($mission_id) {
        global $con;
        $sql = "SELECT l.*, c.hex "
            . "FROM colorstdc c, tdccolorslegend l "
            . "WHERE c.id = l.colorstdc_id AND l.mission_id = {$mission_id}";
            
        return $con->fetchAll($sql);    
    }
    
    public static function getColorsLegendEntreprise($mission_id) {
        global $con;
        $sql = "SELECT l.*, c.hex "
            . "FROM colorstdcentreprise c, tdccolorslegendentreprise l "
            . "WHERE c.id = l.colorstdc_id AND l.mission_id = {$mission_id}";
            
        return $con->fetchAll($sql);    
    }
    
    public static function addEntreprise($data) {
        global $con;
                
        $con->insert('tdc', $data);
        
        $tdc_id = $con->lastInsertId();

        //watchdog
        
        $content_before = array(
            'tdc' => $data,
        );
        
        $log  = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC Ajouter Entreprise',
            'action_code' => 'mod_tdc_add_entreprise',
            'mission_id' => $data['mission_id'],
            'entreprise_id' => $data['entreprise_id'],
            'tdc_id' => $tdc_id,
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
        );        
        Watchdog::Log($log);
        
        // return true;
        return $tdc_id;
    }
    
    public static function addCandidat($data) {
        global $con;
                
        $con->insert('tdc', $data);
        
        $tdc_id = $con->lastInsertId();

        // we also insert a row in table tdcsuivis at once
        $suivi = array(
            'user_id' => $data['user_id'],
            'mission_id' => $data['mission_id'],
            'candidat_id' => $data['candidat_id'],
            'created' => time(),
        );
        
        $con->insert('tdcsuivis', $suivi);
        
        $tdcsuivi_id = $con->lastInsertId();

        //watchdog
        
        $content_before = array(
            'tdc' => $data,
            'tdc_suivi' => $suivi,
        );
        
        $log  = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC Ajouter Candidat',
            'action_code' => 'mod_tdc_add_candidat',
            'mission_id' => $data['mission_id'],
            'candidat_id' => $data['candidat_id'],
            'tdc_id' => $tdc_id,
            'tdcsuivi_id' => $tdcsuivi_id,
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
        );        
        Watchdog::Log($log);
        
        /// return true;
        return $tdc_id;
    }
    
    public static function getTdcEntreprise($mission_id, $entreprise_id) {
        global $con;
        $sql = "SELECT * FROM tdc WHERE entreprise_id = {$entreprise_id} AND mission_id = {$mission_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getTdc($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT * FROM tdc WHERE candidat_id = {$candidat_id} AND mission_id = {$mission_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getTdcSuivi($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT * FROM tdcsuivis WHERE candidat_id = {$candidat_id} AND mission_id = {$mission_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function deleteEntrepriseInTDC($mission_id, $entreprise_id) {
        
        global $con;
        
        // delete candidat in tdc
        $tdc = self::getTdcEntreprise($mission_id, $entreprise_id);
        $con->delete('tdc', array('entreprise_id' => $entreprise_id, 'mission_id' => $mission_id));
        
        //watchdog
        
        $content_before = array(
            'tdc' => $tdc,
        );
        
        $log  = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC Supprimer Entreprise',
            'action_code' => 'mod_tdc_delete_entreprise',
            'mission_id' => $mission_id,
            'entreprise_id' => $entreprise_id,
            'tdc_id' => $tdc['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function deleteCandidatInTDC($mission_id, $candidat_id) {
        
        global $con;
        
        // delete candidat in tdc
        $tdc = self::getTdc($mission_id, $candidat_id);
        $con->delete('tdc', array('candidat_id' => $candidat_id, 'mission_id' => $mission_id));
        // delete candidat in tdcsuivis
        $tdcsuivi = self::getTdcSuivi($mission_id, $candidat_id);
        $con->delete('tdcsuivis', array('candidat_id' => $candidat_id, 'mission_id' => $mission_id));
        
        //watchdog
        
        $content_before = array(
            'tdc' => $tdc,
            'tdc_suivi' => $tdcsuivi,
        );
        
        $log  = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC Supprimer Candidat',
            'action_code' => 'mod_tdc_delete_candidat',
            'mission_id' => $mission_id,
            'candidat_id' => $candidat_id,
            'tdc_id' => $tdc['id'],
            'tdcsuivi_id' => $tdcsuivi['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function updateEntrepriseInTDCStatus($tdc_id, $data) {
        global $con;
        
        // update tdc status ordering tdc.control_statut_tdc_order
        /**
         * ordering output
         * 1: FINI
         * 2: OUT
         * 3: none blank 
         */
        $order_value = 3;
        if ($data['status_entreprise'] == 258) { // FINI
            $order_value = 1;
        } elseif ($data['status_entreprise'] == 257) { // OUT
            $order_value = 2;
        }
        
        $data['status_entreprise_order'] = $order_value;
        
        $con->update('tdc', $data, array('id' => $tdc_id)); 
        
        // check if status has changed the log
        if ($data['status_entreprise_old'] <> $data['status_entreprise']) {
            
            $log  = array(
                'module_name' => 'TDC',
                'module_code' => 'mod_tdc',
                'action' => 'TDC MAJ Entreprise Statut',
                'action_code' => 'mod_tdc_status_entreprise',
                'table_name' => 'tdc',
                'table_id' => $tdc_id,
                'mission_id' => $data['mission_id'],
                'entreprise_id' => $data['entreprise_id'],
                'tdc_id' => $tdc_id,
                'status_before' => $data['status_entreprise_old'],
                'status_after' =>  $data['status_entreprise'],
            );        
            Watchdog::Log($log);
            
        }
        return true;
    }
    
    public static function updateCandidatInTDCStatus($tdc_id, $data) {
        global $con;
        
        // update tdc status ordering tdc.control_statut_tdc_order
        /**
         * ordering output
         * 1: IN (placé) (short listé)  => control_statut_tdc = 229 AND tdcsuivis.place = 1 AND tdcsuivis.shortliste = 1
         * 2: IN (placé) => control_statut_tdc = 229 AND tdcsuivis.place = 1
         * 3: IN (short listé) => control_statut_tdc = 229 AND tdcsuivis.shortliste = 1
         * 4: IN => control_statut_tdc = 229
         * 5: ARC => control_statut_tdc = 251
         * 6: ARCA => control_statut_tdc = 252
         * 7: BU+ => control_statut_tdc = 228
         * 8: BU – => control_statut_tdc = 227
         * 9: Ex BU => control_statut_tdc = 259
         * 10: OUT => control_statut_tdc = 226
         * 11: Sans statut => control_statut_tdc = 0
         * 
         * 
         */
        $order_value = 11;
        if ($data['control_statut_tdc'] == 229) {
            
            $suivi = self::getTdcSuivi($data['mission_id'], $data['candidat_id']);
            if ($suivi['place'] == 1 && $suivi['shortliste'] == 1) {
                $order_value = 1;
            } elseif ($suivi['place'] == 1 && $suivi['shortliste'] == 0) {
                $order_value = 2;
            } elseif ($suivi['place'] == 0 && $suivi['shortliste'] == 1) {
                $order_value = 3;
            } elseif ($suivi['place'] == 0 && $suivi['shortliste'] == 0) {
                $order_value = 4;
            }
        } elseif ($data['control_statut_tdc'] == 251) {
             $order_value = 5;
        } elseif ($data['control_statut_tdc'] == 252) {
             $order_value = 6;
        } elseif ($data['control_statut_tdc'] == 228) {
             $order_value = 7;
        } elseif ($data['control_statut_tdc'] == 227) {
             $order_value = 8;
        } elseif ($data['control_statut_tdc'] == 0) {
             $order_value = 11;
        } elseif ($data['control_statut_tdc'] == 226) {
             $order_value = 10;
        } elseif ($data['control_statut_tdc'] == 259) {
             $order_value = 9;
        }
        
        $data['control_statut_tdc_order'] = $order_value;
        
        $con->update('tdc', $data, array('id' => $tdc_id)); 
        
        // check if status has changed the log
        if ($data['control_statut_tdc_old'] <> $data['control_statut_tdc']) {
            
            $log  = array(
                'module_name' => 'TDC',
                'module_code' => 'mod_tdc',
                'action' => 'TDC MAJ Candidat Statut',
                'action_code' => 'mod_tdc_status_candidat',
                'table_name' => 'tdc',
                'table_id' => $tdc_id,
                'mission_id' => $data['mission_id'],
                'candidat_id' => $data['candidat_id'],
                'tdc_id' => $tdc_id,
                'status_before_id' => $data['control_statut_tdc_old'],
                'status_before' => Control::getControlNameById($data['control_statut_tdc_old']),
                'status_after_id' => $data['control_statut_tdc'],
                'status_after' => Control::getControlNameById($data['control_statut_tdc']),
            );        
            Watchdog::Log($log);
            
            // log for timers
            $timer = array(
                'created' => dateToDb(),
                'mission_id' => $data['mission_id'],
                'candidat_id' => $data['candidat_id'],
                'status_before_id' => $data['control_statut_tdc_old'],
                'status_before' => Control::getControlNameById($data['control_statut_tdc_old']),
                'status_after_id' => $data['control_statut_tdc'],
                'status_after' => Control::getControlNameById($data['control_statut_tdc']),
            );
            Watchdog::timer($timer);     
            
            // check if status has changed and if allowed to be seen in tdc client
            // send an email that the status has been changed from old status to new status
            
            //allowed status for client to see 
            $allowed_status = array(229, 251, 228, 227, 259, 262);
            if (in_array($data['control_statut_tdc'], $allowed_status)) { // if new status can be seen by the client
                
                $tdc = TDC::getTDCById($tdc_id);
                
                // if the client can see the candidate
                if ($tdc['visibilite_client'] == 1) {

                    // there may be many client assigned to the missions and we send to all of them
                    $mission_clients = Mission::getAssignedClientToMissions($tdc['mission_id']);

                    if (!empty($mission_clients)) {

                        $mission = Mission::getMissionById($data['mission_id']);

                        if ($mission['prestataire_id'] == 1) {
                            $logo_mail = "opsearch";
                        } else {
                            $logo_mail = "headhunting";
                        }

                        $link_mission = BASE_URL . "/tdc-client?id=" . $data['mission_id'];

                        $candidat = Candidat::getCandidatById($data['candidat_id']);
                        
                        $status = Control::getControlListByType(8); 
                        
                        $suivi = TDC::getSuivi($data['mission_id'], $data['candidat_id']);
                        
                        if ($data['control_statut_tdc'] == 229) { // IN
                            if ($suivi['place'] == 1 && $suivi['shortliste'] == 1) {
                                $new_status = "IN Placé";
                            } else if ($suivi['place'] == 1 && $suivi['shortliste'] == 0) {
                                $new_status = "IN Placé";
                            } else if ($suivi['place'] == 0 && $suivi['shortliste'] == 1) {
                                $new_status = "IN SL";
                            } else if ($suivi['place'] == 0 && $suivi['shortliste'] == 0) {
                                $new_status = $status[$data['control_statut_tdc']];
                            }
                        } else {
                            $new_status = $status[$data['control_statut_tdc']];
                        }


                        $mail_body = "<strong>Changement statut du candidat sur TDC " . $data['mission_id'] . "</strong><br/>";
                        $mail_body .= "<strong>Référence du candidat : </strong> " . $candidat['id'] . "<br>";
                        $mail_body .= "<strong>Candidat : </strong> " . mb_strtoupper($candidat['nom']) . ' ' . mb_ucfirst($candidat['prenom']) . "<br>";
                        $mail_body .= "<strong>Nouveau statut : </strong> " . $new_status . "<br>";
                        $mail_body .= "Cliquez sur le lien suivant : ";
                        $mail_body .= "<a href='" . $link_mission . "'>TDC " . $data['mission_id'] . "</a><br/><br/>";
                        $mail_body .= "Ou copier coller le lien suivant dans votre navigateur.<br/>";
                        $mail_body .= $link_mission . "<br/><br/>";

                        $mail_subject = "Changement statut du candidat sur TDC " . $data['mission_id'];

                        foreach($mission_clients as $mission_client) :

                            if ($mission_client['mail_notif'] == 1) :

                                $mail_body_into = "Bonjour " . $mission_client['lastName'] . " " . $mission_client['firstName'] . ",<br/><br/>";

                                $new_mail_body = $mail_body_into . $mail_body;

                                if (Mail::sendMail($mail_subject, $mission_client['email'], $new_mail_body, $logo_mail)) {
                                    // log                             
                                    $log  = array(
                                        'module_name' => 'Mail',
                                        'module_code' => 'mod_mail',
                                        'action' => 'Envoie Mail Changement Statut Candidat TDC',
                                        'action_code' => 'mod_mail_send_mail_change_status_candidat',
                                        'table_name' => 'tdc',
                                        'table_id' => $tdc_id,
                                        'content_type' => 'json',
                                        'content_before' => json_encode(array('to' => $mission_client['email'], 'to_user_id' => $mission_client['id'], 'subject' => $mail_subject, 'body' => $new_mail_body, 'mission_id' => $data['mission_id'], 'candidat_id' => $data['candidat_id'])),
                                    );
                                    Watchdog::log($log);
                                }

                            endif;

                        endforeach;

                    }

                }
                
            }
            
        }
        return true;
    }
    
    public static function updateCandidatInTDC($tdc_id, $data) {
        global $con;
        
        $con->update('tdc', $data, array('id' => $tdc_id)); 
        
        $log  = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC CCC Candidat',
            'action_code' => 'mod_tdc_ccc_candidat',
            'table_name' => 'tdc',
            'table_id' => $tdc_id,
            'tdc_id' => $tdc_id,
            'content_type' => 'json',
            'content_before' => json_encode($data),
        ); 
        
        if (isset($data['mission_id'])) {
            $log['mission_id'] = $data['mission_id'];
        }
        
        if (isset($data['candidat_id'])) {
            $log['candidat_id'] = $data['candidat_id'];
        }
        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function updateVisibiliteCandidatInTDC($tdc_id, $data) {
        global $con;
        
        $con->update('tdc', $data, array('id' => $tdc_id)); 
        
        $log  = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC Visibilite Client Candidat',
            'action_code' => 'mod_tdc_visibilite_client_candidat',
            'table_name' => 'tdc',
            'table_id' => $tdc_id,
            'tdc_id' => $tdc_id,
            'content_type' => 'json',
            'content_before' => json_encode($data),
        ); 
        
        if (isset($data['mission_id'])) {
            $log['mission_id'] = $data['mission_id'];
        }
        
        if (isset($data['candidat_id'])) {
            $log['candidat_id'] = $data['candidat_id'];
        }
        
        Watchdog::Log($log);
        
        // send client email alert about new candidat added from the tdc 
        if ($data['visibilite_client'] == 1) {
                       
            // there may be many client assigned to the missions and we send to all of them
            $mission_clients = Mission::getAssignedClientToMissions($data['mission_id']);
            
            if (!empty($mission_clients)) {
                
                $mission = Mission::getMissionById($data['mission_id']);
        
                if ($mission['prestataire_id'] == 1) {
                    $logo_mail = "opsearch";
                } else {
                    $logo_mail = "headhunting";
                }
                
                $link_mission = BASE_URL . "/tdc-client?id=" . $data['mission_id'];
                
                $candidat = Candidat::getCandidatById($data['candidat_id']);
                
                $mail_body = "<strong>Nouveau candidat ajouté sur TDC " . $data['mission_id'] . "</strong><br/>";
                $mail_body .= "<strong>Référence du candidat : </strong> " . $candidat['id'] . "<br>";
                $mail_body .= "<strong>Candidat : </strong> " . mb_strtoupper($candidat['nom']) . ' ' . mb_ucfirst($candidat['prenom']) . "<br>";
                $mail_body .= "Cliquez sur le lien suivant : ";
                $mail_body .= "<a href='" . $link_mission . "'>TDC " . $data['mission_id'] . "</a><br/><br/>";
                $mail_body .= "Ou copier coller le lien suivant dans votre navigateur.<br/>";
                $mail_body .= $link_mission . "<br/><br/>";

                $mail_subject = "Nouveau candidat ajouté sur TDC " . $data['mission_id'];
                
                foreach($mission_clients as $mission_client) :
                    
                    if ($mission_client['mail_notif'] == 1) :
                    
                        $mail_body_into = "Bonjour " . $mission_client['lastName'] . " " . $mission_client['firstName'] . ",<br/><br/>";

                        $new_mail_body = $mail_body_into . $mail_body;

                        if (Mail::sendMail($mail_subject, $mission_client['email'], $new_mail_body, $logo_mail)) {
                            // log                             
                            $log  = array(
                                'module_name' => 'Mail',
                                'module_code' => 'mod_mail',
                                'action' => 'Envoie Mail Visibilite Candidat TDC',
                                'action_code' => 'mod_mail_send_mail_visibilite_candidat',
                                'table_name' => 'tdc',
                                'table_id' => $tdc_id,
                                'content_type' => 'json',
                                'content_before' => json_encode(array('to' => $mission_client['email'], 'to_user_id' => $mission_client['id'], 'subject' => $mail_subject, 'body' => $new_mail_body, 'mission_id' => $data['mission_id'], 'candidat_id' => $data['candidat_id'])),
                            );
                            Watchdog::log($log);
                        }
                        
                    endif;
                    
                endforeach;
                
            }
            
        }
        
        
        
        return true;
    }
    
    public static function updateEntrepriseColorsInTDC($tdc_id, $data) {
        global $con;
        
        $con->update('tdc', $data, array('id' => $tdc_id)); 
        
        return true;
    }
    
    public static function updateEntrepriseStatusInTDC($mission_id, $entreprise_id) {
        global $con;
        
        $con->update('tdc', array('status' => 1, 'modified' => time()), array('mission_id' => $mission_id, 'entreprise_id' => $entreprise_id));
        /// return true;
        // get the id to return
        $sql = "SELECT * FROM tdc WHERE mission_id = {$mission_id} AND entreprise_id = {$entreprise_id}";
        $result = $con->fetchAssoc($sql);
        return $result['id'];
    }
    
    public static function updateCandidatStatusInTDC($mission_id, $candidat_id) {
        global $con;
        
        $con->update('tdc', array('status' => 1, 'modified' => time()), array('mission_id' => $mission_id, 'candidat_id' => $candidat_id));
        /// return true;
        // get the id to return
        $sql = "SELECT * FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = {$candidat_id}";
        $result = $con->fetchAssoc($sql);
        return $result['id'];
        
    }

    public static function isCandidatInTDC($candidat_id, $mission_id) {
        global $con;
        $sql = "SELECT * FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = {$candidat_id} AND status = 1";
        $result = $con->fetchAssoc($sql);
        if (empty($result)) {
            return false;
        } else {
            return true;
        }
    }
    
    public static function isEntrepriseInTDC($mission_id, $entreprise_id) {
        global $con;
        $sql = "SELECT * FROM tdc WHERE mission_id = {$mission_id} AND entreprise_id = {$entreprise_id} AND status = 1";
        $result = $con->fetchAssoc($sql);
        if (empty($result)) {
            return false;
        } else {
            return true;
        }
    }
    
    public static function isCandidatExistInTDC($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT * FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = {$candidat_id} AND status = 1";
        $result = $con->fetchAssoc($sql);
        if (empty($result)) {
            return false;
        } else {
            return true;
        }
    }
    
    public static function getTDCById($tdc_id) {
        global $con;
        $sql = "SELECT * FROM tdc WHERE id = {$tdc_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getCandidatesByTDC_Client($mission_id, $candidat_ids = false) {
        global $con;           
                
        if (!$candidat_ids) {
                        
            $sql = "SELECT t.id as tdc_id, t.*, c.*, s.place, s.shortliste "
            . "FROM tdc t, candidats c, tdcsuivis s "
            . "WHERE t.candidat_id = c.id AND t.status = 1 AND t.mission_id = {$mission_id} AND s.mission_id = {$mission_id} AND s.candidat_id = t.candidat_id AND t.control_statut_tdc IN (229, 251, 228, 227, 259) AND t.visibilite_client = 1 "
            // . "ORDER BY t.id ASC";
            // . "ORDER BY c.societe_actuelle, t.control_statut_tdc ASC";
            . "ORDER BY t.control_statut_tdc_order, c.societe_actuelle ASC";
                        
            return $con->fetchAll($sql);           
                    
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
                        
            $ids = addSeperator($candidat_ids, $sep = ',');
            
            $sql = "SELECT t.id as tdc_id, t.*, c.*, s.place, s.shortliste "
            . "FROM tdc t, candidats c, tdcsuivis s "
            . "WHERE t.candidat_id = c.id AND t.status = 1 AND t.mission_id = {$mission_id} AND t.candidat_id IN ({$ids}) AND s.mission_id = {$mission_id} AND s.candidat_id = t.candidat_id AND t.control_statut_tdc IN (229, 251, 228, 227, 259) AND t.visibilite_client = 1 "
            // . "ORDER BY t.id DESC";
            // . "ORDER BY c.societe_actuelle, t.control_statut_tdc ASC";
            . "ORDER BY t.control_statut_tdc_order, c.societe_actuelle ASC";
            
            return $con->fetchAll($sql);
            
        }

    }
    
    public static function getCandidatesByTDC($mission_id, $candidat_ids = false) {
        global $con;           
                
        if (!$candidat_ids) {
                        
            $sql = "SELECT t.id as tdc_id, t.*, c.*, s.place, s.shortliste "
            . "FROM tdc t, candidats c, tdcsuivis s "
            . "WHERE t.candidat_id = c.id AND t.status = 1 AND t.mission_id = {$mission_id} AND s.mission_id = {$mission_id} AND s.candidat_id = t.candidat_id "
            // . "ORDER BY t.id ASC";
            // . "ORDER BY c.societe_actuelle, t.control_statut_tdc ASC";
            . "ORDER BY t.control_statut_tdc_order, c.societe_actuelle ASC";
                        
            return $con->fetchAll($sql);           
                    
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
                        
            $ids = addSeperator($candidat_ids, $sep = ',');
            
            $sql = "SELECT t.id as tdc_id, t.*, c.*, s.place, s.shortliste "
            . "FROM tdc t, candidats c, tdcsuivis s "
            . "WHERE t.candidat_id = c.id AND t.status = 1 AND t.mission_id = {$mission_id} AND t.candidat_id IN ({$ids}) AND s.mission_id = {$mission_id} AND s.candidat_id = t.candidat_id "
            // . "ORDER BY t.id DESC";
            // . "ORDER BY c.societe_actuelle, t.control_statut_tdc ASC";
            . "ORDER BY t.control_statut_tdc_order, c.societe_actuelle ASC";
            
            return $con->fetchAll($sql);
            
        }

    }
    
    public static function getEntreprisesByTDC($mission_id, $entreprise_ids = false) {
        global $con;           
                
        if (!$entreprise_ids) {
                        
            $sql = "SELECT t.id as tdc_id, t.*, e.* "
            . "FROM tdc t, candidatscompanies e "
            . "WHERE t.entreprise_id = e.id AND t.status = 1 AND t.mission_id = {$mission_id} "
            // . "ORDER BY t.id ASC";
            . "ORDER BY t.status_entreprise_order, e.name ASC";
                        
            return $con->fetchAll($sql);
            
        } else if ($entreprise_ids && is_array($entreprise_ids) && !empty($entreprise_ids)) {
                        
            $ids = addSeperator($entreprise_ids, $sep = ',');
            
            $sql = "SELECT t.id as tdc_id, t.*, e.* "
            . "FROM tdc t, candidatscompanies e "
            . "WHERE t.entreprise_id = e.id AND t.status = 1 AND t.mission_id = {$mission_id} AND t.entreprise_id IN ({$ids}) "
            // . "ORDER BY t.id DESC";
            . "ORDER BY t.status_entreprise_order, e.name ASC";
            
            return $con->fetchAll($sql);
            
        }

    }
    
    public static function getCritereSpec($candidat_id, $mission_id) {
        global $con;
        
        // get count criteres of this mission
        $sql = "SELECT COUNT(id) as TotalCriteres FROM missionscriteres WHERE mission_id = {$mission_id}";
        $result =  $con->fetchAssoc($sql);
        $totalCriteres = $result['TotalCriteres'];
        
        // get count of criteres for the candidate in this mission
        $sql = "SELECT COUNT(id) as Total FROM tdccriteres WHERE candidat_id = {$candidat_id} AND mission_id = {$mission_id}";
        $result = $con->fetchAssoc($sql);
        $total = $result['Total'];
        
        return $total. '/' . $totalCriteres;
    }
    
    public static function getOutCategory($candidat_id, $mission_id) {
        global $con;
        
        // get count criteres of this mission
        $sql = "SELECT COUNT(id) as TotalCats FROM missionsoutcategories WHERE mission_id = {$mission_id}";
        $result =  $con->fetchAssoc($sql);
        $totalCats = $result['TotalCats'];
        
        // get count of criteres for the candidate in this mission
        $sql = "SELECT COUNT(id) as Total FROM tdcoutcategories WHERE candidat_id = {$candidat_id} AND mission_id = {$mission_id}";
        $result = $con->fetchAssoc($sql);
        $total = $result['Total'];
        
        return $total. '/' . $totalCats;
    }
    
    public static function getSelectedCriteres($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT critere_id FROM tdccriteres WHERE mission_id = {$mission_id} AND candidat_id = {$candidat_id}";
        $results = $con->fetchAll($sql);
        
        $critere_ids = array();
        foreach ($results as $result):
            $critere_ids[] = $result['critere_id'];
        endforeach;
        
        return $critere_ids;        
    }
    
    public static function updateCriteres($data) {
        global $con;
        
        $criteres_data = $data;
        
        $tdc_id = $data['tdc_id'];
        $commentaire_criteres = $data['commentaire_criteres'];
        
        unset($data['commentaire_criteres']);
        unset($data['tdc_id']);
        
        // update the comment in tdc
        $con->update('tdc', array('commentaire_criteres' => $commentaire_criteres, 'modified' => time()), array('id' => $tdc_id));
        
        // delete all data in tdccriteres and insert the new data
        $con->delete('tdccriteres', array('mission_id' => $data['mission_id'], 'candidat_id' => $data['candidat_id']));
        
        // insert the new data
        if ($data['criteres_ids'] != "") {
            $critere_ids = explode('|', $data['criteres_ids']);
            unset($data['criteres_ids']);
            foreach($critere_ids as $critere_id):
                $data['critere_id'] = $critere_id;
                $con->insert('tdccriteres', $data);
            endforeach;
        }
        
        $log = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC MAJ Critères spécifiques',
            'action_code' => 'mod_tdc_update_criteres',
            'mission_id' => $data['mission_id'],
            'candidat_id' => $data['candidat_id'],
            'tdc_id' => $tdc_id,
            'content_type' => 'json',
            'content_before' => json_encode($criteres_data),
        );        
        Watchdog::Log($log);
        
        
        return true;
    }
    
    public static function getSelectedOutCategories($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT outcategory_id FROM tdcoutcategories WHERE mission_id = {$mission_id} AND candidat_id = {$candidat_id}";
        $results = $con->fetchAll($sql);
        
        $out_catgory_ids = array();
        foreach ($results as $result):
            $out_catgory_ids[] = $result['outcategory_id'];
        endforeach;
        
        return $out_catgory_ids;        
    }
    
    public static function updateOutCategories($data) {
        global $con;
        
        $out_category_data = $data;
        
        $tdc_id = $data['tdc_id'];        
        unset($data['tdc_id']);
                        
        // delete all data in tdcoutcategories and insert the new data
        $con->delete('tdcoutcategories', array('mission_id' => $data['mission_id'], 'candidat_id' => $data['candidat_id']));
        
        // insert the new data
        if ($data['out_categorie_ids'] != "") {
            $out_categorie_ids = explode('|', $data['out_categorie_ids']);
            unset($data['out_categorie_ids']);
            foreach($out_categorie_ids as $out_categorie_id):
                $data['outcategory_id'] = $out_categorie_id;
                $con->insert('tdcoutcategories', $data);
            endforeach;
        }
        
        $log = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC MAJ Out categorie',
            'action_code' => 'mod_tdc_outcategory',
            'mission_id' => $data['mission_id'],
            'candidat_id' => $data['candidat_id'],
            'tdc_id' => $tdc_id,
            'content_type' => 'json',
            'content_before' => json_encode($out_category_data),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function getHistoriquesEntreprise($mission_id, $entreprise_id) {
        global $con;
        $sql = "SELECT T.*, U.firstName, U.lastName "
            . "FROM tdchistoriques T, users U "
            . "WHERE T.status = 1 AND U.id = T.user_id AND T.mission_id = {$mission_id} AND T.entreprise_id = {$entreprise_id} "
            . "ORDER BY T.id DESC";
        return $con->fetchAll($sql);
    }
    
    public static function getHistoriques($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT T.*, U.firstName, U.lastName "
            . "FROM tdchistoriques T, users U "
            . "WHERE T.status = 1 AND U.id = T.user_id AND T.mission_id = {$mission_id} AND T.candidat_id = {$candidat_id} "
            . "ORDER BY T.id DESC";
        return $con->fetchAll($sql);
    }
    
    public static function getHistoriqueById($id) {
        global $con;
        $sql = "SELECT * FROM tdchistoriques WHERE id = {$id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function addHistoriqueEntreprise($data) {
        global $con;
        $con->insert('tdchistoriques', $data);
        
        $id = $con->lastInsertId();
        
        $log  = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC Ajouter Commentaire Entreprise',
            'action_code' => 'mod_tdc_add_commentaire_entreprise',
            'table_name' => 'tdchistoriques',
            'table_id' => $id,
            'mission_id' => $data['mission_id'],
            'entreprise_id' => $data['entreprise_id'],
            'content_type' => 'json',
            'content_before' => json_encode($data),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function addHistorique($data) {
        global $con;
        $con->insert('tdchistoriques', $data);
        
        $id = $con->lastInsertId();
        
        $log  = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC Ajouter Historique',
            'action_code' => 'mod_tdc_add_historique',
            'table_name' => 'tdchistoriques',
            'table_id' => $id,
            'mission_id' => $data['mission_id'],
            'candidat_id' => $data['candidat_id'],
            'content_type' => 'json',
            'content_before' => json_encode($data),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function deleteHistoriqueEntreprise($id) {
        global $con;
        
        $content_before = self::getHistoriqueById($id);
        
        $con->delete('tdchistoriques', array('id' => $id));
        
        $log = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC Supprimer Historique',
            'action_code' => 'mod_tdc_delete_historique',
            'table_name' => 'tdchistoriques',
            'table_id' => $id,
            'mission_id' => $content_before['mission_id'],
            'candidat_id' => $content_before['candidat_id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function deleteHistorique($id) {
        global $con;
        
        $content_before = self::getHistoriqueById($id);
        
        $con->delete('tdchistoriques', array('id' => $id));
        
        $log = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC Supprimer Historique',
            'action_code' => 'mod_tdc_delete_historique',
            'table_name' => 'tdchistoriques',
            'table_id' => $id,
            'mission_id' => $content_before['mission_id'],
            'candidat_id' => $content_before['candidat_id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function updateHistoriqueEntreprise($data) {
        global $con;
        
        $content_before = self::getHistoriqueById($data['id']);
        
        $con->update('tdchistoriques', $data, array('id' => $data['id']));
                
        $log = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC Éditer Commentaire Entreprise',
            'action_code' => 'mod_tdc_edit_commentaire_entreprise',
            'table_name' => 'tdchistoriques',
            'table_id' => $data['id'],
            'mission_id' => $data['mission_id'],
            'entreprise_id' => $data['entreprise_id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($data),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function updateHistorique($data) {
        global $con;
        
        $content_before = self::getHistoriqueById($data['id']);
        
        $con->update('tdchistoriques', $data, array('id' => $data['id']));
                
        $log = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC Éditer Historique',
            'action_code' => 'mod_tdc_edit_historique',
            'table_name' => 'tdchistoriques',
            'table_id' => $data['id'],
            'mission_id' => $data['mission_id'],
            'candidat_id' => $data['candidat_id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($data),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function getWhoShortliste($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT U.id, U.firstName, U.lastName, K.shortliste_added FROM kpi_missions_shortlist K LEFT JOIN users U ON U.id = K.user_id WHERE K.mission_id = {$mission_id} AND K.candidat_id = {$candidat_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getSL($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT * FROM kpi_missions_shortlist WHERE mission_id = {$mission_id} AND candidat_id = {$candidat_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getSuivi($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT * FROM tdcsuivis WHERE mission_id = {$mission_id} AND candidat_id = {$candidat_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function updateSLUser($data) {
        global $con;
        
        // we must cater for the field value
        $shortliste_in_time = 0;
        $time = 0;
        $sl = self::getSL($data['mission_id'], $data['candidat_id']);
        if ($data['shortliste_added'] > $sl['mission_date_debut']) {
            $time = strtotime($data['shortliste_added']) - strtotime($sl['mission_date_debut']);
            $shortliste_in_time = 1;
        }
        
        $con->update('kpi_missions_shortlist', array('user_id' => $data['user_id'], 'shortliste_added' => $data['shortliste_added'], 'shortliste' => 1, 't_shortliste_from_start_mission' => $time, 'shortliste_in_time' => $shortliste_in_time), array('mission_id' => $data['mission_id'], 'candidat_id' => $data['candidat_id']));
        
        $con->update('tdcsuivis', array('shortliste_added' => $data['shortliste_added'], 'shortliste' => 1), array('mission_id' => $data['mission_id'], 'candidat_id' => $data['candidat_id']));
        
        return true;
    }
    
    public static function updateSuivi($data) {
        global $con;
        
        $shortliste_old = $data['shortliste_old'];
        $shortliste_old_addded = $data['shortliste_old_addded'];
        $tdc_id = $data['tdc_id'];
        unset($data['shortliste_old']);
        unset($data['shortliste_old_addded']);
        unset($data['tdc_id']);
            
        if ($data['date_prise_de_contact'] > 0) {
            $data['date_prise_de_contact_added'] = dateToDb();
        } else {
            $data['date_prise_de_contact_added'] = null;
        }
        
        if ($data['date_rencontre_consultant'] > 0) {
            $data['date_rencontre_consultant_added'] = dateToDb();
        } else {
            $data['date_rencontre_consultant_added'] = null;
        }
        
        if ($data['date_rencontre_client'] > 0) {
            $data['date_rencontre_client_added'] = dateToDb();
        } else {
            $data['date_rencontre_client_added'] = null;
        }
        
        if ($data['date_presentation'] > 0) {
            $data['date_presentation_added'] = dateToDb();
        } else {
            $data['date_presentation_added'] = null;
        }
        
        if ($data['date_retour_apres_rencontre'] > 0) {
            $data['date_retour_apres_rencontre_added'] = dateToDb();
        } else {
            $data['date_retour_apres_rencontre_added'] = null;
        }
        
        // for new KPI clients temps reactivite
        $sql = "SELECT * FROM kpi_clients WHERE tdc_suivi_id = {$data['id']}";
        $kpi_client = $con->fetchAssoc($sql);
        
        if (!empty($kpi_client)) {
            
            if ($data['date_prise_de_contact'] > 0) {
                $kpi_client['date_prise_de_contact_added'] = $data['date_prise_de_contact_added'];
                $kpi_client['date_prise_de_contact'] = dateToDb($data['date_prise_de_contact']);
            } else {
                $kpi_client['date_prise_de_contact_added'] = null;
                $kpi_client['date_prise_de_contact'] = null;
            }

            if ($data['date_rencontre_consultant'] > 0) {
                $kpi_client['date_rencontre_consultant_added'] = $data['date_rencontre_consultant_added'];
                $kpi_client['date_rencontre_consultant'] = dateToDb($data['date_rencontre_consultant']);
            } else {
                $kpi_client['date_rencontre_consultant_added'] = null;
                $kpi_client['date_rencontre_consultant'] = null;
            }

            if ($data['date_rencontre_client'] > 0) {
                $kpi_client['date_rencontre_client_added'] = $data['date_rencontre_client_added'];
                $kpi_client['date_rencontre_client'] = dateToDb($data['date_rencontre_client']);
            } else {
                $kpi_client['date_rencontre_client_added'] = null;
                $kpi_client['date_rencontre_client'] = null;
            }

            if ($data['date_presentation'] > 0) {
                $kpi_client['date_presentation_added'] = $data['date_presentation_added'];
                $kpi_client['date_presentation'] = dateToDb($data['date_presentation']);
            } else {
                $kpi_client['date_presentation_added'] = null;
                $kpi_client['date_presentation'] = null;
            }

            if ($data['date_retour_apres_rencontre'] > 0) {
                $kpi_client['date_retour_apres_rencontre_added'] = $data['date_retour_apres_rencontre_added'];
                $kpi_client['date_retour_apres_rencontre'] = dateToDb($data['date_retour_apres_rencontre']);
            } else {
                $kpi_client['date_retour_apres_rencontre_added'] = null;
                $kpi_client['date_retour_apres_rencontre'] = null;
            }
            
            // calculate time
            if ($data['date_retour_apres_rencontre'] > 0 && $data['date_presentation'] > 0) {
                $kpi_client['t_reactivite_client'] = $data['date_retour_apres_rencontre'] - $data['date_presentation'];
            }

            if ($data['date_rencontre_client'] > 0 && $data['date_presentation'] > 0) {
                $kpi_client['t_presentation_rencontre'] = $data['date_rencontre_client'] - $data['date_presentation'];
            }

            if ($data['date_rencontre_client'] > 0 && $data['date_rencontre_consultant'] > 0) {
                $kpi_client['t_rencontre_consultant_rencontre_client'] = $data['date_rencontre_client'] - $data['date_rencontre_consultant'];
            }
            
            $kpi_client['modified'] = dateToDb();
            $kpi_client['date_modified_status'] = 1;
            
            unset($kpi_client['created']);
                    
            $con->update('kpi_clients', $kpi_client, array('id' => $kpi_client['id']));
            
        } else {
            
            $mission = Mission::getMissionById($data['mission_id']);
                        
            $data_kpi_client = array(
                'tdc_suivi_id' => $data['id'],
                'client_id' => $mission['client_id'],
                'mission_id' => $data['mission_id'],
                'manager_id' => $mission['manager_id'],
                'prestataire_id' => $mission['prestataire_id'],
                'candidat_id' => $data['candidat_id'],
                'user_id' => $data['user_id'],
                'created' => dateToDb(),
            );
            
            if ($data['date_prise_de_contact'] > 0) {
                $data_kpi_client['date_prise_de_contact_added'] = $data['date_prise_de_contact_added'];
                $data_kpi_client['date_prise_de_contact'] = dateToDb($data['date_prise_de_contact']);
            }

            if ($data['date_rencontre_consultant'] > 0) {
                $data_kpi_client['date_rencontre_consultant_added'] = $data['date_rencontre_consultant_added'];
                $data_kpi_client['date_rencontre_consultant'] = dateToDb($data['date_rencontre_consultant']);
            }

            if ($data['date_rencontre_client'] > 0) {
                $data_kpi_client['date_rencontre_client_added'] = $data['date_rencontre_client_added'];
                $data_kpi_client['date_rencontre_client'] = dateToDb($data['date_rencontre_client']);
            }

            if ($data['date_presentation'] > 0) {
                $data_kpi_client['date_presentation_added'] = $data['date_presentation_added'];
                $data_kpi_client['date_presentation'] = dateToDb($data['date_presentation']);
            }

            if ($data['date_retour_apres_rencontre'] > 0) {
                $data_kpi_client['date_retour_apres_rencontre_added'] = $data['date_retour_apres_rencontre_added'];
                $data_kpi_client['date_retour_apres_rencontre'] = dateToDb($data['date_retour_apres_rencontre']);
            }
            
            // calculate time
            if ($data['date_retour_apres_rencontre'] > 0 && $data['date_presentation'] > 0) {
                $data_kpi_client['t_reactivite_client'] = $data['date_retour_apres_rencontre'] - $data['date_presentation'];
            }

            if ($data['date_rencontre_client'] > 0 && $data['date_presentation'] > 0) {
                $data_kpi_client['t_presentation_rencontre'] = $data['date_rencontre_client'] - $data['date_presentation'];
            }

            if ($data['date_rencontre_client'] > 0 && $data['date_rencontre_consultant'] > 0) {
                $data_kpi_client['t_rencontre_consultant_rencontre_client'] = $data['date_rencontre_client'] - $data['date_rencontre_consultant'];
            }
                        
            $con->insert('kpi_clients', $data_kpi_client);
            
        }
        // end for new KPI clients temps reactivite
        
        if (intval($data['shortliste']) == 1) {
            
            if (intval($data['shortliste']) == $shortliste_old) {
                $data['shortliste_added'] = $shortliste_old_addded;
            } else {
                $data['shortliste_added'] = dateToDb();
            }
                        
            // we must process shortliste kpi data for kpi manager
            // check if the record exist
            $sql = "SELECT * FROM kpi_missions_shortlist WHERE tdcsuivi_id = {$data['id']}";            
            $result = $con->fetchAssoc($sql);
            
            //debug($result, true);
            
            if (empty($result)) {
                // get mission details
                $sql = "SELECT * FROM missions WHERE id = {$data['mission_id']}";
                $mission = $con->fetchAssoc($sql);
                $user = $_SESSION['opsearch_user'];
                $kpi_data = array(
                    'tdcsuivi_id' => $data['id'],
                    'mission_id' => $data['mission_id'],
                    'candidat_id' => $data['candidat_id'],
                    'manager_id' => $mission['manager_id'],
                    'user_id' => $user['id'],
                    'shortliste' => 1,
                    'shortliste_added' => $data['shortliste_added'],
                );
                
                // check if shortliste date was in between date_debut and date fin
                $shortliste_added = strtotime($data['shortliste_added']);
                
                if ($shortliste_added >= $mission['date_debut'] && $shortliste_added <= $mission['date_fin']) {
                    $kpi_data['shortliste_in_time'] = 1;
                }

                $kpi_data['t_shortliste_from_start_mission'] = $shortliste_added - $mission['date_debut'];

                $kpi_data['mission_date_debut'] = dateToDb($mission['date_debut']);
                $kpi_data['mission_date_fin'] = dateToDb($mission['date_fin']);

                KPI::shortlister_in($kpi_data);
                
            } else { // we update the results
                                
                // check if shortliste date was in between date_debut and date fin
                $shortliste_added = strtotime($data['shortliste_added']);
                
                if ($shortliste_added >= strtotime($result['mission_date_debut']) && $shortliste_added <= strtotime($result['mission_date_fin'])) {
                    $result['shortliste_in_time'] = 1;
                }
                
                $result['t_shortliste_from_start_mission'] = $shortliste_added - strtotime($result['mission_date_debut']);
                $result['shortliste_added'] = $data['shortliste_added'];
                $result['shortliste'] = 1;

                KPI::updateShortliste_in($result);
                
            }
            
        } else {
            
            $data['shortliste_added'] = null;
            
            // we must update the kpi data
            // we must process shortliste kpi data for kpi manager
            // check if the record exist
            $sql = "SELECT * FROM kpi_missions_shortlist WHERE tdcsuivi_id = {$data['id']}";
            $result = $con->fetchAssoc($sql);
            
            if (empty($result)) {
                // get mission details
                $sql = "SELECT * FROM missions WHERE id = {$data['mission_id']}";
                $mission = $con->fetchAssoc($sql);
                $user = $_SESSION['opsearch_user'];
                $kpi_data = array(
                    'tdcsuivi_id' => $data['id'],
                    'mission_id' => $data['mission_id'],
                    'candidat_id' => $data['candidat_id'],
                    'manager_id' => $mission['manager_id'],
                    'user_id' => $user['id'],
                    'shortliste' => 0,
                    'mission_date_debut' => dateToDb($mission['date_debut']),
                    'mission_date_fin' => dateToDb($mission['date_fin']),
                );
                
                KPI::shortlister_in($kpi_data);
                
            } else { // we update the results
                
                $user = $_SESSION['opsearch_user'];
                $result['user_id'] = $user['id'];
                $result['shortliste'] = 0;
                $result['shortliste_in_time'] = 0;
                $result['t_shortliste_from_start_mission'] = 0;
                $result['shortliste_added'] = null;

                KPI::updateShortliste_in($result);
                
            }
            
        }
        
        if (isset($data['user_id_kpi_assigned'])) {
            $user_id_kpi_assigned = $data['user_id_kpi_assigned'];
            unset($data['user_id_kpi_assigned']);
            $con->update('tdc', array('user_id_kpi_assigned' => $user_id_kpi_assigned, 'modified' => time()), array('mission_id' => $data['mission_id'], 'candidat_id' => $data['candidat_id']));
        }
        
        if ($data['place'] > 0) {
            $data['place_added'] = dateToDb();
        } else {
            $data['place_added'] = null;
        }
        
        $content_before = self::getSuivi($data['mission_id'], $data['candidat_id']);
        
        // check if comment has been modified and if yes store the user id in field comment_user_id
        if ($content_before['comment'] <> $data['comment']) {
            $data['comment_user_id'] = $data['user_id'];
        }
        
        $con->update('tdcsuivis', $data, array('id' => $data['id']));
        
        $log  = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC MAJ Candidat Suivi',
            'action_code' => 'mod_tdc_update_candidat_suivi',
            'mission_id' => $data['mission_id'],
            'candidat_id' => $data['candidat_id'],
            'tdcsuivi_id' => $data['id'],
            'table_name' => 'tdcsuivis',
            'table_id' => $data['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($data),
        );        
        Watchdog::Log($log);
        
        // update tdc status ordering tdc.control_statut_tdc_order
        /**
         * ordering output
         * 1: IN (placé) (short listé)  => control_statut_tdc = 229 AND tdcsuivis.place = 1 AND tdcsuivis.shortliste = 1
         * 2: IN (placé) => control_statut_tdc = 229 AND tdcsuivis.place = 1
         * 3: IN (short listé) => control_statut_tdc = 229 AND tdcsuivis.shortliste = 1
         * 4: IN => control_statut_tdc = 229
         * 5: ARC => control_statut_tdc = 251
         * 6: ARCA => control_statut_tdc = 252
         * 7: BU+ => control_statut_tdc = 228
         * 8: BU – => control_statut_tdc = 227
         * 9: Ex BU => control_statut_tdc = 259
         * 10: OUT => control_statut_tdc = 226
         * 11: Sans statut => control_statut_tdc = 0
         * 
         */
        
        $tdc_order = self::getTdc($data['mission_id'], $data['candidat_id']);
        
        $order_value = 11;
        if ($tdc_order['control_statut_tdc'] == 229) {
            
            $suivi = self::getTdcSuivi($data['mission_id'], $data['candidat_id']);
            if ($suivi['place'] == 1 && $suivi['shortliste'] == 1) {
                $order_value = 1;
            } elseif ($suivi['place'] == 1 && $suivi['shortliste'] == 0) {
                $order_value = 2;
            } elseif ($suivi['place'] == 0 && $suivi['shortliste'] == 1) {
                $order_value = 3;
            } elseif ($suivi['place'] == 0 && $suivi['shortliste'] == 0) {
                $order_value = 4;
            }
        } elseif ($tdc_order['control_statut_tdc'] == 251) {
             $order_value = 5;
        } elseif ($tdc_order['control_statut_tdc'] == 252) {
             $order_value = 6;
        } elseif ($tdc_order['control_statut_tdc'] == 228) {
             $order_value = 7;
        } elseif ($tdc_order['control_statut_tdc'] == 227) {
             $order_value = 8;
        } elseif ($tdc_order['control_statut_tdc'] == 0) {
             $order_value = 11;
        } elseif ($tdc_order['control_statut_tdc'] == 226) {
             $order_value = 10;
        } elseif ($tdc_order['control_statut_tdc'] == 259) {
             $order_value = 9;
        }
                
        $con->update('tdc', array('control_statut_tdc_order' => $order_value), array('id' => $tdc_order['id'])); 
        
        // send client email alert about changes in candidat suivi
        $tdc = TDC::getTDCById($tdc_id);
        
        if ($tdc['visibilite_client'] == 1) {
                       
            // there may be many client assigned to the missions and we send to all of them
            $mission_clients = Mission::getAssignedClientToMissions($data['mission_id']);
            
            if (!empty($mission_clients)) {
                
                $mission = Mission::getMissionById($data['mission_id']);
        
                if ($mission['prestataire_id'] == 1) {
                    $logo_mail = "opsearch";
                } else {
                    $logo_mail = "headhunting";
                }
                
                $link_mission = BASE_URL . "/tdc-client?id=" . $data['mission_id'];
                
                $candidat = Candidat::getCandidatById($data['candidat_id']);
                
                $mail_body = "<strong>Suivi candidat modifié sur TDC " . $data['mission_id'] . "</strong><br/>";
                $mail_body .= "<strong>Référence du candidat : </strong> " . $candidat['id'] . "<br>";
                $mail_body .= "<strong>Candidat : </strong> " . mb_strtoupper($candidat['nom']) . ' ' . mb_ucfirst($candidat['prenom']) . "<br>";
                $mail_body .= "Cliquez sur le lien suivant : ";
                $mail_body .= "<a href='" . $link_mission . "'>TDC " . $data['mission_id'] . "</a><br/><br/>";
                $mail_body .= "Ou copier coller le lien suivant dans votre navigateur.<br/>";
                $mail_body .= $link_mission . "<br/><br/>";

                $mail_subject = "Suivi candidat modifié sur TDC " . $data['mission_id'];
                
                foreach($mission_clients as $mission_client) :
                    
                    if ($mission_client['mail_notif'] == 1) :
                    
                        $mail_body_into = "Bonjour " . $mission_client['lastName'] . " " . $mission_client['firstName'] . ",<br/><br/>";

                        $new_mail_body = $mail_body_into . $mail_body;

                        if (Mail::sendMail($mail_subject, $mission_client['email'], $new_mail_body, $logo_mail)) {
                            // log                             
                            $log  = array(
                                'module_name' => 'Mail',
                                'module_code' => 'mod_mail',
                                'action' => 'Envoie Mail Suivi Candidat TDC',
                                'action_code' => 'mod_mail_send_mail_suivi_candidat',
                                'table_name' => 'tdc',
                                'table_id' => $tdc_id,
                                'content_type' => 'json',
                                'content_before' => json_encode(array('to' => $mission_client['email'], 'to_user_id' => $mission_client['id'], 'subject' => $mail_subject, 'body' => $new_mail_body, 'mission_id' => $data['mission_id'], 'candidat_id' => $data['candidat_id'])),
                            );
                            Watchdog::log($log);
                        }
                        
                    endif;
                    
                endforeach;
                
            }
            
        }
        
        return true;
    }
    
    public static function updateSuiviEntreprise($data) {
        global $con;
        
        $content_before = self::getTDCById($data['tdc_id']);
        
        $con->update('tdc', array('suivis_entreprise' => json_encode($data)), array('id' => $data['tdc_id']));
        
        $log  = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'TDC MAJ Entreprise Suivi',
            'action_code' => 'mod_tdc_update_entreprise_suivi',
            'mission_id' => $data['mission_id'],
            'entreprise_id' => $data['entreprise_id'],
            'tdc_id' => $data['tdc_id'],
            'table_name' => 'tdc',
            'table_id' => $data['tdc_id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before['suivis_entreprise']),
            'content_after' => json_encode($data),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    /** STARTS OF STATS QUERY **/
    public static function getTotalEntrepriseInTDC($mission_id, $entreprise_ids = false) {
        global $con;
                
        if (!$entreprise_ids) {
            
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = 0 AND status = 1";
            
        } else if ($entreprise_ids && is_array($entreprise_ids) && !empty($entreprise_ids)) {
            
            $ids = addSeperator($entreprise_ids, $sep = ',');
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = 0 AND status = 1 AND entreprise_id IN ({$ids})";
            
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    public static function getTotalEntrepriseInTDCAAprocher($mission_id, $entreprise_ids = false) {
        global $con;
                
        if (!$entreprise_ids) {
            
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = 0 AND status = 1 AND (status_entreprise IS NULL OR status_entreprise = 0)";
            
        } else if ($entreprise_ids && is_array($entreprise_ids) && !empty($entreprise_ids)) {
            
            $ids = addSeperator($entreprise_ids, $sep = ',');
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = 0 AND status = 1 AND entreprise_id IN ({$ids}) AND (status_entreprise IS NULL OR status_entreprise = 0)";
            
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    public static function getTotalEntrepriseInTDCApprocher($mission_id, $entreprise_ids = false) {
        global $con;
                
        if (!$entreprise_ids) {
            
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = 0 AND status = 1 AND status_entreprise > 0";
            
        } else if ($entreprise_ids && is_array($entreprise_ids) && !empty($entreprise_ids)) {
            
            $ids = addSeperator($entreprise_ids, $sep = ',');
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = 0 AND status = 1 AND entreprise_id IN ({$ids}) AND status_entreprise > 0";
            
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    public static function getTotalEntrepriseInTDCOUT($mission_id, $entreprise_ids = false) {
        global $con;
                
        if (!$entreprise_ids) {
            
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = 0 AND status = 1 AND status_entreprise = 257";
            
        } else if ($entreprise_ids && is_array($entreprise_ids) && !empty($entreprise_ids)) {
            
            $ids = addSeperator($entreprise_ids, $sep = ',');
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = 0 AND status = 1 AND entreprise_id IN ({$ids}) AND status_entreprise = 257";
            
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    public static function getTotalEntrepriseInTDCFINI($mission_id, $entreprise_ids = false) {
        global $con;
                
        if (!$entreprise_ids) {
            
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = 0 AND status = 1 AND status_entreprise = 258";
            
        } else if ($entreprise_ids && is_array($entreprise_ids) && !empty($entreprise_ids)) {
            
            $ids = addSeperator($entreprise_ids, $sep = ',');
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = 0 AND status = 1 AND entreprise_id IN ({$ids}) AND status_entreprise = 258";
            
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    
    public static function getTotalCandidatInTDC($mission_id, $candidat_ids = false) {
        global $con;
                
        if (!$candidat_ids) {
            
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND entreprise_id = 0 AND status = 1";
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND entreprise_id = 0 AND status = 1 AND candidat_id IN ({$ids})";
            
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    public static function getTotalCandidatCCCinTDC($mission_id, $candidat_ids = false) {
        global $con;
                
        if (!$candidat_ids) {
            
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND entreprise_id = 0 AND status = 1 AND ccc = 1";
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND entreprise_id = 0 AND status = 1 AND ccc = 1 AND candidat_id IN ({$ids})";
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    public static function getTotalCandidatinTDCByStatusGrouped($mission_id, $status_ids, $candidat_ids = false) {       
        // in => 226, out => 227, bu+ => 228, bu- => 229
        global $con;
        
        $query_status = " AND control_statut_tdc IN (". addSeperator($status_ids, ',') .") " ;
                
        if (!$candidat_ids) {
            
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND entreprise_id = 0 AND status = 1 " . $query_status;
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
            $sql = "SELECT COUNT(id) as Total "
                . "FROM tdc "
                . "WHERE mission_id = {$mission_id} "
                . "AND status = 1 " . $query_status
                . "AND candidat_id IN ({$ids})";
        }
                
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    public static function getTotalCandidatinTDCByStatus($mission_id, $status_id, $candidat_ids = false) {       
        // in => 226, out => 227, bu+ => 228, bu- => 229
        global $con;
                
        if (!$candidat_ids) {
            
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND entreprise_id = 0 AND status = 1 AND control_statut_tdc = {$status_id}";
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
            $sql = "SELECT COUNT(id) as Total "
                . "FROM tdc "
                . "WHERE mission_id = {$mission_id} "
                . "AND status = 1 AND control_statut_tdc = {$status_id} "
                . "AND candidat_id IN ({$ids})";
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    public static function getTotalCandidatinTDCHavingStatus($mission_id, $candidat_ids = false) {       
        // in => 226, out => 227, bu+ => 228, bu- => 229
        global $con;
                
        if (!$candidat_ids) {
            
            $sql = "SELECT COUNT(id) as Total FROM tdc WHERE mission_id = {$mission_id} AND status = 1 AND control_statut_tdc > 0";
        
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
            $sql = "SELECT COUNT(id) as Total "
                . "FROM tdc "
                . "WHERE mission_id = {$mission_id} "
                . "AND status = 1 AND control_statut_tdc > 0 "
                . "AND candidat_id IN ({$ids})";
     
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
        
    }
    
    public static function getTotalSocieteinTDC_V2($mission_id, $candidat_ids = false, $entreprise_ids = false) {
        global $con; 
                
        if (!$candidat_ids) {
            
            $sql = "SELECT DISTINCT(c.company_id) as ent_ids "
            . "FROM tdc t, candidats c "
            . "WHERE t.mission_id = {$mission_id} AND t.status = 1 AND t.candidat_id = c.id AND c.societe_actuelle <> ''";
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
            $sql = "SELECT DISTINCT(c.company_id) as ent_ids "
            . "FROM tdc t, candidats c "
            . "WHERE t.mission_id = {$mission_id} AND t.status = 1 AND t.candidat_id = c.id AND c.societe_actuelle <> '' "
            . "AND t.candidat_id IN ({$ids})";
            
        }
        
        $results = $con->fetchAll($sql);
        $candidat_companies_ids = array();
        foreach($results as $id) :
            $candidat_companies_ids['key_'.$id['ent_ids']] = $id['ent_ids'];
        endforeach;
        
        if (!$entreprise_ids) {
            
            $sql = "SELECT DISTINCT(entreprise_id) as ent_ids FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = 0 AND status = 1";
            
        } else if ($entreprise_ids && is_array($entreprise_ids) && !empty($entreprise_ids)) {
            
            $ids = addSeperator($entreprise_ids, $sep = ',');
            $sql = "SELECT DISTINCT(entreprise_id) as ent_ids FROM tdc WHERE mission_id = {$mission_id} AND candidat_id = 0 AND status = 1 AND entreprise_id IN ({$ids})";
            
        }
        
        $results = $con->fetchAll($sql);
        $entreprise_companies_ids = array();
        foreach($results as $id) :
            $entreprise_companies_ids['key_'.$id['ent_ids']] = $id['ent_ids'];
        endforeach;
        
        // note that we have used string keys as it will overwrite when merging and hence elimiate the duplicate ids
        $entreprise_total_arr = array_merge($candidat_companies_ids, $entreprise_companies_ids);
        return count($entreprise_total_arr);
        
    }
    
    public static function getTotalSocieteinTDC($mission_id, $candidat_ids = false) {
        global $con; 
        
        if (!$candidat_ids) {
            
            $sql = "SELECT COUNT(DISTINCT(c.societe_actuelle)) as Total "
            . "FROM tdc t, candidats c "
            . "WHERE t.mission_id = {$mission_id} AND t.status = 1 AND t.candidat_id = c.id AND c.societe_actuelle <> ''";
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
            $sql = "SELECT COUNT(DISTINCT(c.societe_actuelle)) as Total "
            . "FROM tdc t, candidats c "
            . "WHERE t.mission_id = {$mission_id} AND t.status = 1 AND t.candidat_id = c.id AND c.societe_actuelle <> '' "
            . "AND t.candidat_id IN ({$ids})";
            
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    public static function getTotalCandidatApprocheinTDC($mission_id, $candidat_ids = false) {
        global $con; 
        
        if (!$candidat_ids) {
            
            $sql = "SELECT COUNT(candidat_id) as Total "
            . "FROM tdcsuivis "
            . "WHERE date_prise_de_contact > 0 AND mission_id = {$mission_id} "
            . "AND candidat_id IN "
                . "(SELECT candidat_id FROM tdc WHERE mission_id = {$mission_id} AND status = 1)";
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
            $sql = "SELECT COUNT(candidat_id) as Total "
            . "FROM tdcsuivis "
            . "WHERE date_prise_de_contact > 0 AND mission_id = {$mission_id} "
            . "AND candidat_id IN ({$ids})";
            
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    public static function getTotalCandidatRencontrerinTDC($mission_id, $candidat_ids = false) {
        global $con; 
        
        if (!$candidat_ids) {
            
            $sql = "SELECT COUNT(candidat_id) as Total "
            . "FROM tdcsuivis "
            . "WHERE date_rencontre_consultant > 0 AND mission_id = {$mission_id} "
            . "AND candidat_id IN "
                . "(SELECT candidat_id FROM tdc WHERE mission_id = {$mission_id} AND status = 1)";
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
            $sql = "SELECT COUNT(candidat_id) as Total "
            . "FROM tdcsuivis "
            . "WHERE date_rencontre_consultant > 0 AND mission_id = {$mission_id} "
            . "AND candidat_id IN ({$ids})";
            
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    public static function getTotalCandidatRestantApprocheinTDC($mission_id, $candidat_ids = false) {
                
        if (!$candidat_ids) {
            
            //return self::getTotalCandidatInTDC($mission_id) - self::getTotalCandidatApprocheinTDC($mission_id);
            return self::getTotalCandidatInTDC($mission_id) - self::getTotalCandidatinTDCHavingStatus($mission_id);
            
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            //return self::getTotalCandidatInTDC($mission_id, $candidat_ids) - self::getTotalCandidatApprocheinTDC($mission_id, $candidat_ids);
            return self::getTotalCandidatInTDC($mission_id, $candidat_ids) - self::getTotalCandidatinTDCHavingStatus($mission_id, $candidat_ids);
            
        } 
        
    }
    
    public static function getTotalByOutCategoryinTDC($mission_id, $outcategory_id, $candidat_ids = false) {
        global $con;
                
        if (!$candidat_ids) {
            
            $sql = "SELECT COUNT(candidat_id) as Total "
            . "FROM tdcoutcategories "
            . "WHERE mission_id = {$mission_id} AND outcategory_id = {$outcategory_id} "
            . "AND candidat_id IN "
                . "(SELECT candidat_id FROM tdc WHERE mission_id = {$mission_id} AND status = 1)";
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
            $sql = "SELECT COUNT(candidat_id) as Total "
            . "FROM tdcoutcategories "
            . "WHERE mission_id = {$mission_id} AND outcategory_id = {$outcategory_id} "
            . "AND candidat_id IN ({$ids})";
            
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
    }
    
    public static function filterStatsEntrepriseTDC($data) {
        global $con;
        
        if (!empty($data)) {
            
            $mission_id = $data['mission_id'];

            if ($data['start_date'] == "") {
                $start_date = "";
            } else {
                $start_date = strtotime($data['start_date'] . ' 00:00:00');
            }

            if ($data['end_date'] == "") {
                $end_date = "";
            } else {
                $end_date = strtotime($data['end_date'] . ' 23:59:59');
            }
            
            if ($data['type'] == "") {
                $type = "null";
            } else {
                $type = $data['type'];
            }

            // for legende colors
            $tdc_legende_entreprise_color_ids = $data['tdc_legende_entreprise_color_ids']; // array of values seperated by ;

            if ($tdc_legende_entreprise_color_ids <> "") {

                $colors_ids = explode(';', $tdc_legende_entreprise_color_ids);

                $colors_ids_in = addSeperator($colors_ids, $sep = ',');
                $sql_colors = " AND (color_societe_label IN ({$colors_ids_in}) OR color_societe_bg IN ({$colors_ids_in})) ";

            } else {

                $sql_colors = " ";

            }
            
            if ($type == 'date_prise_contact') {

                if ($start_date <> "") {
                    $sql_start_date = " AND created >= {$start_date} ";
                } else {
                    $sql_start_date = " ";
                }

                if ($end_date <> "") {
                    $sql_end_date = " AND created <= {$end_date} ";
                } else {
                    $sql_end_date = " ";
                }

                
            } elseif ($type == 'date_ajoute') {

                if ($start_date <> "") {
                    $sql_start_date = " AND created >= {$start_date} ";
                } else {
                    $sql_start_date = " ";
                }

                if ($end_date <> "") {
                    $sql_end_date = " AND created <= {$end_date} ";
                } else {
                    $sql_end_date = " ";
                }

            } else if ($type == "null") {
                $sql_end_date = " ";
                $sql_start_date = " ";  
            }
            
            $sql = "SELECT entreprise_id FROM tdc WHERE mission_id = {$mission_id} AND status = 1 " . $sql_start_date . $sql_end_date . $sql_colors;
            
            $results = $con->fetchAll($sql);

            if (empty($results)) {
                return $results;
            } else {
                $entreprise_ids = array();
                foreach($results as $result) :
                    $entreprise_ids[] = $result['entreprise_id'];
                endforeach;

                return $entreprise_ids;
            }
            
            // end check empty $data
            
        } else { 
        
            return array();
            
        }        
    
    }
    
    public static function filterStatsTDC($data) {
        global $con;
        
        if (!empty($data)) {
            
            $mission_id = $data['mission_id'];
            
            
            if ($data['start_date'] == "") {
                $start_date = "";
            } else {
                $start_date = strtotime($data['start_date'] . ' 00:00:00');
            }

            if ($data['end_date'] == "") {
                $end_date = "";
            } else {
                $end_date = strtotime($data['end_date'] . ' 23:59:59');
            }
            
            if ($data['type'] == "") {
                $type = "null";
            } else {
                $type = $data['type'];
            }

            // for legende colors
            $tdc_legende_color_ids = $data['tdc_legende_color_ids']; // array of values seperated by ;

            if ($tdc_legende_color_ids <> "") {

                $colors_ids = explode(';', $tdc_legende_color_ids);

                $colors_ids_in = addSeperator($colors_ids, $sep = ',');
                $sql_colors = " AND (color_societe_label IN ({$colors_ids_in}) OR color_societe_bg IN ({$colors_ids_in}) OR color_candidat_label IN ({$colors_ids_in}) OR color_candidat_bg IN ({$colors_ids_in})) ";

            } else {

                $sql_colors = " ";

            }
            
            // for filtrer by out category_id
            $out_category_id = $data['tdc_legende_out_categorie_id'];
            
            if ($type == 'date_prise_contact') {

                if ($start_date <> "") {
                    $sql_start_date = " AND date_prise_de_contact >= {$start_date} ";
                } else {
                    $sql_start_date = " ";
                }

                if ($end_date <> "") {
                    $sql_end_date = " AND date_prise_de_contact <= {$end_date} ";
                } else {
                    $sql_end_date = " ";
                }
                
                if ($out_category_id <> "") {
                    $sql_out_category = " AND candidat_id IN (SELECT candidat_id FROM tdcoutcategories WHERE mission_id = {$mission_id} AND outcategory_id = {$out_category_id}) ";
                } else {
                    $sql_out_category = " ";
                }

                $sql = "SELECT candidat_id "
                . "FROM tdcsuivis "
                . "WHERE mission_id = {$mission_id} " . $sql_start_date . $sql_end_date
                . "AND candidat_id IN (SELECT candidat_id FROM tdc WHERE mission_id = {$mission_id} " . $sql_colors . " AND status = 1) " . $sql_out_category;
                
                // echo $sql;
                
            } elseif ($type == 'date_ajoute') {

                if ($start_date <> "") {
                    $sql_start_date = " AND created >= {$start_date} ";
                } else {
                    $sql_start_date = " ";
                }

                if ($end_date <> "") {
                    $sql_end_date = " AND created <= {$end_date} ";
                } else {
                    $sql_end_date = " ";
                }
                
                if ($out_category_id <> "") {
                    $sql_out_category = " AND candidat_id IN (SELECT candidat_id FROM tdcoutcategories WHERE mission_id = {$mission_id} AND outcategory_id = {$out_category_id}) ";
                } else {
                    $sql_out_category = " ";
                }
                
                $sql = "SELECT candidat_id "
                . "FROM tdc "
                . "WHERE (mission_id = {$mission_id} " . $sql_start_date . $sql_end_date . $sql_colors . " AND status = 1) " . $sql_out_category;
                
            } elseif ($type == "null") { // ignore date here but cater for colors
                
                if ($out_category_id <> "") {
                    $sql_out_category = " AND candidat_id IN (SELECT candidat_id FROM tdcoutcategories WHERE mission_id = {$mission_id} AND outcategory_id = {$out_category_id}) ";
                } else {
                    $sql_out_category = " ";
                }
                
                $sql = "SELECT candidat_id "
                . "FROM tdc "
                . "WHERE (mission_id = {$mission_id} " . $sql_colors . " AND status = 1) " . $sql_out_category;
                
            }
            
            $results = $con->fetchAll($sql);

            if (empty($results)) {
                return $results;
            } else {
                $candidat_ids = array();
                foreach($results as $result) :
                    $candidat_ids[] = $result['candidat_id'];
                endforeach;

                return $candidat_ids;
            }
            
        } else {
            
            return array();
            
        }
        
    }
    
    public static function getTotalCandidatPlaceInTDC($mission_id) {
        global $con;
                
        $sql = "SELECT COUNT(candidat_id) as Total "
            . "FROM tdcsuivis "
            . "WHERE mission_id = {$mission_id} AND place = 1 "
            . "AND candidat_id IN (SELECT candidat_id FROM tdc WHERE mission_id = {$mission_id} AND status = 1)";   
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
        
    }
    
    public static function getTotalCandidatShortlisteInTDC($mission_id) {
        global $con;
                
        $sql = "SELECT COUNT(candidat_id) as Total "
            . "FROM tdcsuivis "
            . "WHERE mission_id = {$mission_id} AND shortliste = 1 "
            . "AND candidat_id IN (SELECT candidat_id FROM tdc WHERE mission_id = {$mission_id} AND status = 1)";
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
        
    } 
    
    public static function getAverageAgeInTDC($mission_id, $candidat_ids = false) {
        global $con;
        
        if (!$candidat_ids) {
            
            $sql = "SELECT ROUND((SUM(((UNIX_TIMESTAMP() - c.dateOfBirth) / 31556926))) / count(*), 2) as average_age, count(*) as TotalWithAge "
                . "FROM candidats c "
                . "LEFT JOIN tdc t ON t.candidat_id = c.id "
                . "WHERE t.mission_id = {$mission_id} AND c.dateOfBirth > 0";
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
            
            $sql = "SELECT ROUND((SUM(((UNIX_TIMESTAMP() - c.dateOfBirth) / 31556926))) / count(*), 2) as average_age, count(*) as TotalWithAge "
                . "FROM candidats c "
                . "LEFT JOIN tdc t ON t.candidat_id = c.id "
                . "WHERE t.mission_id = {$mission_id} AND c.dateOfBirth > 0 AND c.id IN ({$ids})";
            
        }
        
        $result = $con->fetchAssoc($sql);
        
        return $result['average_age'];
    }
    
    public static function getAverageRemuneration ($mission_id, $candidat_ids = false) {
        global $con;
        
        if (!$candidat_ids) {
            
            $sql = "SELECT ROUND(SUM(c.remuneration)/count(*), 2) as average_remuneration, count(*) as total "
                . "FROM candidats c "
                . "LEFT JOIN tdc t ON t.candidat_id = c.id "
                . "WHERE t.mission_id = {$mission_id} AND c.remuneration > 0";
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
                            
            $sql = "SELECT ROUND(SUM(c.remuneration)/count(*), 2) as average_remuneration, count(*) as total "
                . "FROM candidats c "
                . "LEFT JOIN tdc t ON t.candidat_id = c.id "
                . "WHERE t.mission_id = {$mission_id} AND c.remuneration > 0 AND c.id IN ({$ids})";
            
        }
        
        $result = $con->fetchAssoc($sql);
        
        return $result['average_remuneration'];
        
    } 
    
    public static function getAverageSalairefixe ($mission_id, $candidat_ids = false) {
        global $con;
        
        if (!$candidat_ids) {
            
            $sql = "SELECT ROUND(SUM(c.salaire_fixe)/count(*), 2) as average_remuneration, count(*) as total "
                . "FROM candidats c "
                . "LEFT JOIN tdc t ON t.candidat_id = c.id "
                . "WHERE t.mission_id = {$mission_id} AND c.salaire_fixe > 0";
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
                            
            $sql = "SELECT ROUND(SUM(c.salaire_fixe)/count(*), 2) as average_remuneration, count(*) as total "
                . "FROM candidats c "
                . "LEFT JOIN tdc t ON t.candidat_id = c.id "
                . "WHERE t.mission_id = {$mission_id} AND c.salaire_fixe > 0 AND c.id IN ({$ids})";
            
        }
        
        $result = $con->fetchAssoc($sql);
        
        return $result['average_remuneration'];
        
    } 
    
    public static function getLocalisationGrouped($mission_id, $candidat_ids = false) {
        global $con;
        
        if (!$candidat_ids) {
            
            $sql = "SELECT CO.name, count(*) as Total "
                . "FROM candidats c "
                . "LEFT JOIN tdc t ON t.candidat_id = c.id "
                . "LEFT JOIN controles CO ON CO.id = c.control_localisation_id "
                . "WHERE t.mission_id = {$mission_id} AND CO.type_id = 6 "
                . "GROUP BY CO.name";
            
        } else if ($candidat_ids && is_array($candidat_ids) && !empty($candidat_ids)) {
            
            $ids = addSeperator($candidat_ids, $sep = ',');
                                            
            $sql = "SELECT CO.name, count(*) as Total "
                . "FROM candidats c "
                . "LEFT JOIN tdc t ON t.candidat_id = c.id "
                . "LEFT JOIN controles CO ON CO.id = c.control_localisation_id "
                . "WHERE t.mission_id = {$mission_id} AND CO.type_id = 6 AND c.id IN ({$ids}) "
                . "GROUP BY CO.name";
            
        }
        
        
        
        
        return $con->fetchAll($sql);
        
    }
    
    /** END OF STATS QUERY **/
    
    public static function getMissionByCandidatUser($candidat_id, $user_id) {
        global $con;
        $sql = "SELECT * FROM tdc WHERE candidat_id = {$candidat_id} AND user_id = {$user_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getCommentClient($mission_id, $candidat_id) {
        global $con;
        
        $sql = "SELECT C.*, U.firstName, U.lastName FROM tdcclientcomments C LEFT JOIN users U ON U.id = C.user_id WHERE C.status = 1 AND C.mission_id = {$mission_id} AND C.candidat_id = {$candidat_id} ORDER BY C.id DESC";
        
        return $con->fetchAll($sql);
    }
    
    public static function addCommentTDCClient($data) {
        global $con;
        
        $con->insert('tdcclientcomments', $data);
        
        $comment_id = $con->lastInsertId();
            
        //watchdog
        $log  = array(
            'module_name' => 'TDC',
            'module_code' => 'mod_tdc',
            'action' => 'Ajouter TDC Client Commentaire',
            'action_code' => 'mod_tdc_create_client_comment',
            'table_name' => 'tdcclientcomments',
            'table_id' => $comment_id,
            'content_type' => 'json',
            'content_before' => json_encode($data),
        );        
        Watchdog::Log($log);

        // check if user is manager or client
        $user = User::getUserById($data['user_id']);
        $type_user = "manager";
        if ($user['role_id'] == 13) { // client
            $type_user = "client";
        }
        
        $mission = Mission::getMissionById($data['mission_id']);
        
        if ($mission['prestataire_id'] == 1) {
            $logo_mail = "opsearch";
        } else {
            $logo_mail = "headhunting";
        }
        
        $link_mission = BASE_URL . "/tdc-client?id=" . $data['mission_id'];
        
        // if client send mail to mission manager
        if ($type_user === "client") {
            
            $mission_manager = Mission::getManagerByMissionId($data['mission_id']);
            
            if ($mission_manager['mission_mail_notif'] == 1) :
                
                $mail_body = "Bonjour " . $mission_manager['lastName'] . " " . $mission_manager['firstName'] . ",<br/><br/>";
                $mail_body .= "Un nouveau commentaire Client sur TDC " . $data['mission_id'] . "<br/>";
                $mail_body .= "Cliquez sur le lien suivant :<br/>";
                $mail_body .= "<a href='" . $link_mission . "'>TDC CLIENT " . $data['mission_id'] . "</a><br/><br/>";
                $mail_body .= "Ou copier coller le lien suivant dans votre navigateur.<br/>";
                $mail_body .= $link_mission . "<br/><br/>";
                $mail_body .= "<strong>Commentaire: </strong><br>";
                $mail_body .= nl2br($data['comment']);

                $mail_subject = "Un nouveau commentaire Client sur TDC " . $data['mission_id'];

                if (Mail::sendMail($mail_subject, $mission_manager['email'], $mail_body, $logo_mail)) {
                    // log 
                    $log  = array(
                        'module_name' => 'Mail',
                        'module_code' => 'mod_mail',
                        'action' => 'Envoie Mail Commentaire Manager TDC',
                        'action_code' => 'mod_mail_send_mail_comment_manager_tdc',
                        'table_parent_name' => 'tdcclientcomments',
                        'table_parent_id' => $comment_id,
                        'content_type' => 'json',
                        'content_before' => json_encode(array('to' => $mission_manager['email'], 'to_user_id' => $mission_manager['id'], 'subject' => $mail_subject, 'body' => $mail_body, 'mission_id' => $data['mission_id'], 'candidat_id' => $data['candidat_id'])),
                    );
                    Watchdog::log($log);
                }
                
            endif;
            
        // if manager send email to client
        } elseif ($type_user === "manager") {
            
            // there may be many client assigned to the missions and we send to all of them
            $mission_clients = Mission::getAssignedClientToMissions($data['mission_id']);
            
            if (!empty($mission_clients)) {
                
                $mail_body = "Un nouveau commentaire sur TDC " . $data['mission_id'] . "<br/>";
                $mail_body .= "Cliquez sur le lien suivant :<br/>";
                $mail_body .= "<a href='" . $link_mission . "'>TDC " . $data['mission_id'] . "</a><br/><br/>";
                $mail_body .= "Ou copier coller le lien suivant dans votre navigateur.<br/>";
                $mail_body .= $link_mission . "<br/><br/>";
                $mail_body .= "<strong>Commentaire: </strong><br>";
                $mail_body .= nl2br($data['comment']);

                $mail_subject = "Un nouveau commentaire sur TDC " . $data['mission_id'];
                
                foreach($mission_clients as $mission_client) :
                    
                    if ($mission_client['mail_notif'] == 1) :
                    
                        $mail_body_into = "Bonjour " . $mission_client['lastName'] . " " . $mission_client['firstName'] . ",<br/><br/>";

                        $new_mail_body = $mail_body_into . $mail_body;

                        if (Mail::sendMail($mail_subject, $mission_client['email'], $new_mail_body, $logo_mail)) {
                            // log 
                            $log  = array(
                                'module_name' => 'Mail',
                                'module_code' => 'mod_mail',
                                'action' => 'Envoie Mail Commentaire Client TDC',
                                'action_code' => 'mod_mail_send_mail_comment_client_tdc',
                                'table_parent_name' => 'tdcclientcomments',
                                'table_parent_id' => $comment_id,
                                'content_type' => 'json',
                                'content_before' => json_encode(array('to' => $mission_client['email'], 'to_user_id' => $mission_client['id'], 'subject' => $mail_subject, 'body' => $new_mail_body, 'mission_id' => $data['mission_id'], 'candidat_id' => $data['candidat_id'])),
                            );
                            Watchdog::log($log);
                        }
                        
                    endif;
                    
                endforeach;
                
            }
            
        }
            
        
        return true;
    }
    
}
