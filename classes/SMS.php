<?php

/**
 * <p>note that the api works only from https</p>
 * 
 * Description of SMS
 * <p>Sending SMS to user mobile number from TDC</p>
 * 
 * PHP SDK https://github.com/Nexmo/nexmo-php
 * composer require nexmo/client
 * <p>Will not use the client as its too huge and there are bugs</p>
 * 
 * SMS API
    https://www.nexmo.com
    Login: ravish@opsearch.com
    Pass: PtrhNp!@3e3V79w   /    PtrhNp!@3e3V79w 
    https://developer.nexmo.com/api/sms  
 * 
 * @author Faardeen Madarbokas
 * 
 * success json response of API
 * {
        "message-count": "1",
        "messages": [
            {
                "to": "23052585840",
                "message-id": "13000000A3DECCE9",
                "status": "0",
                "remaining-balance": "1.96890000",  => the account remaining balance in Euro
                "message-price": "0.03110000",
                "network": "61701"
            }
        ]
    }
 * 
 * bad request
 * 
 * {
        "message-count": "1",
        "messages": [
            {
                "to": "23057917865",
                "status": "29",
                "error-text": "Non White-listed Destination - rejected"
            }
        ]
    }
 * 
 * 
 * French phone number rules
 * 1. The number must be 10 digits
 * 2. must start with 0
 * 3. if the number start with 06 => it is a mobile number and we can send an sms
 * 4. if the number start with either 01, 02, 03, 04, 05 => it is a fixed phone but still we can send an sms
 * 5. Very rare cases a mobile number can also start by 07
 * 
 * The content of the message must be GSM Standard
 * SMS Max Characters = 160
 * But can split to several sms by max 1600 characters
 * https://www.twilio.com/docs/glossary/what-sms-character-limit
 * When you send a SMS message over 160 characters the message will be split. Large messages are segmented into 153 character segments and sent individually then rebuilt by the recipients device. For example, a 161 character message will be sent as two messages, one with 153 characters and the second with 8 characters.
 * 
 * FRANCE prefix system : https://help.nexmo.com/hc/en-us/articles/225904987
 */
class SMS {
    
    public static function validateTel($str) {
        
        $without_space = formatTelFR($str);
        
        $with_10_characters = substr($without_space, 0, 10);
        
        if (ctype_digit($with_10_characters) !== TRUE) {
            
            return "Le numero ne doit pas contenir des alphabets";
            
        } else {
                        
            // check if start with 01, 02, 03, 04, 05, 06, 07
            $valid_start = array('01', '02', '03', '04', '05', '06', '07');
            $first_2_characters = substr($with_10_characters, 0, 2);
            
            if (in_array($first_2_characters, $valid_start)) {
                return true;
            } else {
                return "Le numero doit commencer par 01, 02, 03, 04, 05, 06, ou 07";
            }
            
        }
        
    }
    
    public static function processTel($str) {
        
        // good format of france number 
        // +33 6 84 98 36 56 => 06 84 98 36 56
        // we must use 33684983656
        
        if (isDev()) {
            return $str;
        }
        
        $without_space = formatTelFR($str);
                
        return "33" . substr($without_space, 1, 10);
        
    }
    
    public static function process($data) {
        global $con;
               
        $data['to_processed'] = self::processTel($data['to_raw']);         
               
        $con->insert('sms', $data);
        $sms_id = $con->lastInsertId();
        
        // now we pushed to the API to send the sms and update the record with the return values
        $api_response = self::send($data['message_text'], $data['to_processed']);
                
        if (is_array($api_response)) { // means we got a response
            
            // we update the table
            // if good request and message sent success => status = 0
            // else status is not zero
            if (intval($api_response['messages'][0]['status']) == 0) {
                
                $sms = array(
                    'message_count' => $api_response['message-count'],
                    'message_id' => $api_response['messages'][0]['message-id'],
                    'status' => $api_response['messages'][0]['status'],
                    'remaining_balance' => $api_response['messages'][0]['remaining-balance'],
                    'message_price' => $api_response['messages'][0]['message-price'],
                    'network' => $api_response['messages'][0]['network'],
                    'modified' => dateToDb(),
                );
                
                $con->update('sms', $sms, array('id' => $sms_id));
                
                $content_before = self::getSMSById($sms_id);
            
                //watchdog
                $log  = array(
                    'module_name' => 'Candidats',
                    'module_code' => 'mod_tdc',
                    'action' => 'TDC Envoie SMS',
                    'action_code' => 'mod_tdc_send_sms',
                    'table_name' => 'sms',
                    'table_id' => $sms_id,
                    'mission_id' => $data['mission_id'],
                    'candidat_id' => $data['candidat_id'],
                    'content_type' => 'json',
                    'content_before' => json_encode($content_before),
                );        
                Watchdog::Log($log);                
                
                return true;
                
            } else {
                
                $sms = array(
                    'message_count' => $api_response['message-count'],
                    'status' => $api_response['messages'][0]['status'],
                    'error_text' => $api_response['messages'][0]['error-text'],
                    'modified' => dateToDb(),
                );
                
                if (isset($api_response['messages'][0]['network'])) {
                    $sms['network'] = $api_response['messages'][0]['network'];
                }
                
                $con->update('sms', $sms, array('id' => $sms_id));
                
                $content_before = self::getSMSById($sms_id);
            
                //watchdog
                $log  = array(
                    'module_name' => 'Candidats',
                    'module_code' => 'mod_tdc',
                    'action' => 'TDC Envoie SMS',
                    'action_code' => 'mod_tdc_send_sms',
                    'table_name' => 'sms',
                    'table_id' => $sms_id,
                    'mission_id' => $data['mission_id'],
                    'candidat_id' => $data['candidat_id'],
                    'content_type' => 'json',
                    'content_before' => json_encode($content_before),
                );        
                Watchdog::Log($log);
                
                return $sms['error_text'];
                
            }
            
        } else { // there is a problem connecting the API
            
            return $api_response;
            
        }
    }
    
    public static function getSMSById($sms_id) {
        global $con;
        $sql = "SELECT * FROM sms WHERE id = {$sms_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function send($text, $to) {
        
        // 1 connect to API
        // 2 get response
        // 2.1 if good response we store a log in db with the message-id return by API
        // 2.2 we check the remaining balance and if it is below 50 Euro we send a notification to 
        // faardeen.madarbokas@gmail.com, ravish@opsearch.com and bastien@opsearch.com to notify
        // 3.1 if bad response we check the error message and log it
        
        $response = self::connectAPI($text, $to);
        
        if ($response === FALSE) { // error connecting to API
            
            // send email to faardeen.madarbokas@gmail.com and ravish@opsearch.com and bastien@opsearch.com
            
            if (isDev()) {
                $email = 'faardeen.madarbokas@gmail.com';
            } else {
                $email = array('faardeen.madarbokas@gmail.com', 'ravish@opsearch.com', 'bastien@opsearch.com');
            }
            
            // send mail
            $mail_body = "Il y a un problème avec l'API SMS NEXMO.";

            Mail::sendMail('API SMS NEXMO NOT WORKING', $email, $mail_body);
            
            return "Un problème est survenu lors de l'envoi du message. Veuillez contacter l'administrateur.";
            
        } else { // API good but can be true and false
            
            // response will be of type json
            $sms_data = json_decode($response, true);
            
            return $sms_data;
            
            
        }
        
    }
    
    public static function connectAPI($text, $to, $from = "GROUPE%20OPS") {
        
        $curl = curl_init();
        
        if (isDev()) {
            $to = "23052585840"; // added to whitelist on dashboard to to testing as sender
        }
                
        $url = "https://rest.nexmo.com/sms/json?api_key=e2e2a1f4&api_secret=LTI1vOzY9vvxWOny&to=".$to."&from=".$from."&text=".urlencode($text);
                
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
        ));

        $response = curl_exec($curl);        
        $err = curl_error($curl);   
                
        curl_close($curl);

        if ($err) {
            // echo "cURL Error #:" . $err;
            return false;
        } 

        return $response;
        
    }
    
    public static function getSMS($tdc_id, $candidat_id) {
        global $con;
        
        $sql = "SELECT S.*, U.firstName, U.lastName FROM sms S LEFT JOIN users U ON U.id = S.user_id WHERE S.mission_id = {$tdc_id} AND S.candidat_id = {$candidat_id} ORDER BY id DESC";
        
        return $con->fetchAll($sql);
        
    }
    
}
