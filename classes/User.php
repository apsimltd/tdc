<?php

/**
 * User Class
 *
 * @author faardeen
 */
class User extends Action {
    
    /**
     * 
     * @global Res $con
     * @param String $email
     * @return String
     */
    public static function getUsernameByMail($email) {
        $result = self::getUserByMail($email);
        return $result['username'];
    }
    
    /**
     * 
     * @global Res $con
     * @param String $email
     * @param String $pass
     * @todo encrypt password
     */
    public static function createNewPass($email, $pass) {
        global $con;
        
        $encrypted_pass = password_hash($pass, PASSWORD_BCRYPT);

        $sql = "UPDATE users SET password = '{$encrypted_pass}' WHERE email = '{$email}'";

        if ($con->executeQuery($sql)) {
            return true;
        }
    }
    
    /**
     * 
     * @param String $token
     * @return array
     */
    public static function TokenVerify($token) {

        $token_row = self::TokenSearch($token);

        // if token does not exist return false
        if (empty($token_row) || !self::TokenVerifyExpiry($token_row['created'])) {
            return false;
        } else {
            $token_content = json_decode($token_row['content_before'], true);
            return $token_content['email'];
        }
    }
    
    /**
     * 
     * @param int $created
     * @param String $token
     * @return boolean
     */
    public static function TokenVerifyExpiry($created) {
        $hours_ellapsed = (time() - strtotime($created)) / 3600;
        if ($hours_ellapsed > 24) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * 
     * @global Res $con
     * @param String $token
     * @return array
     */
    public static function TokenSearch($token) {

        global $con;

        $sql = "SELECT * FROM watchdog WHERE module_code = 'mod_user' AND "
                . "action_code = 'mod_user_reset_pass_request' AND content_before LIKE '%{$token}%' AND table_parent_name = 'users'";

        $results = $con->fetchAll($sql);

        // last element of the results in watchdog, in case the token is edited in url
        return end($results);
    }
    
    public static function isUserActiveMailExist($email, $user_id = NULL) {
        global $con;
        
        if ($user_id) {
            $sql = "SELECT email FROM users WHERE email = '{$email}' AND id <> {$user_id} AND status = 1";
        } else {
            $sql = "SELECT email FROM users WHERE email = '{$email}' AND status = 1";
        }
        
        $results = $con->fetchAll($sql);

        return (!empty($results));
    }
    
    /**
     * 
     * @global Res $con
     * @param String  $email
     * @return boolean
     */
    public static function resetPass($email) {

        if (self::isUserActiveMailExist($email)) {
            
            $token = md5(time()) . md5($email);
            
            // we do not set the status = 0 as if the user do not used the link to reset 
            // and later remember his password he will not be able to log in
//            $sql = "UPDATE c_users SET status = 0 WHERE email = '{$email}'";
//            $con->executeQuery($sql);

            $content = array(
                'email' => $email,
                'token' => $token,
            );

            $user = self::getUserByMail($email); // user array
            
            //watchdog
            $log  = array(
                'module_name' => 'Utilisateurs',
                'module_code' => 'mod_user',
                'action' => 'Réinitialiser mot de passe',
                'action_code' => 'mod_user_reset_pass_request',
                'table_parent_name' => 'users',
                'table_parent_id' => $user['id'],
                'content_type' => 'json',
                'content_before' => json_encode($content),
            );
            Watchdog::Log($log);

            // mail body
            $mail_reset_url = BASE_URL . "/reset-pass?token=" . $token;
            $mail_body = "Bonjour " . $user['lastName'] . " " . $user['firstName'] . ",<br/><br/>";
            $mail_body .= "Voici les instructions pour réinitialiser votre mot de passe.<br/>";
            $mail_body .= "Cliquez sur le lien suivant :<br/>";
            $mail_body .= "<a href='" . $mail_reset_url . "'>Réinitialiser votre mot de passe</a><br/><br/>";
            $mail_body .= "Ou copier coller le lien suivant dans votre navigateur.<br/>";
            $mail_body .= $mail_reset_url . "<br/><br/>";
            $mail_body .= "<strong>Notez que le lien sera expiré après 24 heures et vous devriez répéter les procédures de réinitialiser votre mot de passe.</strong><br/><br/><strong>Si ce n'est pas vous qui avez demandé de réinitialiser votre mot de passe, ignorez cet email.</strong>";
            $mail_subject = 'Réinitialisation mot de passe';

            if (Mail::sendMail($mail_subject, $email, $mail_body)) {
                // log 
                $log  = array(
                    'module_name' => 'Mail',
                    'module_code' => 'mod_mail',
                    'action' => 'Envoie Mail Réinitialiser mot de passe',
                    'action_code' => 'mod_mail_send_mail_forget_pass',
                    'table_parent_name' => 'users',
                    'table_parent_id' => $user['id'],
                    'content_type' => 'json',
                    'content_before' => json_encode(array('to' => $email, 'subject' => $mail_subject, 'body' => $mail_body)),
                );
                Watchdog::log($log);
            }

            return true;
            
        } else {
            
            return false;
            
        }
    }
    
    /**
     * 
     * @global Res $con
     * @param int $role_id
     * @return array
     */
    public static function getUsersByRole($role_name) {
        global $con;

        $role_id = Role::getRoleIdByName($role_name);

        $sql = "SELECT * FROM users WHERE role_id = {$role_id} AND status = 1";
        $result = $con->fetchAll($sql);
        return $result;
    }

    /**
     * 
     * @global Res $con
     * @param String $pass
     * @param int $id
     * @return boolean
     */
    public static function isPassOK($pass, $id) {
        global $con;
        $sql = "SELECT * FROM users WHERE password = '{$pass}' AND id = {$id}";
        $result = $con->fetchAll($sql);
        return (!(empty($result)));
    }
        
    /**
     * <p>Check user Login</p>
     * @global Res $con
     * @param String $login
     * @param String $pass
     * @param String $rememberMe
     * @return boolean true/false
     */
    public static function login($username, $pass, $return = false) {
        global $con;
        
        // first check if the username is valid
        $sql = "SELECT * FROM users WHERE username = '{$username}' AND status = 1";
        $user = $con->fetchAssoc($sql);
        if ($user) {
            // check if the password is valid
            if (password_verify($pass, $user['password'])) {
                
                ///unset($user['password']);
                // get user actions
                $actions = self::getPermissionsByRole($user['role_id']);
                if (is_array($actions) && !empty($actions)) {
                    $user['actions'] = $actions;
                } else {
                    $user['actions'] = array();
                }
                // get user teamlead
                if (intval($user['user_id']) > 0) {
                    $user['teamleader'] = self::getUserTeamlead($user['user_id']);
                    unset($user['teamleader']['password']);
                    unset($user['teamleader']['username']);
                    $user['teamleader']['role_name'] = Role::getRoleNameById($user['teamleader']['role_id']);
                }
                
                // add user_ip 
                $user['ip'] = getUserIp();
                
                // add role name 
                $user['role_name'] = Role::getRoleNameById($user['role_id']);
                
                // get the user details in session
                $_SESSION['opsearch_user'] = $user;
                
                //watchdog
                $log  = array(
                    'module_name' => 'Utilisateurs',
                    'module_code' => 'mod_user',
                    'action' => 'login',
                    'action_code' => 'mod_user_login',
                );
                Watchdog::Log($log);
                
                // update last access
                ///if (self::updateLastLogin($user['id'])) {
                    ///return true;
                ///}
                
                self::updateLastLogin($user['id']);
                
                if ($return) {
                    return $_SESSION['opsearch_user'];
                } else {
                    return true;
                }
                
            } else {
                return false;
            }
            
        } else {
            return false;
        }
    }

    public static function isLoggedIn() {
        if (isset($_SESSION['opsearch_user']) && !empty($_SESSION['opsearch_user'])) {
            return true;
        } else {
            return false;
        }
    }

    public static function updateLastLogin($uid) {
        global $con;
        if ($con->update('users', array('lastLogin' => time()), array('id' => $uid)) 
                && $con->insert('userslogin', array('user_id' => $uid, 'ip' => getUserIp(), 'created' => time()))) {
            return true;
        } else {
            return false;
        }
    }

    public static function logout() {
        if (self::isLoggedIn()) {
            
            //watchdog
            $log  = array(
                'module_name' => 'utilisateurs',
                'module_code' => 'mod_user',
                'action' => 'logout',
                'action_code' => 'mod_user_logout',
            );
            Watchdog::Log($log);
            
            // destroy the session
            unset($_SESSION['opsearch_user']);
            unset($_SESSION['opsearch_rdvp_1']);
            unset($_SESSION['opsearch_rdvp_2']);
            session_destroy();
            return true;
        }
    }
    
    public static function getByRingoverUserId($ringover_userid) {
        global $con;
        $sql = "SELECT * FROM users WHERE ringover_userid = {$ringover_userid}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getByExt($ext) {
        global $con;
        $sql = "SELECT * FROM users WHERE voip_ext = {$ext}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getManagers() {
        global $con;
        $sql = "SELECT id, firstName, lastName FROM users WHERE role_id = 2 OR role_id = 1 "
            . "AND status = 1 ORDER BY id ASC";
        return $con->fetchAll($sql);
    }
    
    public static function getKPIManagers () {
        global $con;
        $sql = "SELECT U.id, U.firstName, U.lastName, U.status, R.name as role_name FROM users U LEFT JOIN roles R ON R.id = U.role_id WHERE U.role_id = 2 OR U.role_id = 1 ORDER BY U.lastName ASC";
        return $con->fetchAll($sql);
    }
    
    public static function getKPICDRs () {
        global $con;
        $sql = "SELECT U.id, U.firstName, U.lastName, U.status, R.name as role_name FROM users U LEFT JOIN roles R ON R.id = U.role_id WHERE U.role_id = 4 ORDER BY U.lastName ASC";
        return $con->fetchAll($sql);
    }
    
    public static function getKPICDRsAssistantManagers () {
        global $con;
        $sql = "SELECT U.id, U.firstName, U.lastName, U.status, R.name as role_name FROM users U LEFT JOIN roles R ON R.id = U.role_id WHERE U.role_id IN(4, 9) ORDER BY U.lastName ASC";
        return $con->fetchAll($sql);
    }
    
    public static function getKPICDRsAssistantManagersManagers () {
        global $con;
        $sql = "SELECT U.id, U.firstName, U.lastName, U.status, R.name as role_name FROM users U LEFT JOIN roles R ON R.id = U.role_id WHERE U.role_id IN(2, 4, 9) ORDER BY U.lastName ASC";
        return $con->fetchAll($sql);
    }
        
    public static function getUsers() {
        global $con;
        $sql = "SELECT u.id, u.firstName, u.lastName, u.email, u.username, u.user_id, "
            . "u.status, u.lastLogin, u.created, r.id AS role_id, r.name AS role_name "
            . "FROM users AS u, roles AS r "
            . "WHERE u.role_id = r.id AND role_id > 0 AND role_id <> 13 ORDER BY u.firstName ASC";
        
        $users = $con->fetchAll($sql);
        
        // get each user team leader
        $count = 0;
        $usersList = array();
        foreach($users as $user):
            $usersList[$count] = $user;
            unset($usersList[$count]['password']);
            
            if (intval($user['user_id']) > 0) {
                $users[$count]['teamleader'] = self::getUserTeamlead($user['user_id']);
                unset($users[$count]['teamleader']['password']);
                unset($users[$count]['teamleader']['username']);
            }
            $count++;
        endforeach;        
        return $users;
    }
    
    public static function getUsersClient() {
        global $con;
        $sql = "SELECT u.id, u.firstName, u.lastName, u.email, u.username, u.user_id, u.status, u.lastLogin, u.created, C.nom as client_name, C.id as client_id FROM users AS u LEFT JOIN roles r ON r.id = u.role_id LEFT JOIN clients C ON C.id = u.client_id WHERE role_id = 13 ORDER BY u.firstName ASC";
                
        return $con->fetchAll($sql);
    }
    
    public static function getUserTeamlead($user_id) {
        global $con;
        $sql = "SELECT * FROM users WHERE id = {$user_id}";
        return $con->fetchAssoc($sql);
    }
    
    /**
     * 
     * @global Res $con
     * @param int $uid
     * @return array
     */
    public static function getUserById($uid) {
        global $con;
        $sql = "SELECT * FROM users WHERE id = {$uid}";
        return $con->fetchAssoc($sql);
    }

    /**
     * 
     * @global Res $con
     * @param String $email
     * @return array
     */
    public static function getUserByMail($email) {
        global $con;
        $sql = "SELECT * FROM users WHERE email = '{$email}'";
        return $con->fetchAssoc($sql);
    }

    /**
     * 
     * @global Res $con
     * @param array $user
     * @return boolean
     */
    public static function addUser($user) {
        global $con;
        $pass = $user['password']; // store in a variable to send email to the user        
        $user['password'] = password_hash($user['password'], PASSWORD_BCRYPT);
        
        $con->insert('users', $user);
        $id = $con->lastInsertId();
        
        //watchdog
        $log  = array(
            'module_name' => 'Utilisateurs',
            'module_code' => 'mod_user',
            'action' => 'Ajouter Utilisateur',
            'action_code' => 'mod_user_create_user',
            'table_name' => 'users',
            'table_id' => $id,
            'content_type' => 'json',
            'content_before' => json_encode($user),
        );
        Watchdog::Log($log);
        
        $email = $user['email'];

        // send mail
        $mail_body = 'Bonjour ' . mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName'])
            . '<br/> Votre compte sur OPSEARCH a été créé avec succès.'
            . '<br/> Visitez le lien <a href="' . BASE_URL . '">' . BASE_URL . '</a>'
            . '<br/> Votre identifiant est <i>' . $user['username'] . '</i> et votre mot de passe est <i>' . $pass . '</i>';

        Mail::sendMail('Compte OPSEARCH', $email, $mail_body);

        return true;
    }
    
    public static function addUserClient($user, $assigned_mission_ids) {
        global $con;
        $pass = $user['password']; // store in a variable to send email to the user        
        $user['password'] = password_hash($user['password'], PASSWORD_BCRYPT);
        
        $con->insert('users', $user);
        $new_user_id = $con->lastInsertId();
        
        //watchdog
        $log  = array(
            'module_name' => 'Utilisateurs',
            'module_code' => 'mod_user',
            'action' => 'Ajouter Utilisateur Client',
            'action_code' => 'mod_user_create_user_client',
            'table_name' => 'users',
            'table_id' => $new_user_id,
            'content_type' => 'json',
            'content_before' => json_encode($user),
        );
        Watchdog::Log($log);
        
        // insert the assigned missions to the user client
        $mission_ids = explode('|', $assigned_mission_ids);
        
        foreach($mission_ids as $mission_id):   
            
            $con->insert('usersclientmissions', array('user_id' => $new_user_id, 'mission_id' => $mission_id));
        
            $id = $con->lastInsertId();
            
            //watchdog
            $log  = array(
                'module_name' => 'Utilisateurs',
                'module_code' => 'mod_user',
                'action' => 'Assigner Utilisateur Client Mission',
                'action_code' => 'mod_user_assign_user_client_to_mission',
                'table_parent_name' => 'missions',
                'table_parent_id' => $mission_id,
                'table_name' => 'missionsusers',
                'table_id' => $id,
                'content_type' => 'json',
                'content_before' => json_encode(array('user_id' => $new_user_id, 'mission_id' => $mission_id)),
            );
            Watchdog::Log($log);
        
        endforeach;
        
        // send mail to the client user to notify of account creation
        $email = $user['email'];
        $mail_body = 'Bonjour ' . mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName'])
            . '<br/> Votre compte sur OPSEARCH a été créé avec succès.'
            . '<br/> Visitez le lien <a href="' . BASE_URL . '">' . BASE_URL . '</a>'
            . '<br/> Votre identifiant est <i>' . $user['username'] . '</i> et votre mot de passe est <i>' . $pass . '</i>';

        if (Mail::sendMail('Compte OPSEARCH', $email, $mail_body)) {
            //watchdog
            $log  = array(
                'module_name' => 'Utilisateurs',
                'module_code' => 'mod_user',
                'action' => 'Envoyer Mail Utilisateur Client Creation Compte',
                'action_code' => 'mod_user_send_email_client_account_creation',
                'table_name' => 'users',
                'table_id' => $new_user_id,
                'content_type' => 'json',
                'content_before' => json_encode(array('email' => $mail_body, 'to' => $email, 'to_user_id' => $new_user_id)),
            );
            Watchdog::Log($log);
        }
                
        return true;
    }
    
    public static function editUserClient($user, $assigned_mission_ids) {
        global $con;
        
        // check if the password has changed
        if (isset($user['password'])) {
            $pass = $user['password'];
            $user['password'] = password_hash($user['password'], PASSWORD_BCRYPT);
        }
        
        $content_before = self::getUserById($user['id']);
        
        // update the user
        $con->update('users', $user, array('id' => $user['id']));
        
        // watchdog
        $log  = array(
            'module_name' => 'Utilisateurs',
            'module_code' => 'mod_user',
            'action' => 'Éditer Utilisateur Client',
            'action_code' => 'mod_user_edit_user_client',
            'table_name' => 'users',
            'table_id' => $user['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($user),
        );
        Watchdog::Log($log);
        
        if (isset($pass)) {
            $email = $user['email'];

            // send mail
            $mail_body = 'Bonjour ' . mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName'])
                . '<br/> Votre compte OPSEARCH a été modifié.'
                . '<br/> Visitez le lien <a href="' . BASE_URL . '">' . BASE_URL . '</a>'
                . '<br/> Votre identifiant est <i>' . $user['username'] . '</i> et votre mot de passe est <i>' . $pass . '</i>';

            if (Mail::sendMail('Compte OPSEARCH', $email, $mail_body)) {
                //watchdog
                $log  = array(
                    'module_name' => 'Utilisateurs',
                    'module_code' => 'mod_user',
                    'action' => 'Envoyer Mail Utilisateur Client Modification Compte',
                    'action_code' => 'mod_user_send_email_client_account_modification',
                    'table_name' => 'users',
                    'table_id' => $user['id'],
                    'content_type' => 'json',
                    'content_before' => json_encode(array('email' => $mail_body, 'to' => $email, 'to_user_id' => $user['id'])),
                );
                Watchdog::Log($log);
            }
        }
        
        // insert the assigned missions to the user client
        
        // 3 possibilities
        // 1. add new mission
        // 2. existing mission
        // 3. deleted mission
     
        // we get all existing mission from db => before modif
        $existingMissions = Mission::getAssignedMissions($user['id']);
        $mission_ids = explode('|', $assigned_mission_ids);
        
        foreach($mission_ids as $mission_id):
            
            // check if the mission id already existed
            if (in_array($mission_id, $existingMissions)) {
                // do nothing
            } else { // mission did not exist
                $con->insert('usersclientmissions', array('mission_id' => $mission_id, 'user_id' => $user['id']));
                
                $id = $con->lastInsertId();
        
                //watchdog
                $log  = array(
                    'module_name' => 'Utilisateurs',
                    'module_code' => 'mod_user',
                    'action' => 'Assigner Utilisateur Client Mission',
                    'action_code' => 'mod_user_assign_user_client_to_mission',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'usersclientmissions',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode(array('user_id' => $user['id'], 'mission_id' => $mission_id)),
                );
                Watchdog::Log($log);
                
                // mail the user to notify
                // send mail
                $link = BASE_URL . '/tdc-client?id=' . $mission_id;
                $mail_body = 'Bonjour ' . mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName'])
                    . '<br/> Une nouvelle mission vous a été assignée.'
                    . '<br/> Visitez le lien <a href="' . $link . '">' . $link . '</a>'
                    . '<br/>' . $link;
                
                // get mission detail
                $mission_data = Mission::getMissionById($mission_id);
                if ($mission_data['prestataire_id'] == 1) {
                    $logo_mail = "opsearch";
                } else {
                    $logo_mail = "headhunting";
                }
                
                if (Mail::sendMail('Nouvelle Mission ' . $mission_id . ' vous a été assignée', $user['email'], $mail_body, $logo_mail)) {
                    //watchdog
                    $log  = array(
                        'module_name' => 'Utilisateurs',
                        'module_code' => 'mod_user',
                        'action' => 'Envoyer Mail Utilisateur Client Assigner Mission',
                        'action_code' => 'mod_user_send_email_client_assign_mission',
                        'table_name' => 'users',
                        'table_id' => $user['id'],
                        'content_type' => 'json',
                        'content_before' => json_encode(array('email' => $mail_body, 'to' => $user['email'], 'to_user_id' => $user['id'])),
                    );
                    Watchdog::Log($log);
                }
                
            }                       
        endforeach;
        
        // check if the mission existed and now does not exist
        // the mission has been deassigned to the user
        foreach ($existingMissions as $mission_id):
            
            if (!in_array($mission_id, $mission_ids)) {
                $con->delete('usersclientmissions', array('mission_id' => $mission_id, 'user_id' => $user['id']));
                        
                //watchdog
                $log  = array(
                    'module_name' => 'Utilisateurs',
                    'module_code' => 'mod_user',
                    'action' => 'Supprimer Mission Utilisateurs Client',
                    'action_code' => 'mod_user_delete_mission_user_client',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'usersclientmissions',
                    'content_type' => 'json',
                    'content_before' => json_encode(array('mission_id' => $mission_id, 'user_id' => $user['id'])),
                );
                Watchdog::Log($log);
                
                // get mission detail
                $mission_data = Mission::getMissionById($mission_id);
                if ($mission_data['prestataire_id'] == 1) {
                    $logo_mail = "opsearch";
                } else {
                    $logo_mail = "headhunting";
                }
                
                // send mail
                $mail_body = 'Bonjour ' . mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName'])
                    . '<br/> Vous avez été désaffecté de la mission ' . $mission_id;
                
                if (Mail::sendMail('Mission ' . $mission_id . ' désaffectée', $user['email'], $mail_body, $logo_mail)) {
                    //watchdog
                    $log  = array(
                        'module_name' => 'Utilisateurs',
                        'module_code' => 'mod_user',
                        'action' => 'Envoyer Mail Utilisateur Client Deassigner Mission',
                        'action_code' => 'mod_user_send_email_client_deassign_mission',
                        'table_name' => 'users',
                        'table_id' => $user['id'],
                        'content_type' => 'json',
                        'content_before' => json_encode(array('email' => $mail_body, 'to' => $user['email'], 'to_user_id' => $user['id'])),
                    );
                    Watchdog::Log($log);
                }
        
            }
            
        endforeach;
        
        // end assigned missions to user client
        
        return true;
    }
    
    public static function editMyProfile($user) {
        global $con;
        
        $content_before = self::getUserById($user['id']);
        
        // check if the password has changed
        if (isset($user['password'])) {
            $user['password'] = password_hash($user['password'], PASSWORD_BCRYPT);
        }
        
        // update the user
        $con->update('users', $user, array('id' => $user['id']));
        
        // watchdog
        $log  = array(
            'module_name' => 'Utilisateurs',
            'module_code' => 'mod_user',
            'action' => 'Modifier Mot de passe mon profile',
            'action_code' => 'mod_user_modify_pass_my_profile',
            'table_name' => 'users',
            'table_id' => $user['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($user),
        );
        Watchdog::Log($log);
        
        return true;
        
    }
    
    public static function editUser($user) {
        global $con;
        
        // check if the password has changed
        if (isset($user['password'])) {
            $pass = $user['password'];
            $user['password'] = password_hash($user['password'], PASSWORD_BCRYPT);
        }
        
        $old_user_id = $user['old_user_id'];
        unset($user['old_user_id']);
        
        $content_before = self::getUserById($user['id']);
        
        // update the user
        $con->update('users', $user, array('id' => $user['id']));
        
        // watchdog
        $log  = array(
            'module_name' => 'Utilisateurs',
            'module_code' => 'mod_user',
            'action' => 'Éditer Utilisateur',
            'action_code' => 'mod_user_edit_user',
            'table_name' => 'users',
            'table_id' => $user['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($user),
        );
        
        // check changes in chef d'equipe
        if ($old_user_id <> $user['user_id']) {
            $log['chef_equipe_before_id'] = $old_user_id;
            $old_user = User::getUserById($old_user_id);
            $log['chef_equipe_before'] = mb_strtoupper($old_user['lastName']) . ' ' . mb_ucfirst($old_user['firstName']);
            $log['chef_equipe_after_id'] = $user['user_id'];
            $new_user = User::getUserById($user['user_id']);
            $log['chef_equipe_after'] = mb_strtoupper($new_user['lastName']) . ' ' . mb_ucfirst($new_user['firstName']);
        }
        
        Watchdog::Log($log);
        
        if (isset($pass)) {
            $email = $user['email'];

            // send mail
            $mail_body = 'Bonjour ' . mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName'])
                . '<br/> Votre compte OPSEARCH a été modifié.'
                . '<br/> Visitez le lien <a href="' . BASE_URL . '">' . BASE_URL . '</a>'
                . '<br/> Votre identifiant est <i>' . $user['username'] . '</i> et votre mot de passe est <i>' . $pass . '</i>';

            Mail::sendMail('Compte OPSEARCH', $email, $mail_body);
        }
        
        return true;
    }

    /**
     * 
     * @global Res $con
     * @param array $user
     * @return boolean
     */
    public static function updateUser($user) {
        global $con;
        $uid = $user['id'];
        $con->update('users', $user, array('id' => $uid));
        return true;
    }

    /**
     * <p>Set status=2 instead of deleting</p>
     * @global Res $con
     * @param int $uid
     * @return boolean
     */
    public static function deleteUser($uid, $email) {
        global $con;
        //$con->delete('users', array('id' => $uid));
        $con->update('users', array('status' => 2), array('id' => $uid));

        // logging
        $log = array(
            'element' => 'users',
            'action' => 'delete_user',
            'result' => 'OK',
            'content' => 'User ' . $uid . ' effacé, status = 2',
        );
        Watchdog::log($log);

        $mail_body = "Votre compte est effacé. Contactez un admin pour vous aider";

        if (Mail::sendMail('Compte effacé', $email, $mail_body)) {
            // log 
            $log = array(
                'element' => 'mail',
                'action' => 'mail_deleted_account',
                'content' => json_encode(array('to' => $email, 'body' => $mail_body)),
                'result' => 'OK',
            );
            Watchdog::log($log);
        }

        return true;
    }
    
    public static function getNumUsersByRoleId($role_id) {
        global $con;
        $sql = "SELECT COUNT(id) AS num_users FROM users WHERE role_id = {$role_id}";
        $result = $con->fetchAssoc($sql);
        return $result['num_users'];
    }


    public static function getPermissionsByRole($role_id) {
        global $con;
        if ($role_id == 0) { //super admin can do everything with role_id = 0
            $sql = "SELECT DISTINCT(code) FROM modulesactions ORDER BY code ASC";
        } else {
            $sql = "SELECT DISTINCT(code) FROM modulesactions c, rolesactions ra "
                    . "WHERE c.id = ra.action_id AND ra.role_id = {$role_id} "
                    . "ORDER BY code ASC";
        }
        $results = $con->fetchAll($sql);
        $actions = array();
        foreach ($results as $result):
            $actions[] = $result['code'];
        endforeach;
        return $actions;
    }

    public static function can($action) {
        
        if (!self::isLoggedIn()){
            return false;
        }
        
        $actionsList = $_SESSION['opsearch_user']['actions'];
        
        if (empty($actionsList)) {
            return false;
        }
        
        if (is_bool($actionsList)) {
            return false;
        }

        if (is_array($action)) {
            
            foreach ($action as $key => $value) {
                if (in_array($value, $actionsList)) {
                    return true;
                }
            }
            return false;
            
        } else {

            if (in_array($action, $actionsList)) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    public static function IsRingOverUserIdExist($ringover_userid, $user_id = NULL) {
        global $con;
        
        if ($user_id) {
            $sql = "SELECT ringover_userid FROM users WHERE ringover_userid = {$ringover_userid} AND id <> {$user_id}";
        } else {
            $sql = "SELECT ringover_userid FROM users WHERE ringover_userid = {$ringover_userid}";
        }
        
        $results = $con->fetchAll($sql);

        return (!empty($results));
    }
    
    public static function isSDAExist($sda, $user_id = NULL) {
        global $con;
        
        if ($user_id) {
            $sql = "SELECT voip_sda FROM users WHERE voip_sda = '{$sda}' AND id <> {$user_id}";
        } else {
            $sql = "SELECT voip_sda FROM users WHERE voip_sda = '{$sda}'";
        }
        
        $results = $con->fetchAll($sql);

        return (!empty($results));
    }
    
    public static function isExtExist($ext, $user_id = NULL) {
        global $con;
        
        if ($user_id) {
            $sql = "SELECT voip_ext FROM users WHERE voip_ext = '{$ext}' AND id <> {$user_id}";
        } else {
            $sql = "SELECT voip_ext FROM users WHERE voip_ext = '{$ext}'";
        }
        
        $results = $con->fetchAll($sql);

        return (!empty($results));
    }
    
    /**
     * 
     * @global Res $con
     * @param String $email
     * @return boolean true/false
     */
    public static function isUserMailExist($email, $user_id = NULL) {
        global $con;
        
        if ($user_id) {
            $sql = "SELECT email FROM users WHERE email = '{$email}' AND id <> {$user_id}";
        } else {
            $sql = "SELECT email FROM users WHERE email = '{$email}'";
        }
        
        $results = $con->fetchAll($sql);

        return (!empty($results));
    }

    /**
     * 
     * @global Res $con
     * @param String $login
     * @return boolean
     */
    public static function isLoginExist($username, $user_id = NULL) {
        global $con;
        
        if ($user_id) {
            $sql = "SELECT username FROM users WHERE username = '{$username}' AND id <> {$user_id}";
        } else {
            $sql = "SELECT username FROM users WHERE username = '{$username}'";
        }
        
        $results = $con->fetchAll($sql);
        

        return (!empty($results));
    }
    
    public static function isSA() {
        if (self::isLoggedIn()) {
            if ($_SESSION['opsearch_user']['role_id'] == 0) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }
    
    public static function isClient() {
        if (self::isLoggedIn()) {
            if ($_SESSION['opsearch_user']['role_id'] == 13) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }
    
    public static function isAdmin() {
        if (self::isLoggedIn()) {
            if ($_SESSION['opsearch_user']['role_id'] == 1) {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }
    
    public static function isMissionManager() {
        global $con;
        $user_id = $_SESSION['opsearch_user']['id'];
        $sql = "SELECT * FROM missions WHERE manager_id = {$user_id}";
        $results = $con->fetchAll($sql);
        return (!empty($results));
    }
    
    public static function getUsersByTLId($TL_user_id) {
        global $con;
        $sql = "SELECT * FROM users WHERE (user_id = {$TL_user_id} OR id = $TL_user_id) AND status = 1";
        return $con->fetchAll($sql);
    }
    
    public static function getCDRByManagerId($manager_id) {
        global $con;
        
        $sql = "SELECT U.id, U.firstName, U.lastName, U.status, R.name as role_name FROM users U LEFT JOIN roles R ON R.id = U.role_id WHERE U.role_id = 4 AND U.user_id = {$manager_id} ORDER BY U.lastName ASC";
                
        return $con->fetchAll($sql);
    }
    
    public static function getCDRAssistantManagerByManagerId($manager_id) {
        global $con;
        
        $sql = "SELECT U.id, U.firstName, U.lastName, U.status, R.name as role_name, U.voip_ext FROM users U LEFT JOIN roles R ON R.id = U.role_id WHERE U.role_id IN(4, 9) AND U.user_id = {$manager_id} ORDER BY U.lastName ASC";
                
        return $con->fetchAll($sql);
    }
            
}
