<?php
class Event {
    
    public static function getTypes() {
        global $con;
        $sql = "SELECT * FROM eventstypes WHERE status = 1 ORDER BY id ASC";
        return $con->fetchAll($sql);
    }
    
    public static function add($event) {
        global $con;
                
        $con->insert('events', $event);
        
        $id = $con->lastInsertId();
                
        //watchdog
        $log  = array(
            'module_name' => 'Événements',
            'module_code' => 'mod_event',
            'action' => 'Ajouter événement',
            'action_code' => 'mod_event_create_event',
            'table_name' => 'events',
            'table_id' => $id,
            'content_type' => 'json',
            'content_before' => json_encode($event),
        );
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function edit($event) {
        global $con;
        
        $content_before = self::getEventById($event['id']);
        
        $con->update('events', $event, array('id' => $event['id']));
                        
        //watchdog
        $log  = array(
            'module_name' => 'Événements',
            'module_code' => 'mod_event',
            'action' => 'Éditer événement',
            'action_code' => 'mod_event_edit_event',
            'table_name' => 'events',
            'table_id' => $event['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($event),
        );
        Watchdog::Log($log);        
        
        return true;
    }
    
    public static function getEventById($id) {
        global $con;
        $sql = "SELECT * FROM events WHERE id = {$id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function delete($id) {
        global $con;
        
        $content_before = self::getEventById($id);
        
        $con->delete('events', array('id' => $id));
        
        //watchdog
        $log  = array(
            'module_name' => 'Événements',
            'module_code' => 'mod_event',
            'action' => 'Supprimer événement',
            'action_code' => 'mod_event_delete_event',
            'table_name' => 'events',
            'table_id' => $id,
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
        );
        Watchdog::Log($log);
        
        return true;
    }
    
    /**
     * 
     * @global type $con
     * @param Int $user_id <p>logged in user id to load event which are private only to him unless the user can </p>
     * @return Array
     */
    public static function getEvents($user_id) {
        global $con;
        
        $sql = "SELECT e.id AS id, e.title AS title, e.start AS start, e.end AS end, "
            . "t.name AS name, t.type AS type, t.cssClass AS cssClass, t.private AS private, u.id as user_id, "
            . "u.firstName as firstName, u.lastName as lastName "
            . "FROM events AS e, eventstypes AS t, users AS u "
            . "WHERE u.id = e.user_id AND e.eventType_id = t.id AND e.status = 1 AND u.status = 1 "
            . "ORDER BY e.id ASC";
        return $con->fetchAll($sql);
                
    }
    
    public static function CheckTime($event) {
        $microtimeStart = $event['start'];
        $microtimeNow = time() * 1000;
        
        // check if date is not today first
        if ($microtimeNow > $microtimeStart) {
            return false;
        } else {
            return true;
        }

    }
    
    public static function CheckOverlapEvent($event) {
        global $con;
                
        $start_date = $event['start_date'];
        $end_date = $event['end_date'];
        $logged_user_id = $event['user_id'];
        
        $sql = "SELECT * FROM events WHERE user_id = {$logged_user_id} AND end_date > '{$start_date}' AND start_date < '{$end_date}'";
        $results = $con->fetchAll($sql);
        if (empty($results)) {
            return true;
        } else {
            return false;
        }
        
    }
    
    public static function CheckOverlapEventEdit($event) {
        global $con;
                
        $start_date = $event['start_date'];
        $end_date = $event['end_date'];
        $logged_user_id = $event['user_id'];
        
        $sql = "SELECT * FROM events WHERE user_id = {$logged_user_id} AND end_date > '{$start_date}' "
        . "AND start_date < '{$end_date}' AND id NOT IN({$event['id']})";
        $results = $con->fetchAll($sql);
        if (empty($results)) {
            return true;
        } else {
            return false;
        }
        
    }
    
    public static function getMaxNumOfEvent($type_id) {
        global $con;
        // check the number of concurrent connections at the same time for the type of event
        if ($type_id == 4) { // RDVP
            // we get the number of active user for the numConnection
            $sql = "SELECT COUNT(id) AS numConnection FROM users WHERE status = 1 AND role_id > 0";
            $result1 = $con->fetchAssoc($sql);
            $maxCon = intval($result1['numConnection']);
        } else {
            $sql = "SELECT * FROM eventstypes WHERE id = {$type_id}";
            $result1 = $con->fetchAssoc($sql);
            $maxCon = intval($result1['numConnection']);
        }
        
        return $maxCon;
    }


    public static function checkOverlapEventType($event) {
        
        global $con;
                
        $eventType_id = $event['eventType_id'];
        $start_date = $event['start_date'];
        $end_date = $event['end_date'];
        $logged_user_id = $event['user_id'];
        
        $maxCon = self::getMaxNumOfEvent($eventType_id);
        
        // check if the event matches other event with the same type on the same start and end time
        $sql = "SELECT COUNT(id) AS numEvents FROM events WHERE eventType_id = {$eventType_id} AND end_date > '{$start_date}' AND start_date < '{$end_date}'";
        $result2 = $con->fetchAssoc($sql);
        $numEvents = intval($result2['numEvents']);
        
        if ($numEvents >= $maxCon) {
            return false;
        } elseif ($numEvents < $maxCon) {
            // check if the event existed at that time belongs to the same user
            // if yes return false
            // else return true
            $sql = "SELECT user_id FROM events WHERE eventType_id = {$eventType_id} AND end_date > '{$start_date}' AND start_date < '{$end_date}'";
            $result3 = $con->fetchAssoc($sql);
            $eventUserId = $result3['user_id'];
            if ($eventUserId == $logged_user_id) {
                return false;
            } else {
                return true;
            }
        }
    }
    
    public static function checkOverlapEventTypeEdit($event) {
        
        global $con;
                
        $eventType_id = $event['eventType_id'];
        $start_date = $event['start_date'];
        $end_date = $event['end_date'];
        $logged_user_id = $event['user_id'];
        
        $maxCon = self::getMaxNumOfEvent($eventType_id);
        
        // check if the event matches other event with the same type on the same start and end time
        $sql = "SELECT COUNT(id) AS numEvents FROM events WHERE eventType_id = {$eventType_id} AND end_date > '{$start_date}' AND start_date < '{$end_date}' AND id NOT IN({$event['id']})";
        $result2 = $con->fetchAssoc($sql);
        $numEvents = intval($result2['numEvents']);
        
        if ($numEvents >= $maxCon) {
            return false;
        } elseif ($numEvents < $maxCon) {
            // check if the event existed at that time belongs to the same user
            // if yes return false
            // else return true
            $sql = "SELECT user_id FROM events WHERE eventType_id = {$eventType_id} AND end_date > '{$start_date}' AND start_date < '{$end_date}' AND id NOT IN({$event['id']})";
            $result3 = $con->fetchAssoc($sql);
            $eventUserId = $result3['user_id'];
            if ($eventUserId == $logged_user_id) {
                return false;
            } else {
                return true;
            }
        }
    }
    
    public static function getRdvp($user_id) {
        
        global $con;
        
        $now = date('Y-m-d H:i:s', time());
            
        $sql = "SELECT * FROM events WHERE eventType_id = 4 AND user_id = {$user_id} AND status = 1 AND start_date >= '{$now}' ORDER BY start_date ASC";
        return $con->fetchAssoc($sql); // retrieve only the first record
    }
    
}

