<?php
class Task {
    
    public static $status = array(
        0 => 'terminée',
        1 => 'actif',
        2 => 'archivée',
        3 => 'supprimée',
    );
    
    public static function addThread($taskThread) {
        global $con;
        $con->insert('tasksthreads', $taskThread);
        
        $id = $con->lastInsertId();
        
        //watchdog
        $log  = array(
            'module_name' => 'Tâches',
            'module_code' => 'mod_task',
            'action' => 'Ajouter Tâche Commentaire',
            'action_code' => 'mod_task_create_comment',
            'table_parent_name' => 'tasks',
            'table_parent_id' => $taskThread['task_id'],
            'table_name' => 'tasksthreads',
            'table_id' => $id,
            'content_type' => 'json',
            'content_before' => json_encode($taskThread),
        );
        Watchdog::Log($log);
        
        // must send an email to the assignee or creator to notify about the comment added
        // get details of task
        $task = self::getTaskById($taskThread['task_id']);
               
        if ($taskThread['user_id'] == $task['assignee_id']) {
            
            $creator = User::getUserById($task['assignee_id']);
            $assignee = User::getUserById($task['creator_id']);
            
        } elseif ($taskThread['user_id'] == $task['creator_id']) {
            
            $creator = User::getUserById($task['creator_id']);
            $assignee = User::getUserById($task['assignee_id']);
            
        }
        
        // send mail
        $mail_body = 'Bonjour ' . mb_ucfirst($assignee['firstName']) . ' ' . mb_strtoupper($assignee['lastName'])
            . '<br/>Un nouveau commentaire a été ajouté à la tâche "' . $task['title'] . '" par ' . mb_ucfirst($creator['firstName']) . ' ' . mb_strtoupper($creator['lastName'])
            . '<br/><p>' . $taskThread['comment'] . '</p>'
            . '<br/>Visitez le lien pour plus de détails <a href="' . BASE_URL . '/taches?id=' . $task['id']  . '">' .  $task['title'] . '</a>'
            . '<br/>' . BASE_URL . '/taches?id=' . $task['id'];

        Mail::sendMail('Nouveau commentaire ' . $task['title'], $assignee['email'], $mail_body);
        
        return true;
    }
    
    public static function Checked($task_data) {
        
        global $con;
        
        $content_before = self::getTaskById($task_data['id']);
        
        if ($task_data['status'] == 1) { //marked as complete
            $action = "Incomplet Tâche";
            $action_code = "mod_task_incomplet_task";
        } else if ($task_data['status'] == 0) { // marked as uncomplete
            $action = "Terminée Tâche";
            $action_code = "mod_task_finish_task";
        }
        
        $completer = $task_data['completer']; // creator or asignee
        unset($task_data['completer']);
        $con->update('tasks', $task_data, array('id' => $task_data['id']));
        
        $task = self::getTaskById($task_data['id']);
                 
        //watchdog
        $log  = array(
            'module_name' => 'Tâches',
            'module_code' => 'mod_task',
            'action' => $action,
            'action_code' => $action_code,
            'table_name' => 'tasks',
            'table_id' => $task_data['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($task),
            'status_before_id' => $content_before['status'],
            'status_before' => self::$status[$content_before['status']],
            'status_after_id' => $task['status'],
            'status_after' => self::$status[$task['status']],
        );
        Watchdog::Log($log);
        
        
        // if completer is creator send email to assignee
        if ($completer == "creator") {
            $user_sender = User::getUserById($task['assignee_id']);
            $user_completer = User::getUserById($task['creator_id']);
        } elseif ($completer == "assignee") {
            $user_sender = User::getUserById($task['creator_id']);
            $user_completer = User::getUserById($task['assignee_id']);
        }
        
        if ($task['status'] == 0) { // was uncompleted and the task task just completed
            // send mail
            $mail_body = 'Bonjour ' . mb_ucfirst($user_sender['firstName']) . ' ' . mb_strtoupper($user_sender['lastName'])
                . '<br/>La tâche a été marquée comme terminée par ' . mb_ucfirst($user_completer['firstName']) . ' ' . mb_strtoupper($user_completer['lastName'])
                . '<br/><p>' . $task['description'] . '</p>'
                . '<br/>Visitez le lien pour plus de détails <a href="' . BASE_URL . '/taches?id=' . $task['id'] . '">' . 'Nouvelle tâche :: ' . $task['title'] . '</a>'
                . '<br/>' . BASE_URL . '/taches?id=' . $task['id'];
            
            Mail::sendMail('Tâche terminée :: ' . $task['title'], $user_sender['email'], $mail_body);
            
        } elseif ($task['status'] == 1) { // was completed and the task just uncpmpleted 
            // send mail
            $mail_body = 'Bonjour ' . mb_ucfirst($user_sender['firstName']) . ' ' . mb_strtoupper($user_sender['lastName'])
                . '<br/>La tâche a été marquée comme incomplète par ' . mb_ucfirst($user_completer['firstName']) . ' ' . mb_strtoupper($user_completer['lastName'])
                . '<br/><p>' . $task['description'] . '</p>'
                . '<br/>Visitez le lien pour plus de détails <a href="' . BASE_URL . '/taches?id=' . $task['id'] . '">' . 'Nouvelle tâche :: ' . $task['title'] . '</a>'
                . '<br/>' . BASE_URL . '/taches?id=' . $task['id'];
            
            Mail::sendMail('Tâche marquée incomplète :: ' . $task['title'], $user_sender['email'], $mail_body);
        }
    }

    public static function add($task) {
        global $con;
                
        $con->insert('tasks', $task);
        
        $task_id = $con->lastInsertId();
                
        //watchdog
        $log  = array(
            'module_name' => 'Tâches',
            'module_code' => 'mod_task',
            'action' => 'Ajouter Tâche',
            'action_code' => 'mod_task_create_task',
            'table_name' => 'tasks',
            'table_id' => $task_id,
            'content_type' => 'json',
            'content_before' => json_encode($task),
            'status_before_id' => 1,
            'status_before' => 'actif',
        );
        Watchdog::Log($log);
        
        // get assignee details
        $assignee = User::getUserById($task['assignee_id']);
        // get creator details
        $creator = User::getUserById($task['creator_id']);    
        
        // send mail
        $mail_body = 'Bonjour ' . mb_ucfirst($assignee['firstName']) . ' ' . mb_strtoupper($assignee['lastName'])
            . '<br/>Une nouvelle tâche vous a été assignée par ' . mb_ucfirst($creator['firstName']) . ' ' . mb_strtoupper($creator['lastName'])
            . '<br/><p>' . $task['description'] . '</p>'
            . '<br/>Visitez le lien pour plus de détails <a href="' . BASE_URL . '/taches?id=' . $task_id . '">' . 'Nouvelle tâche :: ' . $task['title'] . '</a>'
            . '<br/>' . BASE_URL . '/taches?id=' . $task_id;

        Mail::sendMail('Nouvelle tâche :: ' . $task['title'], $assignee['email'], $mail_body);
        
        return true;
    }
    
    public static function edit($task) {
        global $con;
        
        $title_old = $task['title_old'];
        
        $assignee_change = false;
        if ($task['assignee_id_old'] != $task['assignee_id']) {
            $assignee_id_old = $task['assignee_id_old'];
            $assignee_change = true;
        }
        
        $content_before = self::getTaskById($task['id']);
        
        // remove the unused index and save the record
        unset($task['assignee_id_old']);
        unset($task['title_old']);
        $con->update('tasks', $task, array('id' => $task['id']));
        
        //watchdog
        $log  = array(
            'module_name' => 'Tâches',
            'module_code' => 'mod_task',
            'action' => 'Éditer Tâche',
            'action_code' => 'mod_task_edit_task',
            'table_name' => 'tasks',
            'table_id' => $task['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($task),
            'status_before_id' => $content_before['status'],
            'status_before' => self::$status[$content_before['status']],
            'status_after_id' => $content_before['status'],
            'status_after' => self::$status[$content_before['status']],
        );
        Watchdog::Log($log);
        
        
        // send email to user to notifiy based on the conditions
        // if assignee remains the same notify the user about the task changed
        // if assignee changed email the old assignee that this task is no longer for him
        // and notify the new assignee about the new task assigned to him/her
        // get assignee details
        $assignee = User::getUserById($task['assignee_id']);
        // get creator details
        $creator = User::getUserById($task['creator_id']);
        
        if ($assignee_change) {
            
            $assignee_old = User::getUserById($assignee_id_old);
            
            // if assignee changed notify the old assignee that this task is no longer for him
            $mail_body = 'Bonjour ' . mb_ucfirst($assignee_old['firstName']) . ' ' . mb_strtoupper($assignee_old['lastName'])
                . '<br/> La tâche "' . $title_old . '" qui vous a été assignée par ' . mb_ucfirst($creator['firstName']) . ' ' . mb_strtoupper($creator['lastName']) . ' est maintenant assignée à ' . mb_ucfirst($assignee['firstName']) . ' ' . mb_strtoupper($assignee['lastName']) . '.';

            Mail::sendMail('Tâche :: ' . $task['title'], $assignee_old['email'], $mail_body);
            
            // and notify the new assignee about the new task assigned to him/her
            $mail_body = 'Bonjour ' . mb_ucfirst($assignee['firstName']) . ' ' . mb_strtoupper($assignee['lastName'])
                . '<br/>Une nouvelle tâche vous a été assignée par ' . mb_ucfirst($creator['firstName']) . ' ' . mb_strtoupper($creator['lastName'])
                . '<br/><p>' . $task['description'] . '</p>'
                . '<br/>Visitez le lien pour plus de détails <a href="' . BASE_URL . '/taches?id=' . $task['id'] . '">' . 'Nouvelle tâche :: ' . $task['title'] . '</a>'
                . '<br/>' . BASE_URL . '/taches?id=' . $task['id'];

            Mail::sendMail('Nouvelle tâche :: ' . $task['title'], $assignee['email'], $mail_body);
            
        } else { // if assignee remains the same notify the user about the task changed
            
            $mail_body = 'Bonjour ' . mb_ucfirst($assignee['firstName']) . ' ' . mb_strtoupper($assignee['lastName'])
                . '<br/>Tâche "' . $title_old .  '" assignée par ' . mb_ucfirst($creator['firstName']) . ' ' . mb_strtoupper($creator['lastName']) . ' a été modifiée.'
                . '<br/><br/>' . $task['title']
                . '<br/><p>' . $task['description'] . '</p>'
                . '<br/>Visitez le lien pour plus de détails <a href="' . BASE_URL . '/taches?id=' . $task['id'] . '">' . 'Nouvelle tâche :: ' . $task['title'] . '</a>'
                . '<br/>' . BASE_URL . '/taches?id=' . $task['id'];

            Mail::sendMail('Tâche :: "' . $task['title'] . '" modifiée', $assignee['email'], $mail_body);
            
        }
        
        return true;
    }
    
    public static function updateTask($task) {
        global $con;
        
        $content_before = self::getTaskById($task['id']);
        
        $con->update('tasks', $task, array('id' => $task['id']));
        
        if ($task['status'] == 3) { // delete
            $action = 'Supprimée Tâche';
            $action_code = 'mod_task_delete_task';
        } elseif ($task['status'] == 2) { // archivee
            $action = 'Archivée Tâche';
            $action_code = 'mod_task_archivee_task';
        }
         
        //watchdog
        $log  = array(
            'module_name' => 'Tâches',
            'module_code' => 'mod_task',
            'action' => $action,
            'action_code' => $action_code,
            'table_name' => 'tasks',
            'table_id' => $task['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($task),
            'status_before_id' => $content_before['status'],
            'status_before' => self::$status[$content_before['status']],
            'status_after_id' => $task['status'],
            'status_after' => self::$status[$task['status']],
        );
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function getTaskById($task_id) {
        global $con;
        $sql = "SELECT * FROM tasks WHERE id = {$task_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getTaskDetailsById($task_id) {
        global $con;
        $task = self::getTaskById($task_id);
        
        // get task creator details
        $task['creator'] = User::getUserById($task['creator_id']);
        
        // get task assignee details
        $task['assignee'] = User::getUserById($task['assignee_id']);
        
        // get all threads of the task
        $task['threads'] = self::getTaskThreadsByTaskId($task_id);
        
        return $task;
        
    }
    
    public static function getTaskThreadsByTaskId($task_id) {
        global $con;
        $sql = "SELECT t.id, t.task_id, t.user_id, t.comment, t.created, t.status, u.firstName, u.lastName "
            . "FROM tasksthreads t, users u "
            . "WHERE t.status = 1 AND t.user_id = u.id AND task_id = {$task_id} "
            . "ORDER by t.id ASC";
        return $con->fetchAll($sql);
    }
    
    public static function cansee($user_id, $task_id) {
        global $con;
        $task = self::getTaskById($task_id);
        
        // check if the task exist
        if (empty($task)) {
            return false;
        }
        
        if(User::isSA()) {
            return true;
        }
        
        if (User::can('read_all_task')) {
            return true;
        }
        
        // check if teh user is the creator or the assignee
        if (($task['creator_id'] == $user_id) || ($task['assignee_id'] == $user_id)) {
            return true;
        }
        
        return false;
    }


    public static function getTaskListCreator($user_id) {
        global $con;
        
        $sql = "SELECT * FROM tasks WHERE creator_id = {$user_id} AND status IN (0,1) ORDER BY id DESC";
        
        return $con->fetchAll($sql);
    }
    
    public static function getTaskListAssignee($user_id) {        
        global $con;
        
        $sql = "SELECT * FROM tasks WHERE assignee_id = {$user_id} AND status IN (0,1) ORDER BY id DESC";
        
        return $con->fetchAll($sql);
    }
    
}
?>
