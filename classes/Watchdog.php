<?php

/**
 * Description of Watchdog
 * <p>Note that the watchdog has been online as from 2020-01-28</p>
 * @author Faardeen Madarbokas
 */
class Watchdog {
    
    /**
     * $_SESSION['opsearch_user'] => user object
     * $_SESSION['opsearch_user']['teamleader'] => TL object of the user
     */
    
    /**
     * 
     * @param Array $log <p>Array of log data</p>
     * @param String module_name <p>module name Varchar 255</p>
     * @param String module_code <p>module code Varchar 255</p>
     * @param String action <p>action name Varchar 255</p>
     * @param String action_code <p>action code Varchar 255</p>
     * @param String table_name <p>table name Varchar 255</p>
     * @param Int table_id <p>table id Int 11</p>
     * @param String content_type <p>Content Type Varchar 50 ex json, text, int, double, float</p>
     * @param String content_before <p>Content Before Text</p>
     * @param String content_after <p>Content After Text</p>
     * @param Int status_before_id <p>Status Before Id Int</p>
     * @param String status_before <p>Status Before Varchar 100</p>
     * @param Int status_after_id <p>Status After Id Int</p>
     * @param String status_after <p>Status After Varchar 100</p>
     * @return boolean True
     */
    public static function Log ($log) {
        global $con;
        
        if (!WATCH || User::isSA()) { // do not log if user is Super Admin and WATCH is set to false
            return true;
        } 
        
        $user_data = array();
        
        if (isset($_SESSION['opsearch_user'])) {
            $user = $_SESSION['opsearch_user'];
            
            $user_data = array(
                'user_id' => $user['id'],
                'user_name' => mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName']),
                'user_role_id' => $user['role_id'],
                'user_tl_id' => $user['user_id'],
            );
            
        }
        
        $user_data['created'] = dateToDb(); 
        
        if (isset($user['ip'])) {
            $user_data['user_ip'] = $user['ip'];
        } else {
            $user_data['user_ip'] = getUserIp();
        }
        
        if (isset($user['role_name']))
            $user_data['user_role_name'] = $user['role_name'];
        
        if (isset($user['teamleader'])) {
            $user_data['user_tl_name'] = $user['teamleader']['id'];
            $user_data['user_tl_role_id'] = $user['teamleader']['role_id'];
            $user_data['user_tl_name'] = mb_ucfirst($user['teamleader']['firstName']) . ' ' . mb_strtoupper($user['teamleader']['lastName']);
            $user_data['user_tl_role_name'] = $user['teamleader']['role_name'];
        }
        
        $logdata = array_merge($log, $user_data);
        
        $con->insert('watchdog', $logdata);
        
        return true;
        
    }
    
    public static function WatchCandidat($candidat_id) {
        global $con;
        
        $sql = "SELECT * FROM watchdog WHERE module_code = 'mod_candidat' AND (table_parent_id = {$candidat_id} OR table_id = {$candidat_id}) ORDER BY id DESC";
        
        return $con->fetchAll($sql);
        
    }
    
    public static function WatchEntreprise($entreprise_id) {
        global $con;
        
        $sql = "SELECT * FROM watchdog WHERE module_code = 'mod_entreprise' AND table_name = 'candidatscompanies' AND table_id = {$entreprise_id} ORDER BY id DESC";
        
        return $con->fetchAll($sql);
        
    }
    
    public static function getById($id) {
        global $con;
        
        $sql = "SELECT * FROM watchdog WHERE id = {$id}";
        
        return $con->fetchAssoc($sql);
        
    }
    
    public static function timer($timer) {
        global $con;
        
        // get manager id adn client id of mission
        $mission = Mission::getMissionById($timer['mission_id']);
        $timer['manager_id'] = $mission['manager_id'];
        $timer['client_id'] = $mission['client_id'];
        
        // get user 
        if (!isset($timer['user_id'])) {
            $user = $_SESSION['opsearch_user'];
            $timer['user_id'] = $user['id'];
        }

        $con->insert('kpi_candidat_status_time', $timer);        
    }
    
    public static function timer_in($timer) {
        global $con;

        $con->insert('kpi_time_in', $timer);        
    }
    
    public static function getMissionsStatus() {
        global $con;
        $sql = 'SELECT mission_id FROM watchdog WHERE action_code = "mod_tdc_status_candidat" AND module_code = "mod_tdc" GROUP BY mission_id ORDER BY mission_id';
        return $con->fetchAll($sql);
    }
    
    public static function getTDCStatus() {
        global $con;
        $sql = 'SELECT user_id, mission_id, candidat_id, status_before_id, status_before, status_after_id, status_after, created FROM watchdog WHERE action_code = "mod_tdc_status_candidat" AND module_code = "mod_tdc" AND candidat_id > 0 ORDER BY mission_id, candidat_id';
        return $con->fetchAll($sql);
    }
    
    public static function getLastRecordTimerIn() {
        global $con;
        $sql = "SELECT MAX(watchdog_id) as watchdog_id FROM kpi_time_in";
        $result = $con->fetchAssoc($sql);
        return $result['watchdog_id'];
    }
    
    public static function getTDCRencontreIN($last_record_id = null) {
        
        global $con;
        
        if ($last_record_id <> "") {
            $query = " AND W.id > {$last_record_id} ";
        } else {
            $query = " ";
        }
        
        $sql = "SELECT W.id as watchdog_id, W.user_id, W.mission_id, W.candidat_id, W.status_before_id, W.status_before, W.status_after_id, W.status_after, W.created as created_in, M.manager_id, M.prestataire_id, M.client_id, T.date_rencontre_consultant, T.date_rencontre_consultant_added, T.date_rencontre_client, T.date_rencontre_client_added "
            . "FROM watchdog W "
            . "LEFT JOIN missions M ON M.id = W.mission_id "
            . "LEFT JOIN tdcsuivis T ON T.mission_id = W.mission_id AND T.candidat_id = W.candidat_id "
            . "WHERE W.action_code = 'mod_tdc_status_candidat' AND W.module_code = 'mod_tdc' AND W.candidat_id > 0 AND W.status_after_id = 229 " . $query
            . "ORDER BY W.mission_id, W.candidat_id";
        
        return $con->fetchAll($sql);
        
    }
    
    public static function getGroupedMissionCandidat() {
        global $con;
        $sql = 'SELECT mission_id, candidat_id FROM kpi_candidat_status_time GROUP BY mission_id, candidat_id';
        return $con->fetchAll($sql);
    }
    
    public static function getList_KPI_IN_TIME() {
        global $con;
        $sql = 'SELECT * FROM kpi_time_in GROUP BY mission_id, candidat_id';
        return $con->fetchAll($sql);
    }
    
    public static function updateTimerIn($data) {
        global $con;
        $con->update('kpi_time_in', $data, array('id' => $data['id']));
    }
    
    public static function getMissionCandidat($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT * FROM kpi_candidat_status_time WHERE mission_id = {$mission_id} AND candidat_id = {$candidat_id}";
        return $con->fetchAll($sql);
    }
    
    public static function updateTimerARC($id, $time) {
        global $con;
        $con->update('kpi_candidat_status_time', array('t_arc' => $time), array('id' => $id));
    }
    
    /**
     * START OF AUDIT
     */
    public static $audit_modules = array(
        'mod_candidat' => 'Candidats',
        'mod_entreprise' => 'Entreprises',
        'mod_client' => 'Clients',
        'mod_event' => 'Événements',
        'mod_mission' => 'Missions',
        'mod_tdc' => 'TDC',
        'mod_user' => 'Utilisateurs',
        'mod_mail' => 'Mail',
    );
    
    public static $audit_actions = array(
        'mod_candidat' => array(
            'mod_candidat_create_candidat' => 'Ajouter Candidat',
            'mod_candidat_create_candidat_comment' => 'Ajouter Candidat Commentaire',
            'mod_candidat_create_candidat_document' => 'Ajouter Candidat Document',
            'mod_candidat_create_candidat_langue' => 'Ajouter Candidat Langue',
            'mod_candidat_create_candidat_link' => 'Ajouter Candidat Réseaux sociaux',
            'mod_candidat_edit_candidat' => 'Éditer Candidat',
            'mod_candidat_edit_candidat_comment' => 'Éditer Candidat commentaire',
            'mod_candidat_delete_candidat_comment' => 'Supprimer Candidat commentaire',
            'mod_candidat_delete_candidat_document' => 'Supprimer Candidat Document',
            'mod_candidat_delete_candidat_link' => 'Supprimer Candidat Réseaux sociaux',
            'mod_candidat_create_candidat_motivation' => 'Ajouter Candidat Motivation',
            'mod_candidat_edit_candidat_motivation' => 'Éditer Candidat Motivation',
            'mod_candidat_delete_candidat_motivation' => 'Supprimer Candidat Motivation',
        ),
        'mod_entreprise' => array(
            'mod_entreprise_create_entreprise' => 'Ajouter Entreprise',
            'mod_entreprise_create_entreprise_from_candidat_create' => 'Ajouter Entreprise Creation Candidat',
            'mod_entreprise_create_entreprise_from_candidat_edit' => 'Ajouter Entreprise Edition Candidat',
            'mod_entreprise_edit_entreprise' => 'Éditer Entreprise',
            'mod_entreprise_create_entreprise_comment' => 'Ajouter Entreprise Commentaire',
            'mod_entreprise_edit_entreprise_comment' => 'Éditer Entreprise commentaire',
            'mod_candidat_delete_entreprise_comment' => 'Supprimer Entreprise Commentaire',
            'mod_entreprise_create_entreprise_document' => 'Ajouter Entreprise Document',
            'mod_candidat_delete_entreprise_document' => 'Supprimer Entreprise Document',
            'mod_entreprise_blacklister_entreprise' => 'Deblacklister Entreprise',
            'mod_entreprise_deblacklister_entreprise' => 'Blacklister Entreprise',
        ),
        'mod_user' => array(
            'mod_user_create_user' => 'Ajouter Utilisateur',
            'mod_user_edit_user' => 'Éditer Utilisateur',
            'mod_user_login' => 'Login',
            'mod_user_logout' => 'Logout',
            'mod_user_reset_pass_request' => 'Réinitialiser mot de passe',
            'mod_user_reset_pass' => 'Changement mot de passe Réinitialisation',
            'mod_user_create_user_client' => 'Ajouter Utilisateur Client',
            'mod_user_edit_user_client' => 'Éditer Utilisateur Client',
            'mod_user_assign_user_client_to_mission' => 'Assigner Utilisateur Client Mission',
            'mod_user_send_email_client_account_creation' => 'Envoyer Mail Utilisateur Client Creation Compte',
            'mod_user_send_email_client_account_modification' => 'Envoyer Mail Utilisateur Client Modification Compte',
            'mod_user_send_email_client_assign_mission' => 'Envoyer Mail Utilisateur Client Assigner Mission',
            'mod_user_send_email_client_deassign_mission' => 'Envoyer Mail Utilisateur Client Deassigner Mission',
            'mod_user_delete_mission_user_client' => 'Supprimer Mission Utilisateurs Client',
            'mod_user_modify_pass_my_profile' => 'Modifier Mot de passe mon profile'
        ),
        'mod_mission' => array(
            'mod_mission_create_mission' => 'Ajouter mission',
            'mod_mission_create_mission_comment' => 'Ajouter mission commentaire',
            'mod_mission_create_mission_critere' => 'Ajouter mission criteres',
            'mod_mission_create_mission_document' => 'Ajouter mission document',
            'mod_mission_create_mission_outcategory' => 'Ajouter mission out categorie',
            'mod_mission_create_mission_user' => 'Ajouter mission utilisateurs',
            'mod_mission_edit_mission' => 'Éditer mission',
            'mod_mission_edit_mission_comment' => 'Éditer mission commentaire',
            'mod_mission_edit_mission_critere' => 'Éditer mission criteres',
            'mod_mission_edit_mission_outcategory' => 'Éditer mission out categorie',
            'mod_mission_delete_mission_comment' => 'Supprimer mission commentaire',
            'mod_mission_delete_mission_critere' => 'Supprimer mission criteres',
            'mod_mission_delete_mission_document' => 'Supprimer mission document',
            'mod_mission_delete_mission_outcategory' => 'Supprimer mission out categorie',
            'mod_mission_delete_mission_user' => 'Supprimer mission utilisateurs',
            'mod_mission_duplicate_mission' => 'Dupliquer mission',
            'mod_mission_duplicate_mission_create_outcategory' => 'Dupliquer : Ajouter mission out categorie',
            'mod_mission_duplicate_mission_create_critere' => 'Dupliquer : Ajouter mission criteres',
            'mod_mission_duplicate_mission_create_comment' => 'Dupliquer : Ajouter mission commentaire',
            'mod_mission_duplicate_mission_create_user' => 'Dupliquer : Ajouter mission utilisateurs',
            'mod_mission_duplicate_mission_create_document' => 'Dupliquer : Ajouter mission document',
            'mod_mission_delete' => 'Supprimer Mission',
        ),
        'mod_tdc' => array(
            'mod_tdc_add_candidat' => 'TDC Ajouter Candidat',
            'mod_tdc_add_entreprise' => 'TDC Ajouter Entreprise',
            'mod_tdc_add_historique' => 'TDC Ajouter Historique',
            'mod_tdc_ccc_candidat' => 'TDC CCC Candidat',
            'mod_tdc_edit_historique' => 'TDC Éditer Historique',
            'mod_tdc_status_candidat' => 'TDC MAJ Candidat Statut',
            'mod_tdc_status_entreprise' => 'TDC MAJ Entreprise Statut',
            'mod_tdc_update_candidat_suivi' => 'TDC MAJ Candidat Suivi',
            'mod_tdc_add_criteres' => 'TDC Ajouter Critères spécifiques',
            'mod_tdc_update_criteres' => 'TDC MAJ Critères spécifiques',
            'mod_tdc_add_outcategory' => 'TDC Ajouter Out Categorie',
            'mod_tdc_outcategory' => 'TDC MAJ Out categorie',
            'mod_tdc_delete_candidat' => 'TDC Supprimer Candidat',
            'mod_tdc_delete_entreprise' => 'TDC Supprimer Entreprise',
            'mod_tdc_add_historique' => 'TDC Ajouter Historique',
            'mod_tdc_add_commentaire_entreprise' => 'TDC Ajouter Commentaire Entreprise',
            'mod_tdc_edit_historique' => 'TDC Éditer Historique',
            'mod_tdc_edit_commentaire_entreprise' => 'TDC Éditer Commentaire Entreprise',
            'mod_tdc_delete_historique' => 'TDC Supprimer Historique',
            'mod_tdc_send_sms' => 'TDC Envoie SMS',
            'mod_tdc_add_suivi' => 'TDC Ajouter Suivi',
            'mod_tdc_update_entreprise_suivi' => 'TDC MAJ Entreprise Suivi',
            'mod_tdc_create_client_comment' => 'Ajouter TDC Client Commentaire',
            'mod_tdc_visibilite_client_candidat' => 'TDC Visibilite Client Candidat'
        ),
        'mod_event' => array(
            'mod_event_create_event' => 'Ajouter événement',
            'mod_event_edit_event' => 'Éditer événement',
            'mod_event_delete_event' => 'Supprimer événement',
        ),
        'mod_client' => array(
            'mod_client_create_client' => 'Ajouter Client',
            'mod_client_edit_client' => 'Éditer Client',
            'mod_client_deblacklister_client' => 'Deblacklister Client',
            'mod_client_blacklister_client' => 'Blacklister Client',
        ),
        'mod_mail' => array(
            'mod_mail_send_mail_forget_pass' => 'Envoie Mail Réinitialiser mot de passe',
            'mod_mail_send_mail_comment_client_tdc' => 'Envoie Mail Commentaire Client TDC',
            'mod_mail_send_mail_comment_manager_tdc' => 'Envoie Mail Commentaire Manager TDC',
            'mod_mail_send_mail_visibilite_candidat' => 'Envoie Mail Visibilite Candidat TDC',
            'mod_mail_send_mail_change_status_candidat' => 'Envoie Mail Changement Statut Candidat TDC',
            'mod_mail_send_mail_edit_candidat' => 'Envoie Mail Edition Candidat TDC',
            'mod_mail_send_mail_add_doc_candidat' => 'Envoie Mail Nouveau Document Candidat TDC',
            'mod_mail_send_mail_suivi_candidat' => 'Envoie Mail Suivi Candidat TDC',
        ),
    );
    
    public static function getByModuleCodeActionCode($module_code, $action_code, $start_date, $end_date, $user_id) {
        global $con;
                
        $query = " ";
        
        if ($start_date <> "") {
            $start_date = $start_date . " 00:00:00";
            $query .= " AND created >= '{$start_date}' ";
        }
        
        if ($end_date <> "") {
            $end_date = $end_date . " 23:59:59";
            $query .= " AND created <= '{$end_date}' ";
        }
        
        if ($user_id <> "") {
            $query .= " AND user_id = {$user_id} ";
        }
                
        $sql = "SELECT * FROM watchdog WHERE module_code = '{$module_code}' AND action_code = '{$action_code}' " . $query . " ORDER BY id DESC";
                
        return $con->fetchAll($sql);
    }
    
    public static function getByUser($user_id, $start_date, $end_date) {
        global $con;
                
        $query = " ";
        
        if ($start_date <> "") {
            $start_date = $start_date . " 00:00:00";
            $query .= " AND created >= '{$start_date}' ";
        }
        
        if ($end_date <> "") {
            $end_date = $end_date . " 23:59:59";
            $query .= " AND created <= '{$end_date}' ";
        }
        
        if ($user_id <> "") {
            $query .= " AND user_id = {$user_id} ";
        }
                
        $sql = "SELECT * FROM watchdog WHERE 1=1 " . $query . " ORDER BY id DESC";
                
        return $con->fetchAll($sql);
    }
        
}
