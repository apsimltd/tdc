<?php
class Client {
    
    public static function addClient($client) {
        global $con;          
        
        $con->insert('clients', $client);
        
        $id = $con->lastInsertId();
        
        //watchdog
        $log  = array(
            'module_name' => 'Client',
            'module_code' => 'mod_client',
            'action' => 'Ajouter Client',
            'action_code' => 'mod_client_create_client',
            'table_name' => 'clients',
            'table_id' => $id,
            'content_type' => 'json',
            'content_before' => json_encode($client),
        );
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function SearchCandidatCompany($str) {
        global $con;
        $sql = "SELECT id as value, name as label FROM candidatscompanies WHERE name LIKE \"{$str}%\"";
        $results = $con->fetchAll($sql);
        return json_encode($results);
    }
    
    public static function SearchCandidatCompanyMother($str) {
        global $con;
        $sql = "SELECT id as value, name as label FROM candidatscompanies WHERE name LIKE \"{$str}%\" AND is_mother = 1";
        $results = $con->fetchAll($sql);
        return json_encode($results);
    }
    
    public static function checkCandidatCompanyExist($company_name, $id = null) {
        
        global $con;
        
        if ($id) {
            $sql = "SELECT * FROM candidatscompanies WHERE name = \"{$company_name}\" AND id <> {$id}";
        } else {
            $sql = "SELECT * FROM candidatscompanies WHERE name = \"{$company_name}\"";
        }
        
        
        $result = $con->fetchAssoc($sql);
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
        
    }
    
    public static function getClientCandidatBlacklist($clientName) {
        global $con;
                
        $sql = 'SELECT * FROM clients WHERE nom = "'. $clientName . '" AND blacklist = 1';
                
        $result = $con->fetchAssoc($sql);
        
        if (empty($result)) {
            
            $sql = 'SELECT * FROM candidatscompanies WHERE blacklist = 1 AND name = "'. $clientName . '"';
            $result = $con->fetchAssoc($sql);
            if (empty($result)) {
                return false;
            } else {
                return true;
            }
            return false;
            
        } else {
            return true;
        }
        
    }
    
    public static function getClientsBlacklister($order = null) {
        global $con;
        $sql = "SELECT * FROM clients WHERE blacklist = 1 ORDER BY ";
        if ($order == 'nom') {
            $sql .= "clients.nom ASC";
        } else {
            $sql .= "clients.id ASC";
        }
        return $con->fetchAll($sql);
    }
    
    public static function getClientsBlacklisterByName($nom) {
        global $con;
        $sql = "SELECT * FROM clients WHERE blacklist = 1 AND nom LIKE '%{$nom}%' ORDER BY nom ASC";
        return $con->fetchAll($sql);
    }
    
    public static function getClients($order = null) {
        global $con;
        $sql = "SELECT * FROM clients ORDER BY ";
        if ($order == 'nom') {
            $sql .= "clients.nom ASC";
        } else {
            $sql .= "clients.id ASC";
        }
        return $con->fetchAll($sql);
    }
    
    public static function getClientById($id) {
        global $con;
        $sql = "SELECT * FROM clients WHERE id = {$id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function EditClient($client) {
        global $con;
        
        $content_before = self::getClientById($client['id']);
        
        $con->update('clients', $client, array('id' => $client['id']));
        
        // watchdog
        $log  = array(
            'module_name' => 'Client',
            'module_code' => 'mod_client',
            'action' => 'Éditer Client',
            'action_code' => 'mod_client_edit_client',
            'table_name' => 'clients',
            'table_id' => $client['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($client),
        );
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function blacklist($client_id, $data) {
        global $con;
        
        $con->update('clients', $data, array('id' => $client_id));
        
        // get company/client name by id
        $client = self::getClientById($client_id);
        if (!empty($client)) {
            // we update the candidats companies as well
            $con->update('candidatscompanies', array('blacklist' => $data['blacklist']), array('name' => $client['nom']));
        }
        
        // watchdog
        $log  = array(
            'module_name' => 'Client',
            'module_code' => 'mod_client',
            'table_name' => 'clients',
            'table_id' => $client_id,
        );
        
        if ($data['blacklist'] == 0) {
            $log['action'] = 'Deblacklister Client';
            $log['action_code'] = 'mod_client_deblacklister_client';
        } else if ($data['blacklist'] == 1) {
            $log['action'] = 'Blacklister Client';
            $log['action_code'] = 'mod_client_blacklister_client';
        }
        
        Watchdog::Log($log);
        
        return true;
    }
    
}
