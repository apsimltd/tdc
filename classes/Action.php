<?php

/**
 * Description of Permission
 *
 * @author faardeen
 */
class Action extends Role {

    public static function getModule() {
        global $con;
        $sql = "SELECT * FROM modules ORDER BY id ASC";
        return $con->fetchAll($sql);
    }

    public static function getModuleDescByName($name) {
        global $con;
        $sql = "SELECT * FROM modules WHERE name = '{$name}'";
        return $con->fetchAssoc($sql);
    }
    
    public static function getActionGroupByModule() {
        global $con;
        $modules = self::getModule();
        $actions = array();
        foreach ($modules as $module):
            $sql = "SELECT * FROM modulesactions WHERE "
                    . "module_id = {$module['id']} ORDER BY id ASC";
            $actions[$module['name']] = $con->fetchAll($sql);
        endforeach;
        return $actions;
    }

    public static function assignAction($action) {
        global $con;
        $con->insert('rolesactions', $action);
        
        //watchdog
        $log  = array(
            'module_name' => 'Autorisations',
            'module_code' => 'mod_perm',
            'action' => 'Attribuer Autorisations',
            'action_code' => 'mod_perm_assign_action',
            'table_name' => 'rolesactions',
            'content_type' => 'json',
            'content_before' => json_encode($action),
        );
        Watchdog::Log($log);
        
    }

    public static function unassignAction($action) {
        global $con;
        $con->delete('rolesactions', $action);
        
        //watchdog
        $log  = array(
            'module_name' => 'Autorisations',
            'module_code' => 'mod_perm',
            'action' => 'Désaffecter Autorisations',
            'action_code' => 'mod_perm_deassign_action',
            'table_name' => 'rolesactions',
            'content_type' => 'json',
            'content_before' => json_encode($action),
        );
        Watchdog::Log($log);
    }

    public static function assignActionModule($action) {
        global $con;
        $module_id = $action['module_id'];
        $role_id = $action['role_id'];
        // delete all actions for the module chosen
        $con->delete('rolesactions', array('module_id' => $module_id, 'role_id' => $role_id));

        // get all actions id by module id
        $action_ids = self::getActionsIdsByModuleId($module_id);
        foreach ($action_ids as $action_id):
            self::assignAction(array('role_id' => $role_id, 'action_id' => $action_id, 'module_id' => $module_id));
        endforeach;
    }

    public static function unassignActionModule($action) {
        global $con;
        $module_id = $action['module_id'];
        $role_id = $action['role_id'];
        // delete all actions for the module chosen
        $con->delete('rolesactions', array('module_id' => $module_id, 'role_id' => $role_id));
    }

    public static function assignActionRole($data) {
        global $con;
        // delete all actions for this role
        $con->delete('rolesactions', array('role_id' => $data['role_id']));

        // get all actions id
        $actions = self::getActions();
        foreach ($actions as $action):
            self::assignAction(array('role_id' => $data['role_id'], 'action_id' => $action['action_id'], 'module_id' => $action['module_id']));
        endforeach;
    }

    public static function unassignActionRole($data) {
        global $con;
        // delete all actions for this role
        $con->delete('rolesactions', array('role_id' => $data['role_id']));
    }

    public static function getActions() {
        global $con;
        $sql = "SELECT * FROM modulesactions ORDER BY id ASC";
        $actions = $con->fetchAll($sql);
        $perms = array();
        foreach ($actions as $action):
            $perms[] = array(
                'action_id' => $action['id'],
                'module_id' => $action['module_id'],
            );
        endforeach;
        return $perms;
    }

    public static function getActionsIdsByModuleId($module_id) {
        global $con;
        $sql = "SELECT id FROM modulesactions WHERE module_id = {$module_id} ORDER BY id ASC";
        $action_ids = $con->fetchAll($sql);
        $act_ids = array();
        foreach ($action_ids as $action_id):
            $act_ids[] = $action_id['id'];
        endforeach;
        return $act_ids;
    }

    public static function checkAssignedAction($role_id, $action_id) {
        global $con;
        $sql = "SELECT * FROM rolesactions WHERE role_id = {$role_id} AND action_id = {$action_id}";
        $action = $con->fetchAll($sql);
        if (count($action) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function checkAssignedActionByModule($role_id, $module_id) {
        global $con;
        // get all actions id by module id
        $action_ids = self::getActionsIdsByModuleId($module_id);
        // get all actions assigned by this role
        $sql = "SELECT * FROM rolesactions WHERE role_id = {$role_id} AND module_id = {$module_id}";
        $action = $con->fetchAll($sql);
        // check if the number of records matches
        if (count($action_ids) == count($action)) {
            return true;
        } else {
            return false;
        }
    }

    public static function checkAssignedActionByRole($role_id) {
        global $con;
        // get all actions 
        $actions = self::getActions();
        // get all actions assigned by this role
        $sql = "SELECT * FROM rolesactions WHERE role_id = {$role_id}";

        $perm = $con->fetchAll($sql);
        // check if the number of records matches
        if (count($actions) == count($perm)) {
            return true;
        } else {
            return false;
        }
    }

}
