<?php
class Candidat {
    
    public static $fields_label = array(
        'id' =>  'Identifiant',
        'candidat_id' =>  'Identifiant Candidat',
        'user_id' =>  'Identifiant Utilisateur',
        'control_civilite_id' =>  'Civilite',
        'control_localisation_id' =>  'Localisation',
        'nom' =>  'Nom',
        'prenom' =>  'Prénom',
        'tel_standard' =>  'Téléphone standard',
        'tel_ligne_direct' =>  'Téléphone ligne directe',
        'tel_pro' =>  'Téléphone mobile pro',
        'tel_mobile_perso' =>  'Téléphone mobile perso',
        'tel_domicile' =>  'Téléphone domicile',
        'email' =>  'Email 1',
        'email2' =>  'Email 2',
        'dateOfBirth' =>  'Date de naissance',
        'societe_actuelle' =>  'Société actuelle',
        'company_id' =>  'Identifiant Société',
        'company_blacklisted' =>  'Société blacklister',
        'poste' =>  'Poste',
        'control_secteur_id' =>  'Secteur',
        'control_fonction_id' =>  'Fonction',
        'control_langue_id' => 'Langues',
        'remuneration' =>  'En Package salarial (K€)',
        'detail_remuneration' =>  'Détails primes / variable',
        'salaire_package' =>  'Salaire Package (K€)',
        'salaire_fixe' =>  'Salaire Fixe (K€)',
        'sur_combien_de_mois' =>  'Sur Combien de Mois',
        'avantages' =>  'Avantages en nature',
        'remuneration_souhaitee' =>  'Rémunération Souhaitée',
        'control_source_id' =>  'Source',
        'reseauSociaux_id' =>  'reseauSociaux_id',
        'control_niveauFormation_id' =>  'Niveau Formation',
        'diplome_specialisation' =>  'Diplôme / spécialisation',
        'dateDiplome' =>  'Date d\'obtention du diplôme',
        'cv_fourni' =>  'CV fourni',
        'documents' =>  'Documents',
        'tres_bon_candidat' =>  'Très bon candidat',
        'status' =>  'Statut',
        'created' =>  'Date de création',
        'modified' =>  'Date de modification',
        'comment' => 'Commentaire',
        'link' => 'Réseaux sociaux',
        'file' => 'Document',
        'mission_id' => 'Mission ID',
        'motivation' => 'Motivation',
    );
    
    public static $fields_label_entreprise = array(
        'firstName' => 'Prénom',
        'lastName' => 'Nom',
        'name' => 'Nom',
        'entreprise_id' => 'Identifiant Entreprise',
        'secteur_activite_id' => 'Identifiant Secteur Activite',
        'sous_secteur_activite' => 'Sous Secteur Activité',
        'telephone' => 'Téléphone',
        'region' => 'Région',
        'secteur_activite' => 'Secteur',
        'id' =>  'Identifiant',
        'email' =>  'Email',
        'user_id' =>  'Identifiant Utilisateur',
        'status' =>  'Statut',
        'created' =>  'Date de création',
        'modified' =>  'Date de modification',
        0 => array(
            'firstName' => 'Prénom',
            'lastName' => 'Nom',
            'name' => 'Nom',
            'entreprise_id' => 'Identifiant Entreprise',
            'secteur_activite_id' => 'Identifiant Secteur Activite',
            'sous_secteur_activite' => 'Sous Secteur Activité',
            'telephone' => 'Téléphone',
            'region' => 'Région',
            'secteur_activite' => 'Secteur',
            'id' =>  'Identifiant',
            'email' =>  'Email',
            'user_id' =>  'Identifiant Utilisateur',
            'status' =>  'Statut',
            'created' =>  'Date de création',
            'modified' =>  'Date de modification',
        ),
    );
    
    /**
     * start of candidats entreprise
     * 
     */
    
    public static function getEntrepriseMeres() {
        global $con;
        $sql = "SELECT * FROM candidatscompanies WHERE is_mother = 1 ORDER BY name ASC";
        return $con->fetchAll($sql);
    }
    
    public static function checkCompanyMix($name) {
        global $con;
        $sql = "SELECT * FROM candidatscompanies WHERE name = \"{$name}\"";
        $company = $con->fetchAssoc($sql);
        if (!empty($company)) {
            if ($company['parent_id'] > 0 || $company['is_mother'] > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public static function getEntrepriseBlacklistByName($name, $return_id = false) {
        global $con;
                
        $sql = 'SELECT * FROM candidatscompanies WHERE blacklist = 1 AND name = "'. $name . '"';
                        
        $result = $con->fetchAssoc($sql);
        
        if ($return_id) {
            return $result['id'];
        }
        
        if (empty($result)) {
            return false;
        } else {
            return true;
        }
        
    }
    
    public static function getEntrepriseBlacklist($entreprise_id, $return_id = false) {
        global $con;
                
        $sql = "SELECT * FROM candidatscompanies WHERE blacklist = 1 AND id = {$entreprise_id}";
                
        $result = $con->fetchAssoc($sql);
        
        if ($return_id) {
            return $result['id'];
        }
        
        if (empty($result)) {
            return false;
        } else {
            return true;
        }
        
    }
    
    public static function EntrepriseBlacklist($entreprise_id, $data) {
        global $con;
        
        $con->update('candidatscompanies', $data, array('id' => $entreprise_id));
        
        // we need to blacklist the client as well
        // get entreprise name
        $entreprise = self::getEntrepriseById($entreprise_id);
        if (!empty($entreprise)) {
            // we update the candidats companies as well
            $con->update('clients', array('blacklist' => $data['blacklist']), array('name' => $entreprise['name']));
        }        
        
        // we need to blacklist and unblacklist the sous entreprises if its mother
        $company = self::getEntrepriseByIdNew($entreprise_id);
        if ($company['is_mother'] == 1) {
            $con->update('candidatscompanies', $data, array('parent_id' => $entreprise_id));
        }
        
        // watchdog
        $log  = array(
            'module_name' => 'Entreprise',
            'module_code' => 'mod_entreprise',
            'table_name' => 'candidatscompanies',
            'table_id' => $entreprise_id,
        );
        
        if ($data['blacklist'] == 0) {
            $log['action'] = 'Deblacklister Entreprise';
            $log['action_code'] = 'mod_entreprise_deblacklister_entreprise';
        } else if ($data['blacklist'] == 1) {
            $log['action'] = 'Blacklister Entreprise';
            $log['action_code'] = 'mod_entreprise_blacklister_entreprise';
        }
        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function getEntrepriseMere() {
        global $con;
        ///$sql = "SELECT * FROM candidatscompanies WHERE (parent_id = 0 OR parent_id IS NULL) ORDER BY name ASC";
        $sql = "SELECT * FROM candidatscompanies WHERE parent_id = 0 ORDER BY name ASC";
        return $con->fetchAll($sql);
    }
    
    public static function CheckExistanceEntreprise($name) {
        global $con;
        
        if ($name == "") {
            return false;
        } elseif ($name != "") {
            $sql = "SELECT * FROM candidatscompanies WHERE name = \"{$name}\"";
        }
                 
        $result = $con->fetchAssoc($sql);
        if (empty($result)) {
            return false;
        } else {
            return true;
        }
    }
    
    public static function addEntrepriseNew($entreprise, $commentaire, $files) {
        global $con;
        
        // get secteur activite name
        if ($entreprise['secteur_activite_id'] > 0) {
            $secteur = Control::getControlById($entreprise['secteur_activite_id']);
            $entreprise['secteur_activite'] = $secteur['name'];
        }
        
        if ($entreprise['control_localisation_id'] > 0) {
            $localisation = Control::getControlById($entreprise['control_localisation_id']);
            $entreprise['control_localisation'] = $localisation['name'];
        }
        
        // check if the company is sous entreprise and chekc if the mother company is blacklister
        // however if sous entreprise is blacklister not necessary parent must be blacklister
        if ($entreprise['parent_id'] > 0) {
            $parent_company = self::getEntrepriseByIdNew($entreprise['parent_id']);
            if ($parent_company['blacklist'] == 1) {
                $entreprise['blacklist'] = 1;
            }
        }
        
        $con->insert('candidatscompanies', $entreprise);
        
        $entreprise_id = $con->lastInsertId();
        
        // if entreprise is parent and is blacklister
        // update all sub companies blacklister to 1
        if (isset($entreprise['is_mother']) && $entreprise['is_mother'] == 1 && $entreprise['blacklist'] == 1) {
            $con->update('candidatscompanies', array('blacklist' => 1), array('parent_id' => $entreprise_id));
        }
                
        //watchdog
        $log  = array(
            'module_name' => 'Entreprise',
            'module_code' => 'mod_entreprise',
            'action' => 'Ajouter Entreprise',
            'action_code' => 'mod_entreprise_create_entreprise',
            'table_name' => 'candidatscompanies',
            'table_id' => $entreprise_id,
            'content_type' => 'json',
            'content_before' => json_encode($entreprise),
        );        
        Watchdog::Log($log);
        
        if ($commentaire != "") {
            self::addCommentEntreprise($entreprise_id, $commentaire);
        }
        
        self::addDocumentsEntreprise($entreprise_id, $files);
        
        return $entreprise_id;
        
    }
    
    public static function getEntreprisesList() {
        global $con;
        $sql = "SELECT * FROM candidatscompanies WHERE status = 1 ORDER BY name ASC";
        return $con->fetchAll($sql);
    }
    
    public static function addCommentEntreprise($entreprise_id, $commentaire) {
        global $con;
        
        // get all existing comment first
        $existing_comments = json_decode(self::getCommentsEntreprise($entreprise_id), true);
                
        if (is_array($existing_comments) && count($existing_comments) > 0) {
            $comments = $existing_comments;
            $comments_before = $comments;
        } else {
            $comments = array();
            $comments_before = $comments;
        }
        
        
        $comments[] = array(
            'user_id' => $_SESSION['opsearch_user']['id'],
            'firstName' => $_SESSION['opsearch_user']['firstName'],
            'lastName' => $_SESSION['opsearch_user']['lastName'],
            'entreprise_id' => $entreprise_id,
            'comment' => $commentaire,
            'status' => 1,
            'created' => dateToDb(),
        );
        
        $con->update('candidatscompanies', array('commentaire' => json_encode($comments)), array('id' => $entreprise_id));
                            
        //watchdog
        $log  = array(
            'module_name' => 'Entreprise',
            'module_code' => 'mod_entreprise',
            'action' => 'Ajouter Entreprise Commentaire',
            'action_code' => 'mod_entreprise_create_entreprise_comment',
            'table_name' => 'candidatscompanies',
            'table_id' => $entreprise_id,
            'content_type' => 'json',
            'content_before' => json_encode($existing_comments),
            'content_after' => json_encode($comments),
        );        
        Watchdog::Log($log);        
        
        return true;
    }
    
    public static function getCommentsEntreprise($entreprise_id) {
        global $con;
        $sql = "SELECT commentaire FROM candidatscompanies WHERE id = {$entreprise_id} AND status = 1";
        $result = $con->fetchAll($sql);
        return $result[0]['commentaire'];
    }
    
    public static function addDocumentsEntreprise($entreprise_id, $files) {
        global $con;
        $numFiles = count($files['name']);
        
        if ($numFiles > 0 && $files['name'][0] != "") {
            // check if the candidat folder exist
            $entreprise_dir = ENTREPRISE_PATH . '/' . $entreprise_id;

            // check if entreprise folder exist
            // if not create it
            if (!file_exists($entreprise_dir)) {
                mkdir($entreprise_dir); // default chmod is 0777
            }
            
            // we get the field files in json format
            $existing_docs = json_decode(self::getDocumentsEntreprise($entreprise_id), true);            
            if (is_array($existing_docs) && count($existing_docs) > 0) {
                $docs = $existing_docs;
                $doc_before = $docs;
            } else {
                $docs = array();
                $doc_before = $docs;
            }
                        
            foreach($files['name'] as $index => $filename):
                
                $file_renamed = $entreprise_id . '_' . date('dmY', time()) . '_' .$filename;
                move_uploaded_file($files['tmp_name'][$index], $entreprise_dir . '/' . $file_renamed); // we append entreprise_id before
                
                $docs[] = array(
                    'user_id' => $_SESSION['opsearch_user']['id'],
                    'entreprise_id' => $entreprise_id,
                    'file' => $file_renamed, 
                    'status' => 1,
                    'created' => dateToDb(),
                );    
                    
                // update the field documents json_encode in table candidats
                $con->update('candidatscompanies', array('files' => json_encode($docs)), array('id' => $entreprise_id));
                
                //watchdog
                $log  = array(
                    'module_name' => 'Entreprises',
                    'module_code' => 'mod_entreprise',
                    'action' => 'Ajouter Entreprise Document',
                    'action_code' => 'mod_entreprise_create_entreprise_document',
                    'table_name' => 'candidatscompanies',
                    'table_id' => $entreprise_id,
                    'content_type' => 'json',
                    'content_before' => json_encode($doc_before),
                    'content_after' => json_encode($docs),
                );        
                Watchdog::Log($log);
                
            endforeach;
                  
        }        
        return true;    
        
    }
    
    public static function getDocumentsEntreprise($entreprise_id) {
        global $con;
        $sql = "SELECT files FROM candidatscompanies WHERE id = {$entreprise_id}";
        $result = $con->fetchAll($sql);
        return $result[0]['files'];
    }
    
    public static function getDocumentEntrepriseByKey($entreprise_id, $key) {
        
        $files = json_decode(self::getDocumentsEntreprise($entreprise_id), true);
        $file = $files[$key];
        
        return array(
            'path' => ENTREPRISE_PATH. '/' . $entreprise_id . '/' . $file['file'],
            'content_type' => getContentType(getFileExt($file['file'])),
        );
    }
        
    public static function getEntrepriseByIdNew($id) {
        global $con;
        $sql = "SELECT * FROM candidatscompanies WHERE id = {$id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getChildCompanies($id) {
        global $con;
        global $con;
        $sql = "SELECT C.*, CC.name as mother FROM candidatscompanies C LEFT JOIN candidatscompanies CC ON CC.id = C.parent_id WHERE C.parent_id = {$id}";
        return $con->fetchAll($sql);
    }
    
    public static function getEntrepriseByIdNewNew($id) {
        global $con;
        $sql = "SELECT C.*, CC.name as mother FROM candidatscompanies C LEFT JOIN candidatscompanies CC ON CC.id = C.parent_id WHERE C.id = {$id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function EditEntrepriseNew($data) {
        global $con;
        
        $content_before = self::getEntrepriseByIdNew($data['id']);
        
        // get secteur activite name
        if ($data['secteur_activite_id'] > 0) {
            $secteur = Control::getControlById($data['secteur_activite_id']);
            $data['secteur_activite'] = $secteur['name'];
        }
        
        if ($data['control_localisation_id'] > 0) {
            $localisation = Control::getControlById($data['control_localisation_id']);
            $data['control_localisation'] = $localisation['name'];
        }
        
        // check if the company is sous entreprise and chekc if the mother company is blacklister
        // however if sous entreprise is blacklister not necessary parent must be blacklister
        if ($data['parent_id'] > 0) {
            $parent_company = self::getEntrepriseByIdNew($data['parent_id']);
            if ($parent_company['blacklist'] == 1) {
                $data['blacklist'] = 1;
            }
        }
        
        $con->update('candidatscompanies', $data, array('id' => $data['id']));
        
        // if entreprise is parent and is blacklister
        // update all sub companies blacklister to 1
        if (isset($data['is_mother']) && $data['is_mother'] == 1 && $data['blacklist'] == 1) {
            $con->update('candidatscompanies', array('blacklist' => 1), array('parent_id' => $data['id']));
        }
        
        // watchdog
        $log  = array(
            'module_name' => 'Entreprise',
            'module_code' => 'mod_entreprise',
            'action' => 'Éditer Entreprise',
            'action_code' => 'mod_entreprise_edit_entreprise',
            'table_name' => 'candidatscompanies',
            'table_id' => $data['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($data),
        );
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function deleteDocumentEntreprise($file_key, $entreprise_id) {
        global $con;
        
        // first we get the document name and entreprise id
        // delete the file pysically and then delete record in db
        $sql = "SELECT * FROM candidatscompanies WHERE id = {$entreprise_id}";
        $result = $con->fetchAssoc($sql);
        $docs = json_decode($result['files'], true);
        $doc = $docs[$file_key];
        if (unlink(ENTREPRISE_PATH . '/' . $entreprise_id . '/' . $doc['file'])) {
            
            // we must remove the array of this key and update table candidatscompanies
            unset($docs[$file_key]);
            $con->update('candidatscompanies', array('files' => json_encode($docs)), array('id' => $entreprise_id));
            
            //watchdog
            $log  = array(
                'module_name' => 'Entreprises',
                'module_code' => 'mod_entreprise',
                'action' => 'Supprimer Entreprise Document',
                'action_code' => 'mod_candidat_delete_entreprise_document',
                'table_name' => 'candidatscompanies',
                'table_id' => $entreprise_id,
                'content_type' => 'json',
                'content_before' => json_encode($doc),
            );        
            Watchdog::Log($log);
            
            return true;
        } else {
            return false;
        }
        
    }
    
    public static function deleteCommentEntreprise($key, $entreprise_id) {
        global $con;
        
        // first we get the comment and entreprise id
        // delete the file pysically and then delete record in db
        $sql = "SELECT * FROM candidatscompanies WHERE id = {$entreprise_id}";
        $result = $con->fetchAssoc($sql);
        $comments = json_decode($result['commentaire'], true);
        $comment = $comments[$key];
    
        // we must remove the array of this key and update table candidatscompanies
        unset($comments[$key]);
        $con->update('candidatscompanies', array('commentaire' => json_encode($comments)), array('id' => $entreprise_id));

        //watchdog
        $log  = array(
            'module_name' => 'Entreprises',
            'module_code' => 'mod_entreprise',
            'action' => 'Supprimer Entreprise Commentaire',
            'action_code' => 'mod_candidat_delete_entreprise_comment',
            'table_name' => 'candidatscompanies',
            'table_id' => $entreprise_id,
            'content_type' => 'json',
            'content_before' => json_encode($comment),
        );        
        Watchdog::Log($log);

        return true;
        
    }
    
    public static function getCommentEntrepriseByKey($entreprise_id, $key) {
        
        $comments = json_decode(self::getCommentsEntreprise($entreprise_id), true);
        return $comments[$key];

    }
    
    public static function editCommentEntreprise($comment) {
        global $con;
        
        // get all existing comment first
        $existing_comments = json_decode(self::getCommentsEntreprise($comment['entreprise_id']), true);
                
        if (is_array($existing_comments) && count($existing_comments) > 0) {
            $comments = $existing_comments;
            $comments_before = $comments[$comment['key']];
        } else {
            $comments = array();
            $comments_before = "";
        }
        
        $created = $comments[$comment['key']]['created'];
        
        $comments[$comment['key']] = array(
            'user_id' => $_SESSION['opsearch_user']['id'],
            'firstName' => $_SESSION['opsearch_user']['firstName'],
            'lastName' => $_SESSION['opsearch_user']['lastName'],
            'entreprise_id' => $comment['entreprise_id'],
            'comment' => $comment['comment'],
            'status' => 1,
            'created' => $created,
            'modified' => $comment['modified'],
        );
        
        $con->update('candidatscompanies', array('commentaire' => json_encode($comments)), array('id' => $comment['entreprise_id'])); 
                            
        //watchdog
        $log  = array(
            'module_name' => 'Entreprises',
            'module_code' => 'mod_entreprise',
            'action' => 'Éditer Entreprise commentaire',
            'action_code' => 'mod_entreprise_edit_entreprise_comment',
            'table_name' => 'candidatscompanies',
            'table_id' => $comment['entreprise_id'],
            'content_type' => 'json',
            'content_before' => json_encode($comments_before),
            'content_after' => json_encode($comments[$comment['key']]),
        );        
        Watchdog::Log($log);
        
        return true;
    }
        
    public static function SearchEntreprisesListSearch($data) {
        global $con;
                
        $sql = "SELECT * FROM candidatscompanies WHERE 1=1 ";
        
        if ($data['name'] <> "")
            $sql .= " AND name LIKE '%{$data['name']}%' ";
        
        if ($data['telephone'] <> "") {
            $t = $data['telephone'];
            $sql .= " AND telephone LIKE '%{$t}%' ";
        }    
            
        if ($data['control_localisation_id'] <> "")
            $sql .= " AND control_localisation_id = {$data['control_localisation_id']} ";
            
        if ($data['email'] <> "")
            $sql .= " AND email LIKE '%{$data['email']}%' ";
            
        if ($data['secteur_activite_id'] <> "")
            $sql .= " AND secteur_activite_id = {$data['secteur_activite_id']} ";    
        
        if ($data['sous_secteur_activite'] <> "")
            $sql .= " AND sous_secteur_activite LIKE '%{$data['sous_secteur_activite']}%' ";      
                    
        return $con->fetchAll($sql);
    }
    
    /**
     * end of candidats entreprise
     */
    
    
    /**
     * start of entreprise
     */
    public static function getEntreprisesListSearch($data) {
        global $con;
                
        $sql = "SELECT * FROM entreprises WHERE 1=1 ";
        
        if ($data['nom'] <> "")
            $sql .= " AND nom LIKE '%{$data['nom']}%' ";
        
        if ($data['telephone'] <> "") {
            $t = $data['telephone'];
            $sql .= " AND telephone LIKE '%{$t}%' ";
        }    
            
        if ($data['region'] <> "")
            $sql .= " AND region LIKE '%{$data['region']}%' ";
            
        if ($data['email'] <> "")
            $sql .= " AND email LIKE '%{$data['email']}%' ";
            
        if ($data['secteur_activite'] <> "")
            $sql .= " AND secteur_activite LIKE '%{$data['secteur_activite']}%' ";    
        
        if ($data['sous_secteur_activite'] <> "")
            $sql .= " AND sous_secteur_activite LIKE '%{$data['sous_secteur_activite']}%' ";      
            
        return $con->fetchAll($sql);
    }
    
    
    public static function EditEntreprise($data) {
        global $con;
        
        $content_before = self::getEntrepriseById($data['id']);
        
        $con->update('entreprises', $data, array('id' => $data['id']));
        
        // watchdog
        $log  = array(
            'module_name' => 'Entreprise',
            'module_code' => 'mod_entreprise',
            'action' => 'Éditer Entreprise',
            'action_code' => 'mod_entreprise_edit_entreprise',
            'table_name' => 'entreprises',
            'table_id' => $data['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($data),
        );
        Watchdog::Log($log);
        
        return true;
    }
    
    
    public static function getEntrepriseById($id) {
        global $con;
        $sql = "SELECT * FROM entreprises WHERE id = {$id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function addEntreprise($data) {
        global $con;
                
        $con->insert('entreprises', $data);
        
        $id = $con->lastInsertId();
        
        //watchdog
        $log  = array(
            'module_name' => 'Entreprise',
            'module_code' => 'mod_entreprise',
            'action' => 'Ajouter Entreprise',
            'action_code' => 'mod_entreprise_create_entreprise',
            'table_name' => 'entreprises',
            'table_id' => $id,
            'content_type' => 'json',
            'content_before' => json_encode($data),
        );
        Watchdog::Log($log);
        
        return true;
    }
    /**
     * end of entreprise
     */
    
    public static function getCompanyById($company_id) {
        global $con;
        $sql = "SELECT * FROM candidatscompanies WHERE id = {$company_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function blacklist($company_id, $data) {
        global $con;
        
        $content_before = self::getCompanyById($company_id);
        
        $con->update('candidatscompanies', $data, array('id' => $company_id));
        
        // we need to blacklist the client as well
        // get entreprise name
        $entreprise = self::getEntrepriseById($company_id);
        if (!empty($entreprise)) {
            // we update the candidats companies as well
            $con->update('clients', array('blacklist' => $data['blacklist']), array('name' => $entreprise['name']));
        }  
        
        $content_after = self::getCompanyById($company_id);
        
        // watchdog
        if ($data['blacklist'] == 1) {
            $action_code = 'mod_company_candidat_blacklister';
            $action = 'Blacklister Société Candidat';
        } else {
            $action_code = 'mod_company_candidat_deblacklister';
            $action = 'Deblacklister Société Candidat';
        }
        
        $log  = array(
            'module_name' => 'Société Candidats',
            'module_code' => 'mod_company_candidat',
            'action' => $action,
            'action_code' => $action_code,
            'table_name' => 'candidatscompanies',
            'table_id' => $company_id,
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($content_after),
        );
        Watchdog::Log($log);
        
        // now we update all candidats with the same company id        
        $con->update('candidats', array('company_blacklisted' => $data['blacklist']), array('company_id' => $company_id));
        
        return true;
    }
    
    public static function getCompanies($name) {
        global $con;
        $sql = "SELECT * FROM candidatscompanies WHERE name LIKE \"%{$name}%\" ORDER BY id ASC";
        return $con->fetchAll($sql);
    }
    
    public static function getSuivisEntreprise($entreprise_id) {
        global $con;
        $sql = "SELECT s.*, m.poste, c.nom as client "
            . "FROM tdc s, missions m, clients c "
            . "WHERE s.mission_id = m.id AND c.id = m.client_id AND s.entreprise_id = {$entreprise_id} ORDER BY s.id DESC";
                    
        return $con->fetchAll($sql);
    }
    
    public static function getSuivis($candidat_id) {
        global $con;
        $sql = "SELECT s.*, m.poste, c.nom as client "
            . "FROM tdcsuivis s, missions m, clients c "
            . "WHERE s.mission_id = m.id AND c.id = m.client_id AND s.candidat_id = {$candidat_id} ORDER BY s.id DESC";
                    
        return $con->fetchAll($sql);
    }
    
    public static function getCandidatTDC($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT * FROM tdc WHERE candidat_id = {$candidat_id} AND mission_id = {$mission_id}"; 
        return $con->fetchAssoc($sql);
    }
    
    public static function CheckExistance($email, $email2) {
        global $con;
        
        if ($email == "" && $email2 == "") {
            return false;
        } else if ($email == "" && $email2 != "") {
            $sql = "SELECT * FROM candidats WHERE email = '{$email2}' OR email2 = '{$email2}'";
        } else if ($email != "" && $email2 == "") {
            $sql = "SELECT * FROM candidats WHERE email = '{$email}' OR email2 = '{$email}'";
        } else if ($email != "" && $email2 != "") {
            $sql = "SELECT * FROM candidats WHERE email = '{$email}' OR email = '{$email2}'  OR email2 = '{$email}' OR email2 = '{$email2}'";
        }
                 
        $result = $con->fetchAssoc($sql);
        if (empty($result)) {
            return false;
        } else {
            return true;
        }
    }
    
    public static function addCandidat($candidat, $langues, $commentaire, $reseauSociaux, $files) {
        global $con;
        
        // the check box values cv_fourni and tres_bon_candidat is on when checked
        if ($candidat['cv_fourni'] == 'on') {
            $candidat['cv_fourni'] = 1;
        } else {
            $candidat['cv_fourni'] = 0;
        }
        
        if ($candidat['tres_bon_candidat'] == 'on') {
            $candidat['tres_bon_candidat'] = 1;
        } else {
            $candidat['tres_bon_candidat'] = 0;
        }
        
        if ($candidat['is_not_in_this_company'] == 'on') {
            $candidat['is_not_in_this_company'] = 1;
        } else {
            $candidat['is_not_in_this_company'] = 0;
        }
        
        if (isset($candidat['gdpr_status'])) {
            if ($candidat['gdpr_status'] == 'on') {
                $candidat['gdpr_status'] = 1;
            } else {
                $candidat['gdpr_status'] = 0;
            } 
        }
            
           
        // candidatscompanies
        // https://www.tutorialspoint.com/mysql-search-if-more-than-one-string-contains-special-characters
        $sql = "SELECT * FROM candidatscompanies WHERE name = \"{$candidat['societe_actuelle']}\"";
        $row = $con->fetchAssoc($sql);
        if (!empty($row)) { // exist

            $candidat['company_id'] = $row['id'];

        } else { // does not exist

            // we insert the company and update the candidat company_id
            $secteur = Control::getControlById($candidat['control_secteur_id']);

            $data = array(
                'name' => $candidat['societe_actuelle'],
                'telephone' => $candidat['tel_standard'],
                'secteur_activite' => $secteur['name'],
                'secteur_activite_id' => $candidat['control_secteur_id'],
                'created' => dateToDb()
            );

            $con->insert('candidatscompanies', $data);
            $company_id = $con->lastInsertId();

            $candidat['company_id'] = $company_id;
            
            //watchdog
            $log  = array(
                'module_name' => 'Entreprise',
                'module_code' => 'mod_entreprise',
                'action' => 'Ajouter Entreprise Creation Candidat',
                'action_code' => 'mod_entreprise_create_entreprise_from_candidat_create',
                'table_name' => 'candidatscompanies',
                'table_id' => $company_id,
                'content_type' => 'json',
                'content_before' => json_encode($data),
            );        
            Watchdog::Log($log);

        }

        
        $con->insert('candidats', $candidat);
        
        $candidat_id = $con->lastInsertId();
                
        //watchdog
        $log  = array(
            'module_name' => 'Candidats',
            'module_code' => 'mod_candidat',
            'action' => 'Ajouter Candidat',
            'action_code' => 'mod_candidat_create_candidat',
            'table_name' => 'candidats',
            'table_id' => $candidat_id,
            'content_type' => 'json',
            'content_before' => json_encode($candidat),
        );        
        Watchdog::Log($log);
        
        self::addLangues($candidat_id, $langues);
        if ($commentaire != "") {
            self::addComment($candidat_id, $commentaire);
        }
        
        self::addSocialLinks($candidat_id, $reseauSociaux);
        self::addDocuments($candidat_id, $files);
        
        return $candidat_id;
        
    }
    
    public static function editCandidat($candidat, $langues) {
        global $con;
        
        // uses to send mail to client to notify that the candidat has been modified in the tdc accordingly
        if (isset($candidat['mission_id']) && isset($candidat['tdc_id'])) {
            $mission_id = $candidat['mission_id'];
            $tdc_id = $candidat['tdc_id'];
            unset($candidat['mission_id']);
            unset($candidat['tdc_id']);
        }
        
        // the check box values cv_fourni and tres_bon_candidat is on when checked
        if ($candidat['cv_fourni'] == 'on') {
            $candidat['cv_fourni'] = 1;
        } else {
            $candidat['cv_fourni'] = 0;
        }
        
        if ($candidat['tres_bon_candidat'] == 'on') {
            $candidat['tres_bon_candidat'] = 1;
        } else {
            $candidat['tres_bon_candidat'] = 0;
        }
        
        if ($candidat['is_not_in_this_company'] == 'on') {
            $candidat['is_not_in_this_company'] = 1;
        } else {
            $candidat['is_not_in_this_company'] = 0;
        }
        
        if (isset($candidat['gdpr_status'])) {
            if ($candidat['gdpr_status'] == 'on') {
                $candidat['gdpr_status'] = 1;
            } else {
                $candidat['gdpr_status'] = 0;
            } 
        }
                
        $content_before = self::getCandidatById($candidat['id']);
        
        // we must check if the societe_actuelle is the same or not
        if ($candidat['societe_actuelle_old'] <> $candidat['societe_actuelle']) { // means that the societe has been changed
            
            // if societe has been changed we check for existance of the new societe from table candidatscompanies
            $sql = "SELECT * FROM candidatscompanies WHERE name = \"{$candidat['societe_actuelle']}\"";
            $row = $con->fetchAssoc($sql);
            if (!empty($row)) { // exist
                
                $candidat['company_id'] = $row['id'];
            
            } else { // does not exist
            
                // we insert the company and update the candidat company_id
                $secteur = Control::getControlById($candidat['control_secteur_id']);
    
                $data = array(
                    'name' => $candidat['societe_actuelle'],
                    'telephone' => $candidat['tel_standard'],
                    'secteur_activite' => $secteur['name'],
                    'secteur_activite_id' => $candidat['control_secteur_id'],
                    'created' => dateToDb()
                );

                $con->insert('candidatscompanies', $data);
                $company_id = $con->lastInsertId();
                
                $candidat['company_id'] = $company_id;
                
                //watchdog
                $log  = array(
                    'module_name' => 'Entreprise',
                    'module_code' => 'mod_entreprise',
                    'action' => 'Ajouter Entreprise Edition Candidat',
                    'action_code' => 'mod_entreprise_create_entreprise_from_candidat_edit',
                    'table_name' => 'candidatscompanies',
                    'table_id' => $company_id,
                    'content_type' => 'json',
                    'content_before' => json_encode($data),
                );        
                Watchdog::Log($log);
                
                
            }
            
        }
        
        unset($candidat['societe_actuelle_old']);
        
        $con->update('candidats', $candidat, array('id' => $candidat['id']));
                        
        //watchdog
        $log  = array(
            'module_name' => 'Candidats',
            'module_code' => 'mod_candidat',
            'action' => 'Éditer Candidat',
            'action_code' => 'mod_candidat_edit_candidat',
            'table_name' => 'candidats',
            'table_id' => $candidat['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($candidat),
        );        
        Watchdog::Log($log);
        
        self::deleteLangues($candidat['id']);
        self::addLangues($candidat['id'], $langues);
        
        // send mail to client to notify that the candidat has been modified in the tdc accordingly
        if (isset($mission_id) && isset($tdc_id) && $tdc_id <> "") {
                        
            $tdc = TDC::getTDCById($tdc_id);
            
            // send client email alert about new candidat added from the tdc 
            if ($tdc['visibilite_client'] == 1) {

                // there may be many client assigned to the missions and we send to all of them
                $mission_clients = Mission::getAssignedClientToMissions($mission_id);

                if (!empty($mission_clients)) {

                    $mission = Mission::getMissionById($mission_id);

                    if ($mission['prestataire_id'] == 1) {
                        $logo_mail = "opsearch";
                    } else {
                        $logo_mail = "headhunting";
                    }

                    $link_mission = BASE_URL . "/tdc-client?id=" . $mission_id;
                    
                    $cand = Candidat::getCandidatById($candidat['id']);

                    $mail_body = "<strong>Candidat modifié sur TDC " . $mission_id . "</strong><br/>";
                    $mail_body .= "<strong>Référence du candidat : </strong> " . $cand['id'] . "<br>";
                    $mail_body .= "<strong>Candidat : </strong> " . mb_strtoupper($cand['nom']) . ' ' . mb_ucfirst($cand['prenom']) . "<br>";
                    $mail_body .= "Cliquez sur le lien suivant : ";
                    $mail_body .= "<a href='" . $link_mission . "'>TDC " . $mission_id . "</a><br/><br/>";
                    $mail_body .= "Ou copier coller le lien suivant dans votre navigateur.<br/>";
                    $mail_body .= $link_mission . "<br/><br/>";

                    $mail_subject = "Candidat modifié sur TDC " . $mission_id;

                    foreach($mission_clients as $mission_client) :

                        if ($mission_client['mail_notif'] == 1) :

                            $mail_body_into = "Bonjour " . $mission_client['lastName'] . " " . $mission_client['firstName'] . ",<br/><br/>";

                            $new_mail_body = $mail_body_into . $mail_body;

                            if (Mail::sendMail($mail_subject, $mission_client['email'], $new_mail_body, $logo_mail)) {
                                // log                             
                                $log  = array(
                                    'module_name' => 'Mail',
                                    'module_code' => 'mod_mail',
                                    'action' => 'Envoie Mail Edition Candidat TDC',
                                    'action_code' => 'mod_mail_send_mail_edit_candidat',
                                    'table_name' => 'tdc',
                                    'table_id' => $tdc_id,
                                    'content_type' => 'json',
                                    'content_before' => json_encode(array('to' => $mission_client['email'], 'to_user_id' => $mission_client['id'], 'subject' => $mail_subject, 'body' => $new_mail_body, 'mission_id' => $mission_id, 'candidat_id' => $cand['id'])),
                                );
                                Watchdog::log($log);
                            }

                        endif;

                    endforeach;

                }

            }
            
        }
        
        return true;
        
    }
    
    public static function addDocuments($candidat_id, $files, $tdc_id = null, $mission_id = null) {
        global $con;
        $numFiles = count($files['name']);
        
        if ($numFiles > 0 && $files['name'][0] != "") {
            // check if the candidat folder exist
            $candidat_dir = CANDIDAT_PATH . '/' . $candidat_id;

            // check if candidat folder exist
            // if not create it
            if (!file_exists($candidat_dir)) {
                mkdir($candidat_dir); // default chmod is 0777
            }

            foreach($files['name'] as $index => $filename):
                
                $file_renamed = $candidat_id . '_' . date('dmY', time()) . '_' .$filename;
                move_uploaded_file($files['tmp_name'][$index], $candidat_dir . '/' . $file_renamed); // we append candidat_id_before
                
                $data = array('candidat_id' => $candidat_id, 'file' => $file_renamed, 'created' => time());
                
                $con->insert('candidatsdocuments', $data);
                
                $id = $con->lastInsertId();
            
                //watchdog
                $log  = array(
                    'module_name' => 'Candidats',
                    'module_code' => 'mod_candidat',
                    'action' => 'Ajouter Candidat Document',
                    'action_code' => 'mod_candidat_create_candidat_document',
                    'table_parent_name' => 'candidats',
                    'table_parent_id' => $candidat_id,
                    'table_name' => 'candidatsdocuments',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode($data),
                );        
                Watchdog::Log($log);
                
            endforeach;
            
            // we update the field documents for optimization
            $docs = self::getDocuments($candidat_id);

            $document = array();
                
            foreach($docs as $docu) :

                $document[$docu['id']] = array(
                    'id' => $docu['id'],
                    'candidat_id' => $docu['candidat_id'],
                    'file' => $docu['file'],
                    'status' => $docu['status'],
                    'created' => dateToFr($docu['created'], true)
                );

            endforeach;

            $data = array(
                'documents' => json_encode($document),
                'cv_fourni' => 1, // we update the field cv_fourni automatically to 1 if there is atleast 1 document whatever the type
            );

            // update the field documents json_encode in table candidats
            $con->update('candidats', $data, array('id' => $candidat_id));
            
            // send email to client to notify that new document added for this candidate
            if ($tdc_id > 0 && $mission_id > 0) {

                $tdc = TDC::getTDCById($tdc_id);

                // send client email alert about new candidat added from the tdc 
                if ($tdc['visibilite_client'] == 1) {

                    // there may be many client assigned to the missions and we send to all of them
                    $mission_clients = Mission::getAssignedClientToMissions($mission_id);

                    if (!empty($mission_clients)) {

                        $mission = Mission::getMissionById($mission_id);

                        if ($mission['prestataire_id'] == 1) {
                            $logo_mail = "opsearch";
                        } else {
                            $logo_mail = "headhunting";
                        }

                        $link_mission = BASE_URL . "/tdc-client?id=" . $mission_id;

                        $cand = Candidat::getCandidatById($candidat_id);

                        $mail_body = "<strong>Nouveau document candidat ajouté sur TDC " . $mission_id . "</strong><br/>";
                        $mail_body .= "<strong>Référence du candidat : </strong> " . $cand['id'] . "<br>";
                        $mail_body .= "<strong>Candidat : </strong> " . mb_strtoupper($cand['nom']) . ' ' . mb_ucfirst($cand['prenom']) . "<br>";
                        $mail_body .= "Cliquez sur le lien suivant : ";
                        $mail_body .= "<a href='" . $link_mission . "'>TDC " . $mission_id . "</a><br/><br/>";
                        $mail_body .= "Ou copier coller le lien suivant dans votre navigateur.<br/>";
                        $mail_body .= $link_mission . "<br/><br/>";

                        $mail_subject = "Nouveau document candidat ajouté sur TDC " . $mission_id;

                        foreach($mission_clients as $mission_client) :

                            if ($mission_client['mail_notif'] == 1) :

                                $mail_body_into = "Bonjour " . $mission_client['lastName'] . " " . $mission_client['firstName'] . ",<br/><br/>";

                                $new_mail_body = $mail_body_into . $mail_body;

                                if (Mail::sendMail($mail_subject, $mission_client['email'], $new_mail_body, $logo_mail)) {
                                    // log                             
                                    $log  = array(
                                        'module_name' => 'Mail',
                                        'module_code' => 'mod_mail',
                                        'action' => 'Envoie Mail Nouveau Document Candidat TDC',
                                        'action_code' => 'mod_mail_send_mail_add_doc_candidat',
                                        'table_name' => 'tdc',
                                        'table_id' => $tdc_id,
                                        'content_type' => 'json',
                                        'content_before' => json_encode(array('to' => $mission_client['email'], 'to_user_id' => $mission_client['id'], 'subject' => $mail_subject, 'body' => $new_mail_body, 'mission_id' => $mission_id, 'candidat_id' => $cand['id'])),
                                    );
                                    Watchdog::log($log);
                                }

                            endif;

                        endforeach;

                    }

                }

            }
            
        }        
        return true;    
        
    }


    public static function addSocialLinks($candidat_id, $reseauSociaux) {
        global $con;
        
        foreach($reseauSociaux as $link):
            
            if ($link != "") {
                
                $data = array('candidat_id' => $candidat_id, 'link' => $link, 'created' => time());
                
                $con->insert('candidatsreseausociaux', $data);
                
                $id = $con->lastInsertId();
            
                //watchdog
                $log  = array(
                    'module_name' => 'Candidats',
                    'module_code' => 'mod_candidat',
                    'action' => 'Ajouter Candidat Réseaux sociaux',
                    'action_code' => 'mod_candidat_create_candidat_link',
                    'table_parent_name' => 'candidats',
                    'table_parent_id' => $candidat_id,
                    'table_name' => 'candidatsreseausociaux',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode($data),
                );        
                Watchdog::Log($log);
                
            }

        endforeach;
        
        return true;
    }
    
    public static function addComment($candidat_id, $mission_id, $commentaire) {
        global $con;
        
        $data = array('candidat_id' => $candidat_id, 'mission_id' => $mission_id, 'user_id' => $_SESSION['opsearch_user']['id'], 'comment' => $commentaire, 'created' => time());
        
        $con->insert('candidatscommentaires', $data);
        
        $id = $con->lastInsertId();
        
        // now we update the tdc field user_id_kpi_candidat_last_comment for kpi cdr purposes
        $con->update('tdc', array('user_id_kpi_candidat_last_comment' => $_SESSION['opsearch_user']['id'], 'user_id_kpi_assigned' => $_SESSION['opsearch_user']['id'], 'modified' => time()), array('mission_id' => $mission_id, 'candidat_id' => $candidat_id));
        
        //watchdog
        $log  = array(
            'module_name' => 'Candidats',
            'module_code' => 'mod_candidat',
            'action' => 'Ajouter Candidat Commentaire',
            'action_code' => 'mod_candidat_create_candidat_comment',
            'table_parent_name' => 'candidats',
            'table_parent_id' => $candidat_id,
            'table_name' => 'candidatscommentaires',
            'table_id' => $id,
            'mission_id' => $mission_id,
            'content_type' => 'json',
            'content_before' => json_encode($data),
        );        
        Watchdog::Log($log);
        
        
        return true;
    }
    
    public static function addMotivation($candidat_id, $mission_id, $motivation) {
        global $con;
        
        $data = array('candidat_id' => $candidat_id, 'mission_id' => $mission_id, 'user_id' => $_SESSION['opsearch_user']['id'], 'motivation' => $motivation, 'created' => dateToDb());
        
        $con->insert('candidatsmotivations', $data);
        
        $id = $con->lastInsertId();
                
        //watchdog
        $log  = array(
            'module_name' => 'Candidats',
            'module_code' => 'mod_candidat',
            'action' => 'Ajouter Candidat Motivation',
            'action_code' => 'mod_candidat_create_candidat_motivation',
            'table_parent_name' => 'candidats',
            'table_parent_id' => $candidat_id,
            'table_name' => 'candidatsmotivations',
            'table_id' => $id,
            'mission_id' => $mission_id,
            'content_type' => 'json',
            'content_before' => json_encode($data),
        );        
        Watchdog::Log($log);
        
        
        return true;
    }
    
    public static function addLangues($candidat_id, $langues) {
        global $con;
        
        if (!empty($langues)) {
            foreach($langues as $langue):
            
                $con->insert('candidatslangues', array('candidat_id' => $candidat_id, 'control_langue_id' => $langue));
                
                $id = $con->lastInsertId();
            
                //watchdog
                $log  = array(
                    'module_name' => 'Candidats',
                    'module_code' => 'mod_candidat',
                    'action' => 'Ajouter Candidat Langue',
                    'action_code' => 'mod_candidat_create_candidat_langue',
                    'table_parent_name' => 'candidats',
                    'table_parent_id' => $candidat_id,
                    'table_name' => 'candidatslangues',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode(array('candidat_id' => $candidat_id, 'control_langue_id' => $langue)),
                );        
                Watchdog::Log($log);
                
            endforeach;
        }
        
        return true;
    }
    
    public static function deleteLangues($candidat_id) {
        global $con;
        $con->delete('candidatslangues', array('candidat_id' => $candidat_id));
        return true;
    }
    
    public static function getCandidatsList() {
        global $con;
        $sql = "SELECT * FROM candidats WHERE status = 1 ORDER BY id DESC LIMIT 100";
        return $con->fetchAll($sql);
    }
    
    public static function getCandidatsListSearch($candidat) {
        global $con;
        
        $sql = "SELECT * FROM candidats WHERE status = 1";
        
        if ($candidat['nom'] <> "")
            $sql .= " AND nom LIKE '%{$candidat['nom']}%'";
        
        if ($candidat['prenom'] <> "")
            $sql .= " AND prenom LIKE '%{$candidat['prenom']}%'";
        
        if ($candidat['societe_actuelle'] <> "")
            $sql .= " AND societe_actuelle LIKE '%{$candidat['societe_actuelle']}%'";
            
        if ($candidat['control_localisation_id'] <> "")
            $sql .= " AND control_localisation_id = {$candidat['control_localisation_id']}";
            
        if ($candidat['control_secteur_id'] <> "")
            $sql .= " AND control_secteur_id = {$candidat['control_secteur_id']}";
            
        if ($candidat['control_fonction_id'] <> "")
            $sql .= " AND control_fonction_id = {$candidat['control_fonction_id']}";
            
        if ($candidat['control_niveauFormation_id'] <> "")
            $sql .= " AND control_niveauFormation_id = {$candidat['control_niveauFormation_id']}";
        
        if ($candidat['tres_bon_candidat'] == "on")
            $sql .= " AND tres_bon_candidat = 1";
        
        if ($candidat['email'] <> "") {
            $sql .= " AND (email LIKE '%{$candidat['email']}%' OR email2 LIKE '%{$candidat['email']}%') ";
        }
        
        if ($candidat['poste'] <> "") {
            $sql .= " AND poste LIKE '%{$candidat['poste']}%' ";
        }
        
        if ($candidat['control_source_id'] <> "") {
            $sql .= " AND control_source_id = {$candidat['control_source_id']} ";
        }
        
        if ($candidat['tel'] <> "") {
            $t = $candidat['tel'];
            $sql .= " AND (tel_standard LIKE '%{$t}%' OR tel_ligne_direct LIKE '%{$t}%' OR tel_pro LIKE '%{$t}%' OR tel_mobile_perso LIKE '%{$t}%' OR tel_domicile LIKE '%{$t}%') ";
        }
        
        if ($candidat['diplome_specialisation'] <> "") {
            $sql .= " AND diplome_specialisation LIKE '%{$candidat['diplome_specialisation']}%' ";
        }
        
        if ($candidat['detail_remuneration'] <> "") {
            $sql .= " AND detail_remuneration LIKE '%{$candidat['detail_remuneration']}%' ";
        }
        
        if ($candidat['avantages'] <> "") {
            $sql .= " AND avantages LIKE '%{$candidat['avantages']}%' ";
        }
        
        if ($candidat['remuneration_souhaitee'] <> "") {
            $sql .= " AND remuneration_souhaitee LIKE '%{$candidat['remuneration_souhaitee']}%' ";
        }
        
//        if ($candidat['salaire_package'] <> "") {
//            $sql .= " AND control_source_id = {$candidat['control_source_id']} ";
//        }
        
        if ($candidat['documents'] == "on") {
            $sql .= " AND documents IS NOT NULL ";
        }
        
//        if ($candidat['company_blacklisted'] == "on") {
//            $sql .= " AND company_blacklisted = 1 ";
//        }
        
        if ($candidat['remuneration_from'] <> "" && $candidat['remuneration_to'] == "") {
            $sql .= " AND remuneration >= {$candidat['remuneration_from']}";
        } elseif ($candidat['remuneration_from'] == "" && $candidat['remuneration_to'] <> "") {
            $sql .= " AND remuneration <= {$candidat['remuneration_to']}";
        } elseif ($candidat['remuneration_from'] <> "" && $candidat['remuneration_to'] <> "") {
            $sql .= " AND remuneration BETWEEN {$candidat['remuneration_from']} AND {$candidat['remuneration_to']}";
        }
        
//        if ($candidat['salaire_package_from'] <> "" && $candidat['salaire_package_to'] == "") {
//            $sql .= " AND salaire_package >= {$candidat['salaire_package_from']}";
//        } elseif ($candidat['salaire_package_from'] == "" && $candidat['salaire_package_to'] <> "") {
//            $sql .= " AND salaire_package <= {$candidat['salaire_package_to']}";
//        } elseif ($candidat['salaire_package_from'] <> "" && $candidat['salaire_package_to'] <> "") {
//            $sql .= " AND salaire_package BETWEEN {$candidat['salaire_package_from']} AND {$candidat['salaire_package_to']}";
//        }
        
        if ($candidat['salaire_fixe_from'] <> "" && $candidat['salaire_fixe_to'] == "") {
            $sql .= " AND salaire_fixe >= {$candidat['salaire_fixe_from']}";
        } elseif ($candidat['salaire_fixe_from'] == "" && $candidat['salaire_fixe_to'] <> "") {
            $sql .= " AND salaire_fixe <= {$candidat['salaire_fixe_to']}";
        } elseif ($candidat['salaire_fixe_from'] <> "" && $candidat['salaire_fixe_to'] <> "") {
            $sql .= " AND salaire_fixe BETWEEN {$candidat['salaire_fixe_from']} AND {$candidat['salaire_fixe_to']}";
        }
        
//        if ($candidat['sur_combien_de_mois_from'] <> "" && $candidat['sur_combien_de_mois_to'] == "") {
//            $sql .= " AND salaire_fixe >= {$candidat['sur_combien_de_mois_from']}";
//        } elseif ($candidat['sur_combien_de_mois_from'] == "" && $candidat['sur_combien_de_mois_to'] <> "") {
//            $sql .= " AND salaire_fixe <= {$candidat['sur_combien_de_mois_to']}";
//        } elseif ($candidat['sur_combien_de_mois_from'] <> "" && $candidat['sur_combien_de_mois_to'] <> "") {
//            $sql .= " AND salaire_fixe BETWEEN {$candidat['sur_combien_de_mois_from']} AND {$candidat['sur_combien_de_mois_to']}";
//        }
        
        if ($candidat['dateDiplome_from'] <> "" && $candidat['dateDiplome_to'] == "") {
            $sql .= " AND dateDiplome >= {$candidat['dateDiplome_from']}";
        } elseif ($candidat['dateDiplome_from'] == "" && $candidat['dateDiplome_to'] <> "") {
            $sql .= " AND dateDiplome <= {$candidat['dateDiplome_to']}";
        } elseif ($candidat['dateDiplome_from'] <> "" && $candidat['dateDiplome_to'] <> "") {
            $sql .= " AND dateDiplome BETWEEN {$candidat['dateDiplome_from']} AND {$candidat['dateDiplome_from']}";
        }
                
        // age difference
        if ($candidat['age_from'] <> "" && $candidat['age_to'] == "") {
            
            $age_from = getDateOfBirth($candidat['age_from']);
            $sql .= " AND dateOfBirth <= {$age_from}";
            
        } elseif ($candidat['age_from'] == "" && $candidat['age_to'] <> "") {
            
            $age_to = getDateOfBirth($candidat['age_to']);
            $sql .= " AND dateOfBirth >= {$age_to}";
            
        } elseif ($candidat['age_from'] <> "" && $candidat['age_to'] <> "") {
            
            $age_from = getDateOfBirth($candidat['age_from']);
            $age_to = getDateOfBirth($candidat['age_to']);
            $sql .= " AND dateOfBirth BETWEEN {$age_to} AND {$age_from}";
            
        }
        
        if ($candidat['shortliste'] == "on") {
            $query = "SELECT DISTINCT(candidat_id) FROM tdcsuivis WHERE shortliste = 1";
            $results = $con->fetchAll($query);
            $candidat_ids = array();
            foreach($results as $result):
                $candidat_ids[] = $result['candidat_id'];
            endforeach;            
            
            $ids = addSeperator($candidat_ids, ',');
            $sql .= " AND id IN({$ids})";
        }
            
        
        if ($candidat['place'] == "on") {
            $query = "SELECT DISTINCT(candidat_id) FROM tdcsuivis WHERE place = 1";
            $results = $con->fetchAll($query);
            $candidat_ids = array();
            foreach($results as $result):
                $candidat_ids[] = $result['candidat_id'];
            endforeach;            
            
            $ids = addSeperator($candidat_ids, ',');
            $sql .= " AND id IN({$ids}) ";
        }
        
        
        // added to block candidat for tdc 2198 and 2027 as these tdc are private
        // allowed for only the users 301, 481, 1, 5
        $allowed_user_ids = array(301, 481, 1, 5, 502, 537);
        
        if (!in_array($candidat['user_id'], $allowed_user_ids)) {
            $sql_ignore = "SELECT candidat_id FROM tdc WHERE mission_id IN (2198, 2027) AND entreprise_id = 0";
            $candidat_ignore_ids = $con->fetchAll($sql_ignore);
            $candidat_ids_ignore = array();
            foreach($candidat_ignore_ids as $candidat_ignore_id):
                $candidat_ids_ignore[] = $candidat_ignore_id['candidat_id'];
            endforeach;
            $ids_to_ignore = addSeperator($candidat_ids_ignore, ',');

            $sql .= " AND id NOT IN({$ids_to_ignore}) ";
        } 
                
        return $con->fetchAll($sql);
    }


    public static function getCandidatById($id) {
        global $con;
        $sql = "SELECT * FROM candidats WHERE id = {$id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getLangues($candidat_id) {
        global $con;
        $sql = "SELECT control_langue_id FROM candidatslangues WHERE candidat_id = {$candidat_id}";
        $results = $con->fetchAll($sql);
        
        $langs = array();
        foreach ($results as $result):
            $langs[] = $result['control_langue_id'];
        endforeach;
        
        return $langs;
    }
    
    public static function getLinks($candidat_id) {
        global $con;
        $sql = "SELECT * FROM candidatsreseausociaux WHERE candidat_id = {$candidat_id} AND status = 1";
        return $con->fetchAll($sql);
    }
    
    public static function getDocuments($candidat_id) {
        global $con;
        $sql = "SELECT * FROM candidatsdocuments WHERE candidat_id = {$candidat_id} AND status = 1";
        return $con->fetchAll($sql);
    }
    
    public static function getDocumentById($id) {
        global $con;
        $sql = "SELECT * FROM candidatsdocuments WHERE id = {$id}";
        $doc = $con->fetchAssoc($sql);
        return array(
            'path' => CANDIDAT_PATH. '/' . $doc['candidat_id'] . '/' . $doc['file'],
            'content_type' => getContentType(getFileExt($doc['file'])),
        );
    }
    
    public static function getLastCommentByMission($candidat_id, $mission_id) {
        global $con;
        
        $sql = "SELECT MAX(id) AS max_id "
            . "FROM candidatscommentaires "
            . "WHERE status = 1 AND candidat_id = {$candidat_id} AND mission_id = {$mission_id}";
            
        $result = $con->fetchAssoc($sql);
                
        if (!empty($result) && $result['max_id'] > 0) {
            $max_id = $result['max_id'];
            return self::getCommentId($max_id);
        } else {
            return self::getLastComment($candidat_id);
        }
    }
    
    public static function getLastMotivationByMission($candidat_id, $mission_id) {
        global $con;
        
        $sql = "SELECT MAX(id) AS max_id "
            . "FROM candidatsmotivations "
            . "WHERE status = 1 AND candidat_id = {$candidat_id} AND mission_id = {$mission_id}";
            
        $result = $con->fetchAssoc($sql);
                
        if (!empty($result) && $result['max_id'] > 0) {
            $max_id = $result['max_id'];
            return self::getMotivationId($max_id);
        } else {
            return self::getLastMotivation($candidat_id);
        }
    }
    
    public static function getLastComment($candidat_id) {
        global $con;
        
        $sql = "SELECT MAX(id) AS max_id "
            . "FROM candidatscommentaires "
            . "WHERE status = 1 AND candidat_id = {$candidat_id}";
            
        $result = $con->fetchAssoc($sql);
                
        if (!empty($result) && $result['max_id'] > 0) {
            $max_id = $result['max_id'];
            return self::getCommentId($max_id);
        } else {
            return array();
        }
        
    }
    
    public static function getLastMotivation($candidat_id) {
        global $con;
        
        $sql = "SELECT MAX(id) AS max_id "
            . "FROM candidatsmotivations "
            . "WHERE status = 1 AND candidat_id = {$candidat_id}";
            
        $result = $con->fetchAssoc($sql);
                
        if (!empty($result) && $result['max_id'] > 0) {
            $max_id = $result['max_id'];
            return self::getMotivationId($max_id);
        } else {
            return array();
        }
        
    }
    
    public static function getComments($candidat_id) {
        global $con;
        $sql = "SELECT C.id, C.candidat_id, C.comment, C.created, C.modified, U.firstName, U.lastName "
            . "FROM candidatscommentaires C, users U "
            . "WHERE C.status = 1 AND U.id = C.user_id AND C.candidat_id = {$candidat_id} "
            . "ORDER BY C.id DESC";
        return $con->fetchAll($sql);
    }
    
    public static function getCommentsNew($candidat_id) {
        global $con;            
        $sql = "SELECT C.id, C.candidat_id, C.comment, C.created, C.modified, U.firstName, U.lastName, C.mission_id, M.poste as mission, CL.nom as client "
                . "FROM candidatscommentaires C "
                . "LEFT JOIN users U ON U.id = C.user_id "
                . "LEFT JOIN missions M ON M.id = C.mission_id "
                . "LEFT JOIN clients CL ON CL.id = M.client_id "
                . "WHERE C.status = 1 AND C.candidat_id = {$candidat_id} "
                . "ORDER BY C.id DESC";
        
        
        return $con->fetchAll($sql);
    }
    
    public static function getMotivation($candidat_id) {
        global $con;            
        $sql = "SELECT C.id, C.candidat_id, C.motivation, C.created, C.modified, U.firstName, U.lastName, C.mission_id, M.poste as mission, CL.nom as client "
                . "FROM candidatsmotivations C "
                . "LEFT JOIN users U ON U.id = C.user_id "
                . "LEFT JOIN missions M ON M.id = C.mission_id "
                . "LEFT JOIN clients CL ON CL.id = M.client_id "
                . "WHERE C.status = 1 AND C.candidat_id = {$candidat_id} "
                . "ORDER BY C.id DESC";
        
        
        return $con->fetchAll($sql);
    }
    
    public static function getCommentId($id) {
        global $con;
        $sql = "SELECT * FROM candidatscommentaires WHERE id = {$id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getMotivationId($id) {
        global $con;
        $sql = "SELECT * FROM candidatsmotivations WHERE id = {$id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function editComment($comment) {
        global $con;
        
        $content_before = self::getCommentId($comment['id']);
        
        $con->update('candidatscommentaires', $comment, array('id' => $comment['id']));
                    
        //watchdog
        $log  = array(
            'module_name' => 'Candidats',
            'module_code' => 'mod_candidat',
            'action' => 'Éditer Candidat commentaire',
            'action_code' => 'mod_candidat_edit_candidat_comment',
            'table_parent_name' => 'candidats',
            'table_parent_id' => $comment['candidat_id'],
            'table_name' => 'candidatscommentaires',
            'table_id' => $comment['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($comment),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function editMotivation($comment) {
        global $con;
        
        $content_before = self::getMotivationId($comment['id']);
        
        $con->update('candidatsmotivations', $comment, array('id' => $comment['id']));
                    
        //watchdog
        $log  = array(
            'module_name' => 'Candidats',
            'module_code' => 'mod_candidat',
            'action' => 'Éditer Candidat Motivation',
            'action_code' => 'mod_candidat_edit_candidat_motivation',
            'table_parent_name' => 'candidats',
            'table_parent_id' => $comment['candidat_id'],
            'table_name' => 'candidatsmotivations',
            'table_id' => $comment['id'],
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
            'content_after' => json_encode($comment),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function deleteComment($id) {
        global $con;
        
        $content_before = self::getCommentId($id);
        
        $con->delete('candidatscommentaires', array('id' => $id));
        
        //watchdog
        $log  = array(
            'module_name' => 'Candidats',
            'module_code' => 'mod_candidat',
            'action' => 'Supprimer Candidat commentaire',
            'action_code' => 'mod_candidat_delete_candidat_comment',
            'table_parent_name' => 'candidats',
            'table_parent_id' => $content_before['candidat_id'],
            'table_name' => 'candidatscommentaires',
            'table_id' => $id,
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function deleteMotivation($id) {
        global $con;
        
        $content_before = self::getMotivationId($id);
        
        $con->delete('candidatsmotivations', array('id' => $id));
        
        //watchdog
        $log  = array(
            'module_name' => 'Candidats',
            'module_code' => 'mod_candidat',
            'action' => 'Supprimer Candidat motivation',
            'action_code' => 'mod_candidat_delete_candidat_motivation',
            'table_parent_name' => 'candidats',
            'table_parent_id' => $content_before['candidat_id'],
            'table_name' => 'candidatsmotivations',
            'table_id' => $id,
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function getLinkById($id) {
        global $con;
        $sql = "SELECT * FROM candidatsreseausociaux WHERE id = {$id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function deleteLink($id) {
        global $con;
        
        $content_before = self::getLinkById($id);
        
        $con->delete('candidatsreseausociaux', array('id' => $id));
        
        //watchdog
        $log  = array(
            'module_name' => 'Candidats',
            'module_code' => 'mod_candidat',
            'action' => 'Supprimer Candidat Réseaux sociaux',
            'action_code' => 'mod_candidat_delete_candidat_link',
            'table_parent_name' => 'candidats',
            'table_parent_id' => $content_before['candidat_id'],
            'table_name' => 'candidatsreseausociaux',
            'table_id' => $id,
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
        );        
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function deleteDocument($id) {
        global $con;
        
        // first we get the document name and candidat it
        // delete the file pysically and then delete record in db
        $sql = "SELECT * FROM candidatsdocuments WHERE id = {$id}";
        $doc = $con->fetchAssoc($sql);
        if (unlink(CANDIDAT_PATH . '/' . $doc['candidat_id'] . '/' . $doc['file'])) {
            $con->delete('candidatsdocuments', array('id' => $id));
            
            //watchdog
            $log  = array(
                'module_name' => 'Candidats',
                'module_code' => 'mod_candidat',
                'action' => 'Supprimer Candidat Document',
                'action_code' => 'mod_candidat_delete_candidat_document',
                'table_parent_name' => 'candidats',
                'table_parent_id' => $doc['candidat_id'],
                'table_name' => 'candidatsdocuments',
                'table_id' => $id,
                'content_type' => 'json',
                'content_before' => json_encode($doc),
            );        
            Watchdog::Log($log);
            
            // we must update the field cv_fourni to 0 if there is no documents attached after deleting the file
            $docs = self::getDocuments($doc['candidat_id']);
            if (empty($docs)) {
                
                $con->update('candidats', array('cv_fourni' => 0, 'documents' => ''), array('id' => $doc['candidat_id']));
                
            } else { // we update the field documents for optimization
                
                $document = array();
                
                foreach($docs as $docu) :
            
                    $document[$docu['id']] = array(
                        'id' => $docu['id'],
                        'candidat_id' => $docu['candidat_id'],
                        'file' => $docu['file'],
                        'status' => $docu['status'],
                        'created' => dateToFr($docu['created'], true)
                    );

                endforeach;

                $data = array(
                    'documents' => json_encode($document),
                );

                // update the field documents json_encode in table candidats
                $con->update('candidats', $data, array('id' => $doc['candidat_id']));
                
            }
                                   
            return true;
        } else {
            return false;
        }
        
    }
    
}
