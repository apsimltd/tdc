<?php
class Dashboard {
    
    /**
     * 1. Nombre de candidats Ajoutés
     * 2. Nombre de candidats ajoutés ayant terminé Placé en mission
     * 3. Nombre de candidats ajoutés ayant terminé Short-listé en mission
     * 4. Nombre de candidats Contactés
     * 5. Nombre de missions Impliquées
     * 6. Nombre de missions terminées par un Placement
     * 7. Nombre moyen de candidats ajoutés par jour
     * 8. Nombre moyen de candidats contactés par jour
     */
    
    /**
     * <p>Nombre de candidats Ajoutés</p>
     * @param Int $user_id
     */
    public static function getNoCandidateAdded($user_id) {
        global $con;
        
        $sql = "SELECT COUNT(c.id) as Total "
                . "FROM candidats c, users u "
                . "WHERE c.user_id = u.id AND c.status = 1 AND u.id = {$user_id}";
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
        
    }
    
    /**
     * <p>Nombre de candidats ajoutés ayant terminé Placé en mission</p>
     * @param Int $user_id
     */
    public static function getNoCandidateAddedPlace($user_id) {
        global $con;
        
        $sql = "SELECT DISTINCT(COUNT(c.id)) as Total "
                . "FROM candidats c, users u, tdcsuivis t "
                . "WHERE c.user_id = u.id AND t.candidat_id = c.id "
                . "AND c.status = 1 AND u.id = {$user_id} AND t.place = 1";
                
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
        
    }
    
    /**
     * <p>Nombre de candidats ajoutés ayant terminé Short-listé en mission</p>
     * @param Int $user_id
     */
    public static function getNoCandidateAddedShortliste($user_id) {
        global $con;
        
        $sql = "SELECT DISTINCT(COUNT(c.id)) as Total "
                . "FROM candidats c, users u, tdcsuivis t "
                . "WHERE c.user_id = u.id AND t.candidat_id = c.id "
                . "AND c.status = 1 AND u.id = {$user_id} AND t.shortliste = 1";
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
        
    }
    
    /**
     * <p>Nombre de candidats Contactés</p>
     * @param Int $user_id
     */
    public static function getNoCandidateContacted($user_id) {
        global $con;
        
        $sql = "SELECT DISTINCT(COUNT(c.id)) as Total "
                . "FROM candidats c, users u, tdcsuivis t "
                . "WHERE t.user_id = u.id AND t.candidat_id = c.id "
                . "AND c.status = 1 AND u.id = {$user_id} AND t.date_prise_de_contact > 0";
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
        
    }
    
    /**
     * <p>Nombre de missions Impliquées</p>
     * @param Int $user_id
     */
    public static function getNoMissionsImplicated($user_id) {
        global $con;
        
        $sql = "SELECT COUNT(mu.mission_id) as Total "
                . "FROM missionsusers mu, missions m "
                . "WHERE m.id = mu.mission_id AND m.status <> 235 "
                . "AND mu.user_id = {$user_id}";
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
        
    }
    
    /**
     * <p>Nombre de missions terminées par un Placement</p>
     * @param Int $user_id
     */
    public static function getNoMissionsImplicatedPlacement($user_id) {
        global $con;
        
        $sql = "SELECT COUNT(mu.mission_id) as Total "
                . "FROM missionsusers mu, missions m "
                . "WHERE m.id = mu.mission_id AND m.status = 230 "
                . "AND mu.user_id = {$user_id}";
        
        $result = $con->fetchAssoc($sql);
        return $result['Total'];
        
    }
    
    /**
     * <p>Nombre moyen de candidats ajoutés par jour</p>
     * https://dev.mysql.com/doc/refman/5.7/en/date-and-time-functions.html#function_month
     */
    public static function getAvgCandidateAddedPerDay($user_id) {
        global $con;
        
        $sql = "SELECT COUNT(c.id) as TotalPerDay "
                . "FROM candidats c "
                . "WHERE c.status = 1 AND c.user_id = {$user_id} "
                . "GROUP BY DAY(FROM_UNIXTIME(c.created))";
        
        $results = $con->fetchAll($sql);
        
        $no_days = count($results);
        $total_candidats = 0;
        foreach($results as $result):
            $total_candidats += $result['TotalPerDay'];
        endforeach;
        
        if ($no_days == 0) {
            return 0; 
        } else {
            $avgPerDay = floor($total_candidats/$no_days);
            return $avgPerDay;
        }
        
    }
    
    /**
     * <p>Nombre moyen de candidats contactés par jour</p>
     * https://dev.mysql.com/doc/refman/5.7/en/date-and-time-functions.html#function_month
     */
    public static function getAvgCandidateContactedPerDay($user_id) {
        global $con;
        
        $sql = "SELECT COUNT(t.candidat_id) as TotalPerDay "
                . "FROM tdcsuivis t "
                . "WHERE t.user_id = {$user_id} AND t.date_prise_de_contact > 0 "
                . "GROUP BY DAY(FROM_UNIXTIME(t.date_prise_de_contact))";
        
        $results = $con->fetchAll($sql);
        
        $no_days = count($results);
        $total_candidats = 0;
        foreach($results as $result):
            $total_candidats += $result['TotalPerDay'];
        endforeach;
        
        if ($no_days == 0) {
            return 0;
        } else {
            $avgPerDay = floor($total_candidats/$no_days);
            return $avgPerDay;
        }
        
        
    }
    
    public static function getEventsEnCours($user_id) {
        global $con;
        
        $now = date('Y-m-d H:i:s', time());
        
        $sql = "SELECT e.*, t.name, t.cssClass "
                . "FROM events e, eventstypes t "
                . "WHERE e.eventType_id = t.id AND e.status = 1 AND e.user_id = {$user_id} "
                . "AND e.start_date >= '{$now}'"
                . "ORDER BY start_date ASC";
        
        return $con->fetchAll($sql);
    }
    
}

