<?php
class KPI {
    
    /**
     * Traductions 3CX
     */
    public static $reasons_terminated = array(
        'Failed' => 'Echoués',
        'Failed_Cancelled' => 'Annulés',
        'NoRouteExists' => 'Aucun Itinéraire N\'existe',
        'RouteBusy' => 'Route Occupée',
        'ServerError' => 'Erreur Du Serveur',
        'TargetBusy' => 'Cible Occupée',
        'TargetDisabled' => 'Numéros Incorrects',
        'TargetNotFound' => 'Cible Introuvable',
        'TerminatedByDst' => 'Terminé Par Destination',
        'TerminatedByRule' => 'Terminé Par Règle',
        'TerminatedBySrc' => 'Terminé Par Source'
    );
    
    
    /**
     * KPI clients
     */
    
    public static function getNumPosteConfies($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_client_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            /// $query_cdr_id = " "; does not apply here
            
            if (isset($filters['client_id']) && $filters['client_id'] <> "") {
                $query_client_id = " AND client_id = {$filters['client_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND date_debut >= {$start} ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND date_debut <= {$end} ";
            } 
            
            $sql = "SELECT COUNT(*) as num_postes, status "
                . "FROM missions "
                . "WHERE include_in_kpi = 1 " . $query_client_id . $query_mission_id . $query_start_date . $query_end_date
                . "GROUP BY status";
            
        } else {
            
            $sql = "SELECT COUNT(*) as num_postes, status FROM missions WHERE include_in_kpi = 1 GROUP BY status";
            
        }
        
        return $con->fetchAll($sql);
    }
    
    /**
     * <p>Dans les TDC >> dans Suivi >> créer 1 nouvelle date: date de présentation</p>
     * <p>Seuls les candidats qui ont une date de présentation doivent être comptabilisés dans ce KPI (quel que soit leur statut)</p>
     */
    public static function numCandidatsPresenter($filters = null) {
        global $con;
        if ($filters) {
            
            $query_client_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            
            if (isset($filters['client_id']) && $filters['client_id'] <> "") {
                $query_client_id = " AND M.client_id = {$filters['client_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND T.mission_id = {$filters['mission_id']} ";
            } 
                                    
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= {$start} ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_debut <= {$end} ";
            } 
            
            $sql = "SELECT COUNT(*) as num_candidats_presenter, S.control_statut_tdc "
                . "FROM tdcsuivis T "
                . "LEFT JOIN missions M ON M.id = T.mission_id "
                . "LEFT JOIN tdc S ON T.candidat_id = S.candidat_id AND T.mission_id = S.mission_id "
                ///. "WHERE M.include_in_kpi = 1 AND T.date_presentation > 0 " . $query_mission_id . $query_client_id . $query_start_date . $query_end_date
                . "WHERE M.include_in_kpi = 1 AND S.control_statut_tdc > 0 " . $query_mission_id . $query_client_id . $query_start_date . $query_end_date  /// date_presentation > 0 removed as asked by Bastien
                . "GROUP BY S.control_statut_tdc";
            
        } else {
        
            ///$sql = "SELECT COUNT(*) as num_candidats_presenter, T.control_statut_tdc FROM tdcsuivis S LEFT JOIN tdc T ON T.candidat_id = S.candidat_id AND T.mission_id = S.mission_id LEFT JOIN missions M ON M.id = T.mission_id WHERE M.include_in_kpi = 1 AND S.date_presentation > 0 GROUP BY T.control_statut_tdc";
            
            /// date_presentation > 0 removed as asked by Bastien
            $sql = "SELECT COUNT(*) as num_candidats_presenter, T.control_statut_tdc FROM tdcsuivis S LEFT JOIN tdc T ON T.candidat_id = S.candidat_id AND T.mission_id = S.mission_id LEFT JOIN missions M ON M.id = T.mission_id WHERE M.include_in_kpi = 1 AND T.control_statut_tdc > 0 GROUP BY T.control_statut_tdc";
            
        }
        
        return $con->fetchAll($sql);
        
//        $result = $con->fetchAssoc($sql);
//        return $result['num_candidats_presenter'];
    }
    
    public static function getNumCandidatsPresente($filters = null) {
        
        /**
         *  226	8	OUT
            227	8	BU-
            228	8	BU+
            229	8	IN
            251	8	ARC
            252	8	ARCA
            259 8   Ex BU // just added but need to confirm if need to include this status
            candidats presenter => statuts => BU, BU-, BU+, ARCA, ARC
         */
        
        global $con;
        
        if ($filters) {
            
            $query_client_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            
            if (isset($filters['client_id']) && $filters['client_id'] <> "") {
                $query_client_id = " AND M.client_id = {$filters['client_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND T.mission_id = {$filters['mission_id']} ";
            } 
                        
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND T.modified >= {$start} ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND T.modified <= {$end} ";
            } 
            
            $sql = "SELECT T.control_statut_tdc, COUNT(*) as num_candidats_presenter "
                . "FROM tdc T "
                . "LEFT JOIN missions M ON M.id = T.mission_id "
                . "WHERE T.control_statut_tdc IN (227, 228, 251, 252) " . $query_mission_id . $query_client_id . $query_start_date . $query_end_date
                . "GROUP BY T.control_statut_tdc";
            
        } else {
        
            $sql = "SELECT tdc.control_statut_tdc, COUNT(*) as num_candidats_presenter FROM tdc WHERE tdc.control_statut_tdc IN (227, 228, 251, 252) GROUP BY tdc.control_statut_tdc";
            
        }
                
        return $con->fetchAll($sql);
    }
    
    public static function getNumCandidatsRencontrer($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_client_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            
            if (isset($filters['client_id']) && $filters['client_id'] <> "") {
                $query_client_id = " AND M.client_id = {$filters['client_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND T.mission_id = {$filters['mission_id']} ";
            } 
            
//            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
//                $start = $filters['start_date'] . " 00:00:01";
//                $query_start_date = " AND T.shortliste_added >= '{$start}' ";
//            }
//
//            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
//                $end = $filters['end_date'] . " 23:59:59";
//                $query_end_date = " AND T.shortliste_added <= '{$end}' ";
//            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= {$start} ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_debut <= {$end} ";
            } 
            
            $sql = "SELECT COUNT(*) as num_candidats_rencontrer "
                . "FROM tdcsuivis T "
                . "LEFT JOIN missions M ON M.id = T.mission_id "
                . "WHERE M.include_in_kpi = 1 AND T.shortliste = 1 " . $query_client_id . $query_mission_id . $query_start_date . $query_end_date;
            
        } else {
        
            $sql = "SELECT COUNT(*) as num_candidats_rencontrer FROM tdcsuivis T LEFT JOIN missions M ON M.id = T.mission_id WHERE M.include_in_kpi = 1 AND T.shortliste = 1";
            
        }
        
        $result = $con->fetchAssoc($sql);
        return $result['num_candidats_rencontrer'];
    }
    
    public static function getNumCandidatPlace($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_client_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            
            if (isset($filters['client_id']) && $filters['client_id'] <> "") {
                $query_client_id = " AND M.client_id = {$filters['client_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND T.mission_id = {$filters['mission_id']} ";
            } 
            
//            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
//                $start = $filters['start_date'] . " 00:00:01";
//                $query_start_date = " AND T.place_added >= '{$start}' ";
//            }
//
//            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
//                $end = $filters['end_date'] . " 23:59:59";
//                $query_end_date = " AND T.place_added <= '{$end}' ";
//            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= {$start} ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_debut <= {$end} ";
            } 
            
            $sql = "SELECT COUNT(*) as num_candidats_place "
                . "FROM tdcsuivis T "
                . "LEFT JOIN missions M ON M.id = T.mission_id "
                . "WHERE M.include_in_kpi = 1 AND T.place = 1 " . $query_client_id . $query_mission_id . $query_start_date . $query_end_date;
            
        } else {
        
            $sql = "SELECT COUNT(*) as num_candidats_place FROM tdcsuivis T LEFT JOIN missions M ON M.id = T.mission_id WHERE M.include_in_kpi = 1 AND T.place = 1";
            
        }
        
        
        $result = $con->fetchAssoc($sql);
        return $result['num_candidats_place'];
    }
    
    public static function tempsReactiviteClientNew($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_client_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            
            if (isset($filters['client_id']) && $filters['client_id'] <> "") {
                $query_client_id = " AND client_id = {$filters['client_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND mission_id = {$filters['mission_id']} ";
            } 
                        
//            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
//                $start = $filters['start_date'] . " 00:00:01";
//                $query_start_date = " AND date_presentation >= {$start} ";
//            }
//
//            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
//                $end = $filters['end_date'] . " 23:59:59";
//                $query_end_date = " AND date_retour_apres_rencontre <= '{$end}' ";
//            } 
            
            $sql = "SELECT SUM(t_reactivite_client) as t_reactivite_client FROM kpi_clients WHERE 1=1 " . $query_client_id . $query_mission_id . $query_start_date . $query_end_date;
            
        } else {
            
            $sql = "SELECT SUM(t_reactivite_client) as t_reactivite_client FROM kpi_clients";
            
        }
        
        $result = $con->fetchAssoc($sql);

        return seconds_to_hours_minutes(round($result['t_reactivite_client'], 0));
        
    }
    
    public static function tempsReactiviteClient($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_client_id = " ";
            $query_client_id2 = " ";
            $query_start_date = " ";
            $query_start_date2 = " ";
            $query_end_date = " ";
            $query_end_date2 = " ";
            $query_mission_id = " ";
            $query_mission_id2 = " ";
            
            if (isset($filters['client_id']) && $filters['client_id'] <> "") {
                $query_client_id = " AND M.client_id = {$filters['client_id']} ";
                $query_client_id2 = " AND K.client_id = {$filters['client_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND T.mission_id = {$filters['mission_id']} ";
                $query_mission_id2 = " AND K.mission_id = {$filters['mission_id']} ";
            } 
                        
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $start_s = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND T.modified >= {$start_s} ";
                $query_start_date2 = " AND K.created >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $end_s = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND T.modified <= '{$end_s}' ";
                $query_end_date2 = " AND K.created <= '{$end}' ";
            } 
            
            $sql_num_candidat = "SELECT COUNT(T.candidat_id) as num_candidat "
                . "FROM tdc T "
                . "LEFT JOIN missions M ON M.id = T.mission_id "
                . "WHERE 1=1 " . $query_mission_id . $query_client_id . $query_start_date . $query_end_date;
            
            $result_num_candidat = $con->fetchAssoc($sql_num_candidat);
            $num_candidats = $result_num_candidat['num_candidat'];
            
            $sql_seconds = "SELECT SUM(K.t_arc) as temps "
                . "FROM kpi_candidat_status_time K "
                . "WHERE 1=1 " . $query_mission_id2 . $query_client_id2 . $query_start_date2 . $query_end_date2;
                        
            $result_seconds = $con->fetchAssoc($sql_seconds);
            $seconds = $result_seconds['temps'];
            
            if ($seconds == "") {
                return "0";
            } else {
                return seconds_to_hours_minutes(round(($seconds/$num_candidats), 0));
            }
                        
        } else {
        
            $sql = "SELECT (SUM(t_arc)/(SELECT COUNT(candidat_id) FROM tdc WHERE mission_id IN (SELECT DISTINCT(mission_id) FROM kpi_candidat_status_time))) as avg_time_status_arc FROM kpi_candidat_status_time";

            $result = $con->fetchAssoc($sql);

            return seconds_to_hours_minutes(round($result['avg_time_status_arc'], 0));
            
        }
    }
    
    public static function tempsReactiviteVsCandidatsRencontre_HF($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_client_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            
            if (isset($filters['client_id']) && $filters['client_id'] <> "") {
                $query_client_id = " AND K.client_id = {$filters['client_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= {$start} ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_debut <= {$end} ";
            }
            
            $sql = "SELECT K.mission_id, T.control_statut_tdc, T.control_statut_tdc_order, K.tdc_suivi_id, M.poste, C.nom as client, CA.nom as cand_nom, CA.prenom as cand_prenom, date_prise_de_contact, date_presentation, date_presentation_added, date_rencontre_client, date_rencontre_client_added, t_presentation_rencontre, date_rencontre_consultant, date_rencontre_consultant_added, t_rencontre_consultant_rencontre_client, date_retour_apres_rencontre, date_retour_apres_rencontre_added, t_reactivite_client "
                . "FROM kpi_clients K "
                . "LEFT JOIN clients C ON C.id = K.client_id "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "LEFT JOIN candidats CA ON CA.id = K.candidat_id "
                . "LEFT JOIN tdc T ON T.candidat_id = K.candidat_id AND T.mission_id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND K.prestataire_id = 2 AND (K.t_presentation_rencontre > 0 OR K.t_reactivite_client > 0) " . $query_client_id . $query_mission_id . $query_start_date . $query_end_date
                . "ORDER BY K.mission_id, T.control_statut_tdc_order ASC";
            
            
        } else {
        
            $sql = "SELECT K.mission_id, T.control_statut_tdc, T.control_statut_tdc_order, K.tdc_suivi_id, M.poste, C.nom as client, CA.nom as cand_nom, CA.prenom as cand_prenom, date_prise_de_contact, date_presentation, date_presentation_added, date_rencontre_client, date_rencontre_client_added, t_presentation_rencontre, date_rencontre_consultant, date_rencontre_consultant_added, t_rencontre_consultant_rencontre_client, date_retour_apres_rencontre, date_retour_apres_rencontre_added, t_reactivite_client "
                . "FROM kpi_clients K "
                . "LEFT JOIN clients C ON C.id = K.client_id "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "LEFT JOIN candidats CA ON CA.id = K.candidat_id "
                . "LEFT JOIN tdc T ON T.candidat_id = K.candidat_id AND T.mission_id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND K.prestataire_id = 2 AND (K.t_presentation_rencontre > 0 OR K.t_reactivite_client > 0) "
                . "ORDER BY K.mission_id, T.control_statut_tdc_order ASC";
            
        }

        return $con->fetchAll($sql);
    }
    
    public static function tempsReactiviteVsCandidatsRencontre_OP($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_client_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            
            if (isset($filters['client_id']) && $filters['client_id'] <> "") {
                $query_client_id = " AND K.client_id = {$filters['client_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= {$start} ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_debut <= {$end} ";
            }                  
            
            $sql = "SELECT K.mission_id, T.control_statut_tdc, T.control_statut_tdc_order, K.tdc_suivi_id, M.poste, C.nom as client, CA.nom as cand_nom, CA.prenom as cand_prenom, date_prise_de_contact, date_presentation, date_presentation_added, date_rencontre_client, date_rencontre_client_added, t_presentation_rencontre, date_rencontre_consultant, date_rencontre_consultant_added, t_rencontre_consultant_rencontre_client, date_retour_apres_rencontre, date_retour_apres_rencontre_added, t_reactivite_client "
                . "FROM kpi_clients K "
                . "LEFT JOIN clients C ON C.id = K.client_id "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "LEFT JOIN candidats CA ON CA.id = K.candidat_id "
                . "LEFT JOIN tdc T ON T.candidat_id = K.candidat_id AND T.mission_id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND K.prestataire_id = 1 AND (K.t_presentation_rencontre > 0 OR K.t_rencontre_consultant_rencontre_client > 0 OR K.t_reactivite_client > 0) " . $query_client_id . $query_mission_id . $query_start_date . $query_end_date
                . "ORDER BY K.mission_id, T.control_statut_tdc_order ASC";       
            
        } else {
        
            $sql = "SELECT K.mission_id, T.control_statut_tdc, T.control_statut_tdc_order, K.tdc_suivi_id, M.poste, C.nom as client, CA.nom as cand_nom, CA.prenom as cand_prenom, date_prise_de_contact, date_presentation, date_presentation_added, date_rencontre_client, date_rencontre_client_added, t_presentation_rencontre, date_rencontre_consultant, date_rencontre_consultant_added, t_rencontre_consultant_rencontre_client, date_retour_apres_rencontre, date_retour_apres_rencontre_added, t_reactivite_client "
                . "FROM kpi_clients K "
                . "LEFT JOIN clients C ON C.id = K.client_id "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "LEFT JOIN candidats CA ON CA.id = K.candidat_id "
                . "LEFT JOIN tdc T ON T.candidat_id = K.candidat_id AND T.mission_id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND K.prestataire_id = 1 AND (K.t_presentation_rencontre > 0 OR K.t_rencontre_consultant_rencontre_client > 0 OR K.t_reactivite_client > 0) "
                . "ORDER BY K.mission_id, T.control_statut_tdc_order ASC";
                        
        }

        return $con->fetchAll($sql);
    }
    
    public static function tempsReactiviteVsCandidatsRencontreOP($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_client_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            
            if (isset($filters['client_id']) && $filters['client_id'] <> "") {
                $query_client_id = " AND K.client_id = {$filters['client_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= {$start} ";                
            } 
            
            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_debut <= {$end} ";
            }               
                        
            $sql = "SELECT K.prestataire_id, SUM(K.t_presentation_rencontre) as t_presentation_rencontre, SUM(K.t_rencontre_consultant_rencontre_client) as t_rencontre_consultant_rencontre_client, SUM(K.t_reactivite_client) as t_reactivite_client, count(*) as num_candidats "
                . "FROM kpi_clients K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND (K.t_presentation_rencontre > 0 OR K.t_rencontre_consultant_rencontre_client > 0 OR K.t_reactivite_client > 0) AND K.prestataire_id = 1 " . $query_client_id . $query_mission_id . $query_start_date . $query_end_date;
            
        } else {
        
            $sql = "SELECT K.prestataire_id, SUM(K.t_presentation_rencontre) as t_presentation_rencontre, SUM(K.t_rencontre_consultant_rencontre_client) as t_rencontre_consultant_rencontre_client, SUM(K.t_reactivite_client) as t_reactivite_client, count(*) as num_candidats "
                . "FROM kpi_clients K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND (K.t_presentation_rencontre > 0 OR K.t_rencontre_consultant_rencontre_client > 0 OR K.t_reactivite_client > 0) AND K.prestataire_id = 1";
                        
        }

        return $con->fetchAssoc($sql);
    }
    
    public static function tempsReactiviteVsCandidatsRencontreHF($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_client_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            
            if (isset($filters['client_id']) && $filters['client_id'] <> "") {
                $query_client_id = " AND K.client_id = {$filters['client_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= {$start} ";                
            } 
            
            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_debut <= {$end} ";
            }   
                        
            $sql = "SELECT K.prestataire_id, SUM(K.t_presentation_rencontre) as t_presentation_rencontre, SUM(K.t_reactivite_client) as t_reactivite_client, count(*) as num_candidats "
                . "FROM kpi_clients K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND (K.t_presentation_rencontre > 0 OR K.t_reactivite_client > 0) AND K.prestataire_id = 2 " . $query_client_id . $query_mission_id . $query_start_date . $query_end_date;
                        
        } else {
        
            $sql = "SELECT K.prestataire_id, SUM(K.t_presentation_rencontre) as t_presentation_rencontre, SUM(K.t_reactivite_client) as t_reactivite_client, count(*) as num_candidats "
                . "FROM kpi_clients K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND (K.t_presentation_rencontre > 0 OR K.t_reactivite_client > 0) AND K.prestataire_id = 2";
                        
        }

        return $con->fetchAssoc($sql);
    }
    
    public static function tempsRencontreClientConsultantIN($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_client_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            
            if (isset($filters['client_id']) && $filters['client_id'] <> "") {
                $query_client_id = " AND client_id = {$filters['client_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $query_start_date = " AND created_in >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $query_end_date = " AND created_in <= '{$end}' ";
            }                 
            
            $sql = "SELECT prestataire_id, SUM(t_in_date_consultant) as t_in_date_consultant_op, SUM(t_in_date_consultant_date_rencontre_client) as t_in_date_consultant_date_rencontre_client_op, SUM(t_in_date_rencontre_client) as t_in_date_rencontre_client_hf "
                . "FROM kpi_time_in "
                . "WHERE 1=1 " . $query_client_id . $query_mission_id . $query_start_date . $query_end_date
                . "GROUP BY prestataire_id";
            
        } else {
        
            $sql = "SELECT prestataire_id, SUM(t_in_date_consultant) as t_in_date_consultant_op, SUM(t_in_date_consultant_date_rencontre_client) as t_in_date_consultant_date_rencontre_client_op, SUM(t_in_date_rencontre_client) as t_in_date_rencontre_client_hf "
                . "FROM kpi_time_in "
                . "GROUP BY prestataire_id";
            
        }

        return $con->fetchAll($sql);
    }
    
    /**
     * start of KPI manager
     */
    public static function getLastTdcSuiviId() {
        global $con;
        $sql = "SELECT MAX(tdcsuivi_id) as tdcsuivi_id FROM kpi_missions_shortlist";
        $result = $con->fetchAssoc($sql);
        return $result['tdcsuivi_id'];
        
    }
    
    public static function getShortlisteList($last_record_id = null) {
        global $con;
        
        if ($last_record_id <> "") {
            $query = " WHERE T.id > {$last_record_id} ";
        } else {
            $query = " ";
        }
        
        $sql = "SELECT T.id as tdcsuivi_id, T.mission_id, T.candidat_id, M.manager_id, T.user_id, M.date_debut, M.date_fin, T.shortliste, T.shortliste_added "
            . "FROM tdcsuivis T "
            . "LEFT JOIN missions M ON M.id = T.mission_id "
            . $query
            . "ORDER BY T.id ASC";
        return $con->fetchAll($sql);
    }
    
    public static function shortlister_in($data) {
        global $con;

        $con->insert('kpi_missions_shortlist', $data);        
    }
    
    public static function updateShortliste_in($data) {
        global $con;
        $con->update('kpi_missions_shortlist', $data, array('id' => $data['id']));
    }
    
    public static function kpi_cdr_SL ($filters = null) {
        
        global $con;
        
        if ($filters) {
            
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND K.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $query_start_date = " AND K.mission_date_debut >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $query_end_date = " AND K.mission_date_fin <= '{$end}' ";
            }
            
            
            $sql = "SELECT K.mission_id, K.mission_date_debut, K.mission_date_fin, M.manager_name, M.poste, count(*) AS num_shortliste "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND K.shortliste = 1 AND K.manager_id > 0 AND K.shortliste_added IS NOT NULL " . $query_mission_id . $query_cdr_id . $query_mission_status . $query_start_date . $query_end_date
                . "GROUP BY K.mission_id HAVING count(*) >= 3 "
                . "ORDER BY K.mission_id ASC";
            
            $results = $con->fetchAll($sql);
            
            $count = 0;
            foreach ($results as $row) :
                
                $sql = "SELECT K.tdcsuivi_id, K.candidat_id, C.nom, C.prenom, U.firstName, U.lastName, K.mission_date_debut, K.mission_date_fin, K.shortliste_added, K.t_shortliste_from_start_mission, shortliste_in_time "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN candidats C ON C.id = K.candidat_id "
                . "LEFT JOIN users U ON U.id = K.user_id "
                . "WHERE K.shortliste = 1 AND K.manager_id > 0 AND K.shortliste_added IS NOT NULL AND K.mission_id = {$row['mission_id']} " . $query_cdr_id
                . "ORDER BY K.shortliste_added ASC";
                
                $results[$count]['candidats'] = $con->fetchAll($sql);
                
                $count++;
            endforeach;
            
            return $results;
            
        } else {
            
            $sql = "SELECT K.mission_id, K.mission_date_debut, K.mission_date_fin, M.manager_name, M.poste, count(*) AS num_shortliste "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND K.shortliste = 1 AND K.manager_id > 0 AND K.shortliste_added IS NOT NULL "
                . "GROUP BY K.mission_id HAVING count(*) >= 3 "
                . "ORDER BY K.mission_id ASC";
                        
            $results = $con->fetchAll($sql);
            
            $count = 0;
            foreach ($results as $row) :
                
                $sql = "SELECT K.tdcsuivi_id, K.candidat_id, C.nom, C.prenom, U.firstName, U.lastName, K.mission_date_debut, K.mission_date_fin, K.shortliste_added, K.t_shortliste_from_start_mission, shortliste_in_time "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN candidats C ON C.id = K.candidat_id "
                . "LEFT JOIN users U ON U.id = K.user_id "
                . "WHERE K.shortliste = 1 AND K.manager_id > 0 AND K.shortliste_added IS NOT NULL AND K.mission_id = {$row['mission_id']} "
                . "ORDER BY K.shortliste_added ASC";
                
                $results[$count]['candidats'] = $con->fetchAll($sql);
                
                $count++;
            endforeach;
            
            return $results;
            
        }
        
    }
    
    public static function kpi_manager_SL ($filters = null) {
        
        global $con;
        
        if ($filters) {
            
            $query_manager_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $query_manager_id = " AND K.manager_id = {$filters['manager_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND K.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $query_start_date = " AND K.mission_date_debut >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $query_end_date = " AND K.mission_date_fin <= '{$end}' ";
            }
            
            
            $sql = "SELECT K.mission_id, K.mission_date_debut, K.mission_date_fin, M.manager_name, M.poste, count(*) AS num_shortliste "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND K.shortliste = 1 AND K.manager_id > 0 AND K.shortliste_added IS NOT NULL " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_mission_status . $query_start_date . $query_end_date
                . "GROUP BY K.mission_id HAVING count(*) >= 3 "
                . "ORDER BY K.mission_id ASC";
            
            $results = $con->fetchAll($sql);
            
            $count = 0;
            foreach ($results as $row) :
                
                $sql = "SELECT K.tdcsuivi_id, K.candidat_id, C.nom, C.prenom, K.mission_date_debut, K.mission_date_fin, K.shortliste_added, K.t_shortliste_from_start_mission, shortliste_in_time "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN candidats C ON C.id = K.candidat_id "
                . "WHERE K.shortliste = 1 AND K.manager_id > 0 AND K.shortliste_added IS NOT NULL AND K.mission_id = {$row['mission_id']} "
                . "ORDER BY K.shortliste_added ASC";
                
                $results[$count]['candidats'] = $con->fetchAll($sql);
                
                $count++;
            endforeach;
            
            return $results;
            
        } else {
            
            $sql = "SELECT K.mission_id, K.mission_date_debut, K.mission_date_fin, M.manager_name, M.poste, count(*) AS num_shortliste "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND K.shortliste = 1 AND K.manager_id > 0 AND K.shortliste_added IS NOT NULL "
                . "GROUP BY K.mission_id HAVING count(*) >= 3 "
                . "ORDER BY K.mission_id ASC";
                        
            $results = $con->fetchAll($sql);
            
            $count = 0;
            foreach ($results as $row) :
                
                $sql = "SELECT K.tdcsuivi_id, K.candidat_id, C.nom, C.prenom, K.mission_date_debut, K.mission_date_fin, K.shortliste_added, K.t_shortliste_from_start_mission, shortliste_in_time "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN candidats C ON C.id = K.candidat_id "
                . "WHERE K.shortliste = 1 AND K.manager_id > 0 AND K.shortliste_added IS NOT NULL AND K.mission_id = {$row['mission_id']} "
                . "ORDER BY K.shortliste_added ASC";
                
                $results[$count]['candidats'] = $con->fetchAll($sql);
                
                $count++;
            endforeach;
            
            return $results;
            
        }
        
    }
    
    public static function kpi_manager_shortliste_in_time ($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_manager_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $query_manager_id = " AND K.manager_id = {$filters['manager_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND K.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $query_start_date = " AND K.mission_date_debut >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $query_end_date = " AND K.mission_date_fin <= '{$end}' ";
            }
            
            $sql1 = "SELECT count(*) as num_shortliste_in_time, K.mission_id, K.mission_date_debut, K.mission_date_fin, M.manager_name, M.poste "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE K.shortliste = 1 AND K.shortliste_in_time = 1 AND K.manager_id > 0 " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_mission_status . $query_start_date . $query_end_date
                . "GROUP BY K.mission_id "
                . "HAVING count(*) >= 3";
                        
            $result1 = $con->fetchAll($sql1);
                        
            $sql2 = "SELECT count(*) as num_shortliste_not_in_time, K.mission_id, K.mission_date_debut, K.mission_date_fin, M.manager_name, M.poste "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE K.shortliste = 1 AND K.shortliste_in_time = 1 AND K.manager_id > 0 " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_mission_status . $query_start_date . $query_end_date
                . "GROUP BY K.mission_id "
                . "HAVING count(*) = 0";
                        
            $result2 = $con->fetchAll($sql2);
                        
            return array('shortlist_in_time' => $result1, 'shortlist_not_in_time' => $result2);
            
        } else {
            
            // Le nombre de mission où une Short List (3 candidats qui ont ce statut) à été réalisée dans les temps (avant la date de fin de la mission) / le nombre de mission où il n'y a pas eu de Short List dans les temps.
            
            $sql1 = "SELECT count(*) as num_shortliste_in_time, K.mission_id, K.mission_date_debut, K.mission_date_fin, M.manager_name, M.poste FROM kpi_missions_shortlist K LEFT JOIN missions M ON M.id = K.mission_id WHERE K.shortliste = 1 AND K.shortliste_in_time = 1 AND K.manager_id > 0 GROUP BY K.mission_id HAVING count(*) >= 3";

            $result1 = $con->fetchAll($sql1);

            $sql2 = "SELECT count(*) as num_shortliste_not_in_time, K.mission_id, K.mission_date_debut, K.mission_date_fin, M.manager_name, M.poste FROM kpi_missions_shortlist K LEFT JOIN missions M ON M.id = K.mission_id WHERE K.shortliste = 1 AND K.shortliste_in_time = 1 AND K.manager_id > 0 GROUP BY K.mission_id HAVING count(*) = 0";

            $result2 = $con->fetchAll($sql2);

            return array('shortlist_in_time' => $result1, 'shortlist_not_in_time' => $result2);
            
        }
        
    }
    
    public static function kpi_manager_1 ($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_manager_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $query_manager_id = " AND K.manager_id = {$filters['manager_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND K.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $query_start_date = " AND K.shortliste_added >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $query_end_date = " AND K.shortliste_added <= '{$end}' ";
            }
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            $sql1 = "SELECT count(*) as num_shortliste, K.mission_id "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE K.shortliste = 1 AND K.shortliste_in_time = 1 " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_start_date . $query_end_date . $query_mission_status
                . "GROUP BY K.mission_id "
                . "HAVING count(*) > 3";
            
            $result1 = $con->fetchAll($sql1);

            $sql2 = "SELECT count(*) as num_shortliste, K.mission_id "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE K.shortliste = 0 AND K.shortliste_in_time = 0 " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_start_date . $query_end_date . $query_mission_status
                . "GROUP BY K.mission_id";

            $result2 = $con->fetchAll($sql2);

            return count($result1) . " / " . count($result2);
            
        } else {
        
        
            $sql1 = "SELECT count(*) as num_shortliste, mission_id FROM kpi_missions_shortlist WHERE shortliste = 1 AND shortliste_in_time = 1 
    GROUP BY mission_id HAVING count(*) > 3";

            $result1 = $con->fetchAll($sql1);

            $sql2 = "SELECT count(*) as num_shortliste, mission_id FROM kpi_missions_shortlist WHERE shortliste = 0 AND shortliste_in_time = 0 
    GROUP BY mission_id";

            $result2 = $con->fetchAll($sql2);

            return count($result1) . " / " . count($result2);
            
        }
        
    }
    
    public static function kpi_manager_2_new ($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_manager_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $query_manager_id = " AND K.manager_id = {$filters['manager_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND K.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $query_start_date = " AND K.shortliste_added >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $query_end_date = " AND K.shortliste_added <= '{$end}' ";
            }
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            $sql = "SELECT K.*, M.poste, M.manager_name, U.firstName, U.lastName, C.nom as client_name, CA.nom as cand_lastname, CA.prenom as cand_firstname "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "LEFT JOIN users U On U.id = K.user_id "
                . "LEFT JOIN clients C ON C.id = M.client_id "
                . "LEFT JOIN candidats CA ON CA.id = K.candidat_id "
                . "WHERE K.manager_id > 0 AND K.shortliste = 1 AND shortliste_added <> '' " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_start_date . $query_end_date . $query_mission_status;
                        
            return $con->fetchAll($sql);
            
        } else {
                    
            $sql = "SELECT K.*, M.poste, M.manager_name, U.firstName, U.lastName, C.nom as client_name, CA.nom as cand_lastname, CA.prenom as cand_firstname FROM kpi_missions_shortlist K LEFT JOIN missions M ON M.id = K.mission_id LEFT JOIN users U On U.id = K.user_id LEFT JOIN clients C ON C.id = M.client_id LEFT JOIN candidats CA ON CA.id = K.candidat_id WHERE K.manager_id > 0 AND K.shortliste = 1 AND shortliste_added <> ''";
            
            return $con->fetchAll($sql);

            
        }
    }
    
    public static function kpi_manager_2 ($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_manager_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $query_manager_id = " AND K.manager_id = {$filters['manager_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND K.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $query_start_date = " AND K.shortliste_added >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $query_end_date = " AND K.shortliste_added <= '{$end}' ";
            }
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            $sql = "SELECT (SUM(K.t_shortliste_from_start_mission) / count(*)) as avg_time_shortlist "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE K.shortliste = 1 " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_start_date . $query_end_date . $query_mission_status;
                        
            $result = $con->fetchAssoc($sql);

            return seconds_to_hours_minutes(round($result['avg_time_shortlist'], 0));
            
        } else {
        
            $sql = "SELECT (SUM(t_shortliste_from_start_mission) / count(*)) as avg_time_shortlist FROM kpi_missions_shortlist WHERE shortliste = 1";
            $result = $con->fetchAssoc($sql);

            return seconds_to_hours_minutes(round($result['avg_time_shortlist'], 0));
            
        }
    }
    
    public static function kpi_manager_new_1($filters = null) {
        
        global $con;
        
        if ($filters) {
            
            $query_manager_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $query_manager_id = " AND M.manager_id = {$filters['manager_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND M.id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND TS.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start_s = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= '{$start_s}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end_s = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_fin <= '{$end_s}' ";
            }
            
            
            
            // get all missions
            $sql = "SELECT M.id, M.poste, M.manager_name, C.nom as client "
                . "FROM missions M "
                . "LEFT JOIN clients C ON C.id = M.client_id "
                . "WHERE M.include_in_kpi = 1 AND M.manager_id > 0 " . $query_manager_id . $query_mission_id . $query_mission_status . $query_start_date . $query_end_date;
            $results = $con->fetchAll($sql);
                        
            $count = 0;
            foreach($results as $row) :
                
                $sql = "SELECT T.control_statut_tdc, COUNT(*) as num_candidat_by_status "
                . "FROM tdc T "
                . "LEFT JOIN tdcsuivis TS ON TS.mission_id = T.mission_id AND TS.candidat_id = T.candidat_id "
                . "WHERE T.mission_id = {$row['id']} " . $query_cdr_id
                . "GROUP BY T.control_statut_tdc";
                $rows = $con->fetchAll($sql);
                                
                $status_data = array();
                
                foreach($rows as $row) {
                    $status_data[$row['control_statut_tdc']] = $row['num_candidat_by_status'];
                }

                $results[$count]['num_candidat_by_status'] = $status_data;
                
                $count++;
                
            endforeach;
            
            return $results;
            
        } else {
            
            // get all missions
            $sql = "SELECT M.id, M.poste, M.manager_name, C.nom as client FROM missions M LEFT JOIN clients C ON C.id = M.client_id WHERE M.include_in_kpi = 1 AND M.manager_id > 0";
            $results = $con->fetchAll($sql);
                        
            $count = 0;
            foreach($results as $row) :
                
                $sql = "SELECT control_statut_tdc, COUNT(*) as num_candidat_by_status FROM tdc WHERE mission_id = {$row['id']} GROUP BY control_statut_tdc";
                $rows = $con->fetchAll($sql);
                                
                $status_data = array();
                
                foreach($rows as $row) {
                    $status_data[$row['control_statut_tdc']] = $row['num_candidat_by_status'];
                }
                
                $results[$count]['num_candidat_by_status'] = $status_data;
                
                $count++;
            endforeach;
            
            return $results;
            
        }
        
    }
    
    public static function kpi_manager_3 ($filters = null) {
        global $con;
                
        if ($filters) {
            
            $query_manager_id = " ";
            $query_manager_id2 = " ";
            $query_start_date = " ";
            $query_start_date2 = " ";
            $query_end_date = " ";
            $query_end_date2 = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_cdr_id2 = " ";
            $query_mission_status = " ";
            $query_mission_status2 = " ";
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND m.status IN ({$filters['status']}) ";
                $query_mission_status2 = " AND M.status IN ({$filters['status']}) ";
            }  
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $query_manager_id = " AND m.manager_id = {$filters['manager_id']} ";
                $query_manager_id2 = " AND M.manager_id = {$filters['manager_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND w.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND w.user_id = {$filters['cdr_id']} ";
                $query_cdr_id2 = " AND T.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $start_s = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND w.created >= '{$start}' ";
                $query_start_date2 = " AND T.created >= '{$start_s}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $end_s = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND w.created <= '{$end}' ";
                $query_end_date2 = " AND T.created <= '{$end_s}' ";
            } 
            
            $sql = "SELECT w.mission_id, count(*) as num_candidat_in_after, m.poste "
                . "FROM watchdog w "
                . "LEFT JOIN missions m ON m.id = w.mission_id "
                . "WHERE w.action_code = 'mod_tdc_status_candidat' AND w.status_before_id IN (227, 228, 251, 252) AND w.status_after_id = 229 " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_start_date . $query_end_date . $query_mission_status
                . "GROUP BY w.mission_id";
            
            $results = $con->fetchAll($sql);
            
            $count = 0;
            foreach($results as $row) :

                $sql = "SELECT COUNT(*) as num_candidat "
                . "FROM tdc T "
                . "LEFT JOIN missions M ON M.id = T.mission_id "
                . "WHERE T.mission_id = {$row['mission_id']} " . $query_manager_id2 . $query_cdr_id2 . $query_start_date2 . $query_end_date2 . $query_mission_status2;
                $result = $con->fetchAssoc($sql);
                $results[$count]['num_candidat_in_mission'] = $result['num_candidat'];

                $count++;
            endforeach;

            return $results;
            
        } else {
        
        
            $sql = "SELECT w.mission_id, count(*) as num_candidat_in_after, m.poste FROM watchdog w LEFT JOIN missions m ON m.id = w.mission_id WHERE w.action_code = 'mod_tdc_status_candidat' AND w.status_before_id IN (227, 228, 251, 252) AND w.status_after_id = 229 GROUP BY w.mission_id";

            $results = $con->fetchAll($sql);

            $count = 0;
            foreach($results as $row) :

                $sql = "SELECT COUNT(*) as num_candidat FROM tdc WHERE mission_id = {$row['mission_id']}";
                $result = $con->fetchAssoc($sql);
                $results[$count]['num_candidat_in_mission'] = $result['num_candidat'];

                $count++;
            endforeach;

            return $results;
            
        }
    }
    
    public static function kpi_cdr_4_new($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND w.table_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND w.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND m.date_debut >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND m.date_fin <= '{$end}' ";
            }                  
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND m.status IN ({$filters['status']}) ";
            } 
            
            $sql = "SELECT w.table_id, m.id as mission_id, m.manager_name, w.user_id, w.user_name, w.user_role_name, m.poste, m.date_debut, m.date_fin, w.status_before_id, w.status_before, w.status_after_id, w.status_after, w.created as date_changement_statut_mission "
                . "FROM watchdog w "
                . "LEFT JOIN missions m ON m.id = w.table_id "
                . "WHERE m.include_in_kpi = 1 AND w.action_code = 'mod_mission_edit_mission' AND w.status_before_id = 232 AND w.status_after_id <> 232 " . $query_mission_id . $query_cdr_id . $query_start_date . $query_end_date . $query_mission_status
                . "GROUP BY w.table_id " // group by to remove duplicate entry that is the user may have saved several times which add another records
                . "ORDER BY w.table_id ASC";
                        
            return $con->fetchAll($sql);
            
        } else {
        
            $sql = "SELECT w.table_id, m.id as mission_id, m.manager_name, w.user_id, w.user_name, w.user_role_name, m.poste, m.date_debut, m.date_fin, w.status_before_id, w.status_before, w.status_after_id, w.status_after, w.created as date_changement_statut_mission "
                . "FROM watchdog w "
                . "LEFT JOIN missions m ON m.id = w.table_id "
                . "WHERE m.include_in_kpi = 1 AND w.action_code = 'mod_mission_edit_mission' AND w.status_before_id = 232 AND w.status_after_id <> 232 "
                . "GROUP BY w.table_id " // group by to remove duplicate entry that is the user may have saved several times which add another records
                . "ORDER BY w.table_id ASC";

            return $con->fetchAll($sql);

        }
    }
    
    public static function kpi_manager_4_new($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_manager_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $query_manager_id = " AND m.manager_id = {$filters['manager_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND w.table_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND w.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND m.date_debut >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND m.date_fin <= '{$end}' ";
            }                  
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND m.status IN ({$filters['status']}) ";
            } 
            
            $sql = "SELECT w.table_id, m.id as mission_id, m.manager_name, w.user_id, w.user_name, w.user_role_name, m.poste, m.date_debut, m.date_fin, w.status_before_id, w.status_before, w.status_after_id, w.status_after, w.created as date_changement_statut_mission "
                . "FROM watchdog w "
                . "LEFT JOIN missions m ON m.id = w.table_id "
                . "WHERE m.include_in_kpi = 1 AND w.action_code = 'mod_mission_edit_mission' AND w.status_before_id = 232 AND w.status_after_id NOT IN (232, 235, 250) " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_start_date . $query_end_date . $query_mission_status
                . "GROUP BY w.table_id " // group bu to remove duplicate entry that is the user may have saved several times which add another records
                . "ORDER BY w.table_id ASC";
                        
            return $con->fetchAll($sql);
            
        } else {
        
            $sql = "SELECT w.table_id, m.id as mission_id, m.manager_name, w.user_id, w.user_name, w.user_role_name, m.poste, m.date_debut, m.date_fin, w.status_before_id, w.status_before, w.status_after_id, w.status_after, w.created as date_changement_statut_mission "
                . "FROM watchdog w "
                . "LEFT JOIN missions m ON m.id = w.table_id "
                . "WHERE m.include_in_kpi = 1 AND w.action_code = 'mod_mission_edit_mission' AND w.status_before_id = 232 AND w.status_after_id NOT IN (232, 235, 250) "
                . "GROUP BY w.table_id " // group by to remove duplicate entry that is the user may have saved several times which add another records
                . "ORDER BY w.table_id ASC";

            return $con->fetchAll($sql);

        }
    }
    
    public static function kpi_manager_4($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_manager_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $query_manager_id = " AND m.manager_id = {$filters['manager_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND w.table_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND w.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $query_start_date = " AND w.created >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $query_end_date = " AND w.created <= '{$end}' ";
            }                  
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND m.status IN ({$filters['status']}) ";
            } 
            
            $sql = "SELECT (SUM(UNIX_TIMESTAMP(w.created)) - SUM(m.date_debut))/count(DISTINCT(w.table_id)) as avg_time "
                . "FROM watchdog w "
                . "LEFT JOIN missions m ON m.id = w.table_id "
                . "WHERE w.action_code = 'mod_mission_edit_mission' AND w.status_before_id = 232 AND w.status_after_id <> 232 " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_start_date . $query_end_date . $query_mission_status;

            $result = $con->fetchAssoc($sql);

            return seconds_to_hours_minutes(round($result['avg_time'], 0));
            
        } else {
        
            $sql = "SELECT (SUM(UNIX_TIMESTAMP(w.created)) - SUM(m.date_debut))/count(DISTINCT(w.table_id)) as avg_time "
                . "FROM watchdog w "
                . "LEFT JOIN missions m ON m.id = w.table_id "
                . "WHERE w.action_code = 'mod_mission_edit_mission' AND w.status_before_id = 232 AND w.status_after_id <> 232";

            $result = $con->fetchAssoc($sql);

            return seconds_to_hours_minutes(round($result['avg_time'], 0));
        }
    }
    
    public static function kpi_cdr_3cx ($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            $query_to_cdr_id = " ";
            $query_from_cdr_id = " ";
            $query_call_start_date = " ";
            $query_call_end_date = " ";   
            
                        
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                
                // get cdr extension
                $user = User::getUserById($filters['cdr_id']);
                $ext = "Ext." . $user['voip_ext'];
                
                ///$query_cdr_id = " AND (K.from_user_id = {$filters['cdr_id']} OR K.to_user_id = {$filters['cdr_id']}) ";
                $query_cdr_id = " AND (K.from_no = \"{$ext}\" OR K.to_no = \"{$ext}\") ";
                
                ///$query_to_cdr_id = " AND K.to_user_id = {$filters['cdr_id']} ";
                $query_to_cdr_id = " AND K.to_no = \"{$ext}\" ";
                
                //$query_from_cdr_id = " AND K.from_user_id = {$filters['cdr_id']} ";
                $query_from_cdr_id = " AND K.from_no = \"{$ext}\" ";
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }             
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= '{$start}' ";
                
                $start_s = $filters['start_date'] . " 00:00:01";
                $query_call_start_date = " AND K.created >= '{$start_s}' ";
                
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_fin <= '{$end}' ";
                
                $end_s = $filters['end_date'] . " 23:59:59";
                $query_call_end_date = " AND K.created <= '{$end_s}' ";
                
            }
            
            //////////////////// NEW ////////////////////////
            
            $total_calls = array();
            
            foreach(self::$reasons_terminated as $key => $value) :
                
                $sql = "SELECT count(*) as total_call_sortants, K.reason_terminated, SUM(K.duration) as total_time "
                . "FROM kpi_cdrs K "
                . "WHERE K.reason_terminated = '{$key}' " . $query_from_cdr_id . $query_call_start_date . $query_call_end_date
                . "GROUP BY K.reason_terminated";
                                
                $recordOut = $con->fetchAssoc($sql);
                
                $total_calls[$key]['out'] = array(
                    'total_calls' => $recordOut['total_call_sortants'],
                    'total_time' => $recordOut['total_time'],
                );
                
                $sql = "SELECT count(*) as total_call_entrants, K.reason_terminated, SUM(K.duration) as total_time "
                . "FROM kpi_cdrs K "
                . "WHERE K.reason_terminated = '{$key}' " . $query_to_cdr_id . $query_call_start_date . $query_call_end_date
                . "GROUP BY K.reason_terminated";
                                
                $recordIn = $con->fetchAssoc($sql);
                
                $total_calls[$key]['in'] = array(
                    'total_calls' => $recordIn['total_call_entrants'],
                    'total_time' => $recordIn['total_time'],
                );
                
            endforeach;
            
            /// debug($total_calls);
            
            //////////////////// END NEW ////////////////////
            
            // total number of calls in/out and its time
            /// $sql = "SELECT count(*) as total_calls, reason_terminated, SUM(duration) as total_time FROM kpi_cdrs WHERE duration > 0 AND reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc', 'TerminatedByRule') GROUP BY reason_terminated";
//            $sql = "SELECT count(*) as total_calls, K.reason_terminated, SUM(K.duration) as total_time "
//                . "FROM kpi_cdrs K "
//                . "LEFT JOIN missions M ON M.id = K.mission_id "
//                . "WHERE 1=1 " . $query_mission_id . $query_cdr_id . $query_mission_status . $query_start_date . $query_end_date 
//                . "GROUP BY K.reason_terminated";
//                        
//            $total_calls = $con->fetchAll($sql);
            
            // total calls grouped by mission
            $sql = "SELECT K.mission_id, M.poste, M.manager_id, M.manager_name, M.status, count(*) as total_appels, SUM(duration) as total_time "
                . "FROM kpi_cdrs K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND K.reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND mission_id > 0 " . $query_mission_id . $query_cdr_id . $query_mission_status . $query_call_start_date . $query_call_end_date 
                . "GROUP BY K.mission_id "
                . "ORDER BY K.mission_id DESC";
                                    
            $total_calls_grouped_by_mission = $con->fetchAll($sql);
            
            $count = 0;
            foreach($total_calls_grouped_by_mission as $calls) :
                
                // appels sortants
                $sql1 = "SELECT count(*) as total_calls_sortants, SUM(K.duration) as total_time_sortants "
                . "FROM kpi_cdrs K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE K.reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND K.mission_id = {$calls['mission_id']} " . $query_mission_id . $query_from_cdr_id . $query_mission_status . $query_call_start_date . $query_call_end_date;                
                $result1 = $con->fetchAssoc($sql1);
                $total_calls_grouped_by_mission[$count]['total_calls_sortants'] = $result1['total_calls_sortants'];
                $total_calls_grouped_by_mission[$count]['total_time_sortants'] = $result1['total_time_sortants'];
                                
                // appels entrants
                $sql2 = "SELECT count(*) as total_calls_entrants, SUM(K.duration) as total_time_entrants "
                    . "FROM kpi_cdrs K "
                    . "LEFT JOIN missions M ON M.id = K.mission_id "
                    . "WHERE K.reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND K.mission_id = {$calls['mission_id']} " . $query_mission_id . $query_to_cdr_id . $query_mission_status . $query_call_start_date . $query_call_end_date ;
                $result2 = $con->fetchAssoc($sql2);
                $total_calls_grouped_by_mission[$count]['total_calls_entrants'] = $result2['total_calls_entrants'];
                $total_calls_grouped_by_mission[$count]['total_time_entrants'] = $result2['total_time_entrants'];
                                
                $count++;
            endforeach;
                        
            return array(
                'total_calls' => $total_calls,
                'total_calls_grouped_by_mission' => $total_calls_grouped_by_mission,
            );
            
        } else {
            
            //////////////////// NEW ////////////////////////
            
            $total_calls = array();
            
            foreach(self::$reasons_terminated as $key => $value) :
                
                $sql = "SELECT count(*) as total_call_sortants, reason_terminated, SUM(duration) as total_time "
                . "FROM kpi_cdrs "
                . "WHERE from_no LIKE 'Ext.%' AND reason_terminated = '{$key}' "
                . "GROUP BY reason_terminated";
                
                $recordOut = $con->fetchAssoc($sql);
                
                $total_calls[$key]['out'] = array(
                    'total_calls' => $recordOut['total_call_sortants'],
                    'total_time' => $recordOut['total_time'],
                );
                
                $sql = "SELECT count(*) as total_call_entrants, reason_terminated, SUM(duration) as total_time "
                . "FROM kpi_cdrs "
                . "WHERE to_no LIKE 'Ext.%' AND reason_terminated = '{$key}' "
                . "GROUP BY reason_terminated";
                
                $recordIn = $con->fetchAssoc($sql);
                
                $total_calls[$key]['in'] = array(
                    'total_calls' => $recordIn['total_call_entrants'],
                    'total_time' => $recordIn['total_time'],
                );
                
            endforeach;
                        
            //////////////////// END NEW ////////////////////
            
            // total number of calls in/out and its time
            /// $sql = "SELECT count(*) as total_calls, reason_terminated, SUM(duration) as total_time FROM kpi_cdrs WHERE duration > 0 AND reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc', 'TerminatedByRule') GROUP BY reason_terminated";
            ////$sql = "SELECT count(*) as total_calls, reason_terminated, SUM(duration) as total_time FROM kpi_cdrs GROUP BY reason_terminated";
            ////$total_calls = $con->fetchAll($sql);
            
            // total calls grouped by mission
            $sql = "SELECT K.mission_id, M.poste, M.manager_id, M.manager_name, M.status, count(*) as total_appels, SUM(duration) as total_time FROM kpi_cdrs K LEFT JOIN missions M ON M.id = K.mission_id WHERE M.include_in_kpi = 1 AND reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') GROUP BY K.mission_id ORDER BY K.mission_id DESC";
            $total_calls_grouped_by_mission = $con->fetchAll($sql);
            
            $count = 0;
            foreach($total_calls_grouped_by_mission as $calls) :
                
                // appels sortants
                ///$sql1 = "SELECT count(*) as total_calls_sortants, SUM(K.duration) as total_time_sortants FROM kpi_cdrs K WHERE reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND K.mission_id = {$calls['mission_id']} AND from_user_id > 0";
                $sql1 = "SELECT count(*) as total_calls_sortants, SUM(K.duration) as total_time_sortants FROM kpi_cdrs K WHERE reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND K.mission_id = {$calls['mission_id']} AND from_no LIKE 'Ext.%'";
                
                $result1 = $con->fetchAssoc($sql1);
                $total_calls_grouped_by_mission[$count]['total_calls_sortants'] = $result1['total_calls_sortants'];
                $total_calls_grouped_by_mission[$count]['total_time_sortants'] = $result1['total_time_sortants'];
                                
                // appels entrants
                ///$sql2 = "SELECT count(*) as total_calls_entrants, SUM(K.duration) as total_time_entrants FROM kpi_cdrs K WHERE reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND K.mission_id = {$calls['mission_id']} AND to_user_id > 0";
                $sql2 = "SELECT count(*) as total_calls_entrants, SUM(K.duration) as total_time_entrants FROM kpi_cdrs K WHERE reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND K.mission_id = {$calls['mission_id']} AND to_no LIKE 'Ext.%'";
                $result2 = $con->fetchAssoc($sql2);
                $total_calls_grouped_by_mission[$count]['total_calls_entrants'] = $result2['total_calls_entrants'];
                $total_calls_grouped_by_mission[$count]['total_time_entrants'] = $result2['total_time_entrants'];
                                
                $count++;
            endforeach;
            
            // appels messagerie
            $sql = "SELECT count(*) as total_calls_messagerie, SUM(K.duration) as total_time FROM kpi_cdrs K WHERE reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND K.from_no = 'Playfile'";
            $messagerie = $con->fetchAssoc($sql);
            
            return array(
                'total_calls' => $total_calls,
                'total_calls_grouped_by_mission' => $total_calls_grouped_by_mission,
                'messagerie' => $messagerie,
            );
            
        }
        
        
    }
    
    public static function kpi_manager_3cx ($filters = null) {
        global $con;
        
        if ($filters) {
            
            $query_manager_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            $query_to_cdr_id = " ";
            $query_from_cdr_id = " "; 
            
            $query_call_start_date = " ";
            $query_call_end_date = " "; 
            
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $query_manager_id = " AND K.mission_manager_id = {$filters['manager_id']} ";
            } 
            
            if ((isset($filters['manager_id']) && $filters['manager_id'] <> "") && (!isset($filters['cdr_id'])))  {
                // get all users extension of the manager
                $users = User::getCDRAssistantManagerByManagerId($filters['manager_id']);
                $exts = array();
                foreach($users as $user):
                    $exts[] = 'Ext.' . $user['voip_ext'];
                endforeach;
                                
                $query_to_cdr_id = " AND K.to_no IN (" .  addSeperatorClosure($exts, ",") . ") ";
                                
                $query_from_cdr_id = " AND K.from_no IN (" .  addSeperatorClosure($exts, ",") . ") ";
                
                $query_cdr_id = " AND ( K.to_no IN (" . addSeperatorClosure($exts, ",") .") OR K.from_no IN (" .  addSeperatorClosure($exts, ",") . ") )";
                
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                
                // get cdr extension
                $user = User::getUserById($filters['cdr_id']);
                $ext = "Ext." . $user['voip_ext'];
                
                ///$query_cdr_id = " AND (K.from_user_id = {$filters['cdr_id']} OR K.to_user_id = {$filters['cdr_id']}) ";
                $query_cdr_id = " AND (K.from_no = \"{$ext}\" OR K.to_no = \"{$ext}\") ";
                
                ///$query_to_cdr_id = " AND K.to_user_id = {$filters['cdr_id']} ";
                $query_to_cdr_id = " AND K.to_no = \"{$ext}\" ";
                
                //$query_from_cdr_id = " AND K.from_user_id = {$filters['cdr_id']} ";
                $query_from_cdr_id = " AND K.from_no = \"{$ext}\" ";
                
                
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= '{$start}' ";
                
                $start_s = $filters['start_date'] . " 00:00:01";
                $query_call_start_date = " AND K.created >= '{$start_s}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_fin <= '{$end}' ";
                
                $end_s = $filters['end_date'] . " 23:59:59";
                $query_call_end_date = " AND K.created <= '{$end_s}' ";
            }
            
            
            //////////////////// NEW ////////////////////////
            
            $total_calls = array();
            
            foreach(self::$reasons_terminated as $key => $value) :
                
                $sql = "SELECT count(*) as total_call_sortants, K.reason_terminated, SUM(K.duration) as total_time "
                . "FROM kpi_cdrs K "
                . "WHERE K.reason_terminated = '{$key}' " . $query_from_cdr_id . $query_call_start_date . $query_call_end_date
                . "GROUP BY K.reason_terminated";
                                
                $recordOut = $con->fetchAssoc($sql);
                
                $total_calls[$key]['out'] = array(
                    'total_calls' => $recordOut['total_call_sortants'],
                    'total_time' => $recordOut['total_time'],
                );
                
                $sql = "SELECT count(*) as total_call_entrants, K.reason_terminated, SUM(K.duration) as total_time "
                . "FROM kpi_cdrs K "
                . "WHERE K.reason_terminated = '{$key}' " . $query_to_cdr_id . $query_call_start_date . $query_call_end_date
                . "GROUP BY K.reason_terminated";
                                
                $recordIn = $con->fetchAssoc($sql);
                
                $total_calls[$key]['in'] = array(
                    'total_calls' => $recordIn['total_call_entrants'],
                    'total_time' => $recordIn['total_time'],
                );
                
            endforeach;
            
            // total calls grouped by mission
            $sql = "SELECT K.mission_id, M.poste, M.manager_id, M.manager_name, M.status, count(*) as total_appels, SUM(duration) as total_time "
                . "FROM kpi_cdrs K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE M.include_in_kpi = 1 AND K.reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_mission_status . $query_call_start_date . $query_call_end_date 
                . "GROUP BY K.mission_id "
                . "ORDER BY K.mission_id DESC";
                        
            $total_calls_grouped_by_mission = $con->fetchAll($sql);
            
            $count = 0;
            foreach($total_calls_grouped_by_mission as $calls) :
                
                // appels sortants
                $sql1 = "SELECT count(*) as total_calls_sortants, SUM(K.duration) as total_time_sortants "
                . "FROM kpi_cdrs K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE K.reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND K.mission_id = {$calls['mission_id']} " . $query_manager_id . $query_mission_id . $query_from_cdr_id . $query_mission_status . $query_call_start_date . $query_call_end_date;
                                
                $result1 = $con->fetchAssoc($sql1);
                $total_calls_grouped_by_mission[$count]['total_calls_sortants'] = $result1['total_calls_sortants'];
                $total_calls_grouped_by_mission[$count]['total_time_sortants'] = $result1['total_time_sortants'];
                                
                // appels entrants
                $sql2 = "SELECT count(*) as total_calls_entrants, SUM(K.duration) as total_time_entrants "
                    . "FROM kpi_cdrs K "
                    . "LEFT JOIN missions M ON M.id = K.mission_id "
                    . "WHERE K.reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND K.mission_id = {$calls['mission_id']} " . $query_manager_id . $query_mission_id . $query_to_cdr_id . $query_mission_status . $query_call_start_date . $query_call_end_date;    
                    
                $result2 = $con->fetchAssoc($sql2);
                $total_calls_grouped_by_mission[$count]['total_calls_entrants'] = $result2['total_calls_entrants'];
                $total_calls_grouped_by_mission[$count]['total_time_entrants'] = $result2['total_time_entrants'];
                                
                $count++;
            endforeach;
                        
            return array(
                'total_calls' => $total_calls,
                'total_calls_grouped_by_mission' => $total_calls_grouped_by_mission,
            );
            
        } else {
            
            //////////////////// NEW ////////////////////////
            
            $total_calls = array();
            
            foreach(self::$reasons_terminated as $key => $value) :
                
                $sql = "SELECT count(*) as total_call_sortants, reason_terminated, SUM(duration) as total_time "
                . "FROM kpi_cdrs "
                . "WHERE from_no LIKE 'Ext.%' AND reason_terminated = '{$key}' "
                . "GROUP BY reason_terminated";
                
                $recordOut = $con->fetchAssoc($sql);
                
                $total_calls[$key]['out'] = array(
                    'total_calls' => $recordOut['total_call_sortants'],
                    'total_time' => $recordOut['total_time'],
                );
                
                $sql = "SELECT count(*) as total_call_entrants, reason_terminated, SUM(duration) as total_time "
                . "FROM kpi_cdrs "
                . "WHERE to_no LIKE 'Ext.%' AND reason_terminated = '{$key}' "
                . "GROUP BY reason_terminated";
                
                $recordIn = $con->fetchAssoc($sql);
                
                $total_calls[$key]['in'] = array(
                    'total_calls' => $recordIn['total_call_entrants'],
                    'total_time' => $recordIn['total_time'],
                );
                
            endforeach;
                        
            
            // total calls grouped by mission
            $sql = "SELECT K.mission_id, M.poste, M.manager_id, M.manager_name, M.status, count(*) as total_appels, SUM(duration) as total_time FROM kpi_cdrs K LEFT JOIN missions M ON M.id = K.mission_id WHERE M.include_in_kpi = 1 AND reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') GROUP BY K.mission_id ORDER BY K.mission_id DESC";
            $total_calls_grouped_by_mission = $con->fetchAll($sql);
            
            $count = 0;
            foreach($total_calls_grouped_by_mission as $calls) :
                
                // appels sortants
                $sql1 = "SELECT count(*) as total_calls_sortants, SUM(K.duration) as total_time_sortants FROM kpi_cdrs K WHERE reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND K.mission_id = {$calls['mission_id']} AND from_no LIKE 'Ext.%'";
                $result1 = $con->fetchAssoc($sql1);
                $total_calls_grouped_by_mission[$count]['total_calls_sortants'] = $result1['total_calls_sortants'];
                $total_calls_grouped_by_mission[$count]['total_time_sortants'] = $result1['total_time_sortants'];
                                
                // appels entrants
                $sql2 = "SELECT count(*) as total_calls_entrants, SUM(K.duration) as total_time_entrants FROM kpi_cdrs K WHERE reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND K.mission_id = {$calls['mission_id']} AND to_no LIKE 'Ext.%'";
                $result2 = $con->fetchAssoc($sql2);
                $total_calls_grouped_by_mission[$count]['total_calls_entrants'] = $result2['total_calls_entrants'];
                $total_calls_grouped_by_mission[$count]['total_time_entrants'] = $result2['total_time_entrants'];
                                
                $count++;
            endforeach;
            
            // appels messagerie
            $sql = "SELECT count(*) as total_calls_messagerie, SUM(K.duration) as total_time FROM kpi_cdrs K WHERE reason_terminated IN ('TerminatedByDst', 'TerminatedBySrc') AND K.from_no = 'Playfile'";
            $messagerie = $con->fetchAssoc($sql);
            
            return array(
                'total_calls' => $total_calls,
                'total_calls_grouped_by_mission' => $total_calls_grouped_by_mission,
                'messagerie' => $messagerie,
            );
            
        }
        
        
    } 
    
    public static function kpi_manager_5($filters = null) {
        global $con;
                
        if ($filters) {
            
            $query_start_date = " ";
            $query_end_date = " ";
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "" && !isset($filters['mission_id']) && !isset($filters['cdr_id']) ) {
                
                // get all missions users id including manager_id
                if (isset($filters['status']) && $filters['status'] <> "") { 
                    $sql = "SELECT MU.user_id FROM missionsusers MU LEFT JOIN missions M ON M.id = MU.mission_id WHERE M.manager_id = {$filters['manager_id']} AND M.status IN ({$filters['status']})";
                } else {
                    $sql = "SELECT MU.user_id FROM missionsusers MU LEFT JOIN missions M ON M.id = MU.mission_id WHERE M.manager_id = {$filters['manager_id']}";
                }

                $user_ids = $con->fetchAll($sql);
                
                $ids = array();
                foreach($user_ids as $id) :
                    $ids[] = $id['user_id'];
                endforeach;
                
                $user_id = addSeperator($ids, ',');
                                
                $sql = "SELECT voip_ext FROM users WHERE voip_ext <> '' AND id IN ($user_id)";
                
                $extensions = $con->fetchAll($sql);
                
                if (empty($extensions)) {
                    return "Le CDR n'a pas une extension et SDA";
                }
                
                // in table kpi_cdrs field from_no e.g. Ext.764
                $voip_ext = array();
                foreach($extensions as $ext):
                    // must check of ext is 3 characters
                    if (strlen($ext['voip_ext']) == 3) {
                        $voip_ext[] = "Ext." . $ext['voip_ext'];
                    }
                endforeach;

                $ext = addSeperatorClosure($voip_ext, ',');

                if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                    $start = $filters['start_date'] . " 00:00:01";
                    $query_start_date = " AND time_start >= '{$start}' ";
                }

                if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                    $end = $filters['end_date'] . " 23:59:59";
                    $query_end_date = " AND time_end <= '{$end}' ";
                }

                // now we get all outbound calls from kpi_cdrs
                $sql = "SELECT count(*) as outbound_calls FROM kpi_cdrs WHERE from_no IN ({$ext}) " . $query_start_date . $query_end_date; 
                
                $result = $con->fetchAssoc($sql);
                $num_outbound_calls = $result['outbound_calls'];
                $avg_outbound_calls = round($num_outbound_calls/count($voip_ext), 0);

                // inbound calls
                $sql = "SELECT count(*) as inbound_calls FROM kpi_cdrs WHERE to_no IN ({$ext}) " . $query_start_date . $query_end_date;     
                $result = $con->fetchAssoc($sql);
                $num_inbound_calls = $result['inbound_calls'];
                $avg_inbound_calls = round($num_inbound_calls/count($voip_ext), 0);
                
                return array('outbound' => $avg_outbound_calls, 'inbound' => $avg_inbound_calls);
                
            } 
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "" && isset($filters['mission_id']) && !isset($filters['cdr_id'])) {                
                // get all missions users id including manager_id
                if (isset($filters['status']) && $filters['status'] <> "") { 
                    $sql = "SELECT MU.user_id FROM missionsusers MU LEFT JOIN missions M ON M.id = MU.mission_id WHERE M.manager_id = {$filters['manager_id']} AND MU.mission_id = {$filters['mission_id']} AND M.status IN ({$filters['status']})";
                    
                } else {
                    $sql = "SELECT MU.user_id FROM missionsusers MU LEFT JOIN missions M ON M.id = MU.mission_id WHERE M.manager_id = {$filters['manager_id']} AND MU.mission_id = {$filters['mission_id']}";
                }
                              
                $user_ids = $con->fetchAll($sql);
                                
                $ids = array();
                foreach($user_ids as $id) :
                    $ids[] = $id['user_id'];
                endforeach;
                
                $user_id = addSeperator($ids, ',');
                                                
                $sql = "SELECT voip_ext FROM users WHERE voip_ext <> '' AND id IN ($user_id)";
                
                $extensions = $con->fetchAll($sql);
                
                if (empty($extensions)) {
                    return "Le CDR n'a pas une extension et SDA";
                }
                
                // in table kpi_cdrs field from_no e.g. Ext.764
                $voip_ext = array();
                foreach($extensions as $ext):
                    // must check of ext is 3 characters
                    if (strlen($ext['voip_ext']) == 3) {
                        $voip_ext[] = "Ext." . $ext['voip_ext'];
                    }
                endforeach;

                $ext = addSeperatorClosure($voip_ext, ',');

                if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                    $start = $filters['start_date'] . " 00:00:01";
                    $query_start_date = " AND time_start >= '{$start}' ";
                }

                if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                    $end = $filters['end_date'] . " 23:59:59";
                    $query_end_date = " AND time_end <= '{$end}' ";
                }

                // now we get all outbound calls from kpi_cdrs
                $sql = "SELECT count(*) as outbound_calls FROM kpi_cdrs WHERE from_no IN ({$ext}) " . $query_start_date . $query_end_date; 
                
                $result = $con->fetchAssoc($sql);
                $num_outbound_calls = $result['outbound_calls'];
                $avg_outbound_calls = round($num_outbound_calls/count($voip_ext), 0);

                // inbound calls
                $sql = "SELECT count(*) as inbound_calls FROM kpi_cdrs WHERE to_no IN ({$ext}) " . $query_start_date . $query_end_date;     
                $result = $con->fetchAssoc($sql);
                $num_inbound_calls = $result['inbound_calls'];
                $avg_inbound_calls = round($num_inbound_calls/count($voip_ext), 0);
                
                return array('outbound' => $avg_outbound_calls, 'inbound' => $avg_inbound_calls);               
                
            } 
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "" && isset($filters['mission_id']) && isset($filters['cdr_id'])) {
                             
                $sql = "SELECT voip_ext FROM users WHERE voip_ext <> '' AND id = {$filters['cdr_id']}";
                
                $extensions = $con->fetchAll($sql);
                
                if (empty($extensions)) {
                    return "Le CDR n'a pas une extension et SDA";
                }
                
                // in table kpi_cdrs field from_no e.g. Ext.764
                $voip_ext = array();
                foreach($extensions as $ext):
                    // must check of ext is 3 characters
                    if (strlen($ext['voip_ext']) == 3) {
                        $voip_ext[] = "Ext." . $ext['voip_ext'];
                    }
                endforeach;

                $ext = addSeperatorClosure($voip_ext, ',');
                                
                if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                    $start = $filters['start_date'] . " 00:00:01";
                    $query_start_date = " AND time_start >= '{$start}' ";
                }

                if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                    $end = $filters['end_date'] . " 23:59:59";
                    $query_end_date = " AND time_end <= '{$end}' ";
                }

                // now we get all outbound calls from kpi_cdrs
                $sql = "SELECT count(*) as outbound_calls FROM kpi_cdrs WHERE from_no IN ({$ext}) " . $query_start_date . $query_end_date; 
                
                $result = $con->fetchAssoc($sql);
                $num_outbound_calls = $result['outbound_calls'];
                $avg_outbound_calls = round($num_outbound_calls/count($voip_ext), 0);

                // inbound calls
                $sql = "SELECT count(*) as inbound_calls FROM kpi_cdrs WHERE to_no IN ({$ext}) " . $query_start_date . $query_end_date;     
                $result = $con->fetchAssoc($sql);
                $num_inbound_calls = $result['inbound_calls'];
                $avg_inbound_calls = round($num_inbound_calls/count($voip_ext), 0);
                
                return array('outbound' => $avg_outbound_calls, 'inbound' => $avg_inbound_calls);  
                
            }

        } else {
        
            // get extensions of users
            $sql = "SELECT voip_ext FROM users WHERE role_id IN (4, 2) AND voip_ext <> ''";
            
            $extensions = $con->fetchAll($sql);

            // in table kpi_cdrs field from_no e.g. Ext.764
            $voip_ext = array();
            foreach($extensions as $ext):
                // must check of ext is 3 characters
                if (strlen($ext['voip_ext']) == 3) {
                    $voip_ext[] = "Ext." . $ext['voip_ext'];
                }
            endforeach;

            $ext = addSeperatorClosure($voip_ext, ',');

            // now we get all outbound calls from kpi_cdrs
            $sql = "SELECT count(*) as outbound_calls FROM kpi_cdrs WHERE from_no IN ({$ext})";        
            $result = $con->fetchAssoc($sql);
            $num_outbound_calls = $result['outbound_calls'];
            $avg_outbound_calls = round($num_outbound_calls/count($voip_ext), 0);

            // inbound calls
            $sql = "SELECT count(*) as inbound_calls FROM kpi_cdrs WHERE to_no IN ({$ext})";
            $result = $con->fetchAssoc($sql);
            $num_inbound_calls = $result['inbound_calls'];
            $avg_inbound_calls = round($num_inbound_calls/count($voip_ext), 0);

            return array('outbound' => $avg_outbound_calls, 'inbound' => $avg_inbound_calls);
            
        }
 
    }
    
    public static function kpi_cdr_6_new ($filters = null) {
        global $con;
        
        // chasse = 97
        // field control_source_id in table candidats
        
        if ($filters) {
            
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
                        
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND T.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND T.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start_s = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= '{$start_s}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end_s = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_fin <= '{$end_s}' ";
            }
            
            $sql = "SELECT count(*) as num_candidats, C.control_source_id "
                . "FROM candidats C "
                . "LEFT JOIN tdc T ON T.candidat_id = C.id "
                . "LEFT JOIN missions M ON M.id = T.mission_id "
                . "WHERE M.include_in_kpi = 1 " . $query_mission_id . $query_cdr_id . $query_mission_status . $query_start_date . $query_end_date
                . "GROUP BY C.control_source_id";
            
            return $con->fetchAll($sql);
            
            
        } else {
            
            $sql = "SELECT count(*) as num_candidats, control_source_id FROM candidats GROUP BY control_source_id";
                    
            return $con->fetchAll($sql);
            
        }
    }
    
    public static function kpi_manager_6_new ($filters = null) {
        global $con;
        
        // chasse = 97
        // field control_source_id in table candidats
        
        if ($filters) {
            
            $query_manager_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $query_manager_id = " AND M.manager_id = {$filters['manager_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND T.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND T.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start_s = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= '{$start_s}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end_s = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_fin <= '{$end_s}' ";
            }
            
            $sql = "SELECT count(*) as num_candidats, C.control_source_id "
                . "FROM candidats C "
                . "LEFT JOIN tdc T ON T.candidat_id = C.id "
                . "LEFT JOIN missions M ON M.id = T.mission_id "
                . "WHERE M.include_in_kpi = 1 " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_mission_status . $query_start_date . $query_end_date
                . "GROUP BY C.control_source_id";
            
            return $con->fetchAll($sql);
            
            
        } else {
            
            $sql = "SELECT count(*) as num_candidats, control_source_id FROM candidats GROUP BY control_source_id";
                    
            return $con->fetchAll($sql);
            
        }
    }
    
    public static function kpi_manager_6($filters = null) {
        global $con;
                
        if ($filters) {
            
            if (isset($filters['manager_id']) && !isset($filters['mission_id']) && !isset($filters['cdr_id'])) {
                
                // get all user ids under this manager including manager id
                if (isset($filters['status']) && $filters['status'] <> "") {
                    $sql = "SELECT U.id FROM users U LEFT JOIN missionsusers MU ON MU.user_id = U.id LEFT JOIN missions M ON M.id = MU.mission_id WHERE (U.user_id = {$filters['manager_id']} OR U.id = {$filters['manager_id']}) AND M.status IN({$filters['status']})";
                } else {
                    $sql = "SELECT id FROM users WHERE user_id = {$filters['manager_id']} OR id = {$filters['manager_id']}";
                }
                
                $results = $con->fetchAll($sql);
                
                $user_ids = array();
                foreach($results as $id) :
                    $user_ids[] = $id['id'];
                endforeach;
                
                $user_ids = addSeperator($user_ids, ',');
                
                $sql = "SELECT count(*)/(SELECT count(*) FROM candidats WHERE control_source_id <> 97 AND user_id IN ({$user_ids})) as ratio_source FROM candidats WHERE control_source_id = 97 AND user_id IN ({$user_ids})";
        
                $result = $con->fetchAssoc($sql);

                return $result['ratio_source'];
                
            } else if (isset($filters['manager_id']) && isset($filters['mission_id']) && !isset($filters['cdr_id'])) {
                                
                // get all user ids under this manager including manager id
                
                if (isset($filters['status']) && $filters['status'] <> "") {                    
                    $sql = "SELECT MU.user_id FROM missionsusers MU LEFT JOIN missions M ON M.id = MU.mission_id WHERE MU.mission_id = {$filters['mission_id']} AND M.status IN({$filters['status']})";
                    
                } else {
                    $sql = "SELECT user_id FROM missionsusers WHERE mission_id = {$filters['mission_id']}";
                }
                
                $results = $con->fetchAll($sql);
                
                $user_ids = array();
                foreach($results as $id) :
                    $user_ids[] = $id['user_id'];
                endforeach;
                
                $user_ids = addSeperator($user_ids, ',');
                                
                // get all candidats ids from the mission
                $sql = "SELECT candidat_id FROM tdc WHERE mission_id = {$filters['mission_id']}";
                $results = $con->fetchAll($sql);
                
                $candidat_ids = array();
                foreach($results as $id) :
                    $candidat_ids[] = $id['candidat_id'];
                endforeach;
                
                $candidat_ids = addSeperator($candidat_ids, ',');   
                
                if ($candidat_ids == "") {
                    return "0";
                } else {
                    $sql = "SELECT count(*)/(SELECT count(*) FROM candidats WHERE control_source_id <> 97 AND user_id IN ({$user_ids}) AND id IN ({$candidat_ids})) as ratio_source FROM candidats WHERE control_source_id = 97 AND user_id IN ({$user_ids}) AND id IN ({$candidat_ids})";
                
                
                    $result = $con->fetchAssoc($sql);

                    return $result['ratio_source'];
                }
                
                
                
            } else if (isset($filters['manager_id']) && $filters['manager_id'] <> "" && isset($filters['mission_id']) && isset($filters['cdr_id'])) {
                                                
                // get all candidats ids from the mission
                if (isset($filters['status']) && $filters['status'] <> "") {                                        
                    $sql = "SELECT T.candidat_id FROM tdc T LEFT JOIN missions M ON M.id = T.mission_id WHERE T.mission_id = {$filters['mission_id']} AND M.manager_id = {$filters['manager_id']} AND M.status IN({$filters['status']})";
                } else {
                    $sql = "SELECT T.candidat_id FROM tdc T LEFT JOIN missions M ON M.id = T.mission_id WHERE T.mission_id = {$filters['mission_id']} AND M.manager_id = {$filters['manager_id']}";
                }
                                                
                $results = $con->fetchAll($sql);
                
                $candidat_ids = array();
                foreach($results as $id) :
                    $candidat_ids[] = $id['candidat_id'];
                endforeach;
                
                $candidat_ids = addSeperator($candidat_ids, ',');                
                
                if ($candidat_ids == "") {
                    
                    return "0";
                    
                } else {
                    
                    $sql = "SELECT count(*)/(SELECT count(*) FROM candidats WHERE control_source_id <> 97 AND user_id IN ({$filters['cdr_id']}) AND id IN ({$candidat_ids})) as ratio_source FROM candidats WHERE control_source_id = 97 AND user_id IN ({$filters['cdr_id']}) AND id IN ({$candidat_ids})";
                    
                    $result = $con->fetchAssoc($sql);

                    return $result['ratio_source'];
                    
                }
                
                
            }
            
            
        } else {
            
            $sql = "SELECT count(*)/(SELECT count(*) FROM candidats WHERE control_source_id <> 97) as ratio_source FROM candidats WHERE control_source_id = 97";
        
            $result = $con->fetchAssoc($sql);

            return $result['ratio_source'];
            
        }
        
        
    }
    
    /**
     * START OF KPI CDR
     * 
     */
    public static function kpi_cdr_1_new ($filters = null) {
        
        global $con;
                
        if ($filters) {
                        
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            /// $query_cdr_id_2 = " ";
            $query_mission_status = " ";
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= {$start} ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_fin <= {$end} ";
            }    
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND T.user_id_kpi_assigned = {$filters['cdr_id']} ";
                /// $query_cdr_id_2 = " AND U.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND TC.mission_id = {$filters['mission_id']} ";
            }  
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            // total candidats grouped by statut tdc
            $sql = "SELECT T.control_statut_tdc, count(*) AS total_candidats "
                . "FROM tdcsuivis TC "
                . "LEFT JOIN tdc T ON T.mission_id = TC.mission_id AND T.candidat_id = TC.candidat_id "
                . "LEFT JOIN missions M ON M.id = TC.mission_id "
                /// . "LEFT JOIN missionsusers U ON U.mission_id = M.id "
                /// . "WHERE M.include_in_kpi = 1 " . $query_cdr_id . $query_mission_id . $query_mission_status . $query_cdr_id_2
                . "WHERE M.include_in_kpi = 1 AND T.control_statut_tdc IS NOT NULL " . $query_cdr_id . $query_mission_id . $query_mission_status
                . "GROUP BY T.control_statut_tdc";
            
            $results = $con->fetchAll($sql);
                        
            $count = 0;
            foreach($results as $row) :
                
                // total candidats date_prise_contact by status
                $sql = "SELECT count(*) AS total_candidats_date_prise_contact "
                . "FROM tdcsuivis TC "
                . "LEFT JOIN tdc T ON T.mission_id = TC.mission_id AND T.candidat_id = TC.candidat_id "
                . "LEFT JOIN missions M ON M.id = TC.mission_id "
                ///. "LEFT JOIN missionsusers U ON U.mission_id = M.id "
                /// . "WHERE M.include_in_kpi = 1 AND TC.date_prise_de_contact > 0 AND T.control_statut_tdc = {$row['control_statut_tdc']} " . $query_start_date . $query_end_date . $query_mission_id . $query_mission_status . $query_cdr_id . $query_cdr_id_2
                . "WHERE M.include_in_kpi = 1 AND TC.date_prise_de_contact > 0 AND T.control_statut_tdc = {$row['control_statut_tdc']} " . $query_start_date . $query_end_date . $query_mission_id . $query_mission_status . $query_cdr_id
                . "GROUP BY T.control_statut_tdc";          
                
                $result = $con->fetchAssoc($sql);
                
                if (empty($result)) {
                    $results[$count]['total_candidats_date_prise_contact'] = 0;
                } else {
                    $results[$count]['total_candidats_date_prise_contact'] = $result['total_candidats_date_prise_contact'];
                }
                
                $count++;
            endforeach;
                        
            return $results;
            
        } else {
            
            // total candidats grouped by statut tdc
            /*$sql = "SELECT T.control_statut_tdc, count(*) AS total_candidats FROM tdcsuivis TC LEFT JOIN tdc T ON T.mission_id = TC.mission_id AND T.candidat_id = TC.candidat_id LEFT JOIN missions M ON M.id = TC.mission_id WHERE  M.include_in_kpi = 1 AND T.control_statut_tdc IS NOT NULL GROUP BY T.control_statut_tdc";*/
            
            $sql = "SELECT T.control_statut_tdc, count(*) AS total_candidats "
                    . "FROM tdc T "
                    . "LEFT JOIN missions M ON M.id = T.mission_id "
                    . "WHERE M.include_in_kpi = 1 AND T.control_statut_tdc IS NOT NULL AND T.entreprise_id = 0 "
                    . "GROUP BY T.control_statut_tdc";
            
            $results = $con->fetchAll($sql);
            
            $count = 0;
            foreach($results as $row) :
                
                // total candidats date_prise_contact by status
                /*$sql = "SELECT count(*) AS total_candidats_date_prise_contact FROM tdcsuivis TC LEFT JOIN tdc T ON T.mission_id = TC.mission_id AND T.candidat_id = TC.candidat_id LEFT JOIN missions M ON M.id = T.mission_id WHERE  M.include_in_kpi = 1 AND TC.date_prise_de_contact > 0 AND T.control_statut_tdc = {$row['control_statut_tdc']} GROUP BY T.control_statut_tdc"; */
                
                $sql = "SELECT count(*) AS total_candidats_date_prise_contact "
                        . "FROM tdcsuivis TC "
                        . "LEFT JOIN tdc T ON T.mission_id = TC.mission_id AND T.candidat_id = TC.candidat_id "
                        . "LEFT JOIN missions M ON M.id = T.mission_id "
                        . "WHERE M.include_in_kpi = 1 AND T.entreprise_id = 0 AND TC.date_prise_de_contact > 0 AND T.control_statut_tdc = {$row['control_statut_tdc']} "
                        . "GROUP BY T.control_statut_tdc";
                
                $result = $con->fetchAssoc($sql);
                                
                if (empty($result)) {
                    $results[$count]['total_candidats_date_prise_contact'] = 0;
                } else {
                    $results[$count]['total_candidats_date_prise_contact'] = $result['total_candidats_date_prise_contact'];
                }
                
                $count++;
            endforeach;
                        
            return $results;
            
        }
        
    }
    
    
    public static function kpi_cdr_1 ($filters = null) {
        
        global $con;
        
        /**
             -- 227	8	BU-
            -- 228	8	BU+
            -- 251	8	ARC
            -- 252	8	ARCA
         */
        
        if ($filters) {
                        
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $query_start_date = " AND TC.date_prise_de_contact_added >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $query_end_date = " AND TC.date_prise_de_contact_added <= '{$end}' ";
            }    
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND TC.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND TC.mission_id = {$filters['mission_id']} ";
            }  
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            $sql = "SELECT ((count(*))/(SELECT count(*) FROM tdc WHERE control_statut_tdc IN (227, 228, 251, 252))) as ratio_transformation, T.control_statut_tdc "
                . "FROM tdcsuivis TC "
                . "LEFT JOIN tdc T ON T.mission_id = TC.mission_id AND T.candidat_id = TC.candidat_id "
                . "LEFT JOIN missions M ON M.id = TC.mission_id "
                . "WHERE TC.date_prise_de_contact > 0 AND T.control_statut_tdc IN (227, 228, 251, 252) "
                . $query_start_date . $query_end_date . $query_cdr_id . $query_mission_id . $query_mission_status
                . "GROUP BY T.control_statut_tdc";
            
            return $con->fetchAll($sql);
            
        } else {
        
            $sql = "SELECT ((count(*))/(SELECT count(*) FROM tdc WHERE control_statut_tdc IN (227, 228, 251, 252))) as ratio_transformation, T.control_statut_tdc "
                . "FROM tdcsuivis TC "
                . "LEFT JOIN tdc T ON T.mission_id = TC.mission_id AND T.candidat_id = TC.candidat_id "
                . "WHERE TC.date_prise_de_contact > 0 AND T.control_statut_tdc IN (227, 228, 251, 252) "
                . "GROUP BY T.control_statut_tdc";

            return $con->fetchAll($sql);
            
        }
        
    }
    
    public static function kpi_cdr_2_new ($filters = null) {
        
        global $con;
        
        if ($filters) {
            
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_cdr_id_2 = " ";
            $query_mission_status = " ";
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND M.id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND U.user_id = {$filters['cdr_id']} ";
                $query_cdr_id_2 = " AND T.user_id_kpi_assigned = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start_s = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= '{$start_s}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end_s = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_fin <= '{$end_s}' ";
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            } 
            
            $sql = "SELECT M.* "
                . "FROM missions M "
                . "LEFT JOIN missionsusers U ON U.mission_id = M.id "
                . "WHERE M.include_in_kpi = 1 AND manager_id > 0 " . $query_mission_id . $query_start_date . $query_end_date . $query_mission_status . $query_cdr_id
                . "ORDER BY id DESC";
            $missions = $con->fetchAll($sql);
            
            $count = 0;
            foreach($missions as $mission) :
                
                $num_candidats = TDC::getTotalCandidatInTDC($mission['id']);
                $missions[$count]['total_candidats'] = $num_candidats;
                
                $sql = "SELECT T.control_statut_tdc AS status, count(*) AS num_candidats "
                    . "FROM tdc T "
                    . "LEFT JOIN tdcsuivis TS ON TS.mission_id = T.mission_id AND TS.candidat_id = T.candidat_id "
                    . "WHERE T.mission_id = {$mission['id']} AND T.entreprise_id = 0 " . $query_cdr_id_2
                    . "GROUP BY T.control_statut_tdc "
                    . "ORDER BY T.control_statut_tdc ASC";
                    
                $results = $con->fetchAll($sql);
                
                $status = array();
                foreach($results as $res) :
                    $status[$res['status']] = $res['num_candidats'];
                endforeach;
                
                $missions[$count]['num_candidat_by_status'] = $status;
                
                $count++;
            endforeach;
            
            return $missions;
            
        } else {
            
            
            $sql = "SELECT * FROM missions WHERE include_in_kpi = 1 AND manager_id > 0 AND status NOT IN (232, 250, 235) ORDER BY id DESC";
            $missions = $con->fetchAll($sql);
            
            $count = 0;
            foreach($missions as $mission) :
                
                $num_candidats = TDC::getTotalCandidatInTDC($mission['id']);
                $missions[$count]['total_candidats'] = $num_candidats;
                
                $sql = "SELECT control_statut_tdc AS status, count(*) AS num_candidats FROM tdc WHERE mission_id = {$mission['id']} AND entreprise_id = 0 GROUP BY control_statut_tdc ORDER BY control_statut_tdc ASC";
                $results = $con->fetchAll($sql);
                
                $status = array();
                foreach($results as $res) :
                    $status[$res['status']] = $res['num_candidats'];
                endforeach;
                
                $missions[$count]['num_candidat_by_status'] = $status;
                
                $count++;
            endforeach;
            
            return $missions;
            
        }
        
    }
    
    public static function kpi_cdr_2 ($filters = null) {
        
        global $con;
                
        if ($filters) {
            
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND T.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND T.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start_s = strtotime($filters['start_date'] . " 00:00:01");
                $query_start_date = " AND M.date_debut >= '{$start_s}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end_s = strtotime($filters['end_date'] . " 23:59:59");
                $query_end_date = " AND M.date_fin <= '{$end_s}' ";
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            } 
            
            $sql = "SELECT T.mission_id, count(*) as num_candidat_in_after, M.poste, M.manager_name "
                . "FROM tdc T "
                . "LEFT JOIN missions M ON M.id = T.mission_id "
                . "WHERE T.control_statut_tdc = 229 AND T.control_statut_tdc_old IN (227, 228, 251, 252) " . $query_mission_id . $query_cdr_id . $query_start_date . $query_end_date . $query_mission_status
                . "GROUP BY T.mission_id";
                        
            $results = $con->fetchAll($sql);
            
            $count = 0;
            foreach($results as $row) :

                $num_candidats = TDC::getTotalCandidatInTDC($row['mission_id']);
                $results[$count]['num_candidat_in_mission'] = $num_candidats;
                
                // get all candidates
                $sql = "SELECT T.candidat_id, C.nom, C.prenom "
                    . "FROM tdc T "
                    . "LEFT JOIN candidats C ON C.id = T.candidat_id "
                    . "WHERE T.control_statut_tdc = 229 AND T.control_statut_tdc_old IN (227, 228, 251, 252) AND T.mission_id = {$row['mission_id']} " . $query_cdr_id . $query_start_date . $query_end_date;

                $candidats = $con->fetchAll($sql);
                
                $results[$count]['candidats'] = $candidats;
                
                $count++;
            endforeach;

            return $results;
            
        } else {
            
            /**
             * IN forms parts in => 227 (BU-), 228 (BU+), 251 (ARC), 252 (ARCA)
             */
            
            // ex Bu = 259
        
            $sql = "SELECT T.mission_id, count(*) as num_candidat_in_after, M.poste, M.manager_name FROM tdc T LEFT JOIN missions M ON M.id = T.mission_id WHERE T.control_statut_tdc = 229 AND T.control_statut_tdc_old IN (227, 228, 251, 252) GROUP BY T.mission_id";
            
            $results = $con->fetchAll($sql);

            $count = 0;
            foreach($results as $row) :

                $num_candidats = TDC::getTotalCandidatInTDC($row['mission_id']);
                $results[$count]['num_candidat_in_mission'] = $num_candidats;
                
                // get all candidates
                $sql = "SELECT T.candidat_id, C.nom, C.prenom FROM tdc T LEFT JOIN candidats C ON C.id = T.candidat_id WHERE T.control_statut_tdc = 229 AND T.control_statut_tdc_old IN (227, 228, 251, 252) AND T.mission_id = {$row['mission_id']}";

                $candidats = $con->fetchAll($sql);
                
                $results[$count]['candidats'] = $candidats;
                
//                // get count BU by mission
//                $sql = "SELECT mission_id, count(*) as num_candidats, control_statut_tdc_old FROM tdc WHERE control_statut_tdc = 229 AND control_statut_tdc_old IN (227, 228, 251, 252) AND mission_id = {$row['mission_id']} GROUP BY control_statut_tdc_old ORDER BY control_statut_tdc_old ASC";
//                
//                $status = $con->fetchAll($sql);
//                $results[$count]['status'] = $status;
//                
//                // get count exBU
//                $sql = "SELECT count(*) as num_exbu FROM tdc WHERE control_statut_tdc = 259 AND mission_id = {$row['mission_id']}";
//                $exBu = $con->fetchAssoc($sql);
//                $results[$count]['exbu'] = $exBu['num_exbu'];
                                
                $count++;
            endforeach;
            
            
            
            return $results;
            
        }
        
    }
    
    public static function kpi_cdr_3_new ($filters = null) {
        
        global $con;
        
        if ($filters) {
            
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_cdr_id_2 = " ";
            $query_mission_status = " ";
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND W.table_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                // $query_cdr_id = " AND W.user_id = {$filters['cdr_id']} ";
                $query_cdr_id_2 = " AND U.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $query_start_date = " AND W.created >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $query_end_date = " AND W.created <= '{$end}' ";
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            } 
            
             $sql = "SELECT W.table_id as mission_id, M.poste, M.manager_name, M.date_debut, M.date_fin, W.status_before_id, W.status_before, W.status_after_id, W.status_after, W.created as mission_status_date_changed "
                . "FROM watchdog W "
                . "LEFT JOIN missions M ON M.id = W.table_id "
                . "LEFT JOIN missionsusers U ON M.id = U.mission_id "
                . "WHERE M.include_in_kpi = 1 AND W.action_code = 'mod_mission_edit_mission' AND W.status_after_id IN (230, 231) " . $query_mission_id . $query_cdr_id . $query_mission_status . $query_start_date . $query_end_date . $query_cdr_id_2
                . "GROUP BY W.table_id "
                . "ORDER BY W.table_id DESC";
                         
            return $con->fetchAll($sql); 
             
        } else {
            
            $sql = "SELECT W.table_id as mission_id, M.poste, M.manager_name, M.date_debut, M.date_fin, W.status_before_id, W.status_before, W.status_after_id, W.status_after, W.created as mission_status_date_changed "
                . "FROM watchdog W "
                . "LEFT JOIN missions M ON M.id = W.table_id "
                . "WHERE M.include_in_kpi = 1 AND W.action_code = 'mod_mission_edit_mission' AND  W.status_after_id IN (230, 231)  "
                . "GROUP BY W.table_id "
                . "ORDER BY W.table_id DESC";
            
            return $con->fetchAll($sql);
            
        }
        
    }
    
    public static function kpi_cdr_3 ($filters = null) {
        
        global $con;
        
        if ($filters) {
            
            if (isset($filters['cdr_id']) && !isset($filters['mission_id'])) {
                
                // get all missions the cdr was
                
                if (isset($filters['status']) && $filters['status'] <> "") {                    
                    $sql = "SELECT MU.mission_id FROM missionsusers MU LEFT JOIN missions M ON M.id = MU.mission_id WHERE MU.user_id = {$filters['cdr_id']} AND M.status IN ({$filters['status']})";
                    
                } else {
                    $sql = "SELECT mission_id FROM missionsusers WHERE user_id = {$filters['cdr_id']}";
                }
                
                
                $missions = $con->fetchAll($sql);
                
                $mission_id = array();
                foreach($missions as $mission):
                    $mission_id[] = $mission['mission_id'];
                endforeach;
                
                $mission_ids = addSeperator($mission_id, ',');
                                
                $sql = "SELECT W.table_id as mission_id, M.poste, W.created as mission_status_date_changed, M.date_debut, M.date_fin, W.status_after_id "
                . "FROM watchdog W "
                . "LEFT JOIN missions M ON M.id = W.table_id "
                . "WHERE W.action_code = 'mod_mission_edit_mission' AND W.status_before_id = 232 AND W.status_after_id NOT IN (232, 233, 235, 250) AND W.table_id IN ({$mission_ids}) "
                . "GROUP BY W.table_id "
                . "ORDER BY W.id DESC";

                $results = $con->fetchAll($sql);

                $data = array();
                $count = 0;
                foreach($results as $result) :

                    $data[$count] = $result;

                    $time_mission_finished = strtotime($result['mission_status_date_changed']);
                    $time_mission = $result['date_fin'] - $result['date_debut'];

                    $ratio = (($time_mission_finished - $result['date_fin']) / $time_mission);

                    $data[$count]['ratio'] = round($ratio, 2);

                    $count++;
                endforeach;

                return $data;
                
            } else if (isset($filters['cdr_id']) && isset($filters['mission_id'])) {
                
                // get all missions the cdr was
                if (isset($filters['status']) && $filters['status'] <> "") {                    
                    $sql = "SELECT MU.mission_id FROM missionsusers MU LEFT JOIN missions M ON M.id = MU.mission_id WHERE MU.user_id = {$filters['cdr_id']} AND M.status IN ({$filters['status']}) AND mission_id = {$filters['mission_id']}";
                                        
                } else {
                    $sql = "SELECT mission_id FROM missionsusers WHERE user_id = {$filters['cdr_id']} AND mission_id = {$filters['mission_id']}";
                }
                
                $missions = $con->fetchAll($sql);
                
                $mission_id = array();
                foreach($missions as $mission):
                    $mission_id[] = $mission['mission_id'];
                endforeach;
                
                $mission_ids = addSeperator($mission_id, ',');
                                
                $sql = "SELECT W.table_id as mission_id, M.poste, W.created as mission_status_date_changed, M.date_debut, M.date_fin, W.status_after_id "
                . "FROM watchdog W "
                . "LEFT JOIN missions M ON M.id = W.table_id "
                . "WHERE W.action_code = 'mod_mission_edit_mission' AND W.status_before_id = 232 AND W.status_after_id NOT IN (232, 233, 235, 250) AND W.table_id IN ({$mission_ids}) "
                . "GROUP BY W.table_id "
                . "ORDER BY W.id DESC";

                $results = $con->fetchAll($sql);

                $data = array();
                $count = 0;
                foreach($results as $result) :

                    $data[$count] = $result;

                    $time_mission_finished = strtotime($result['mission_status_date_changed']);
                    $time_mission = $result['date_fin'] - $result['date_debut'];

                    $ratio = (($time_mission_finished - $result['date_fin']) / $time_mission);

                    $data[$count]['ratio'] = round($ratio, 2);

                    $count++;
                endforeach;

                return $data;
                
            }
            
        } else {
            
            $sql = "SELECT W.table_id as mission_id, M.poste, W.created as mission_status_date_changed, M.date_debut, M.date_fin, W.status_after_id "
                . "FROM watchdog W "
                . "LEFT JOIN missions M ON M.id = W.table_id "
                . "WHERE W.action_code = 'mod_mission_edit_mission' AND W.status_before_id = 232 AND W.status_after_id NOT IN (232, 233, 235, 250) "
                . "GROUP BY W.table_id "
                . "ORDER BY W.id DESC";

            $results = $con->fetchAll($sql);

            $data = array();
            $count = 0;
            foreach($results as $result) :

                $data[$count] = $result;

                $time_mission_finished = strtotime($result['mission_status_date_changed']);
                $time_mission = $result['date_fin'] - $result['date_debut'];

                $ratio = (($time_mission_finished - $result['date_fin']) / $time_mission);

                $data[$count]['ratio'] = round($ratio, 2);

                $count++;
            endforeach;

            return $data;
 
        }
        
            
        
    }
    
    public static function kpi_cdr_4 ($filters = null) {
        
        global $con;
        
        if ($filters) {
            
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
                        
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND w.table_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND w.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $query_start_date = " AND w.created >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $query_end_date = " AND w.created <= '{$end}' ";
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND m.status IN ({$filters['status']}) ";
            } 
            
            $sql = "SELECT (SUM(UNIX_TIMESTAMP(w.created)) - SUM(m.date_debut))/count(DISTINCT(w.table_id)) as avg_time "
                . "FROM watchdog w "
                . "LEFT JOIN missions m ON m.id = w.table_id "
                . "WHERE w.action_code = 'mod_mission_edit_mission' AND w.status_before_id = 232 AND w.status_after_id <> 232 " . $query_mission_id . $query_cdr_id . $query_start_date . $query_end_date . $query_mission_status;

            $result = $con->fetchAssoc($sql);

            return seconds_to_hours_minutes(round($result['avg_time'], 0));
            
        } else {
        
            $sql = "SELECT (SUM(UNIX_TIMESTAMP(w.created)) - SUM(m.date_debut))/count(DISTINCT(w.table_id)) as avg_time "
                . "FROM watchdog w "
                . "LEFT JOIN missions m ON m.id = w.table_id "
                . "WHERE w.action_code = 'mod_mission_edit_mission' AND w.status_before_id = 232 AND w.status_after_id <> 232";

            $result = $con->fetchAssoc($sql);

            return seconds_to_hours_minutes(round($result['avg_time'], 0));
        }
        
    }
    
    public static function kpi_cdr_5 ($filters = null) {
        
        global $con;
                
        if ($filters) {
            
            $query_start_date = " ";
            $query_end_date = " ";
            
            if (!isset($filters['mission_id']) && isset($filters['cdr_id'])) {        
                                                                
                $sql = "SELECT voip_ext FROM users WHERE voip_ext <> '' AND id IN ({$filters['cdr_id']})";
                
                $extensions = $con->fetchAll($sql);
                
                if (empty($extensions)) {
                    return "Le CDR n'a pas une extension et SDA";
                }
                
                // in table kpi_cdrs field from_no e.g. Ext.764
                $voip_ext = array();
                foreach($extensions as $ext):
                    // must check of ext is 3 characters
                    if (strlen($ext['voip_ext']) == 3) {
                        $voip_ext[] = "Ext." . $ext['voip_ext'];
                    }
                endforeach;

                $ext = addSeperatorClosure($voip_ext, ',');
                                
                // check all phone numbers candidats in the missions
                $sql = "SELECT candidat_id FROM tdc WHERE user_id IN ({$filters['cdr_id']})";
                $candidats = $con->fetchAll($sql);

                $candidats_ids = array();
                foreach($candidats as $candidats_id):
                    $candidats_ids[] = $candidats_id['candidat_id'];
                endforeach;

                $candidats_ids = addSeperator($candidats_ids, ',');

                // get all phone number of all candidats
                $sql = "SELECT tel_standard, tel_pro, tel_mobile_perso, tel_domicile FROM candidats WHERE id IN ({$candidats_ids})";
                $phones = $con->fetchAll($sql);

                $tel_nos = array();
                foreach($phones as $phone) :

                    if ($phone['tel_standard'] <> "") {
                        $tel_nos[] = substr(formatTelFR(str_replace('+', '', ltrim($phone['tel_standard']))), 0, 10);
                    }

                    if ($phone['tel_pro'] <> "") {
                        $tel_nos[] = substr(formatTelFR(str_replace('+', '', ltrim($phone['tel_pro']))), 0, 10);
                    }

                    if ($phone['tel_mobile_perso'] <> "") {
                        $tel_nos[] = substr(formatTelFR(str_replace('+', '', ltrim($phone['tel_mobile_perso']))), 0, 10);
                    }

                    if ($phone['tel_domicile'] <> "") {
                        $tel_nos[] = substr(formatTelFR(str_replace('+', '', ltrim($phone['tel_domicile']))), 0, 10);
                    }

                endforeach;
                
                if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                    $start = $filters['start_date'] . " 00:00:01";
                    $query_start_date = " AND time_start >= '{$start}' ";
                }

                if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                    $end = $filters['end_date'] . " 23:59:59";
                    $query_end_date = " AND time_end <= '{$end}' ";
                }

                // now we get all outbound calls from kpi_cdrs
                $tels = build_query_like_OR($tel_nos, 'to_no');
                $sql = "SELECT count(*) as outbound_calls FROM kpi_cdrs WHERE from_no IN ({$ext}) AND {$tels} " . $query_start_date . $query_end_date; 
                
                $result = $con->fetchAssoc($sql);
                $num_outbound_calls = $result['outbound_calls'];
                $avg_outbound_calls = round($num_outbound_calls/count($voip_ext), 0);

                // inbound calls
                $tels = build_query_like_OR($tel_nos, 'from_no');
                $sql = "SELECT count(*) as inbound_calls FROM kpi_cdrs WHERE to_no IN ({$ext}) AND {$tels} " . $query_start_date . $query_end_date;     
                $result = $con->fetchAssoc($sql);
                $num_inbound_calls = $result['inbound_calls'];
                $avg_inbound_calls = round($num_inbound_calls/count($voip_ext), 0);
                
                return array('outbound' => $avg_outbound_calls, 'inbound' => $avg_inbound_calls);               
                
            } 
            
            if (isset($filters['mission_id']) && isset($filters['cdr_id'])) {
                             
                $sql = "SELECT voip_ext FROM users WHERE voip_ext <> '' AND id = {$filters['cdr_id']}";
                
                $extensions = $con->fetchAll($sql);
                
                if (empty($extensions)) {
                    return "Le CDR n'a pas une extension et SDA";
                }
                
                // in table kpi_cdrs field from_no e.g. Ext.764
                $voip_ext = array();
                foreach($extensions as $ext):
                    // must check of ext is 3 characters
                    if (strlen($ext['voip_ext']) == 3) {
                        $voip_ext[] = "Ext." . $ext['voip_ext'];
                    }
                endforeach;

                $ext = addSeperatorClosure($voip_ext, ',');
                
                // check all phone numbers candidats in the missions
                $sql = "SELECT candidat_id FROM tdc WHERE user_id IN ({$filters['cdr_id']}) AND mission_id = {$filters['mission_id']}";
                $candidats = $con->fetchAll($sql);

                $candidats_ids = array();
                foreach($candidats as $candidats_id):
                    $candidats_ids[] = $candidats_id['candidat_id'];
                endforeach;

                $candidats_ids = addSeperator($candidats_ids, ',');

                // get all phone number of all candidats
                $sql = "SELECT tel_standard, tel_pro, tel_mobile_perso, tel_domicile FROM candidats WHERE id IN ({$candidats_ids})";
                $phones = $con->fetchAll($sql);

                $tel_nos = array();
                foreach($phones as $phone) :

                    if ($phone['tel_standard'] <> "") {
                        $tel_nos[] = substr(formatTelFR(str_replace('+', '', ltrim($phone['tel_standard']))), 0, 10);
                    }

                    if ($phone['tel_pro'] <> "") {
                        $tel_nos[] = substr(formatTelFR(str_replace('+', '', ltrim($phone['tel_pro']))), 0, 10);
                    }

                    if ($phone['tel_mobile_perso'] <> "") {
                        $tel_nos[] = substr(formatTelFR(str_replace('+', '', ltrim($phone['tel_mobile_perso']))), 0, 10);
                    }

                    if ($phone['tel_domicile'] <> "") {
                        $tel_nos[] = substr(formatTelFR(str_replace('+', '', ltrim($phone['tel_domicile']))), 0, 10);
                    }

                endforeach;
                
                if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                    $start = $filters['start_date'] . " 00:00:01";
                    $query_start_date = " AND time_start >= '{$start}' ";
                }

                if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                    $end = $filters['end_date'] . " 23:59:59";
                    $query_end_date = " AND time_end <= '{$end}' ";
                }

                // now we get all outbound calls from kpi_cdrs
                $tels = build_query_like_OR($tel_nos, 'to_no');
                $sql = "SELECT count(*) as outbound_calls FROM kpi_cdrs WHERE from_no IN ({$ext}) AND {$tels} " . $query_start_date . $query_end_date; 
                
                $result = $con->fetchAssoc($sql);
                $num_outbound_calls = $result['outbound_calls'];
                $avg_outbound_calls = round($num_outbound_calls/count($voip_ext), 0);

                // inbound calls
                $tels = build_query_like_OR($tel_nos, 'from_no');
                $sql = "SELECT count(*) as inbound_calls FROM kpi_cdrs WHERE to_no IN ({$ext}) AND {$tels} " . $query_start_date . $query_end_date;     
                $result = $con->fetchAssoc($sql);
                $num_inbound_calls = $result['inbound_calls'];
                $avg_inbound_calls = round($num_inbound_calls/count($voip_ext), 0);
                
                return array('outbound' => $avg_outbound_calls, 'inbound' => $avg_inbound_calls);  
                
            }

        } else {
        
            // get extensions of users
            $sql = "SELECT voip_ext, id FROM users WHERE role_id = 4 AND voip_ext <> ''";
            
            $extensions = $con->fetchAll($sql);

            // in table kpi_cdrs field from_no e.g. Ext.764
            $voip_ext = array();
            $user_ids = array();
            foreach($extensions as $ext):
                // must check of ext is 3 characters
                if (strlen($ext['voip_ext']) == 3) {
                    $voip_ext[] = "Ext." . $ext['voip_ext'];
                    $user_ids[] = $ext['id'];
                }
            endforeach;
                       
            $ext = addSeperatorClosure($voip_ext, ',');
            $cdr_ids = addSeperator($user_ids, ',');
            
            // check all phone numbers candidats in the missions
            $sql = "SELECT candidat_id FROM tdc WHERE user_id IN ({$cdr_ids})";
            $candidats = $con->fetchAll($sql);
                        
            $candidats_ids = array();
            foreach($candidats as $candidats_id):
                $candidats_ids[] = $candidats_id['candidat_id'];
            endforeach;
                        
            $candidats_ids = addSeperator($candidats_ids, ',');
            
            // get all phone number of all candidats
            $sql = "SELECT tel_standard, tel_pro, tel_mobile_perso, tel_domicile FROM candidats WHERE id IN ({$candidats_ids})";
            $phones = $con->fetchAll($sql);
            
            $tel_nos = array();
            foreach($phones as $phone) :
                
                if ($phone['tel_standard'] <> "") {
                    $tel_nos[] = substr(formatTelFR(str_replace('+', '', ltrim($phone['tel_standard']))), 0, 10);
                }
                
                if ($phone['tel_pro'] <> "") {
                    $tel_nos[] = substr(formatTelFR(str_replace('+', '', ltrim($phone['tel_pro']))), 0, 10);
                }
                
                if ($phone['tel_mobile_perso'] <> "") {
                    $tel_nos[] = substr(formatTelFR(str_replace('+', '', ltrim($phone['tel_mobile_perso']))), 0, 10);
                }
                
                if ($phone['tel_domicile'] <> "") {
                    $tel_nos[] = substr(formatTelFR(str_replace('+', '', ltrim($phone['tel_domicile']))), 0, 10);
                }
                
            endforeach;
            

            // now we get all outbound calls from kpi_cdrs
            
            $tels = build_query_like_OR($tel_nos, 'to_no');
            $sql = "SELECT count(*) as outbound_calls FROM kpi_cdrs WHERE from_no IN ({$ext}) AND {$tels} ";        
            $result = $con->fetchAssoc($sql);
            $num_outbound_calls = $result['outbound_calls'];
            $avg_outbound_calls = round($num_outbound_calls/count($voip_ext), 0);

            // inbound calls
            
            $tels = build_query_like_OR($tel_nos, 'from_no');
            
            $sql = "SELECT count(*) as inbound_calls FROM kpi_cdrs WHERE to_no IN ({$ext}) AND {$tels} ";
            $result = $con->fetchAssoc($sql);
            $num_inbound_calls = $result['inbound_calls'];
            $avg_inbound_calls = round($num_inbound_calls/count($voip_ext), 0);

            return array('outbound' => $avg_outbound_calls, 'inbound' => $avg_inbound_calls);
            
        }
        
    }
    
    
    public static function kpi_cdr_6 ($filters = null) {
        
        global $con;
                
        
        
    }
    
    public static function kpi_cdr_7 ($filters = null) {
        
        global $con;
                
        if ($filters) {
            
            if (!isset($filters['mission_id']) && isset($filters['cdr_id'])) {
                                                
                $user_ids = $filters['cdr_id'];
                                
                // get all candidats ids from the mission
                
                if (isset($filters['status']) && $filters['status'] <> "") {
                    $sql = "SELECT T.candidat_id FROM tdc T LEFT JOIN missions M ON M.id = T.mission_id WHERE M.status IN ({$filters['status']}) AND T.user_id = {$filters['cdr_id']}";
                } else {
                    $sql = "SELECT candidat_id FROM tdc WHERE user_id = {$filters['cdr_id']}";
                }

                $results = $con->fetchAll($sql);
                
                $candidat_ids = array();
                foreach($results as $id) :
                    $candidat_ids[] = $id['candidat_id'];
                endforeach;
                
                if (count($candidat_ids) == 0) {
                    $candidat_ids = 0;
                } else {
                    $candidat_ids = addSeperator($candidat_ids, ',');
                }            
                
                $sql = "SELECT count(*)/(SELECT count(*) FROM candidats WHERE control_source_id <> 97 AND user_id IN ({$user_ids}) AND id IN ({$candidat_ids})) as ratio_source FROM candidats WHERE control_source_id = 97 AND user_id IN ({$user_ids}) AND id IN ({$candidat_ids})";
                
                
                $result = $con->fetchAssoc($sql);

                if ($result['ratio_source'] == "") {
                    return "0.0000";
                } else {
                    return $result['ratio_source'];
                }
                
            } else if (isset($filters['mission_id']) && isset($filters['cdr_id'])) {
                                                
                // get all candidats ids from the mission
                
                if (isset($filters['status']) && $filters['status'] <> "") {
                    $sql = "SELECT T.candidat_id FROM tdc T LEFT JOIN missions M ON M.id = T.mission_id WHERE T.mission_id = {$filters['mission_id']} AND T.user_id = {$filters['cdr_id']} AND M.status IN ({$filters['status']})";
                } else {
                    $sql = "SELECT T.candidat_id FROM tdc T LEFT JOIN missions M ON M.id = T.mission_id WHERE T.mission_id = {$filters['mission_id']} AND T.user_id = {$filters['cdr_id']}";
                }
                              
                $results = $con->fetchAll($sql);
                
                $candidat_ids = array();
                foreach($results as $id) :
                    $candidat_ids[] = $id['candidat_id'];
                endforeach;
                
                if (count($candidat_ids) == 0) {
                    $candidat_ids = 0;
                } else {
                    $candidat_ids = addSeperator($candidat_ids, ',');
                }
                
                
                $sql = "SELECT count(*)/(SELECT count(*) FROM candidats WHERE control_source_id <> 97 AND user_id IN ({$filters['cdr_id']}) AND id IN ({$candidat_ids})) as ratio_source FROM candidats WHERE control_source_id = 97 AND user_id IN ({$filters['cdr_id']}) AND id IN ({$candidat_ids})";
                
                
                $result = $con->fetchAssoc($sql);
                
                if ($result['ratio_source'] == "") {
                    return "0.0000";
                } else {
                    return $result['ratio_source'];
                }

            }
            
            
        } else {
            
            $sql = "SELECT count(*)/(SELECT count(*) FROM candidats WHERE control_source_id <> 97) as ratio_source FROM candidats WHERE control_source_id = 97";
        
            $result = $con->fetchAssoc($sql);

            return $result['ratio_source'];
            
        }
        
    }
    
    public static function kpi_cdr_8 ($filters = null) {
        
        global $con;
        
        if ($filters) {
            
            
            
            if (!isset($filters['mission_id']) && isset($filters['cdr_id'])) {
                
                $query_start_date = " ";
                $query_start_date2 = " ";
                $query_end_date = " ";
                $query_end_date2 = " ";
                $query_mission_status = " ";
                $query_mission_status2 = " ";
                
                if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                    $start = $filters['start_date'] . " 00:00:01";
                    $query_start_date = " AND K.shortliste_added >= '{$start}' ";
                    $query_start_date2 = " AND K.shortliste_added >= '{$start}' ";
                }

                if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                    $end = $filters['end_date'] . " 23:59:59";
                    $query_end_date = " AND K.shortliste_added <= '{$end}' ";
                    $query_end_date2 = " AND K.shortliste_added <= '{$end}' ";
                }  
                
                if (isset($filters['status']) && $filters['status'] <> "") {
                    $query_mission_status = " AND M.status IN ({$filters['status']}) ";
                    $query_mission_status2 = " AND M.status IN ({$filters['status']}) ";
                }  
                
                // get all mission ids
                $sql = "SELECT K.mission_id, count(*) as num_shortliste "
                    . "FROM kpi_missions_shortlist K "
                    . "LEFT JOIN missions M ON M.id = K.mission_id "
                    . "WHERE K.shortliste = 1 AND K.shortliste_added IS NOT NULL AND K.user_id = {$filters['cdr_id']}" . $query_start_date . $query_start_date . $query_mission_status
                    . "GROUP BY mission_id "
                    . "HAVING count(*) >= 3";
            
                $missions = $con->fetchAll($sql);

                // debug($missions);

                $mission_data = array();
                $count = 0;
                foreach($missions as $mission) :

                    $sql = "SELECT K.*, M.poste, U.firstName, U.lastName "
                    . "FROM kpi_missions_shortlist K "
                    . "LEFT JOIN users U ON U.id = K.user_id "
                    . "LEFT JOIN missions M ON M.id = K.mission_id "
                    . "WHERE K.shortliste = 1 AND K.shortliste_added IS NOT NULL AND K.mission_id = {$mission['mission_id']} AND K.user_id = {$filters['cdr_id']}" . $query_start_date2 . $query_end_date2 . $query_mission_status2 
                    . "ORDER BY K.shortliste_added ASC "
                    . "LIMIT 3";

                    $rows = $con->fetchAll($sql);

                    $row_count = 0;
                    foreach($rows as $row) :

                        $mission_data[$count][$row_count] = $row;

                        // time between date debut of mission formatted and date shortliste added
                        $mission_data[$count][$row_count]['time_shortliste_from_start_mission'] = seconds_to_hours_minutes($row['t_shortliste_from_start_mission']);

                        // calculate time between each rows except the first one
                        if ($row_count > 0) {
                            $mission_data[$count][$row_count]['time_difference_between_previous'] = seconds_to_hours_minutes($row['t_shortliste_from_start_mission'] - $rows[$row_count-1]['t_shortliste_from_start_mission']);
                        } else {
                            $mission_data[$count][$row_count]['time_difference_between_previous'] = 'N/A';
                        }

                        $row_count++;
                    endforeach;

                    $count++;
                endforeach;

                return $mission_data;
                
                
            } else if (isset($filters['mission_id']) && isset($filters['cdr_id'])) {
                
                $query_start_date = " ";
                $query_start_date2 = " ";
                $query_end_date = " ";
                $query_end_date2 = " ";
                $query_mission_status = " ";
                $query_mission_status2 = " ";
                
                if (isset($filters['status']) && $filters['status'] <> "") {
                    $query_mission_status = " AND M.status IN ({$filters['status']}) ";
                    $query_mission_status2 = " AND M.status IN ({$filters['status']}) ";
                }  
                
                if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                    $start = $filters['start_date'] . " 00:00:01";
                    $query_start_date = " AND K.shortliste_added >= '{$start}' ";
                    $query_start_date2 = " AND K.shortliste_added >= '{$start}' ";
                }

                if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                    $end = $filters['end_date'] . " 23:59:59";
                    $query_end_date = " AND K.shortliste_added <= '{$end}' ";
                    $query_end_date2 = " AND K.shortliste_added <= '{$end}' ";
                }  
                                
                // get all mission ids
                $sql = "SELECT K.mission_id, count(*) as num_shortliste "
                    . "FROM kpi_missions_shortlist K "
                    . "LEFT JOIN missions M ON M.id = K.mission_id "
                    . "WHERE K.shortliste = 1 AND K.shortliste_added IS NOT NULL AND K.user_id = {$filters['cdr_id']} AND K.mission_id = {$filters['mission_id']} " . $query_start_date . $query_start_date 
                    . "GROUP BY mission_id "
                    . "HAVING count(*) >= 3";
            
                $missions = $con->fetchAll($sql);

                // debug($missions);

                $mission_data = array();
                $count = 0;
                foreach($missions as $mission) :

                    $sql = "SELECT K.*, M.poste, U.firstName, U.lastName "
                    . "FROM kpi_missions_shortlist K "
                    . "LEFT JOIN users U ON U.id = K.user_id "
                    . "LEFT JOIN missions M ON M.id = K.mission_id "
                    . "WHERE K.shortliste = 1 AND K.shortliste_added IS NOT NULL AND K.mission_id = {$mission['mission_id']} AND K.user_id = {$filters['cdr_id']}" . $query_start_date2 . $query_end_date2 . $query_mission_status2
                    . "ORDER BY K.shortliste_added ASC "
                    . "LIMIT 3";

                    $rows = $con->fetchAll($sql);

                    $row_count = 0;
                    foreach($rows as $row) :

                        $mission_data[$count][$row_count] = $row;

                        // time between date debut of mission formatted and date shortliste added
                        $mission_data[$count][$row_count]['time_shortliste_from_start_mission'] = seconds_to_hours_minutes($row['t_shortliste_from_start_mission']);

                        // calculate time between each rows except the first one
                        if ($row_count > 0) {
                            $mission_data[$count][$row_count]['time_difference_between_previous'] = seconds_to_hours_minutes($row['t_shortliste_from_start_mission'] - $rows[$row_count-1]['t_shortliste_from_start_mission']);
                        } else {
                            $mission_data[$count][$row_count]['time_difference_between_previous'] = 'N/A';
                        }

                        $row_count++;
                    endforeach;

                    $count++;
                endforeach;

                return $mission_data;
                
                
            }
            
                                  
            
            
            
        } else {
            
            // get all mission ids
            $sql = "SELECT mission_id, count(*) as num_shortliste "
                . "FROM kpi_missions_shortlist "
                . "WHERE shortliste = 1 AND shortliste_added IS NOT NULL "
                . "GROUP BY mission_id "
                . "HAVING count(*) >= 3";
            
            $missions = $con->fetchAll($sql);
            
            // debug($missions);
            
            $mission_data = array();
            $count = 0;
            foreach($missions as $mission) :
                
                $sql = "SELECT K.*, M.poste, U.firstName, U.lastName FROM kpi_missions_shortlist K LEFT JOIN users U ON U.id = K.user_id LEFT JOIN missions M ON M.id = K.mission_id WHERE K.shortliste = 1 AND K.shortliste_added IS NOT NULL AND K.mission_id = {$mission['mission_id']} ORDER BY K.shortliste_added ASC LIMIT 3";
                
                $rows = $con->fetchAll($sql);
                
                $row_count = 0;
                foreach($rows as $row) :
                    
                    $mission_data[$count][$row_count] = $row;
                    
                    // time between date debut of mission formatted and date shortliste added
                    $mission_data[$count][$row_count]['time_shortliste_from_start_mission'] = seconds_to_hours_minutes($row['t_shortliste_from_start_mission']);
                    
                    // calculate time between each rows except the first one
                    if ($row_count > 0) {
                        $mission_data[$count][$row_count]['time_difference_between_previous'] = seconds_to_hours_minutes($row['t_shortliste_from_start_mission'] - $rows[$row_count-1]['t_shortliste_from_start_mission']);
                    } else {
                        $mission_data[$count][$row_count]['time_difference_between_previous'] = 'N/A';
                    }
                
                    $row_count++;
                endforeach;
                
                $count++;
            endforeach;
            
            
            return $mission_data;
            
        }
        
    }
    
    public static function kpi_manager_shortliste_moyenne ($filters = null) {
        
        global $con;
        
        if ($filters) {
            
            $query_manager_id = " ";
            $query_start_date = " ";
            $query_end_date = " ";
            $query_mission_id = " ";
            $query_cdr_id = " ";
            $query_mission_status = " ";
            
            
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $query_manager_id = " AND K.manager_id = {$filters['manager_id']} ";
            } 
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id = " AND K.mission_id = {$filters['mission_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $query_cdr_id = " AND K.user_id = {$filters['cdr_id']} ";
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status = " AND M.status IN ({$filters['status']}) ";
            }  
            
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $query_start_date = " AND K.mission_date_debut >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $query_end_date = " AND K.mission_date_fin <= '{$end}' ";
            }
            
            // get all mission ids
            $sql = "SELECT K.mission_id, count(*) as num_shortliste "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE K.shortliste = 1 AND K.shortliste_added IS NOT NULL " . $query_manager_id . $query_mission_id . $query_cdr_id . $query_mission_status . $query_mission_status . $query_start_date . $query_start_date
                . "GROUP BY K.mission_id "
                . "HAVING count(*) >= 3";
            
            $missions = $con->fetchAll($sql);
            
            // debug($missions);
            
            $mission_data = array();
            $count = 0;
            foreach($missions as $mission) :
                
                $sql = "SELECT K.*, M.poste, U.firstName, U.lastName "
                . "FROM kpi_missions_shortlist K "
                . "LEFT JOIN users U ON U.id = K.manager_id "
                . "LEFT JOIN missions M ON M.id = K.mission_id "
                . "WHERE K.shortliste = 1 AND K.shortliste_added IS NOT NULL AND K.manager_id > 0 AND K.mission_id = {$mission['mission_id']} "
                . "ORDER BY K.shortliste_added ASC LIMIT 3";
                
                $rows = $con->fetchAll($sql);
                
                $row_count = 0;
                foreach($rows as $row) :
                    
                    $mission_data[$count][$row_count] = $row;
                    
                    // time between date debut of mission formatted and date shortliste added
                    $mission_data[$count][$row_count]['time_shortliste_from_start_mission'] = seconds_to_hours_minutes_f($row['t_shortliste_from_start_mission']);
                
                    $row_count++;
                endforeach;
                
                $count++;
            endforeach;
            
            
            return $mission_data;            
            
        } else {
            
            // get all mission ids
            $sql = "SELECT mission_id, count(*) as num_shortliste "
                . "FROM kpi_missions_shortlist "
                . "WHERE shortliste = 1 AND shortliste_added IS NOT NULL "
                . "GROUP BY mission_id "
                . "HAVING count(*) >= 3";
            
            $missions = $con->fetchAll($sql);
            
            // debug($missions);
            
            $mission_data = array();
            $count = 0;
            foreach($missions as $mission) :
                
                $sql = "SELECT K.*, M.poste, U.firstName, U.lastName FROM kpi_missions_shortlist K LEFT JOIN users U ON U.id = K.manager_id LEFT JOIN missions M ON M.id = K.mission_id WHERE K.shortliste = 1 AND K.shortliste_added IS NOT NULL AND K.manager_id > 0 AND K.mission_id = {$mission['mission_id']} ORDER BY K.shortliste_added ASC LIMIT 3";
                
                $rows = $con->fetchAll($sql);
                
                $row_count = 0;
                foreach($rows as $row) :
                    
                    $mission_data[$count][$row_count] = $row;
                    
                    // time between date debut of mission formatted and date shortliste added
                    $mission_data[$count][$row_count]['time_shortliste_from_start_mission'] = seconds_to_hours_minutes_f($row['t_shortliste_from_start_mission']);
                
                    $row_count++;
                endforeach;
                
                $count++;
            endforeach;
            
            
            return $mission_data;
            
        }
        
    }
    
    /**
     * END OF KPI CDR
     */
    
}
