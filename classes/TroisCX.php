<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of 3CX
 *
 * @author Faardeen Madarbokas
 */
class TroisCX {
    
    public static function findByNumber ($number) {
        
        global $con;
        
        // format the number
        // add spaces between each 2 digits
        $num = AddSpaceTelFR($number);
        
//        $sql = "SELECT * FROM candidats WHERE tel_standard LIKE '%{$num}%' OR tel_ligne_direct LIKE '%{$num}%' OR tel_pro LIKE '%{$num}%' OR tel_mobile_perso LIKE '%{$num}%' OR tel_domicile LIKE '%{$num}%'";
        
        $sql = "SELECT * FROM candidats WHERE tel_standard LIKE '%{$num}%'";
        
        return $con->fetchAssoc($sql);
        
    }
    
    public static function showById ($id) {
        
        global $con;
        
        $sql = "SELECT * FROM candidats WHERE id = {$id}";
        
        return $con->fetchAssoc($sql);
        
    }
    
    public static function processCDR($file) {
        
        global $con;
                
        // read the file
        // must read only the first line as each cdr file is for one call in 3CX
        $line_number = 1;
        $handle = fopen($file, "r");
        
        while($line = fgetcsv($handle, 5000)){
                                    
            if ($line_number == 1) {
                
                $cdr = array(
                    'historyid' => $line[0],
                    'callid' => $line[1],
                    'duration' => seconds_from_time(($line[2] <> "") ? $line[2] : ""), //store in seconds
                    'time_start' => (isset($line[3]) && $line[3] <> "") ? dateToDb(strtotime($line[3])) : "",
                    'time_answered' => (isset($line[4]) && $line[4] <> "") ? dateToDb(strtotime($line[4])) : "",
                    'time_end' => (isset($line[5]) && $line[5] <> "") ? dateToDb(strtotime($line[5])) : "",
                    'reason_terminated' => (isset($line[6]) && $line[6] <> "") ? $line[6] : "",
                    'from_no' => (isset($line[7]) && $line[7] <> "") ? $line[7] : "",
                    'to_no' => (isset($line[8]) && $line[8] <> "") ? $line[8] : "",
                    'from_dn' => (isset($line[9]) && $line[9] <> "") ? $line[9] : "",
                    'to_dn' => (isset($line[10]) && $line[10] <> "") ? $line[10] : "",
                    'dial_no' => (isset($line[11]) && $line[11] <> "") ? $line[11] : "",
                    'reason_changed' => (isset($line[12]) && $line[12] <> "") ? $line[12] : "",
                    'final_number' => (isset($line[13]) && $line[13] <> "") ? $line[13] : "",
                    'final_dn' => (isset($line[14]) && $line[14] <> "") ? $line[14] : "",
                    'bill_code' => (isset($line[15]) && $line[15] <> "") ? $line[15] : "",
                    'bill_rate' => (isset($line[16]) && $line[16] <> "") ? $line[16] : "",
                    'bill_cost' => (isset($line[17]) && $line[17] <> "") ? $line[17] : "",
                    'bill_name' => (isset($line[18]) && $line[18] <> "") ? $line[18] : "",
                    'chain' => (isset($line[19]) && $line[19] <> "") ? $line[19] : "",
                    'from_type' => (isset($line[20]) && $line[20] <> "") ? $line[20] : "",
                    'to_type' => (isset($line[21]) && $line[21] <> "") ? $line[21] : "",
                    'final_type' => (isset($line[22]) && $line[22] <> "") ? $line[22] : "",
                    'from_dispname' => (isset($line[23]) && $line[23] <> "") ? $line[23] : "",
                    'to_dispname' => (isset($line[24]) && $line[24] <> "") ? $line[24] : "",
                    'final_dispname' => (isset($line[25]) && $line[25] <> "") ? $line[25] : "",
                    'missed_queue_calls' => (isset($line[26]) && $line[26] <> "") ? $line[26] : "",
                    'content' => file_get_contents($file),
                    'created' => dateToDb(filemtime($file)), // datetime the file was created
                );
                
                
                // before insert we search all related data for KPI
                // START KPI 
                // appels sortants
                if (strlen($cdr['from_no']) == 7) {

                    // get the user id by extension
                    if (strlen($cdr['from_dn']) == 3) {

                        $user = User::getByExt($cdr['from_dn']);

                        if (!empty($user)) {

                            $cdr['from_user_id'] = $user['id'];
                            $cdr['user_manager_id'] = $user['user_id'];

                            // search the candidate by phone number
                            $tel_len = strlen($cdr['to_no']);

                            if ($tel_len == 10) {
                                $tel_with_space = ltrim(AddSpaceTelFR(ltrim($cdr['to_no'])));
                            } elseif ($tel_len == 9) {
                                $tel_with_space = ltrim(AddSpaceTelFR('0' . ltrim($cdr['to_no'])));
                            } else {
                                $tel = '0' . substr(formatTelFR(str_replace('+', '', ltrim($cdr['to_no']))), ($tel_len-10), $tel_len);
                                $tel_with_space = ltrim(AddSpaceTelFR($tel));
                            }

                            $sql = 'SELECT * FROM candidats WHERE tel_standard LIKE "%'. $tel_with_space .'%" OR tel_pro LIKE "%'. $tel_with_space .'%" OR tel_mobile_perso LIKE "%'. $tel_with_space .'%" OR tel_domicile LIKE "%'. $tel_with_space .'%" OR tel_ligne_direct LIKE "%'. $tel_with_space .'%"';

                            $candidat = $con->fetchAssoc($sql);

                            if (!empty($candidat)) {
                                $cdr['to_candidat_id'] = $candidat['id'];

                                // get the mission in which we find this candidat and this user
                                $mission = TDC::getMissionByCandidatUser($cdr['to_candidat_id'], $cdr['from_user_id']);
                                if (!empty($mission)) {
                                    $cdr['mission_id'] = $mission['mission_id'];
                                    // get misison manager id
                                    $manager = Mission::getMissionById($mission['mission_id']);
                                    $cdr['mission_manager_id'] = $manager['manager_id'];
                                }

                            }

                        }

                    }      


                // appels entrants    
                } elseif (strlen($cdr['from_no']) > 7) {

                    if ($cdr['from_no'] == "Playfile") {

                        $cdr['messagerie_vocale'] = 1;

                    } else {

                        if (strlen($cdr['to_dn']) == 3) {

                            // get the user id by extension
                            $user = User::getByExt($cdr['to_dn']);

                            if (!empty($user)) {

                                $cdr['to_user_id'] = $user['id'];
                                $cdr['user_manager_id'] = $user['user_id'];

                                // search the candidate by phone number
                                $tel_len = strlen($cdr['from_no']); // from no is always 10 char
                                $tel = formatTelFR(ltrim($cdr['from_no']));
                                $tel_with_space = ltrim(AddSpaceTelFR($tel));

                                $sql = 'SELECT * FROM candidats WHERE tel_standard LIKE "%'. $tel_with_space .'%" OR tel_pro LIKE "%'. $tel_with_space .'%" OR tel_mobile_perso LIKE "%'. $tel_with_space .'%" OR tel_domicile LIKE "%'. $tel_with_space .'%" OR tel_ligne_direct LIKE "%'. $tel_with_space .'%"';

                                $candidat = $con->fetchAssoc($sql);

                                if (!empty($candidat)) {
                                    $cdr['from_candidat_id'] = $candidat['id'];

                                    // get the mission in which we find this candidat and this user
                                    $mission = TDC::getMissionByCandidatUser($cdr['from_candidat_id'], $cdr['to_user_id']);
                                    if (!empty($mission)) {
                                        $cdr['mission_id'] = $mission['mission_id'];
                                        // get misison manager id
                                        $manager = Mission::getMissionById($mission['mission_id']);
                                        $cdr['mission_manager_id'] = $manager['manager_id'];
                                    }

                                }                 

                            }


                        }

                    }  

                }
                // END KPI
                
                
                // insert into table kpi_cdrs
                $con->insert('kpi_cdrs', $cdr);
                
            }
            
            $line_number++;
            
        }
        
        fclose($handle);
        
        return true;
        
    }
    
}
