<?php

/**
 * Mail Helper Using Swift
 *
 * @author faardeen
 */
class Mail {
    
    /**
     * 
     * @param String $subject <p>Subject of the mail</p>
     * @param String $recipient <p>Recipient of the mail</p>
     * @param String/HTML $body <p>The body of the mail</p>
     * @param String $prestataire <p>to display either logo of opsearch or headhunting</p>
     * @param String $sender <p>Email of the sender</p>
     * @return boolean True/False
     */
    public static function sendMail($subject, $recipient, $body, $prestataire = 'opsearch', $sender = "noreply@opsearch.com") {
        
        if (isDev()) {
            
            //initialize headers
            $headers = "MIME-Version: 1.0\n"; 
            $headers .= "Content-type: text/html; charset=utf-8\n"; 
            $headers .= "From: " . MAIL_NOREPLY . " <" . MAIL_NOREPLY . ">\n";
            $headers .= "Reply-to : " . MAIL_NOREPLY . " <" . MAIL_NOREPLY . ">\nX-Mailer:PHP"; 
            $headers .= "Return-Path: " . MAIL_NOREPLY . "\n";
            $headers .= "Content-Transfer-Encoding: 8bit";
            
            $mail_sent_to = "<strong>Recipient</strong>: " . $recipient . "<br/>";
            $body = self::getMailHeader($prestataire) . $mail_sent_to . $body . self::getMailFooter();
            
            //send mail
            if (mail(MAIL_DEV, $subject, $body, $headers)) {
                return true;
            } else {
                return false;
            }
            
        } else {
            // Create the Transport
            $transport = Swift_MailTransport::newInstance(); // uses sendmail for prod ##commented on 2020-12-01 so as to use smtp for mail out
            
//            $transport = Swift_SmtpTransport::newInstance('smtp.office365.com', 587, 'tls')
//                        ->setUsername('noreply@opsearch.com')
//                        ->setPassword('Kop10996');
            
            // note that the mail from must be the same as the username here else it will not work
        
//            if (isDev()) {
//                $transport = (new Swift_SmtpTransport('localhost', 25))
//                    ->setUsername('faardeen@localmail.com')
//                    ->setPassword('caimps');
//            } else {
//                $transport = Swift_SmtpTransport::newInstance('smtp.office365.com', 587, 'tls')
//                        ->setUsername('noreply@opsearch.com')
//                        ->setPassword('Kop10996');
//            }
            
            // Create the Mailer using your created Transport
            $mailer = Swift_Mailer::newInstance($transport);
            
            // initialize mail
            $message = Swift_Message::newInstance();

            // mail subject
            $message->setSubject($subject);

            // mail from addresses array
            $message->setFrom($sender);
            
            // mail recipients array
            ///$message->setTo(MAIL_DEV);
            ///$message->setTo($recipient);
            
            ///if (isDev() || isDemo()) {
                ///$message->setTo(MAIL_DEV);
                ///$mail_sent_to = "<strong>Recipient</strong>: " . $recipient . "<br/>";
                ///$body = self::getMailHeader($prestataire) . $mail_sent_to . $body . self::getMailFooter();
            ///} else {
                $message->setTo($recipient);
                $body = self::getMailHeader($prestataire) . $body . self::getMailFooter();
            ///}
            
            // preparing the body
            ///$mail_sent_to = "<strong>Recipient</strong>: " . $recipient . "<br/>";
            ///$body = self::getMailHeader() . $mail_sent_to . $body . self::getMailFooter();
            ///$body = self::getMailHeader() . $body . self::getMailFooter();
            
            // mail body
            $message->setBody($body, 'text/html');
            
            // send the mail
            if ($mailer->send($message)) {
                return true;
            } else {
                return false;
            }
        }
        
    }

    private static function getMailHeader($prestataire) {
        
        if ($prestataire == "opsearch") {
            $logo_url = "https://www.opsearch.info/asset/images/mail/mail-header-opsearch.jpg";
        } else {
            $logo_url = "https://www.opsearch.info/asset/images/mail/mail-header-headhunting.jpg";
        }
        
        $header = '<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td bgcolor="#858686"><table width="600" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#FFFFFF"><tr><td><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td height="20" bgcolor="#858686"></td></tr></table><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td align="center" style="width:600px; height:56px; background-color:#FFF;"><a href="' . BASE_URL . '" target="_blank"><img src="' . $logo_url . '" width="289" height="56" /></a></td></tr></table><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td height="2" bgcolor="#BD9C7D"></td></tr></table><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td height="25"></td></tr></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:18px;"><tr><td width="20"></td><td>';
        return $header;
    }

    private static function getMailFooter() {
        $footer = '</td><td width="20"></td></tr></table><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td height="25"></td></tr></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:9px;"><tr><td height="20" bgcolor="#906C4A" align="center" style="color:#FFF;">Mail envoyé automatiquement. Merci de ne pas y répondre.</td></tr></table><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td height="20" bgcolor="#858686"></td></tr></table></td></tr></table></td></tr></table>';
        return $footer;
    }

}
