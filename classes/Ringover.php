<?php
class Ringover {
    
    public static $type_calls = array(
        1 => array(
            'direction' => 'in',
            'last_state' => 'ANSWERED',
        ),
        2 => array(
            'direction' => 'in',
            'last_state' => 'MISSED',
        ),
        3 => array(
            'direction' => 'in',
            'last_state' => 'VOICEMAIL',
        ),
        4 => array(
            'direction' => 'out',
            'last_state' => 'ANSWERED',
        ),
        5 => array(
            'direction' => 'out',
            'last_state' => 'CANCELLED',
        ),
        6 => array(
            'direction' => 'out',
            'last_state' => 'FAILED',
        )
    );
    
    public static function API_GetAllCalls($filters = null) {
        
        if (isset($filters['limit_count'])) {
            $limit_count = $filters['limit_count'];
        } else {
            $limit_count = 1000;
        }
        
        $url = 'https://public-api.ringover.com/v2/calls?start_date=' . $filters['start_date'] . '&end_date=' . $filters['end_date'] . '&limit_count=' . $limit_count;
        
        if (isset($filters['last_id_returned'])) {
            $url .= '&last_id_returned=' . $filters['last_id_returned'];
        }
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: bb214aef0ea80347dc24d26dacf04d5873e9fee4'
            ),
        ));
       
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        
        if ($err) {
            return false;
        } 

        return json_decode($response, true);
 
    }
    
    public static function GetAllCallsFromStart() {
        
        global $con;
        
        // ringover was started on 1st May 2021
        // we need to get all these calls only the first time        
        $start_day = strtotime('2021-05-01 00:00:01');
        // $start_day = strtotime('2021-12-11 00:00:01');
                
        do {
            
            echo date('Y-m-d', $start_day) ."<br>\n";
            
            $filters = array(
                'start_date' => date('Y-m-d', $start_day) . 'T00:00:01Z',
                'end_date' => date('Y-m-d', $start_day) . 'T23:59:59Z',
            );            
            
            Ringover::GetAllCalls($filters);
            
            // increment the day by one day
            $start_day = $start_day + 86400;
                    
        } while($start_day <= time()); // it excludes today
        
        
    }
        
    public static function GetAllCalls($filters = null) {
        
        global $con;
        
        if (empty($filters)) { // $str = 'Y-m-d H:i:s';
            $filters['start_date'] = date('Y-m-d') . 'T00:00:01Z';
            $filters['end_date'] = date('Y-m-d') . 'T23:59:59Z';
        }
        
        $num_query_api = 1;
        
        $calls = Ringover::API_GetAllCalls($filters);
        
        // checking if there are more than 1000 calls for the day as the max limit of return calls is 1000 per request
        // if there are more than 1000 calls we must query the API again and set the query param last_id_returned => the cdr_id field
        // also limit of 2 request per second to query the API
        if ($calls['total_call_count'] > $calls['limit_count_setted']) {
            // num query api 
            $num_query_api = round($calls['total_call_count']/$calls['limit_count_setted']) - 1; // minus one because we already query once
            $last_id_returned = end($calls['call_list'])['cdr_id'];
            $filters['last_id_returned'] = $last_id_returned;
            
            for($i = 1; $i <= $num_query_api; $i++) {
                $new_calls = Ringover::API_GetAllCalls($filters);
                $filters['last_id_returned'] = end($new_calls['call_list'])['cdr_id'];
                // append the results in the first call result
                $calls['call_list'] = array_merge($calls['call_list'], $new_calls['call_list']);
            }
            
        } 
        
        // the api does not return anything when there are nothing
        // it shall have return an empty json
        if (!empty($calls) && isset($calls['call_list'])) {
            
            foreach($calls['call_list'] as $call) :

                // if the call log already in the database we do not store it again
                // check if the record exists in our table by the field call_id and cdr_id
                $sql = "SELECT * FROM ringover_calls_log WHERE call_id = '{$call['call_id']}' AND cdr_id = {$call['cdr_id']}";
                $existing_record = $con->fetchAssoc($sql);

                if (empty($existing_record)) {
                    
                    if ($call['answered_time'] <> "") {
                        $answered_time = dateToDb(strtotime($call['answered_time']));
                    } else {
                        $answered_time = $call['answered_time'];
                    }
                    
                    $data = array(
                        'cdr_id' => $call['cdr_id'],
                        'call_id' => $call['call_id'],
                        'type' => $call['type'],
                        'direction' => $call['direction'],
                        'last_state' => $call['last_state'],
                        'start_time' => dateToDb(strtotime($call['start_time'])),
                        'answered_time' => $answered_time,
                        'end_time' => dateToDb(strtotime($call['end_time'])),
                        'incall_duration' => $call['incall_duration'], // the time in call
                        'total_duration' => $call['total_duration'],
                        'contact_number' => $call['contact_number'],
                        'from_number' => $call['from_number'],
                        'to_number' => $call['to_number'],
                        'note' => $call['note'],
                        'user_id' => $call['user']['user_id'],
                        'firstname' => $call['user']['firstname'],
                        'lastname' => $call['user']['lastname'],
                        'company' => $call['user']['company'],
                        'email' => $call['user']['email'],
                        'concat_name' => $call['user']['concat_name'],
                    );
                    
                    // before insert we build all data for KPI
                    
                    // from_user_id or to_user_id and user_manager_id
                    if ($call['user']['user_id'] <> "") {
                        
                        $user = User::getByRingoverUserId($call['user']['user_id']);
                        
                        if (!empty($user)) {

                            if ($call['direction'] === "in") {
                                $data['to_user_id'] = $user['id'];
                            } elseif ($call['direction'] === "out") {
                                $data['from_user_id'] = $user['id'];
                            }

                            // user manager id
                            $data['user_manager_id'] = $user['user_id'];

                        } // if not empty user
                        
                    } // if user is not null
                    
                    // get candidat detail for the call
                    if ($call['direction'] === "in") {
                        $candidat_phone_num = $call['from_number'];
                    } elseif ($call['direction'] === "out") {
                        $candidat_phone_num = $call['to_number'];
                    }
                    // convert the phone number example 33757933858 converted to 07 57 93 38 58 as the number is stored with space for candidates
                    $candidat_phone_num = AddSpaceTelFR(formatTelFR('0' . substr($candidat_phone_num, 2, 10)));
                    // search the candidate by phone number 
                    $sql = 'SELECT * FROM candidats WHERE tel_standard LIKE "%'. $candidat_phone_num .'%" OR tel_pro LIKE "%'. $candidat_phone_num .'%" OR tel_mobile_perso LIKE "%'. $candidat_phone_num .'%" OR tel_domicile LIKE "%'. $candidat_phone_num .'%" OR tel_ligne_direct LIKE "%'. $candidat_phone_num .'%" ORDER BY id DESC LIMIT 1';

                    $candidat = $con->fetchAssoc($sql);

                    if (!empty($candidat)) {

                        if ($call['direction'] === "in") {
                            $data['from_candidat_id'] = $candidat['id'];
                        } elseif ($call['direction'] === "out") {
                            $data['to_candidat_id'] = $candidat['id'];
                        }

                        // get mission id and mission manager id
                        /// $mission = TDC::getMissionByCandidatUser($candidat['id'], $user['id']);
                        $sql = "SELECT * FROM tdc WHERE candidat_id = {$candidat['id']} ORDER BY mission_id DESC LIMIT 1";
                        $mission = $con->fetchAssoc($sql);
                        if (!empty($mission)) {
                            $data['mission_id'] = $mission['mission_id'];
                            // get misison manager id
                            $manager = Mission::getMissionById($mission['mission_id']);
                            $data['mission_manager_id'] = $manager['manager_id'];
                        }

                    } // if not empty candidat
                        
                    // insert the data in the table
                    $con->insert('ringover_calls_log', $data);
                    echo "Data Inserted for date ".$filters['start_date']." <br>\n";

                } else {

                    echo "CDR exist in DB<br>\n";

                }  

            endforeach;
            
        }
         
    }
    
    public static function RelateCandidatsMissions() {
        global $con;
        $sql = "SELECT * FROM ringover_calls_log WHERE from_user_id IS NULL AND to_user_id IS NULL";
        $calls = $con->fetchAll($sql);
        
        foreach($calls as $call) :
            
            $data = array();
            
            if ($call['user_id'] <> "") {
                
                $user = User::getByRingoverUserId($call['user_id']);
                
                if (!empty($user)) {

                    if ($call['direction'] === "in") {
                        $data['to_user_id'] = $user['id'];
                    } elseif ($call['direction'] === "out") {
                        $data['from_user_id'] = $user['id'];
                    }

                    // user manager id
                    $data['user_manager_id'] = $user['user_id'];
                    
                } // if not empty user
                
            } // if user is not null
            
            // get candidat detail for the call
            if ($call['direction'] === "in") {
                $candidat_phone_num = $call['from_number'];
            } elseif ($call['direction'] === "out") {
                $candidat_phone_num = $call['to_number'];
            }
                    
            // convert the phone number example 33757933858 converted to 07 57 93 38 58 as the number is stored with space for candidates
            $candidat_phone_num = AddSpaceTelFR(formatTelFR('0' . substr($candidat_phone_num, 2, 10)));
            // search the candidate by phone number 
            $sql = 'SELECT * FROM candidats WHERE tel_standard LIKE "%'. $candidat_phone_num .'%" OR tel_pro LIKE "%'. $candidat_phone_num .'%" OR tel_mobile_perso LIKE "%'. $candidat_phone_num .'%" OR tel_domicile LIKE "%'. $candidat_phone_num .'%" OR tel_ligne_direct LIKE "%'. $candidat_phone_num .'%" ORDER BY id DESC LIMIT 1';

            $candidat = $con->fetchAssoc($sql);

            if (!empty($candidat)) {

                if ($call['direction'] === "in") {
                    $data['from_candidat_id'] = $candidat['id'];
                } elseif ($call['direction'] === "out") {
                    $data['to_candidat_id'] = $candidat['id'];
                }

                // get mission id and mission manager id
                /// $mission = TDC::getMissionByCandidatUser($candidat['id'], $user['id']);
                $sql = "SELECT * FROM tdc WHERE candidat_id = {$candidat['id']} ORDER BY mission_id DESC LIMIT 1";
                $mission = $con->fetchAssoc($sql);
                if (!empty($mission)) {
                    $data['mission_id'] = $mission['mission_id'];
                    // get misison manager id
                    $manager = Mission::getMissionById($mission['mission_id']);
                    $data['mission_manager_id'] = $manager['manager_id'];
                }

            } // if not empty candidat

            // insert the data in the table
            if (!empty($data)) {
                $con->update('ringover_calls_log', $data, array('id' => $call['id']));
                echo "Call updated - call id = "  . $call['id'] . "<br>\n";
            }

        endforeach;
    } 
    
    // start of KPI
    public static function KPI_CDR($filters = null) {
        
        global $con;
        
        if ($filters) {
            
            $conds = " ";
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $user = User::getUserById($filters['cdr_id']);
                $conds .= "AND R.user_id = {$user['ringover_userid']} ";
            }
                        
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $conds .= "AND R.start_time >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $conds .= "AND R.start_time <= '{$end}' ";           
            }
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $conds .= "AND R.mission_id = {$filters['mission_id']} ";    
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $conds .= " AND M.status IN ({$filters['status']}) ";
            }  

            $sql = "SELECT SUM(R.incall_duration) as duree, R.direction, R.last_state, count(*) as num_calls "
                    . "FROM ringover_calls_log R "
                    . "LEFT JOIN missions M ON M.id = R.mission_id "
                    . "WHERE 1=1 " . $conds
                    . "GROUP BY R.direction, R.last_state";
                        
            return $con->fetchAll($sql);
            
        } else {
            
            $sql = "SELECT SUM(incall_duration) as duree, direction, last_state, count(*) as num_calls "
                    . "FROM ringover_calls_log "
                    . "GROUP BY direction, last_state";
                        
            return $con->fetchAll($sql);
            
        }

        
    }
    
    public static function KPI_CDR_GROUPED_BY_MISSION($filters = null) {
        global $con;
        
        if ($filters) {
            
            $conds = " ";
            $query_mission_status = " ";
            $query_mission_id = " ";
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $user = User::getUserById($filters['cdr_id']);
                $conds .= " AND user_id = {$user['ringover_userid']} ";
            }
                        
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $conds .= " AND start_time >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $conds .= " AND start_time <= '{$end}' ";           
            }
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id .= " AND m.mission_id = {$filters['mission_id']} ";    
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status .= " AND m.status IN ({$filters['status']}) ";
            }  
            
            // get all missions with include_in_kpi = 1 AND cdr_id (user_id) is assigned
            $sql = "SELECT m.id as mission_id, m.poste, m.manager_id, m.manager_name, m.status "
                    . "FROM missions m "
                    . "LEFT JOIN missionsusers mu ON mu.mission_id = m.id "
                    . "WHERE m.include_in_kpi = 1 AND mu.user_id = {$filters['cdr_id']} " . $query_mission_status . $query_mission_id
                    . "ORDER BY m.poste ASC"; 
                        
            $missions = $con->fetchAll($sql);
            
            foreach($missions as $key => $mission) :
                
                foreach(self::$type_calls as $type_call) :
                    
                    $direction = $type_call['direction'];
                    $last_state = $type_call['last_state'];
                
                    $sql = "SELECT SUM(incall_duration) as duree, direction, last_state, count(*) as num_calls "
                            . "FROM ringover_calls_log "
                            . "WHERE direction = '{$direction}' AND last_state = '{$last_state}' AND mission_id = {$mission['mission_id']} " . $conds;
                            
                    $results = $con->fetchAssoc($sql);
                    
                    $missions[$key][$direction][$last_state] = $results;
                
                endforeach;
                
            endforeach;
            
            return $missions;
            
        } else {
            
            // get all missions with include_in_kpi = 1
            $sql = "SELECT M.id as mission_id, M.poste, M.manager_id, M.manager_name, M.status "
                    . "FROM missions M "
                    . "WHERE M.include_in_kpi = 1 AND M.id IN (SELECT DISTINCT(mission_id) FROM ringover_calls_log) "
                    . "ORDER BY id DESC";
            
            $missions = $con->fetchAll($sql);
            
            foreach($missions as $key => $mission) :
                
                foreach(self::$type_calls as $type_call) :
                    
                    $direction = $type_call['direction'];
                    $last_state = $type_call['last_state'];
                
                    $sql = "SELECT SUM(incall_duration) as duree, direction, last_state, count(*) as num_calls "
                            . "FROM ringover_calls_log "
                            . "WHERE direction = '{$direction}' AND last_state = '{$last_state}' AND mission_id = {$mission['mission_id']}";
                            
                    $results = $con->fetchAssoc($sql);
                    
                    $missions[$key][$direction][$last_state] = $results;
                
                endforeach;
                
            endforeach;
            
            return $missions;
            
        }
        
    }
    
    public static function KPI_MANAGER($filters = null) {
        
        global $con;
        
        if ($filters) {
            
            $conds = " ";
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $conds .= " AND R.user_manager_id = {$filters['manager_id']} ";
            } 
                        
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $user = User::getUserById($filters['cdr_id']);
                $conds .= "AND R.user_id = {$user['ringover_userid']} ";
            }
                        
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $conds .= "AND R.start_time >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $conds .= "AND R.start_time <= '{$end}' ";           
            }
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $conds .= "AND R.mission_id = {$filters['mission_id']} ";    
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $conds .= " AND M.status IN ({$filters['status']}) ";
            }  

            $sql = "SELECT SUM(R.incall_duration) as duree, R.direction, R.last_state, count(*) as num_calls "
                    . "FROM ringover_calls_log R "
                    . "LEFT JOIN missions M ON M.id = R.mission_id "
                    . "WHERE 1=1 " . $conds
                    . "GROUP BY R.direction, R.last_state";
                        
            return $con->fetchAll($sql);
            
        } else {
            
            $sql = "SELECT SUM(incall_duration) as duree, direction, last_state, count(*) as num_calls "
                    . "FROM ringover_calls_log "
                    . "GROUP BY direction, last_state";
                        
            return $con->fetchAll($sql);
            
        }

        
    }
    
    public static function KPI_MANAGER_GROUPED_BY_MISSION($filters = null) {
        global $con;
        
        if ($filters) {
            
            $conds = " ";
            $query_mission_status = " ";
            $query_mission_id = " ";
            $query_manager_id = " ";
            $query_cdr_id = " ";
            
            if (isset($filters['manager_id']) && $filters['manager_id'] <> "") {
                $conds .= " AND user_manager_id = {$filters['manager_id']} ";
                $query_manager_id .= " AND m.manager_id = {$filters['manager_id']} ";
            } 
            
            if (isset($filters['cdr_id']) && $filters['cdr_id'] <> "") {
                $user = User::getUserById($filters['cdr_id']);
                $conds .= " AND user_id = {$user['ringover_userid']} ";
                $query_cdr_id .= " AND mu.user_id = {$filters['cdr_id']} ";
            }
                        
            if (isset($filters['start_date']) && $filters['start_date'] <> "") {
                $start = $filters['start_date'] . " 00:00:01";
                $conds .= " AND start_time >= '{$start}' ";
            }

            if (isset($filters['end_date']) && $filters['end_date'] <> "") {
                $end = $filters['end_date'] . " 23:59:59";
                $conds .= " AND start_time <= '{$end}' ";           
            }
            
            if (isset($filters['mission_id']) && $filters['mission_id'] <> "") {
                $query_mission_id .= " AND m.mission_id = {$filters['mission_id']} ";    
            } 
            
            if (isset($filters['status']) && $filters['status'] <> "") {
                $query_mission_status .= " AND m.status IN ({$filters['status']}) ";
            }  
            
            // get all missions with include_in_kpi = 1 AND cdr_id (user_id) is assigned
            $sql = "SELECT m.id as mission_id, m.poste, m.manager_id, m.manager_name, m.status "
                    . "FROM missions m "
                    . "LEFT JOIN missionsusers mu ON mu.mission_id = m.id "
                    . "WHERE m.include_in_kpi = 1 " . $query_mission_status . $query_mission_id . $query_manager_id . $query_cdr_id
                    . "ORDER BY m.poste ASC"; 
                        
            $missions = $con->fetchAll($sql);
            
            foreach($missions as $key => $mission) :
                
                foreach(self::$type_calls as $type_call) :
                    
                    $direction = $type_call['direction'];
                    $last_state = $type_call['last_state'];
                
                    $sql = "SELECT SUM(incall_duration) as duree, direction, last_state, count(*) as num_calls "
                            . "FROM ringover_calls_log "
                            . "WHERE direction = '{$direction}' AND last_state = '{$last_state}' AND mission_id = {$mission['mission_id']} " . $conds;
                            
                    $results = $con->fetchAssoc($sql);
                    
                    $missions[$key][$direction][$last_state] = $results;
                
                endforeach;
                
            endforeach;
            
            return $missions;
            
        } else {
            
            // get all missions with include_in_kpi = 1
            $sql = "SELECT M.id as mission_id, M.poste, M.manager_id, M.manager_name, M.status "
                    . "FROM missions M "
                    . "WHERE M.include_in_kpi = 1 AND M.id IN (SELECT DISTINCT(mission_id) FROM ringover_calls_log) "
                    . "ORDER BY id DESC";
            
            $missions = $con->fetchAll($sql);
            
            foreach($missions as $key => $mission) :
                
                foreach(self::$type_calls as $type_call) :
                    
                    $direction = $type_call['direction'];
                    $last_state = $type_call['last_state'];
                
                    $sql = "SELECT SUM(incall_duration) as duree, direction, last_state, count(*) as num_calls "
                            . "FROM ringover_calls_log "
                            . "WHERE direction = '{$direction}' AND last_state = '{$last_state}' AND mission_id = {$mission['mission_id']}";
                            
                    $results = $con->fetchAssoc($sql);
                    
                    $missions[$key][$direction][$last_state] = $results;
                
                endforeach;
                
            endforeach;
            
            return $missions;
            
        }
        
    }
    
}
