<?php

/**
 * Description of Role
 *
 */
class Migration {
    
    public static function langues() {
        global $con;
        $sql = "SELECT * FROM ctrl_langue ORDER BY langue ASC";
        $results = $con->fetchAll($sql);
        foreach($results as $result) {
            $con->insert('controles', array('type_id' => 5, 'name' => $result['langue']));
        }
    }
    
    public static function secteurs() {
        global $con;
        $sql = "SELECT * FROM ctrl_secteur ORDER BY secteur ASC";
        $results = $con->fetchAll($sql);
        foreach($results as $result) {
            $con->insert('controles', array('type_id' => 2, 'name' => $result['secteur']));
        }
    }
    
    public static function sources() {
        global $con;
        $sql = "SELECT * FROM ctrl_source ORDER BY source ASC";
        $results = $con->fetchAll($sql);
        foreach($results as $result) {
            $con->insert('controles', array('type_id' => 3, 'name' => $result['source']));
        }
    }
    
    public static function formations() {
        global $con;
        $sql = "SELECT * FROM ctrl_formation ORDER BY name ASC";
        $results = $con->fetchAll($sql);
        foreach($results as $result) {
            $con->insert('controles', array('type_id' => 4, 'name' => $result['name']));
        }
    }
    
    public static function zones() {
        global $con;
        $sql = "SELECT * FROM ctrl_zone ORDER BY name ASC";
        $results = $con->fetchAll($sql);
        foreach($results as $result) {
            $con->insert('controles', array('type_id' => 6, 'name' => $result['name']));
        }
    }

}


