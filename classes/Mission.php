<?php
class Mission {
    
    public static function getTDCMissionsClient($manager_id = null) {
        
        global $con;
        
        $where = " ";
        if ($manager_id && $manager_id > 0) {
            $where = " WHERE M.manager_id = {$manager_id} ";
        }
        
        $sql = "SELECT DISTINCT(M.id) as mission_id, M.manager_name, M.poste, M.date_debut, M.date_fin, M.`status`, C.nom as client
FROM usersclientmissions UCM
LEFT JOIN missions M ON M.id = UCM.mission_id
LEFT JOIN clients C ON C.id = M.client_id 
$where
ORDER BY M.id DESC";
        
        return $con->fetchAll($sql);
        
    }
    
    public static function getMissionsListSearch($mission) {
        global $con;
                
        $sql = "SELECT m.*, u.user_id, c.nom as client, x.firstName, x.lastName, p.nom as prestataire "
                . "FROM missions m, missionsusers u, clients c, users x, prestataires p "
                . "WHERE m.id = u.mission_id AND u.user_id = {$mission['user_id']} "
                . "AND c.id = m.client_id AND x.id = m.consultant_id AND p.id = m.prestataire_id AND m.is_private = 0 ";
                    
       
        if ($mission['all'] == 'on' && User::can('read_all_mission')) {
            $sql = "SELECT m.*, c.nom as client, x.firstName, x.lastName, p.nom as prestataire "
                . "FROM missions m, clients c, users x, prestataires p "
                . "WHERE c.id = m.client_id AND x.id = m.consultant_id AND p.id = m.prestataire_id AND m.is_private = 0 ";
        }
        
        if ($mission['user_id'] == 301 || $mission['user_id'] == 481 || $mission['user_id'] == 5) { // bastien and sweety pursun and emeline peralta to view private mission
            
            $sql = "SELECT m.*, u.user_id, c.nom as client, x.firstName, x.lastName, p.nom as prestataire "
                . "FROM missions m, missionsusers u, clients c, users x, prestataires p "
                . "WHERE m.id = u.mission_id AND u.user_id = {$mission['user_id']} "
                . "AND c.id = m.client_id AND x.id = m.consultant_id AND p.id = m.prestataire_id ";            
            
            
            if ($mission['all'] == 'on' && User::can('read_all_mission')) {
                $sql = "SELECT m.*, c.nom as client, x.firstName, x.lastName, p.nom as prestataire "
                    . "FROM missions m, clients c, users x, prestataires p "
                    . "WHERE c.id = m.client_id AND x.id = m.consultant_id AND p.id = m.prestataire_id ";
            }    
                
        }
        
        
        if ($mission['id'] <> "")
            $sql .= " AND m.id  = {$mission['id']}";
            
        if ($mission['prestataire_id'] <> "")
            $sql .= " AND m.prestataire_id  = {$mission['prestataire_id']}";
        
        if ($mission['client_id'] <> "")
            $sql .= " AND m.client_id  = {$mission['client_id']}";    
        
        if ($mission['consultant_id'] <> "")
            $sql .= " AND m.consultant_id  = {$mission['consultant_id']}";    
            
        if ($mission['poste'] <> "")
            $sql .= " AND m.poste LIKE '%{$mission['poste']}%'";
        
        if ($mission['control_secteur_id'] <> "")
            $sql .= " AND m.control_secteur_id = {$mission['control_secteur_id']}";    
            
        if ($mission['control_localisation_id'] <> "")
            $sql .= " AND m.control_localisation_id = {$mission['control_localisation_id']}";
              
        if ($mission['control_fonction_id'] <> "")
            $sql .= " AND m.control_fonction_id = {$mission['control_fonction_id']}";
            
        if ($mission['control_niveauFormation_id'] <> "")
            $sql .= " AND m.control_niveauFormation_id = {$mission['control_niveauFormation_id']}";
        
        if ($mission['status'] <> "")
            $sql .= " AND m.status = {$mission['status']}";
        
        if ($mission['remuneration_from'] <> "" && $mission['remuneration_to'] == "") {
            $sql .= " AND m.remuneration_min >= {$mission['remuneration_min']}";
        } elseif ($mission['remuneration_from'] == "" && $mission['remuneration_to'] <> "") {
            $sql .= " AND m.remuneration_max <= {$mission['remuneration_max']}";
        } elseif ($mission['remuneration_from'] <> "" && $mission['remuneration_to'] <> "") {
            $sql .= " AND m.remuneration_min >= {$mission['remuneration_from']} AND m.remuneration_max <= {$mission['remuneration_to']}";
        }
                
        // age difference
        if ($mission['age_from'] <> "" && $mission['age_to'] == "") {
            
            $age_from = getDateOfBirth($mission['age_from']);
            $sql .= " AND m.age_min >= {$age_from}";
            
        } elseif ($mission['age_from'] == "" && $mission['age_to'] <> "") {
            
            $age_to = getDateOfBirth($mission['age_to']);
            $sql .= " AND m.age_max <= {$age_to}";
            
        } elseif ($mission['age_from'] <> "" && $mission['age_to'] <> "") {
            
            $age_from = getDateOfBirth($mission['age_from']);
            $age_to = getDateOfBirth($mission['age_to']);
            $sql .= " AND m.age_min >= {$age_from} AND m.age_max <= {$age_to}";
            
        }
        
        $sql .= " ORDER BY m.id";
             
        return $con->fetchAll($sql);
    }
    
    public static function delete($mission_id) {
        global $con;
        $con->update('missions', array('status' => 235, 'updated' => time()), array('id' => $mission_id));
        return true;
    }
    
    public static function deleteReal($mission_id) {
        global $con;
        
        $content_before = self::getMissionById($mission_id);
        
        $con->delete('missions', array('id' => $mission_id));
        $con->delete('missionsusers', array('mission_id' => $mission_id));
        //watchdog
        $log  = array(
            'module_name' => 'Missions',
            'module_code' => 'mod_mission',
            'action' => 'Supprimer Mission',
            'action_code' => 'mod_mission_delete',
            'table_name' => 'missions',
            'table_id' => $mission_id,
            'content_type' => 'json',
            'content_before' => json_encode($content_before),
        );
        Watchdog::Log($log);
        return true;
    }
    
    public static function getSuivis($mission_id) {
        global $con;
        $sql = "SELECT s.*, c.nom, c.prenom "
            . "FROM tdcsuivis s, candidats c "
            . "WHERE s.candidat_id = c.id AND s.mission_id = {$mission_id} ORDER BY s.id DESC";
            
        return $con->fetchAll($sql);
    }
    
    public static function isMissionForMe($user_id, $mission_id) {
        global $con;
        $sql = "SELECT * FROM missionsusers WHERE user_id = {$user_id} AND mission_id = {$mission_id}";
        $result = $con->fetchAssoc($sql);
        if (empty($result)) {
            return false;
        } else {
            return true;
        }
    }
    
    public static function search($type, $value, $all = false) {
        
        global $con;
        
        $sql = "SELECT m.*, u.user_id, c.nom as client, x.firstName, x.lastName, n.name as fonction, p.nom as prestataire "
                . "FROM missions m, missionsusers u, clients c, users x, controles n, controles sect, prestataires as p "
                . "WHERE m.id = u.mission_id AND u.user_id = {$_SESSION['opsearch_user']['id']} "
                . "AND c.id = m.client_id AND x.id = m.consultant_id AND n.type_id = 1 AND p.id = m.prestataire_id "
                . "AND m.control_fonction_id = n.id AND sect.id = m.control_secteur_id";
        
        if ($all && User::can('read_all_mission')) {
            $sql = "SELECT m.*, c.nom as client, x.firstName, x.lastName, n.name as fonction, p.nom as prestataire "
                . "FROM missions m, clients c, users x, controles n, controles sect, prestataires as p "
                . "WHERE c.id = m.client_id AND x.id = m.consultant_id AND n.type_id = 1 AND p.id = m.prestataire_id "
                . "AND m.control_fonction_id = n.id AND sect.id = m.control_secteur_id";
        }        
                
        if ($type == 'all') {
            
            $sql .= " AND (n.name LIKE '%{$value}%' OR m.poste LIKE '%{$value}%' OR c.nom LIKE '%{$value}%') ORDER BY m.id";
            
        } elseif ($type == "fonction") {
            
            $sql .= " AND n.name LIKE '%{$value}%' ORDER BY n.name ASC";
            
        } elseif ($type == "poste") {
            
            $sql .= " AND m.poste LIKE '%{$value}%' ORDER BY m.poste ASC";
            
        } elseif ($type == "client") {
            
            $sql .= " AND c.nom LIKE '%{$value}%' ORDER BY c.nom ASC";
            
        } elseif ($type == "secteur") {
            
            $sql .= " AND sect.name LIKE '%{$value}%' ORDER BY c.nom ASC";
            
        }
                
        return $con->fetchAll($sql);
    }  
    
    public static function addMission($mission, $commentaire, $out_category, $criteres, $assignedUsers, $files) {
        global $con;
        
        // get manager name
        $manager = User::getUserById($mission['manager_id']);
        $mission['manager_name'] = mb_strtoupper($manager['lastName']) . ' ' . mb_ucfirst($manager['firstName']);
        
        $con->insert('missions', $mission);
        
        $mission_id = $con->lastInsertId();
        
        //watchdog
        $log  = array(
            'module_name' => 'Mission',
            'module_code' => 'mod_mission',
            'action' => 'Ajouter mission',
            'action_code' => 'mod_mission_create_mission',
            'table_name' => 'missions',
            'table_id' => $mission_id,
            'content_type' => 'json',
            'content_before' => json_encode($mission),
            'status_before_id' => $mission['status'],
            'status_before' => Control::getControlNameById($mission['status']),
        );        
        Watchdog::Log($log);
        
        self::addComment($mission_id, $commentaire);
        self::addDocuments($mission_id, $files);
        self::addAssignedUsers($mission_id, $assignedUsers);
        self::addCriteres($mission_id, $criteres);
        self::addOutCategory($mission_id, $out_category);
        
        // add rows for the tdccolorslegend
        $colors_tdc = Control::getColorTDC();
        foreach($colors_tdc as $color):
            $con->insert('tdccolorslegend', array('mission_id' => $mission_id, 'colorstdc_id' => $color['id'], 'updated' => time()));
        endforeach;
        
        // add rows for the tdccolorslegend
        $colors_tdc_ent = Control::getColorTDCEntreprise();
        foreach($colors_tdc_ent as $color):
            $con->insert('tdccolorslegendentreprise', array('mission_id' => $mission_id, 'colorstdc_id' => $color['id'], 'updated' => time()));
        endforeach;
                
        // send mail to the users to inform that they have been assigned this mission
        self::mailUsers($mission_id, $assignedUsers);
        
        return $mission_id;
        
    }
    
    public static function duplicate($mission_id) {
        global $con;
        // 1. mission base
        $mission = self::getMissionById($mission_id);
        unset($mission['id']);
        $con->insert('missions', $mission);
        $new_mission_id = $con->lastInsertId();
        
        //watchdog
        $log  = array(
            'module_name' => 'Mission',
            'module_code' => 'mod_mission',
            'action' => 'Dupliquer mission',
            'action_code' => 'mod_mission_duplicate_mission',
            'table_name' => 'missions',
            'table_id' => $new_mission_id,
            'duplicate_from_id' => $mission_id,
            'content_type' => 'json',
            'content_before' => json_encode($mission),
            'status_before_id' => $mission['status'],
            'status_before' => Control::getControlNameById($mission['status']),
        );        
        Watchdog::Log($log);
        
        // 2. out category
        $categories = self::getOutCategory($mission_id);
        if (!empty($categories)) {
            foreach($categories as $category):   
                $con->insert('missionsoutcategories', array('mission_id' => $new_mission_id, 'out_categorie' => $category['out_categorie']));
            
                $id = $con->lastInsertId();
        
                //watchdog
                $log  = array(
                    'module_name' => 'Mission',
                    'module_code' => 'mod_mission',
                    'action' => 'Ajouter mission out categorie',
                    'action_code' => 'mod_mission_duplicate_mission_create_outcategory',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'missionsoutcategories',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode(array('mission_id' => $new_mission_id, 'out_categorie' => $category['out_categorie'])),
                    'duplicate_from_id' => $mission_id,
                );
                Watchdog::Log($log);
            
            endforeach;
        }
        
        // 3. critere specs
        $criteres = self::getCriteres($mission_id);
        if (!empty($criteres)) {
            foreach($criteres as $critere):   
                $con->insert('missionscriteres', array('mission_id' => $new_mission_id, 'ordre' => $critere['ordre'], 'criteres' => $critere['criteres']));
            
                $id = $con->lastInsertId();
                
                //watchdog
                $log  = array(
                    'module_name' => 'Mission',
                    'module_code' => 'mod_mission',
                    'action' => 'Ajouter mission criteres',
                    'action_code' => 'mod_mission_duplicate_mission_create_critere',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'missionscriteres',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode(array('mission_id' => $new_mission_id, 'ordre' => $critere['ordre'], 'criteres' => $critere['criteres'])),
                    'duplicate_from_id' => $mission_id,
                );
                Watchdog::Log($log);
            
            endforeach;
        }
        
        // 4. comments
        $comments = self::getCommentsByMissionId($mission_id);
        if (!empty($comments)) {
            foreach($comments as $comment):
                unset($comment['id']);
                $comment['mission_id'] = $new_mission_id;
                $con->insert('missionscommentaires', $comment);
                
                $id = $con->lastInsertId();
        
                //watchdog
                $log  = array(
                    'module_name' => 'Mission',
                    'module_code' => 'mod_mission',
                    'action' => 'Ajouter mission commentaire',
                    'action_code' => 'mod_mission_duplicate_mission_create_comment',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'missionscommentaires',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode($comment),
                    'duplicate_from_id' => $mission_id,
                );
                Watchdog::Log($log);
                
            endforeach;
        }
            
        // 5. affecte a
        $assignees = self::getAssignee($mission_id);
        foreach($assignees as $assignee):   
            $con->insert('missionsusers', array('mission_id' => $new_mission_id, 'user_id' => $assignee['user_id']));
        
            $id = $con->lastInsertId();
        
            //watchdog
            $log  = array(
                'module_name' => 'Mission',
                'module_code' => 'mod_mission',
                'action' => 'Ajouter mission utilisateurs',
                'action_code' => 'mod_mission_duplicate_mission_create_user',
                'table_parent_name' => 'missions',
                'table_parent_id' => $mission_id,
                'table_name' => 'missionsusers',
                'table_id' => $id,
                'content_type' => 'json',
                'content_before' => json_encode(array('mission_id' => $new_mission_id, 'user_id' => $assignee['user_id'])),
                'duplicate_from_id' => $mission_id,
            );
            Watchdog::Log($log);
        
        endforeach;
        
        // 6. documents
        $documents = self::getDocuments($mission_id);
        
        if (!empty($documents)) {
            
            foreach($documents as $doc):
                $con->insert('missionsdocuments', array('mission_id' => $new_mission_id, 'file' => $doc['file'], 'created' => $doc['created']));
            
                $id = $con->lastInsertId();
        
                //watchdog
                $log  = array(
                    'module_name' => 'Mission',
                    'module_code' => 'mod_mission',
                    'action' => 'Ajouter mission document',
                    'action_code' => 'mod_mission_duplicate_mission_create_document',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'missionsdocuments',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode(array('mission_id' => $new_mission_id, 'file' => $doc['file'], 'created' => $doc['created'])),
                    'duplicate_from_id' => $mission_id,
                );
                Watchdog::Log($log);
            
            endforeach;
            
            // clone the folder with its content
            $src = MISSION_PATH . '/' . $mission_id;
            $dst = MISSION_PATH . '/' . $new_mission_id;
            recurse_copy($src, $dst);
        }

        // concerning TDC
        // 1. base TDC
        $tdcs = self::getTDCByMissionId($mission_id);
        if (!empty($tdcs)) {
            foreach($tdcs as $tdc):
                $tdc_id = $tdc['id'];
                unset($tdc['id']);
                
                $tdc['mission_id'] = $new_mission_id;
                $con->insert('tdc', $tdc);
                
                $id = $con->lastInsertId();
                
                //watchdog
                $log  = array(
                    'module_name' => 'TDC',
                    'module_code' => 'mod_tdc',
                    'action' => 'Ajouter candidat tdc',
                    'action_code' => 'mod_tdc_add_candidat',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $new_mission_id,
                    'table_name' => 'tdc',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode($tdc),
                    'duplicate_from_id' => $tdc_id,
                );
                Watchdog::Log($log);
                
            endforeach;
        }
            
        // 2. tdc criteres
        $tdc_criteres = self::getTDCCriteresByMissionId($mission_id);
        if (!empty($tdc_criteres)) {
            foreach($tdc_criteres as $tdc_critere):
                
                $tdc_critere_id = $tdc_critere['id'];
                
                unset($tdc_critere['id']);
                $tdc_critere['mission_id'] = $new_mission_id;
                $con->insert('tdccriteres', $tdc_critere);
                
                $id = $con->lastInsertId();
                
                //watchdog
                $log  = array(
                    'module_name' => 'TDC',
                    'module_code' => 'mod_tdc',
                    'action' => 'Ajouter TDC Critères spécifiques',
                    'action_code' => 'mod_tdc_add_criteres',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $new_mission_id,
                    'table_name' => 'tdccriteres',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode($tdc_critere),
                    'duplicate_from_id' => $tdc_critere_id,
                );
                Watchdog::Log($log);
                
            endforeach;
        }
        
        // 3. tdc out categories
        $tdc_categories = self::getTDCCatgoriesByMissionId($mission_id);
        if (!empty($tdc_categories)) {
            foreach($tdc_categories as $tdc_category):
                $tdc_category_id = $tdc_category['id'];
                unset($tdc_category['id']);
                $tdc_category['mission_id'] = $new_mission_id;
                $con->insert('tdcoutcategories', $tdc_category);
                
                $id = $con->lastInsertId();
                
                //watchdog
                $log  = array(
                    'module_name' => 'TDC',
                    'module_code' => 'mod_tdc',
                    'action' => 'Ajouter TDC Out Categorie',
                    'action_code' => 'mod_tdc_add_outcategory',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $new_mission_id,
                    'table_name' => 'tdcoutcategories',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode($tdc_category),
                    'duplicate_from_id' => $tdc_category_id,
                );
                Watchdog::Log($log);
                
            endforeach;
        }
            
        // 4. tdc historiques
        $tdc_historiques = self::getTDCHistoriqueByMissionId($mission_id);
        if (!empty($tdc_historiques)) {
            foreach($tdc_historiques as $tdc_historique):
                
                $tdc_historique_id = $tdc_historique['id'];
                
                unset($tdc_historique['id']);
                $tdc_historique['mission_id'] = $new_mission_id;
                $con->insert('tdchistoriques', $tdc_historique);
                
                $id = $con->lastInsertId();
                
                //watchdog
                $log  = array(
                    'module_name' => 'TDC',
                    'module_code' => 'mod_tdc',
                    'action' => 'Ajouter TDC Historique',
                    'action_code' => 'mod_tdc_add_historique',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $new_mission_id,
                    'table_name' => 'tdchistoriques',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode($tdc_historique),
                    'duplicate_from_id' => $tdc_historique_id,
                );
                Watchdog::Log($log);
                
            endforeach;
        }
             
        // 5. tdc suivis
        $tdc_suivis = self::getTDCSuivisByMissionId($mission_id);
        if (!empty($tdc_suivis)) {
            foreach($tdc_suivis as $tdc_suivi):
                
                $tdc_suivi_id = $tdc_suivi['id'];
                
                unset($tdc_suivi['id']);
                $tdc_suivi['mission_id'] = $new_mission_id;
                $con->insert('tdcsuivis', $tdc_suivi);
                
                $id = $con->lastInsertId();
                
                //watchdog
                $log  = array(
                    'module_name' => 'TDC',
                    'module_code' => 'mod_tdc',
                    'action' => 'Ajouter TDC Suivi',
                    'action_code' => 'mod_tdc_add_suivi',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $new_mission_id,
                    'table_name' => 'tdcsuivis',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode($tdc_suivi),
                    'duplicate_from_id' => $tdc_suivi_id,
                );
                Watchdog::Log($log);
                
            endforeach;
        }
            
        // 6. tdc legend colors
        $tdc_legend_colors = TDC::getColorsLegend($mission_id);
        foreach($tdc_legend_colors as $colors):
            unset($colors['id']);
            unset($colors['hex']);
            $colors['mission_id'] = $new_mission_id;
            $con->insert('tdccolorslegend', $colors);
        endforeach;
        
        return $new_mission_id;
    }
    
    
    
    public static function getTDCSuivisByMissionId($mission_id) {
        global $con;
        $sql = "SELECT * FROM tdcsuivis WHERE mission_id = {$mission_id} ORDER BY id ASC";
        return $con->fetchAll($sql);
    }
    
    public static function getTDCHistoriqueByMissionId($mission_id) {
        global $con;
        $sql = "SELECT * FROM tdchistoriques WHERE mission_id = {$mission_id} ORDER BY id ASC";
        return $con->fetchAll($sql);
    }
    
    public static function getTDCCatgoriesByMissionId($mission_id) {
        global $con;
        $sql = "SELECT * FROM tdcoutcategories WHERE mission_id = {$mission_id} ORDER BY id ASC";
        return $con->fetchAll($sql);
    }
    
    public static function getTDCCriteresByMissionId($mission_id) {
        global $con;
        $sql = "SELECT * FROM tdccriteres WHERE mission_id = {$mission_id} ORDER BY id ASC";
        return $con->fetchAll($sql);
    }


    public static function getTDCByMissionId($mission_id) {
        global $con;
        $sql = "SELECT * FROM tdc WHERE mission_id = {$mission_id} ORDER BY id ASC";
        return $con->fetchAll($sql);
    }

    public static function editMission($mission, $out_category, $out_category_ids, $criteres, $criteres_ids, $assignedUsers) {
        global $con;
        
        $mission_before = Mission::getMissionById($mission['id']);
        
        $status_old = $mission['status_old'];
        unset($mission['status_old']);
        
        $manager_old = $mission['old_manager_id'];
        unset($mission['old_manager_id']);
        
        $manager_name_old = $mission['old_manager_name'];
        unset($mission['old_manager_name']);
        
        if ($manager_old <> $mission['manager_id']) {
            $manager = User::getUserById($mission['manager_id']);
            $mission['manager_name'] = mb_strtoupper($manager['lastName']) . ' ' . mb_ucfirst($manager['firstName']);
        }
        
        $con->update('missions', $mission, array('id' => $mission['id']));
        
        // update manager_id in table kpi_time_in
        $con->update('kpi_time_in', array('manager_id' => $mission['manager_id']), array('mission_id' => $mission['id']));
        
        // update manager_id and mision start date and end date in table kpi_missions_shortlist
        $con->update('kpi_missions_shortlist', array('manager_id' => $mission['manager_id'], 'mission_date_debut' => dateToDb($mission['date_debut']), 'mission_date_fin' => dateToDb($mission['date_fin'])), array('mission_id' => $mission['id']));
        
        //watchdog
        $log  = array(
            'module_name' => 'Mission',
            'module_code' => 'mod_mission',
            'action' => 'Éditer mission',
            'action_code' => 'mod_mission_edit_mission',
            'table_name' => 'missions',
            'table_id' => $mission['id'],
            'content_type' => 'json',
            'content_before' => json_encode($mission_before),
            'content_after' => json_encode($mission),
        );
        
        // if mission status changes we log the status
        if ($status_old <> $mission['status']) {
            $log['status_before_id'] = $status_old;
            $log['status_before'] = Control::getControlNameById($status_old);
            $log['status_after_id'] = $mission['status'];
            $log['status_after'] = Control::getControlNameById($mission['status']);
        }
        
        if ($manager_old <> $mission['manager_id']) {
            $log['manager_before_id'] = $manager_old;
            $log['manager_before'] = $manager_name_old;
            $log['manager_after_id'] = $mission['manager_id'];
            $log['manager_after'] = $mission['manager_name'];
        }
        
        Watchdog::Log($log);
        
        self::EditAssignedUsers($mission['id'], $assignedUsers);
        self::EditCriteres($mission['id'], $criteres, $criteres_ids);
        self::EditOutCategory($mission['id'], $out_category, $out_category_ids);
        
        return true;
        
    }
    
    public static function EditOutCategory($mission_id, $out_categories, $out_categories_id) {
        global $con;
        
        // possibilities
        // 1. new critere has been added
        // 2. existing critere has been updated
        // 3. existing criteres has been deleted
        // @note has used criteres as variables name here as its the same thing except order 
        
        // build the arrays
        if ($out_categories_id == "") {
            $critere_ids = array();
        } else {
            $critere_ids = explode('|', $out_categories_id);
        }
        
        if ($out_categories == "") {
            $criteres = array();
        } else {
            $criteres = explode('|', $out_categories);
        }
                
        // select all criteres for this mission
        $count = 0;
        $processed_criteres_ids = array();
        if (!empty($critere_ids)) {
            foreach($critere_ids as $critere_id):
                if ($critere_id == "new") {

                    $con->insert('missionsoutcategories', array('mission_id' => $mission_id, 'out_categorie' => $criteres[$count]));
                    
                    $id = $con->lastInsertId();
        
                    //watchdog
                    $log  = array(
                        'module_name' => 'Mission',
                        'module_code' => 'mod_mission',
                        'action' => 'Ajouter mission out categorie',
                        'action_code' => 'mod_mission_create_mission_outcategory',
                        'table_parent_name' => 'missions',
                        'table_parent_id' => $mission_id,
                        'table_name' => 'missionsoutcategories',
                        'table_id' => $id,
                        'content_type' => 'json',
                        'content_before' => json_encode(array('mission_id' => $mission_id, 'out_categorie' => $criteres[$count])),
                    );
                    Watchdog::Log($log);
                    
                    $processed_criteres_ids[] = $id;
                } else {

                    $actual_critere = self::getOutCategoryById($critere_id);

                    if (!empty($actual_critere)) { // we update the critere => it may have been modified
                        $data = array(
                            'mission_id' => $mission_id,
                            'out_categorie' => $criteres[$count]
                        );
                        $con->update('missionsoutcategories', $data, array('id' => $critere_id));
                        $processed_criteres_ids[] = $critere_id;
                        
                        //watchdog
                        $log  = array(
                            'module_name' => 'Mission',
                            'module_code' => 'mod_mission',
                            'action' => 'Éditer mission out categorie',
                            'action_code' => 'mod_mission_edit_mission_outcategory',
                            'table_parent_name' => 'missions',
                            'table_parent_id' => $mission_id,
                            'table_name' => 'missionsoutcategories',
                            'table_id' => $critere_id,
                            'content_type' => 'json',
                            'content_before' => json_encode($data),
                        );
                        Watchdog::Log($log);
                        
                    }              
                }   

                $count++;
            endforeach;
        }

        // get all ids from table
        $all_ids = self::getOutCategoryIdList($mission_id);
                
        foreach($all_ids as $existing_id):
            
            if (!in_array($existing_id, $processed_criteres_ids)) {
                $con->delete('missionsoutcategories', array('id' => $existing_id));
                
                //watchdog
                $log  = array(
                    'module_name' => 'Mission',
                    'module_code' => 'mod_mission',
                    'action' => 'Supprimer mission out categorie',
                    'action_code' => 'mod_mission_delete_mission_outcategory',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'missionsoutcategories',
                    'table_id' => $existing_id,
                );
                Watchdog::Log($log);
                
            }
            
        endforeach;
        
        return true;
    }
    
    public static function EditCriteres($mission_id, $criteres_arr, $criteres_ids) {
        global $con;
                
        // possibilities
        // 1. new critere has been added
        // 2. existing critere has been updated
        // 3. existing criteres has been deleted
                
        // build the arrays
        if ($criteres_ids == "") {
            $critere_ids = array();
        } else {
            $critere_ids = explode('|', $criteres_ids);
        }
        
        if ($criteres_arr == "") {
            $criteres = array();
        } else {
            $criteres = explode('|', $criteres_arr);
        }
                
        // select all criteres for this mission
        $count = 0;
        $index = 1;
        $processed_criteres_ids = array();
        if (!empty($critere_ids)) {
            foreach($critere_ids as $critere_id):

                if ($critere_id == "new") {
                    
                    $con->insert('missionscriteres', array('mission_id' => $mission_id, 'ordre' => $index, 'criteres' => $criteres[$count]));
                                        
                    $id = $con->lastInsertId();
                                        
                    //watchdog
                    $log  = array(
                        'module_name' => 'Mission',
                        'module_code' => 'mod_mission',
                        'action' => 'Ajouter mission criteres',
                        'action_code' => 'mod_mission_create_mission_critere',
                        'table_parent_name' => 'missions',
                        'table_parent_id' => $mission_id,
                        'table_name' => 'missionscriteres',
                        'table_id' => $id,
                        'content_type' => 'json',
                        'content_before' => json_encode(array('mission_id' => $mission_id, 'ordre' => $index, 'criteres' => $criteres[$count])),
                    );
                    Watchdog::Log($log);
                    
                    $index++;
                    $processed_criteres_ids[] = $id;
                } else {

                    $actual_critere = self::getCriteresById($critere_id);

                    if (!empty($actual_critere)) { // we update the critere => it may have been modified
                        $data = array(
                            'mission_id' => $mission_id,
                            'ordre' => $index,
                            'criteres' => $criteres[$count]
                        );
                        $con->update('missionscriteres', $data, array('id' => $critere_id));
                        
                        //watchdog
                        $log  = array(
                            'module_name' => 'Mission',
                            'module_code' => 'mod_mission',
                            'action' => 'Éditer mission criteres',
                            'action_code' => 'mod_mission_edit_mission_critere',
                            'table_parent_name' => 'missions',
                            'table_parent_id' => $mission_id,
                            'table_name' => 'missionscriteres',
                            'table_id' => $critere_id,
                            'content_type' => 'json',
                            'content_before' => json_encode($data),
                        );
                        Watchdog::Log($log);
                        
                        $index++;
                        $processed_criteres_ids[] = $critere_id;
                    }              
                }   

                $count++;
            endforeach;
        }
            
        // get all ids from table
        $all_ids = self::getCriteresIdList($mission_id);
        
        foreach($all_ids as $existing_id):
            
            if (!in_array($existing_id, $processed_criteres_ids)) {
                $con->delete('missionscriteres', array('id' => $existing_id));
                
                //watchdog
                $log  = array(
                    'module_name' => 'Mission',
                    'module_code' => 'mod_mission',
                    'action' => 'Supprimer mission criteres',
                    'action_code' => 'mod_mission_delete_mission_critere',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'missionscriteres',
                    'table_id' => $existing_id,
                );
                Watchdog::Log($log);
                
            }
            
        endforeach;
        
        return true;
    }
    
    public static function mailUsers($mission_id, $assignedUsers) {
        $user_ids = explode('|', $assignedUsers);
        
        foreach($user_ids as $user_id):
            $user = User::getUserById($user_id);
            // send mail
            $link = BASE_URL . '/tdc?id=' . $mission_id;
            $mail_body = 'Bonjour ' . mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName'])
                . '<br/> Une nouvelle mission vous a été assignée.'
                . '<br/> Visitez le lien <a href="' . $link . '">' . $link . '</a>'
                . '<br/>' . $link;

            Mail::sendMail('Nouvelle Mission', $user['email'], $mail_body);
            
        endforeach;
        
    }
    
    public static function mailUser($mission_id, $user_id) {
      
        $user = User::getUserById($user_id);
        // send mail
        $link = BASE_URL . '/tdc?id=' . $mission_id;
        $mail_body = 'Bonjour ' . mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName'])
            . '<br/> Une nouvelle mission vous a été assignée.'
            . '<br/> Visitez le lien <a href="' . $link . '">' . $link . '</a>'
            . '<br/>' . $link;

        Mail::sendMail('Nouvelle Mission', $user['email'], $mail_body);
            
    }
    
    public static function mailUserDeassigned($mission_id, $user_id) {
      
        $user = User::getUserById($user_id);
        
        // send mail
        $link = BASE_URL . '/tdc?id=' . $mission_id;
        $mail_body = 'Bonjour ' . mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName'])
            . '<br/> Vous avez été désaffecté de la mission ' . $mission_id
            . '<br/> Visitez le lien <a href="' . $link . '">' . $link . '</a>'
            . '<br/>' . $link;

        Mail::sendMail('Mission ' . $mission_id . ' désaffectée', $user['email'], $mail_body);
            
    }
    
    public static function addOutCategory ($mission_id, $out_category) {
        global $con;
        
        if ($out_category != "") {
            $categories = explode('|', $out_category);
        
            foreach($categories as $category):   
                $con->insert('missionsoutcategories', array('mission_id' => $mission_id, 'out_categorie' => $category));
            
                $id = $con->lastInsertId();
        
                //watchdog
                $log  = array(
                    'module_name' => 'Mission',
                    'module_code' => 'mod_mission',
                    'action' => 'Ajouter mission out categorie',
                    'action_code' => 'mod_mission_create_mission_outcategory',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'missionsoutcategories',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode(array('mission_id' => $mission_id, 'out_categorie' => $category)),
                );
                Watchdog::Log($log);
            
            endforeach;
        }
        return true;
    }
    
    public static function addCriteres ($mission_id, $criteres) {
        global $con;
        
        if ($criteres != "") {
            $criteres_specifics = explode('|', $criteres);
               
            $count = 1;
            foreach($criteres_specifics as $critere):   
                
                $con->insert('missionscriteres', array('mission_id' => $mission_id, 'ordre' => $count, 'criteres' => $critere));
            
                $id = $con->lastInsertId();
        
                //watchdog
                $log  = array(
                    'module_name' => 'Mission',
                    'module_code' => 'mod_mission',
                    'action' => 'Ajouter mission criteres',
                    'action_code' => 'mod_mission_create_mission_critere',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'missionscriteres',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode(array('mission_id' => $mission_id, 'ordre' => $count, 'criteres' => $critere)),
                );
                Watchdog::Log($log);
            
                $count++;
                
            endforeach;
        }
        return true;
    }
    
    public static function EditAssignedUsers ($mission_id, $assignedUsers) {
        global $con;
        
        // 3 possibilities
        // 1. add new user
        // 2. existing user
        // 3. deleted user
        // we get all existing user from db => before modif
        $existingUsers = self::getAssignedUsers($mission_id);
        $user_ids = explode('|', $assignedUsers);
        foreach($user_ids as $user_id):
            // check if the user id already existed
            if (in_array($user_id, $existingUsers)) {
                // do nothing
            } else { // user did not exist
                $con->insert('missionsusers', array('mission_id' => $mission_id, 'user_id' => $user_id));
                
                $id = $con->lastInsertId();
        
                //watchdog
                $log  = array(
                    'module_name' => 'Mission',
                    'module_code' => 'mod_mission',
                    'action' => 'Ajouter mission utilisateurs',
                    'action_code' => 'mod_mission_create_mission_user',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'missionsusers',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode(array('mission_id' => $mission_id, 'user_id' => $user_id)),
                );
                Watchdog::Log($log);
                
                // mail the user to notify
                self::mailUser($mission_id, $user_id);
            }                       
        endforeach;
        
        // check if the user existed and now does not exist
        // the user has been deassigned to the mission
        foreach ($existingUsers as $user_id):
            
            if (!in_array($user_id, $user_ids)) {
                $con->delete('missionsusers', array('mission_id' => $mission_id, 'user_id' => $user_id));
                        
                //watchdog
                $log  = array(
                    'module_name' => 'Mission',
                    'module_code' => 'mod_mission',
                    'action' => 'Supprimer mission utilisateurs',
                    'action_code' => 'mod_mission_delete_mission_user',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'missionsusers',
                    'content_type' => 'json',
                    'content_before' => json_encode(array('mission_id' => $mission_id, 'user_id' => $user_id)),
                );
                Watchdog::Log($log);
                
                self::mailUserDeassigned($mission_id, $user_id);
            }
            
        endforeach;
        
        return true;
    }
    
    public static function addAssignedUsers ($mission_id, $assignedUsers) {
        global $con;
        
        $user_ids = explode('|', $assignedUsers);
        
        foreach($user_ids as $user_id):   
            
            $con->insert('missionsusers', array('mission_id' => $mission_id, 'user_id' => $user_id));
        
            $id = $con->lastInsertId();
        
            //watchdog
            $log  = array(
                'module_name' => 'Mission',
                'module_code' => 'mod_mission',
                'action' => 'Ajouter mission utilisateurs',
                'action_code' => 'mod_mission_create_mission_user',
                'table_parent_name' => 'missions',
                'table_parent_id' => $mission_id,
                'table_name' => 'missionsusers',
                'table_id' => $id,
                'content_type' => 'json',
                'content_before' => json_encode(array('mission_id' => $mission_id, 'user_id' => $user_id)),
            );
            Watchdog::Log($log);
        
        endforeach;
        
        return true;
    }
    
    public static function addComment($mission_id, $commentaire) {
        global $con;
        
        $con->insert('missionscommentaires', array('mission_id' => $mission_id, 'user_id' => $_SESSION['opsearch_user']['id'], 'comment' => $commentaire, 'created' => time()));
        
        $id = $con->lastInsertId();
        
        //watchdog
        $log  = array(
            'module_name' => 'Mission',
            'module_code' => 'mod_mission',
            'action' => 'Ajouter mission commentaire',
            'action_code' => 'mod_mission_create_mission_comment',
            'table_parent_name' => 'missions',
            'table_parent_id' => $mission_id,
            'table_name' => 'missionscommentaires',
            'table_id' => $id,
            'content_type' => 'json',
            'content_before' => json_encode(array('mission_id' => $mission_id, 'user_id' => $_SESSION['opsearch_user']['id'], 'comment' => $commentaire, 'created' => time())),
        );
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function addDocuments($mission_id, $files) {
        global $con;
        $numFiles = count($files['name']);
        
        if ($numFiles > 0 && $files['name'][0] != "") {
            // check if the candidat folder exist
            $mission_dir = MISSION_PATH . '/' . $mission_id;

            // check if candidat folder exist
            // if not create it
            if (!file_exists($mission_dir)) {
                mkdir($mission_dir); // default chmod is 0777
            }

            foreach($files['name'] as $index => $filename):   
                $file_renamed = $mission_id . '_' . date('dmY', time()) . '_' .$filename;
                move_uploaded_file($files['tmp_name'][$index], $mission_dir . '/' . $file_renamed); // we append mission_id_before
                $con->insert('missionsdocuments', array('mission_id' => $mission_id, 'file' => $file_renamed, 'created' => time()));
                
                $id = $con->lastInsertId();
        
                //watchdog
                $log  = array(
                    'module_name' => 'Mission',
                    'module_code' => 'mod_mission',
                    'action' => 'Ajouter mission document',
                    'action_code' => 'mod_mission_create_mission_document',
                    'table_parent_name' => 'missions',
                    'table_parent_id' => $mission_id,
                    'table_name' => 'missionsdocuments',
                    'table_id' => $id,
                    'content_type' => 'json',
                    'content_before' => json_encode(array('mission_id' => $mission_id, 'file' => $file_renamed, 'created' => time())),
                );
                Watchdog::Log($log);
                
            endforeach;
        }        
        return true;    
        
    }
    
    public static function getMissionById($id) {
        global $con;
        $sql = "SELECT * FROM missions WHERE id = {$id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function isMissionBlank($id) {
        global $con;
        $sql = "SELECT count(*) as num_can_ent FROM tdc WHERE mission_id = {$id} AND status = 1";
        $num = $con->fetchAssoc($sql);
        if ($num['num_can_ent'] > 0) {
            return false;
        } else {
            return true;
        }
    }
    
    public static function checkMissionExist($id) {
        global $con;
        $sql = "SELECT * FROM missions WHERE id = {$id}";
        $result = $con->fetchAssoc($sql);
        if (empty($result)) {
            return false;
        } else {
            return true;
        }
    }
    
    public static function getMissionDetailsById($id) {
        global $con;
        $sql = "SELECT m.*, c.nom as nom_client "
            . "FROM missions m, clients c "
            . "WHERE m.id = {$id} AND c.id = m.client_id";
        return $con->fetchAssoc($sql);
    }

    public static function getCommentsByMissionId($mission_id) {
        global $con;
        $sql = "SELECT * FROM missionscommentaires WHERE mission_id = {$mission_id} ORDER By id ASC";
        return $con->fetchAll($sql);
    }
    
    public static function getComments($mission_id) {
        global $con;
        $sql = "SELECT M.id, M.mission_id, M.comment, M.created, M.modified, U.firstName, U.lastName "
            . "FROM missionscommentaires M, users U "
            . "WHERE M.status = 1 AND U.id = M.user_id AND M.mission_id = {$mission_id} "
            . "ORDER BY M.id DESC";
        return $con->fetchAll($sql);
    }
    
    public static function getDocuments($mission_id) {
        global $con;
        $sql = "SELECT * FROM missionsdocuments WHERE mission_id = {$mission_id} AND status = 1";
        return $con->fetchAll($sql);
    }
    
    public static function getAssignee($mission_id) {        
        global $con;
        $sql = "SELECT * FROM missionsusers WHERE mission_id = {$mission_id}";
        return $con->fetchAll($sql);
    }
    
    public static function getPrestataires() {
        global $con;
        $sql = "SELECT * FROM prestataires WHERE status = 1 ORDER BY id ASC";
        return $con->fetchAll($sql);
    }
    
    public static function getAssignedUsers($mission_id) {        
        global $con;
        $sql = "SELECT user_id FROM missionsusers WHERE mission_id = {$mission_id}";
        $results = $con->fetchAll($sql);
        
        $user_ids = array();
        foreach ($results as $result):
            $user_ids[] = $result['user_id'];
        endforeach;
        
        return $user_ids;
    }
    
    public static function getAssignedUsersDetails($mission_id) {
        global $con;
        $sql = "SELECT u.firstName, u.lastName "
                . "FROM missionsusers m, users u "
                . "WHERE m.user_id = u.id AND m.mission_id = {$mission_id} "
                . "ORDER BY u.firstName";
        
        $results = $con->fetchAll($sql);
        
        $users = array();
        foreach($results as $result):
            $users[] = mb_ucfirst($result['firstName']) . ' ' . mb_strtoupper($result['lastName']);
        endforeach;
        
        return $users;
    }
    
    public static function getAssignedUsersDetailsMore($mission_id) {
        global $con;
        $sql = "SELECT u.id, u.firstName, u.lastName "
                . "FROM missionsusers m, users u "
                . "WHERE m.user_id = u.id AND m.mission_id = {$mission_id} "
                . "ORDER BY u.firstName";
        
        $results = $con->fetchAll($sql);
        
        $users = array();
        foreach($results as $result):
            $users[] = array(
                'id' => $result['id'], 
                'name' => mb_ucfirst($result['firstName']) . ' ' . mb_strtoupper($result['lastName'])
            );
        endforeach;
        
        return $users;
    }
    
    public static function getCriteres($mission_id) {
        global $con;
        $sql = "SELECT * FROM missionscriteres WHERE mission_id = {$mission_id} ORDER BY ordre ASC";
        return $con->fetchAll($sql);        
    }
    
    public static function getCriteresIdList ($mission_id) {
        global $con;
        $sql = "SELECT id FROM missionscriteres WHERE mission_id = {$mission_id} ORDER BY ordre ASC";
        $results = $con->fetchAll($sql);
        $ids = array();
        foreach($results as $result):
            $ids[] = $result['id'];
        endforeach;
        return $ids;
    }
    
    public static function getCriteresIdListByCandidate ($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT critere_id FROM tdccriteres WHERE mission_id = {$mission_id} AND candidat_id = {$candidat_id} ORDER BY id ASC";
        $results = $con->fetchAll($sql);
        $ids = array();
        foreach($results as $result):
            $ids[] = $result['critere_id'];
        endforeach;
        return $ids;
    }
    
    public static function getOutCategoriesIdListByCandidate ($mission_id, $candidat_id) {
        global $con;
        $sql = "SELECT outcategory_id FROM tdcoutcategories WHERE mission_id = {$mission_id} AND candidat_id = {$candidat_id} ORDER BY id ASC";
        $results = $con->fetchAll($sql);
        $ids = array();
        foreach($results as $result):
            $ids[] = $result['outcategory_id'];
        endforeach;
        return $ids;
    }
    
    public static function getCriteresById($critere_id) {
        global $con;
        $sql = "SELECT * FROM missionscriteres WHERE id = {$critere_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function getOutCategory($mission_id) {
        global $con;
        $sql = "SELECT * FROM missionsoutcategories WHERE mission_id = {$mission_id}";
        return $con->fetchAll($sql); 
    }
    
    public static function getOutCategoryIdList ($mission_id) {
        global $con;
        $sql = "SELECT * FROM missionsoutcategories WHERE mission_id = {$mission_id}";
        $results = $con->fetchAll($sql);
        $ids = array();
        foreach($results as $result):
            $ids[] = $result['id'];
        endforeach;
        return $ids;
    }
    
    public static function getOutCategoryById($category_id) {
        global $con;
        $sql = "SELECT * FROM missionsoutcategories WHERE id = {$category_id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function deleteComment($id) {
        global $con;
        
        $comment_before = self::getCommentId($id);
        
        $con->delete('missionscommentaires', array('id' => $id));
        
        //watchdog
        $log  = array(
            'module_name' => 'Mission',
            'module_code' => 'mod_mission',
            'action' => 'Supprimer mission commentaire',
            'action_code' => 'mod_mission_delete_mission_comment',
            'table_parent_name' => 'missions',
            'table_parent_id' => $comment_before['mission_id'],
            'table_name' => 'missionscommentaires',
            'table_id' => $id,
            'content_type' => 'json',
            'content_before' => json_encode($comment_before),
        );
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function getCommentId($id) {
        global $con;
        $sql = "SELECT * FROM missionscommentaires WHERE id = {$id}";
        return $con->fetchAssoc($sql);
    }
    
    public static function editComment($comment) {
        global $con;
        
        $comment_before = self::getCommentId($comment['id']);
        
        $con->update('missionscommentaires', $comment, array('id' => $comment['id']));
               
        //watchdog
        $log  = array(
            'module_name' => 'Mission',
            'module_code' => 'mod_mission',
            'action' => 'Éditer mission commentaire',
            'action_code' => 'mod_mission_edit_mission_comment',
            'table_parent_name' => 'missions',
            'table_parent_id' => $comment_before['mission_id'],
            'table_name' => 'missionscommentaires',
            'table_id' => $comment['id'],
            'content_type' => 'json',
            'content_before' => json_encode($comment_before),
            'content_after' => json_encode($comment),
        );
        Watchdog::Log($log);
        
        return true;
    }
    
    public static function getDocumentById($id) {
        global $con;
        $sql = "SELECT * FROM missionsdocuments WHERE id = {$id}";
        $doc = $con->fetchAssoc($sql);
        return array(
            'path' => MISSION_PATH. '/' . $doc['mission_id'] . '/' . $doc['file'],
            'content_type' => getContentType(getFileExt($doc['file'])),
        );
    }
    
    public static function deleteDocument($id) {
        global $con;
        
        // first we get the document name and candidat it
        // delete the file pysically and then delete record in db
        $sql = "SELECT * FROM missionsdocuments WHERE id = {$id}";
        $doc = $con->fetchAssoc($sql);
        if (unlink(MISSION_PATH . '/' . $doc['mission_id'] . '/' . $doc['file'])) {
            $con->delete('missionsdocuments', array('id' => $id));
                    
            //watchdog
            $log  = array(
                'module_name' => 'Mission',
                'module_code' => 'mod_mission',
                'action' => 'Supprimer mission document',
                'action_code' => 'mod_mission_delete_mission_document',
                'table_parent_name' => 'missions',
                'table_parent_id' => $doc['mission_id'],
                'table_name' => 'missionsdocuments',
                'table_id' => $id,
                'content_type' => 'json',
                'content_before' => json_encode($doc),
            );
            Watchdog::Log($log);
            
            return true;
        } else {
            return false;
        }
        
    }
    
    public static function getMyMissionList($user_id, $all = false) {
        global $con;
        $sql = "SELECT m.*, u.user_id, c.nom as client, x.firstName, x.lastName, p.nom as prestataire "
                . "FROM missions m, missionsusers u, clients c, users x, prestataires p "
                . "WHERE m.id = u.mission_id AND u.user_id = {$user_id} AND m.status <> 235 "
                . "AND c.id = m.client_id AND x.id = m.consultant_id AND p.id = m.prestataire_id "
                    . "ORDER BY m.id DESC LIMIT 200";
                
        if ($all && User::can('read_all_mission')) {
            $sql = "SELECT m.*, c.nom as client, x.firstName, x.lastName, p.nom as prestataire "
                . "FROM missions m, clients c, users x, prestataires p "
                . "WHERE c.id = m.client_id AND x.id = m.consultant_id AND p.id = m.prestataire_id AND m.status <> 235 "
                . "ORDER BY m.id DESC LIMIT 200";
        }
                
        return $con->fetchAll($sql); 
    }
    
    public static function getMyMissionListEnCours($user_id) {
        global $con;
        /*$sql = "SELECT m.*, u.user_id, c.nom as client, x.firstName, x.lastName, p.nom as prestataire "
                . "FROM missions m, missionsusers u, clients c, users x, prestataires p "
                . "WHERE m.id = u.mission_id AND u.user_id = {$user_id} AND m.status = 232 "
                . "AND c.id = m.client_id AND x.id = m.consultant_id AND p.id = m.prestataire_id";*/
        
        $sql = "SELECT M.*, MU.user_id, C.nom as client, CONCAT_WS(' ',CONSULTANT.firstName, CONSULTANT.lastName) as consultant, P.nom as prestataire
FROM missions M
LEFT JOIN missionsusers MU ON MU.mission_id = M.id
LEFT JOIN clients C ON C.id = M.client_id
LEFT JOIN users CONSULTANT ON CONSULTANT.id = M.consultant_id
LEFT JOIN prestataires P ON P.id = M.prestataire_id
WHERE (MU.user_id = {$user_id} OR M.manager_id = {$user_id} OR M.consultant_id = {$user_id}) AND M.status = 232
GROUP BY M.id";
                        
        return $con->fetchAll($sql); 
    }
    
    public static function getMissionByCDRId($cdr_id, $start_date, $end_date) {
        global $con;
        
        if ($start_date <> "") {
            $start = strtotime($start_date . " 00:00:01");
            $query_start = " AND m.date_debut >= {$start} ";
        } else {
            $query_start = " ";
        }
        
        if ($end_date <> "") {
            $end = strtotime($end_date . " 23:59:59");
            $query_end = " AND m.date_fin <= {$end} ";
        } else {
            $query_end = " ";
        }
         
        $sql = "SELECT m.id, m.poste, m.manager_id, m.manager_name, m.status, m.date_debut, m.date_fin "
            . "FROM missions m "
            . "LEFT JOIN missionsusers mu ON mu.mission_id = m.id "
            . "WHERE mu.user_id = {$cdr_id} " . $query_start . $query_end
            . "ORDER BY m.poste ASC";   
            
        return $con->fetchAll($sql); 
    }
    
    public static function getMissionByCDRIdKPI($cdr_id, $start_date, $end_date) {
        global $con;
        
        if ($start_date <> "") {
            $start = strtotime($start_date . " 00:00:01");
            $query_start = " AND m.date_debut >= {$start} ";
        } else {
            $query_start = " ";
        }
        
        if ($end_date <> "") {
            $end = strtotime($end_date . " 23:59:59");
            $query_end = " AND m.date_fin <= {$end} ";
        } else {
            $query_end = " ";
        }
         
        $sql = "SELECT m.id, m.poste, m.manager_id, m.manager_name, m.status, m.date_debut, m.date_fin "
            . "FROM missions m "
            . "LEFT JOIN missionsusers mu ON mu.mission_id = m.id "
            . "WHERE m.include_in_kpi = 1 AND mu.user_id = {$cdr_id} " . $query_start . $query_end
            . "ORDER BY m.poste ASC";   
            
        return $con->fetchAll($sql); 
    }
    
    public static function getMissionByManagerId($manager_id, $start_date, $end_date) {
        global $con;
        
        if ($start_date <> "") {
            $start = strtotime($start_date . " 00:00:01");
            $query_start = " AND date_debut >= {$start} ";
        } else {
            $query_start = " ";
        }
        
        if ($end_date <> "") {
            $end = strtotime($end_date . " 23:59:59");
            $query_end = " AND date_fin <= {$end} ";
        } else {
            $query_end = " ";
        }
         
        $sql = "SELECT m.id, m.poste, m.manager_id, m.manager_name, m.status, m.date_debut, m.date_fin "
            . "FROM missions m "
            . "WHERE m.manager_id = {$manager_id} " . $query_start . $query_end
            . "ORDER BY m.poste ASC";   
            
        return $con->fetchAll($sql); 
    }
    
    public static function getMissionByManagerIdKPI($manager_id, $start_date, $end_date) {
        global $con;
        
        if ($start_date <> "") {
            $start = strtotime($start_date . " 00:00:01");
            $query_start = " AND date_debut >= {$start} ";
        } else {
            $query_start = " ";
        }
        
        if ($end_date <> "") {
            $end = strtotime($end_date . " 23:59:59");
            $query_end = " AND date_fin <= {$end} ";
        } else {
            $query_end = " ";
        }
         
        $sql = "SELECT m.id, m.poste, m.manager_id, m.manager_name, m.status, m.date_debut, m.date_fin "
            . "FROM missions m "
            . "WHERE m.include_in_kpi = 1 AND m.manager_id = {$manager_id} " . $query_start . $query_end
            . "ORDER BY m.poste ASC";   
            
        return $con->fetchAll($sql); 
    }
    
    public static function getMissionByClientId($client_id, $start_date, $end_date) {
        global $con;
        
        if ($start_date <> "") {
            $start = strtotime($start_date . " 00:00:01");
            $query_start = " AND date_debut >= {$start} ";
        } else {
            $query_start = " ";
        }
        
        if ($end_date <> "") {
            $end = strtotime($end_date . " 23:59:59");
            $query_end = " AND date_fin <= {$end} ";
        } else {
            $query_end = " ";
        }
         
        $sql = "SELECT m.id, m.poste, m.manager_id, m.manager_name, m.status, m.date_debut, m.date_fin "
            . "FROM missions m "
            . "WHERE m.client_id = {$client_id} " . $query_start . $query_end
            . "ORDER BY m.poste ASC";   
            
        return $con->fetchAll($sql); 
    }
    
    public static function getMissionByClientIdKPI($client_id, $start_date, $end_date) {
        global $con;
        
        if ($start_date <> "") {
            $start = strtotime($start_date . " 00:00:01");
            $query_start = " AND date_debut >= {$start} ";
        } else {
            $query_start = " ";
        }
        
        if ($end_date <> "") {
            $end = strtotime($end_date . " 23:59:59");
            $query_end = " AND date_fin <= {$end} ";
        } else {
            $query_end = " ";
        }
         
        $sql = "SELECT m.id, m.poste, m.manager_id, m.manager_name, m.status, m.date_debut, m.date_fin "
            . "FROM missions m "
            . "WHERE m.include_in_kpi = 1 AND m.client_id = {$client_id} " . $query_start . $query_end
            . "ORDER BY m.poste ASC";   
            
        return $con->fetchAll($sql); 
    }
    
    public static function getMissionsByClientId($client_id) {
        global $con;
                 
        $sql = "SELECT m.id, m.poste, m.manager_id, m.manager_name, m.status, m.date_debut, m.date_fin "
            . "FROM missions m "
            . "WHERE m.client_id = {$client_id} "
            . "ORDER BY m.poste ASC";   
            
        return $con->fetchAll($sql); 
    }
    
    public static function getMissionsByAssignedClientUserId($user_id) {
        global $con;
        
        $sql = "SELECT M.id, M.manager_name, M.poste, M.`status`, M.date_debut, M.date_fin FROM missions M LEFT JOIN usersclientmissions U ON U.mission_id = M.id WHERE U.user_id = {$user_id}";
        
        return $con->fetchAll($sql); 
    }
    
    public static function checkIfClientIsAssigned($user_id, $mission_id) {
        global $con;
        
        $sql = "SELECT * FROM usersclientmissions WHERE user_id = {$user_id} AND mission_id = {$mission_id}";
        
        $result = $con->fetchAssoc($sql);
        
        if (empty($result)) {
            return false;
        } else {
            return true;
        }
    }
    
    public static function getMissionsDetailsByAssignedClientUserId($user_id) {
        global $con;
        
        $sql = "SELECT M.*, U.firstName as consultant_fname, U.lastName as consultant_lname, P.nom as prestataire
FROM missions M
LEFT JOIN users U ON U.id = M.consultant_id
LEFT JOIN prestataires P ON P.id = M.prestataire_id
LEFT JOIN usersclientmissions UA ON UA.mission_id = M.id
WHERE UA.user_id = {$user_id} ORDER BY M.id DESC";
        
        return $con->fetchAll($sql); 
    }
    
    public static function getAssignedMissions($uid) {        
        global $con;
        $sql = "SELECT mission_id FROM usersclientmissions WHERE user_id = {$uid}";
        $results = $con->fetchAll($sql);
        
        $mission_ids = array();
        foreach ($results as $result):
            $mission_ids[] = $result['mission_id'];
        endforeach;
        
        return $mission_ids;
    }
    
    public static function getAssignedClientToMissions($mission_id) {        
        global $con;
        $sql = "SELECT U.* FROM usersclientmissions M LEFT JOIN users U ON U.id = M.user_id WHERE M.mission_id = {$mission_id}";
        return $con->fetchAll($sql);
    }
    
    public static function getCdrByMissionId($mission_id) {
        global $con;
        $sql = "SELECT U.id, U.firstName, U.lastName, U.status, R.name as role_name FROM missionsusers MU LEFT JOIN users U ON U.id = MU.user_id LEFT JOIN roles R ON R.id = U.role_id WHERE MU.mission_id = {$mission_id}";
        return $con->fetchAll($sql);  
    }
    
    public static function getManagerByMissionId($mission_id) {
        global $con;
        $sql = "SELECT U.*, R.name as role_name, M.mail_notif as mission_mail_notif FROM missions M LEFT JOIN users U ON U.id = M.manager_id LEFT JOIN roles R ON R.id = U.role_id WHERE M.id = {$mission_id}";
        return $con->fetchAssoc($sql);  
    }
    
    public static function isMissionManager($mission_id, $manager_id) {
        
        global $con;
        
        $sql = "SELECT * FROM missions WHERE id = {$mission_id} AND manager_id = {$manager_id}";
        
        $result = $con->fetchAssoc($sql);
        
        return (!empty($result));
        
    }
        
}
