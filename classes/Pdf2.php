<?php
require_once __DIR__ . '/../libraries/fpdf181/fpdf.php';

class Pdf2 extends FPDF {
    
    // Page header
    function Header() {
        
        // draw the header rectangle
        $this->SetFillColor(238, 236, 236);
		$this->Rect(0, 0, 210, 26, "F");

        // logo
        $this->Image(__DIR__ . '/../asset/images/logo.png', 75, 5);
        
        // title
        $this->SetFont('Helvetica', '', 12);
        $this->setTextColor(102, 102, 102);
        $this->Text(110, 16, 'OPSEARCH');
        
        //slogan
        $this->SetFont('Helvetica', 'B', 6);
        $this->Text(75, 22, 'RECRUTEMENT PAR APPROCHE DIRECTE - INTELLIGENCE RH');
        
        // line
        $this->SetLineWidth(.3);
        $this->SetDrawColor(102, 102, 102);
        $this->Line(0, 26, 210, 26);
        
		$this->SetXY(20, 33);
        $this->SetMargins(20, 20, 20);
    }
    
    // Page footer
    function Footer() {
        
        // Position at 1.5 cm from bottom
        $this->SetY(-10);
        
        // line
        $this->SetLineWidth(.3);
        $this->SetDrawColor(102, 102, 102);
        $this->Line(0, 287, 210, 287);
        
        // rectangle
        $this->SetFillColor(238, 236, 236);
		$this->Rect(0, 287, 210, 10, "F");
        
        // Arial italic 8
        $this->SetFont('Arial', 'I', 7);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }
        
    function setValue ($str, $i) {
        
        if ($i%2 == 0) {
            $this->SetFillColor(238, 229, 220);
        } else {
            $this->SetFillColor(240, 239, 239);
        }
        
        $this->SetDrawColor(20, 15, 10);
        $this->SetLineWidth(1);
        
        $this->SetTextColor(58, 56, 53);
        $this->SetFont('Helvetica', '', 12);
        $this->Cell(110, 8, $str, 0, 1, 'L', true);
    }
    
    function setLabel ($str, $i) {
        
        if ($i%2 == 0) {
            $this->SetFillColor(238, 229, 220);
        } else {
            $this->SetFillColor(240, 239, 239);
        }
        
        
        $this->SetDrawColor(20, 15, 10);
        $this->SetLineWidth(1);
        
        $this->SetTextColor(189, 156, 125);
        $this->SetFont('Helvetica', 'B', 12);
        $this->Cell(60, 8, $str, 0, 0, 'L', true);
    }
    
}