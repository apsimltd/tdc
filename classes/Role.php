<?php

/**
 * Description of Role
 *
 */
class Role {

    /**
     * 
     * @global Res $con
     * @param String $name
     * @return int
     */
    public static function getRoleIdByName($name) {
        global $con;
        $sql = "SELECT id FROM roles WHERE name='{$name}'";
        $result = $con->fetchAll($sql);
        if (empty($result)) {
            return false;
        } else {
            return $result[0]['id'];
        }
    }

    /**
     * 
     * @global Res $con
     * @param int $id
     * @return String
     */
    public static function getRoleNameById($id) {
        global $con;
        $sql = "SELECT name FROM roles WHERE id={$id}";
        $result = $con->fetchAll($sql);
        if (empty($result)) {
            return false;
        } else {
            return $result[0]['name'];
        }
    }

    /**
     * 
     * @global Res $con
     * @return array
     */
    public static function getRoles() {
        global $con;
        $sql = "SELECT * FROM roles WHERE id > 0 ORDER BY id ASC";
        return $con->fetchAll($sql);
    }
    
    public static function getById($role_id) {
        global $con;
        $sql = "SELECT * FROM roles WHERE id = {$role_id}";
        return $con->fetchAssoc($sql);
    }
    
    /**
     * 
     * @global Res $con
     * @param array $role
     * @return booelean
     */
    public static function addRole($role) {
        global $con;
        
        $con->insert('roles', $role);
        $id = $con->lastInsertId();
        
        //watchdog
        $log  = array(
            'module_name' => 'Rôle',
            'module_code' => 'mod_role',
            'action' => 'Ajouter Rôle',
            'action_code' => 'mod_role_create_role',
            'table_name' => 'roles',
            'table_id' => $id,
        );
        Watchdog::Log($log);
        
        return true;
    }

    /**
     * 
     * @global Res $con
     * @param int $role_id
     * @return boolean
     */
    public static function deleteRole($role_id) {
        global $con;
        
        if ($role_id <= 4)
            return false;
        
        $role = self::getById($role_id);
        
        // delete role
        $con->delete('roles', array('id' => $role_id));
        // now delete all rolesactions bind to it
        $con->delete('rolesactions', array('role_id' => $role_id));
                
        //watchdog
        $log  = array(
            'module_name' => 'Rôle',
            'module_code' => 'mod_role',
            'action' => 'Supprimer Rôle',
            'action_code' => 'mod_role_delete_role',
            'table_name' => 'roles',
            'table_id' => $role_id,
            'content_type' => 'json',
            'content_before' => json_encode($role),
        );
        Watchdog::Log($log);
        
        return true;
    }

    /**
     * 
     * @global Res $con
     * @param int $role_id
     * @param String $new_name
     * @return boolean
     */
    public static function updateRole($role_id, $new_name) {
        global $con;

        if ($role_id == 0)
            return false;

        $con->update('roles', array('name' => $new_name), array('id' => $role_id));

        return true;
    }

    /**
     * <p> check if any user has this role</p>
     * @global Res $con
     * @param int $role_id
     * @return boolean
     */
    public static function isAnyUserHaveRole($role_id) {
        global $con;
        $sql = "SELECT role_id FROM users WHERE role_id = {$role_id}";
        $result = $con->fetchAll($sql);
        return (!empty($result));
    }

    /**
     * 
     * @global Res $con
     * @param String $role_name
     * @return boolean
     */
    public static function isRoleNameExist($role_name) {
        global $con;
        $sql = "SELECT name FROM roles WHERE name = '{$role_name}'";
        $result = $con->fetchAll($sql);
        return (!empty($result));
    }

}
