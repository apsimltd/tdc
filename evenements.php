<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Événements" ?>
<?php $page = "events"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('add_event', 'view_event', 'edit_event', 'delete_event'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>

	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base">

        <div id="contentWrap">
        
			<section class="title block">
				<h1>Événements</h1>					
			</section>
			
            <div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><span class="glyphicon glyphicon-calendar"></span>Événements</header>
                        </div>
						
						<div class="block-body">
							
                            <div class="calendar_nav clearfix">
                                
                                <?php if (User::can('view_event')): ?>
                                <div class="CalendarNav btn-group pull-left">
                                	
                                    <button class="btn btn-primary pull-left tooltips" title="Précédent" data-calendar-nav="prev"><span class="glyphicon glyphicon-chevron-left"></span></button>
                                    <button class="btn btn-info pull-left" data-calendar-nav="today">Aujourd'hui</button>
                                    <button class="btn btn-primary pull-left tooltips" title="Suivant" data-calendar-nav="next"><span class="glyphicon glyphicon-chevron-right"></span></button>
                                    
                                </div><!-- /CalendarNav -->
                                <?php endif; ?>
                                
                                <div class="CalendarViews btn-group pull-right">
                                	
                                    <?php if (User::can('view_event')): ?>
                                    <button class="btn btn-warning pull-left active" data-calendar-view="month">Mois</button>
                                    <button class="btn btn-warning pull-left" data-calendar-view="week">Semaine</button>
                                    <button class="btn btn-warning pull-left" data-calendar-view="day">Jour</button>
                                    <?php endif; ?>
                                    <?php if (User::can('add_event')): ?>
									<button class="add_event btn btn-icon btn-primary pull-left tooltips" title="Ajouter événement"><span class="glyphicon glyphicon-plus"></span></button>
                                    <?php endif; ?>
                                    
                                </div><!-- / CalendarViews -->
                                
                                <div class="MonthName"></div>
                            
                            </div><!-- /calendar_nav -->
                            
                            <div class="calendar_wrap clearfix">
                            
                            	<div id="calendar" class="clearfix pull-left"></div><!-- /calendar -->
                                
                                <div class="calendar_legend clearfix pull-left">
                                
                                	<h4>Légende</h4>
                                    
                                    <ul class="event_legend">
                                    	<li class="linkedIn1">LinkedIn 1</li>
                                        <li class="linkedIn2">LinkedIn 2</li>
                                        <li class="cvaden">CV Aden <span class="glyphicon glyphicon-info-sign tooltips" title="Maximum 2 connexions en même temps"></span></li>
                                        <li class="rdvp">RDVP <span class="glyphicon glyphicon-info-sign tooltips" title="Rendez-Vous Personnel"></span></li>
                                    </ul>
                                    
                                    <ul class="event_legend">
                                        <li class="linkedIn1"><span class="time">13:30 - 14:30</span> <span class="glyphicon glyphicon-info-sign tooltips" title="L'événement en vert gras est le vôtre."></span></li>
                                        <li class="linkedIn2"><span class="time">13:30 - 14:30</span> <span class="glyphicon glyphicon-info-sign tooltips" title="L'événement en vert gras est le vôtre."></span></li>
                                        <li class="cvaden"><span class="time">13:30 - 14:30</span> <span class="glyphicon glyphicon-info-sign tooltips" title="L'événement en vert gras est le vôtre."></span></li>
                                    </ul>
                                
                                </div><!-- /calendar_legend -->
                                
                            </div><!-- /calendar_wrap -->
                                                   
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
        
        </div><!-- /contentWrap -->
        
    </div><!-- /base -->
	
	<?php require_once './views/debug.php'; ?>
    
</body>
</html>
<script>
$(document).ready(function(){
	    
    <?php if (User::can('edit_event')): ?>
    $(document).on('click', 'div.day-highlight.triggerEvent', function(){
        if ($(this).hasClass('cal-cell')) { // month_view hack
            return false;
        }
        var event_id = $(this).attr('data-event-id');
        console.log($(this));
        var lock = 'false';
        if ($(this).hasClass('completed')) {
            lock = 'true';
        }
        Modal.Show(AJAX_UI + '/edit-event?id='+event_id+'&lock='+lock, null, null, '<span class="glyphicon glyphicon-calendar"></span>', 50);
	});
    // month_view
    $(document).on('click', 'a.month_view.triggerEvent', function(){
        var event_id = $(this).attr('data-event-id');
        var lock = 'false';
        if ($(this).hasClass('completed')) {
            lock = 'true';
        }
        Modal.Show(AJAX_UI + '/edit-event?id='+event_id+'&lock='+lock, null, null, '<span class="glyphicon glyphicon-calendar"></span>', 50);
	});
    <?php endif; ?>
	
    <?php if (User::can('add_event')): ?>
	$(document).on('click', '.add_event', function(){
        Modal.Show(AJAX_UI + '/add-event', null, null, '<span class="glyphicon glyphicon-calendar"></span>', 50);
	});
    <?php endif; ?>
    
    <?php if (User::can('delete_event')): ?>
    $(document).on('click', '.delete_event', function(){
        var event_id = $(this).attr('data-event_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer événement', AJAX_HANDLER + '/delete-event?id='+event_id);
    });
    <?php endif; ?>
		
});

<?php if (User::can('add_event', 'edit_event')): ?>
function frm_create_event_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}
<?php endif; ?>
</script>
