<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Rechercher" ?>
<?php $page = "search"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can('search')) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>

	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base" class="Candidats">

        <div id="contentWrap">
                    
            <div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
						
						<div class="block-body inner_search clearfix">
							
                            <form class="frm_frm" name="frm_main_search" id="frm_main_search" action="<?php echo BASE_URL ?>/rechercher" method="get">
                                <input type="hidden" name="type" id="typeSearch">
								<input type="text" name="value" id="TopSearchInput" class="frm_text frm_TopSearch" placeholder="Rechercher" autocomplete="off">
								<span class="glyphicon glyphicon-search"></span>
								
							</form>
							
							<div class="SearchSelector">
                	
								<ul>
									<li data-search="fonction"><i class="os-icon os-icon-hierarchy-structure-2"></i>Fonction</li>
									<li data-search="poste"><span class="glyphicon glyphicon-user"></span>Poste</li>
									<li data-search="client"><i class="fa fa-users"></i>Client</li>
                                    <li data-search="secteur"><span class="glyphicon glyphicon-wrench"></span>Secteur</li>
<!--									<li data-search="search"><i class="os-icon os-icon-ui-37"></i>Rechercher</li>-->
								</ul>
							
							</div><!-- /SearchSelector -->
							                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
			
			<div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="os-icon os-icon-ui-37"></i>Résultat de la recherche</header>
                            <?php $status = Control::getControlListByType(9); ?>
							<span class="legende">
								<span class="title">Légende</span>
                                <?php foreach($status as $key => $value): ?>
                                <span class="status <?php echo getClassStatusMission($key) ?>"><?php echo $value ?></span>
                                <?php endforeach; ?>
							</span><!--/ legende -->
							<div class="block-head-btns pull-right">
                            	<button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page précédente" onClick="window.history.back();"><span class="glyphicon glyphicon-menu-left"></span></button>
                                <?php if (User::can('read_all_mission') && (get('type') || get('value'))): ?>
                                <a href="<?php BASE_URL ?>/rechercher<?php echo buildSearchQueryString() ?>"><button type="button" class="btn btn-icon btn-success pull-right tooltips" title="Visualiser tous les missions"><span class="glyphicon glyphicon-filter"></span></button></a>
                                <?php endif; ?>
                            </div>
                        </div>
						
						<div class="block-body search_results_loader">
							
							<?php if (get('type') || get('value') || get('filter')): ?>
                                <?php
                                if (get('filter') && get('filter') == "all") {
                                    $missions = Mission::search(get('type'), get('value'), true);
                                } else {
                                    $missions = Mission::search(get('type'), get('value'));
                                }
                                ?>
                                <?php $secteurs = Control::getControlListByType(2); ?>
                                <?php if (!empty($missions)): ?>
                                <table class="datable stripe hover missionList">
                                    <thead>
                                        <tr>
                                            <th>Ref</th>
                                            <th>Prestataire</th>
                                            <th>Client</th>
                                            <th>Poste</th>
                                            <th>Secteur</th>
                                            <th>Fonction</th>
                                            <th>Consultant</th>
                                            <th>Date de début</th>
                                            <th>Statut</th>
                                            <?php if (User::can('tdc')): ?><th>TDC</th><?php endif; ?>
                                            <?php if (User::can('edit_mission', 'duplicate_mission', 'delete_mission', 'read_mission')): ?><th>Actions</th><?php endif; ?>
                                            <th><i class="os-icon os-icon-user-male-circle2 tooltips" title="Assigné à"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($missions as $mission): ?>
                                        <tr <?php if (Mission::isMissionForMe($me['id'], $mission['id'])): ?>class="mine-mission <?php echo getClassStatusMission($mission['status']) ?>"<?php endif; ?>>
                                            <td align="center"><?php echo $mission['id'] ?></td>
                                            <td><?php echo $mission['prestataire'] ?></td>
                                            <td><?php echo $mission['client'] ?></td>
                                            <td><?php echo $mission['poste'] ?></td>
                                            <td>
                                                <?php 
                                                if ($mission['control_secteur_id'] > 0){
                                                    echo $secteurs[$mission['control_secteur_id']];
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $mission['fonction'] ?></td>
                                            <td><?php echo mb_ucfirst($mission['firstName']) . ' ' . mb_strtoupper($mission['lastName']) ?></td>
                                            <td align="center" data-order="<?php echo $mission['date_debut'] ?>">
                                                <?php if ($mission['date_debut'] > 0): ?>
                                                <span title="<?php echo $mission['date_debut'] ?>"><?php echo dateToFr($mission['date_debut']) ?></span>
                                                <?php else: ?>
                                                <span title="0"></span>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center">
                                                <span class="status <?php echo getClassStatusMission($mission['status']) ?>">
                                                    <?php echo $status[$mission['status']] ?>
                                                </span>
                                            </td>
                                            <?php if (User::can('tdc')): ?>
                                            <td align="center">
                                                <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $mission['id'] ?>" class="link_to_tdc">
                                                    <span class="glyphicon glyphicon-screenshot"></span>
                                                </a>
                                            </td>
                                            <?php endif; ?>
                                            <?php if (User::can('edit_mission', 'duplicate_mission', 'delete_mission', 'read_mission')): ?>
                                            <td class="actions_button">
                                                <?php if (User::can('edit_mission')): ?>
                                                <a href="<?php echo BASE_URL ?>/editer-mission?id=<?php echo $mission['id'] ?>" class="btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></a>
                                                <?php endif; ?>
                                                <?php if (User::can('duplicate_mission')): ?>
                                                <button type="button" data-mission_id="<?php echo $mission['id'] ?>" class="btn btn-icon btn-success pull-right duplicate_mission tooltips" title="Dupliquer"><i class="fa fa-clone"></i></button>
                                                <?php endif; ?>
                                                <?php if (User::can('delete_mission')): ?>
                                                <button type="button" data-mission_id="<?php echo $mission['id'] ?>" class="delete_mission btn btn-icon btn-danger pull-right tooltips" title="Supprimer"><span class="glyphicon glyphicon-trash" style="padding-left: 0px;"></span></button>
                                                <?php endif; ?>
                                                <?php if (User::can('read_mission')): ?>
<!--                                                <a href="<?php echo AJAX_HANDLER ?>/pdf-mission?id=<?php echo $mission['id'] ?>" class="generate_pdf_mission btn btn-icon btn-warning tooltips" title="Générer un fichier PDF"><span class="glyphicon glyphicon-file"></span></a>-->
                                                <button type="button" data-mission_id="<?php echo $mission['id'] ?>" class="comment_zoom btn btn-icon btn-primary tooltips" title="Historiques Commentaires"><span class="glyphicon glyphicon-comment"></span></button>
                                                <?php endif; ?>
                                            </td>
                                            <?php endif; ?>
                                            <td align="center">
                                                <i class="os-icon os-icon-user-male-circle2 assigned_users tooltips" title="<?php echo addSeperator(Mission::getAssignedUsersDetails($mission['id'])) ?>"></i>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <?php else: ?>
                                <div class="no_search_results">

                                    <i class="os-icon os-icon-ui-37"></i>

                                    <p>Votre recherche n'a donné aucun résultat.</p>

                                </div><!-- /no_search_results -->
                                <?php endif; ?>
							<?php else: ?>
                                <div class="no_search_results">

                                    <i class="os-icon os-icon-ui-37"></i>

                                    <p>Votre recherche n'a donné aucun résultat.</p>

                                </div><!-- /no_search_results -->
							<?php endif; ?>
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
			
        
        </div><!-- /contentWrap -->
        
    </div><!-- /base -->    
    
</body>
</html>
<script src="<?php echo JS_URL ?>/mission.js"></script>