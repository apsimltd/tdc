<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_mission')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $comments = Mission::getComments(get('id')); ?>

<?php if (!empty($comments)): ?>
<div class="task-threads-wrap clearfix">

    <?php foreach ($comments as $comment): ?>
    <div class="task-thread clearfix">

        <p><?php echo nl2br($comment['comment']) ?></p>

        <div class="publish-info">
            Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo dateToFr($comment['created'], true) ?>
            <span class="glyphicon glyphicon-edit edit_comment tooltips" title="Éditer" data-comment_id="<?php echo $comment['id'] ?>" data-mission_id="<?php echo $comment['mission_id'] ?>"></span>
            <span class="glyphicon glyphicon-trash delete_comment tooltips" title="Supprimer" data-comment_id="<?php echo $comment['id'] ?>" data-mission_id="<?php echo $comment['mission_id'] ?>"></span>
        </div>

    </div>
    <?php endforeach; ?>

</div><!--/ task-threads-wrap -->
<?php endif; ?>