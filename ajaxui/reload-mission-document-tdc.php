<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_mission')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $documents = Mission::getDocuments(get('id')); ?>

<?php if (!empty($documents)) :?>
    <ul class="links_candidat">
    <?php foreach($documents as $document): ?>
        <?php 
        $imgType = array('jpg', 'jpeg', 'png');
        $ext = getFileExt($document['file']);
        if (in_array($ext, $imgType)):
        ?>
        <li>
            <a href="<?php echo MISSION_URL ?>/<?php echo get('id') ?>/<?php echo $document['file'] ?>" data-lightbox="<?php echo $document['file'] ?>"><?php echo $document['file'] ?></a>
            <span class="glyphicon glyphicon-trash delete_document tooltips" title="Supprimer" data-document_id="<?php echo $document['id'] ?>" data-mission_id="<?php echo get('id') ?>"></span>
        </li>
        <?php else: ?>
        <li>
            <a href="<?php echo BASE_URL ?>/download?type=miss&id=<?php echo $document['id'] ?>"><?php echo $document['file'] ?></a>
            <span class="glyphicon glyphicon-trash delete_document tooltips" title="Supprimer" data-document_id="<?php echo $document['id'] ?>" data-mission_id="<?php echo get('id') ?>"></span>
        </li>
        <?php endif; ?>
    <?php endforeach;?>
    </ul>
<?php endif; ?>