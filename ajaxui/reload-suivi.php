<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
$mission_id = get('mission_id');
$candidat_id = get('candidat_id');
$suivi = TDC::getSuivi($mission_id, $candidat_id);
                                            
$date_prise_de_contact = ($suivi['date_prise_de_contact'] > 0) ? dateToFr($suivi['date_prise_de_contact']) : '';
$date_rencontre_consultant = ($suivi['date_rencontre_consultant'] > 0) ? dateToFr($suivi['date_rencontre_consultant']) : '';
$date_rencontre_client = ($suivi['date_rencontre_client'] > 0) ? dateToFr($suivi['date_rencontre_client']) : '';
$shortliste = ($suivi['shortliste'] > 0) ? '<span class="status active">Oui</span>' : '<span class="status inactive">Non</span>';
$place = ($suivi['place'] > 0) ? '<span class="status active">Oui</span>' : '<span class="status inactive">Non</span>';

$suivi_hover = '<h2>Suivis</h2>';
$suivi_hover .= '<table>';
    $suivi_hover .= '<tr>';
        $suivi_hover .= '<td>Date de prise de contact</td>';
        $suivi_hover .=  '<td>' . $date_prise_de_contact . '</td>';
    $suivi_hover .= '</tr>';
    $suivi_hover .= '<tr>';
        $suivi_hover .= '<td>Date de rencontre du consultant</td>';
        $suivi_hover .=  '<td>' . $date_rencontre_consultant . '</td>';
    $suivi_hover .= '</tr>';
    $suivi_hover .= '<tr>';
        $suivi_hover .= '<td>Date de rencontre du client</td>';
        $suivi_hover .=  '<td>' . $date_rencontre_client . '</td>';
    $suivi_hover .= '</tr>';
    $suivi_hover .= '<tr>';
        $suivi_hover .= '<td>Short-listé</td>';
        $suivi_hover .=  '<td>' . $shortliste . '</td>';
    $suivi_hover .= '</tr>';
    $suivi_hover .= '<tr>';
        $suivi_hover .= '<td>Placé</td>';
        $suivi_hover .=  '<td>' . $place . '</td>';
    $suivi_hover .= '</tr>';
$suivi_hover .= '</table>';
?>

