<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat') || get('entreprise_id') == ""): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $entreprise = Candidat::getEntrepriseByIdNew(get('entreprise_id')); ?>
<?php $comments = json_decode($entreprise['commentaire'], true) ?>

<h4 class="onboarding-title">Commentaire Entreprise</h4>

<form class="frm_frm frm_ajax" name="frm_edit_entreprise_add_comment" id="frm_edit_entreprise_add_comment" data-url="<?php echo AJAX_HANDLER ?>/add-entreprise-comment" data-type="json">
    <input type="hidden" name="entreprise_id" value="<?php echo get('entreprise_id') ?>">
    <fieldset>
        <textarea name="commentaire" class="frm_textarea commentZoom must" style="width: 100%; height:250px;" placeholder="Ajouter un nouveau commentaire" data-validation="val_blank"></textarea>                                
    </fieldset>

    <fieldset>
        <button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter commentaire</button>
    </fieldset>

</form>

<div class="historiques_comment_wrap">

    <?php if (!empty($comments)): ?>
    <div class="task-threads-wrap clearfix">

        <?php foreach ($comments as $key => $comment): ?>
        <div class="task-thread clearfix">

            <p><?php echo nl2br($comment['comment']) ?></p>

            <div class="publish-info">
                Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo $comment['created'] ?>
                <span class="glyphicon glyphicon-edit edit_comment tooltips" title="Éditer" data-comment_id="<?php echo $key ?>" data-entreprise_id="<?php echo get('entreprise_id') ?>"></span>
                <span class="glyphicon glyphicon-trash delete_comment tooltips" title="Supprimer" data-comment_id="<?php echo $key ?>" data-entreprise_id="<?php echo get('entreprise_id') ?>"></span>
            </div>

        </div>
        <?php endforeach; ?>

    </div><!--/ task-threads-wrap -->
    <?php endif; ?>

</div><!-- /historiques_comment_wrap -->
