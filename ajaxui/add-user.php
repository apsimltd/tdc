<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_user')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<h4 class="onboarding-title">Utilisateur</h4>
<div class="onboarding-text">Ajouter un nouveau utilisateur</div>
<form class="frm_frm frm_ajax" name="frm_create_user" id="frm_create_user" data-url="<?php echo AJAX_HANDLER ?>/add-user" data-type="json">
	<fieldset>
		<label>Nom</label>
		<input class="frm_text must" name="lastName" placeholder="Nom" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Prénom</label>
		<input class="frm_text must" name="firstName" placeholder="Prénom" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Identifiant</label>
		<input class="frm_text must" name="username" placeholder="Identifiant" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Email</label>
		<input class="frm_text must" name="email" placeholder="Email" type="text" autocomplete="off" data-validation="val_email">
	</fieldset>
    <fieldset>
		<label>Mot de passe</label>
		<input class="frm_text must" name="password" placeholder="Mot de passe" type="password" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>3CX Extension</label>
		<input class="frm_text must" name="voip_ext" maxlength="3" placeholder="3CX Extension" type="text" autocomplete="off" data-validation="val_3cx">
	</fieldset>
    <fieldset>
		<label>3CX SDA</label>
		<input class="frm_text must" maxlength="10" name="voip_sda" placeholder="3CX SDA" type="text" autocomplete="off" data-validation="val_sda">
	</fieldset>
    <fieldset>
		<label>Ringover User Id</label>
		<input class="frm_text" name="ringover_userid" placeholder="Ringover User Id" type="text" autocomplete="off" data-validation="val_num">
	</fieldset>
    <fieldset>
		<label>Rôle</label>
		<select class="frm_chosen must" name="role_id" data-validation="val_blank">
        	<option value="">Choisir Rôle</option>
            <?php $roles = Role::getRoles() ?>
            <?php foreach($roles as $role): ?>
            <option value="<?php echo $role['id'] ?>"><?php echo $role['name'] ?></option>
            <?php endforeach;?>
		</select>
	</fieldset>
	<fieldset>
		<label>Chef d'équipe <span class="glyphicon glyphicon-info-sign tooltips" title="Responsable de l'utilisateur"></span></label>
		<select class="frm_chosen" name="user_id" data-validation="val_blank">
        	<option value="0">Choisir Chef d'équipe</option>
			<?php $managers = User::getManagers() ?>
            <?php foreach($managers as $manager): ?>
            <option value="<?php echo $manager['id'] ?>"><?php echo mb_ucfirst($manager['firstName']) . ' ' . mb_strtoupper($manager['lastName']) ?></option>
            <?php endforeach;?>
		</select>
	</fieldset>
    <fieldset>
		<label>Statut</label>
		<select class="frm_chosen must ok" name="status" data-validation="val_blank">
			<option value="1">Actif</option>
			<option value="0">Inactif</option>
		</select>
	</fieldset>
	<fieldset>
		<button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter Utilisateur</button>
	</fieldset>
</form>