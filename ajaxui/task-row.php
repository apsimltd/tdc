<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('create_task', 'read_task', 'read_all_task', 'edit_task', 'delete_task', 'archive_task'))): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>

<?php if (get('id')): ?>
    
    <?php 
    $task_actual_state = Task::getTaskById(get('id'));
    // update the task first
    $task_data = array('id' => get('id'));
    if ($task_actual_state['status'] == 1) { // it is uncomplete/actif => marked as complete
        $task_data['status'] = 0;
    } elseif ($task_actual_state['status'] == 0) { // it is completed => marked as uncomplete/actif
        $task_data['status'] = 1;
    }
    
    if ($task_actual_state['creator_id'] == $me['id']) { // completer of the task
        $task_data['completer'] = 'creator';
    } elseif ($task_actual_state['assignee_id'] == $me['id']) { // i'm the assignee of the task
        $task_data['completer'] = 'assignee';
    }
    
    $task_data['modified'] = time();
    Task::Checked($task_data); // update the task and send email to notify user about the status changed
    
    $task = Task::getTaskById(get('id'));
    // check if the task is in assignee list or creator list of the user
    $myTask = true; // flag for task assigned to me 
    if ($task['creator_id'] == $me['id']) { // creator of the task and display assignee
        $user = User::getUserById($task['assignee_id']);
        $myTask = false;
    } elseif ($task['assignee_id'] == $me['id']) { // i'm the assignee of the task
        $user = User::getUserById($task['creator_id']); // in list of mes taches we display the creator
    }
    ?>

    <div class="task-check tooltips" title="<?php if ($task['status'] == 1): ?>Terminé<?php else: ?>Incomplet<?php endif; ?>" data-task_id="<?php echo $task['id'] ?>">
        <svg viewBox="0 0 32 32"><polygon points="27.672,4.786 10.901,21.557 4.328,14.984 1.5,17.812 10.901,27.214 30.5,7.615 "></polygon></svg>
    </div>
    <div class="task-title">
        <?php echo $task['title'] ?>
        <?php if (User::can('read_task', 'read_all_task', 'create_task')): ?>
        <span class="glyphicon glyphicon-eye-open tooltips view_task" data-task_id="<?php echo $task['id'] ?>" title="Voir Plus"></span>
        <?php endif; ?>
    </div>
    <?php if ($myTask): ?>
    <div class="task-creator">
        <?php echo dateToFr($task['created'], true) ?> - <strong><?php echo mb_ucfirst(substr($user['firstName'], 0, 1)) . '. ' . mb_strtoupper($user['lastName']) ?></strong>

        <?php if (User::can('archive_task')): ?>
            <?php if ($task['status'] == 0): ?><i class="fa fa-archive archive_task tooltips" title="Archiver" data-task_id="<?php echo $task['id'] ?>"></i><?php endif; ?>
        <?php endif; ?>
    </div>
    <?php else: ?>
    <div class="task-creator">
        <?php echo dateToFr($task['created'], true) ?> - <strong><?php echo mb_ucfirst(substr($user['firstName'], 0, 1)) . '. ' . mb_strtoupper($user['lastName']) ?></strong>
        <?php if (User::can('edit_task')): ?>
            <?php if ($task['status'] == 1): ?><span class="glyphicon glyphicon-edit edit_task tooltips" title="Éditer" data-task_id="<?php echo $task['id'] ?>"></span><?php endif; ?>
        <?php endif; ?>
        <?php if (User::can('archive_task')): ?>
            <?php if ($task['status'] == 0): ?><i class="fa fa-archive archive_task tooltips" title="Archiver" data-task_id="<?php echo $task['id'] ?>"></i><?php endif; ?>
        <?php endif; ?>
        <?php if (User::can('delete_task')): ?>
            <?php if ($task['status'] == 1): ?><span class="glyphicon glyphicon-trash delete_task tooltips" title="Supprimer" data-task_id="<?php echo $task['id'] ?>"></span><?php endif; ?>
        <?php endif; ?>
    </div>
    <?php endif; ?>

<?php endif; ?>

