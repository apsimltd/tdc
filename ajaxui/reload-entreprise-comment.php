<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $entreprise = Candidat::getEntrepriseByIdNew(get('id')); ?>
<?php $comments = json_decode($entreprise['commentaire'], true) ?>

<?php if (!empty($comments)): ?>
<div class="task-threads-wrap clearfix">

    <?php foreach (array_reverse($comments, true) as $key => $comment): ?>
    <div class="task-thread clearfix">

        <p><?php echo nl2br($comment['comment']) ?></p>

        <div class="publish-info">
            Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo $comment['created'] ?>
            <span class="glyphicon glyphicon-edit edit_comment tooltips" title="Éditer" data-comment_id="<?php echo $key ?>" data-entreprise_id="<?php echo $entreprise['id'] ?>"></span>
            <span class="glyphicon glyphicon-trash delete_comment tooltips" title="Supprimer" data-comment_id="<?php echo $key ?>" data-entreprise_id="<?php echo $entreprise['id'] ?>"></span>
        </div>

    </div>
    <?php endforeach; ?>

</div><!--/ task-threads-wrap -->
<?php endif; ?>