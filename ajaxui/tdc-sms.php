<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $candidat = Candidat::getCandidatById(get('candidat_id')); // debug($candidat); ?>
<?php $civilite = Control::getControlListByType(7); ?>

<h4 class="onboarding-title">
    Envoie SMS à
    <?php
    if ($candidat['control_civilite_id'] == 0): 
        echo mb_strtoupper($candidat['nom']) . ' ' . mb_ucfirst($candidat['prenom']);
    else:
        echo $civilite[$candidat['control_civilite_id']] . ' ' . mb_strtoupper($candidat['nom']) . ' ' . mb_ucfirst($candidat['prenom']);
    endif;
    ?>
</h4>

<p style="color: red; line-height: 15px;">Le caractère maximum pour un message est <strong>160</strong>, espaces compris.</p>
<p style="color: red; line-height: 15px;">Eviter les caractères spéciaux comme <strong>è</strong> car ils seront tous remplacés en tant que caractère normal <strong>e</strong> comme cella consommerai 8-10 caractères pour un message texte par SMS.</p>

<form class="frm_frm frm_ajax" name="frm_sms" id="frm_sms" data-url="<?php echo AJAX_HANDLER ?>/tdc-sms" data-type="json">
    
    <input type="hidden" name="mission_id" value="<?php echo get('mission_id') ?>">
    <input type="hidden" name="candidat_id" value="<?php echo get('candidat_id') ?>">
    
    <fieldset>
		<label>Coordonnée</label>
		<select class="frm_chosen must" name="to_raw" data-validation="val_blank">
        	<option value="">Choisir Coordonnée</option>
			<?php if ($candidat['tel_standard'] <> ""): ?>
            <option value="<?php echo $candidat['tel_standard'] ?>"><?php echo $candidat['tel_standard'] ?> - Téléphone standard</option>
            <?php endif; ?>
            <?php if ($candidat['tel_ligne_direct'] <> ""): ?>
            <option value="<?php echo $candidat['tel_ligne_direct'] ?>"><?php echo $candidat['tel_ligne_direct'] ?> - Téléphone ligne directe</option>
            <?php endif; ?>
            <?php if ($candidat['tel_pro'] <> ""): ?>
            <option value="<?php echo $candidat['tel_pro'] ?>"><?php echo $candidat['tel_pro'] ?> - Téléphone mobile pro</option>
            <?php endif; ?>
            <?php if ($candidat['tel_mobile_perso'] <> ""): ?>
            <option value="<?php echo $candidat['tel_mobile_perso'] ?>"><?php echo $candidat['tel_mobile_perso'] ?> - Téléphone mobile perso</option>
            <?php endif; ?>
            <?php if ($candidat['tel_domicile'] <> ""): ?>
            <option value="<?php echo $candidat['tel_domicile'] ?>"><?php echo $candidat['tel_domicile'] ?> - Téléphone domicile</option>
            <?php endif; ?>
		</select>
	</fieldset>
    
    <fieldset>
        <label>Message <span class="glyphicon glyphicon-info-sign tooltips" title="Le caractère maximum est 160"></span></label>
        <textarea name="message_typed" maxlength="160" class="frm_textarea must" style="height:150px;" placeholder="Message" data-validation="val_blank"></textarea>
    </fieldset>
    
    <fieldset>
        <button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-envelope"></span> Envoyer SMS</button>
    </fieldset>
    
</form>

<?php $smss = SMS::getSMS(get('mission_id'), get('candidat_id')); ?>
<?php // debug($smss) ?>

<div class="historiques_comment_wrap">

    <?php if (!empty($smss)): ?>
    <div class="task-threads-wrap clearfix sms">

        <?php foreach ($smss as $sms): ?>
        <div class="task-thread clearfix">

            <p><strong>Message Original</strong> : <?php echo nl2br($sms['message_typed']) ?></p>
            
            <p><strong>Message Envoyé</strong> : <?php echo nl2br($sms['message_typed']) ?></p>
            
            <p><strong>Téléphone</strong> : <?php echo $sms['to_raw'] ?> => <?php echo $sms['to_processed'] ?></p>
            <p><strong>Statut</strong> : <?php if (intval($sms['status']) == 0 && $sms['status'] <> NULL): echo "<span class='status in'>Succès</span>"; else : echo "<span class='status out'>Échoué</span> : " . $sms['error_text']; endif; ?></p>

            <div class="publish-info">
                Envoyée par <strong><?php echo mb_ucfirst($sms['firstName']) . ' ' . mb_strtoupper($sms['lastName']) ?></strong> le <?php echo $sms['created'] ?>
            </div>

        </div>
        <?php endforeach; ?>

    </div><!--/ task-threads-wrap -->
    <?php endif; ?>

</div><!-- /historiques_comment_wrap -->