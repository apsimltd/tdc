<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_audit')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php if (getPost()) { ?>
    
    <fieldset>
        <select class="frm_chosen must" name="action_code" id="action_code" data-validation="val_blank">
            <option value="">Choisir Action</option>
            <?php $actions = Watchdog::$audit_actions[getPost('module_code')]; ?>
            <?php foreach($actions as $key => $value): ?>
            <option value="<?php echo $key ?>"><?php echo $value ?></option>
            <?php endforeach; ?>
        </select>
    </fieldset>

    <fieldset class="single-row">
        <label>Date</label>
        <input class="frm_text start_date small must" id="start_date" name="start_date" placeholder="Dé" type="text" autocomplete="off" data-validation="val_blank">
        <span class="interval"> - </span>
        <input class="frm_text end_date small" id="end_date" name="end_date" placeholder="À" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>

    <fieldset>
        <label>Utilisateur</label>
        <select class="frm_chosen" name="user_id" id="user_id" data-validation="val_blank">
            <option value="">Choisir Utilisateur</option>
            <?php $users = USER::getUsers(); ?>
            <?php foreach($users as $user): ?>
            <option value="<?php echo $user['id'] ?>"><?php echo mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName']) . ' - [' . $user['role_name'] . '] - [' . statut($user['status']) . ']' ?></option>
            <?php endforeach; ?>
        </select>
    </fieldset>
    
<script>
$(document).ready(function(){
    $("#start_date").on().datepicker({
        dateFormat: 'yy-mm-dd',
        regional: 'fr',
        // minDate: '-7y', 
        minDate: "2020-01-28",
        maxDate: '+1d',
        showButtonPanel: true,
        onClose: function(selectedDate) {
            $("#end_date").datepicker("option", "minDate", selectedDate); 
        },
    });
    $("#end_date").on().datepicker({
        dateFormat: 'yy-mm-dd',
        regional: 'fr',
        minDate: '-7y', 
        maxDate: '+1d',
        showButtonPanel: true,
        onClose: function(selectedDate) {
            $("#start_date").datepicker("option", "maxDate", selectedDate);
        }
    });
});
</script>

<?php } ?>