<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
$tdc = TDC::getTDCById(get('tdc_id'));
$status = Control::getControlListByType(8);
if (isGet()) {
    if ($tdc['control_statut_tdc'] > 0): ?>
    <span class="status <?php echo getClassStatusTDC($tdc['control_statut_tdc']) ?>">
        <?php echo $status[$tdc['control_statut_tdc']] ?>
    </span>
   <?php
   endif;
}
?>