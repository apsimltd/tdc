<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_candidat') || get('entreprise_id') == ""): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $suivis = Candidat::getSuivisEntreprise(get('entreprise_id')); ///debug($suivis); ?> 
<h4 class="onboarding-title">Historiques Missions</h4>

<?php if (!empty($suivis)): ?>
<table class="table-list compact fixed-actions">
    <thead>
        <tr>
            <th>Mission</th>
            <th>Statut</th>
            <th>Client</th>
            <th>Poste</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($suivis as $suivi): ?>
        <tr>
            
            <td align="center"><a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $suivi['mission_id'] ?>"><?php echo $suivi['mission_id'] ?></a></td>
            <td align="center">
               <?php if ($suivi['status_entreprise'] == "" || $suivi['status_entreprise'] == 0): ?>
                    <?php // do nothing ?>
                <?php elseif ($suivi['status_entreprise'] == 257): ?>
                <span class="status out">
                    OUT
                </span>
                <?php elseif ($suivi['status_entreprise'] == 258): ?>
                <span class="status buminus">
                    FINI
                </span>
                <?php endif; ?>

            </td>
            <td><?php echo $suivi['client'] ?></td>
            <td><?php echo $suivi['poste'] ?></td>

        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php else: ?>
    <div class="message warning">Cette entreprise n'a pas des suivis dans aucun mission.</div>
<?php endif; ?>