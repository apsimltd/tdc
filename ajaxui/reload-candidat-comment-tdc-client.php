<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('tdc', 'TDC_CLIENT'))): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php $comments = TDC::getCommentClient(get('mission_id'), get('candidat_id')); // debug($comments); ?>

<?php if (!empty($comments)): ?>
<div class="task-threads-wrap clearfix">

    <?php foreach ($comments as $comment): ?>
    <div class="task-thread clearfix">

        <p><?php echo nl2br($comment['comment']) ?></p>

        <div class="publish-info">
            Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo $comment['created'] ?>
        </div>

    </div>
    <?php endforeach; ?>

</div><!--/ task-threads-wrap -->
<?php endif; ?>