<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('edit_candidat', 'create_candidat'))): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $log = Watchdog::getById(get('id')); ?>

<h4 class="onboarding-title">Historique Action Entreprise</h4>
<div class="onboarding-text"><strong>Action :</strong> <?php echo $log['action'] ?> <strong>Par :</strong> <?php echo $log['user_name'] ?> <strong>Le :</strong> <?php echo $log['created'] ?></div>

<?php if ($log['content_before'] <> "" || $log['content_after'] <> ""): ?>

<?php 
$col_before = true;
$col_after = true;
if ($log['content_before'] <> "" && $log['content_after'] <> "") {
    $col_size = 6;
    $col_before = true;
    $col_after = true;
} elseif ($log['content_before'] <> "" && $log['content_after'] == "") {
    $col_size = 12;
    $col_before = true;
    $col_after = false;
} elseif ($log['content_before'] == "" && $log['content_after'] <> "") {
    $col_size = 12;
    $col_before = false;
    $col_after = true;
}

$secteurs = Control::getControlListByType(2);

// note that all content type for entreprise log is in json format 
// so no need to check if its of type json
?>

<div class="watch_wrapper">
    
    <div class="row">
		
        <?php if ($col_before): ?>
        
            <?php $before = json_decode($log['content_before'], true);  ?>

            <?php if (!empty($before)): ?>
            <div class="col col-<?php echo $col_size ?>">
                <div class="block nopadding">
                    <div class="block-body">

                        <?php debug($before); ?>

                    </div><!-- / block-body -->
                </div><!-- / block -->
            </div><!-- / col -->
            <?php endif; ?>
        
        <?php endif; ?>
        
        
        <?php if ($col_after): ?>
            
            <?php $after = json_decode($log['content_after'], true); //debug($after) ?>
            
            <?php if (!empty($after)): ?>
            <div class="col col-<?php echo $col_size ?>">
                <div class="block nopadding">
                    <div class="block-body">

                        <?php debug($after); ?>                                                

                    </div><!-- / block-body -->
                </div><!-- / block -->
            </div><!-- / col -->
            <?php endif; ?>
        
        <?php endif; ?>    
            
    </div><!-- / row -->
    
</div><!-- /watch_wrapper --> 
<?php endif; ?>