<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_my_profile')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $user = User::getUserById(get('user_id')); ?>

<h4 class="onboarding-title">Mon Profile</h4>
<form class="frm_frm frm_ajax" name="frm_mon_profile" id="frm_mon_profile" data-url="<?php echo AJAX_HANDLER ?>/mon-profile" data-type="json">
    
    <input type="hidden" name="id" value="<?php echo $user['id'] ?>">
    
	<fieldset>
		<label>Nom</label>
        <input class="frm_text" name="lastName" value="<?php echo mb_strtoupper($user['lastName']) ?>" placeholder="Nom" type="text" autocomplete="off" readonly="readonly">
	</fieldset>
    <fieldset>
		<label>Prénom</label>
        <input class="frm_text" name="firstName" value="<?php echo mb_ucfirst($user['firstName']) ?>" placeholder="Prénom" type="text" autocomplete="off" readonly="readonly">
	</fieldset>
    <fieldset>
		<label>Identifiant</label>
        <input class="frm_text" name="username" value="<?php echo $user['username'] ?>" placeholder="Identifiant" type="text" autocomplete="off" readonly="readonly">
	</fieldset>
    <fieldset>
		<label>Email</label>
		<input class="frm_text" name="email" value="<?php echo $user['email'] ?>" placeholder="Email" type="text" autocomplete="off" readonly="readonly">
	</fieldset>
    
    <?php if (User::isClient()): // user client ?>
    <fieldset class="mission-checkbox">
        <input type="checkbox" class="frm_checkbox" name="mail_notif_check" id="mail_notif_check" <?php if ($user['mail_notif'] == 1): ?>checked="checked"<?php endif; ?>>
        <label class="pointer" for="mail_notif_check">Notification Email</label>
    </fieldset>
    <input type="hidden" name="mail_notif" id="mail_notif" value="<?php echo $user['mail_notif'] ?>">
    <?php endif; ?>
    
    <h4>Changement mot de passe</h4>
    
    <div class="message error" style="display: none;">Le mot de passe actuel ne correspond pas</div><!-- message -->
    
    <fieldset>
        <label>Mot de passe actuel</label>
        <input class="frm_text" name="password" id="password" placeholder="Mot de passe actuel" type="password" autocomplete="off" data-validation="val_blank">
    </fieldset>
    <div class="password_tracker" style="display: none;">
        <input class="frm_text" id="password_tracker" type="text" autocomplete="off" data-validation="val_blank">
    </div><!-- password_tracker -->
    <fieldset>
        <label>Nouveau mot de passe</label>
        <input class="frm_text" name="new_password" id="new_password" placeholder="Nouveau mot de passe" type="password" autocomplete="off" data-validation="val_blank">
    </fieldset>
    <fieldset>
        <button type="button" class="btn btn-primary frm_submit frm_notif frm_before_submit pull-right" data-form="2"><span class="glyphicon glyphicon-pencil"></span> Éditer</button>
    </fieldset>
</form>
<?php if (User::isClient()): ?>
<script>
$(document).ready(function(){
    
    // checked and unchecked include mission in kpi
    $(document).on('ifUnchecked', '#mail_notif_check', function(){
        $('#mail_notif').val('null');
    });
    $(document).on('ifChecked', '#mail_notif_check', function(){
        $('#mail_notif').val('1');
    });
    
});
</script>
<?php endif; ?>
