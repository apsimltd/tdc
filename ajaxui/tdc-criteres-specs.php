<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $tdc_id = get('tdc_id') ?>
<?php $tdc = TDC::getTDCById($tdc_id); ?>
<?php $criteres = Mission::getCriteres(get('mission_id')) ?>
<?php $selected_criteres_ids = TDC::getSelectedCriteres(get('mission_id'), get('candidat_id'))?>
<?php // debug($tdc) ?>

<h4 class="onboarding-title">Critères spécifiques</h4>

<form class="frm_frm frm_ajax" name="frm_tdc_criteres" id="frm_tdc_criteres" data-url="<?php echo AJAX_HANDLER ?>/tdc-criteres-specs" data-type="json">
    
    <input type="hidden" name="mission_id" value="<?php echo get('mission_id') ?>">
    <input type="hidden" name="candidat_id" value="<?php echo get('candidat_id') ?>">
    <input type="hidden" name="tdc_id" value="<?php echo get('tdc_id') ?>">
    
    <fieldset class="selectOption largemultiselect">
        <label style="text-align: left;">Critères spécifiques</label>
        <div class="frm_select_multi_wrapper">
            <select class="frm_select_multi <?php if (count($selected_criteres_ids) > 0): ?>ok<?php endif; ?>" data-validation="val_blank" multiple='multiple'>
                <?php foreach($criteres as $critere): ?>
                    <option value="<?php echo $critere['id'] ?>" <?php if (in_array($critere['id'], $selected_criteres_ids)): ?>selected="selected"<?php endif; ?>>
                        <?php echo $critere['criteres'] ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <input type="hidden" name="criteres_ids" value="<?php echo addSep($selected_criteres_ids) ?>" class="frm_select_multi_helper" data-validation="val_blank">
        </div>
    </fieldset>
    
    <fieldset>
        <textarea name="commentaire_criteres" class="frm_textarea <?php if ($tdc['commentaire_criteres'] != ""): ?>ok<?php endif; ?>" style="width: 100%; height:150px;" placeholder="Commentaire" data-validation="val_blank"><?php echo $tdc['commentaire_criteres'] ?></textarea>                                
    </fieldset>
    
    <fieldset>
        <button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-floppy-disk"></span> Sauvegarder</button>
    </fieldset>
    
</form>