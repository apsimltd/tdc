<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_audit')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php if (isPost()) : ?>
    
    <?php
    $user_id = getPost('user_id');
    $start_date = getPost('start_date');
    $end_date = getPost('end_date');    
    
    $audits = Watchdog::getByUser($user_id, $start_date, $end_date);
    ?>

    <div class="row">

        <div class="col col-12">
            <div class="block nopadding">

                <div class="block-head with-border">
                    <header><span class="glyphicon glyphicon-time"></span>Audit</header>
                </div>

                <div class="block-body">

                    <table class="datable stripe hover">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Action</th>
                                <th>IP</th>
                                <th>Statut Précédent</th>
                                <th>Statut Après</th>
                                <th>Voir Plus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($audits as $audit): ?>
                            <tr>
                                <td><?php echo $audit['created'] ?></td>
                                <td><?php echo $audit['action'] ?></td>
                                <td><?php echo $audit['user_ip'] ?></td>
                                <td>
                                    <?php if ($audit['status_before'] <> ""): ?>
                                        <?php if ($audit['status_before'] == 258): ?>
                                            <?php echo "FINI"; ?>
                                        <?php elseif ($audit['status_before'] == 257): ?>
                                            <?php echo "OUT"; ?>
                                        <?php else: ?>
                                            <?php echo $audit['status_before'] ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if ($audit['status_after'] <> ""): ?>
                                        <?php if ($audit['status_after'] == 258): ?>
                                            <?php echo "FINI"; ?>
                                        <?php elseif ($audit['status_after'] == 257): ?>
                                            <?php echo "OUT"; ?>
                                        <?php else: ?>
                                            <?php echo $audit['status_after'] ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td class="actions_button">
                                    <?php if ($audit['content_type'] == "json"): ?>
                                    <button type="button" data-watchdog_id="<?php echo $audit['id'] ?>" class="see_more_audit_watchdog btn btn-icon btn-success tooltips" title="Voir Plus"><span class="glyphicon glyphicon-eye-open"></span></button> 
                                    <?php endif; ?>
                                </td>
                                
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div><!-- / block-body -->

            </div>
        </div><!-- /col -->

    </div><!-- / row -->
<?php endif; ?>