<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('edit_candidat', 'create_candidat'))): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $log = Watchdog::getById(get('id')); ?>

<h4 class="onboarding-title">Historique Action Candidat</h4>
<div class="onboarding-text"><strong>Action :</strong> <?php echo $log['action'] ?> <strong>Par :</strong> <?php echo $log['user_name'] ?> <strong>Le :</strong> <?php echo $log['created'] ?></div>

<?php if ($log['content_before'] <> "" || $log['content_after'] <> ""): ?>

<?php 
if ($log['content_before'] <> "" && $log['content_after'] <> "")
    $col_size = 6;
elseif ($log['content_before'] <> "" && $log['content_after'] == "")
    $col_size = 12;
elseif ($log['content_before'] == "" && $log['content_after'] <> "")
    $col_size = 12;

$civilite = Control::getControlListByType(7);
$dpt = Control::getControlListByType(6);
$secteurs = Control::getControlListByType(2);
$fonctions = Control::getControlListByType(1);
$sources = Control::getControlListByType(3);
$formations = Control::getControlListByType(4);
$langues = Control::getControlListByType(5);

// note that all content type for candidat log is in json format 
// so no need to check if its of type json
?>

<div class="watch_wrapper">
    
    <div class="row">
		
        <?php if ($log['content_before'] <> ""): ?>
        <div class="col col-<?php echo $col_size ?>">
            <div class="block nopadding">
                <div class="block-body">
                    
                    <?php $before = json_decode($log['content_before'], true); //debug($before) ?> 
                    
                    <table class="table-list compact inside-nano">
                        
                        <thead>
                            <tr>
                                <th>Libele</th>
                                <th>valeur</th>
                            </tr>
                        </thead>
                        
                        <?php foreach($before as $key => $value): ?>
                            <?php if ($key <> "reseauSociaux_id"): ?>
                            <tr>
                                <td><strong><?php echo Candidat::$fields_label[$key] ?></strong></td>
                                <td>
                                    <?php
                                    if ($key == "created" || $key == "modified") {
                                        echo dateToFr($value, true);
                                    } elseif ($key == "dateOfBirth") {
                                        echo dateToFr($value);
                                    } elseif ($key == "control_civilite_id") {
                                        if ($value == 0) {
                                            echo "";
                                        } else {
                                            echo $civilite[$value];
                                        }
                                    } elseif ($key == "control_localisation_id") {
                                        if ($value == 0) {
                                            echo "Aucun";
                                        } else {
                                            echo $dpt[$value];
                                        }
                                    } elseif ($key == "control_secteur_id") {
                                        if ($value == 0) {
                                            echo "Aucun";
                                        } else {
                                            echo $secteurs[$value];
                                        }
                                    } elseif ($key == "control_fonction_id") {
                                        if ($value == 0) {
                                            echo "Aucun";
                                        } else {
                                            echo $fonctions[$value];
                                        }
                                    } elseif ($key == "control_source_id") {
                                        if ($value == 0) {
                                            echo "Aucun";
                                        } else {
                                            echo $sources[$value];
                                        }
                                    } elseif ($key == "control_niveauFormation_id") {
                                        if ($value == 0) {
                                            echo "Aucun";
                                        } else {
                                            echo $formations[$value];
                                        }
                                    } elseif ($key == "control_langue_id") {
                                        if ($value == 0) {
                                            echo "Aucun";
                                        } else {
                                            echo $langues[$value];
                                        }
                                    } elseif ($key == "documents" && isJSON($value)) {
                                        echo "";
                                    } else {
                                        echo $value;
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        
                    </table>
                    
                    
                </div><!-- / block-body -->
            </div><!-- / block -->
        </div><!-- / col -->
        <?php endif; ?>
        
        <?php if ($log['content_after'] <> ""): ?>
        <div class="col col-<?php echo $col_size ?>">
            <div class="block nopadding">
                <div class="block-body">
                    
                    <?php $after = json_decode($log['content_after'], true); //debug($after) ?>
                    
                    <table class="table-list compact inside-nano">
                        
                        <thead>
                            <tr>
                                <th>Libele</th>
                                <th>valeur</th>
                            </tr>
                        </thead>
                        
                        <?php foreach($after as $key => $value): ?>
                            <?php if ($key <> "reseauSociaux_id"): ?>
                            <tr>
                                <td><strong><?php echo Candidat::$fields_label[$key] ?></strong></td>
                                <td>
                                    <?php
                                    if ($key == "created" || $key == "modified") {
                                        echo dateToFr($value, true);
                                    } elseif ($key == "dateOfBirth") {
                                        echo dateToFr($value);
                                    } elseif ($key == "control_civilite_id") {
                                        if ($value == 0) {
                                            echo "";
                                        } else {
                                            echo $civilite[$value];
                                        }
                                    } elseif ($key == "control_localisation_id") {
                                        if ($value == 0) {
                                            echo "Aucun";
                                        } else {
                                            echo $dpt[$value];
                                        }
                                    } elseif ($key == "control_secteur_id") {
                                        if ($value == 0) {
                                            echo "Aucun";
                                        } else {
                                            echo $secteurs[$value];
                                        }
                                    } elseif ($key == "control_fonction_id") {
                                        if ($value == 0) {
                                            echo "Aucun";
                                        } else {
                                            echo $fonctions[$value];
                                        }
                                    } elseif ($key == "control_source_id") {
                                        if ($value == 0) {
                                            echo "Aucun";
                                        } else {
                                            echo $sources[$value];
                                        }
                                    } elseif ($key == "control_niveauFormation_id") {
                                        if ($value == 0) {
                                            echo "Aucun";
                                        } else {
                                            echo $formations[$value];
                                        }
                                    } elseif ($key == "control_langue_id") {
                                        if ($value == 0) {
                                            echo "Aucun";
                                        } else {
                                            echo $langues[$value];
                                        }
                                    } elseif ($key == "documents" && isJSON($value)) {
                                        echo "";
                                    } else {
                                        echo $value;
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        
                    </table>
                    
                </div><!-- / block-body -->
            </div><!-- / block -->
        </div><!-- / col -->
        <?php endif; ?>
        
    </div><!-- / row -->
    
</div><!-- /watch_wrapper --> 
<?php endif; ?>