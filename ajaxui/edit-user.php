<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_user')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $user = User::getUserById(get('id')); ?>

<h4 class="onboarding-title">Utilisateur</h4>
<div class="onboarding-text">Éditer l'utilisateur</div>
<form class="frm_frm frm_ajax" name="frm_edit_user" id="frm_edit_user" data-url="<?php echo AJAX_HANDLER ?>/edit-user" data-type="json">
    <input type="hidden" name="id" value="<?php echo $user['id'] ?>">
	<fieldset>
		<label>Nom</label>
        <input class="frm_text must ok" value="<?php echo mb_strtoupper($user['lastName']) ?>" name="lastName" placeholder="Nom" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Prénom</label>
        <input class="frm_text must ok" value="<?php echo mb_ucfirst($user['firstName']) ?>" name="firstName" placeholder="Prénom" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Identifiant</label>
		<input class="frm_text must ok" value="<?php echo $user['username'] ?>" name="username" placeholder="Identifiant" type="text" autocomplete="off" data-validation="val_blank">
        <input type="hidden" name="old_username" value="<?php echo $user['username'] ?>">
	</fieldset>
    <fieldset>
		<label>Email</label>
		<input class="frm_text must ok" value="<?php echo $user['email'] ?>" name="email" placeholder="Email" type="text" autocomplete="off" data-validation="val_email">
        <input type="hidden" name="old_email" value="<?php echo $user['email'] ?>">
	</fieldset>
    <fieldset>
		<label>Mot de passe</label>
		<input class="frm_text" name="password" placeholder="Mot de passe" type="password" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>3CX Extension</label>
		<input class="frm_text must ok" name="voip_ext" value="<?php if($user['voip_ext'] > 0) { echo $user['voip_ext']; } ?>" placeholder="3CX Extension" type="text" autocomplete="off" data-validation="val_3cx" maxlength="3">
        <input type="hidden" name="old_voip_ext" value="<?php echo $user['voip_ext'] ?>">
	</fieldset>
    <fieldset>
		<label>3CX SDA</label>
        <input class="frm_text must ok" name="voip_sda" value="<?php echo $user['voip_sda'] ?>" placeholder="3CX SDA" type="text" autocomplete="off" data-validation="val_sda" maxlength="10">
        <input type="hidden" name="old_voip_sda" value="<?php echo $user['voip_sda'] ?>">
	</fieldset>
    <fieldset>
		<label>Ringover User Id</label>
		<input class="frm_text" name="ringover_userid" value="<?php echo $user['ringover_userid'] ?>" placeholder="Ringover User Id" type="text" autocomplete="off" data-validation="val_num">
        <input type="hidden" name="old_ringover_userid" value="<?php echo $user['ringover_userid'] ?>">
	</fieldset>
    <fieldset>
		<label>Rôle</label>
		<select class="frm_chosen must ok" name="role_id" data-validation="val_blank">
        	<option value="">Choisir Rôle</option>
            <?php $roles = Role::getRoles() ?>
            <?php foreach($roles as $role): ?>
            <option value="<?php echo $role['id'] ?>" <?php if ($user['role_id'] == $role['id']): ?>selected="selected"<?php endif; ?>>
                <?php echo $role['name'] ?>
            </option>
            <?php endforeach;?>
		</select>
	</fieldset>
	<fieldset>
		<label>Chef d'équipe <span class="glyphicon glyphicon-info-sign tooltips" title="Responsable de l'utilisateur"></span></label>
		<select class="frm_chosen <?php if (intval($user['user_id']) > 0): ?>ok<?php endif; ?>" name="user_id" data-validation="val_blank">
        	<option value="0">Choisir Chef d'équipe</option>
			<?php $managers = User::getManagers() ?>
            <?php foreach($managers as $manager): ?>
            <option value="<?php echo $manager['id'] ?>" <?php if (intval($user['user_id']) > 0 && $user['user_id'] == $manager['id']): ?>selected="selected"<?php endif; ?>>
                <?php echo mb_ucfirst($manager['firstName']) . ' ' . mb_strtoupper($manager['lastName']) ?>
            </option>
            <?php endforeach;?>
		</select>
        <input type="hidden" name="old_user_id" value="<?php echo $user['user_id'] ?>">
	</fieldset>
    <fieldset>
		<label>Statut</label>
		<select class="frm_chosen must ok" name="status" data-validation="val_blank">
			<option value="1" <?php if ($user['status'] == 1): ?>selected="selected"<?php endif; ?>>Actif</option>
			<option value="0" <?php if ($user['status'] == 0): ?>selected="selected"<?php endif; ?>>Inactif</option>
		</select>
	</fieldset>
	<fieldset>
		<button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-pencil"></span> Éditer Utilisateur</button>
	</fieldset>
</form>