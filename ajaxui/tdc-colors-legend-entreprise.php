<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
$colors = TDC::getColorsLegendEntreprise(get('mission_id'));
if (empty($colors)) {
    TDC::AddColorsLegendEntreprise(get('mission_id'));
    $colors = TDC::getColorsLegendEntreprise(get('mission_id'));
}
?>
<h4 class="onboarding-title">Légende Couleurs Entreprise</h4>

<form class="frm_frm frm_ajax" name="frm_tdc_colors_legende_entreprise" id="frm_tdc_colors_legende_entreprise" data-url="<?php echo AJAX_HANDLER ?>/tdc-colors-legend-entreprise" data-type="json">
    
    <?php foreach($colors as $color): ?>
    <fieldset>
        <label>
            <span style="display:inline-block;width:100%; height: 32px;background-color:#<?php echo $color['hex'] ?>"></span>
        </label>
        <input type="hidden" name="id[]" value="<?php echo $color['id'] ?>">
        <input class="frm_text <?php if ($color['description'] != ""): ?>ok<?php endif; ?>" value="<?php echo $color['description'] ?>" name="desc[]" placeholder="legende" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>
    <?php endforeach; ?>
    
    <fieldset>
        <button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-floppy-disk"></span> Sauvegarder</button>
    </fieldset>
    
</form>