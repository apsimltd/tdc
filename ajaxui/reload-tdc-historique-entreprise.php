<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $comments = TDC::getHistoriquesEntreprise(get('mission_id'), get('entreprise_id')) ?>

<?php if (!empty($comments)): ?>


    <?php foreach ($comments as $comment): ?>
    <div class="task-thread clearfix">

        <p><?php echo nl2br($comment['comment']) ?></p>

        <div class="publish-info">
            Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo dateToFr($comment['created'], true) ?>
            <span class="glyphicon glyphicon-edit edit_comment_histo_entreprise tooltips" title="Éditer" data-historique_id="<?php echo $comment['id'] ?>"></span>
                <span class="glyphicon glyphicon-trash delete_comment_histo_entreprise tooltips" title="Supprimer" data-historique_id="<?php echo $comment['id'] ?>" data-mission_id="<?php echo $comment['mission_id'] ?>" data-entreprise_id="<?php echo $comment['entreprise_id'] ?>"></span>
        </div>

    </div>
    <?php endforeach; ?>


<?php endif; ?>