<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $tdc = TDC::getTDCById(get('tdc_id')) ?>
<?php $suivi = json_decode($tdc['suivis_entreprise'], true); ?>

<?php
// the user must put a status first
$disabled = '';
if ($tdc['status_entreprise'] == 0 || $tdc['status_entreprise'] == "")
    $disabled = 'disabled="disabled"';
?>

<h4 class="onboarding-title">Suivis Entreprise</h4>

<?php if ($tdc['status_entreprise'] == 0 || $tdc['status_entreprise'] == ""): ?>
<div class="message warning">
    Vous ne pouvez pas ajouter de suivi pour cette entreprise car il n'a pas de statut.
</div>
<?php endif; ?>

<form class="frm_frm frm_ajax" name="frm_tdc_suivi_entreprise" id="frm_tdc_suivi_entreprise" data-url="<?php echo AJAX_HANDLER ?>/tdc-suivi-entreprise" data-type="json">
    
    <input type="hidden" name="mission_id" value="<?php echo get('mission_id') ?>">
    <input type="hidden" name="entreprise_id" value="<?php echo get('entreprise_id') ?>">
    <input type="hidden" name="tdc_id" id="tdc_id_value" value="<?php echo get('tdc_id') ?>">
        
    <fieldset>
        <textarea name="comment" class="frm_textarea <?php if($suivi['comment'] != ""): ?>ok<?php endif; ?>" style="width: 100%; height:150px;" placeholder="Suivi du statut" data-validation="val_blank" <?php echo $disabled ?>><?php echo $suivi['comment'] ?></textarea>
    </fieldset>
    
    <?php if ($tdc['status_entreprise'] > 0): ?>
    <fieldset>
        <button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-floppy-disk"></span> Sauvegarder</button>
    </fieldset>
    <?php endif; ?>
    
</form>