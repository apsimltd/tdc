<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('add_event')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<h4 class="onboarding-title">Événements</h4>
<div class="onboarding-text">Ajouter un nouvel événement</div>
<form class="frm_frm frm_ajax" name="frm_create_event" id="frm_create_event" data-url="<?php echo AJAX_HANDLER ?>/add-event" data-type="json">
	<fieldset>
		<label>Événement</label>
		<input class="frm_text must" name="title" placeholder="Description evénement" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Type Événement</label>
		<select class="frm_chosen must" name="eventType_id" data-validation="val_blank">
        	<option value="">Choisir Type Événement</option>
            <?php $types = Event::getTypes() ?>
            <?php foreach($types as $type): ?>
			<option value="<?php echo $type['id'] ?>"><?php echo $type['name'] ?></option>
			<?php endforeach; ?>
		</select>
	</fieldset>
	<fieldset>
		<label>Date</label>
        <input class="frm_text datepicker must ok" name="date" value="<?php echo dateToFr(time()) ?>" placeholder="Date" type="text" autocomplete="off" readonly="readonly" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Heure de début</label>
        <input class="frm_text must time" id="start" name="start" placeholder="Heure de début" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
	<fieldset>
		<label>Heure de fin</label>
        <input class="frm_text must time" id="end" name="end" placeholder="Heure de fin" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
	<fieldset>
		<button type="button" class="btn btn-primary frm_submit frm_before_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter evénement</button>
	</fieldset>
</form>
<script>    
$(document).ready(function(){
   $('.time').bootstrapMaterialDatePicker({
        date: false,
        shortTime: false,
        format: 'HH:mm',
        switchOnClick: true,
        cancelText: 'Annuler',
        okText: 'OK',
    }).on('close', function(event){
        if ($('#end').val() != "" && $('#start').val() != "") {
            var start = ($('#start').val()).replace(':', '');
            var end = ($('#end').val()).replace(':', '');
            if (start == end) {
                $('#end').removeClass('ok').addClass('error');
                $('#start').removeClass('ok').addClass('error');
                $('#end', '#start').val('');
                Notif.Show('L\'heure de début doit être antérieure à l\'heure de fin', 'error', true, 5000);
            } else if (parseInt(start) > parseInt(end)) {
                $('#end').removeClass('ok').addClass('error');
                $('#start').removeClass('ok').addClass('error');
                $('#end', '#start').val('');
                Notif.Show('L\'heure de début doit être antérieure à l\'heure de fin', 'error', true, 5000);
            } else if (parseInt(start) < parseInt(end)) {
                $('#end').removeClass('error').addClass('ok');
                $('#start').removeClass('error').addClass('ok');
            }
        }
    });    
});

function frm_create_event_before_submit (element) {
    if ($('#end').val() != "" && $('#start').val() != "") {
        var start = ($('#start').val()).replace(':', '');
        var end = ($('#end').val()).replace(':', '');
        if (start == end) {
            $('#end').removeClass('ok').addClass('error');
            $('#start').removeClass('ok').addClass('error');
            $('#end', '#start').val('');
            Notif.Show('L\'heure de début doit être antérieure à l\'heure de fin', 'error', true, 5000);
            return false;
        } else if (parseInt(start) > parseInt(end)) {
            $('#end').removeClass('ok').addClass('error');
            $('#start').removeClass('ok').addClass('error');
            $('#end', '#start').val('');
            Notif.Show('L\'heure de début doit être antérieure à l\'heure de fin', 'error', true, 5000);
            return false;
        } else if (parseInt(start) < parseInt(end)) {
            $('#end').removeClass('error').addClass('ok');
            $('#start').removeClass('error').addClass('ok');
            return true;
        }
    } else if ($('#end').val() == "" && $('#start').val() == "") {
        $('#end').removeClass('ok').addClass('error');
        $('#start').removeClass('ok').addClass('error');
        return false;
    }
} 
</script>
