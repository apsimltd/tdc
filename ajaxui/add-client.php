<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_client')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<h4 class="onboarding-title">Client</h4>
<div class="onboarding-text">Ajouter un nouveau client</div>
<form class="frm_frm frm_ajax" name="frm_create_client" id="frm_create_client" data-url="<?php echo AJAX_HANDLER ?>/add-client" data-type="json">
	<legend>Information Société</legend>
	<fieldset>
		<label>Nom</label>
		<input class="frm_text must caps" name="nom" placeholder="Nom Société" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Adresse</label>
		<input class="frm_text" name="address" placeholder="Adresse Société" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Téléphone</label>
		<input class="frm_text" name="telephone" placeholder="Téléphone Société" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
	<fieldset>
		<label>Email</label>
		<input class="frm_text" name="email" placeholder="Email Société" type="text" autocomplete="off" data-validation="val_email">
	</fieldset>
    <fieldset class="client-checkbox">
        <input type="checkbox" class="frm_checkbox" id="blacklist" name="blacklist">
        <label class="pointer" for="blacklist">Blacklister</label>
    </fieldset>
	<legend>Information Contact</legend>	
    <fieldset>
		<label>Nom</label>
		<input class="frm_text" name="contact_lastName" placeholder="Nom Contact" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Prénom</label>
		<input class="frm_text" name="contact_firstName" placeholder="Prénom Contact" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Poste</label>
		<input class="frm_text" name="contact_post" placeholder="Poste Contact" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
	<fieldset>
		<label>Téléphone</label>
		<input class="frm_text" name="contact_telephone" placeholder="Téléphone Contact" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
	<fieldset>
		<label>Email</label>
		<input class="frm_text" name="contact_email" placeholder="Email Contact" type="text" autocomplete="off" data-validation="val_email">
	</fieldset>
	<fieldset>
		<button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter Client</button>
	</fieldset>
</form>