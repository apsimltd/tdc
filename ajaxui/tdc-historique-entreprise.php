<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<h4 class="onboarding-title">Historiques Entreprise</h4>

<p style="color: red">Note : la modification du commentaire de l'entreprise se fait dans la fiche de l'entreprise.</p>

<form class="frm_frm frm_ajax" name="frm_tdc_historique_entreprise" id="frm_tdc_historique_entreprise" data-url="<?php echo AJAX_HANDLER ?>/tdc-historique-entreprise" data-type="json">
    
    <input type="hidden" name="mission_id" value="<?php echo get('mission_id') ?>">
    <input type="hidden" name="entreprise_id" value="<?php echo get('entreprise_id') ?>">
    
    <fieldset>
        <textarea name="comment" class="frm_textarea must" id="comment_histo" style="width: 100%; height:150px;" placeholder="Commentaire" data-validation="val_blank"></textarea>                                
    </fieldset>
    
    <fieldset>
        <button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-floppy-disk"></span> Sauvegarder</button>
    </fieldset>
    
</form>

<?php $comments = TDC::getHistoriquesEntreprise(get('mission_id'), get('entreprise_id')) ?>
<?php // debug($comments) ?>

<div class="historiques_comment_wrap">

    <?php if (!empty($comments)): ?>
    <div class="task-threads-wrap clearfix">

        <?php foreach ($comments as $comment): ?>
        <div class="task-thread clearfix">

            <p><?php echo nl2br($comment['comment']) ?></p>

            <div class="publish-info">
                Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo dateToFr($comment['created'], true) ?>
                <span class="glyphicon glyphicon-edit edit_comment_histo_entreprise tooltips" title="Éditer" data-historique_id="<?php echo $comment['id'] ?>"></span>
                <span class="glyphicon glyphicon-trash delete_comment_histo_entreprise tooltips" title="Supprimer" data-historique_id="<?php echo $comment['id'] ?>" data-mission_id="<?php echo $comment['mission_id'] ?>" data-entreprise_id="<?php echo $comment['entreprise_id'] ?>"></span>
            </div>

        </div>
        <?php endforeach; ?>

    </div><!--/ task-threads-wrap -->
    <?php endif; ?>

</div><!-- /historiques_comment_wrap -->
