<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<h4 class="onboarding-title">Entreprise</h4>
<div class="onboarding-text">Ajouter un nouveau entreprise</div>
<form class="frm_frm frm_ajax" name="frm_create_entreprise" id="frm_create_client" data-url="<?php echo AJAX_HANDLER ?>/add-entreprise" data-type="json">
		
    <fieldset>
        <label>Nom</label>
        <input class="frm_text must" name="nom" placeholder="Nom" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>
    <fieldset>
        <label>Téléphone</label>
        <input class="frm_text tel_fr" name="telephone" placeholder="Téléphone" type="text" autocomplete="off" data-validation="val_blank" maxlength="14">
    </fieldset>    
    <fieldset>
        <label>Région</label>
        <input class="frm_text" name="region" placeholder="Région" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>    
    <fieldset>
        <label>Email</label>
        <input class="frm_text" name="email" placeholder="Email" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>
    <fieldset>
        <label>Secteur Activité</label>
        <input class="frm_text" name="secteur_activite" placeholder="Secteur Activité" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset> 
    <fieldset>
        <label>Sous Secteur Activité</label>
        <input class="frm_text" name="sous_secteur_activite" placeholder="Sous Secteur Activité" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>
    <fieldset>
        <label>Commentaire</label>
        <textarea name="commentaire" class="frm_textarea" style="height:150px;" placeholder="Commentaire" data-validation="val_blank"></textarea>
    </fieldset>
	<fieldset>
		<button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter Entreprise</button>
	</fieldset>
</form>