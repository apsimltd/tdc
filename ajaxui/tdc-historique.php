<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<h4 class="onboarding-title">Historiques</h4>

<p style="color: red">Note : la modification du commentaire du candidat ainsi que de ses compétences informatiques se font dans la fiche du candidat.</p>

<form class="frm_frm frm_ajax" name="frm_tdc_historique" id="frm_tdc_historique" data-url="<?php echo AJAX_HANDLER ?>/tdc-historique" data-type="json">
    
    <input type="hidden" name="mission_id" value="<?php echo get('mission_id') ?>">
    <input type="hidden" name="candidat_id" value="<?php echo get('candidat_id') ?>">
    
    <fieldset>
        <textarea name="comment" id="comment_histo" class="frm_textarea must" style="width: 100%; height:150px;" placeholder="Commentaire" data-validation="val_blank"></textarea>                                
    </fieldset>
    
    <fieldset>
        <button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-floppy-disk"></span> Sauvegarder</button>
    </fieldset>
    
</form>

<?php $comments = TDC::getHistoriques(get('mission_id'), get('candidat_id')) ?>
<?php // debug($comments) ?>

<div class="historiques_comment_wrap">

    <?php if (!empty($comments)): ?>
    <div class="task-threads-wrap clearfix">

        <?php foreach ($comments as $comment): ?>
        <div class="task-thread clearfix">

            <p><?php echo nl2br($comment['comment']) ?></p>

            <div class="publish-info">
                Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo dateToFr($comment['created'], true) ?>
                <span class="glyphicon glyphicon-edit edit_comment_histo tooltips" title="Éditer" data-historique_id="<?php echo $comment['id'] ?>"></span>
                <span class="glyphicon glyphicon-trash delete_comment_histo tooltips" title="Supprimer" data-historique_id="<?php echo $comment['id'] ?>" data-mission_id="<?php echo $comment['mission_id'] ?>" data-candidat_id="<?php echo $comment['candidat_id'] ?>"></span>
            </div>

        </div>
        <?php endforeach; ?>

    </div><!--/ task-threads-wrap -->
    <?php endif; ?>

</div><!-- /historiques_comment_wrap -->
