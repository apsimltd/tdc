<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>


<h4 class="onboarding-title">Changement Utilisateur Short-listé</h4>

<form class="frm_frm frm_ajax" name="frm_tdc_change_sl" id="frm_tdc_change_sl" data-url="<?php echo AJAX_HANDLER ?>/tdc-change-sl" data-type="json">
    
    <input type="hidden" name="mission_id" value="<?php echo get('mission_id') ?>">
    <input type="hidden" name="candidat_id" value="<?php echo get('candidat_id') ?>">
    
    <?php $shortliste_added_who = TDC::getWhoShortliste(get('mission_id'), get('candidat_id')); ?>
    <?php $assignedUsers = Mission::getAssignedUsersDetailsMore(get('mission_id')); ?>
    
    <fieldset>
        <label>Utilisateurs</label>
        <select class="frm_chosen must <?php if ($shortliste_added_who['id'] > 0): ?>ok<?php endif; ?>" name="user_id" data-validation="val_blank">
            <option value="">Choisir Utilisateur</option>
            <?php foreach($assignedUsers as $user): ?>
            <option value="<?php echo $user['id'] ?>" <?php if ($user['id'] == $shortliste_added_who['id']): ?>selected="selected"<?php endif; ?>>
                <?php echo $user['name'] ?>
            </option>
            <?php endforeach; ?>
        </select>
    </fieldset>
    
    <fieldset>
        <label>Date SL</label>
        <input class="frm_text must datepicker_yyyy_mm_dd <?php if ($shortliste_added_who['shortliste_added'] <> ""): ?>ok<?php endif; ?>" <?php if ($shortliste_added_who['shortliste_added'] <> ""): ?>value="<?php echo dateYYMMDD($shortliste_added_who['shortliste_added']) ?>"<?php endif; ?> name="shortliste_added" placeholder="Date SL" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>
    
    <fieldset>
        <button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-floppy-disk"></span> Sauvegarder</button>
    </fieldset>
    
</form>