<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>

<?php $suivi = TDC::getSL(get('mission_id'), get('candidat_id')) ?>
<?php $shortliste_added = ($suivi['shortliste_added'] <> "" && $suivi['shortliste'] > 0) ? $suivi['shortliste_added'] : ''; ?>
<?php $shortliste_added_who = TDC::getWhoShortliste(get('mission_id'), get('candidat_id')); ?>
<?php $shortliste_added_who_formatted = ' - ' . mb_ucfirst($shortliste_added_who['firstName']) . ' ' . mb_strtoupper($shortliste_added_who['lastName']); ?>
<?php echo dateYYMMDD($shortliste_added) . $shortliste_added_who_formatted ?> 
<a class="btn btn-icon btn-info tooltips changeSLTrigger" title="Modifier l'utilisateur SL" data-mission_id="<?php echo get('mission_id') ?>" data-candidat_id="<?php echo get('candidat_id') ?>">
    <span class="glyphicon glyphicon-pencil"></span>
</a>