<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_audit')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $log = Watchdog::getById(get('id')); ?>

<h4 class="onboarding-title">Historique Audit</h4>
<div class="onboarding-text"><strong>Module :</strong> <?php echo Watchdog::$audit_modules[$log['module_code']] ?>  <strong>Action :</strong> <?php echo Watchdog::$audit_actions[$log['module_code']][$log['action_code']] ?>  <strong>Par :</strong> <?php echo $log['user_name'] ?>  <strong>Le :</strong> <?php echo $log['created'] ?></div>

<?php if ($log['content_before'] <> "" || $log['content_after'] <> ""): ?>

<?php 
if ($log['content_before'] <> "" && $log['content_after'] <> "")
    $col_size = 6;
elseif ($log['content_before'] <> "" && $log['content_after'] == "")
    $col_size = 12;
elseif ($log['content_before'] == "" && $log['content_after'] <> "")
    $col_size = 12;
?>

<div class="watch_wrapper">
    
    <div class="row">
		
        <?php if ($log['content_before'] <> ""): ?>
        <div class="col col-<?php echo $col_size ?>">
            <div class="block nopadding">
                <div class="block-body">
                    
                    <?php $before = json_decode($log['content_before'], true); //debug($before) ?>
                    <?php if (!empty($before) && is_array($before)): ?>
                    <table class="table-list compact inside-nano">
                        
                        <thead>
                            <tr>
                                <th>Libele</th>
                                <th>valeur</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                        <?php foreach($before as $key => $value): ?>
                            <tr>
                                <td><strong><?php echo $key ?></strong></td>
                                <?php if (is_array($value)): ?>
                                <td><?php debug($value) ?></td>
                                <?php else: ?>
                                <td>
                                    <?php 
                                    if ($key == "created" || $key == "modified" || $key == "updated"):
                                        if (is_numeric($value)) {
                                            echo dateToFr($value, true);
                                        } else {
                                            echo $value;
                                        }
                                    else:
                                        echo $value;
                                    endif;
                                    ?>
                                </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php endif; ?>
                    
                </div><!-- / block-body -->
            </div><!-- / block -->
        </div><!-- / col -->
        <?php endif; ?>
        
        <?php if ($log['content_after'] <> ""): ?>
        <div class="col col-<?php echo $col_size ?>">
            <div class="block nopadding">
                <div class="block-body">
                    
                    <?php $after = json_decode($log['content_after'], true); //debug($before) ?> 
                    <?php if (!empty($after) && is_array($after)): ?>
                    <table class="table-list compact inside-nano">
                        
                        <thead>
                            <tr>
                                <th>Libele</th>
                                <th>valeur</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                        <?php foreach($after as $key => $value): ?>
                            <tr>
                                <td><strong><?php echo $key ?></strong></td>
                                <?php if (is_array($value)): ?>
                                <td><?php debug($value) ?></td>
                                <?php else: ?>
                                <td>
                                    <?php 
                                    if ($key == "created" || $key == "modified" || $key == "updated"):
                                        if (is_numeric($value)) {
                                            echo dateToFr($value, true);
                                        } else {
                                            echo $value;
                                        }
                                    else:
                                        echo $value;
                                    endif;
                                    ?>
                                </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        
                    </table>
                    <?php endif; ?>
                    
                </div><!-- / block-body -->
            </div><!-- / block -->
        </div><!-- / col -->
        <?php endif; ?>
        
    </div><!-- / row -->
    
</div><!-- /watch_wrapper --> 
<?php endif; ?>