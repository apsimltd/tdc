<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php 
$client_id = getPost('client_id');
$missions = Mission::getMissionsByClientId($client_id);
$status = Control::getControlListByType(9);
?>
<?php if (!empty($missions)): ?>
<fieldset class="selectOption largemultiselect">
    <label><span class="pull-left">Missions client</span><span class="pull-right">Mission Affectés</span></label>
    <div class="frm_select_multi_wrapper">
        <select class="frm_select_multi must" data-validation="val_blank" multiple='multiple'>
            <?php foreach($missions as $mission): ?>
                <option value="<?php echo $mission['id'] ?>">
                    <?php echo 'TDC ' .  $mission['id'] . ' - ' . $mission['poste'] . ' - [ ' .  $mission['manager_name'] . ' ] - [ ' . dateToFr($mission['date_debut']) . ' - ' . dateToFr($mission['date_fin']) . ' ] - [ ' . $status[$mission['status']] . ' ]'  ?>
                </option>
            <?php endforeach; ?>
        </select>
        <input type="hidden" name="mission_ids" class="frm_select_multi_helper must" data-validation="val_blank">
    </div>
</fieldset>
<fieldset>
    <button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="3"><span class="glyphicon glyphicon-pencil"></span> Éditer Utilisateur</button>
</fieldset>
<?php else: ?>
<div class="message warning">Aucune mission trouvée pour ce client.</div>
<?php endif; ?>