<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $tdc_id = get('tdc_id') ?>
<?php $tdc = TDC::getTDCById($tdc_id); ?>
<?php $status = Control::getControlListByType(10); ?>

<h4 class="onboarding-title">Statut TDC Entreprise</h4>

<form class="frm_frm frm_ajax" name="frm_tdc_status_entreprise" id="frm_tdc_status_entreprise" data-url="<?php echo AJAX_HANDLER ?>/tdc-status-entreprise" data-type="json">
    
    <input type="hidden" name="mission_id" value="<?php echo get('mission_id') ?>">
    <input type="hidden" name="entreprise_id" value="<?php echo get('entreprise_id') ?>">
    <input type="hidden" name="tdc_id" value="<?php echo get('tdc_id') ?>">
    <input type="hidden" name="status_entreprise_old" value="<?php echo $tdc['status_entreprise'] ?>">
        
    <fieldset>
        <label>Statut</label>
        <select class="frm_chosen must ok" name="status_entreprise" data-validation="val_blank">
            <option value="">Choisir Statut</option>
            <option value="0" <?php if ($tdc['status_entreprise'] == 0): ?>selected="selected"<?php endif; ?>>Aucun</option>
            <?php foreach($status as $key => $value): ?>
            <option value="<?php echo $key ?>" <?php if ($tdc['status_entreprise'] == $key): ?>selected="selected"<?php endif; ?>>
                <?php echo $value ?>
            </option>
            <?php endforeach; ?>
        </select>
    </fieldset>
    
    <fieldset>
        <button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-floppy-disk"></span> Sauvegarder</button>
    </fieldset>
    
</form>