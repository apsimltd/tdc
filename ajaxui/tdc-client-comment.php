<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('tdc', 'TDC_CLIENT'))): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>

<h4 class="onboarding-title">Commentaires</h4>

<form class="frm_frm frm_ajax" name="frm_tdc_client_comment" id="frm_tdc_client_comment" data-url="<?php echo AJAX_HANDLER ?>/add-candidat-comment-tdc-client" data-type="json">
    <input type="hidden" name="candidat_id" value="<?php echo get('candidat_id') ?>">
    <input type="hidden" name="mission_id" value="<?php echo get('mission_id') ?>">
    <fieldset>
        <textarea name="comment" id="tdc-candidat-client-comment" class="frm_textarea must" style="width: 100%; height:250px;" placeholder="Ajouter un nouveau commentaire" data-validation="val_blank"></textarea>                          
    </fieldset>

    <fieldset>
        <button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter commentaire</button>
    </fieldset>

</form>

<?php $comments = TDC::getCommentClient(get('mission_id'), get('candidat_id')); // debug($comments); ?>

<div class="historiques_comment_wrap">

    <?php if (!empty($comments)): ?>
    <div class="task-threads-wrap clearfix">

        <?php foreach ($comments as $comment): ?>
        <div class="task-thread clearfix">

            <p><?php echo nl2br($comment['comment']) ?></p>

            <div class="publish-info">
                Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo $comment['created'] ?>
            </div>

        </div>
        <?php endforeach; ?>

    </div><!--/ task-threads-wrap -->
    <?php else: ?>
    <div class="message warning">Aucun Commentaire</div>
    <?php endif; ?>

</div><!-- /historiques_comment_wrap -->