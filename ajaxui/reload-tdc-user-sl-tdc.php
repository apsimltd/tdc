<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>

<?php $suivi = TDC::getSL(get('mission_id'), get('candidat_id')) ?>
<?php
$shortliste = ($suivi['shortliste'] > 0) ? '<span class="status active">Oui</span>' : '<span class="status inactive">Non</span>';
$shortliste_added = ($suivi['shortliste_added'] <> "" && $suivi['shortliste'] > 0) ? $suivi['shortliste_added'] : '';
if($suivi['shortliste_added'] <> "" && $suivi['shortliste'] > 0) {
    $shortliste_added_who = TDC::getWhoShortliste(get('mission_id'), get('candidat_id'));
    $shortliste_added_who_formatted = ' - ' . mb_ucfirst($shortliste_added_who['firstName']) . ' ' . mb_strtoupper($shortliste_added_who['lastName']);
} else {
    $shortliste_added_who_formatted = "";
}

echo $shortliste .  ' ' . dateYYMMDD($shortliste_added) . $shortliste_added_who_formatted;
?>