<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_client')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $client = Client::getClientById(get('id')); ?>


<h4 class="onboarding-title">Client</h4>
<div class="onboarding-text">Éditer client</div>
<form class="frm_frm frm_ajax" name="frm_edit_client" id="frm_edit_client" data-url="<?php echo AJAX_HANDLER ?>/edit-client" data-type="json">
	<legend>Information Société</legend>
    <input type="hidden" name="id" value="<?php echo $client['id'] ?>">
	<fieldset>
		<label>Nom</label>
        <input class="frm_text must caps ok" value="<?php echo $client['nom'] ?>" name="nom" placeholder="Nom Société" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Adresse</label>
		<input class="frm_text <?php if ($client['address'] != ""): ?>ok<?php endif; ?>" value="<?php echo $client['address'] ?>" name="address" placeholder="Adresse Société" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Téléphone</label>
		<input class="frm_text <?php if ($client['telephone'] != ""): ?>ok<?php endif; ?>" value="<?php echo $client['telephone'] ?>" name="telephone" placeholder="Téléphone Société" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
	<fieldset>
		<label>Email</label>
		<input class="frm_text <?php if ($client['email'] != ""): ?>ok<?php endif; ?>" value="<?php echo $client['email'] ?>" name="email" placeholder="Email Société" type="text" autocomplete="off" data-validation="val_email">
	</fieldset>
    <fieldset class="client-checkbox">
        <input type="checkbox" class="frm_checkbox" id="blacklist" <?php if ($client['blacklist'] == 1): ?>checked="checked"<?php endif; ?> name="blacklist">
        <label class="pointer" for="blacklist">Blacklister</label>
    </fieldset>
	<legend>Information Contact</legend>	
    <fieldset>
		<label>Nom</label>
		<input class="frm_text <?php if ($client['contact_lastName'] != ""): ?>ok<?php endif; ?>" value="<?php echo $client['contact_lastName'] ?>" name="contact_lastName" placeholder="Nom Contact" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Prénom</label>
		<input class="frm_text <?php if ($client['contact_firstName'] != ""): ?>ok<?php endif; ?>" value="<?php echo $client['contact_firstName'] ?>" name="contact_firstName" placeholder="Prénom Contact" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Poste</label>
		<input class="frm_text <?php if ($client['contact_post'] != ""): ?>ok<?php endif; ?>" value="<?php echo $client['contact_post'] ?>" name="contact_post" placeholder="Poste Contact" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
	<fieldset>
		<label>Téléphone</label>
		<input class="frm_text <?php if ($client['contact_telephone'] != ""): ?>ok<?php endif; ?>" value="<?php echo $client['contact_telephone'] ?>" name="contact_telephone" placeholder="Téléphone Contact" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
	<fieldset>
		<label>Email</label>
		<input class="frm_text <?php if ($client['contact_email'] != ""): ?>ok<?php endif; ?>" value="<?php echo $client['contact_email'] ?>" name="contact_email" placeholder="Email Contact" type="text" autocomplete="off" data-validation="val_email">
	</fieldset>
	<fieldset>
		<button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-pencil"></span> Éditer Client</button>
	</fieldset>
</form>