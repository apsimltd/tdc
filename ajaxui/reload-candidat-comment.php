<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $comments = Candidat::getCommentsNew(get('id')); ?>

<?php if (!empty($comments)): ?>
<div class="task-threads-wrap clearfix">

    <?php foreach ($comments as $comment): ?>
    <div class="task-thread clearfix">

        <p><?php echo nl2br($comment['comment']) ?></p>

        <div class="publish-info">
            Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo dateToFr($comment['created'], true) ?>
            <span class="glyphicon glyphicon-edit edit_comment tooltips" title="Éditer" data-comment_id="<?php echo $comment['id'] ?>" data-candidat_id="<?php echo $comment['candidat_id'] ?>" data-mission_id="<?php echo ($comment['mission_id'] == 0) ? get('tdc_id') : $comment['mission_id']; ?>"></span>
            <span class="glyphicon glyphicon-trash delete_comment tooltips" title="Supprimer" data-comment_id="<?php echo $comment['id'] ?>" data-candidat_id="<?php echo $comment['candidat_id'] ?>"></span>
        </div>
        
        <?php if ($comment['mission_id'] <> ""): ?>
        <div class="publish-info">
            Mission : <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $comment['mission_id'] ?>" target="_blank"><span class="glyphicon glyphicon-screenshot"></span> <?php echo $comment['mission'] . ' - ' . $comment['client'] ?></a>
        </div>
        <?php endif; ?>

    </div>
    <?php endforeach; ?>

</div><!--/ task-threads-wrap -->
<?php endif; ?>