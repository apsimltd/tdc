<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('read_task', 'read_all_task', 'create_task'))): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>

<?php if (get('id')): ?>
    
    <?php  
    // check if the user has the right to see this task
    if (!Task::cansee($me['id'], get('id'))): ?>
        <script>window.location.href = BASE_URL;</script>
    <?php endif; ?>

    <?php $task = Task::getTaskDetailsById(get('id')) ?>
    <?php // debug($task); ?>
    <div class="row">

        <div class="col col-12">
            <div class="block nopadding taskView">

                <div class="block-head with-border">
                    <div class="task-check tooltips <?php if ($task['status'] == 0): ?>completed<?php endif; ?>" title="<?php if ($task['status'] == 1): ?>Terminé<?php else: ?>Incomplet<?php endif; ?>" data-task_id="<?php echo $task['id'] ?>">
                        <svg viewBox="0 0 32 32"><polygon points="27.672,4.786 10.901,21.557 4.328,14.984 1.5,17.812 10.901,27.214 30.5,7.615 "></polygon></svg>
                    </div>
                    <span class="title"><?php echo mb_ucfirst($task['title']) ?></span>
                    <span class="os-icon os-icon-close pull-right tooltips close_task" title="Fermer"></span>
                </div>

                <div class="block-body">

                    <div class="task-main clearfix">
                        <p><?php echo $task['description'] ?></p>
                        <div class="publish-info">Créé par <strong><?php echo mb_ucfirst($task['creator']['firstName']) . ' ' . mb_strtoupper($task['creator']['lastName']) ?></strong> le <?php echo dateToFr($task['created'], true) ?></div>
                        <div class="publish-info">Affecté à <strong><?php echo mb_ucfirst($task['assignee']['firstName']) . ' ' . mb_strtoupper($task['assignee']['lastName']) ?></strong></div>
                    </div><!-- /task-main -->
                    
                    <?php if (!empty($task['threads'])): ?>
                    <div class="task-threads-wrap clearfix">
                        
                        <?php foreach ($task['threads'] as $thread): ?>
                        <div class="task-thread clearfix">

                            <p><?php echo $thread['comment'] ?></p>

                            <div class="publish-info">
                                Créé par <strong><?php echo mb_ucfirst($thread['firstName']) . ' ' . mb_strtoupper($thread['lastName']) ?></strong> le <?php echo dateToFr($thread['created'], true) ?>
                            </div>

                        </div>
                        <?php endforeach; ?>

                    </div><!--/ task-threads-wrap -->
                    <?php endif; ?>
                    
                    <form class="frm_frm frm_ajax" <?php if ($task['status'] == 1): ?>style="display:block;"<?php endif ?> name="frm_add_task_thread" id="frm_add_task_thread" data-url="<?php echo AJAX_HANDLER ?>/add-task-thread" data-type="json">
                        <input type="hidden" name="task_id" value="<?php echo $task['id'] ?>">
                        <fieldset>
                            <textarea name="comment" class="frm_textarea must" placeholder="Ajouter commentaire" data-validation="val_blank"></textarea>                                
                        </fieldset>

                        <fieldset>
                            <button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter Commentaire</button>
                        </fieldset>

                    </form>

                </div><!-- / block-body -->

            </div>
        </div><!-- /col -->

    </div><!-- / row -->
<?php endif; ?>
