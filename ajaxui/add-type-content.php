<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_type_content', 'edit_type_content')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $edit = false; ?>
<?php $type = Control::getTypeById(get('type_id')); ?>
<?php if (get('control_id') != ""): $edit = true; $control = Control::getControlById(get('control_id')); endif; ?>

<h4 class="onboarding-title"><?php echo $type['description'] ?></h4>
<div class="onboarding-text">
    <?php if($edit): ?>Éditer<?php else: ?>Ajouter<?php endif; ?> <?php echo $type['description'] ?>
</div>

<form class="frm_frm frm_ajax" name="frm_create_type_content" id="frm_create_type_content" data-url="<?php echo AJAX_HANDLER ?>/add-type-content" data-type="json">
    <input type="hidden" name="type_id" value="<?php echo $type['id'] ?>">
    <input type="hidden" name="type" value="<?php echo $type['description'] ?>">
    <?php if ($edit): ?><input type="hidden" name="id" value="<?php echo get('control_id') ?>"><?php endif; ?>
	<fieldset>
		<label><?php echo $type['description'] ?></label>
        <input class="frm_text must <?php if ($edit): ?>ok<?php endif; ?>" <?php if ($edit): ?>value="<?php echo $control['name'] ?>"<?php endif; ?> name="name" placeholder="<?php echo $type['description'] ?>" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>    
    <fieldset>
		<label>Statut</label>
		<select class="frm_chosen must ok" name="status" data-validation="val_blank">
            <option value="1" <?php if ($edit && $control['status'] == 1): ?>selected="selected"<?php endif; ?>>Actif</option>
			<option value="0" <?php if ($edit && $control['status'] == 0): ?>selected="selected"<?php endif; ?>>Inactif</option>
		</select>
	</fieldset>
	<fieldset>
		<button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><?php if($edit): ?><span class="glyphicon glyphicon-pencil"></span> Éditer<?php else: ?><i class="ico-txt fa fa-plus"></i> Ajouter<?php endif; ?> <?php echo $type['description'] ?></button>
	</fieldset>
</form>

