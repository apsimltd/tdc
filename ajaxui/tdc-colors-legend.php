<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php
$colors = TDC::getColorsLegend(get('mission_id'));
if (empty($colors)) {
    TDC::AddColorsLegend(get('mission_id'));
    $colors = TDC::getColorsLegend(get('mission_id'));
}
?>
<h4 class="onboarding-title">Légende Couleurs Candidats</h4>

<form class="frm_frm frm_ajax" name="frm_tdc_colors_legende" id="frm_tdc_colors_legende" data-url="<?php echo AJAX_HANDLER ?>/tdc-colors-legend" data-type="json">
    
    <?php foreach($colors as $color): ?>
    <fieldset>
        <label>
            <span style="display:inline-block;width:100%; height: 32px;background-color:#<?php echo $color['hex'] ?>"></span>
        </label>
        <input type="hidden" name="id[]" value="<?php echo $color['id'] ?>">
        <input class="frm_text <?php if ($color['description'] != ""): ?>ok<?php endif; ?>" value="<?php echo $color['description'] ?>" name="desc[]" placeholder="legende" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>
    <?php endforeach; ?>
    
    <fieldset>
        <button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-floppy-disk"></span> Sauvegarder</button>
    </fieldset>
    
</form>