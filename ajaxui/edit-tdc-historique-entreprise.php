<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $comment = TDC::getHistoriqueById(get('historique_id')); ?>

<h4 class="onboarding-title">Éditer Commentaire</h4>
<form class="frm_frm frm_ajax" name="frm_tdc_historique_edit_entreprise" id="frm_tdc_historique_edit_entreprise" data-url="<?php echo AJAX_HANDLER ?>/edit-tdc-historique-entreprise" data-type="json">
    <input type="hidden" name="mission_id" value="<?php echo $comment['mission_id'] ?>">
    <input type="hidden" name="entreprise_id" value="<?php echo $comment['entreprise_id'] ?>">
    <input type="hidden" name="id" value="<?php echo $comment['id'] ?>">
    
    <fieldset>
        <textarea name="comment" class="frm_textarea must ok" style="width: 100%; height: 250px;" data-validation="val_blank"><?php echo $comment['comment'] ?></textarea>                                
    </fieldset>

    <fieldset>
        <button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-pencil"></span> Éditer Historique</button>
    </fieldset>

</form>