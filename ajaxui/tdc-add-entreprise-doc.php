<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<h4 class="onboarding-title">Ajouter Documents Entreprise</h4>

<form class="frm_frm" name="frm_tdc_add_entreprise_doc" id="frm_tdc_add_entreprise_doc" action="<?php echo AJAX_HANDLER ?>/add-entreprise-tdc-file" method="POST" enctype="multipart/form-data">
        
    <input type="hidden" name="mission_id" value="<?php echo get('mission_id') ?>">
    <input type="hidden" name="entreprise_id" value="<?php echo get('entreprise_id') ?>">
    <input type="hidden" name="tdc_line_id" value="<?php echo get('tdc_line_id') ?>">
    
    <fieldset class="uploadMultiple">
        <div class="clearfix">
            <label class="pull-left">Documents <span class="glyphicon glyphicon-info-sign tooltips" title="Les fichiers autorisés sont : .doc, .docx, .xls, .xlsx, .pdf, .jpg, .jpeg, .png ; Taille max : 10 Mo"></span></label>
            <input type="hidden" name="uploadFlag" id="uploadFlag_cv" value="1">
            <div class="clearfix add_field_wrap_documents cv pull-left" style="width: 70%;">
                <div class="add_field_row pull-left">
                    <input type="file" name="file[]" id="filecv_1" data-number="1" class="inputfilecv cv inputfile-1">
                    <label id="uploadLabelcv_1" for="filecv_1" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename cv">Choisir un fichier</span></label>
                    <button type="button" class="clear_field_upload_cv btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
            </div>
        </div>
        <div class="mt6 clearfix">
            <button type="button" class="add_field_upload_cv btn btn-icon btn-success pull-right tooltips" title="Ajouter un nouveau fichier"><span class="glyphicon glyphicon-plus"></span></button>
        </div>                              
    </fieldset>
    
    <fieldset>
        <button type="button" class="btn btn-primary frm_submit frm_before_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter Document/s</button>
    </fieldset>
    
</form>
<script>
$(document).ready(function(){
    
    UploadCV.Init();
    UploadCV.Validate(); 
    
    // add a new field for upload
    $(document).on('click', 'button.add_field_upload_cv', function(){
        
        // get the last input number
        var number = parseInt($('.add_field_wrap_documents.cv .add_field_row:last input.inputfilecv.cv').attr('data-number')) + 1;
        
        var inputHtml = '<input type="file" name="file[]" id="filecv_'+number+'" data-number="'+number+'" class="inputfilecv cv inputfile-1"><label id="uploadLabelcv_'+number+'" for="filecv_'+number+'" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename cv">Choisir un fichier</span></label><button type="button" class="clear_field_upload_cv btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>';
        var newFieldHtml = '<div class="add_field_row clearfix">'+inputHtml+'</div>';
        $('.add_field_wrap_documents.cv').append(newFieldHtml);
        
        var flagNumber = parseInt($('#uploadFlag_cv').val()) + 1;
        $('#uploadFlag_cv').val(flagNumber);
        
        UploadCV.Init();
    });
    
    $(document).on('click', '.clear_field_upload_cv', function(){
        
        var numRows = parseInt($('fieldset.uploadMultiple .add_field_wrap_documents.cv .add_field_row:last').index()) + 1;
        
        if (numRows >= 2) {
            $(this).parent('.add_field_row').remove();
        } else if (numRows == 1) {
            $(this).parent('.add_field_row').find('input.inputfilecv.cv').val('');
            $(this).parent('.add_field_row').find('label').html('<span class="glyphicon glyphicon-open"></span> <span class="filename cv">Choisir un fichier</span>');
        }
        
    });
         
}); // end document ready function

var UploadCV = {
    Init: function (){
        $('.inputfilecv.cv').each(function(){
		
            var $input	 = $( this ),
                $label	 = $input.next( 'label' ),
                labelVal = $label.html();

            $input.on( 'change', function( e )
            {
                var fileName = '';

                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else if( e.target.value )
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    $label.find( 'span.filename.cv' ).html( fileName );
                else
                    $label.html( labelVal );
            });

            // Firefox bug fix
            $input
            .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
            .on( 'blur', function(){ 
                $input.removeClass( 'has-focus' ); 
            });

        });
    },
    Validate: function (){
        $(document).on('change', '.inputfilecv.cv', function(){
              UploadCV.ValidateUpload($(this));            
        });
    },
    ValidateUpload: function (input) {
        var file_data = input.prop("files")[0];
        // start validate file extension
        var validExtensions = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx', 'xls', 'xlsx']; //array of valid extensions
        var filename = file_data.name;
        var ext = filename.substr(filename.lastIndexOf('.') + 1);
        if ($.inArray(ext, validExtensions) == -1){
            Notif.Show('Fichier non valide', 'error', true, 4000);
            input.val("");
            input.next('label').removeClass('ok').addClass('error');
            return false;
        } else {
            // check file size
            // 4MB allowed
            var filesize = file_data.size;
            if (filesize > 10485760) {
                Notif.Show('La taille du fichier est supérieure à celle autorisée. Télécharger moins de 10 Mo.', 'error', true, 4000);
                input.val("");
                input.next('label').removeClass('ok').addClass('error');
                return false;
            } else {
                input.next('label').removeClass('error').addClass('ok');
                return true;
            }
        }
        // end validation file extension

    },
}


function frm_tdc_add_entreprise_doc_before_submit(obj) {
    
    var uploadFlag = true;
    var numLabel = parseInt($('#uploadFlag_cv').val());
    
    for(var i = 1; i <= numLabel; i++) {
        
        if ($('#filecv_'+numLabel).val() == ""){
            $('#uploadLabelcv_'+numLabel).addClass('error');
            uploadFlag = false;
        }
        
        if ($('#uploadLabelcv_'+numLabel).length) {
            if ($('#uploadLabelcv_'+numLabel).hasClass('error')) {
                uploadFlag = false;
            }
        }
    }
    
    return uploadFlag;
    
}
</script>