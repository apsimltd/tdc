<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_task')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<h4 class="onboarding-title">Tâche</h4>
<div class="onboarding-text">Ajouter une tâche</div>
<form class="frm_frm frm_ajax" name="frm_create_task" id="frm_create_task" data-url="<?php echo AJAX_HANDLER ?>/add-task" data-type="json">
	<fieldset>
		<label>Titre</label>
		<input class="frm_text must" name="title" placeholder="Titre" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
    	<label>Description</label>
        <textarea name="description" class="frm_textarea must" placeholder="Description" data-validation="val_blank"></textarea>                                
    </fieldset>
	<fieldset>
		<label>Affectée à</label>
		<select class="frm_chosen must" name="assignee_id" data-validation="val_blank">
        	<option value="">Choisir Utilisateur</option>
            <?php $users = User::getUsersByTLId($me['id']); ?>
            <?php foreach($users as $user): ?>
            <option value="<?php echo $user['id'] ?>"><?php echo mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName']) ?></option>
            <?php endforeach; ?>
		</select>
	</fieldset>
	<fieldset>
		<button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter Tâche</button>
	</fieldset>
</form>

