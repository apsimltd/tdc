<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $links = Candidat::getLinks(get('id')); ?>

<?php if (!empty($links)) :?>
    <ul class="links_candidat">
    <?php foreach($links as $link): ?>
        <li>
            <a href="<?php echo $link['link'] ?>" target="_blank"><?php echo $link['link'] ?></a>
            <span class="glyphicon glyphicon-trash delete_link tooltips" title="Supprimer" data-link_id="<?php echo $link['id'] ?>" data-candidat_id="<?php echo $link['candidat_id'] ?>"></span>
        </li>
    <?php endforeach;?>
    </ul>
<?php endif; ?>

<form class="frm_frm frm_ajax" name="frm_edit_candidat_add_link" id="frm_edit_candidat_add_link" data-url="<?php echo AJAX_HANDLER ?>/add-candidat-link" data-type="json">
    <input type="hidden" name="candidat_id" value="<?php echo get('id') ?>">

    <fieldset class="socialMedia">
        <div class="clearfix">
            <label class="pull-left">Réseaux sociaux <span class="glyphicon glyphicon-info-sign tooltips" title="Un lien ex. www.linkedin.com/bastien"></span></label>
            <div class="clearfix add_field_wrap_social pull-left" style="width: 60%;">
                <div class="add_field_row clearfix">
                    <input class="frm_text must socialmedia_link val_link" name="reseauSociaux[]" placeholder="Entrez le lien" type="text" autocomplete="off" data-validation="val_link">
                </div>
            </div>
        </div>
        <div class="mt6 clearfix">
            <button type="button" class="add_field_social btn btn-icon btn-success pull-right tooltips" title="Ajouter un nouveau lien"><span class="glyphicon glyphicon-plus"></span></button>
        </div>
    </fieldset>

    <fieldset>
        <button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter lien</button>
    </fieldset>

</form>