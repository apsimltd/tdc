<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_candidat') || get('candidat_id') == ""): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $suivis = Candidat::getSuivis(get('candidat_id')); ?>
<?php $status = Control::getControlListByType(9); ?>
<?php $statusCandidat = Control::getControlListByType(8); ?>
<?php // debug($suivis) ?>

<h4 class="onboarding-title">Historiques Missions</h4>
<!--<table class="datable stripe hover">-->
<table class="table-list compact fixed-actions">
    <thead>
        <tr>
            <th>Mission</th>
            <th>Statut Mission</th>
            <th>Client</th>
            <th>Poste</th>
            <th>Prise de contact</th>
            <th>Date rencontre consultant</th>
            <th>Statut Candidat</th>
            <th>Short-listé</th>
            <th>Placé</th>
            <th>Suivi du statut</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($suivis as $suivi): ?>
        <?php $mission = Mission::getMissionById($suivi['mission_id']); ?>
        <tr>
            <td align="center"><a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $suivi['mission_id'] ?>"><?php echo $suivi['mission_id'] ?></a></td>
            <td align="center" style="width: 100px;">
                <span class="status <?php echo getClassStatusMission($mission['status']) ?>">
                    <?php echo $status[$mission['status']] ?>
                </span>
            </td>
            <td><?php echo $suivi['client'] ?></td>
            <td><?php echo $suivi['poste'] ?></td>
            <td>
                <?php 
                if ($suivi['date_prise_de_contact'] > 0) {
                    echo dateToFr($suivi['date_prise_de_contact']);
                }
                ?>
            </td>
            <td>
                <?php 
                if ($suivi['date_rencontre_consultant'] > 0) {
                    echo dateToFr($suivi['date_rencontre_consultant']);
                }
                ?>
            </td>
            <td align="center" style="width: 50px;">
                <?php $candidatTDC = TDC::getCandidatesByTDC($suivi['mission_id'], array($suivi['candidat_id'])) ?>
                <?php if ($candidatTDC[0]['control_statut_tdc'] > 0): ?>
                <span class="status <?php echo getClassStatusTDC($candidatTDC[0]['control_statut_tdc']) ?>">
                    <?php echo $statusCandidat[$candidatTDC[0]['control_statut_tdc']] ?>
                </span>
                <?php endif; ?>
            </td>
            <td align="center">
                <span class="status <?php if($suivi['shortliste'] == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
                    <?php if($suivi['shortliste'] == 1): ?>Oui<?php else: ?>Non<?php endif; ?>
                </span>
            </td>
            <td align="center">
                <span class="status <?php if($suivi['place'] == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
                    <?php if($suivi['place'] == 1): ?>Oui<?php else: ?>Non<?php endif; ?>
                </span>
            </td>
            <td>
                <?php
                if ($suivi['comment'] != "")
                    echo nl2br($suivi['comment']);
                ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>