<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $suivi = TDC::getSuivi(get('mission_id'), get('candidat_id')) ?>
<?php $tdc = TDC::getTDCById(get('tdc_id')); // debug($tdc); ?>
<?php $prestataire_id = get('prestataire_id'); ?>
<?php // debug($tdc) ?>

<?php
// the user must put a status first
$disabled = '';
if ($tdc['control_statut_tdc'] == 0)
    $disabled = 'disabled="disabled"';
?>

<h4 class="onboarding-title">Suivis</h4>

<?php if ($tdc['control_statut_tdc'] == 0): ?>
<div class="message warning">
    Vous ne pouvez pas ajouter de suivi pour ce candidat car il n'a pas de statut.
</div>
<?php endif; ?>

<form class="frm_frm frm_ajax" name="frm_tdc_suivi" id="frm_tdc_suivi" data-url="<?php echo AJAX_HANDLER ?>/tdc-suivi" data-type="json">
    
    <input type="hidden" name="mission_id" value="<?php echo get('mission_id') ?>">
    <input type="hidden" name="candidat_id" value="<?php echo get('candidat_id') ?>">
    <input type="hidden" name="id" value="<?php echo $suivi['id'] ?>">
    <input type="hidden" name="tdc_id" id="tdc_id_value" value="<?php echo get('tdc_id') ?>">
    
    <fieldset>
        <label>Date de prise de contact</label>
        <input class="frm_text datepicker <?php if($suivi['date_prise_de_contact'] > 0): ?>ok<?php endif; ?>" <?php if($suivi['date_prise_de_contact'] > 0): ?>value="<?php echo dateToFr($suivi['date_prise_de_contact']) ?>"<?php endif; ?> name="date_prise_de_contact" placeholder="Date de prise de contact" type="text" autocomplete="off" data-validation="val_blank" <?php echo $disabled ?>>
    </fieldset>
    
    <fieldset>
        <label>Date de présentation</label>
        <input class="frm_text datepicker <?php if($suivi['date_presentation'] > 0): ?>ok<?php endif; ?>" <?php if($suivi['date_presentation'] > 0): ?>value="<?php echo dateToFr($suivi['date_presentation']) ?>"<?php endif; ?> name="date_presentation" placeholder="Date de présentation" type="text" autocomplete="off" data-validation="val_blank" <?php echo $disabled ?>>
    </fieldset>
    
    
    <?php if ($prestataire_id == 1): //only for prestataire OPSEARCH ?>
    <fieldset>
        <label>Date de rencontre consultant</label>
        <input class="frm_text datepicker <?php if($suivi['date_rencontre_consultant'] > 0): ?>ok<?php endif; ?>" <?php if($suivi['date_rencontre_consultant'] > 0): ?>value="<?php echo dateToFr($suivi['date_rencontre_consultant']) ?>"<?php endif; ?> name="date_rencontre_consultant" placeholder="Date de rencontre consultant" type="text" autocomplete="off" data-validation="val_blank" <?php echo $disabled ?>>
    </fieldset>
    <?php endif; ?>
    
    <fieldset>
        <label>Date de rencontre client</label>
        <input class="frm_text datepicker <?php if($suivi['date_rencontre_client'] > 0): ?>ok<?php endif; ?>" <?php if($suivi['date_rencontre_client'] > 0): ?>value="<?php echo dateToFr($suivi['date_rencontre_client']) ?>"<?php endif; ?> name="date_rencontre_client" id="date_rencontre_client_suivi" placeholder="Date de rencontre client" type="text" autocomplete="off" data-validation="val_blank" <?php echo $disabled ?>>
    </fieldset>
    
    <fieldset>
        <label>Date retour après rencontre</label>
        <input class="frm_text datepicker <?php if($suivi['date_retour_apres_rencontre'] > 0): ?>ok<?php endif; ?>" <?php if($suivi['date_retour_apres_rencontre'] > 0): ?>value="<?php echo dateToFr($suivi['date_retour_apres_rencontre']) ?>"<?php endif; ?> name="date_retour_apres_rencontre" placeholder="Date retour après rencontre" type="text" autocomplete="off" data-validation="val_blank" <?php echo $disabled ?>>
    </fieldset>
    
    <fieldset class="single-row">                                   
        <input type="checkbox" class="frm_checkbox" id="shortliste" name="shortliste" <?php if($suivi['shortliste'] > 0): ?>checked="checked"<?php endif; ?> <?php echo $disabled ?>>
        <label class="pointer" for="shortliste">Short-listé</label>
        <?php ///if($suivi['shortliste_added'] <> "" && $suivi['shortliste'] > 0) : ?>
        <?php if($suivi['shortliste'] > 0) : ?>
            <?php $shortliste_added = ($suivi['shortliste_added'] <> "" && $suivi['shortliste'] > 0) ? dateYYMMDD($suivi['shortliste_added']) : ''; ?>
            <?php $shortliste_added_who = TDC::getWhoShortliste(get('mission_id'), get('candidat_id')); ?>
            <?php $shortliste_added_who_formatted = ' - ' . mb_ucfirst($shortliste_added_who['firstName']) . ' ' . mb_strtoupper($shortliste_added_who['lastName']); ?>
        <div class="changeSL">
            <?php echo $shortliste_added . $shortliste_added_who_formatted ?> 
            <a class="btn btn-icon btn-info tooltips changeSLTrigger" title="Modifier l'utilisateur SL" data-mission_id="<?php echo get('mission_id') ?>" data-candidat_id="<?php echo get('candidat_id') ?>">
                <?php if (User::isMissionManager() || User::isSA() || User::isAdmin()): ?>
                <span class="glyphicon glyphicon-pencil"></span>
                <?php endif; ?>
            </a>
        </div>
        <?php endif; ?>
    </fieldset>
    <input type="hidden" name="shortliste_old" value="<?php echo $suivi['shortliste'] ?>">
    <input type="hidden" name="shortliste_old_addded" id="shortliste_old_addded" value="<?php echo $suivi['shortliste_added'] ?>">
    <fieldset class="single-row">
        <input type="checkbox" class="frm_checkbox" id="place" name="place" <?php if($suivi['place'] > 0): ?>checked="checked"<?php endif; ?> <?php echo $disabled ?>>
        <label class="pointer" for="place">Placé</label>
    </fieldset>
    
    <?php // used for kpi cdr to know to which cdr this candidat is assigned ?>
    <?php /// if ($tdc['control_statut_tdc'] == 229 && (User::isMissionManager() || User::isSA() || User::isAdmin())): // status IN => modified to all status ?>
    <?php if (User::isMissionManager() || User::isSA() || User::isAdmin()): // status IN => modified to all status ?>
        <?php $users_in_mission = TDC::getTDCCandidatAssignedUsers(get('mission_id')); ?>
        <?php // debug($users_in_mission); ?>
        <fieldset>
            <label>KPI CDR Affectés à</label>
            <select class="frm_chosen" name="user_id_kpi_assigned" data-validation="val_blank">
                <option value="">Choisir KPI CDR</option>
                <?php foreach($users_in_mission as $user): ?>
                <option value="<?php echo $user['id'] ?>" <?php if ($tdc['user_id_kpi_assigned'] == $user['id']): ?>selected="selected"<?php endif; ?>>
                    <?php echo mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName']) . ' - [' . $user['role_name'] . ']' ?>
                </option>
                <?php endforeach;?>
            </select>
        </fieldset>
    <?php endif; ?>
    
    <fieldset>
        <textarea name="comment" class="frm_textarea <?php if($suivi['comment'] != ""): ?>ok<?php endif; ?>" style="width: 100%; height:150px;" placeholder="Suivi du statut" data-validation="val_blank" <?php echo $disabled ?>><?php echo $suivi['comment'] ?></textarea>                                
    </fieldset>
    
    <?php if ($tdc['control_statut_tdc'] > 0): ?>
    <fieldset>
        <button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-floppy-disk"></span> Sauvegarder</button>
    </fieldset>
    <?php endif; ?>
    
</form>
<script>
$(document).ready(function(){    
    // check shortlister automatically when a date is entered in date rencontre client
    // but do not remove the date when shortlister is unchecked
    // asked by Bastien on 2020-04-20 to remove it as it was previously asked by Emeline
//    $('#date_rencontre_client_suivi').on().datepicker({
//        dateFormat: 'yy-mm-dd', 
//        regional: 'fr',
//        // maxDate: 0, 
//        yearRange: '-20:+0',
//        changeMonth: true,
//        changeYear: true,
//        showButtonPanel: false,
//        onClose: function(selectedDate) {
//            if (selectedDate != "") {
//                $('#shortliste').iCheck('check');
//                $('#shortliste').iCheck('update');
//            } else {
//                
//            }
//        }
//    });
    
});
</script>