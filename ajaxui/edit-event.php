<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_event', 'delete_event')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $event = Event::getEventById(get('id')); ?>
<?php 
// check if event completed or not
// if yes we lock the element and view mode only
// and if user has right to delete he/she can delete
if (get('lock') == 'true') {
    $locked = true;
} elseif (get('lock') == 'false') {
    $locked = false;
}
?>

<h4 class="onboarding-title">Événements</h4>
<div class="onboarding-text"><?php if ($locked): ?><strong style="color: #dd4b39;">Événement terminé</strong><?php else: ?>Éditer événement<?php endif; ?></div>
<form class="frm_frm frm_ajax" name="frm_create_event" id="frm_create_event" data-url="<?php echo AJAX_HANDLER ?>/edit-event" data-type="json">
    <input type="hidden" name="id" value="<?php echo $event['id'] ?>">
    <fieldset>
		<label>Événement</label>
        <input class="frm_text must ok" value="<?php echo $event['title'] ?>" name="title" placeholder="Description evénement" type="text" autocomplete="off" data-validation="val_blank" <?php if ($locked): ?>disabled="disabled"<?php endif; ?>>
	</fieldset>
    <fieldset>
		<label>Type Événement</label>
		<select class="frm_chosen must ok" name="eventType_id" data-validation="val_blank" <?php if ($locked): ?>disabled="disabled"<?php endif; ?>>
        	<option value="">Choisir Type Événement</option>
            <?php $types = Event::getTypes() ?>
            <?php foreach($types as $type): ?>
			<option value="<?php echo $type['id'] ?>" <?php if ($event['eventType_id'] == $type['id']): ?>selected="selected"<?php endif; ?>>
                <?php echo $type['name'] ?>
            </option>
			<?php endforeach; ?>
		</select>
	</fieldset>
	<fieldset>
		<label>Date</label>
        <input class="frm_text datepicker must ok" name="date" value="<?php echo $event['date'] ?>" placeholder="Date" type="text" autocomplete="off" data-validation="val_blank" <?php if ($locked): ?>disabled="disabled"<?php else: ?>readonly="readonly"<?php endif; ?>>
	</fieldset>
    <fieldset>
		<label>Heure de début</label>
        <input class="frm_text must ok time" id="start" value="<?php echo date('H:i', ($event['start'])/1000) ?>" name="start" placeholder="Heure de début" type="text" autocomplete="off" <?php if ($locked): ?>disabled="disabled"<?php endif; ?>>
	</fieldset>
	<fieldset>
		<label>Heure de fin</label>
        <input class="frm_text must ok time" id="end" value="<?php echo date('H:i', ($event['end'])/1000) ?>" name="end" placeholder="Heure de fin" type="text" autocomplete="off" <?php if ($locked): ?>disabled="disabled"<?php endif; ?>>
	</fieldset>
	<fieldset>
        <?php if (!$locked): ?>
		<button type="button" class="btn btn-primary frm_submit frm_before_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-pencil"></span> Éditer evénement</button>
        <?php endif; ?>
        <?php if (User::can('delete_event')): ?>
        <button type="button" class="btn btn-danger pull-right delete_event" data-event_id="<?php echo $event['id'] ?>" <?php if (!$locked): ?>style="margin-right: 15px;"<?php endif; ?>><span class="glyphicon glyphicon-remove"></span> Supprimer evénement</button>
        <?php endif; ?>
	</fieldset>
</form>
<script>
$(document).ready(function(){
   $('.time').bootstrapMaterialDatePicker({
        date: false,
        shortTime: false,
        format: 'HH:mm',
        switchOnClick: true,
        cancelText: 'Annuler',
        okText: 'OK',
    }).on('close', function(event){
        if ($('#end').val() != "" && $('#start').val() != "") {
            var start = ($('#start').val()).replace(':', '');
            var end = ($('#end').val()).replace(':', '');
            if (start == end) {
                $('#end').removeClass('ok').addClass('error');
                $('#start').removeClass('ok').addClass('error');
                $('#end', '#start').val('');
                Notif.Show('L\'heure de début doit être antérieure à l\'heure de fin', 'error', true, 5000);
            } else if (parseInt(start) > parseInt(end)) {
                $('#end').removeClass('ok').addClass('error');
                $('#start').removeClass('ok').addClass('error');
                $('#end', '#start').val('');
                Notif.Show('L\'heure de début doit être antérieure à l\'heure de fin', 'error', true, 5000);
            } else if (parseInt(start) < parseInt(end)) {
                $('#end').removeClass('error').addClass('ok');
                $('#start').removeClass('error').addClass('ok');
            }
        }
    });
});

function frm_create_event_before_submit (element) {
    if ($('#end').val() != "" && $('#start').val() != "") {
        var start = ($('#start').val()).replace(':', '');
        var end = ($('#end').val()).replace(':', '');
        if (start == end) {
            $('#end').removeClass('ok').addClass('error');
            $('#start').removeClass('ok').addClass('error');
            $('#end', '#start').val('');
            Notif.Show('L\'heure de début doit être antérieure à l\'heure de fin', 'error', true, 5000);
            return false;
        } else if (parseInt(start) > parseInt(end)) {
            $('#end').removeClass('ok').addClass('error');
            $('#start').removeClass('ok').addClass('error');
            $('#end', '#start').val('');
            Notif.Show('L\'heure de début doit être antérieure à l\'heure de fin', 'error', true, 5000);
            return false;
        } else if (parseInt(start) < parseInt(end)) {
            $('#end').removeClass('error').addClass('ok');
            $('#start').removeClass('error').addClass('ok');
            return true;
        }
    } else if ($('#end').val() == "" && $('#start').val() == "") {
        $('#end').removeClass('ok').addClass('error');
        $('#start').removeClass('ok').addClass('error');
        return false;
    }
}
</script>