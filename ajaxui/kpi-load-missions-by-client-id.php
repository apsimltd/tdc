<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('read_client_kpi', 'read_manager_kpi'))): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php if (getPost()) { ?>

    <?php $missions = Mission::getMissionByClientIdKPI(getPost('client_id'), getPost('start_date'), getPost('end_date')); // debug($cdrs); ?>
    <?php $status = Control::getControlListByType(9); ?>
    
    <fieldset>
        <label>Mission</label>
        <select class="frm_chosen" name="mission_id" id="mission_id" data-validation="val_blank">
            <option value="">Choisir Mission</option>
            <?php foreach($missions as $mission): ?>
            <option value="<?php echo $mission['id'] ?>">
                <?php echo $mission['poste'] . ' - [' . $status[$mission['status']] . '] - [' . dateToFr($mission['date_debut']) . ' - ' . dateToFr($mission['date_fin']) . ']' ?>
            </option>
            <?php endforeach; ?>
        </select>
    </fieldset>

<?php } ?>