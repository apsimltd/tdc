<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_mission')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $documents = Mission::getDocuments(get('id')); ?>

<?php if (!empty($documents)) :?>
    <ul class="links_candidat">
    <?php foreach($documents as $document): ?>
        <?php 
        $imgType = array('jpg', 'jpeg', 'png');
        $ext = getFileExt($document['file']);
        if (in_array($ext, $imgType)):
        ?>
        <li>
            <a href="<?php echo MISSION_URL ?>/<?php echo get('id') ?>/<?php echo $document['file'] ?>" data-lightbox="<?php echo $document['file'] ?>"><?php echo $document['file'] ?></a>
            <span class="glyphicon glyphicon-trash delete_document tooltips" title="Supprimer" data-document_id="<?php echo $document['id'] ?>" data-mission_id="<?php echo get('id') ?>"></span>
        </li>
        <?php else: ?>
        <li>
            <a href="<?php echo BASE_URL ?>/download?type=miss&id=<?php echo $document['id'] ?>"><?php echo $document['file'] ?></a>
            <span class="glyphicon glyphicon-trash delete_document tooltips" title="Supprimer" data-document_id="<?php echo $document['id'] ?>" data-mission_id="<?php echo get('id') ?>"></span>
        </li>
        <?php endif; ?>
    <?php endforeach;?>
    </ul>
<?php endif; ?>

<form class="frm_frm" name="frm_edit_mission_add_files" id="frm_edit_mission_add_files" action="<?php echo AJAX_HANDLER ?>/add-mission-file" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="mission_id" value="<?php echo get('id') ?>">
    
    <?php if (get('tdc_id') && get('tdc_id') != ""): ?>
    <input type="hidden" name="tdc_id" value="<?php echo get('tdc_id') ?>">
    <?php endif; ?>
    
    <fieldset class="uploadMultiple">
        <div class="clearfix">
            <label class="pull-left">Documents <span class="glyphicon glyphicon-info-sign tooltips" title="Les fichiers autorisés sont : .doc, .docx, .xls, .xlsx, .pdf, .jpg, .jpeg, .png ; Taille max : 4 Mo"></span></label>
            <input type="hidden" name="uploadFlag" id="uploadFlag" value="1">
            <div class="clearfix add_field_wrap_documents pull-left" style="width: 60%;">
                <div class="add_field_row pull-left">
                    <input type="file" name="file[]" id="file_1" data-number="1" class="inputfile inputfile-1">
                    <label id="uploadLabel_1" for="file_1" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span></label>
                    <button type="button" class="clear_field_upload btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
            </div>
        </div>
        <div class="mt6 clearfix">
            <button type="button" class="add_field_upload btn btn-icon btn-success pull-right tooltips" title="Ajouter un nouveau fichier"><span class="glyphicon glyphicon-plus"></span></button>
        </div>                              
    </fieldset>

    <fieldset>
        <button type="button" class="btn btn-primary frm_submit frm_before_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter Document/s</button>
    </fieldset>

</form>