<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $comment = Candidat::getCommentEntrepriseByKey(get('entreprise_id'), get('comment_id')); ?>

<h4 class="onboarding-title">Commentaire Entreprise</h4>
<div class="onboarding-text">Éditer Commentaire</div>
<form class="frm_frm frm_ajax" name="frm_edit_entreprise_edit_comment" id="frm_edit_entreprise_edit_comment" data-url="<?php echo AJAX_HANDLER ?>/edit-entreprise-comment" data-type="json">
    <input type="hidden" name="entreprise_id" value="<?php echo get('entreprise_id') ?>">
    <input type="hidden" name="key" value="<?php echo get('comment_id') ?>">
    <fieldset>
        <textarea name="commentaire" class="frm_textarea must ok" style="width: 100%; height: 250px;" data-validation="val_blank"><?php echo $comment['comment'] ?></textarea>                                
    </fieldset>

    <fieldset>
        <button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-pencil"></span> Éditer commentaire</button>
    </fieldset>

</form>