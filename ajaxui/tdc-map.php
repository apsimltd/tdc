<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>

<?php ### http://opsearch.fm/tdc?id=2915 ?>

<?php
require_once __DIR__ . '/../includes/functions.php';

$mission_id = array_key_exists('mission_id', $_GET) ? intval($_GET['mission_id']) : 0;

$has_mission = $mission_id !== 0;
$candidats_par_departments = $mission_id === 0 ? false : load_candidats_par_departments($mission_id);
/**
 * $candidats_par_departments array => always loading by nombre desc
 * [76] => Array
        (
            [nombre] => 265
            [region] => 194
            [name] => Seine-Maritime
            [pourcentage] => 84.66
            [dept_code] => 76
        )
 * 
 * where key is the dept_code
 */

$bg_colors = array();
$counter = 0;

//if ($me['id'] == 301) { // bastien
//    debug($candidats_par_departments);
//}

$grouped_regions_dept = array(75, 77, 78, 91, 92, 93, 94, 95);
$sum_grouped_dept = 0;

$greatest_value_dept_key = '';

foreach($candidats_par_departments as $key => $data) :
    
    // new modif july 2021
    // must grouped the dept 75, 77, 78, 91, 92, 93, 94 and 95 as one region and display the total num of candidats
    // must also add the region Ile de france into it as candidat number => ile de france has control id 221 in table controles but this is not a region
    // in the map
    
    /// if ($key == 75 || $key == 92 || $key == 93 || $key == 94) {
    if (in_array($key, $grouped_regions_dept)) {
        
        //$bg_colors[$key] = '#00FF40'; // must changed the color later => for now it is green
        $sum_grouped_dept += $data['nombre'];
        
//        if ($key == 75) {
//            $bg_colors[$key] = '#FF0000'; 
//        } else if ($key == 92) {
//            $bg_colors[$key] = '#36D900'; 
//        } else if ($key == 93) {
//            $bg_colors[$key] = '#FF8000'; 
//        } else if ($key == 94) {
//            $bg_colors[$key] = '#D900D9'; 
//        }
        
    } else {
        if ($counter == 0) {
            $bg_colors[$key] = '#EE417C'; // red => the highest num of candidats is always the first index
        } else {
            $bg_colors[$key] = '#2A77B4'; // orange => the rest is orange
        }
    }
    
    if ($counter == 0) {
        $greatest_value_dept_key = $key;
        $bg_colors[$key] = '#EE417C'; // red => the highest num of candidats is always the first index
    }
    
    
    $counter++;
endforeach;

// if the greatest value is in the grouped depts => color it red as the highest dept

if (in_array($greatest_value_dept_key, $grouped_regions_dept)) {
    
    foreach($grouped_regions_dept as $grouped_dept) :
        
        $bg_colors[$grouped_dept] = '#EE417C'; // red as its the highest number of candidates
        
    endforeach;
    
} 

// must add ile de france in the $sum_grouped_dept
$num_candidates_in_ile_de_france = TDC::getTotalCandidatInIleDeFrance($mission_id);

// sum $sum_grouped_dept and $num_candidates_in_ile_de_france
$sum_grouped_dept += $num_candidates_in_ile_de_france;

//debug($bg_colors);
//debug($candidats_par_departments);
/**
 *  En rose : le département ou il y’a le plus de candidats
    En blue foncer : les autres départements ou il y’a au moins 1 candidat
    En blue pal : les départements ou il n’y a pas de candidats
 */
?>

<h4 class="onboarding-title">MAP <a class="tdc-map-pdf tooltips" data-mission_id="<?php echo $mission_id ?>" title="Télécharger le PDF">Télécharger le PDF <span class="glyphicon glyphicon-download-alt"></span></a></h4>

<div class="map-legende">
    <?php foreach (Map::$departments as $dept_code => $dept) : ?>
        <?php if (array_key_exists($dept_code, $candidats_par_departments)) { ?>
            <?php /// if ($dept_code == 75 || $dept_code == 92 || $dept_code == 93 || $dept_code == 94): ?>
            <?php if (in_array($dept_code, $grouped_regions_dept)): ?>
                <?php if (array_key_exists($dept_code, $bg_colors)) : ?>
                    <?php $bgcolor = "background-color: $bg_colors[$dept_code];"; ?>
                <?php else: ?>
                    <?php $bgcolor = "background-color:#80C7E3;"; ?>
                <?php endif; ?>
                <span style="<?php print $bgcolor ?>">
                    <?php print $dept['name'] ?> - <?php print $candidats_par_departments[$dept_code]['nombre'] ?>
                </span>
            <?php endif; ?>
        <?php } ?>
    <?php endforeach; ?>
    
    <?php 
    // ile de france legende
    if ($num_candidates_in_ile_de_france > 0) {
    ?>
    <span style="<?php print $bgcolor ?>">
        Ile de France - <?php print $num_candidates_in_ile_de_france ?>
    </span>
    <?php } ?>
    
</div><!-- / map-legende -->

<div class="map-wrapper" style="min-height: 900px; margin-top: 100px;">
    
    <div class="map clearfix" style="padding-top: 100px;">
        
        <svg
            xmlns:mapsvg="http://mapsvg.com"
            xmlns:dc="http://purl.org/dc/elements/1.1/"
            xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
            xmlns:svg="http://www.w3.org/2000/svg"
            xmlns="http://www.w3.org/2000/svg"
            mapsvg:geoViewBox="-5.181900 51.089515 9.560929 41.366975"
            width="612.3844"
            height="584.36005">
            
            <?php foreach (Map::$departments as $dept_code => $dept) { ?>
                <path 
                    d="<?php print $dept['path']; ?>" 
                    <?php if (array_key_exists($dept_code, $bg_colors)) : ?>
                    fill="<?php print $bg_colors[$dept_code]; ?>"
                    <?php else: ?>
                    fill="#80C7E3<?php // light blue nothing ?>" 
                    <?php endif; ?>
                    fill-opacity="0.8" 
                    <?php if (!in_array($dept_code, $grouped_regions_dept)): ?>
                    stroke="black" 
                    <?php endif; ?>
                    class="tooltips"
                    title="<?php print $dept['name']; ?>">
                </path>
            <?php }?>
            
            <?php  if ($sum_grouped_dept > 0) { // must create a rectangle to display the counter of the grouped dept ?> 
            
                <rect x="304.629000" y="138.902000" width="<?php print rectw($sum_grouped_dept) ?>" height="11" fill="white" fill-opacity="0.5"></rect>
                
                <text x="307.629000" y="148.902000" font-size="9pt" font-family="Roboto" fill="black">
                    <?php print $sum_grouped_dept ?>
                </text>
                
            <?php } ?>
            
            <?php foreach (Map::$departments as $dept_code => $dept) { ?>
                <?php if (array_key_exists($dept_code, $candidats_par_departments)) { ?>
                
                    <?php /// if ($dept_code == 75 || $dept_code == 92 || $dept_code == 93 || $dept_code == 94): ?>
                    <?php if (in_array($dept_code, $grouped_regions_dept)): ?>
                    
                        <?php // do nothing ?>
            
                    <?php else : ?>
            
                        <rect x="<?php print $dept['rx'] ?>" y="<?php print $dept['ry'] ?>" width="<?php print rectw($candidats_par_departments[$dept_code]['nombre']) ?>" height="11" fill="white" fill-opacity="0.5"></rect>
                        
                    <?php endif; ?>
                <?php } ?>
            <?php }?>

            <?php foreach (Map::$departments as $dept_code => $dept) { ?>
                <?php if (array_key_exists($dept_code, $candidats_par_departments)) { ?>
                    <?php /// if ($dept_code == 75 || $dept_code == 92 || $dept_code == 93 || $dept_code == 94): ?>
                    <?php if (in_array($dept_code, $grouped_regions_dept)): ?>
                        
                        <?php // do nothing ?>
                        
                    <?php else : ?>   
                        
                        <text x="<?php print $dept['x'] ?>" y="<?php print $dept['y'] ?>" font-size="9pt" font-family="Roboto" fill="black" >
                            <title><?php print $dept['name'] ?></title>
                            <?php print $candidats_par_departments[$dept_code]['nombre'] ?>
                        </text>
                        
                    <?php endif; ?>
                <?php }?>
            <?php } ?>
                    
        </svg>

    </div><!-- / map -->
    
    <div class="map-table" style="margin-top: 185px;">
        
        <table class="table-list compact">
            <thead>
                <tr>
                    <th>Region</th>
                    <th>Nombre</th>
                    <th>%</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            // must recalculate the % for each as we added ile de france manually 
            $total = 0;
            foreach($candidats_par_departments as $department) :
                
                $total += $department['nombre'];
                
            endforeach;
            // add the ile de france
            $total += $num_candidates_in_ile_de_france;
            ?>
                
            <?php foreach ($candidats_par_departments as $department) {?>
                <tr>
                    <td>
                        <?php print $department['name']; ?>
                    </td>
                    <td>
                        <?php print $department['nombre']; ?>
                    </td>
                    <td>
                        <?php 
                        // print $department['pourcentage']; 
                        print round(100*($department['nombre'] / $total), 2);
                        ?> %
                    </td>
                </tr>
            <?php }?>
                
                <?php if ($num_candidates_in_ile_de_france > 0): ?>
                <tr>
                    <td>
                       Ile de France 
                    </td>
                    <td>
                        <?php print $num_candidates_in_ile_de_france; ?>
                    </td>
                    <td>
                        <?php print round(100*($num_candidates_in_ile_de_france / $total), 2); ?> %
                    </td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
        
    </div><!-- . map-table -->
    
</div><!-- map-wrapper -->

<style type="text/css">
    .modal-v2 .modal-dialog.modal-80 {min-width: 950px;}
    svg {transform: scale(1.5);display: block; margin: 0px auto;}
    .modal-v2 .onboarding-modal .onboarding-side-by-side .onboarding-content.with-gradient { background-image: none;}
    a.tdc-map-pdf { cursor: pointer; font-size: 12px; font-weight: normal; text-decoration: underline; display: inline-block; color: #7cbd5a;}
        a.tdc-map-pdf span {text-decoration: none; display: inline-block; padding-left: 5px;}
        a.tdc-map-pdf:hover { color: #486e34;text-decoration: none;}
    .onboarding-media { display: none!important;}
    .modal-v2 .onboarding-modal .onboarding-side-by-side .onboarding-content.with-gradient { width: 100%!important;}
    .map-legende { text-align: center;}
        .map-legende span { display: inline-block; margin-right: 10px; line-height: 20px; padding: 0px 10px; color: #fff; border-radius: 3px;}
</style>