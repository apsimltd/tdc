<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_task')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $task = Task::getTaskById(get('id')) ?>

<h4 class="onboarding-title">Tâche</h4>
<div class="onboarding-text">Éditer tâche</div>
<form class="frm_frm frm_ajax" name="frm_edit_task" id="frm_edit_task" data-url="<?php echo AJAX_HANDLER ?>/edit-task" data-type="json">
    <input type="hidden" name="id" value="<?php echo $task['id'] ?>">
    <input type="hidden" name="title_old" value="<?php echo $task['title'] ?>">
    <input type="hidden" name="creator_id" value="<?php echo $task['creator_id'] ?>">
	<fieldset>
		<label>Titre</label>
        <input class="frm_text must ok" value="<?php echo $task['title'] ?>" name="title" placeholder="Titre" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
    	<label>Description</label>
        <textarea name="description" class="frm_textarea must ok" placeholder="Description" data-validation="val_blank"><?php echo $task['description'] ?></textarea>                                
    </fieldset>
    <input type="hidden" name="assignee_id_old" value="<?php echo $task['assignee_id'] ?>">
	<fieldset>
		<label>Affecté à</label>
		<select class="frm_chosen must ok" name="assignee_id" data-validation="val_blank">
        	<option value="">Choisir Utilisateur</option>
            <?php $users = User::getUsersByTLId($me['id']); ?>
            <?php foreach($users as $user): ?>
            <option value="<?php echo $user['id'] ?>" <?php if ($task['assignee_id'] == $user['id']): ?>selected="selected"<?php endif; ?>>
                <?php echo mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName']) ?>
            </option>
            <?php endforeach; ?>
		</select>
	</fieldset>
	<fieldset>
		<button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-pencil"></span> Éditer Tâche</button>
	</fieldset>
</form>


