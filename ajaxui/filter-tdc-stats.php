<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('tdc', 'TDC_CLIENT'))): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $mission_id = getPost('mission_id'); ?>
<?php $candidat_ids = TDC::filterStatsTDC(getPost()); ?>
<?php $entreprise_ids = TDC::filterStatsEntrepriseTDC(getPost()); ?>

<div class="stats-wrapper clearfix">
	<div class="col-stats pull-left clearfix">
		<div class="nano">
			<div class="nano-content">
				<ul>
<!--                    <li>Entreprises identifiées : <strong><?php ///echo (is_array($entreprise_ids) && !empty($entreprise_ids)) ? TDC::getTotalEntrepriseInTDC($mission_id, $entreprise_ids) : 0 ?></strong></li>-->
                                        
                    <?php
                    $ent = 0;
                    if ((is_array($candidat_ids) && !empty($candidat_ids)) && (is_array($entreprise_ids) && !empty($entreprise_ids))) {
                        $ent = TDC::getTotalSocieteinTDC_V2($mission_id, $candidat_ids, $entreprise_ids);
                    } else if ((is_array($candidat_ids) && !empty($candidat_ids)) && (!is_array($entreprise_ids) && empty($entreprise_ids))) {
                        $ent = TDC::getTotalSocieteinTDC_V2($mission_id, $candidat_ids);
                    } else if ((!is_array($candidat_ids) && empty($candidat_ids)) && (is_array($entreprise_ids) && !empty($entreprise_ids))) {
                        $ent = TDC::getTotalSocieteinTDC_V2($mission_id, false, $entreprise_ids);
                    }
                    ?>
                    
                    <li>Entreprises identifiées : <strong><?php echo $ent ?></strong></li>
                    
                    <li>Entreprises à approcher : <strong><?php echo (is_array($entreprise_ids) && !empty($entreprise_ids)) ? TDC::getTotalEntrepriseInTDCAAprocher($mission_id, $entreprise_ids) : 0 ?></strong></li>
                    <li>Entreprises OUT : <strong><?php echo (is_array($entreprise_ids) && !empty($entreprise_ids)) ? TDC::getTotalEntrepriseInTDCOUT($mission_id, $entreprise_ids) : 0 ?></strong></li>
                    <li>Entreprises FINI : <strong><?php echo (is_array($entreprise_ids) && !empty($entreprise_ids)) ? TDC::getTotalEntrepriseInTDCFINI($mission_id, $entreprise_ids) : 0 ?></strong></li>
					<li>Candidats identifiés : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalCandidatInTDC($mission_id, $candidat_ids) : 0 ?></strong></li>
					<li>Candidats coeur de cible : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalCandidatCCCinTDC($mission_id, $candidat_ids) : 0 ?></strong></li>
					<li>Candidats approchés : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalCandidatApprocheinTDC($mission_id, $candidat_ids) : 0 ?></strong></li>
					<li>Candidats restants à approcher : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalCandidatRestantApprocheinTDC($mission_id, $candidat_ids) : 0 ?></strong></li>
                    
                    <li>Âge moyen : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getAverageAgeInTDC($mission_id, $candidat_ids) : 0 ?></strong></li>
                    <li>Package Salarial moyenne : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getAverageRemuneration($mission_id, $candidat_ids) : 0 ?> K€</strong></li>
                    <li>Salaire fixe moyenne : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getAverageSalairefixe($mission_id, $candidat_ids) : 0 ?> K€</strong></li>
                    <li><strong>Localisation</strong></li>
                    <?php
                    if ((is_array($candidat_ids) && !empty($candidat_ids))) {
                        $locas = TDC::getLocalisationGrouped($mission_id, $candidat_ids);
                    } else {
                        $locas = array();
                    }
                    ?>
                    <?php if (!empty($locas)): ?>
                        <?php foreach($locas as $loca): ?>
                        <li class="indented"><?php echo $loca['name'] ?> : <strong><?php echo $loca['Total'] ?></strong></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
				</ul>
			</div>
		</div>        
	</div>
	<div class="col-stats pull-left clearfix">
		<div class="nano">
			<div class="nano-content">
				<ul>
					<?php // in => 229, out => 226, bu+ => 228, bu- => 227 ?>
					<li>IN : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalCandidatinTDCByStatus($mission_id, 229, $candidat_ids) : 0 ?></strong></li>
					<li>BU+ : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalCandidatinTDCByStatus($mission_id, 228, $candidat_ids) : 0 ?></strong></li>
					<li>BU- : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalCandidatinTDCByStatus($mission_id, 227, $candidat_ids) : 0 ?></strong></li>
                    <li>Ex BU : <strong>><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalCandidatinTDCByStatus($mission_id, 259, $candidat_ids) : 0 ?></strong></li>
					<li>OUT : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalCandidatinTDCByStatus($mission_id, 226, $candidat_ids) : 0 ?></strong></li>
                    <li>ARC : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalCandidatinTDCByStatus($mission_id, 251, $candidat_ids) : 0 ?></strong></li>
                    <li>ARCA : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalCandidatinTDCByStatus($mission_id, 252, $candidat_ids) : 0 ?></strong></li>
				</ul>
			</div>
		</div>        
	</div>
	<div class="col-stats pull-left clearfix">
		<div class="nano">
			<div class="nano-content">
				<ul>
<!--					<li>Société Candidats identifiées : <strong><?php ///echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalSocieteinTDC($mission_id, $candidat_ids) : 0 ?></strong></li>-->
					<?php $out_categories = Mission::getOutCategory($mission_id) ?>
					<?php foreach($out_categories as $out): ?>
					<li><?php echo $out['out_categorie'] ?> : <strong><?php echo (is_array($candidat_ids) && !empty($candidat_ids)) ? TDC::getTotalByOutCategoryinTDC($mission_id, $out['id'], $candidat_ids) : 0 ?></strong></li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>        
	</div>
</div><!-- /stats-wrapper -->
<div class="stats-filter clearfix">
	
    <?php
    if (getPost()) {
        $start_date = getPost('start_date');
        $end_date = getPost('end_date');
        $type = getPost('type');
        $tdc_legende_color_ids = getPost('tdc_legende_color_ids');
        $tdc_legende_entreprise_color_ids = getPost('tdc_legende_entreprise_color_ids');
        $tdc_legende_out_categorie_id = getPost('tdc_legende_out_categorie_id');
    } else {
        $start_date = '';
        $end_date = '';
        $type = '';
        $tdc_legende_color_ids = '';
        $tdc_legende_entreprise_color_ids = '';
        $tdc_legende_out_categorie_id = '';
    }
    ?>
    
	<form class="frm_frm frm_ajax frm_horizontal clearfix" name="frm_tdc_stats" id="frm_tdc_stats" data-url="<?php echo AJAX_HANDLER ?>/filter-tdc-stats" data-type="json">
		
        <input type="hidden" name="mission_id" value="<?php echo $mission_id ?>">
		
		<fieldset>
			<button type="button" class="btn btn-icon btn-warning pull-right filter_stats_debut" data-mission_id="<?php echo $mission_id ?>"><span class="glyphicon glyphicon-calendar"></span> Depuis le début</button>
		</fieldset>
        <fieldset>
            <select class="frm_chosen <?php if ($type !== ""): ?>ok<?php endif; ?>" name="type" id="tdc_stats_type" data-validation="val_blank">
                <option value="">Choisir Filtrer Par</option>
                <option value="date_prise_contact" <?php if ($type == "date_prise_contact"): ?>selected="selected"<?php endif; ?>>Date Prise Contact</option>
                <option value="date_ajoute" <?php if ($type == "date_ajoute"): ?>selected="selected"<?php endif; ?>>Date Ajouté</option>
            </select>   
        </fieldset>
		<fieldset>
            <input class="frm_text must <?php if ($start_date != ""): ?>ok<?php endif; ?>" <?php if ($start_date != ""): ?>value="<?php echo $start_date ?>"<?php endif; ?> id="start_date" name="start_date" placeholder="Date de début" type="text" autocomplete="off" data-validation="val_blank">
		</fieldset>
		<fieldset>
			<input class="frm_text must <?php if ($end_date != ""): ?>ok<?php endif; ?>" <?php if ($end_date != ""): ?>value="<?php echo $end_date ?>"<?php endif; ?> id="end_date" name="end_date" placeholder="Date de fin" type="text" autocomplete="off" data-validation="val_blank">
		</fieldset>
        <input type="hidden" name="tdc_legende_color_ids" id="tdc_legende_color_ids" value="<?php echo $tdc_legende_color_ids; ?>">
        <input type="hidden" name="tdc_legende_entreprise_color_ids" id="tdc_legende_entreprise_color_ids" value="<?php echo $tdc_legende_entreprise_color_ids; ?>">
        <input type="hidden" name="tdc_legende_out_categorie_id" id="tdc_legende_out_categorie_id" value="<?php echo $tdc_legende_out_categorie_id ?>">
		<fieldset class="submit">
			<button type="button" class="btn btn-success frm_submit frm_notif" id="tdc_stats_filter_submit" data-form="2"><span class="glyphicon glyphicon-filter"></span> Filtrer</button>
		</fieldset>
	</form>
	
</div><!-- /stats-filter -->