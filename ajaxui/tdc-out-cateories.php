<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $tdc_id = get('tdc_id') ?>
<?php $tdc = TDC::getTDCById($tdc_id); ?>
<?php $out_categories = Mission::getOutCategory(get('mission_id')) ?>
<?php $selected_out_catgory_ids = TDC::getSelectedOutCategories(get('mission_id'), get('candidat_id'))?>
<?php // debug($tdc) ?>

<h4 class="onboarding-title">Out Categorie</h4>

<form class="frm_frm frm_ajax" name="frm_tdc_out_categories" id="frm_tdc_out_categories" data-url="<?php echo AJAX_HANDLER ?>/tdc-out-categories" data-type="json">
    
    <input type="hidden" name="mission_id" value="<?php echo get('mission_id') ?>">
    <input type="hidden" name="candidat_id" value="<?php echo get('candidat_id') ?>">
    <input type="hidden" name="tdc_id" value="<?php echo get('tdc_id') ?>">
    
    <fieldset class="selectOption largemultiselect">
        <label style="text-align: left;">Out Categorie</label>
        <div class="frm_select_multi_wrapper">
            <select class="frm_select_multi <?php if (count($selected_out_catgory_ids) > 0): ?>ok<?php endif; ?>" data-validation="val_blank" multiple='multiple'>
                <?php foreach($out_categories as $out_category): ?>
                    <option value="<?php echo $out_category['id'] ?>" <?php if (in_array($out_category['id'], $selected_out_catgory_ids)): ?>selected="selected"<?php endif; ?>>
                        <?php echo $out_category['out_categorie'] ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <input type="hidden" name="out_categorie_ids" value="<?php echo addSep($selected_out_catgory_ids) ?>" class="frm_select_multi_helper" data-validation="val_blank">
        </div>
    </fieldset>
    
<!--    <fieldset>
        <textarea name="commentaire_outcategory" class="frm_textarea <?php if ($tdc['commentaire_outcategory'] != ""): ?>ok<?php endif; ?>" style="width: 100%; height:150px;" placeholder="Commentaire" data-validation="val_blank"><?php echo $tdc['commentaire_outcategory'] ?></textarea>                                
    </fieldset>-->
    
    <fieldset>
        <button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-floppy-disk"></span> Sauvegarder</button>
    </fieldset>
    
</form>