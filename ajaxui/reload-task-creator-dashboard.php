<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('create_task', 'read_task', 'edit_task', 'delete_task', 'archive_task'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php if (get('id')): ?>
    <?php $createdTasks = Task::getTaskListCreator(get('id')) ?>

    <?php if (!empty($createdTasks)): ?>

        <?php foreach($createdTasks as $task): ?>
        <div class="task-list-row task_rows_<?php echo $task['id'] ?> <?php if ($task['status'] == 0): ?>completed<?php endif; ?>">
            <div class="task-check tooltips" title="<?php if ($task['status'] == 1): ?>Terminé<?php else: ?>Incomplet<?php endif; ?>" data-task_id="<?php echo $task['id'] ?>">
                <svg viewBox="0 0 32 32"><polygon points="27.672,4.786 10.901,21.557 4.328,14.984 1.5,17.812 10.901,27.214 30.5,7.615 "></polygon></svg>
            </div>
            <div class="task-title">
                <?php echo $task['title'] ?>
                <?php if (User::can('read_task', 'read_all_task', 'create_task')): ?>
                <span class="glyphicon glyphicon-eye-open tooltips view_task" data-task_id="<?php echo $task['id'] ?>" title="Voir Plus"></span>
                <?php endif; ?>
            </div>
            <?php $assignee = User::getUserById($task['assignee_id']); ?>
            <div class="task-creator">
                <?php echo dateToFr($task['created'], true) ?> - <strong><?php echo mb_ucfirst(substr($assignee['firstName'], 0, 1)) . '. ' . mb_strtoupper($assignee['lastName']) ?></strong>
                <?php if (User::can('edit_task')): ?>
                    <?php if ($task['status'] == 1): ?><span class="glyphicon glyphicon-edit edit_task tooltips" title="Éditer" data-task_id="<?php echo $task['id'] ?>"></span><?php endif; ?>
                <?php endif; ?>
                <?php if (User::can('archive_task')): ?>
                    <?php if ($task['status'] == 0): ?><i class="fa fa-archive archive_task tooltips" title="Archiver" data-task_id="<?php echo $task['id'] ?>"></i><?php endif; ?>
                <?php endif; ?>
                <?php if (User::can('delete_task')): ?>
                    <?php if ($task['status'] == 1): ?><span class="glyphicon glyphicon-trash delete_task tooltips" title="Supprimer" data-task_id="<?php echo $task['id'] ?>"></span><?php endif; ?>
                <?php endif; ?>
            </div>
        </div><!-- /task-list-row -->
        <?php endforeach; ?>

    <?php else: ?>

    <div class="task-list-row completed">
        <div class="task-title">
            Aucune tâche                                        
        </div>
    </div><!-- /task-list-row -->

    <?php endif; ?>
    
<?php endif; ?>