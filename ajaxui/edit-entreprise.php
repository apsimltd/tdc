<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $entreprise = Candidat::getEntrepriseById(get('id')); ?>


<h4 class="onboarding-title">Entreprise</h4>
<div class="onboarding-text">Éditer entreprise</div>
<form class="frm_frm frm_ajax" name="frm_edit_entreprise" id="frm_edit_entreprise" data-url="<?php echo AJAX_HANDLER ?>/edit-entreprise" data-type="json">
	<input type="hidden" name="id" value="<?php echo $entreprise['id'] ?>">
    <?php if (get('tdc_id')): ?>
    <input type="hidden" name="tdc_id" value="<?php echo get('tdc_id') ?>">
    <?php endif; ?>
    <?php if (get('tdc_line_id')): ?>
    <input type="hidden" name="tdc_line_id" value="<?php echo get('tdc_line_id') ?>">
    <?php endif; ?>
    <fieldset>
        <label>Nom</label>
        <input class="frm_text must ok" value="<?php echo $entreprise['nom'] ?>" name="nom" placeholder="Nom" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>
    <fieldset>
        <label>Téléphone</label>
        <input class="frm_text tel_fr" name="telephone" value="<?php echo $entreprise['telephone'] ?>" placeholder="Téléphone" type="text" autocomplete="off" data-validation="val_blank" maxlength="14">
    </fieldset>    
    <fieldset>
        <label>Région</label>
        <input class="frm_text" name="region" value="<?php echo $entreprise['region'] ?>" placeholder="Région" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>    
    <fieldset>
        <label>Email</label>
        <input class="frm_text" name="email" value="<?php echo $entreprise['email'] ?>" placeholder="Email" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>
    <fieldset>
        <label>Secteur Activité</label>
        <input class="frm_text" name="secteur_activite" value="<?php echo $entreprise['secteur_activite'] ?>" placeholder="Secteur Activité" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset> 
    <fieldset>
        <label>Sous Secteur Activité</label>
        <input class="frm_text" name="sous_secteur_activite" value="<?php echo $entreprise['sous_secteur_activite'] ?>" placeholder="Sous Secteur Activité" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>
    <fieldset>
        <label>Commentaire</label>
        <textarea name="commentaire" class="frm_textarea" style="height:150px;" placeholder="Commentaire" data-validation="val_blank"><?php echo $entreprise['commentaire'] ?></textarea>
    </fieldset>
    
	<fieldset>
		<button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-pencil"></span> Éditer Entreprise</button>
	</fieldset>
</form>