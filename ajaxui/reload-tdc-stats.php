<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('tdc', 'TDC_CLIENT'))): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php $mission_id = get('mission_id'); ?>

<div class="stats-wrapper clearfix">
	<div class="col-stats pull-left clearfix">
		<div class="nano">
			<div class="nano-content">
				<ul>
                    <li>Entreprises identifiées : <strong><?php echo TDC::getTotalEntrepriseInTDC($mission_id) ?></strong></li>
					<li>Candidats identifiés : <strong><?php echo TDC::getTotalCandidatInTDC($mission_id) ?></strong></li>
					<li>Candidats coeur de cible : <strong><?php echo TDC::getTotalCandidatCCCinTDC($mission_id) ?></strong></li>
					<li>Candidats approchés : <strong><?php echo TDC::getTotalCandidatApprocheinTDC($mission_id) ?></strong></li>
					<li>Candidats restants à approcher : <strong><?php echo TDC::getTotalCandidatRestantApprocheinTDC($mission_id) ?></strong></li>
                    <li>Âge moyen : <strong><?php echo TDC::getAverageAgeInTDC($mission_id) ?></strong></li>
                    <li>Rémunération moyenne : <strong><?php echo TDC::getAverageRemuneration($mission_id) ?> K€</strong></li>
                    <li><strong>Localisation</strong></li>
                    <?php $locas = TDC::getLocalisationGrouped($mission_id); //debug($locas); ?>
                    <?php foreach($locas as $loca): ?>
                    <li class="indented"><?php echo $loca['name'] ?> : <strong><?php echo $loca['Total'] ?></strong></li>
                    <?php endforeach; ?>
				</ul>
			</div>
		</div>        
	</div>
	<div class="col-stats pull-left clearfix">
		<div class="nano">
			<div class="nano-content">
				<ul>
					<?php // in => 229, out => 226, bu+ => 228, bu- => 227 ?>
					<li>IN : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission_id, 229) ?></strong></li>
					<li>BU+ : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission_id, 228) ?></strong></li>
					<li>BU- : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission_id, 227) ?></strong></li>
					<li>OUT : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission_id, 226) ?></strong></li>
                    <li>ARC : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission_id, 251) ?></strong></li>
                    <li>ARCA : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission_id, 252) ?></strong></li>
				</ul>
			</div>
		</div>        
	</div>
	<div class="col-stats pull-left clearfix">
		<div class="nano">
			<div class="nano-content">
				<ul>
					<li>Société Candidats identifiées : <strong><?php echo TDC::getTotalSocieteinTDC($mission_id) ?></strong></li>
					<?php $out_categories = Mission::getOutCategory($mission_id) ?>
					<?php foreach($out_categories as $out): ?>
					<li><?php echo $out['out_categorie'] ?> : <strong><?php echo TDC::getTotalByOutCategoryinTDC($mission_id, $out['id']) ?></strong></li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>        
	</div>
</div><!-- /stats-wrapper -->
<div class="stats-filter clearfix">
	
	<form class="frm_frm frm_ajax frm_horizontal clearfix" name="frm_tdc_stats" id="frm_tdc_stats" data-url="<?php echo AJAX_HANDLER ?>/filter-tdc-stats" data-type="json">
		
		<input type="hidden" name="mission_id" value="<?php echo $mission_id ?>">
		
		<fieldset>
			<button type="button" class="btn btn-icon btn-warning pull-right filter_stats_debut"><span class="glyphicon glyphicon-calendar"></span> Depuis le début</button>
		</fieldset>
        <fieldset>
            <select class="frm_chosen ok must" name="type" data-validation="val_blank">
                <option value="date_prise_contact">Date Prise Contact</option>
                <option value="date_ajoute">Date Ajouté</option>
            </select>   
        </fieldset>
		<fieldset>
			<input class="frm_text must" id="start_date" name="start_date" placeholder="Date de début" type="text" autocomplete="off" data-validation="val_blank">
		</fieldset>
		<fieldset>
			<input class="frm_text must" id="end_date" name="end_date" placeholder="Date de fin" type="text" autocomplete="off" data-validation="val_blank">
		</fieldset>
		<fieldset class="submit">
			<button type="button" class="btn btn-success frm_submit frm_notif" data-form="2"><span class="glyphicon glyphicon-filter"></span> Filtrer</button>
		</fieldset>
	</form>
	
</div><!-- /stats-filter -->

