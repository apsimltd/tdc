<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_user')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<h4 class="onboarding-title">Utilisateur Client</h4>
<div class="onboarding-text">Ajouter un nouveau utilisateur client</div>
<form class="frm_frm frm_ajax" name="frm_create_user" id="frm_create_user" data-url="<?php echo AJAX_HANDLER ?>/add-user-client" data-type="json">
    <fieldset>
        <label>Client</label>
        <select class="frm_chosen must" name="client_id" id="add_user_client_select" data-validation="val_blank">
            <option value="">Choisir Client</option>
            <?php $clients = Client::getClients('nom') ?>
            <?php foreach($clients as $client): ?>
            <option value="<?php echo $client['id'] ?>"><?php echo $client['id'] . ' - ' . mb_strtoupper($client['nom']) ?></option>
            <?php endforeach; ?>
        </select>
    </fieldset>
	<fieldset>
		<label>Nom</label>
		<input class="frm_text must" name="lastName" placeholder="Nom" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Prénom</label>
		<input class="frm_text must" name="firstName" placeholder="Prénom" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Identifiant</label>
		<input class="frm_text must" name="username" placeholder="Identifiant" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Email</label>
		<input class="frm_text must" name="email" placeholder="Email" type="text" autocomplete="off" data-validation="val_email">
	</fieldset>
    <fieldset>
		<label>Mot de passe</label>
		<input class="frm_text must" name="password" placeholder="Mot de passe" type="password" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Statut</label>
		<select class="frm_chosen must ok" name="status" data-validation="val_blank">
			<option value="1">Actif</option>
			<option value="0">Inactif</option>
		</select>
	</fieldset>
    <fieldset class="mission-checkbox">
        <input type="checkbox" class="frm_checkbox" name="mail_notif_check" id="mail_notif_check" checked="checked">
        <label class="pointer" for="mail_notif_check">Notification Email <span class="glyphicon glyphicon-info-sign tooltips" title="L'utilisateur recevra une notification par courrier électronique du commentaire manager sur les missions"></span></label>
    </fieldset>
    <input type="hidden" name="mail_notif" id="mail_notif" value="1">
    <div class="client_missions_select"><?php // load on change of client ?></div>
</form>