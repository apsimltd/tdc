<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php $tdc_id = get('tdc_id') ?>
<?php $tdc = TDC::getTDCById($tdc_id); ?>
<?php $colors_tdc = Control::getColorTDCEntreprise(); ?>
<?php // debug($tdc); ?>

<h4 class="onboarding-title">Couleurs Entreprise</h4>

<h5>Nom Entreprise <span class="glyphicon glyphicon-remove pull-right remove-colors entreprise tooltips" data-object="societe" data-tdc_id="<?php echo $tdc_id ?>" title="Enlever les couleurs"></span></h5>
<table class="table-list compact no-head societe">
    <tr>
        <td>Sélection du texte</td>
        <td>
            <div class="colorSelector color_societe_label nopointer" style="background-color: <?php echo Control::getColorTDCById($tdc['color_societe_label']) ?>"></div>
        </td>
    </tr>
    <tr>
        <td>Couleur du texte</td>
        <td>
            <?php foreach($colors_tdc as $color): ?>
            <div class="colorSelector triggerColorEntreprise" data-color="#<?php echo $color['hex'] ?>" data-class="color_societe_label" data-tdc_id="<?php echo $tdc_id ?>" data-color_id="<?php echo $color['id'] ?>" style="background-color: #<?php echo $color['hex'] ?>"><span><?php echo '#' . $color['hex'] ?></span></div>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td>Sélection de fond</td>
        <td>
            <div class="colorSelector color_societe_bg nopointer" style="background-color: <?php echo Control::getColorTDCById($tdc['color_societe_bg']) ?>"></div>
        </td>
    </tr>
    <tr>
        <td>Couleur de fond</td>
        <td>
            <?php foreach($colors_tdc as $color): ?>
            <div class="colorSelector triggerColorEntreprise" data-color="#<?php echo $color['hex'] ?>" data-class="color_societe_bg" data-tdc_id="<?php echo $tdc_id ?>" data-color_id="<?php echo $color['id'] ?>" style="background-color: #<?php echo $color['hex'] ?>"><span><?php echo '#' . $color['hex'] ?></span></div>
            <?php endforeach; ?>
        </td>
    </tr>
</table>
