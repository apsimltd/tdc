<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_type_content', 'edit_type_content')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $type_id = get('type_id'); ?>
<table class="table-list compact inside-nano no-head fixed-actions">
    <tbody>
        <?php $controls = Control::getControlByType($type_id) ?>
        <?php foreach($controls as $control): ?>
        <tr <?php if ($control['status'] == 0): ?>class="inactive"<?php endif; ?>>
            <td><?php echo $control['name'] ?></td>
            <?php if (User::can('edit_type_content')): ?>
            <td class="actions_button">
                <button type="button" data-type_id="<?php echo $type_id ?>" data_control_id="<?php echo $control['id'] ?>" class="edit_type_contenu btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>
            </td>
            <?php endif; ?>
        </tr>
        <?php endforeach; ?>                                               
    </tbody>
</table>