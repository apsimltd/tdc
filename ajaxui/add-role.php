<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_role')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<h4 class="onboarding-title">Rôle Utilisateur</h4>
<div class="onboarding-text">Ajouter un nouveau rôle d'utilisateur</div>
<form class="frm_frm frm_ajax" name="frm_create_role" id="frm_create_role" data-url="<?php echo AJAX_HANDLER ?>/add-role" data-type="json">
    <fieldset>
        <label>Rôle Utilisateur</label>
        <input class="frm_text must" name="name" placeholder="Rôle Utilisateur" type="text" autocomplete="off" data-validation="val_blank">
    </fieldset>
    <fieldset>
        <button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter Rôle</button>
    </fieldset>
</form>

