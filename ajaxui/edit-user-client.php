<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_user')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php $user = User::getUserById(get('id')); ?>
<?php $assignedMissions = Mission::getAssignedMissions(get('id')); ?>

<h4 class="onboarding-title">Utilisateur Client</h4>
<div class="onboarding-text">Éditer l'utilisateur client</div>
<form class="frm_frm frm_ajax" name="frm_edit_user" id="frm_edit_user" data-url="<?php echo AJAX_HANDLER ?>/edit-user-client" data-type="json">
    <input type="hidden" name="id" value="<?php echo $user['id'] ?>">
    <fieldset>
        <label>Client</label>
        <select class="frm_chosen must ok" name="client_id" id="add_user_client_select_edit" data-validation="val_blank">
            <option value="">Choisir Client</option>
            <?php $clients = Client::getClients('nom') ?>
            <?php foreach($clients as $client): ?>
            <option value="<?php echo $client['id'] ?>" <?php if ($user['client_id'] == $client['id']): ?>selected="selected"<?php endif; ?>>
                <?php echo $client['id'] . ' - ' . mb_strtoupper($client['nom']) ?>
            </option>
            <?php endforeach; ?>
        </select>
    </fieldset>
	<fieldset>
		<label>Nom</label>
        <input class="frm_text must ok" value="<?php echo mb_strtoupper($user['lastName']) ?>" name="lastName" placeholder="Nom" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Prénom</label>
        <input class="frm_text must ok" value="<?php echo mb_ucfirst($user['firstName']) ?>" name="firstName" placeholder="Prénom" type="text" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Identifiant</label>
		<input class="frm_text must ok" value="<?php echo $user['username'] ?>" name="username" placeholder="Identifiant" type="text" autocomplete="off" data-validation="val_blank">
        <input type="hidden" name="old_username" value="<?php echo $user['username'] ?>">
	</fieldset>
    <fieldset>
		<label>Email</label>
		<input class="frm_text must ok" value="<?php echo $user['email'] ?>" name="email" placeholder="Email" type="text" autocomplete="off" data-validation="val_email">
        <input type="hidden" name="old_email" value="<?php echo $user['email'] ?>">
	</fieldset>
    <fieldset>
		<label>Mot de passe</label>
		<input class="frm_text" name="password" placeholder="Mot de passe" type="password" autocomplete="off" data-validation="val_blank">
	</fieldset>
    <fieldset>
		<label>Statut</label>
		<select class="frm_chosen must ok" name="status" data-validation="val_blank">
			<option value="1" <?php if ($user['status'] == 1): ?>selected="selected"<?php endif; ?>>Actif</option>
			<option value="0" <?php if ($user['status'] == 0): ?>selected="selected"<?php endif; ?>>Inactif</option>
		</select>
	</fieldset>
    <fieldset class="mission-checkbox">
        <input type="checkbox" class="frm_checkbox" name="mail_notif_check" id="mail_notif_check" <?php if ($user['mail_notif'] == 1): ?>checked="checked"<?php endif; ?>>
        <label class="pointer" for="mail_notif_check">Notification Email <span class="glyphicon glyphicon-info-sign tooltips" title="L'utilisateur recevra une notification par courrier électronique du commentaire manager sur les missions"></span></label>
    </fieldset>
    <input type="hidden" name="mail_notif" id="mail_notif" value="<?php echo $user['mail_notif'] ?>">
    
    <div class="client_missions_select">
        <?php
        $client_id = $user['client_id'];
        $missions = Mission::getMissionsByClientId($client_id);
        $status = Control::getControlListByType(9);
        ?>
        <?php if (!empty($missions)): ?>
        <fieldset class="selectOption largemultiselect">
            <label><span class="pull-left">Missions client</span><span class="pull-right">Mission Affectés</span></label>
            <div class="frm_select_multi_wrapper">
                <select class="frm_select_multi must ok" data-validation="val_blank" multiple='multiple'>
                    <?php foreach($missions as $mission): ?>
                        <option value="<?php echo $mission['id'] ?>" <?php if (in_array($mission['id'], $assignedMissions)): ?>selected="selected"<?php endif; ?>>
                            <?php echo 'TDC ' .  $mission['id'] . ' - ' . $mission['poste'] . ' - [ ' .  $mission['manager_name'] . ' ] - [ ' . dateToFr($mission['date_debut']) . ' - ' . dateToFr($mission['date_fin']) . ' ] - [ ' . $status[$mission['status']] . ' ]'  ?>
                        </option>
                    <?php endforeach; ?>
                </select>
                <input type="hidden" name="mission_ids" value="<?php echo addSep($assignedMissions) ?>" class="frm_select_multi_helper must" data-validation="val_blank">
            </div>
        </fieldset>
        <fieldset>
            <button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="3"><span class="glyphicon glyphicon-pencil"></span> Éditer Utilisateur</button>
        </fieldset>
        
        <?php else: ?>
        <div class="message warning">Aucune mission trouvée pour ce client.</div>
        <?php endif; ?>

    </div>
        
</form>