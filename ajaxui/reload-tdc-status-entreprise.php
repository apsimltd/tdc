<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
$tdc = TDC::getTDCById(get('tdc_id'));
if (isGet()) { ?>
    <?php if ($tdc['status_entreprise'] == "" || $tdc['status_entreprise'] == 0): ?>
        <?php // do nothing ?>
    <?php elseif ($tdc['status_entreprise'] == 257): ?>
    <span class="status out">
        OUT
    </span>
    <?php elseif ($tdc['status_entreprise'] == 258): ?>
    <span class="status buminus">
        FINI
    </span>
    <?php endif; ?>
<?php
}
?>