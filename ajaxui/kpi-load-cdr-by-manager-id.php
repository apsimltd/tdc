<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('read_client_kpi', 'read_manager_kpi'))): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php if (getPost()) { ?>
    
    <?php $cdrs = User::getCDRByManagerId(getPost('manager_id')); // debug($cdrs); ?>
    
    <fieldset>
        <label>CDR</label>
        <select class="frm_chosen" name="cdr_id" id="cdr_id" data-validation="val_blank">
            <option value="">Choisir CDR</option>
            <?php foreach($cdrs as $cdr): ?>
            <option value="<?php echo $cdr['id'] ?>"><?php echo mb_strtoupper($cdr['lastName']) . ' ' . mb_ucfirst($cdr['firstName']) . ' - [' . $cdr['role_name'] . '] - [' . statut($cdr['status']) . ']' ?></option>
            <?php endforeach; ?>
        </select>
    </fieldset>

<?php } ?>