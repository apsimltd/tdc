<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_user') || !isGet()): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<h4 class="onboarding-title">Sélection de la société</h4>

<p style="margin-bottom: 15px; color: #008d4c;">Choisissez le format du fichier.</p>
   
<fieldset>
    <label>Format fichier</label>
    <select class="frm_chosen must ok" name="file_format" id="file_format" data-validation="val_blank">
        <option value="PDF">PDF</option>
        <option value="WORD">WORD</option>
    </select>
</fieldset>
	
<p style="margin-bottom: 15px; color: #008d4c;">Choisissez pour quelle entreprise vous voulez générer le fichier.</p>

<table class="table-list compact no-head no-hover">
    <tr>
        <td align="center">OPSEARCH</td>
        <td align="center">HEADHUNTING</td>
    </tr>
    <tr>
        <td align="center">
            <img data-entreprise_id="<?php echo get('entreprise_id') ?>" data-societe="opsearch" class="pdf-candidate-selector pdf-entreprise tooltips" title="OPSEARCH" src="<?php echo IMAGE_URL ?>/logo.png" alt="OPSEARCH">
        </td>
        <td align="center">
            <img data-entreprise_id="<?php echo get('entreprise_id') ?>" data-societe="headhunting" class="pdf-candidate-selector pdf-entreprise tooltips" title="HEADHUNTING FACTORY" src="<?php echo IMAGE_URL ?>/logo2.png" alt="HEADHUNDTING FACTORY">
        </td>
    </tr>
</table>