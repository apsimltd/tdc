<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Audit" ?>
<?php $page = "audit"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can('read_audit')) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>

	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base" class="Missions">

        <div id="contentWrap">
        
			<section class="title block">

				<h1 class="pull-left">Audit <span class="note">[Notez que les données AUDIT commencent à partir du 2020-01-28]</span></h1>
				
			</section>
            

			<?php require_once './views/audit-search-by-user.php'; ?>

            
            <div class="audit-listing">
                <!-- load by ajax search form -->
            </div>
            
        </div><!-- /contentWrap -->
        
    </div><!-- /base --> 
	
	<?php require_once './views/debug.php'; ?>  
    
</body>
</html>
<script>
$(document).ready(function(){
    
    $(document).on('change', '#module_code', function(){
        
        if ($(this).val() != "") {
            
            var url = AJAX_UI + '/audit-load-action-codes';
            $.ajax({
                url: url,
                dataType : 'html',
                type: 'POST',
                cache: false,
                data:{module_code:$(this).val()},
                success: function (result) {                
                    $('.load_action_code').html(result);
                }
            });
            
        } else {
            $('.load_action_code').html("");
        }
        
    });
    
    
    // see more details for watchog
    $(document).on('click', '.see_more_audit_watchdog', function(){
        var watchdog_id = $(this).data('watchdog_id');
        ajaxBusy = true;
        Modal.Show(AJAX_UI + '/watchdog-audit?id='+watchdog_id, null, null, '<span class="glyphicon glyphicon-time"></span>', 80, 'watchdog_audit');
    });
    
});

function frm_audit_ajax_success (result, status) {
    $('.audit-listing').html(result);
    Table.Init();
}

function frm_audit_by_user_ajax_success (result, status) {
    $('.audit-listing').html(result);
    Table.Init();
}

</script>