<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Réinitialiser mot de passe" ?>
<?php $page = "page-login"; ?>
<?php require_once './views/init.php'; ?>
<?php require_once './views/head.php'; ?>
	
    <?php require_once './views/header.php'; ?>
            
    <div class="login">

        <div class="login-title">Réinitialiser le mot de passe</div>
        
        <form class="frm_frm" name="frm_forget_pass" id="frm_forget_pass" action="<?php echo AJAX_HANDLER ?>/forget-pass" method="POST">
                
            <fieldset>
                <input type="text" name="email" class="frm_text must" placeholder="Email" autocomplete="off" data-validation="val_email">
                <i class="far fa-envelope"></i>
            </fieldset>

            <fieldset>
                <button type="button" id="forgetPassSubmit" class="btn btn-lg btn-primary btn-full frm_submit frm_notif" data-form="2">Réinitialiser</button>
            </fieldset>

            <a id="retour_login" href="<?php echo BASE_URL ?>/connexion" title="Retour Connexion">Retour Connexion</a>

        </form>

    </div><!-- / login -->
    
    <?php require_once './views/debug.php'; ?>
    
</body>
</html>
<script>
$(document).ready(function(){
    
    // login page
    ForgetPass.Init();
    $(window).resize(function(){
        ForgetPass.Init();
    });
    ForgetPass.SubmitOnEnter();
    
});

var ForgetPass = {
    Init: function (){
        if ($('body').hasClass('page-login')) {
            var height = $(window).height() - (($('#header').height()) * 2);
            $('body').height(height);
        }
    },
    SubmitOnEnter: function() {
        $(document).on('keypress', '#frm_forget_pass input.frm_text', function(event){
            var code = event.keyCode ? event.keyCode : event.which;
            if (code == 13) { // on pressing carriage return
                $('#forgetPassSubmit').trigger('click');
            }
        });
    },
}
</script>
