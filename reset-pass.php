<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Réinitialiser mot de passe" ?>
<?php $page = "page-login"; ?>
<?php 
    if (get('token') && !(empty(get('token')))) {
        $token = filter_input(INPUT_POST | INPUT_GET, 'token', FILTER_SANITIZE_SPECIAL_CHARS);
        $email = User::TokenVerify($token);
        if (!$email) {
            
            $notif = array(
                'status' => 'NOK',
                'msg' => 'Le lien est expiré ou n\'est pas valide. Veillez réinitialiser votre mot de passe.',
                'type' => 'error', 
            );
            setNotifCookie($notif);
            header('Location:'. BASE_URL . '/connexion');
            
        } 
        
    } else { // redirect to login
        
        header("Location:" . BASE_URL . "/connexion");
        
    }    
?>
<?php require_once './views/init.php'; ?>
<?php require_once './views/head.php'; ?>
	
    <?php require_once './views/header.php'; ?>
	
    <div class="login">

        <div class="login-title">Réinitialiser le mot de passe</div>
        
        <form class="frm_frm" name="frm_reset_pass" id="frm_reset_pass" action="<?php echo AJAX_HANDLER ?>/reset-pass" method="POST">
            
            <?php if (isset($email)): ?><input type="hidden" name="user_email" id="user_email" value="<?php print $email ?>" /><?php endif; ?>
            
            <fieldset>
                <input type="password" name="user_pass" id="user_pass" class="frm_text must pass" placeholder="Nouveau mot de passe" autocomplete="off" data-validation="val_blank">
                <span class="glyphicon glyphicon-lock"></span>
                <i class="fas fa-eye togglePassword"></i>
            </fieldset>

            <fieldset>
                <button type="button" id="resetPassSubmit" class="btn btn-lg btn-primary frm_submit frm_notif" data-form="2">Réinitialiser</button>
            </fieldset>

        </form>

    </div><!-- / login -->
        
</body>
</html>
<script>
$(document).ready(function(){
    
    // login page
    ForgetPass.Init();
    $(window).resize(function(){
        ForgetPass.Init();
    });
    ForgetPass.SubmitOnEnter();
    
    Password.Toggle();
    
});

var Password = {
    Toggle: function () {
        $(document).on('click', '.togglePassword', function(){
            if ($(this).hasClass('fa-eye-slash')) {
                $(this).removeClass('fa-eye-slash').addClass('fa-eye').removeClass('active');
                $(this).parent('fieldset').find('input.pass').attr('type', 'password');
            } else if ($(this).hasClass('fa-eye')) {
                $(this).removeClass('fa-eye').addClass('fa-eye-slash').addClass('active');
                $(this).parent('fieldset').find('input.pass').attr('type', 'text');
            }
        });
    },
}

var ForgetPass = {
    Init: function (){
        if ($('body').hasClass('page-login')) {
            var height = $(window).height() - (($('#header').height()) * 2);
            $('body').height(height);
        }
    },
    SubmitOnEnter: function() {
        $(document).on('keypress', '#frm_reset_pass input.frm_text', function(event){
            var code = event.keyCode ? event.keyCode : event.which;
            if (code == 13) { // on pressing carriage return
                $('#frm_reset_pass').trigger('click');
            }
        });
    },
}
</script>
