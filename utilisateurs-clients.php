<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Utilisateurs Client" ?>
<?php $page = "users"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('create_user', 'read_user', 'edit_user'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>
	
    <?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base">

        <div id="contentWrap">
        
			<section class="title block">

				<h1 class="pull-left">Utilisateurs Client</h1>
				
                <?php if (User::can('create_user')): ?>
				<div class="btn-box pull-right">                        
					<button type="button" class="add_user btn btn-icon btn-primary pull-right tooltips" title="Ajouter Utilisateur Client"><span class="glyphicon glyphicon-plus"></span></button>
				</div>
                <?php endif; ?>
				
			</section>
			
            <?php if (User::can('read_user', 'edit_user')): ?>
            <div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-users"></i> Liste des utilisateurs client</header>
                        </div>
						
						<div class="block-body">                   
							<?php $users = User::getUsersClient(); //debug($users) ?>
                            <?php $status_mission = Control::getControlListByType(9); ?>
                            <table class="datable stripe hover">
                                <thead>
                                    <tr>
                                        <th>Ref</th>
										<th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Identifiant</th>
                                        <th>Email</th>
                                        <th>Client</th>
										<th>Missions Affectés</th>
										<th>Date de création</th>
										<th>Dernière connexion</th>
										<th>Statut</th>
                                        <?php if (User::can('edit_user')): ?>
                                        <th>Actions</th>
                                        <?php endif; ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($users as $user): ?>
                                    <tr>
                                        <td><?php echo $user['id'] ?></td>
                                        <td><?php echo mb_strtoupper($user['lastName']) ?></td>
                                        <td><?php echo mb_ucfirst($user['firstName']) ?></td>
                                        <td><?php echo $user['username'] ?></td>
										<td><?php echo $user['email'] ?></td>
                                        <td><?php echo $user['client_name'] ?></td>
                                        <td>
                                            <?php $missions = Mission::getMissionsByAssignedClientUserId($user['id']); // debug($missions); ?>
                                            
                                            <?php foreach($missions as $mission): ?>
                                            <a href="<?php echo BASE_URL ?>/editer-mission?id=<?php echo $mission['id'] ?>" class="mission-list" target="_blank"> <?php echo 'TDC ' .  $mission['id'] . ' - ' . $mission['poste'] . ' - [ ' .  $mission['manager_name'] . ' ] - [ ' . dateToFr($mission['date_debut']) . ' - ' . dateToFr($mission['date_fin']) . ' ] - [ ' . $status_mission[$mission['status']] . ' ]'  ?></a>
                                            <?php endforeach; ?>
                                        </td>
										<td data-order="<?php echo $user['created'] ?>">
                                            <span title="<?php echo $user['created'] ?>"><?php echo dateToFr($user['created'], true) ?></span>
                                        </td>
										<td data-order="<?php echo $user['lastLogin'] ?>">
                                            <span title="<?php echo $user['lastLogin'] ?>"><?php echo dateToFr($user['lastLogin'], true) ?></span>
                                        </td>
										<td align="center">
                                            <span class="glyphicon <?php if (intval($user['status']) == 1): ?>table-active glyphicon-ok<?php else: ?>table-inactive glyphicon-remove<?php endif; ?>"></span>
                                        </td>
                                        <?php if (User::can('edit_user')): ?>
                                        <td class="actions_button">
                                            <button type="button" data-user_id="<?php echo $user['id'] ?>" class="edit_user btn btn-icon btn-primary tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>                                        
                                        </td>
                                        <?php endif; ?>
                                    </tr>
                                    <?php endforeach; ?>						
                                </tbody>
                            </table>
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
            <?php endif; ?>
        
        </div><!-- /contentWrap -->
        
    </div><!-- /base -->
    
	<?php require_once './views/debug.php'; ?>    
    
</body>
</html>
<script>
$(document).ready(function(){
    
    // checked and unchecked include mission in kpi
    $(document).on('ifUnchecked', '#mail_notif_check', function(){
        $('#mail_notif').val('null');
    });
    $(document).on('ifChecked', '#mail_notif_check', function(){
        $('#mail_notif').val('1');
    });
    
    <?php if (User::can('create_user')): ?>User.AddUser();<?php endif; ?>
    <?php if (User::can('edit_user')): ?>User.EditUser();<?php endif; ?>
    User.ShowClientMissions();
    User.ShowClientMissionsEdit();
});

var User = {
    <?php if (User::can('create_user')): ?>
    AddUser: function (){
        $(document).on('click', '.add_user', function (){
            Modal.Show(AJAX_UI + '/add-user-client', null, null, '<i class="fa fa-users"></i>', 80);
        });
    },
    <?php endif; ?>
    <?php if (User::can('edit_user')): ?>
    EditUser: function (){
        $(document).on('click', '.edit_user', function (){
            var user_id = $(this).attr('data-user_id');
            Modal.Show(AJAX_UI + '/edit-user-client?id='+user_id, null, null, '<i class="fa fa-users"></i>', 80);
        });
    },
    <?php endif; ?>
    ShowClientMissions: function() {
        $(document).on('change', '#add_user_client_select', function(){
            
            if ($(this).val() !== "") {
                var url = AJAX_UI + '/show-client-missions';
                $.ajax({
                    url: url,
                    dataType : 'html',
                    type: 'POST',
                    cache: false,
                    data:{client_id:$(this).val()},
                    success: function (response) {                
                        $('.client_missions_select').html(response);
                    }
                });
            } else {
                $('.client_missions_select').html("");
            }
                
        });
    },
    ShowClientMissionsEdit: function() {
        $(document).on('change', '#add_user_client_select_edit', function(){
            
            if ($(this).val() !== "") {
                var url = AJAX_UI + '/show-client-missions-edit';
                $.ajax({
                    url: url,
                    dataType : 'html',
                    type: 'POST',
                    cache: false,
                    data:{client_id:$(this).val()},
                    success: function (response) {                
                        $('.client_missions_select').html(response);
                    }
                });
            } else {
                $('.client_missions_select').html("");
            }
                
        });
    },
}

<?php if (User::can('create_user')): ?>
function frm_create_user_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}
<?php endif; ?>
    
<?php if (User::can('edit_user')): ?>
function frm_edit_user_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}
<?php endif; ?>
</script>