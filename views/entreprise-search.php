<div class="row">
				
	<div class="col col-12">
		<div class="block nopadding">
			
			<div class="block-head with-border">
				<header><i class="os-icon os-icon-ui-37"></i>Rechercher entreprises dans la BDD</header>
			</div>
			
			<div class="block-body">
                
                <p style="color: #090; margin-bottom: 10px; font-weight: bold;">* Au moins un champ doit être sélectionné pour rechercher une entreprise</p>
                
                <?php 
                $url = AJAX_HANDLER . '/search-entreprise';
                if (get('tdc_id')) {
                    $url .= '?tdc_id=' . get('tdc_id');
                }
                ?>
                
				<form class="frm_frm frm_ajax frm_horizontal" name="frm_search_entreprise" id="frm_search_entreprise" data-url="<?php echo $url ?>" data-type="html">
                    
                    <?php if (get('tdc_id') && get('tdc_id') != ""): ?>
                        <input type="hidden" name="mission_id" value="<?php echo get('tdc_id') ?>">
                    <?php endif; ?>
                    
<!--                    <fieldset class="single-row">
                        <input type="checkbox" class="frm_checkbox" id="is_mother" name="is_mother" style="width: 10px;">
						<label class="pointer" for="is_mother">Entreprise Mère</label>
					</fieldset>    -->
                        
					<fieldset>
						<input class="frm_text caps" name="name" placeholder="Nom" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
                    <fieldset>
						<input class="frm_text tel_fr" name="telephone" placeholder="Téléphone" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>        
                    <fieldset>
						<input class="frm_text" name="email" placeholder="Email" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
                    <fieldset>
                        <select class="frm_chosen" name="control_localisation_id" data-validation="val_blank">
                            <option value="">Choisir Localisation</option>
                            <?php $controls = Control::getControlByType(6, 1) ?>
                            <?php foreach($controls as $control): ?>
                            <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </fieldset>
                    <fieldset>
                        <select class="frm_chosen" name="secteur_activite_id" data-validation="val_blank">
                            <option value="">Choisir Secteur</option>
                            <?php $controls = Control::getControlByType(2, 1) ?>
                            <?php foreach($controls as $control): ?>
                            <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </fieldset>
                    <fieldset>
						<input class="frm_text" name="sous_secteur_activite" placeholder="Sous Secteur Activité" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
                    
					<fieldset class="submit">
						<button type="button" class="btn btn-success frm_before_submit frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt os-icon os-icon-ui-37"></i> Rechercher</button>
					</fieldset>
				</form>
				
			</div><!-- / block-body -->
			
		</div>
	</div><!-- /col -->
	
</div><!-- / row -->
