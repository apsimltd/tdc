<div class="row">
				
	<div class="col col-12">
		<div class="block nopadding">
			
			<div class="block-head with-border">
				<header><i class="os-icon os-icon-ui-37"></i>Rechercher audit dans la BDD</header>
			</div>
			
			<div class="block-body">
                
                <p style="color: #090; margin-bottom: 10px; font-weight: bold;">* Au moins un champ doit être sélectionné pour la recherche</p>
                                
				<form class="frm_frm frm_ajax frm_horizontal" name="frm_audit" id="frm_audit" data-url="<?php echo AJAX_UI ?>/audit-search" data-type="html">
                    
                    <fieldset>
                        <select class="frm_chosen must" name="module_code" id="module_code" data-validation="val_blank">
                            <option value="">Choisir Module</option>
                            <?php $modules = Watchdog::$audit_modules; ?>
                            <?php foreach($modules as $key => $value): ?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                            <?php endforeach; ?>
                        </select>
					</fieldset>
                    
                    <div class="load_action_code">
                        
                    </div><!--/load_action_code -->
                    
					<fieldset class="submit">
						<button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt os-icon os-icon-ui-37"></i> Rechercher</button>
					</fieldset>
				</form>
				
			</div><!-- / block-body -->
			
		</div>
	</div><!-- /col -->
	
</div><!-- / row -->