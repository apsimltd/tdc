<div id="tdc_summary">

	<div class="row tdc_summary_wrapper clearfix">
		        
		<span class="triggerSummary glyphicon glyphicon-chevron-down open <?php if (isset($_SESSION['tdc_summary']) && $_SESSION['tdc_summary'] == 'close'): ?>active<?php endif; ?> tooltips" title="Ouvrir"></span>
		<span class="triggerSummary glyphicon glyphicon-chevron-up close <?php if (isset($_SESSION['tdc_summary']) && $_SESSION['tdc_summary'] == 'open'): ?>active<?php endif; ?> tooltips" title="Fermer"></span>
		
		<div class="col col-12">
			<div class="block nopadding">

				<div class="block-head with-border">
					<header>
						<span class="glyphicon glyphicon-screenshot"></span>Tableau de Chasse : 
                        <span style="color:#7cbd5a; font-size: 12px; padding-left: 10px;"><?php echo $mission['id'] . ' - ' . $mission['nom_client'] . ' - ' . $mission['poste'] . ' - ' . dateToFr($mission['date_debut']) . ' - ' . dateToFr($mission['date_fin']) ?> / <?php if ($mission['prestataire_id'] == 1): echo "OPSEARCH"; elseif($mission['prestataire_id'] == 2) : echo "HF"; endif; ?></span>
					</header>
					<div class="block-head-btns pull-right">
                        
                        <a href="<?php echo BASE_URL ?>/editer-mission?id=<?php echo get('id') ?>&tdc_id=<?php echo get('id') ?>"><button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Éditer Mission"><span class="glyphicon glyphicon-edit"></span></button></a>

                        <a href="<?php echo BASE_URL ?>/candidats?tdc_id=<?php echo get('id') ?>"><button type="button" class="btn btn-icon btn-success pull-right tooltips" title="Rechercher le candidat dans la BDD"><span class="glyphicon glyphicon-user"></span></button></a>
                        
                        <a href="<?php echo BASE_URL ?>/entreprises?tdc_id=<?php echo get('id') ?>"><button type="button" class="btn btn-icon btn-primary pull-right tooltips" title="Rechercher entreprise dans la BDD"><span class="glyphicon glyphicon-tower"></span></button></a>
                        
                        <a href="<?php echo BASE_URL ?>/excel?id=<?php echo get('id') ?>"><button type="button" class="btn btn-icon btn-warning pull-right tooltips" title="Exporter en format Excel"><span class="glyphicon glyphicon-export"></span></button></a>
                        
						<button type="button" data-mission_id="<?php echo get('id') ?>" class="btn tdc_pdf_client btn-icon btn-warning pull-right tooltips" title="Télécharger TDC Client"><i class="fa fa-users"></i></button>

						<button type="button" data-mission_id="<?php echo get('id') ?>" class="btn tdc_pdf_anonyme btn-icon btn-warning pull-right tooltips" title="Télécharger TDC Anonyme"><i class="fa fa-file-pdf"></i></button>
                        
                        <button type="button" data-mission_id="<?php echo get('id') ?>" class="btn tdc_map btn-icon btn-success pull-right tooltips" title="Map"><i class="fa fa-globe"></i></button>

					</div>
				</div>

				<div class="block-body tdc clearfix <?php if ($_SESSION['tdc_summary'] == 'close'): ?>hide<?php endif; ?>">
					
					<div class="block-body-wrap clearfix">
						
						<div class="box tdc-stats-wrap pull-left clearfix">

							<div class="box-inner">
								<div class="box-inner-icon pull-left">
									<i class="os-icon os-icon-bar-chart-stats-up"></i>
								</div>
								<div class="box-inner-content pull-left clearfix" id="stats-tdc-wrapper">
									
									<div class="stats-wrapper clearfix">
										<div class="col-stats pull-left clearfix">
                                            <div class="nano">
                                                <div class="nano-content">
                                                    <ul>
<!--                                                        <li>Entreprises identifiées : <strong><?php ///echo TDC::getTotalEntrepriseInTDC($mission['id']) ?></strong></li>-->
                                                        <li>Entreprises identifiées : <strong><?php echo TDC::getTotalSocieteinTDC_V2($mission['id']) ?></strong></li>
                                                        <li>Entreprises à approcher : <strong><?php echo TDC::getTotalEntrepriseInTDCAAprocher($mission['id']) ?></strong></li>
                                                        <li>Entreprises OUT : <strong><?php echo TDC::getTotalEntrepriseInTDCOUT($mission['id']) ?></strong></li>
                                                        <li>Entreprises FINI : <strong><?php echo TDC::getTotalEntrepriseInTDCFINI($mission['id']) ?></strong></li>
                                                        
                                                        <li>Candidats identifiés : <strong><?php echo TDC::getTotalCandidatInTDC($mission['id']) ?></strong></li>
                                                        <li>Candidats coeur de cible : <strong><?php echo TDC::getTotalCandidatCCCinTDC($mission['id']) ?></strong></li>
                                                        <li>Candidats approchés : <strong><?php echo TDC::getTotalCandidatApprocheinTDC($mission['id']) ?></strong></li>
                                                        <li>Candidats restants à approcher : <strong><?php echo TDC::getTotalCandidatRestantApprocheinTDC($mission['id']) ?></strong></li>
                                                        
                                                        <li>Âge moyen : <strong><?php echo TDC::getAverageAgeInTDC($mission['id']) ?></strong></li>
                                                        <li>Package Salarial moyenne : <strong><?php echo TDC::getAverageRemuneration($mission['id']) ?> K€</strong></li>
                                                        <li>Salaire fixe moyenne : <strong><?php echo TDC::getAverageSalairefixe($mission['id']) ?> K€</strong></li>
                                                        
                                                        <li><strong>Localisation</strong></li>
                                                        <?php $locas = TDC::getLocalisationGrouped($mission['id']); //debug($locas); ?>
                                                        <?php foreach($locas as $loca): ?>
                                                        <li class="indented"><?php echo $loca['name'] ?> : <strong><?php echo $loca['Total'] ?></strong></li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            </div>        
										</div>
										<div class="col-stats pull-left clearfix">
                                            <div class="nano">
                                                <div class="nano-content">
                                                    <ul>
                                                        <?php // in => 229, out => 226, bu+ => 228, bu- => 227 ?>
                                                        <li>IN : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission['id'], 229) ?></strong></li>
                                                        <li>BU+ : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission['id'], 228) ?></strong></li>
                                                        <li>BU- : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission['id'], 227) ?></strong></li>
                                                        <li>Ex BU : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission['id'], 259) ?></strong></li>
                                                        
                                                        <li>OUT : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission['id'], 226) ?></strong></li>
                                                        <li>ARC : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission['id'], 251) ?></strong></li>
                                                        <li>ARCA : <strong><?php echo TDC::getTotalCandidatinTDCByStatus($mission['id'], 252) ?></strong></li>
                                                    </ul>
                                                </div>
                                            </div>        
										</div>
										<div class="col-stats pull-left clearfix">
                                            <div class="nano">
                                                <div class="nano-content">
                                                    <ul>
<!--                                                        <li>Société Candidats identifiées : <strong><?php ///echo TDC::getTotalSocieteinTDC($mission['id']) ?></strong></li>-->
                                                        <?php $out_categories = Mission::getOutCategory($mission['id']) ?>
                                                        <?php foreach($out_categories as $out): ?>
                                                        <li><?php echo $out['out_categorie'] ?> : <strong><?php echo TDC::getTotalByOutCategoryinTDC($mission['id'], $out['id']) ?></strong></li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            </div>        
										</div>
									</div><!-- /stats-wrapper -->
									<div class="stats-filter clearfix">
										
										<form class="frm_frm frm_ajax frm_horizontal clearfix" name="frm_tdc_stats" id="frm_tdc_stats" data-url="<?php echo AJAX_HANDLER ?>/filter-tdc-stats" data-type="json">
                                            
                                            <input type="hidden" name="mission_id" value="<?php echo $mission['id'] ?>">
                                            
											<fieldset>
                                                <button type="button" class="btn btn-icon btn-warning pull-right filter_stats_debut" data-mission_id="<?php echo $mission['id'] ?>"><span class="glyphicon glyphicon-calendar"></span> Depuis le début</button>
											</fieldset>
                                            <fieldset>
                                                <select class="frm_chosen must" id="tdc_stats_type" name="type" data-validation="val_blank">
                                                    <option value="">Choisir Filtrer Par</option>
                                                    <option value="date_prise_contact">Date Prise Contact</option>
                                                    <option value="date_ajoute">Date Ajouté</option>
                                                </select>   
                                            </fieldset>
											<fieldset>
                                                <input class="frm_text must" id="start_date" name="start_date" placeholder="Date de début" type="text" autocomplete="off" data-validation="val_blank">
											</fieldset>
											<fieldset>
                                                <input class="frm_text must" id="end_date" name="end_date" placeholder="Date de fin" type="text" autocomplete="off" data-validation="val_blank">
											</fieldset>
                                            <input type="hidden" name="tdc_legende_color_ids" id="tdc_legende_color_ids">
                                            <input type="hidden" name="tdc_legende_entreprise_color_ids" id="tdc_legende_entreprise_color_ids">
                                            <input type="hidden" name="tdc_legende_out_categorie_id" id="tdc_legende_out_categorie_id">
											<fieldset class="submit">
                                                <button type="button" class="btn btn-success frm_submit frm_notif" id="tdc_stats_filter_submit" data-form="2"><span class="glyphicon glyphicon-filter"></span> Filtrer</button>
											</fieldset>
										</form>
										
									</div><!-- /stats-filter -->

								</div><!-- /box-inner-content -->
							</div><!-- box-inner -->

						</div><!-- /tdc-stats-wrap -->

						<div class="box tdc-files-wrap pull-left clearfix">

							<div class="box-inner">
								<div class="box-inner-icon pull-left">
									<i class="os-icon os-icon-documents-03"></i>
								</div>
								<div class="box-inner-content pull-left clearfix">
									<div class="files-list pull-left clearfix">
										<div class="nano">
											<div class="nano-content">
												<?php if (!empty($documents)) :?>
													<ul class="links_candidat">
													<?php foreach($documents as $document): ?>
														<?php 
														$imgType = array('jpg', 'jpeg', 'png');
														$ext = getFileExt($document['file']);
														if (in_array($ext, $imgType)):
														?>
														<li>
															<a href="<?php echo MISSION_URL ?>/<?php echo $mission['id'] ?>/<?php echo $document['file'] ?>" data-lightbox="<?php echo $document['file'] ?>"><?php echo $document['file'] ?></a>
															<span class="glyphicon glyphicon-trash delete_document tooltips" title="Supprimer" data-document_id="<?php echo $document['id'] ?>" data-mission_id="<?php echo $mission['id'] ?>"></span>
														</li>
														<?php else: ?>
														<li>
															<a href="<?php echo BASE_URL ?>/download?type=miss&id=<?php echo $document['id'] ?>"><?php echo $document['file'] ?></a>
															<span class="glyphicon glyphicon-trash delete_document tooltips" title="Supprimer" data-document_id="<?php echo $document['id'] ?>" data-mission_id="<?php echo $mission['id'] ?>"></span>
														</li>
														<?php endif; ?>
													<?php endforeach;?>
													</ul>
												<?php endif; ?>
											</div><!--/ nano-content -->
										</div><!-- /nano -->
									</div><!-- /files-list -->
									<div class="add-files clearfix">
										<form class="frm_frm frm_upload_file_tdc frm_horizontal clearfix" name="frm_edit_mission_add_files" action="<?php echo AJAX_HANDLER ?>/add-mission-file?src=tdc" method="POST" enctype="multipart/form-data">
											<input type="hidden" name="mission_id" value="<?php echo $mission['id'] ?>">
										
											<fieldset class="uploadMultiple">

												<label class="pull-left">Documents <span class="glyphicon glyphicon-info-sign tooltips" title="Les fichiers autorisés sont : .doc, .docx, .xls, .xlsx, .pdf, .jpg, .jpeg, .png ; Taille max : 10 Mo"></span></label>
                                                <input type="hidden" name="uploadFlag" id="uploadFlag" value="1">
												<div class="clearfix add_field_wrap_documents pull-left" style="width:70%;">
													<div class="add_field_row pull-left">
														<input type="file" name="file[]" id="file_1" data-number="1" class="inputfile inputfile-1">
														<label id="uploadLabel_1" for="file_1" class="fileUpload pull-left"><span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span></label>
														<button type="button" class="clear_field_upload btn btn-icon btn-danger pull-left tooltips" style="margin-top:1px;" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>
													</div>
												</div>
                             
											</fieldset>
											
											<fieldset class="submit">
												<button type="button" class="btn btn-success frm_submit frm_before_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter</button>
											</fieldset>
										
										</form>
                                        
<!--                                        <div class="triggerUploadTDC">
                                            <button type="button" data-mission_id="<?php //echo $mission['id'] ?>" class="btn btn-icon btn-success uploadTDCDocument"><span class="glyphicon glyphicon-upload"></span> Télécharger un document</button>
                                        </div> /triggerUploadTDC -->
                                        
									</div> <!--/add-files--> 
								</div><!-- /box-inner-content -->
							</div><!-- /box-inner -->

						</div><!-- /tdc-files-wrap -->
						
					</div><!-- /block-body-wrap -->  

				</div><!-- / block-body -->

			</div>
		</div><!-- /col -->

	</div><!-- / row -->
	
</div><!-- /tdc_summary -->