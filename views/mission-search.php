<div class="row">
				
	<div class="col col-12">
		<div class="block nopadding">
			
			<div class="block-head with-border">
				<header><i class="os-icon os-icon-ui-37"></i>Rechercher missions dans la BDD</header>
			</div>
			
			<div class="block-body">
                
                <p style="color: #090; margin-bottom: 10px; font-weight: bold;">* Au moins un champ doit être sélectionné pour rechercher une mission</p>
                                
				<form class="frm_frm frm_ajax frm_horizontal" name="frm_search_mission" id="frm_search_mission" data-url="<?php echo AJAX_HANDLER ?>/search-mission" data-type="html">
                    
                    <fieldset>
						<input class="frm_text xsmall" name="id" placeholder="Ref" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>                
					<fieldset>
						<select class="frm_chosen" name="prestataire_id" data-validation="val_blank">
                            <option value="">Choisir Prestataire</option>
                            <?php $prestataires = Mission::getPrestataires() ?>
                            <?php foreach($prestataires as $presta): ?>
                            <option value="<?php echo $presta['id'] ?>"><?php echo $presta['nom'] ?></option>
                            <?php endforeach; ?>
                        </select>
					</fieldset>
                    <fieldset>
                        <select class="frm_chosen" name="client_id" data-validation="val_blank">
                            <option value="">Choisir Client</option>
                            <?php $clients = Client::getClients('nom') ?>
                            <?php foreach($clients as $client): ?>
                            <option value="<?php echo $client['id'] ?>">
                                <?php echo $client['id'] . ' - ' . mb_strtoupper($client['nom']) ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </fieldset>
                    <fieldset>
                        <select class="frm_chosen" name="consultant_id" data-validation="val_blank">
                            <option value="">Choisir Consultant</option>
                            <?php $consultants = User::getUsersByRole('Consultant') ?>
                            <?php foreach($consultants as $consultant): ?>
                            <option value="<?php echo $consultant['id'] ?>">
                                <?php echo mb_ucfirst($consultant['firstName']) . ' ' . mb_strtoupper($consultant['lastName']) ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </fieldset>
					<fieldset>
						<input class="frm_text" name="poste" placeholder="Poste" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
                    <fieldset>
						<select class="frm_chosen" name="control_secteur_id" data-validation="val_blank">
							<option value="">Choisir Secteur</option>
							<?php $controls = Control::getControlByType(2, 1) ?>
							<?php foreach($controls as $control): ?>
							<option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
							<?php endforeach; ?>
						</select>
					</fieldset>
					<fieldset>
						<select class="frm_chosen" name="control_localisation_id" data-validation="val_blank">
							<option value="">Choisir Localisation</option>
							<?php $controls = Control::getControlByType(6, 1) ?>
							<?php foreach($controls as $control): ?>
							<option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
							<?php endforeach; ?>  
						</select>
					</fieldset>
                    <fieldset>
						<select class="frm_chosen" name="control_fonction_id" data-validation="val_blank">
							<option value="">Choisir Fonction</option>
							<?php $controls = Control::getControlByType(1, 1) ?>
							<?php foreach($controls as $control): ?>
							<option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
							<?php endforeach; ?>
						</select>
					</fieldset>
					<fieldset>
						<select class="frm_chosen" name="control_niveauFormation_id" data-validation="val_blank">
							<option value="">Choisir Niveau</option>
							<?php $controls = Control::getControlByType(4, 1) ?>
							<?php foreach($controls as $control): ?>
							<option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
							<?php endforeach; ?>
						</select>
					</fieldset>
                    <fieldset>
                        <select class="frm_chosen" name="status" data-validation="val_blank">
                            <option value="">Choisir Statut</option>
                            <?php $controls = Control::getControlListByType(9) ?>
                            <?php foreach($controls as $key => $value): ?>
                            <option value="<?php echo $key ?>">
                                <?php echo $value ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </fieldset>
					<fieldset class="single-row">
						<label>Âge</label>
						<input class="frm_text xxsmall" name="age_from" placeholder="Entre" type="text" autocomplete="off" data-validation="val_num">
						<span class="interval"> - </span>
						<input class="frm_text xxsmall" name="age_to" placeholder="Et" type="text" autocomplete="off" data-validation="val_num">
					</fieldset>
					<fieldset class="single-row">
						<label>Rémunération</label>
						<input class="frm_text xsmall" name="remuneration_from" placeholder="Entre" type="text" autocomplete="off" data-validation="val_num">
						<span class="interval"> - </span>
						<input class="frm_text xsmall" name="remuneration_to" placeholder="Et" type="text" autocomplete="off" data-validation="val_num">
					</fieldset>
                    
                    <fieldset class="single-row">
						<input type="checkbox" class="frm_checkbox" id="allMission" name="all">
						<label class="pointer" for="allMission">Visualiser toutes les missions</label>
					</fieldset>
                    
					<fieldset class="submit">
						<button type="button" class="btn btn-success frm_before_submit frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt os-icon os-icon-ui-37"></i> Rechercher</button>
					</fieldset>
				</form>
				
			</div><!-- / block-body -->
			
		</div>
	</div><!-- /col -->
	
</div><!-- / row -->