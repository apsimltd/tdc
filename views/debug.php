<?php if (isDev() && $debug): ?>
<div id="debug">
    <div class="debug-head">
        <h1>OPSEARCH DEBUG ON</h1>
        <i class="os-icon os-icon-close tooltips" title="Fermer"></i>
    </div>
    <div class="debug-body">
        <h1>User Details</h1>
        <div class="dump">
            <?php if (User::isLoggedIn()): ?>
                <?php debug($me) ?>
            <?php else: ?>
                User not logged In:: <?php debug($me) ?>
            <?php endif; ?>
        </div>
        <h1>Session Alert RDVP</h1>
        <div class="dump">
            <?php if (User::isLoggedIn() && isset($_SESSION['opsearch_rdvp'])): ?>
                <?php debug($_SESSION['opsearch_rdvp']) ?>
            <?php else: ?>
                No Session alert RDVP
            <?php endif; ?>
        </div>
        <h1>POST Data</h1>
        <div class="dump">
            <?php if (isset($_POST)): ?>
                <?php debug($_POST) ?>
            <?php else: ?>
                No POST data
            <?php endif; ?>
        </div>
        <h1>GET data</h1>
        <div class="dump">
            <?php if (isset($_GET)): ?>
                <?php debug($_GET) ?>
            <?php else: ?>
                No GET data
            <?php endif; ?>
        </div>
        <h1>Session Id</h1>
        <div class="dump">
            <?php debug(session_id()); ?>
        </div>
    </div><!-- /debug-body -->
</div><!-- /debug -->
<?php endif; ?>

