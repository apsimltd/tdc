<?php
// when user is logged in
if (User::isLoggedIn() && $page == 'page-login') {
    header("Location:" . BASE_URL . "/tableau-de-bord"); 
} elseif (!User::isLoggedIn() && $page != 'page-login'){
    header("Location:" . BASE_URL . "/connexion"); 
}
?>