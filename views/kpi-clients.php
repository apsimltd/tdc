<div class="row">
				
	<div class="col col-12">
		<div class="block nopadding">
						
			<div class="block-body">
                                
<!--                <div class="message warning" style="margin-bottom: 10px;"><strong>A noter qu'il n'y a pas des KPIs pour les missions avant le 2020-02-01</strong> </div>-->
                                
				<form class="frm_frm frm_ajax frm_horizontal" name="frm_kpi_clients" id="frm_kpi_clients" data-url="<?php echo AJAX_HANDLER ?>/kpi-clients-search" data-type="html">
                    
                    <fieldset class="single-row">
                        <label>Date Début Mission</label>
                        <input class="frm_text start_date small" id="start_date" name="start_date" placeholder="Dé" type="text" autocomplete="off" data-validation="val_blank">
                        <span class="interval"> - </span>
                        <input class="frm_text end_date small" id="end_date" name="end_date" placeholder="À" type="text" autocomplete="off" data-validation="val_blank">
                    </fieldset>
                    
                    <fieldset>
                        <label>Client</label>
                        <select class="frm_chosen" name="client_id" id="client_id" data-validation="val_blank">
                            <option value="">Choisir Client</option>
                            <?php $clients = Client::getClients('nom') ?>
                            <?php foreach($clients as $client): ?>
                            <option value="<?php echo $client['id'] ?>"><?php echo $client['nom'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </fieldset>
                                        
                    <div class="kpi_filter_mission">
                        
                    </div><!-- /kpi_filter_mission -->
                    
					<fieldset class="submit">
						<button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt os-icon os-icon-ui-37"></i> Rechercher</button>
					</fieldset>
				</form>
				
			</div><!-- / block-body -->
			
		</div>
	</div><!-- /col -->
	
</div><!-- / row -->