<div class="row">
				
	<div class="col col-12">
		<div class="block nopadding">
						
			<div class="block-body">
                
                <p style="color: #090; margin-bottom: 10px; font-weight: bold;">* Au moins un champ doit être sélectionné</p>
                                
				<form class="frm_frm frm_ajax frm_horizontal" name="frm_kpi_manager" id="frm_kpi_manager" data-url="<?php echo AJAX_HANDLER ?>/kpi-manager-search" data-type="html">
                    
                    <fieldset class="single-row">
                        <label>Date</label>
                        <input class="frm_text start_date small" id="start_date" name="start_date" placeholder="Dé" type="text" autocomplete="off" data-validation="val_blank">
                        <span class="interval"> - </span>
                        <input class="frm_text end_date small" id="end_date" name="end_date" placeholder="À" type="text" autocomplete="off" data-validation="val_blank">
                    </fieldset>
                    
                    <fieldset>
                        <label>Manager</label>
                        <select class="frm_chosen must" name="manager_id" id="manager_id" data-validation="val_blank">
                            <option value="">Choisir Manager</option>
                            <?php $managers = User::getKPIManagers() ?>
                            <?php foreach($managers as $manager): ?>
                            <option value="<?php echo $manager['id'] ?>"><?php echo mb_strtoupper($manager['lastName']) . ' ' . mb_ucfirst($manager['firstName']) . ' - [' . $manager['role_name'] . '] - [' . statut($manager['status']) . ']' ?></option>
                            <?php endforeach; ?>
                        </select>
                    </fieldset>
                    
                    <div class="kpi_filter_mission">
                        
                    </div><!-- /kpi_filter_mission -->
                    
                    <div class="kpi_filter_cdr">
                        
                    </div><!-- /kpi_filter_cdr -->
                    
					<fieldset class="submit">
						<button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt os-icon os-icon-ui-37"></i> Rechercher</button>
					</fieldset>
				</form>
				
			</div><!-- / block-body -->
			
		</div>
	</div><!-- /col -->
	
</div><!-- / row -->