<?php if (User::isLoggedIn()): ?>
<header id="header">
		
    <div class="headerbar">

        <div class="menu-trigger pull-left tooltips" title="Menu">
            <span class="glyphicon glyphicon-menu-hamburger"></span>
        </div><!-- menu-trigger /-->

        <div class="headerbar-left pull-left">
            <a href="<?php echo BASE_URL ?>/tableau-de-bord">
                <img src="<?php echo IMAGE_URL ?>/logo.png" alt="OPSEARCH">
            	<span>OPSEARCH</span>
            </a>
        </div><!-- /headerbar-left -->
        
        <?php if (User::can('search')): ?>
            <?php if ($page != "search" && $page != "tdc"): ?>
            <div class="TopSearch">

                <form class="frm_frm" name="frm_main_search" id="frm_main_search" action="<?php echo BASE_URL ?>/rechercher" method="get">
                    <input type="hidden" name="type" id="typeSearch">
                    <input type="text" name="value" id="TopSearchInput" class="frm_text frm_TopSearch" placeholder="Rechercher" autocomplete="off">
                    <span class="glyphicon glyphicon-search"></span>   
                </form>

                <div class="SearchSelector">

                    <ul>
                        <li data-search="fonction"><i class="os-icon os-icon-hierarchy-structure-2"></i>Fonction</li>
                        <li data-search="poste"><span class="glyphicon glyphicon-user"></span>Poste</li>
                        <li data-search="client"><i class="fa fa-users"></i>Client</li>
                        <li data-search="secteur"><span class="glyphicon glyphicon-wrench"></span>Secteur</li>
    <!--                    <li data-search="search"><i class="os-icon os-icon-ui-37"></i>Rechercher</li>-->
                    </ul>

                </div><!-- /SearchSelector -->

            </div><!-- /topSearch -->
            <?php endif; ?>
        <?php endif; ?>

        <div class="headerbar-right pull-right">
            <ul class="profile">
                <li class="username <?php if (User::can('read_my_profile')): ?>my-profile tooltips<?php endif; ?>" <?php if (User::can('read_my_profile')): ?>title="Mon Profile" data-user_id="<?php echo $me['id'] ?>"<?php endif; ?>>
                    <i class="os-icon os-icon-user-male-circle2"></i> 
                    <span class="name">
                        <?php echo mb_ucfirst($me['firstName']) ?> <span><?php echo mb_strtoupper($me['lastName']) ?></span>
                    </span>
                </li>
                <li class="logout"><a href="<?php echo BASE_URL ?>/logout" class="tooltips" title="Déconnecter"><span class="glyphicon glyphicon-off"></span></a></li>
            </ul><!-- / profile -->

        </div><!-- /headerbar-right -->

    </div><!-- /headerbar -->

</header><!-- /header -->
<?php else: ?>
<header id="header">
    <div class="headerbar">
        <div class="headerbar-left pull-left">
            <img src="<?php echo IMAGE_URL ?>/logo.png" alt="OPSEARCH">
            <span>OPSEARCH</span>
        </div><!-- /headerbar-left -->
    </div><!-- /headerbar -->
</header><!-- /header -->
<?php endif; ?>

