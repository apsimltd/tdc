<div class="row">
				
	<div class="col col-12">
		<div class="block nopadding">
			
			<div class="block-head with-border">
				<header><i class="os-icon os-icon-ui-37"></i>Rechercher candidats dans la BDD</header>
			</div>
			
			<div class="block-body">
                
                <p style="color: #090; margin-bottom: 10px; font-weight: bold;">* Au moins un champ doit être sélectionné pour rechercher un candidat</p>
                
                <?php 
                $url = AJAX_HANDLER . '/search-candidat';
                if (get('tdc_id')) {
                    $url .= '?tdc_id=' . get('tdc_id');
                }
                ?>
                
				<form class="frm_frm frm_ajax frm_horizontal" name="frm_search_candidat" id="frm_search_candidat" data-url="<?php echo $url ?>" data-type="html">
                    
                    <?php if (get('tdc_id') && get('tdc_id') != ""): ?>
                        <input type="hidden" name="mission_id" value="<?php echo get('tdc_id') ?>">
                    <?php endif; ?>
                    
					<fieldset>
						<input class="frm_text" name="nom" placeholder="Nom" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
					<fieldset>
						<input class="frm_text" name="prenom" placeholder="Prénom" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
                    <fieldset>
						<input class="frm_text" name="email" placeholder="Email / Email 2" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
                    <fieldset>
						<input class="frm_text tel_fr" name="tel" placeholder="Téléphone" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
                    <fieldset>
						<input class="frm_text" name="societe_actuelle" placeholder="Société" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
                    <fieldset>
						<input class="frm_text" name="poste" placeholder="Poste" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
                    <fieldset>
						<input class="frm_text xlarge" name="diplome_specialisation" placeholder="Diplôme / spécialisation" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
                    <fieldset class="single-row">
						<label>Date d'obtention du diplôme</label>
						<input class="frm_text xsmall dateOfBirth" name="dateDiplome_from" placeholder="Entre" type="text" autocomplete="off" data-validation="val_blank">
						<span class="interval"> - </span>
						<input class="frm_text xsmall dateOfBirth" name="dateDiplome_to" placeholder="Et" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
					<fieldset>
						<select class="frm_chosen" name="control_localisation_id" data-validation="val_blank">
							<option value="">Choisir Localisation</option>
							<?php $controls = Control::getControlByType(6, 1) ?>
							<?php foreach($controls as $control): ?>
							<option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
							<?php endforeach; ?>  
						</select>
					</fieldset>
					<fieldset>
						<select class="frm_chosen" name="control_secteur_id" data-validation="val_blank">
							<option value="">Choisir Secteur</option>
							<?php $controls = Control::getControlByType(2, 1) ?>
							<?php foreach($controls as $control): ?>
							<option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
							<?php endforeach; ?>
						</select>
					</fieldset>
					<fieldset>
						<select class="frm_chosen" name="control_fonction_id" data-validation="val_blank">
							<option value="">Choisir Fonction</option>
							<?php $controls = Control::getControlByType(1, 1) ?>
							<?php foreach($controls as $control): ?>
							<option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
							<?php endforeach; ?>
						</select>
					</fieldset> 
					<fieldset>
						<select class="frm_chosen" name="control_niveauFormation_id" data-validation="val_blank">
							<option value="">Choisir Niveau</option>
							<?php $controls = Control::getControlByType(4, 1) ?>
							<?php foreach($controls as $control): ?>
							<option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
							<?php endforeach; ?>
						</select>
					</fieldset>
                    <fieldset>
                        <select class="frm_chosen" name="control_source_id" data-validation="val_blank">
                            <option value="">Choisir Source</option>
                            <?php $controls = Control::getControlByType(3, 1) ?>
                            <?php foreach($controls as $control): ?>
                            <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </fieldset>
					<fieldset class="single-row">
						<label>Âge</label>
						<input class="frm_text xxsmall" name="age_from" placeholder="Entre" type="text" autocomplete="off" data-validation="val_num">
						<span class="interval"> - </span>
						<input class="frm_text xxsmall" name="age_to" placeholder="Et" type="text" autocomplete="off" data-validation="val_num">
					</fieldset>
					<fieldset class="single-row">
						<label>En Package salarial (K€)</label>
						<input class="frm_text xsmall" name="remuneration_from" placeholder="Entre" type="text" autocomplete="off" data-validation="val_num">
						<span class="interval"> - </span>
						<input class="frm_text xsmall" name="remuneration_to" placeholder="Et" type="text" autocomplete="off" data-validation="val_num">
					</fieldset>
                    <fieldset>
						<input class="frm_text xlarge" name="detail_remuneration" placeholder="Détails primes / variable" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
<!--                    <fieldset class="single-row">
						<label>Salaire Package (K€)</label>
						<input class="frm_text xsmall" name="salaire_package_from" placeholder="Entre" type="text" autocomplete="off" data-validation="val_num">
						<span class="interval"> - </span>
						<input class="frm_text xsmall" name="salaire_package_to" placeholder="Et" type="text" autocomplete="off" data-validation="val_num">
					</fieldset>-->
					</fieldset><fieldset class="single-row">
						<label>Salaire Fixe (K€)</label>
						<input class="frm_text xsmall" name="salaire_fixe_from" placeholder="Entre" type="text" autocomplete="off" data-validation="val_num">
						<span class="interval"> - </span>
						<input class="frm_text xsmall" name="salaire_fixe_to" placeholder="Et" type="text" autocomplete="off" data-validation="val_num">
					</fieldset>
<!--                    </fieldset><fieldset class="single-row">
						<label>Sur Combien de Mois</label>
						<input class="frm_text xsmall" name="sur_combien_de_mois_from" placeholder="Entre" type="text" autocomplete="off" data-validation="val_num">
						<span class="interval"> - </span>
						<input class="frm_text xsmall" name="sur_combien_de_mois_to" placeholder="Et" type="text" autocomplete="off" data-validation="val_num">
					</fieldset>-->
                    <fieldset>
						<input class="frm_text xlarge" name="avantages" placeholder="Avantages en nature" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
                    <fieldset>
						<input class="frm_text xlarge" name="remuneration_souhaitee" placeholder="Rémunération Souhaitée" type="text" autocomplete="off" data-validation="val_blank">
					</fieldset>
					<fieldset class="single-row">                                   
                        <input type="checkbox" class="frm_checkbox" id="shortliste" name="shortliste">
						<label class="pointer" for="shortliste">Short-listé</label>
					</fieldset>
					<fieldset class="single-row">
                        <input type="checkbox" class="frm_checkbox" id="place" name="place">
						<label class="pointer" for="place">Placé</label>
					</fieldset>
					<fieldset class="single-row">
						<input type="checkbox" class="frm_checkbox" id="tresBonCandidat" name="tres_bon_candidat">
						<label class="pointer" for="tresBonCandidat">Très bon candidat</label>
					</fieldset>
                    <fieldset class="single-row">
						<input type="checkbox" class="frm_checkbox" id="cv_fourni" name="cv_fourni">
						<label class="pointer" for="cv_fourni">CV Fourni</label>
					</fieldset>
<!--                    <fieldset class="single-row">
						<input type="checkbox" class="frm_checkbox" id="societe_blacklister" name="company_blacklisted">
						<label class="pointer" for="societe_blacklister">Société Blacklister</label>
					</fieldset>-->
                    <fieldset class="single-row">
						<input type="checkbox" class="frm_checkbox" id="documentsss" name="documents">
						<label class="pointer" for="documentsss">Documents</label>
					</fieldset>
					<fieldset class="submit">
						<button type="button" class="btn btn-success frm_before_submit frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt os-icon os-icon-ui-37"></i> Rechercher</button>
					</fieldset>
				</form>
				
			</div><!-- / block-body -->
			
		</div>
	</div><!-- /col -->
	
</div><!-- / row -->
