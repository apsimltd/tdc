<div class="row">
				
	<div class="col col-12">
		<div class="block nopadding">
			
			<div class="block-head with-border">
				<header><i class="os-icon os-icon-ui-37"></i>Rechercher Client Blacklistés / Deblacklistés</header>
			</div>
			
			<div class="block-body">
                
                <p style="color: #090; margin-bottom: 10px; font-weight: bold;">* Au moins un champ doit être sélectionné</p>
                                
				<form class="frm_frm frm_ajax frm_horizontal" name="frm_search_societe" id="frm_search_societe" data-url="<?php echo AJAX_HANDLER ?>/search-societe" data-type="html">
                    
                    <fieldset>
                        <input class="frm_text must" name="nom" placeholder="Nom Client" type="text" autocomplete="off" data-validation="val_blank" style="width:250px;">
					</fieldset>
                    
                    <fieldset class="submit" style="float: left;">
						<button type="button" class="btn btn-success frm_submit frm_notif pull-left" data-form="2"><i class="ico-txt os-icon os-icon-ui-37"></i> Rechercher</button>
					</fieldset>
				</form>
				
			</div><!-- / block-body -->
			
		</div>
	</div><!-- /col -->
	
</div><!-- / row -->