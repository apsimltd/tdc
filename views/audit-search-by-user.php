<div class="row">
				
	<div class="col col-12">
		<div class="block nopadding">
			
			<div class="block-head with-border">
				<header><i class="os-icon os-icon-ui-37"></i>Rechercher audit dans la BDD</header>
			</div>
			
			<div class="block-body">
                
                <p style="color: #090; margin-bottom: 10px; font-weight: bold;">* Au moins un champ doit être sélectionné pour la recherche</p>
                                
				<form class="frm_frm frm_ajax frm_horizontal" name="frm_audit_by_user" id="frm_audit_by_user" data-url="<?php echo AJAX_UI ?>/audit-search-by-user" data-type="html">
                    
                    <fieldset>
                        <label>Utilisateur</label>
                        <select class="frm_chosen must" name="user_id" id="user_id" data-validation="val_blank">
                            <option value="">Choisir Utilisateur</option>
                            <?php $users = USER::getUsers(); ?>
                            <?php foreach($users as $user): ?>
                            <option value="<?php echo $user['id'] ?>"><?php echo mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName']) . ' - [' . $user['role_name'] . '] - [' . statut($user['status']) . ']' ?></option>
                            <?php endforeach; ?>
                        </select>
                    </fieldset>
                    
                    <fieldset class="single-row">
                        <label>Date</label>
                        <input class="frm_text start_date small must ok" value="<?php echo date('Y-m-d') ?>" id="start_date" name="start_date" placeholder="Dé" type="text" autocomplete="off" data-validation="val_blank">
                        <span class="interval"> - </span>
                        <input class="frm_text end_date small" id="end_date" name="end_date" placeholder="À" type="text" autocomplete="off" data-validation="val_blank">
                    </fieldset>
                    
					<fieldset class="submit">
						<button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt os-icon os-icon-ui-37"></i> Rechercher</button>
					</fieldset>
				</form>
				
			</div><!-- / block-body -->
			
		</div>
	</div><!-- /col -->
	
</div><!-- / row -->
<script>
$(document).ready(function(){
    $("#start_date").on().datepicker({
        dateFormat: 'yy-mm-dd',
        regional: 'fr',
        // minDate: '-7y', 
        minDate: "2020-01-28",
        maxDate: '+1d',
        showButtonPanel: true,
        onClose: function(selectedDate) {
            $("#end_date").datepicker("option", "minDate", selectedDate); 
        },
    });
    $("#end_date").on().datepicker({
        dateFormat: 'yy-mm-dd',
        regional: 'fr',
        minDate: '-7y', 
        maxDate: '+1d',
        showButtonPanel: true,
        onClose: function(selectedDate) {
            $("#start_date").datepicker("option", "maxDate", selectedDate);
        }
    });
});
</script>