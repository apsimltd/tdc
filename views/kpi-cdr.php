<div class="row">
				
	<div class="col col-12">
		<div class="block nopadding">
						
			<div class="block-body">
                
                <p style="color: #090; margin-bottom: 10px; font-weight: bold;">* Au moins un champ doit être sélectionné</p>
                                
				<form class="frm_frm frm_ajax frm_horizontal" name="frm_kpi_cdr" id="frm_kpi_cdr" data-url="<?php echo AJAX_HANDLER ?>/kpi-cdr-search" data-type="html">
                    
                    <fieldset class="single-row">
                        <label>Date</label>
                        <input class="frm_text start_date small" id="start_date" name="start_date" placeholder="Dé" type="text" autocomplete="off" data-validation="val_blank">
                        <span class="interval"> - </span>
                        <input class="frm_text end_date small" id="end_date" name="end_date" placeholder="À" type="text" autocomplete="off" data-validation="val_blank">
                    </fieldset>
                    
                    <fieldset>
                        <label>CDR</label>
                        <select class="frm_chosen must" name="cdr_id" id="cdr_id" data-validation="val_blank">
                            <option value="">Choisir CDR</option>
                            <?php $cdrs = User::getKPICDRsAssistantManagersManagers() ?>
                            <?php foreach($cdrs as $cdr): ?>
                            <option value="<?php echo $cdr['id'] ?>"><?php echo mb_strtoupper($cdr['lastName']) . ' ' . mb_ucfirst($cdr['firstName']) . ' - [' . $cdr['role_name'] . '] - [' . statut($cdr['status']) . ']' ?></option>
                            <?php endforeach; ?>
                        </select>
                    </fieldset>
                    
                    <div class="kpi_filter_mission">
                        
                    </div><!-- /kpi_filter_mission -->
                    
					<fieldset class="submit">
						<button type="button" class="btn btn-success frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt os-icon os-icon-ui-37"></i> Rechercher</button>
					</fieldset>
				</form>
				
			</div><!-- / block-body -->
			
		</div>
	</div><!-- /col -->
	
</div><!-- / row -->