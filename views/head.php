<!doctype html>
<html>
<head>
    <title><?php echo $page_title ?></title>
    <meta name="author" content="Faardeen Madarbokas">
    <meta name="company" content="LABf3 LTD">
    <meta name="email" content="fmadarbokas@labf3.com">
    <meta name="website" content="http://www.labf3.com">
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=0" name="viewport">
    <link href="<?php echo BASE_URL ?>/favicon.png" rel="shortcut icon">
    <link rel="stylesheet"  href="<?php echo CSS_URL ?>/main.css">
    <link rel="stylesheet"  href="<?php echo CSS_URL ?>/lightbox.min.css">
    <link rel="stylesheet"  href="<?php echo CSS_URL ?>/multi-select.css">
    <?php if ($page == "permissions"): ?><link rel="stylesheet"  href="<?php echo CSS_URL ?>/permissions.css"><?php endif; ?>
    <?php if ($page == "events"): ?>
    <link rel="stylesheet"  href="<?php echo CSS_URL ?>/calendar.css">
    <link rel="stylesheet"  href="<?php echo CSS_URL ?>/bootstrap-material-datetimepicker.css">
    <?php endif; ?>
    <?php if ($page == "add-candidate" || $page == "add-mission" || $page == "tdc" || $page == "add-entreprise"): ?>
    <link rel="stylesheet"  href="<?php echo CSS_URL ?>/custom-file-input.css">
    <?php endif; ?>
    <?php if ($page == "contentType"): ?><link rel="stylesheet"  href="<?php echo CSS_URL ?>/colorpicker.css"><?php endif; ?>
    <script  src="<?php echo JS_URL ?>/jquery-3.2.1.min.js"></script>
    <script  src="<?php echo JS_URL ?>/jquery-ui.min.js"></script>
    <script  src="<?php echo JS_URL ?>/datepicker-fr.js"></script>
    <script  src="<?php echo JS_URL ?>/jquery.nanoscroller.min.js"></script>
    <script  src="<?php echo JS_URL ?>/jquery.hoverIntent.minified.js"></script>
    <script  src="<?php echo JS_URL ?>/jquery.easing.min.js"></script>
    <script  src="<?php echo JS_URL ?>/icheck.min.js"></script>
    <script  src="<?php echo JS_URL ?>/chosen.jquery.min.js"></script>
    <script  src="<?php echo JS_URL ?>/utilities.js"></script>
    <script  src="<?php echo JS_URL ?>/forms.js"></script>
    <script  src="<?php echo JS_URL ?>/jquery.tooltipster.min.js"></script>
    <script  src="<?php echo JS_URL ?>/quick-up.js"></script>
    <script  src="<?php echo JS_URL ?>/jquery.dataTables.js"></script>
    <script  src="<?php echo JS_URL ?>/dataTables.fixedHeader.min.js"></script>
<!--    <script  src="<?php //echo JS_URL ?>/dataTables.sort.title.js"></script>-->
    <script  src="<?php echo JS_URL ?>/jquery.browser.min.js"></script>
    <script  src="<?php echo JS_URL ?>/lightbox.min.js"></script>
    <script  src="<?php echo JS_URL ?>/jquery.multi-select.js"></script>
    <?php if ($page == "events"): ?>
    <script  src="<?php echo JS_URL ?>/underscore-min.js"></script>
    <script  src="<?php echo JS_URL ?>/bootstrap.min.js"></script>
    <script  src="<?php echo JS_URL ?>/calendar.js"></script>
    <script  src="<?php echo JS_URL ?>/calendar-fr-FR.js"></script>
    <script  src="<?php echo JS_URL ?>/init-calendar.js"></script>
    <script  src="<?php echo JS_URL ?>/moment-2.21.0.min.js"></script>
    <script  src="<?php echo JS_URL ?>/bootstrap-material-datetimepicker.js"></script>
    <?php endif; ?>
    <?php if ($page == "contentType"): ?><script  src="<?php echo JS_URL ?>/colorpicker.js"></script><?php endif; ?>
    <script  src="<?php echo JS_URL ?>/notify.js"></script>
    <script  src="<?php echo JS_URL ?>/js.cookie.js"></script>
    <script  src="<?php echo JS_URL ?>/main.js"></script>
    <?php if ($page == "kpi_clients"): ?>
    <script  src="<?php echo JS_URL ?>/kpi-clients-filters.js"></script>
    <?php endif; ?>
    <?php if ($page == "kpi_manager"): ?>
    <script  src="<?php echo JS_URL ?>/kpi_manager_filters.js"></script>
    <?php endif; ?>
    <?php if ($page == "kpi_cdr"): ?>
    <script  src="<?php echo JS_URL ?>/kpi-cdr-filters.js"></script>
    <?php endif; ?>
    <script  src="<?php echo JS_URL ?>/user-clients.js"></script>
    
    <script >
        var BASE_URL = '<?php echo BASE_URL ?>';
        var AJAX_HANDLER = '<?php echo AJAX_HANDLER ?>';
        var AJAX_UI = '<?php echo AJAX_UI ?>';
    </script>
</head>

<body class="<?php echo $page ?>">
