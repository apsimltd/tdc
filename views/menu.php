<div id="menuWrapper">
    <ul class="main-menu">
        
        <li class="first level_1"><a href="<?php echo BASE_URL ?>/tableau-de-bord"><span class="glyphicon glyphicon-th"></span>Tableau de bord</a></li>
        
        <?php if (User::can(array('create_mission', 'read_mission', 'read_all_mission', 'edit_mission', 'delete_mission', 'read_all_mission', 'duplicate_mission', 'tdc'))): ?>
        <li class="level_1"><a href="<?php echo BASE_URL ?>/missions"><span class="glyphicon glyphicon-tasks"></span>Missions</a></li>
        <?php endif; ?>
        
        <?php if (User::can('TDC_MISSIONS_CLIENT')): ?>
        <li class="level_1"><a href="<?php echo BASE_URL ?>/tdc-missions-client"><span class="glyphicon glyphicon-screenshot"></span>TDC MISSIONS CLIENT</a></li>
        <?php endif; ?>
        
        <?php if (User::can('search')): ?>        
        <li class="level_1"><a href="<?php echo BASE_URL ?>/rechercher"><span class="glyphicon glyphicon-search"></span>Recherche</a></li>
        <?php endif; ?>
        
        <?php if (User::can(array('create_task', 'read_task', 'read_all_task', 'edit_task', 'delete_task', 'archive_task'))): ?>
        <li class="level_1"><a href="<?php echo BASE_URL ?>/taches"><span class="glyphicon glyphicon-tags"></span>Tâches</a></li>
        <?php endif; ?>
        
        <?php if (User::can(array('add_event', 'view_event', 'edit_event', 'delete_event'))): ?>
        <li class="level_1"><a href="<?php echo BASE_URL ?>/evenements"><span class="glyphicon glyphicon-calendar"></span>Événements</a></li>
        <?php endif; ?>
        
        <?php if (User::can(array('create_candidat', 'read_candidat', 'edit_candidat', 'tdc'))): ?>
        <li class="level_1"><a href="<?php echo BASE_URL ?>/candidats"><span class="glyphicon glyphicon-user"></span>Candidats</a></li>
        <li class="level_1"><a href="<?php echo BASE_URL ?>/entreprises"><span class="glyphicon glyphicon-tower"></span>Entreprises</a></li>
        <?php endif; ?>
        
        <?php if (User::can(array('create_client', 'read_client', 'edit_client'))) :?>
        <li class="level_1"><a href="<?php echo BASE_URL ?>/clients"><i class="fa fa-users"></i>Clients</a></li>
        <?php endif; ?>
        <?php if (User::isLoggedIn()) :?>
        <li class="level_1"><a href="<?php echo BASE_URL ?>/societes"><span class="glyphicon glyphicon-home"></span>Clients Blacklistés</a></li>
        <?php endif; ?>
        
        <?php if (User::can(array('read_cdr_kpi', 'read_manager_kpi', 'read_client_kpi'))): ?>
        <li class="level_1">
            <a class="main"><i class="os-icon os-icon-bar-chart-stats-up"></i>KPI ACTIVITÉ</a>
            <ul>
                <?php if (User::can('read_client_kpi')): ?>
                <li><a href="<?php echo BASE_URL ?>/kpi-clients"><i class="fa fa-users"></i>KPI CLIENT</a></li>
                <?php endif; ?>
                <?php if (User::can('read_manager_kpi')): ?>
                <li><a href="<?php echo BASE_URL ?>/kpi-manager"><i class="os-icon os-icon-bar-chart-stats-up"></i>KPI MANAGER</a></li>
                <?php endif; ?>
                <?php if (User::can('read_cdr_kpi')): ?>
                <li><a href="<?php echo BASE_URL ?>/kpi-cdr"><i class="os-icon os-icon-user-male-circle2"></i>KPI CDR</a></li>
                <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
        
        <?php if (User::can(array('hr_read_user_details', 'hr_edit_user_details'))): ?>
        <li class="level_1">
            <a class="main"><i class="fa fa-users"></i>RH</a>
            <ul>
                <?php if (User::can(array('hr_read_user_details', 'hr_edit_user_details'))): ?>
                <li><a href="<?php echo BASE_URL ?>/rh/employees"><i class="fa fa-users"></i>Employés</a></li>
                <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
        
        <?php if (User::can(array('create_type_content', 'read_type_content', 'edit_type_content', 'create_user', 'read_user', 'edit_user', 'assign_deassign_perm', 'create_role', 'delete_role'))): ?>
        <li class="level_1">
            <a class="main"><span class="glyphicon glyphicon-wrench"></span>Administration</a>
            <ul>
                <?php if (User::can(array('create_type_content', 'read_type_content', 'edit_type_content'))): ?>
                <li><a href="<?php echo BASE_URL ?>/types-de-contenu"><span class="glyphicon glyphicon-cog"></span>Types de contenu</a></li>
                <?php endif; ?>
                <?php if (User::can(array('create_user', 'read_user', 'edit_user'))): ?>
                <li><a href="<?php echo BASE_URL ?>/utilisateurs"><i class="os-icon os-icon-user-male-circle2"></i>Utilisateurs</a></li>
                <?php endif; ?>
                <?php if (User::can(array('create_user', 'read_user', 'edit_user'))): ?>
                <li><a href="<?php echo BASE_URL ?>/utilisateurs-clients"><i class="fa fa-users"></i>Utilisateurs Clients</a></li>
                <?php endif; ?>
                <?php if (User::can(array('assign_deassign_perm', 'create_role', 'delete_role'))): ?>
                <li><a href="<?php echo BASE_URL ?>/autorisations"><span class="glyphicon glyphicon-lock"></span>Autorisations Utilisateurs</a></li>
                <?php endif; ?>
                <?php if (User::can('read_audit')): ?>
                <li><a href="<?php echo BASE_URL ?>/audit"><span class="glyphicon glyphicon-time"></span>Audit</a></li>
                <?php endif; ?>
            </ul>
        </li>
        <?php endif; ?>
        
    </ul><!-- /main-menu -->
</div><!-- /menuWrapper -->

