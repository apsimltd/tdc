<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Ajouter un candidat" ?>
<?php $page = "add-candidate"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('create_candidat'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>

	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base" class="AddCandidat">

        <div id="contentWrap">					
            
			<section class="title block">

				<h1 class="pull-left"><span class="glyphicon glyphicon-user"></span>Ajouter un candidat</h1>
				
				<div class="btn-box pull-right">
                    <?php if (get('tdc_id') && get('tdc_id') != ""): ?>
                    <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo get('tdc_id') ?>">
                        <button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page précédente"><span class="glyphicon glyphicon-menu-left"></span></button>
                    </a>
                    <?php else: ?>
					<button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page précédente" onClick="window.history.back();"><span class="glyphicon glyphicon-menu-left"></span></button>
                    <?php endif; ?>
				</div>
				
			</section>
			
            <div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
												
						<div class="block-body">
							
                            <form class="frm_frm" name="frm_create_candidat" id="frm_create_candidat" method="POST" action="<?php echo AJAX_HANDLER ?>/add-candidat" enctype="multipart/form-data">
                                
                                <?php if (get('tdc_id') && get('tdc_id') != ""): ?>
                                <input type="hidden" name="mission_id" value="<?php echo get('tdc_id') ?>">
                                <?php endif; ?>
                                
								<div class="row">
				
									<div class="col col-4">
										<div class="block nopadding">
																	
											<div class="block-body">
												
                                                <fieldset>
                                                    <label>Civilité</label>
                                                    <div class="labf3-radio-square-wrapper">
                                                        <?php $controls = Control::getControlByType(7, 1); $counter = 1; ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <div class="labf3-radio-square" data-value="<?php echo $control['id'] ?>">
                                                            <span class="ticks"></span>
                                                            <span class="libele"><?php echo $control['name'] ?></span>
                                                        </div><!-- /labf3-radio-square -->
                                                        <?php $counter++; ?>
                                                        <?php endforeach; ?>
                                                        <input type="hidden" name="control_civilite_id" class="frm_text frm_labf3_radio_square" data-validation="val_blank">
                                                    </div><!-- /labf3-radio-square-wrapper -->   
                                                </fieldset>
												<fieldset>
													<label>Localisation</label>
													<select class="frm_chosen" name="control_localisation_id" data-validation="val_blank">
														<option value="">Choisir Localisation</option>
														<?php $controls = Control::getControlByType(6, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
												<fieldset>
													<label>Nom</label>
													<input class="frm_text caps" name="nom" placeholder="Nom" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
												<fieldset>
													<label>Prénom</label>
													<input class="frm_text" name="prenom" placeholder="Prénom" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
												<fieldset>
													<label>Téléphone standard</label>
													<input class="frm_text tel_fr" name="tel_standard" placeholder="Téléphone standard" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
												<fieldset>
													<label>Téléphone ligne directe</label>
													<input class="frm_text" name="tel_ligne_direct" placeholder="Téléphone ligne directe" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
												<fieldset>
													<label>Téléphone mobile pro</label>
													<input class="frm_text" name="tel_pro" placeholder="Téléphone mobile professionnel" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
												<fieldset>
													<label>Téléphone mobile perso</label>
													<input class="frm_text" name="tel_mobile_perso" placeholder="Téléphone mobile perso" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
												<fieldset>
													<label>Téléphone domicile</label>
													<input class="frm_text" name="tel_domicile" placeholder="Téléphone domicile" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
												<fieldset>
													<label>Email 1</label>
													<input class="frm_text" name="email" placeholder="Email 1" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
                                                <fieldset>
													<label>Email 2</label>
													<input class="frm_text" name="email2" placeholder="Email 2" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
												
                                                <fieldset>
													<label>Date de naissance</label>
													<input class="frm_text dateOfBirth" name="dateOfBirth" placeholder="Date de naissance" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
                                                
												
												
                                                
											</div><!-- / block-body -->
											
										</div>
									</div><!-- /col -->
									
									<div class="col col-4">
										<div class="block nopadding">
																	
											<div class="block-body">
												
                                                <fieldset>
													<label>Société actuelle</label>
                                                    <input class="frm_text caps checkSocieteBlacklisteCandidat" id="societe_autocomplete" name="societe_actuelle" placeholder="Société actuelle" type="text" data-validation="val_blank">
                                                    <input type="hidden" name="company_id" id="societe_autocomplete_id">
                                                    
                                                    <div class="msgBlackliste" style="display:none;">
                                                        <div class="clearboth notif warning pull-right" style="background-color:#ff9800; color: #fff; width: 60%;line-height: 24px; padding: 0px 10px;"><strong>Attention :</strong> La Société est blacklistée</div>
                                                    </div><!-- / msgBlackliste -->
                                                    
												</fieldset>
                                                
                                                <fieldset class="horizontal_checkboxes">
                                                    <label>&nbsp;</label>
													<div class="radio_wrap">
														<div class="radio-item">
															<label class="long" for="is_not_in_this_company">Candidat plus en poste</label>
                                                            <input type="checkbox" name="is_not_in_this_company" class="frm_checkbox" id="is_not_in_this_company">
														</div>
													</div>													
												</fieldset>
                                                
                                                <fieldset>
													<label>Poste</label>
													<input class="frm_text" name="poste" placeholder="Poste" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
                                                
												<fieldset>
													<label>Secteur</label>
													<select class="frm_chosen" name="control_secteur_id" data-validation="val_blank">
														<option value="">Choisir Secteur</option>
														<?php $controls = Control::getControlByType(2, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
												<fieldset>
													<label>Fonction</label>
													<select class="frm_chosen" name="control_fonction_id" data-validation="val_blank">
														<option value="">Choisir Fonction</option>
                                                        <?php $controls = Control::getControlByType(1, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
												<fieldset>
													<label>En Package salarial (K€)</label>
													<input class="frm_text" name="remuneration" placeholder="En Package salarial (K€)" type="text" autocomplete="off" data-validation="val_float">
												</fieldset>
												<fieldset>
													<label>Détails primes / variable</label>
													<input class="frm_text" name="detail_remuneration" placeholder="Détails primes / variable" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
                                                
<!--                                                <fieldset>
													<label>Salaire Package (K€)</label>
													<input class="frm_text" name="salaire_package" placeholder="Salaire Package (K€)" type="text" autocomplete="off" data-validation="val_float">
												</fieldset>-->
                                                
                                                <fieldset>
													<label>Salaire Fixe (K€)</label>
													<input class="frm_text" name="salaire_fixe" placeholder="Salaire Fixe (K€)" type="text" autocomplete="off" data-validation="val_float">
												</fieldset>
                                                
<!--                                                <fieldset>
													<label>Prime ou Variable</label>
													<input class="frm_text" name="sur_combien_de_mois" placeholder="Prime ou Variable" type="text" autocomplete="off" data-validation="val_num">
												</fieldset>-->
                                                
                                                <fieldset>
													<label>Avantages en nature</label>
                                                    <textarea name="avantages" class="frm_textarea" placeholder="Avantages en nature" data-validation="val_blank" maxlength="300"></textarea>                                
												</fieldset>
                                                
                                                <fieldset>
													<label>Rémunération Souhaitée</label>
                                                    <textarea name="remuneration_souhaitee" class="frm_textarea" placeholder="Rémunération Souhaitée" data-validation="val_blank" maxlength="300"></textarea>                                
												</fieldset>
                                                
																					
												
											</div><!-- / block-body -->
											
										</div>
									</div><!-- /col -->
									
									<div class="col col-4">
										<div class="block nopadding">
																	
											<div class="block-body">
                                                
                                                <fieldset>
													<label>Niveau</label>
													<select class="frm_chosen" name="control_niveauFormation_id" data-validation="val_blank">
														<option value="">Choisir Niveau</option>
														<?php $controls = Control::getControlByType(4, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
												<fieldset>
													<label>Diplôme / spécialisation</label>
													<textarea name="diplome_specialisation" class="frm_textarea" placeholder="Diplôme / spécialisation" data-validation="val_blank"></textarea>                                
												</fieldset>
												<fieldset>
													<label>Date d'obtention du diplôme</label>
													<input class="frm_text dateOfBirth" name="dateDiplome" placeholder="Date d'obtention du diplôme" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>		
                                                
                                                <fieldset class="horizontal_checkboxes">
													<div class="radio_wrap">
														<div class="radio-item">
															<label class="long" for="cvFourni">CV fourni</label>
                                                            <input type="checkbox" name="cv_fourni" class="frm_checkbox" id="cvFourni">
														</div>
														<div class="radio-item">
															<label class="long" for="tresBonCandidat">Très bon candidat</label>
                                                            <input type="checkbox" name="tres_bon_candidat" class="frm_checkbox" id="tresBonCandidat">
														</div>
													</div>													
												</fieldset>
                                                
												<fieldset>
													<label>Source</label>
													<select class="frm_chosen" name="control_source_id" data-validation="val_blank">
														<option value="">Choisir Source</option>
                                                        <?php $controls = Control::getControlByType(3, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
                                                <fieldset class="socialMedia">
                                                    <div class="clearfix">
                                                        <label class="pull-left">Réseaux sociaux <span class="glyphicon glyphicon-info-sign tooltips" title="Un lien ex. www.linkedin.com/bastien"></span></label>
                                                        <div class="clearfix add_field_wrap_social pull-left" style="width: 60%;">
                                                            <div class="add_field_row clearfix">
                                                                <input class="frm_text socialmedia_link" name="reseauSociaux[]" placeholder="Entrez le lien" type="text" autocomplete="off" data-validation="val_url">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt6 clearfix">
                                                        <button type="button" class="add_field_social btn btn-icon btn-success pull-right tooltips" title="Ajouter un nouveau lien"><span class="glyphicon glyphicon-plus"></span></button>
                                                    </div>
												</fieldset>
												<fieldset>
													<label>Langues</label>
													<select class="frm_chosen" name="langues[]" data-validation="val_blank" data-placeholder="Choisissez une ou plusieurs langues" multiple="multiple">
													    <?php $controls = Control::getControlByType(5, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
                                                <?php if (User::can('rgpd_candidat')): ?>
                                                <fieldset class="mission-checkbox">
                                                    <input type="checkbox" class="frm_checkbox" name="gdpr_status" id="gdpr_status">
                                                    <label class="pointer" for="gdpr_status" style="color: #FF0000">Candidat Inactif (RGPD)</label>
                                                </fieldset>
                                                <?php endif; ?>
												<fieldset>
													<label>Commentaire</label>
													<textarea name="commentaire" class="frm_textarea" placeholder="Commentaire" data-validation="val_blank"></textarea>                                
												</fieldset>
                                                <fieldset class="uploadMultiple">
                                                    <div class="clearfix">
                                                        <label class="pull-left">Documents <span class="glyphicon glyphicon-info-sign tooltips" title="Les fichiers autorisés sont : .doc, .docx, .xls, .xlsx, .pdf, .jpg, .jpeg, .png ; Taille max : 10 Mo"></span></label>
                                                        <input type="hidden" name="uploadFlag" id="uploadFlag" value="1">
                                                        <div class="clearfix add_field_wrap_documents pull-left" style="width: 60%;">
                                                            <div class="add_field_row pull-left">
                                                                <input type="file" name="file[]" id="file_1" data-number="1" class="inputfile inputfile-1">
                                                                <label id="uploadLabel_1" for="file_1" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span></label>
                                                                <button type="button" class="clear_field_upload btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt6 clearfix">
                                                        <button type="button" class="add_field_upload btn btn-icon btn-success pull-right tooltips" title="Ajouter un nouveau fichier"><span class="glyphicon glyphicon-plus"></span></button>
                                                    </div>                              
												</fieldset>
												<fieldset>
													<button type="button" class="btn btn-primary frm_submit frm_before_submit frm_notif pull-right" data-form="6"><i class="ico-txt fa fa-plus"></i> Ajouter Candidat</button>
												</fieldset>
												
											</div><!-- / block-body -->
											
										</div>
									</div><!-- /col -->
									
								</div><!-- / row -->	

							</form>
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
        
        </div><!-- /contentWrap -->
        
    </div><!-- /base --> 
	
	<?php require_once './views/debug.php'; ?>  
    
</body>
</html>
<script>
$(document).ready(function(){
    
    <?php if (get('msg') && get('msg') != "" && get('tdc_id') == ""): ?>
        Notif.Show('<?php echo $error_codes[get('msg')] ?>', 'error', true, 3000, 'reloadpagenoparam');
    <?php elseif (get('msg') && get('msg') != "" && get('tdc_id') && get('tdc_id') != ""): ?>
        var param = {'querystring': 'tdc_id', 'value': '<?php echo get('tdc_id') ?>'};
        Notif.Show('<?php echo $error_codes[get('msg')] ?>', 'error', true, 3000, 'reloadpageparam', param);
    <?php endif; ?>    
    
    Upload.Init();
    Upload.Validate();
    
    // add a new field for reseau socieau link
    $(document).on('click', 'button.add_field_social', function(){
        var inputHtml = '<input class="frm_text socialmedia_link" name="reseauSociaux[]" placeholder="Entrez le lien" type="text" autocomplete="off" data-validation="val_url">';
        var newFieldHtml = '<div class="add_field_row clearfix">'+inputHtml+'</div>';
        $('.add_field_wrap_social').append(newFieldHtml);
    });
    
    $(document).on('blur', 'input.socialmedia_link', function(){
        var numRows = parseInt($('fieldset.socialMedia .add_field_wrap_social .add_field_row:last').index()) + 1;
        
        if (numRows >= 2 && $(this).val() == "") {
            $(this).parent('.add_field_row').remove();
        }       
        
    });
    
    // add a new field for upload
    $(document).on('click', 'button.add_field_upload', function(){
        
        // get the last input number
        var number = parseInt($('.add_field_wrap_documents .add_field_row:last input.inputfile').attr('data-number')) + 1;
        
        var inputHtml = '<input type="file" name="file[]" id="file_'+number+'" data-number="'+number+'" class="inputfile inputfile-1"><label id="uploadLabel_'+number+'" for="file_'+number+'" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span></label><button type="button" class="clear_field_upload btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>';
        var newFieldHtml = '<div class="add_field_row clearfix">'+inputHtml+'</div>';
        $('.add_field_wrap_documents').append(newFieldHtml);
        
        var flagNumber = parseInt($('#uploadFlag').val()) + 1;
        $('#uploadFlag').val(flagNumber);
        
        Upload.Init();
    });
    
    $(document).on('click', '.clear_field_upload', function(){
        
        var numRows = parseInt($('fieldset.uploadMultiple .add_field_wrap_documents .add_field_row:last').index()) + 1;
        
        if (numRows >= 2) {
            $(this).parent('.add_field_row').remove();
        } else if (numRows == 1) {
            $(this).parent('.add_field_row').find('input.inputfile').val('');
            $(this).parent('.add_field_row').find('label').html('<span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span>');
        }
        
    }); 
    
    // autocomplete on societe actuelle field
    // https://makitweb.com/jquery-ui-autocomplete-with-php-and-ajax/
            
    $("#societe_autocomplete").autocomplete({
        minLength: 3,
        source: function(request, response) {
            // Fetch data
            $.ajax({
                url: AJAX_HANDLER + "/search-candidat-companies",
                type: 'post',
                dataType: "json",
                data: {
                    search: request.term
                },
                // response must be {value: id of the data, label: name to display}
                success: function( data ) {
                    response( data );
                }
            });
        },
        select: function (event, ui) {
            // Set selection
            $('#societe_autocomplete').val(ui.item.label); // display the selected text
            $('#societe_autocomplete_id').val(ui.item.value); // save selected id to input
            return false;
        },
        focus: function(event, ui){
            $('#societe_autocomplete').val(ui.item.label); // display the selected text
            $('#societe_autocomplete_id').val(ui.item.value); // save selected id to input
            return false;
        },
    });
    
    // check candidat societe blacklister
    $(document).on('blur', '.checkSocieteBlacklisteCandidat', function(){
        if ($(this).val() != "") {
            
            ajaxBusy = true;
            var nom_societe = $(this).val();
            var url = AJAX_HANDLER+'/check-societe-candidat-blackliste?nom_societe='+nom_societe;
            $.ajax({
                url: url,
                dataType : 'json',
                cache: false,
                success: function (result, status) {
                    if (status === "success" && result.status === "OK") {
                        $('.msgBlackliste').find('.notif').html(result.msg);
                        $('.msgBlackliste').fadeIn();
                    } else if (status === "success" && result.status === "NOK") {
                        $('.msgBlackliste').fadeOut();
                    }
                }
            });
            
        } else {
            $('.msgBlackliste').fadeOut();
        }
            
    });
        
}); // end document ready function

var Upload = {
    Init: function (){
        $('.inputfile').each(function(){
		
            var $input	 = $( this ),
                $label	 = $input.next( 'label' ),
                labelVal = $label.html();

            $input.on( 'change', function( e )
            {
                var fileName = '';

                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else if( e.target.value )
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    $label.find( 'span.filename' ).html( fileName );
                else
                    $label.html( labelVal );
            });

            // Firefox bug fix
            $input
            .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
            .on( 'blur', function(){ 
                $input.removeClass( 'has-focus' ); 
            });

        });
    },
    Validate: function (){
        $(document).on('change', '.inputfile', function(){
              Upload.ValidateUpload($(this));            
        });
    },
    ValidateUpload: function (input) {
        var file_data = input.prop("files")[0];
        // start validate file extension
        var validExtensions = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx', 'xls', 'xlsx']; //array of valid extensions
        var filename = file_data.name;
        var ext = filename.substr(filename.lastIndexOf('.') + 1);
        if ($.inArray(ext, validExtensions) == -1){
            Notif.Show('Fichier non valide', 'error', true, 4000);
            input.val("");
            input.next('label').removeClass('ok').addClass('error');
            return false;
        } else {
            // check file size
            // 4MB allowed
            var filesize = file_data.size;
            if (filesize > 10485760) {
                Notif.Show('La taille du fichier est supérieure à celle autorisée. Télécharger moins de 10 Mo.', 'error', true, 4000);
                input.val("");
                input.next('label').removeClass('ok').addClass('error');
                return false;
            } else {
                input.next('label').removeClass('error').addClass('ok');
                return true;
            }
        }
        // end validation file extension

    },
}

function frm_create_candidat_before_submit(obj) {  
    
    // verify upload
    var uploadFlag = true;
    var numLabel = parseInt($('#uploadFlag').val());
    
    for(var i = 1; i <= numLabel; i++) {
        if ($('#uploadLabel_'+numLabel).length) {
            if ($('#uploadLabel_'+numLabel).hasClass('error')) {
                uploadFlag = false;
            }
        }
    }
    
    return uploadFlag;
    
}
</script>