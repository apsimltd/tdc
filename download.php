<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php
if (get('type') && get('type') != "" && get('id') && get('id') != "") {
    if (get('type') == 'can') { // candidate documents
        $file = Candidat::getDocumentById(get('id'));
    } else if (get('type') == 'miss') { // mission documents
        $file = Mission::getDocumentById(get('id'));
    } else if (get('type') == 'ent') { // entreprise documents
        $file = Candidat::getDocumentEntrepriseByKey(get('id'), get('key'));
    }
    
    // make the file downloadable    
	header("Pragma: public"); // required
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false); // required for certain browsers 
	header("Content-Type: ". $file['content_type']);
	// change, added quotes to allow spaces in filenames
	header("Content-Disposition: attachment; filename=\"".basename($file['path'])."\";" );
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: ".filesize($file['path']));
	readfile($file['path']);
	exit();
    
} else {
    header('Location:' . BASE_URL);
}
?>

