<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Missions" ?>
<?php $page = "missions"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('create_mission', 'read_mission', 'edit_mission', 'delete_mission', 'read_all_mission', 'duplicate_mission', 'tdc'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>

	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base" class="Missions">

        <div id="contentWrap">
        
			<section class="title block">

				<h1 class="pull-left">Mes Missions</h1>
				
                <?php if (User::can('create_mission', 'create_candidat')): ?>
				<div class="btn-box pull-right"> 
                    <?php if (User::can('create_mission')): ?>
					<a href="ajoute-mission"><button type="button" class="btn btn-icon btn-primary pull-right tooltips" title="Ajouter Mission"><span class="glyphicon glyphicon-tasks"></span></button></a>
                    <?php endif; ?>
                    <?php if (User::can('create_candidat')): ?>
                    <a href="ajoute-candidat"><button type="button" class="btn btn-icon btn-primary pull-right tooltips" title="Ajouter Candidat"><span class="glyphicon glyphicon-user"></span></button></a>
                    <?php endif; ?>
				</div>
                <?php endif; ?>
				
			</section>
            
            <?php if (User::can('read_mission')): ?>
				<?php require_once './views/mission-search.php'; ?>
			<?php endif; ?>
            
            <div class="missions-listing">
                <!-- load by ajax search form -->
            </div>
            
        </div><!-- /contentWrap -->
        
    </div><!-- /base --> 
	
	<?php require_once './views/debug.php'; ?>  
    
</body>
</html>
<script src="<?php echo JS_URL ?>/mission.js"></script>