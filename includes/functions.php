<?php
require_once __DIR__ . '/../conf/bootstrap.inc';

if (isDev()) {
    define('DBHOST', isDev() ? (isHome() ? '192.168.14.128' : '192.168.247.129') : '151.80.21.111');
    define('DBUSER', 'root');
    define('DBPASS', 'caimps');
    define('MPDF_TMP', '/tmp');
} else {
    define('DBHOST', '151.80.21.111');
    define('DBUSER', 'caimps');
    define('DBPASS', '181285cAiMpS');
    define('MPDF_TMP', '/tmp');
}



/**
 * Connexion a la bdd
 * Garde la connexion en static pour ne pas reconnecter
 * @return PDO
 */
function dbconnect() {
    static $db = null;

    if (!is_null($db)) {
        return $db;
    }

    $options = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];

    $db = new PDO('mysql:host='.DBHOST.';dbname=opsearch', DBUSER, DBPASS, $options);
    return $db;
}

/**
 * Charger les departments en bdd
 * @return array
 */
function load_departments() {
    try {
        $db = dbconnect();
    } catch (PDOException $e) {
        return [];
    }

    $stmt = $db->query('SELECT id, dept_code, name FROM controles WHERE type_id = 6 AND status = 1 AND map_status = 1');
    $data = [];
    while ($row = $stmt->fetch()) {
        $row['name'] = preg_replace('/[0-9]{2}\s/m', '', $row['name']);
        $data[$row['id']] = $row;
    }

    return $data;
}

/**
 * Prend une array de department (array avec la clef 'dept_code') et retourne les valeurs dept_code -> department
 * @param $departments
 * @return array
 */
function departments_by_dept_code($departments) {
    $depts = [];
    foreach ($departments as $department) {
        $depts[$department['dept_code']] = $department;
    }

    return $depts;
}

/**
 * Charger les candidats et retourner dept_code -> resultats(region, pourcentage, dept_code)
 * @param $missionid
 * @return array
 */
function load_candidats_par_departments($missionid) {
    $regions = load_departments();

    $sql = <<<SQL
SELECT COUNT(id) nombre, control_localisation_id region FROM candidats WHERE id IN (
    SELECT candidat_id id FROM tdc WHERE mission_id = {$missionid}
) GROUP BY control_localisation_id ORDER BY nombre DESC;
SQL;


    try {
        $db          = dbconnect();
        $stmt        = $db->query($sql);
        $departments = array_values(array_filter($stmt->fetchAll(), function ($candidat) use (&$regions) {
            return array_key_exists($candidat['region'], $regions);
        })); //avoir les candidats mais seulement avec les regions que nous connaissons.

    } catch (PDOException $e) {
        print 'Failed to connect to database: ' . $e->getMessage();
        exit;
    }

    $total = array_reduce($departments, function ($nombre, $candidat) {
        return $candidat['nombre'] + $nombre;
    }, 0);

    $departments = array_map(function ($department) use (&$regions, $total) {
        $region = $regions[$department['region']];
        $department['name']       = $region['name'];
        $department['pourcentage'] = $total === 0 ? 0 : round(100*($department['nombre'] / $total), 2);
        $department['dept_code']   = $region['dept_code'];

        return $department;
    }, $departments);

    return departments_by_dept_code($departments);
}