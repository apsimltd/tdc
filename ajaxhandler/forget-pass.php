<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php
if (isPost()) {
    
    $email = filter_var(getPost('email'), FILTER_SANITIZE_STRING);
    
    if (!User::resetPass($email)){
        
        $notif = array(
            'status' => 'NOK',
            'msg' => 'L\'adresse email ' . $email . ' n\'existe pas ou le compte est désactivé',
            'type' => 'error', 
        );
        setNotifCookie($notif);
        header('Location:'. BASE_URL . '/mot-de-passe-oublie');
          
    } else {
         
        $notif = array(
            'status' => 'OK',
            'msg' => 'Un email a été envoyé à votre adresse email ' . $email . ' avec les instructions pour réinitialiser votre mot de passe',
            'type' => 'success', 
        );
        setNotifCookie($notif);
        header('Location:'. BASE_URL . '/connexion');             

    }
    
} else {
    header("Location:" . BASE_URL . "/connexion"); 
}

?>