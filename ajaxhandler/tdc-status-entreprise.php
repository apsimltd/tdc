<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
    $data = array(
        'user_id' => $me['id'],
        'status_entreprise' => getPost('status_entreprise'),
        'mission_id' => getPost('mission_id'),
        'entreprise_id' => getPost('entreprise_id'),
        'status_entreprise_old' => getPost('status_entreprise_old'),
        'modified' => time(),
    );
     
     if (TDC::updateEntrepriseInTDCStatus(getPost('tdc_id'), $data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Statut mis à jour avec succès',
            'type' => 'success',
            'callback' => 'reloadstatusentreprise',
            'param' => array(
                'mission_id' => getPost('mission_id'),
                'tdc_id' => getPost('tdc_id'),
            ),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>