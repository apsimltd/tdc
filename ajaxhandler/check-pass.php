<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_my_profile')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php
if (isPost()) {
    
    $pass = filter_var(getPost('pass'), FILTER_SANITIZE_STRING);
    $pass_hash = $me['password'];
    
    if (!password_verify($pass, $pass_hash)){
        
        $response = array(
            'status' => 'NOK',
            'msg' => 'Le mot de passe actuel ne correspond pas',
            'type' => 'error', 
        );
        
    } else {
        
        $response = array(
            'status' => 'OK',
            'type' => 'success', 
        );
        
    }
    
    echo json_encode($response);
    exit();
    
} else {
    header("Location:" . BASE_URL . "/connexion"); 
}

?>

