<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php
if (isPost()) {
    $username = getPost('username');
    $password = getPost('password');
        
    if (!User::login($username, $password)){
        $response = array(
            'status' => 'NOK',
            'msg' => 'Le nom d\'utilisateur ou le mot de passe est incorrect',
            'type' => 'error',           
        );      
    } else {
        $user = $_SESSION['opsearch_user'];
        $userName = mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName']);
        $response = array(
            'status' => 'OK',
            'msg' => 'Salut ' . $userName . '. Bienvenue à OPSEARCH.',
            'type' => 'success',
            'callback' => 'gotodashboard',
        );
    }
    
    echo json_encode($response);
    exit();
    
} else {
    header("Location:" . BASE_URL . "/connexion"); 
}
?>