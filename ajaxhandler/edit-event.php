<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_event')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
       
    $event = array(
        'id' => getPost('id'),
        'title' => getPost('title'),
        'eventType_id' => getPost('eventType_id'),
        'date' => getPost('date'),
        'start' => strtotime(getPost('date') . ' ' . getPost('start')) * 1000, // multiple timestamp by 1000 gets microtime
        'end' => strtotime(getPost('date') . ' ' . getPost('end')) * 1000,
        'user_id' => $_SESSION['opsearch_user']['id'],
        'start_date' => date('Y-m-d H:i:s', strtotime(getPost('date') . ' ' . getPost('start') .':00')),
        'end_date' => date('Y-m-d H:i:s', strtotime(getPost('date') . ' ' . getPost('end') .':00') - 60),
    );
    
    // we check if the time chose has already expired and display an error message
    if (!Event::CheckTime($event)) {
        $response = array(
            'status' => 'NOK',
            'msg' => 'La plage de temps n\'est pas valide ou a expiré',
            'type' => 'error',
        ); 
        echo json_encode($response);
        exit();
    }
        
    if (!Event::CheckOverlapEventEdit($event)) {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Vous ne pouvez pas avoir plusieurs événements en même temps',
            'type' => 'error',           
        ); 
        echo json_encode($response);
        exit();
    }
    
    // check if there is already an event at this time according to max event per type
    if (!Event::checkOverlapEventTypeEdit($event)) {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Vous ne pouvez pas avoir plusieurs événements du même type en même temps',
            'type' => 'error',           
        ); 
        echo json_encode($response);
        exit();
    }
    
    if (Event::edit($event)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Événement ajouté avec succès',
            'type' => 'success',
            'callback' => 'reloadpage',
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>