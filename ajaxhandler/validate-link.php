<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php
if (isPost()) {
    
    $link = getPost('link');
    
    if (checkLink($link)) {
        $response = array(
            'status' => 'OK',
        );
    } else {
        $response = array(
            'status' => 'NOK',
        );
    }
     
} else {
    $response = array(
        'status' => 'NOK',
    );
}
echo json_encode($response);
exit();
?>