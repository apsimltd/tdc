<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php
if (isPost()) {

    $data = array(
        getPost('field') => getPost('color_id'),
        'modified' => time(),
    );   
        
    TDC::updateCandidatInTDC(getPost('tdc_id'), $data);
}
?>