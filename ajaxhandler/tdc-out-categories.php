<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
    $data = array(
        'user_id' => $me['id'],
        'mission_id' => getPost('mission_id'),
        'candidat_id' => getPost('candidat_id'),
        'out_categorie_ids' => getPost('out_categorie_ids'), // seperated by |
        'tdc_id' => getPost('tdc_id'),
        'created' => time(),
    );
     
    if (TDC::updateOutCategories($data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Out Categorie mises à jour avec succès',
            'type' => 'success',
            'callback' => 'reloadoutcategories',
            'param' => array(
                'candidat_id' => getPost('candidat_id'),
                'mission_id' => getPost('mission_id'),
                'tdc_id' => getPost('tdc_id'),
            ),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>