<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php 
if (isGet()) {
    
    if (Candidat::deleteDocument(get('id'))) {
        $response = array(
            'status' => 'OK',
            'msg' => 'Document supprimé avec succès',
            'type' => 'success',
//            'callback' => 'reloaddocument',
//            'param' => get('candidat_id'),
            'callback' => 'reloadpage'
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>
