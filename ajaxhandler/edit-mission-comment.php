<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_mission')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {

    $comment = array(
        'id' => getPost('id'),
        'user_id' => $me['id'],
        'mission_id' => getPost('mission_id'),
        'comment' => getPost('commentaire'),
        'modified' => time(),
    );
     
    if (Mission::editComment($comment)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Commentaire modifié avec succès',
            'type' => 'success',
            'callback' => 'reloadcomment',
            'param' => getPost('mission_id'),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>
