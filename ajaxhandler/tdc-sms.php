<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
        
    $data = array(
        'user_id' => $me['id'],
        'mission_id' => getPost('mission_id'),
        'candidat_id' => getPost('candidat_id'),
        'message_typed' => getPost('message_typed'),
        'message_text' => filterChar(getPost('message_typed')),
        'to_raw' => getPost('to_raw'),
        'created' => dateToDb(),
    );
    
    // validate tel characters first
    if (!isDev()) {
        
        $tel_valid = SMS::validateTel($data['to_raw']);
        if ($tel_valid !== TRUE) {

            $response = array(
                'status' => 'NOK',
                'msg' => $tel_valid,
                'type' => 'error',           
            );

            echo json_encode($response);
            exit();

        }
        
    }

    $response = SMS::process($data);
    
    if ($response === TRUE) {
        $response = array(
            'status' => 'OK',
            'msg' => 'SMS envoyé avec succès',
            'type' => 'success',
        );
        echo json_encode($response);
        exit();
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => $response,
            'type' => 'error',           
        ); 
        echo json_encode($response);
        exit();
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
    echo json_encode($response);
    exit();
}

?>