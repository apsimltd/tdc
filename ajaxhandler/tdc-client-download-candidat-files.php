<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('TDC_CLIENT')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php
if (isGet()) {
    
    $candidat_id = get('candidat_id');
    
    $candidat = Candidat::getCandidatById($candidat_id);
    $cand_docs = json_decode($candidat['documents'], true);
    
    $response = array();
    
    if (!empty($cand_docs)) {
        
        $fileType = array('doc', 'docx', 'pdf');
        
        
        foreach($cand_docs as $cand_doc):
           
            $ext = getFileExt($cand_doc['file']);
            if (in_array($ext, $fileType)):
                
                $response[] = $cand_doc;
                
            endif;

        endforeach;
        
    } 
    
    echo json_encode($response);
    exit();
    
}

?>