<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_candidat') || get('id') == ""): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php
$societe = get('societe'); // opsearch or headhunting
$mission_id = get('id'); // mission id

$mission = Mission::getMissionDetailsById($mission_id);
$criteres = Mission::getCriteres($mission_id);
$out_categories = Mission::getOutCategory($mission_id);
$secteurs = Control::getControlListByType(2);
$localisations = Control::getControlListByType(6);
$fonctions = Control::getControlListByType(1);
$dpt = Control::getControlListByType(6);
$status = Control::getControlListByType(8);
$civilite = Control::getControlListByType(7);
$colorsTDC = Control::getColorPDF();

$tdcCandidates = TDC::getCandidatesByTDC($mission_id);
$tdcEntreprises = TDC::getEntreprisesByTDC($mission_id);

// config @src https://mpdf.github.io/reference/mpdf-functions/construct.html
$pdfConfig = array(
    'mode' => 'utf-8',
    // 'tempDir' => __DIR__ . 'C:/wamp/www/mybox/mpdf/' // folder should be writable
    'format' => [279, 216], // default => A4
    // 'format' => [400, 216], // default => A4
    //'orientation' => 'L', // P => portrait, L => Landscape
    'default_font_size' => 9, // in points
    'default_font' => 'Arial', // Arial, Helvetica, sans-serif
    'margin_left' => 15,
    'margin_right' => 15,
    'margin_top' => 25,
    'margin_bottom' => 16,
    'margin_header' => 5,
    'margin_footer' => 5,
);

// everything should be in millimeters for the pdf

$mpdf = new \Mpdf\Mpdf($pdfConfig);

// pdf file metadata
// metadata
$mpdf->SetTitle('TDC::'.mb_strtoupper($mission['nom_client']) . '-' . $mission['poste']);


// header
// https://mpdf.github.io/reference/mpdf-functions/sethtmlheader.html

// swap header and footer for pdf based on company chosen
if ($societe == 'opsearch') {
    $header = '<div style="text-align:center; padding-bottom:10px; font-size:10px; font-weight:normal; color:#000; border-bottom:1px solid #CCC; height:20px;"><img src="../asset/images/pdf/opsearch.png" style="width:300px;height:49px;"></div>';
    $mpdf->SetAuthor('OPSEARCH');
    $mpdf->SetCreator('OPSEARCH');
} elseif ($societe == 'headhunting') {
    $header = '<div style="text-align:center; padding-bottom:10px; font-size:10px; font-weight:normal; color:#000; border-bottom:1px solid #CCC; height:20px;"><img src="../asset/images/pdf/headhunting.png" style="width:180px;height:40px;"></div>';
    $mpdf->SetAuthor('HEADHUNTING FACTORY');
    $mpdf->SetCreator('HEADHUNTING FACTORY');
}

$mpdf->SetHTMLHeader($header);

// footer
$mpdf->SetHTMLFooter('
<table width="100%">
    <tr>
        <td width="33%">{DATE j-m-Y}</td>
        <td width="33%" align="center">{PAGENO}/{nbpg}</td>
        <td width="33%" style="text-align: right;">TDC</td>
    </tr>
</table>');

// stats
$client = mb_strtoupper($mission['nom_client']);
$poste = $mission['poste'];
$sector_mission = ($mission['control_secteur_id'] > 0) ? $secteurs[$mission['control_secteur_id']] : "";
$localistion_mission = ($mission['control_localisation_id'] > 0) ? $localisations[$mission['control_localisation_id']] : "";
$date_debut = dateToFr($mission['date_debut']);
$date_fin = dateToFr($mission['date_fin']);

// out categories
$out_cat_html = '';
if (!empty($out_categories)) {
    $out_cat_html .= '<div style="width:35%; float:left;">';
    $out_cat_html .= '<table style="border-collapse:collapse; border:1px solid #CCC;border-bottom:none;width:95%;">';
    $out_cat_html .= '<tbody>';
    $out_cat_html .= '<tr><td colspan="2" style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;background-color:#D2B9A1;color:#fff;">OUT Catégories</td></tr>';
    
    foreach($out_categories as $out):
        $out_cat_html .= '<tr>';
            $out_cat_html .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;">'.$out['out_categorie'].'</td>';
            $out_cat_html .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:40px; text-align:center;">'.TDC::getTotalByOutCategoryinTDC($mission_id, $out['id']).'</td>';
        $out_cat_html .= '</tr>';
    endforeach;
    
    $out_cat_html .= '</tbody></table></div>';
}

// only prestataire OPSEARCH we display field stat rencontrer
if ($mission['prestataire_id'] == 1) {
    $rencontrer_html = '<tr>
        <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;text-align:center;color:#'.$colorsTDC[15]['font_hex'].';background-color:#'.$colorsTDC[15]['bg_hex'].';">Rencontré</td>
        <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:40px; text-align:center;">' . TDC::getTotalCandidatRencontrerinTDC($mission_id) . '</td>
      </tr>';
} else {
    $rencontrer_html = '';
}

// new request to group some status only on view mode and only here on generating pdf anonyme and pdf client
// 1. BU = Back Up = BU- (227), BU+ (228), ARCA (252)
// 2. IN = IN (229), ARC (251)
// 3. OUT = OUT (226), EX BU (259)
$stats_status_html = '<table style="border-collapse:collapse; border:1px solid #CCC;border-bottom:none;">
        <tbody>
          <tr>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;background-color:#'.$colorsTDC[11]['bg_hex'].';color:#'.$colorsTDC[11]['font_hex'].';text-align:center;">IN</td>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:40px; text-align:center;">' . TDC::getTotalCandidatinTDCByStatusGrouped($mission_id, array(229,251)) . '</td>
          </tr>
          <tr>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;background-color:#'.$colorsTDC[10]['bg_hex'].';color:#'.$colorsTDC[10]['font_hex'].';text-align:center;">BU</td>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:40px; text-align:center;">' . TDC::getTotalCandidatinTDCByStatusGrouped($mission_id, array(227,228,252)) . '</td>
          </tr>
          <tr>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;background-color:#'.$colorsTDC[12]['bg_hex'].';color:#'.$colorsTDC[12]['font_hex'].';text-align:center;">OUT</td>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:40px; text-align:center;">' . TDC::getTotalCandidatinTDCByStatusGrouped($mission_id, array(226,259)) . '</td>
          </tr>
          <tr>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;text-align:center;color:#'.$colorsTDC[13]['font_hex'].';background-color:#'.$colorsTDC[13]['bg_hex'].';">Short-listé</td>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:40px; text-align:center;">' . TDC::getTotalCandidatShortlisteInTDC($mission_id) . '</td>
          </tr>
          <tr>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;text-align:center;color:#'.$colorsTDC[14]['font_hex'].';background-color:#'.$colorsTDC[14]['bg_hex'].';">Placé</td>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:40px; text-align:center;">' . TDC::getTotalCandidatPlaceInTDC($mission_id) . '</td>
          </tr>
          ' . $rencontrer_html . '
        </tbody>
      </table>';

$stats = '<div style="width:100%; clear:both; margin-bottom:20px;">
  <div style="float:left; width:25%;">
    <table style="border-collapse:collapse; border:1px solid #CCC;border-bottom:none; width:95%;">
      <tbody>
        <tr>
          <td colspan="2" style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;background-color:#D2B9A1;color:#fff;">Mission</td>
        </tr>
        <tr>
          <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;">Client</td>
          <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">' . $client . '</td>
        </tr>
        <tr>
          <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;">Poste</td>
          <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">' . $poste . '</td>
        </tr>
        <tr>
          <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;">Secteur</td>
          <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">'. $sector_mission . '</td>
        </tr>
        <tr>
          <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;">Localisation</td>
          <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">' . $localistion_mission . '</td>
        </tr>
        <tr>
          <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;">Début</td>
          <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">' . $date_debut . '</td>
        </tr>
        <tr>
          <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;">Fin</td>
          <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">' . $date_fin . '</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div style="float:left; width:70%;">
    <div style="width:35%; float:left;">
      <table style="border-collapse:collapse; border:1px solid #CCC;border-bottom:none; width:95%;">
        <tbody>
          <tr>
            <td colspan="2" style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;background-color:#D2B9A1;color:#fff;">Données chiffrées</td>
          </tr>
          <tr>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;">Entreprises identifiées</td>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:40px; text-align:center;">' . TDC::getTotalSocieteinTDC_V2($mission_id) . '</td>
          </tr>
          <tr>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;">Entreprises restants à approcher</td>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:40px; text-align:center;">' . TDC::getTotalEntrepriseInTDCAAprocher($mission_id) . '</td>
          </tr>
          <tr>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;">Candidats identifiés</td>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:40px; text-align:center;">' . TDC::getTotalCandidatInTDC($mission_id) . '</td>
          </tr>
          <tr>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;">Candidats approchés</td>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:40px; text-align:center;">' . TDC::getTotalCandidatApprocheinTDC($mission_id) . '</td>
          </tr>
          <tr>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;">Candidats restants à approcher</td>
            <td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:40px; text-align:center;">' . TDC::getTotalCandidatRestantApprocheinTDC($mission_id) . '</td>
          </tr>
        </tbody>
      </table>
    </div>
    '.$out_cat_html.'
    <div style="width:15%; float:left;">
      ' . $stats_status_html . '
    </div></div></div>';

//echo $stats;

$mpdf->WriteHTML($stats);

$mpdf->AddPage(); // move to the next page

// table data
if (!empty($tdcCandidates)):
    
    $tableHeader = '<table style="border-collapse:collapse; border:1px solid #CCC;border-bottom:none;">
        <thead>
            <tr style="padding:5px;border-bottom:1px solid #CCC; background-color:#D2B9A1;">
                <th style="padding:5px;border-right:1px solid #CCC;color:#FFF;font-size:16px;">Candidat</th>
                <th style="padding:5px;border-right:1px solid #CCC;color:#FFF;font-size:16px;">Société</th>
                <th style="padding:5px;border-right:1px solid #CCC;color:#FFF;font-size:16px;">Poste</th>
                <th style="padding:5px;border-right:1px solid #CCC;color:#FFF;font-size:16px;">Rémunération</th>
                <th style="padding:5px;border-right:1px solid #CCC;color:#FFF;font-size:16px;">Commentaires</th>
                <th style="padding:5px;color:#FFF;">Statut</th>
            </tr>
        </thead><tbody>';

    $mpdf->WriteHTML($tableHeader);

    foreach($tdcCandidates as $tdcCandidat):
//        debug($tdcCandidat);
        // candidat name
        $candidatName = '';
        if ($tdcCandidat['control_civilite_id'] == 0): 
            $candidatName =  mb_strtoupper($tdcCandidat['nom']) . ' ' . mb_ucfirst($tdcCandidat['prenom']);
        else:
            $candidatName =  $civilite[$tdcCandidat['control_civilite_id']] . ' ' . mb_strtoupper($tdcCandidat['nom']) . ' ' . mb_ucfirst($tdcCandidat['prenom']);
        endif;
        if ($tdcCandidat['cv_fourni'] == 1):
            $candidatName .= "<br>";
            $candidatName .= "(CV Fourni)";
        endif;
        
        $cand_societe = (($tdcCandidat['is_not_in_this_company'] == 1) ? 'EX - ': '') . mb_strtoupper($tdcCandidat['societe_actuelle']);
        // $cand_fonction = ($tdcCandidat['control_fonction_id'] > 0) ? $fonctions[$tdcCandidat['control_fonction_id']] : "";
        $cand_poste = $tdcCandidat['poste'];
        
        $cand_remuneration = "";
        if ($tdcCandidat['remuneration'] > 0) {
            $cand_remuneration .= "En Package salarial (K€) : " . $tdcCandidat['remuneration'] . " K€";
        }
        if ($tdcCandidat['detail_remuneration'] != "") {
            $cand_remuneration .= "<br>Détails primes / variable : " . $tdcCandidat['detail_remuneration'];
        }
//        if ($tdcCandidat['salaire_package'] != "") {
//            $cand_remuneration .= "<br>Salaire Package (K€) : " . $tdcCandidat['salaire_package'] . " K€";
//        }
        if ($tdcCandidat['salaire_fixe'] != "") {
            $cand_remuneration .= "<br>Salaire Fixe (K€) : " . $tdcCandidat['salaire_fixe'] . " K€";
        }
//        if ($tdcCandidat['sur_combien_de_mois'] != "") {
//            $cand_remuneration .= "<br>Sur Combien de Mois : " . $tdcCandidat['sur_combien_de_mois'];
//        }
        if ($tdcCandidat['avantages'] != "") {
            $cand_remuneration .= "<br>Avantages en nature : " . $tdcCandidat['avantages'];
        }
        if ($tdcCandidat['remuneration_souhaitee'] != "") {
            $cand_remuneration .= "<br>Rémunération Souhaitée : " . $tdcCandidat['remuneration_souhaitee'];
        }
        
        $candidat_id = $tdcCandidat['candidat_id'];
                
        // comment
        $suivi = TDC::getSuivi($mission_id, $candidat_id);
        
        // status bg and font color
        // groupement BU (BU-, BU+, ARCA)
        if ($tdcCandidat['control_statut_tdc'] == 227 || $tdcCandidat['control_statut_tdc'] == 228 || $tdcCandidat['control_statut_tdc'] == 252) {
            $bgStatus = '#' . $colorsTDC[10]['bg_hex'];
            $fontStatus = '#' . $colorsTDC[10]['font_hex'];
        // groupement IN (IN, ARC)
        } else if ($tdcCandidat['control_statut_tdc'] == 229 || $tdcCandidat['control_statut_tdc'] == 251) {
            $bgStatus = '#' . $colorsTDC[11]['bg_hex'];
            $fontStatus = '#' . $colorsTDC[11]['font_hex'];
        // groupement OUT (OUT, Ex BU)
        } else if ($tdcCandidat['control_statut_tdc'] == 226 || $tdcCandidat['control_statut_tdc'] == 259) {
            $bgStatus = '#' . $colorsTDC[12]['bg_hex'];
            $fontStatus = '#' . $colorsTDC[12]['font_hex'];
        } else {
            $bgStatus = '#ffffff';
            $fontStatus = '#000000';
        }
        
        // comment field
        /// $comment = Candidat::getLastComment($candidat_id);
        $comment = Candidat::getLastCommentByMission($candidat_id, $mission_id);        
        $cand_comment = '';
        
        $age_formation_localisation = '';
        
        $cand_age = ($tdcCandidat['dateOfBirth'] != 0) ? getAge($tdcCandidat['dateOfBirth']) : "";
        $cand_comment .= '<strong>Age : </strong>' . $cand_age . '<br>';
        $age_formation_localisation .= '<strong>Age : </strong>' . $cand_age;
        
        $cand_diplome = nl2br($tdcCandidat['diplome_specialisation']);
        $cand_comment .= '<strong>Formation : </strong>' . $cand_diplome . '<br>';
        $age_formation_localisation .= '<br><strong>Formation : </strong>' . $cand_diplome;
        
        $cand_localisation = ($tdcCandidat['control_localisation_id'] > 0) ? $dpt[$tdcCandidat['control_localisation_id']] : "";
        $cand_comment .= '<strong>Localisation : </strong>' . $cand_localisation . '<br>';
        $age_formation_localisation .= '<br><strong>Localisation : </strong>' . $cand_localisation;
        
        // add secteur of candidate in comment field
//        if ($tdcCandidat['control_secteur_id'] > 0) {
//            $cand_secteur = '<strong>Secteur : </strong>' . $secteurs[$tdcCandidat['control_secteur_id']] . '<br><br>';
//        } else {
//            $cand_secteur = "";
//        }
        
        // add coordonnes in pdf client in comment field
        // Coordonnées
        $coordonnees = '<strong>Coordonnées : </strong>';
        if ($tdcCandidat['tel_standard'] != "")
            $coordonnees .= '<br>Téléphone standard : ' . $tdcCandidat['tel_standard'];
        if ($tdcCandidat['tel_mobile_perso'] != "")
            $coordonnees .= '<br>Mobile perso : ' . $tdcCandidat['tel_mobile_perso'];
        if ($tdcCandidat['tel_ligne_direct'] != "")
            $coordonnees .= '<br>Direct : ' . $tdcCandidat['tel_ligne_direct'];
        if ($tdcCandidat['tel_pro'] != "")
            $coordonnees .= '<br>Pro : ' . $tdcCandidat['tel_pro'];
        if ($tdcCandidat['tel_domicile'] != "")
            $coordonnees .= '<br>Domicile : ' . $tdcCandidat['tel_domicile'];
        if ($tdcCandidat['email'] != "")
            $coordonnees .= '<br>Email 1 : ' . $tdcCandidat['email'];
        if ($tdcCandidat['email2'] != "")
            $coordonnees .= '<br>Email 2 : ' . $tdcCandidat['email2'];       
        
        $comment_candidat = "<strong>Commentaire Candidat : </strong>";
        if (!empty($comment)) {
            $cand_comment .= '<br><br><strong>Commentaire Candidat : </strong><br><p>' . wordwrap(nl2br($comment['comment']), 150, "<br />\n", TRUE) . '</p>';
            $comment_candidat .= '<br>' . wordwrap(nl2br($comment['comment']), 150, "<br />\n", TRUE);
        }
        
        $suivi_candidat = "<strong>Commentaire Suivi : </strong>";
        if ($suivi['comment'] != "") {
            $cand_comment .= '<br><br><strong>Commentaire Suivi : </strong><br><p>' . wordwrap(nl2br($suivi['comment']), 150, "<br />\n", TRUE) . '</p>';
            $suivi_candidat .= '<br>' . wordwrap(nl2br($suivi['comment']), 150, "<br />\n", TRUE);
        }
        
        $motivation = Candidat::getLastMotivationByMission($candidat_id, $mission_id);
        $motivation_candidat = "<strong>Motivation : </strong>";
        if (!empty($motivation['motivation'])) {
            $cand_comment .= '<br><br><strong>Motivation : </strong><br><p>' . wordwrap(nl2br($motivation['motivation']), 150, "<br />\n", TRUE) . '</p>';
            $motivation_candidat .= '<br>' . wordwrap(nl2br($motivation['motivation']), 150, "<br />\n", TRUE);
        }
        
        /// $cand_comment = $cand_secteur . $coordonnees . $cand_comment; 
        $cand_comment = $coordonnees . "<br>" . $cand_comment; 
        // $cand_comment = $coordonnees . wordwrap($cand_comment, 50, "<br />\n", FALSE);
        
        // status
        // $status_cand = ($tdcCandidat['control_statut_tdc'] > 0) ? $status[$tdcCandidat['control_statut_tdc']] : "";
        $status_cand = pdf_status_text($tdcCandidat['control_statut_tdc']);
        
        // shortliste
        $shortliste = ($suivi['shortliste'] == 1) ? '<br>Short listé' : '';
        // place
        $place = ($suivi['place'] == 1) ? '<br>Placé' : '';
        
//        $table_row = '';
//        $table_row .= '<tr>';
//            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">' . $candidatName . '</td>';
//            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">' . $cand_societe . '</td>';
//            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">'. $cand_poste .'</td>';
//            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">' . $cand_remuneration . '</td>';
//            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:60%;font-size:12px;">'. $cand_comment .'</td>';
//            $table_row .= '<td style="padding:5px;border-bottom:1px solid #CCC;text-align:center;color:'.$fontStatus.';background-color:' . $bgStatus . ';"><strong>'. $status_cand .'</strong>' . $shortliste . $place . '</td>';
//        $table_row .= '</tr>';
        
        #########new styling for bigger font size#######
        
        $table_row = '';
        $table_row .= '<tr>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-size:20pt;">' . $candidatName . '</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-size:20pt;">' . $cand_societe . '</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-size:20pt;">'. $cand_poste .'</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-size:20pt;">' . $cand_remuneration . '</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:60%;font-size:20pt;">'. $coordonnees .'</td>';
            $table_row .= '<td style="padding:5px;border-bottom:1px solid #CCC;text-align:center;font-size:20pt;color:'.$fontStatus.';background-color:' . $bgStatus . ';"><strong>'. $status_cand .'</strong>' . $shortliste . $place . '</td>';
        $table_row .= '</tr>';
        
        $table_row .= '<tr>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:60%;font-size:20pt;">'.$age_formation_localisation.'</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;color:'.$fontStatus.';background-color:' . $bgStatus . ';">&nbsp;</td>';
        $table_row .= '</tr>';
        
        $table_row .= '<tr>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:60%;font-size:20pt;">'.$comment_candidat.'</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;color:'.$fontStatus.';background-color:' . $bgStatus . ';">&nbsp;</td>';
        $table_row .= '</tr>';
        
        $table_row .= '<tr>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:60%;font-size:20pt;">'.$suivi_candidat.'</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;color:'.$fontStatus.';background-color:' . $bgStatus . ';">&nbsp;</td>';
        $table_row .= '</tr>';
        
        $table_row .= '<tr>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:2px solid #000;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:2px solid #000;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:2px solid #000;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:2px solid #000;">&nbsp;</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:2px solid #000;width:60%;font-size:20pt;">'.$motivation_candidat.'</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:2px solid #000;color:'.$fontStatus.';background-color:' . $bgStatus . ';">&nbsp;</td>';
        $table_row .= '</tr>';
        ################
        
        $mpdf->WriteHTML($table_row);
        
    endforeach;
    
    $tableEnd = '</tbody></table>';
    $mpdf->WriteHTML($tableEnd);
    
else:
    $table = '';
    $table .= '<table style="border-collapse:collapse; border:1px solid #CCC;border-bottom:none;"><tr>';
        $table .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">Il n\'y a aucun candidat dans le TDC</td>';
    $table .= '</tr></table>';
    $mpdf->WriteHTML($table);
endif;

// start of entreprises
// table data

if (!empty($tdcEntreprises)):
    
    $tableHeader = '<div style="clear:both; margin-top:30px;"><table style="border-collapse:collapse; border:1px solid #CCC;border-bottom:none;">
        <thead>
            <tr style="padding:5px;border-bottom:1px solid #CCC; background-color:#D2B9A1;">
                <th style="padding:5px;border-right:1px solid #CCC;color:#FFF;">Entreprise</th>
                <th style="padding:5px;border-right:1px solid #CCC;color:#FFF;">Coordonnées</th>
                <th style="padding:5px;border-right:1px solid #CCC;color:#FFF;">Région</th>
                <th style="padding:5px;border-right:1px solid #CCC;color:#FFF;">Email</th>
                <th style="padding:5px;border-right:1px solid #CCC;color:#FFF;">Secteur Activité</th>
                <th style="padding:5px;border-right:1px solid #CCC;color:#FFF;">Sous Secteur Activité</th>
                <th style="padding:5px;border-right:1px solid #CCC;color:#FFF;">Suivi</th>
                <th style="padding:5px;color:#FFF;">Statut</th>
            </tr>
        </thead><tbody>';

    $mpdf->WriteHTML($tableHeader);
    
    foreach($tdcEntreprises as $tdcEntreprise):       
        
        $table_row = '';
        $table_row .= '<tr>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">' . $tdcEntreprise['name'] . '</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">'. $tdcEntreprise['telephone'] .'</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;text-align:center;">' . $tdcEntreprise['region'] . '</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">' . $tdcEntreprise['email'] . '</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">'. $tdcEntreprise['secteur_activite'] .'</td>';
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">'. $tdcEntreprise['sous_secteur_activite'] .'</td>';
            
            // suivi from tdc
            $suivi_ent = json_decode($tdcEntreprise['suivis_entreprise'], true);
            $suivi = '';
            if ($suivi_ent['comment'] != "") {
                $suivi .= '<p>' . wordwrap(nl2br($suivi_ent['comment']), 150, "<br />\n", TRUE) . '</p>';
            }
            
            $table_row .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;width:30%;font-size:12px;">'. $suivi .'</td>';
            
            if ($tdcEntreprise['status_entreprise'] == 257) {// OUT
                $statut_ent = "OUT";
                $bgStatusEnt = '#' . $colorsTDC[17]['bg_hex'];
                $fontStatusEnt = '#' . $colorsTDC[17]['font_hex'];
            } elseif ($tdcEntreprise['status_entreprise'] == 258) {// FINI
                $statut_ent = "FINI";
                $bgStatusEnt = '#' . $colorsTDC[16]['bg_hex'];
                $fontStatusEnt = '#' . $colorsTDC[16]['font_hex'];
            } else {
                $statut_ent = "";
                $bgStatusEnt = '#ffffff';
                $fontStatusEnt = '#000000';
            }
            
            $table_row .= '<td style="padding:5px;border-bottom:1px solid #CCC;text-align:center;color:'.$fontStatusEnt.';background-color:' . $bgStatusEnt . ';">' . $statut_ent . '</td>';
        $table_row .= '</tr>';
                
        $mpdf->WriteHTML($table_row);
        
    endforeach;
    
    $tableEnd = '</tbody></table></div>';
    $mpdf->WriteHTML($tableEnd);
    
else:
    $table = '<div style="clear:both; margin-top:30px;">';
    $table .= '<table style="border-collapse:collapse; border:1px solid #CCC;border-bottom:none;"><tr>';
        $table .= '<td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">Il n\'y a aucune entreprise dans le TDC</td>';
    $table .= '</tr></table></div>';
    $mpdf->WriteHTML($table);
endif;
// end of entreprises

// generate the pdf
$title = 'TDC_client_' . $mission['id'] . '_' .  str_replace(' ', '-', filterChar($mission['nom_client'])) . '_' . str_replace(' ', '-', filterChar($mission['poste']));
$file_name =  $title . '.pdf';

$file = PDF_URL . '/' . $file_name;

// save the pdf file
// http://www.fpdf.org/en/doc/output.htm
$mpdf->Output($file); // save to a local file with the name given by name (may include a path).

$response = array(
    'status' => 'OK',
    'file' => $file_name,
);

echo json_encode($response);
exit();
?>