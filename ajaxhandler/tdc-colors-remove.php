<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php
if (isPost()) {
    
    $data = array(
        'modified' => time(),
    );
    
    if (getPost('object') == "societe") {
        $data['color_societe_label'] = 0;
        $data['color_societe_bg'] = 0;
    } elseif (getPost('object') == "candidat") {
        $data['color_candidat_label'] = 0;
        $data['color_candidat_bg'] = 0;
    }  
        
    TDC::updateCandidatInTDC(getPost('tdc_id'), $data);
}
?>