<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_mission')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
//    debug($_FILES);
//    debug(getPost(), true);
       
    $mission = array(
        'id' => getPost('id'),
        'manager_id' => getPost('manager_id'),
        'old_manager_id' => getPost('old_manager_id'),
        'old_manager_name' => getPost('old_manager_name'),
        'prestataire_id' => getPost('prestataire_id'),
        'client_id' => getPost('client_id'),
        'consultant_id' => getPost('consultant_id'),
        'poste' => getPost('poste'),
        'control_secteur_id' => getPost('control_secteur_id'),
        'control_localisation_id' => getPost('control_localisation_id'),
        'control_fonction_id' => getPost('control_fonction_id'),
        'control_niveauFormation_id' => getPost('control_niveauFormation_id'),
        'status' => getPost('status'),
        'remuneration_min' => (getPost('remuneration_min') == '') ? 0 : getPost('remuneration_min'),
        'remuneration_max' => (getPost('remuneration_max') == '') ? 0 : getPost('remuneration_max'),
        'age_min' => (getPost('age_min') == '') ? 0 : getPost('age_min'),
        'age_max' => (getPost('age_max') == '') ? 0 : getPost('age_max'),
        'experience_min' => (getPost('experience_min') == '') ? 0 : getPost('experience_min'),
        'experience_max' => (getPost('experience_max') == '') ? 0 : getPost('experience_max'),
        'date_debut' => (getPost('date_debut') == '') ? 0 : strtotime(getPost('date_debut')),
        'date_fin' => (getPost('date_fin') == '') ? 0 : strtotime(getPost('date_fin')),
        'updated' => time(),
        'status_old' => getPost('status_old'),
    );
    
    if (getPost('include_in_kpi')) {
        $mission['include_in_kpi'] = (getPost('include_in_kpi') == "null" ? 0 : 1);
    }
    
    if (getPost('mail_notif')) {
        $mission['mail_notif'] = (getPost('mail_notif') == "null" ? 0 : 1);
    }
    
    if (getPost('is_private')) {
        $mission['is_private'] = (getPost('is_private') == "null" ? 0 : 1);
    }
        
    $out_category = getPost('out_category'); // data seperated by |
    $out_category_ids = getPost('out_category_ids'); // data seperated by |
    $criteres = getPost('critere_specific'); // data seperated by |
    $criteres_ids = getPost('critere_specific_ids'); // data seperated by |
    $assignedUsers = getPost('users_id'); // users id seperated by |
     
    if (Mission::editMission($mission, $out_category, $out_category_ids, $criteres, $criteres_ids, $assignedUsers)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Mission modifiée avec succès',
            'type' => 'success',
            'callback' => 'reloadpage',
        );
        
        if (getPost('mission_id') && getPost('mission_id') != "") {
            $response['callback'] = 'gotopage';
            $response['param'] = array(
                'page' => 'tdc',
                'querystring' => 'id',
                'value' => getPost('mission_id'),
            );
        }
        
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>