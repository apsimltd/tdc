<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_task')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
       
    $task = array(
        'id' => getPost('id'),
        'title' => getPost('title'),
        'title_old' => getPost('title_old'),
        'description' => getPost('description'),
        'creator_id' => getPost('creator_id'),
        'assignee_id' => getPost('assignee_id'),
        'assignee_id_old' => getPost('assignee_id_old'),
        'modified' => time(),
    );
    
    if (Task::edit($task)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Tâche modifiée avec succès',
            'type' => 'success',
            'callback' => 'reloadpagenoparam',
//            'callback' => 'reloadtaskcreator',
//            'param' => $me['id'], // creator id
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>
