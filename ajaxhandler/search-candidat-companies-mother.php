<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('create_candidat', 'edit_candidat'))): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php
if (isPost()) {
    echo Client::SearchCandidatCompanyMother(getPost('search'));
    exit();
}
?>