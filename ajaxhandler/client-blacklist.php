<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_client')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isGet()) {
    
    if (get('op') == 'select') {
        $blacklist = 1;
    } elseif (get('op') == 'deselect') {
        $blacklist = 0;
    }
    
    $data = array(
        'blacklist' => $blacklist,
        'modified' => time(),
    );
    
    if (Client::blacklist(get('id'), $data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Client blacklister avec succès',
            'type' => 'success',
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>