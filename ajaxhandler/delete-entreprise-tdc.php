<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php 
if (isGet()) {
    
    $data = array(
        'status' => 0,
        'modified' => time(),
    );
    
    if (TDC::deleteEntrepriseInTDC(get('mission_id'), get('entreprise_id'))) {
        $response = array(
            'status' => 'OK',
            'msg' => 'Entreprise supprimée avec succès du TDC',
            'type' => 'success',
//            'callback' => 'reloadpage',
            'callback' => 'goto_tdc',
            'param' => get('mission_id'),
        );
        
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>
