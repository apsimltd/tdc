<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_candidat', 'tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php if (isPost()) : ?>
    
    <?php
    $data = array(  
        'name' => ltrim(getPost('name')),
        'telephone' => ltrim(getPost('telephone')),
        'control_localisation_id' => getPost('control_localisation_id'),
        'email' => ltrim(getPost('email')),
        'secteur_activite_id' => getPost('secteur_activite_id'),
        'sous_secteur_activite' => ltrim(getPost('sous_secteur_activite')),
    );
    
//    if (getPost('is_mother')) {
//        $data['is_mother'] = 1;
//    } 
        
    $entreprises = Candidat::SearchEntreprisesListSearch($data);
    ?>

    <?php if (User::can('read_candidat', 'edit_candidat')): ?>
    <div class="row">

        <div class="col col-12">
            <div class="block nopadding">

                <div class="block-head with-border">
                    <header><span class="glyphicon glyphicon-user"></span>Liste des entreprises</header>
                    <div class="block-head-btns pull-right">
                        <a href="<?php BASE_URL ?>/entreprises"><button type="button" class="btn btn-icon btn-success pull-right tooltips" title="Effacer filtre"><span class="glyphicon glyphicon-refresh"></span></button></a>
                    </div>
                </div>

                <div class="block-body">

                    <table class="datable stripe hover">
                        <thead>
                            <tr>
                                <th>Ref</th>
                                <th>Est Entreprise Mère</th>
                                <th>Nom</th>
                                <th>Entreprise Mère</th>
                                <th>Téléphone</th>
                                <th>Localisation</th>
                                <th>Précision adresse</th>
                                <th>Email</th>
                                <th>Secteur Activité</th>
                                <th>Sous Secteur Activité</th>
                                <th>Date création</th>
                                <th>Blacklist</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($entreprises as $entreprise): ?>
                            <tr>
                                <td><?php echo $entreprise['id'] ?></td>
                                <td>
                                    <?php if ($entreprise['is_mother'] == 1): ?>
                                        <span class="status" style="color:#ffffff; background-color:#339900">Mère</span>
                                        <span class="glyphicon glyphicon-plus see_child_companies tooltips" data-entreprise_id="<?php echo $entreprise['id'] ?>" title="Voir les sous entreprises"></span>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php echo $entreprise['name']; ?>
                                     <?php if ($entreprise['name'] != "" && Client::getClientCandidatBlacklist($entreprise['name'])): ?>
                                    <div class="clearboth notif warning" style="background-color:#ff9800; color: #fff; line-height: 24px; padding: 0px 10px;"><strong>Attention :</strong> La Société est blacklistée</div>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if ($entreprise['parent_id'] > 0): ?>
                                        <?php $mother_company = Candidat::getEntrepriseByIdNew($entreprise['parent_id']); ?>
                                        <?php echo $mother_company['name'] ?>
                                        <?php if ($mother_company['blacklist'] == 1): ?>
                                        <div class="clearboth notif warning" style="background-color:#ff9800; color: #fff; line-height: 24px; padding: 0px 10px;"><strong>Attention :</strong> La Société est blacklistée</div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td><?php echo $entreprise['telephone']; ?></td>
                                <td><?php echo $entreprise['control_localisation']; ?></td>
                                <td><?php echo $entreprise['addresse']; ?></td>
                                <td><?php echo $entreprise['email']; ?></td>
                                <td><?php echo $entreprise['secteur_activite']; ?></td>
                                <td><?php echo $entreprise['sous_secteur_activite']; ?></td>
                                <td data-order="<?php echo strtotime($entreprise['created']) ?>">
                                    <?php echo $entreprise['created'] ?>
                                </td>
                                <td align="center" style="width: 40px;">
                                    <div data-entreprise_id="<?php echo $entreprise['id'] ?>" class="tick <?php if (User::can('edit_candidat')): ?>entreprise_blacklist<?php endif; ?> <?php if ($entreprise['blacklist'] == 1): ?>active<?php endif; ?>"></div>
                                </td>
                                <td class="actions_button">
                                    <?php if (User::can('edit_candidat')): ?>                                    
                                    <?php
                                    $queryString = "";
                                    if (User::can('tdc') && get('tdc_id') && get('tdc_id') != "" && !TDC::isEntrepriseInTDC($entreprise['id'], get('tdc_id'))){
                                        $queryString = "&tdc_id=" . get('tdc_id');
                                    }
                                    ?>

                                    <a href="<?php echo BASE_URL ?>/editer-entreprise?id=<?php echo $entreprise['id'] . $queryString ?>" class="edit_candidat btn btn-icon btn-info tooltips" title="Éditer">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                    
                                    <button type="button" data-entreprise_id="<?php echo $entreprise['id'] ?>" class="comment_zoom btn btn-icon btn-primary tooltips" title="Historiques Commentaires"><span class="glyphicon glyphicon-comment"></span></button>
                                    <?php endif; ?>
                                    
                                    <button type="button" data-entreprise_id="<?php echo $entreprise['id'] ?>" class="entreprise_mission_history btn btn-icon btn-success tooltips" title="Historiques Missions"><span class="glyphicon glyphicon-tasks"></span></button>
                                    
                                    <button type="button" data-entreprise_id="<?php echo $entreprise['id'] ?>" class="generate_pdf_entreprise pull-right btn btn-icon btn-warning tooltips" title="Générer un fichier PDF"><i class="fa fa-file-pdf"></i></button>

                                    <?php if (User::can('tdc') && get('tdc_id') && get('tdc_id') != ""): ?>
                                        <?php 
                                        // check if the entreprise already in this tdc
                                        if (!TDC::isEntrepriseInTDC(get('tdc_id'), $entreprise['id'])) {
                                        ?>
                                        
                                        <a href="<?php echo AJAX_HANDLER ?>/add-entreprise-tdc?entreprise_id=<?php echo $entreprise['id'] ?>&mission_id=<?php echo get('tdc_id') ?><?php if (Client::getClientCandidatBlacklist($entreprise['name'])): ?>&blacklist=1&nom=<?php echo $entreprise['name'] ?><?php endif; ?>" class="pull-right btn btn-icon btn-info tooltips hidemeafterclick" title="Ajouter Entreprise au TDC Ref <?php echo get('tdc_id') ?>"><span class="glyphicon glyphicon-screenshot"></span></a>
                                        
                                        <?php } ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div><!-- / block-body -->

            </div>
        </div><!-- /col -->

    </div><!-- / row -->
    <?php endif; ?>

<?php endif; ?>
