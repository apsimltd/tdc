<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_task', 'read_all_task', 'create_task')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php
if (isPost()) {
       
    $taskThread = array(
        'task_id' => getPost('task_id'),
        'user_id' => $me['id'],
        'comment' => getPost('comment'),
        'created' => time(),
    );
    
    if (Task::addThread($taskThread)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Commentaire ajoutée avec succès',
            'type' => 'success',
            'callback' => 'reloadtaskview',
            'param' => getPost('task_id'),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>

