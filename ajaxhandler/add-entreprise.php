<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
//    debug($_FILES);
//    debug(getPost(), true);
       
    $entreprise = array(
        'user_id' => $me['id'],
        'parent_id' => getPost('parent_id'),
        'control_localisation_id' => getPost('control_localisation_id'),
        'name' => mb_strtoupper(filterCompanyName(rtrim(ltrim(getPost('name'))))),
        'secteur_activite_id' => (getPost('secteur_activite_id') == "")? 0 : getPost('secteur_activite_id'),
        'sous_secteur_activite' => getPost('sous_secteur_activite'),
        'telephone' => getPost('telephone'),
        'email' => getPost('email'),
        'addresse' => getPost('addresse'),
        // 'region' => getPost('region'),
        'created' => dateToDb(),
    );
        
    if (getPost('is_mother')) {
        $entreprise['is_mother'] = (getPost('is_mother') == "null" ? 0 : 1);
    }
    
    if (getPost('blacklist') && getPost('blacklist') == "on") {
        $entreprise['blacklist'] = 1;
    } else {
        $entreprise['blacklist'] = 0;
    }
    
    $commentaire = getPost('commentaire');
    $files = $_FILES['file']; // array 
    
    // first of all we validate if the candidate already exist
    
    if (!Candidat::CheckExistanceEntreprise(getPost('name'))) {
        $entreprise_id = Candidat::addEntrepriseNew($entreprise, $commentaire, $files);

        if (getPost('mission_id') && getPost('mission_id') != "") {
            // add the candidat to tdc
            $tdc = array(
                'mission_id' => getPost('mission_id'),
                'entreprise_id' => $entreprise_id,
                'user_id' => $me['id'],
                'created' => time(),
            );
            
            $tdc_id = TDC::addEntreprise($tdc);
            
            if ($tdc_id > 0) {
                header('Location:' . BASE_URL . '/tdc?id=' . getPost('mission_id') . '&tdc_id=' . $tdc_id);
            }
        } else {
            
            $notif = array(
                'status' => 'OK',
                'msg' => 'Entreprise ajoutée avec succès',
                'type' => 'success', 
            );
            setNotifCookie($notif);
            
            header('Location:' . BASE_URL . '/editer-entreprise?id='.$entreprise_id);
            
        }

    } else {
        if (getPost('mission_id') && getPost('mission_id') != "") {
            header('Location:' . BASE_URL . '/ajoute-entreprise?msg=C002&tdc_id=' . getPost('mission_id'));
        } else {
            header('Location:' . BASE_URL . '/ajoute-entreprise?msg=C002');
        }
    }  
    
} else {
    header('Location:' . BASE_URL . '/entreprises');
}

?>