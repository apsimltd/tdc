<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {

    if (Candidat::addMotivation(getPost('candidat_id'), getPost('mission_id'), getPost('motivation'))){
        $response = array(
            'status' => 'OK',
            'msg' => 'Motivation ajouté avec succès',
            'type' => 'success',
            'callback' => 'reloadmotivation',
            'param' => getPost('candidat_id')
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        );
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>