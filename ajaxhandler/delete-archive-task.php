<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('delete_task', 'archive_task'))): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php 
if (isGet()) {
    if (get('op') == "delete") {
        $status = 3;
        $msg = 'Tâche supprimée avec succès';
    } elseif (get('op') == "archive") {
        $status = 2;
        $msg = 'Tâche archivée avec succès';
    }
    
    $task = array(
        'id' => get('id'),
        'status' => $status,
        'modified' => time(),
    );
    
    if (Task::updateTask($task)) {
        $response = array(
            'status' => 'OK',
            'msg' => $msg,
            'type' => 'success',
            'callback' => 'reloadpagenoparam',
//            'callback' => 'reloadtaskcreator',
//            'param' => $me['id'], // creator id
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>
