<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_task')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
       
    $task = array(
        'title' => getPost('title'),
        'description' => getPost('description'),
        'creator_id' => $me['id'],
        'assignee_id' => getPost('assignee_id'),
        'created' => time(),
    );
    
    if (Task::add($task)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Tâche ajoutée avec succès',
            'type' => 'success',
            'callback' => 'reloadpage',
            ///'callback' => 'reloadtaskcreator',
            ///'param' => $me['id'], // creator id
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>
