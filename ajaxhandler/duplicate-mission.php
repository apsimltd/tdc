<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('duplicate_mission')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isGet()) {
    
    $mission_id = get('id');
    
    $new_mission_id = Mission::duplicate($mission_id);
    $response = array(
        'status' => 'OK',
        'msg' => 'Mission dupliquée avec succès',
        'type' => 'success',
        'url' => BASE_URL . '/tdc?id=' . $new_mission_id,
    );
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>
