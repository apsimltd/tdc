<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_client')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
        
    $client = array(
        'nom' => mb_strtoupper(getPost('nom')),
        'address' => getPost('address'),
        'telephone' => getPost('telephone'),
        'email' => getPost('email'),
        'contact_firstName' => getPost('contact_firstName'),
        'contact_lastName' => getPost('contact_lastName'),
        'contact_post' => getPost('contact_post'),
        'contact_telephone' => getPost('contact_telephone'),
        'contact_email' => getPost('contact_email'),
        'created' => time(),
    );
    
    if (getPost('blacklist') && getPost('blacklist') == "on") {
        $client['blacklist'] = 1;
    }
     
    if (Client::addClient($client)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Client ajouté avec succès',
            'type' => 'success',
            'callback' => 'reloadpage',
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>