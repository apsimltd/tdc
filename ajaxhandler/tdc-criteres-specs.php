<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
    $data = array(
        'user_id' => $me['id'],
        'mission_id' => getPost('mission_id'),
        'candidat_id' => getPost('candidat_id'),
        'criteres_ids' => getPost('criteres_ids'), // seperated by |
        'tdc_id' => getPost('tdc_id'),
        'commentaire_criteres' => getPost('commentaire_criteres'),
        'created' => time(),
    );
     
    if (TDC::updateCriteres($data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Critères spécifiques mises à jour avec succès',
            'type' => 'success',
            'callback' => 'reloadcriteres',
            'param' => array(
                'candidat_id' => getPost('candidat_id'),
                'mission_id' => getPost('mission_id'),
                'tdc_id' => getPost('tdc_id'),
            ),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>