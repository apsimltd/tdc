<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('delete_event')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isGet()) {
    
    $id = get('id');
    
    if (Event::delete($id)) {
        $response = array(
            'status' => 'OK',
            'msg' => 'Événement supprimé avec succès',
            'type' => 'success',
            'callback' => 'reloadpage',
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>