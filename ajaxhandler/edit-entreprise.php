<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
//    debug($_FILES);
//    debug(getPost(), true);
       
    $entreprise = array(
        'id' => getPost('id'),
        'user_id' => $me['id'],
        'parent_id' => getPost('parent_id'),
        'control_localisation_id' => getPost('control_localisation_id'),
        'name' => mb_strtoupper(filterCompanyName(rtrim(ltrim(getPost('name'))))),
        'secteur_activite_id' => (getPost('secteur_activite_id') == "")? 0 : getPost('secteur_activite_id'),
        'sous_secteur_activite' => getPost('sous_secteur_activite'),
        'telephone' => getPost('telephone'),
        'email' => getPost('email'),
        'addresse' => getPost('addresse'),
        // 'region' => getPost('region'),
        'modified' => dateToDb(),
    );
    
    if (getPost('is_mother')) {
        $entreprise['is_mother'] = (getPost('is_mother') == "null" ? 0 : 1);
    }
    
    if (getPost('blacklist') && getPost('blacklist') == "on") {
        $entreprise['blacklist'] = 1;
    } else {
        $entreprise['blacklist'] = 0;
    }
    
    if (Candidat::EditEntrepriseNew($entreprise)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Entreprise modifiée avec succès',
            'type' => 'success',
        );
        
//        if (getPost('mission_id') && getPost('mission_id') != "") {
//            $response['callback'] = 'gotopage';
//            $response['param'] = array(
//                'page' => 'tdc',
//                'querystring' => 'id',
//                'value' => getPost('mission_id'),
//            );
//        }
        
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>