<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_user')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
        
    $user = array(
        'id' => getPost('id'),
        'user_id' => $me['id'], // the user who edited the user client
        'client_id' => getPost('client_id'),
        'role_id' => 13,
        'firstName' => getPost('firstName'),
        'lastName' => getPost('lastName'),
        'email' => getPost('email'),
        'username' => getPost('username'),
        'status' => getPost('status'),
        'modified' => time(),
    );
    
    if (getPost('mail_notif')) {
        $user['mail_notif'] = (getPost('mail_notif') == "null" ? 0 : 1);
    }
    
    $assigned_mission_ids = getPost('mission_ids'); 
      
    // check if username has changed
    if (getPost('old_username') != $user['username']) { // username changes
        // check if there is duplicate new username
        if (User::isLoginExist($user['username'], $user['id'])) {
            $response = array(
                'status' => 'NOK',
                'msg' => 'Cette identifiant existe déjà. Merci d\'utiliser une autre identifiant.',
                'type' => 'error',           
            ); 
            echo json_encode($response);
            exit();
        }
    }
    
    // check if email has changed
    if (getPost('old_email') != $user['email']) { // username changes
        // check if there is duplicate new email
        if (User::isUserMailExist($user['email'], $user['id'])) {
             $response = array(
                'status' => 'NOK',
                'msg' => 'Cet email existe déjà. Merci d\'utiliser un autre email.',
                'type' => 'error',           
            ); 
            echo json_encode($response);
            exit();
        }
    }
    
    // check if the password changed and append the new 
    // password in the user array to be hashed and send
    // to the user by mail
    if (getPost('password') && getPost('password') != "") {
        $user['password'] = getPost('password');
    }

    // save the user edit mode
    if (User::editUserClient($user, $assigned_mission_ids)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Utilisateur Client modifié avec succès',
            'type' => 'success',
            'callback' => 'reloadpage',
        );
        $notif = array(
            'status' => 'OK',
            'msg' => 'Utilisateur Client modifié avec succès',
            'type' => 'success',
        );
        setNotifCookie($notif);
        echo json_encode($response);
        exit();
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        );
        echo json_encode($response);
        exit();
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
    echo json_encode($response);
    exit();
}

?>