<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_type_content', 'edit_type_content')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
    $type = getPost('type');
    
    // edit mode
    if (getPost('id')) {
        $type_content = array(
            'id' => getPost('id'),
            'type_id' => getPost('type_id'),
            'name' => getPost('name'),
            'status' => getPost('status'),
        );
        $message = $type . ' edité avec succès';
    } else {
        $type_content = array(
            'type_id' => getPost('type_id'),
            'name' => getPost('name'),
            'status' => getPost('status'),
        );
        $message = $type . ' ajouté avec succès';
    }
       
    if (Control::addControl($type_content)){
        $response = array(
            'status' => 'OK',
            'msg' => $message,
            'type' => 'success',
            'callback' => 'reloadtypecontent',
            'param' => getPost('type_id'),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>