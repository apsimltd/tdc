<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_candidat', 'tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php if (isPost()) : ?>
    
    <?php
    $candidatSearch = array(
        'user_id' => $me['id'],
        'nom' => getPost('nom'),
        'prenom' => getPost('prenom'),
        'societe_actuelle' => getPost('societe_actuelle'),
        'control_localisation_id' => getPost('control_localisation_id'),
        'control_secteur_id' => getPost('control_secteur_id'),
        'control_fonction_id' => getPost('control_fonction_id'),
        'control_niveauFormation_id' => getPost('control_niveauFormation_id'),
        'age_from' => getPost('age_from'),
        'age_to' => getPost('age_to'),
        'remuneration_from' => getPost('remuneration_from'),
        'remuneration_to' => getPost('remuneration_to'),
        'tres_bon_candidat' => getPost('tres_bon_candidat'),
        'shortliste' => getPost('shortliste'),
        'place' => getPost('place'),
        'email' => getPost('email'),
        'tel' => getPost('tel'),
//        'company_blacklisted' => getPost('company_blacklisted'),
        'poste' => getPost('poste'),
        'control_source_id' => getPost('control_source_id'),
        'diplome_specialisation' => getPost('diplome_specialisation'),
        'cv_fourni' => getPost('cv_fourni'),
        'dateDiplome_from' => (getPost('dateDiplome_from') == "")? "" : strtotime(getPost('dateDiplome_from')),
        'dateDiplome_to' => (getPost('dateDiplome_to') == "")? "" : strtotime(getPost('dateDiplome_to')),
        'detail_remuneration' => getPost('detail_remuneration'),
//        'salaire_package' => getPost('salaire_package'),
//        'salaire_package_from' => getPost('salaire_package_from'),
//        'salaire_package_to' => getPost('salaire_package_to'),
        'salaire_fixe_from' => getPost('salaire_fixe_from'),
        'salaire_fixe_to' => getPost('salaire_fixe_to'),
//        'sur_combien_de_mois_from' => getPost('sur_combien_de_mois_from'),
//        'sur_combien_de_mois_to' => getPost('sur_combien_de_mois_to'),
        'avantages' => getPost('avantages'),
        'remuneration_souhaitee' => getPost('remuneration_souhaitee'),
        'documents' => getPost('documents'),
    );
    
    $candidats = Candidat::getCandidatsListSearch($candidatSearch);
    ?>

    <?php if (User::can('read_candidat', 'edit_candidat')): ?>
    <div class="row">

        <div class="col col-12">
            <div class="block nopadding">

                <div class="block-head with-border">
                    <header><span class="glyphicon glyphicon-user"></span>Liste des candidats</header>
                    <div class="block-head-btns pull-right">
                        <a href="<?php BASE_URL ?>/candidats"><button type="button" class="btn btn-icon btn-success pull-right tooltips" title="Effacer filtre"><span class="glyphicon glyphicon-refresh"></span></button></a>
                    </div>
                </div>

                <div class="block-body">

                    <table class="datable stripe hover">
                        <thead>
                            <tr>
                                <th>Ref</th>
                                <th>Candidat</th>
                                <th>DPT</th>
                                <th>Âge</th>
                                <th>Société</th>
                                <th>Poste</th>
                                <th>Rémunération</th>
                                <th>Secteur</th>
                                <th>Fonction</th>
                                <th>CV</th>
                                <th>TBC <span class="glyphicon glyphicon-info-sign tooltips" title="Très Bon Candidat"></span></th>
                                <th>Date création</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $civilite = Control::getControlListByType(7); ?>
                            <?php $dpt = Control::getControlListByType(6); ?>
                            <?php $secteurs = Control::getControlListByType(2); ?>
                            <?php $fonctions = Control::getControlListByType(1); ?>
                            <?php foreach($candidats as $candidat): ?>
                            <tr>
                                <td><?php echo $candidat['id'] ?></td>
                                <td>
                                    <?php 
                                    if ($candidat['control_civilite_id'] == 0): 
                                        echo mb_strtoupper($candidat['nom']) . ' ' . mb_ucfirst($candidat['prenom']);
                                    else:
                                        echo $civilite[$candidat['control_civilite_id']] . ' ' . mb_strtoupper($candidat['nom']) . ' ' . mb_ucfirst($candidat['prenom']);
                                    endif; ?>
                                    <?php if ($candidat['gdpr_status'] == 1): ?>
                                        <div class="clearboth notif warning" style="background-color:#ff9800; color: #fff; line-height: 16px; padding: 0px 10px;"><strong>Attention :</strong> Candidat Inactif (RGPD)</div>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php
                                    if ($candidat['control_localisation_id'] > 0) {
                                        echo $dpt[$candidat['control_localisation_id']];
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($candidat['dateOfBirth'] != 0) {
                                        echo getAge($candidat['dateOfBirth']);
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php echo mb_strtoupper($candidat['societe_actuelle']) ?>
                                     <?php if ($candidat['societe_actuelle'] != "" && Client::getClientCandidatBlacklist($candidat['societe_actuelle'])): ?>
                                    <div class="clearboth notif warning" style="background-color:#ff9800; color: #fff; line-height: 24px; padding: 0px 10px;"><strong>Attention :</strong> La Société est blacklistée</div>
                                    <?php endif; ?>
                                </td>
                                <td><?php echo $candidat['poste'] ?></td>
                                <td>
                                    <?php
                                    if ($candidat['remuneration'] > 0){
                                        echo $candidat['remuneration'] . ' K€';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                    if ($candidat['control_secteur_id'] > 0){
                                        echo $secteurs[$candidat['control_secteur_id']];
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($candidat['control_fonction_id'] > 0) {
                                        echo $fonctions[$candidat['control_fonction_id']];
                                    }
                                    ?>
                                </td>
                                <td align="center">
                                    <span class="status <?php if($candidat['cv_fourni'] == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
                                        <?php if($candidat['cv_fourni'] == 1): ?>Oui<?php else: ?>Non<?php endif; ?>
                                    </span>
                                </td>
                                <td align="center">
                                    <span class="status <?php if($candidat['tres_bon_candidat'] == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
                                        <?php if($candidat['tres_bon_candidat'] == 1): ?>Oui<?php else: ?>Non<?php endif; ?>
                                    </span>
                                </td>
                                <td data-order="<?php echo $candidat['created'] ?>">
                                    <span title="<?php echo $candidat['created'] ?>"><?php echo dateToFr($candidat['created'], true) ?></span> 
                                </td>
                                <td class="actions_button">
                                    <?php if (User::can('edit_candidat')): ?>
                                    <?php
                                    $queryString = "";
                                    if (User::can('tdc') && get('tdc_id') && get('tdc_id') != "" && !TDC::isCandidatInTDC($candidat['id'], get('tdc_id'))){
                                        $queryString = "&tdc_id=" . get('tdc_id');
                                    }
                                    ?>

                                    <a href="<?php echo BASE_URL ?>/editer-candidat?id=<?php echo $candidat['id'] . $queryString ?>" class="edit_candidat btn btn-icon btn-info tooltips" title="Éditer">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>

                                    <button type="button" data-candidat_id="<?php echo $candidat['id'] ?>" class="comment_zoom btn btn-icon btn-primary tooltips" title="Historiques Commentaires"><span class="glyphicon glyphicon-comment"></span></button>
                                    <?php endif; ?>
                                    <button type="button" data-candidat_id="<?php echo $candidat['id'] ?>" class="candidat_mission_history btn btn-icon btn-success tooltips" title="Historiques Missions"><span class="glyphicon glyphicon-tasks"></span></button>
                                    <button type="button" data-candidat_id="<?php echo $candidat['id'] ?>" class="generate_pdf_candidat pull-right btn btn-icon btn-warning tooltips" title="Générer un fichier PDF"><i class="fa fa-file-pdf"></i></button>
                                    <?php if (User::can('tdc') && get('tdc_id') && get('tdc_id') != ""): ?>
                                        <?php 
                                        // check if the candidat already in this tdc
                                        if (!TDC::isCandidatInTDC($candidat['id'], get('tdc_id'))) {
                                        ?>
<!--                                        <a href="<?php ///echo AJAX_HANDLER ?>/add-candidat-tdc?candidat_id=<?php ///echo $candidat['id'] ?>&mission_id=<?php ///echo get('tdc_id') ?><?php ///if ($candidat['company_blacklisted'] == 1): ?>&blacklist=1<?php ///endif; ?>" class="pull-right btn btn-icon btn-info tooltips hidemeafterclick" title="Ajouter Candidat au TDC Ref <?php ///echo get('tdc_id') ?>"><span class="glyphicon glyphicon-screenshot"></span></a>-->
                                        
                                    <a href="<?php echo AJAX_HANDLER ?>/add-candidat-tdc?candidat_id=<?php echo $candidat['id'] ?>&mission_id=<?php echo get('tdc_id') ?><?php if (Client::getClientCandidatBlacklist($candidat['societe_actuelle'])): ?>&blacklist=1&nom=<?php echo $candidat['societe_actuelle'] ?><?php endif; ?>" class="pull-right btn btn-icon btn-info tooltips hidemeafterclick" title="Ajouter Candidat au TDC Ref <?php echo get('tdc_id') ?>"><span class="glyphicon glyphicon-screenshot"></span></a>
                                        
                                        <?php } ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div><!-- / block-body -->

            </div>
        </div><!-- /col -->

    </div><!-- / row -->
    <?php endif; ?>

<?php endif; ?>
