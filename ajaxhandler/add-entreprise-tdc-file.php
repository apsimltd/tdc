<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
        
    $files = $_FILES['file']; // array
     
    Candidat::addDocumentsEntreprise(getPost('entreprise_id'), $files);
    
    $notif = array(
        'status' => 'OK',
        'msg' => 'Documents ajoutés avec succès',
        'type' => 'success', 
    );
    setNotifCookie($notif);
    
    if (getPost('mission_id') && !getPost('tdc_line_id')) {

        header('Location:' . BASE_URL . '/tdc?id=' . getPost('mission_id'));
        
    } elseif (getPost('tdc_line_id') && getPost('mission_id')) {
        
        header('Location:' . BASE_URL . '/tdc?id=' . getPost('mission_id') . '&tdc_id=' . getPost('tdc_line_id'));
        
    }
    
}
?>