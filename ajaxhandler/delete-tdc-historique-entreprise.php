<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php 
if (isGet()) {
    
    if (TDC::deleteHistoriqueEntreprise(get('historique_id'))) {
        $response = array(
            'status' => 'OK',
            'msg' => 'Commentaire supprimée avec succès',
            'type' => 'success',
            'callback' => 'reloadhistoriqueentreprise',
            'param' => array(
                'mission_id' => get('mission_id'),
                'entreprise_id' => get('entreprise_id'),
            ),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>
