<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php $entreprises = Candidat::getChildCompanies(getPost('id')); ?>
<?php //debug($suivis); ?>

<tr class="ajaxRowL">
    <td colspan="13">
        <table class="datable stripe hover compact" style="width: 100%;">
            <thead>
                <tr>
                    <th>Ref</th>
                    <th>Est Entreprise Mère</th>
                    <th>Nom</th>
                    <th>Entreprise Mère</th>
                    <th>Téléphone</th>
                    <th>Localisation</th>
                    <th>Précision adresse</th>
                    <th>Email</th>
                    <th>Secteur Activité</th>
                    <th>Sous Secteur Activité</th>
                    <th>Date création</th>
                    <th>Blacklist</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($entreprises as $entreprise): ?>
                <tr>
                    <td><?php echo $entreprise['id'] ?></td>
                    <td>
                        <?php if ($entreprise['is_mother'] == 1): ?>
                            <span class="status" style="color:#ffffff; background-color:#339900">Mère</span>
                            <button type="button" data-entreprise_id="<?php echo $entreprise['id'] ?>" class="see_child_companies btn btn-icon btn-info tooltips" title="Voir les sous entreprises"><i class="fas fa-eye togglePassword"></i></button>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php echo $entreprise['name']; ?>
                         <?php if ($entreprise['name'] != "" && Client::getClientCandidatBlacklist($entreprise['name'])): ?>
                        <div class="clearboth notif warning" style="background-color:#ff9800; color: #fff; line-height: 24px; padding: 0px 10px;"><strong>Attention :</strong> La Société est blacklistée</div>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($entreprise['parent_id'] > 0): ?>
                            <?php $mother_company = Candidat::getEntrepriseByIdNew($entreprise['parent_id']); ?>
                            <?php echo $mother_company['name'] ?>
                        <?php endif; ?>
                    </td>
                    <td><?php echo $entreprise['telephone']; ?></td>
                    <td><?php echo $entreprise['control_localisation']; ?></td>
                    <td><?php echo $entreprise['addresse']; ?></td>
                    <td><?php echo $entreprise['email']; ?></td>
                    <td><?php echo $entreprise['secteur_activite']; ?></td>
                    <td><?php echo $entreprise['sous_secteur_activite']; ?></td>
                    <td data-order="<?php echo strtotime($entreprise['created']) ?>">
                        <?php echo $entreprise['created'] ?>
                    </td>
                    <td align="center" style="width: 40px;">
                        <div data-entreprise_id="<?php echo $entreprise['id'] ?>" class="tick <?php if (User::can('edit_candidat')): ?>entreprise_blacklist<?php endif; ?> <?php if ($entreprise['blacklist'] == 1): ?>active<?php endif; ?>"></div>
                    </td>
                    <td class="actions_button">
                        <?php if (User::can('edit_candidat')): ?>                                    
                        <?php
                        $queryString = "";
                        if (User::can('tdc') && get('tdc_id') && get('tdc_id') != "" && !TDC::isEntrepriseInTDC($entreprise['id'], get('tdc_id'))){
                            $queryString = "&tdc_id=" . get('tdc_id');
                        }
                        ?>

                        <a href="<?php echo BASE_URL ?>/editer-entreprise?id=<?php echo $entreprise['id'] . $queryString ?>" class="edit_candidat btn btn-icon btn-info tooltips" title="Éditer">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>

                        <button type="button" data-entreprise_id="<?php echo $entreprise['id'] ?>" class="comment_zoom btn btn-icon btn-primary tooltips" title="Historiques Commentaires"><span class="glyphicon glyphicon-comment"></span></button>
                        <?php endif; ?>

                        <button type="button" data-entreprise_id="<?php echo $entreprise['id'] ?>" class="entreprise_mission_history btn btn-icon btn-success tooltips" title="Historiques Missions"><span class="glyphicon glyphicon-tasks"></span></button>

                        <button type="button" data-entreprise_id="<?php echo $entreprise['id'] ?>" class="generate_pdf_entreprise pull-right btn btn-icon btn-warning tooltips" title="Générer un fichier PDF"><i class="fa fa-file-pdf"></i></button>

                        <?php if (User::can('tdc') && get('tdc_id') && get('tdc_id') != ""): ?>
                            <?php 
                            // check if the entreprise already in this tdc
                            if (!TDC::isEntrepriseInTDC(get('tdc_id'), $entreprise['id'])) {
                            ?>

                            <a href="<?php echo AJAX_HANDLER ?>/add-entreprise-tdc?entreprise_id=<?php echo $entreprise['id'] ?>&mission_id=<?php echo get('tdc_id') ?><?php if (Client::getClientCandidatBlacklist($entreprise['name'])): ?>&blacklist=1&nom=<?php echo $entreprise['name'] ?><?php endif; ?>" class="pull-right btn btn-icon btn-info tooltips hidemeafterclick" title="Ajouter Entreprise au TDC Ref <?php echo get('tdc_id') ?>"><span class="glyphicon glyphicon-screenshot"></span></a>

                            <?php } ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </td>
</tr>
