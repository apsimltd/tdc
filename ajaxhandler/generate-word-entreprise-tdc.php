<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_candidat') || get('id') == ""): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php
$societe = get('societe'); // opsearch or headhunting
$mission_id = get('mission_id');

$entreprise = Candidat::getEntrepriseByIdNew(get('id'));
if ($entreprise['commentaire'] <> "") {
    $comment = array_reverse(json_decode($entreprise['commentaire'], true));
} else {
    $comment = array();
}
$mission = Mission::getMissionDetailsById($mission_id);

// https://github.com/PHPOffice/PHPWord
// http://phpword.readthedocs.io/en/latest/
// Creating the new document...
$phpWord = new \PhpOffice\PhpWord\PhpWord();

// document properties
$properties = $phpWord->getDocInfo();

if ($societe == 'opsearch') {
    
    $properties->setCreator('OPSEARCH');
    $properties->setCompany('OPSEARCH');
    
} elseif ($societe == 'headhunting') {
    
    $properties->setCreator('HEADHUNTING FACTORY');
    $properties->setCompany('HEADHUNTING FACTORY');
    
}

$title = mb_ucfirst($entreprise['name']);
$properties->setTitle('Entreprise::' . $title);

/* Note: any element you append to a document must reside inside of a Section. */

// Adding an empty Section to the document...
$section = $phpWord->addSection();

// header  http://phpword.readthedocs.io/en/latest/containers.html
// Add first page header
$header = $section->addHeader();

if ($societe == 'opsearch') {
    $header->addImage('../asset/images/word/opsearch.png', array('height' => 25, 'width' => 213, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
} elseif ($societe == 'headhunting') {
    $header->addImage('../asset/images/word/headhunting.png', array('height' => 20, 'width' => 213, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
}

// footer http://phpword.readthedocs.io/en/latest/containers.html
// Add footer
$footer = $section->addFooter();
$footer->addPreserveText('{PAGE}/{NUMPAGES}', null, array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));

#####################
$section->addText(''); // adding a blank text for space between header and content

$label_style = array('size' => 11, 'bold' => true);
$value_style = array('size' => 11);
$table = $section->addTable();

// nom client
$table->addRow(600);
$table->addCell(3000)->addText('Client :', $label_style);
$table->addCell(5500)->addText(mb_strtoupper($mission['nom_client']), $value_style);

// poste
$table->addRow(600);
$table->addCell(3000)->addText('Poste :', $label_style);
$table->addCell(5500)->addText(mb_ucfirst($mission['poste']), $value_style);

// name
$table->addRow(600);
$table->addCell(3000)->addText('Nom :', $label_style);
$table->addCell(5500)->addText(mb_ucfirst($entreprise['name']), $value_style);

// coordonees
$table->addRow(600);
$table->addCell(3000)->addText('Coordonnées :', $label_style);


$innertable = $table->addCell(5500)->addTable(array('cellMargin' => 80));

if ($entreprise['telephone'] != "") {
    $innertable->addRow(500);
    $innertable->addCell()->addText('Téléphone : ', $label_style);
    $innertable->addCell()->addText($entreprise['telephone'], $value_style);
}

$table->addRow(600);
$table->addCell(3000)->addText('Région :', $label_style);
$table->addCell(5500)->addText($entreprise['region'], $value_style);

$table->addRow(600);
$table->addCell(3000)->addText('Email :', $label_style);
$table->addCell(5500)->addText($entreprise['email'], $value_style);

$table->addRow(600);
$table->addCell(3000)->addText('Secteur Activité :', $label_style);
$table->addCell(5500)->addText($entreprise['secteur_activite'], $value_style);

$table->addRow(600);
$table->addCell(3000)->addText('Sous Secteur Activité :', $label_style);
$table->addCell(5500)->addText($entreprise['sous_secteur_activite'], $value_style);

// commentaire:
if (!empty($comment)) {
    
    // move to the next page
    // $section->addPageBreak();
    
    $section->addText('Commentaire :', $label_style);
    
    $section->addText($comment[0]['comment']);
    
}

// generate the word doc
$file_name = 'TDC_' . $mission['id'] . '_' . filterChar(mb_ucfirst($entreprise['name'])) .'.docx';
$file = PDF_URL . '/' . $file_name;

// save the word file
// Saving the document as OOXML file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$objWriter->save($file);

$response = array(
    'status' => 'OK',
    'file' => $file_name,
);

echo json_encode($response);
exit();
?>