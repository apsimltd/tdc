<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php 
if (isGet()) {
    
    if (Candidat::deleteCommentEntreprise(get('comment_id'), get('entreprise_id'))) {
        $response = array(
            'status' => 'OK',
            'msg' => 'Commentaire supprimé avec succès',
            'type' => 'success',
            'callback' => 'reloadcomment',
            'param' => get('entreprise_id'),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>