<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('delete_mission')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isGet()) {

    if (Mission::deleteReal(get('mission_id'))){
        $response = array(
            'status' => 'OK',
            'msg' => 'Mission suprimée avec succès',
            'type' => 'success',
            'callback' => 'reloadpage',
        );
        $notif = array(
            'status' => 'OK',
            'msg' => 'Mission suprimée avec succès',
            'type' => 'success',
        );
        setNotifCookie($notif);
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        );
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>