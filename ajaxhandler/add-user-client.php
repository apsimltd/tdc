<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_user')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
        
    $user = array(
        'user_id' => $me['id'], // the user who added the user client
        'client_id' => getPost('client_id'),
        'role_id' => 13,
        'firstName' => getPost('firstName'),
        'lastName' => getPost('lastName'),
        'email' => getPost('email'),
        'username' => getPost('username'),
        'password' => getPost('password'),
        'status' => getPost('status'),
        'created' => time(),
    );
    
    if (getPost('mail_notif')) {
        $user['mail_notif'] = (getPost('mail_notif') == "null" ? 0 : 1);
    }
    
    $assigned_mission_ids = getPost('mission_ids'); // mission id seperated by |
    
    // check if username already exist
    if (User::isLoginExist($user['username'])) {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Cette identifiant existe déjà. Merci d\'utiliser une autre identifiant.',
            'type' => 'error',           
        ); 
        echo json_encode($response);
        exit();
    }
    
    // check if user email exist
    if (User::isUserMailExist($user['email'])) {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Cet email existe déjà. Merci d\'utiliser un autre email.',
            'type' => 'error',           
        ); 
        echo json_encode($response);
        exit();
    }
    
    if (User::addUserClient($user, $assigned_mission_ids)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Utilisateur Client ajouté avec succès',
            'type' => 'success',
            'callback' => 'reloadpage',
        );
        
        $notif = array(
            'status' => 'OK',
            'msg' => 'Utilisateur Client ajouté avec succès',
            'type' => 'success',
        );
        setNotifCookie($notif);
        
        echo json_encode($response);
        exit();
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        );
        echo json_encode($response);
        exit();
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
    echo json_encode($response);
    exit();
}

?>