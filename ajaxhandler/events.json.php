<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('view_event')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php
$events = Event::getEvents($me['id']);

// debug($events);

$response = array();
$response['success'] = 1;
$response['result'] = array();

foreach($events as $event):
    
    // if event is private and the user logged is not the owner of the event do not push into the array
    // then we move to the next iteration
    if (!User::can('view_all_event')) { 
        if ($event['private'] == 1 && $event['user_id'] != $me['id']) {
            continue;
        }
    }    
    
    $class = "event-". $event['type'];
    if ($_SESSION['opsearch_user']['id'] == $event['user_id']) { // only owner can edit or delete its event
        $class .= ' triggerEvent mine';
    } else {
        $class .= ' yours';
    }
    
    // if event completed must lock edit and view only by passing a class into it
    $microtimeEnd = $event['end'];
    $microtimeNow = time() * 1000;
    // check if date is not today first
    if ($microtimeNow > $microtimeEnd) {
        $class .= ' completed';
    } 
    
    $start = date('H:i', ($event['start']/1000));
    $end = date('H:i', ($event['end']/1000));
    
    $response['result'][] = array(
        'id' => $event['id'],
        'title' => $event['title'] . ' - ' . $start . '-' . $end . ' - ' . mb_ucfirst(substr($event['firstName'], 0, 1)) . '. ' . mb_strtoupper($event['lastName']),
        'class' => $class,
        'start' => $event['start'],
        'end' => $event['end'],
    );
    
endforeach;

echo json_encode($response);
exit();
?>