<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
    $data = array(
        'user_id' => $me['id'],
        'control_statut_tdc' => getPost('control_statut_tdc'),
        'mission_id' => getPost('mission_id'),
        'candidat_id' => getPost('candidat_id'),
        'control_statut_tdc_old' => getPost('control_statut_tdc_old'),
        'user_id_kpi_last_status' => $me['id'],
        'modified' => time(),
    );
     
    if (TDC::updateCandidatInTDCStatus(getPost('tdc_id'), $data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Statut mis à jour avec succès',
            'type' => 'success',
            'callback' => 'reloadstatus',
            'param' => array(
                'mission_id' => getPost('mission_id'),
                'tdc_id' => getPost('tdc_id'),
            ),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>