<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_my_profile')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php
if (isPost()) {
        
    $user = array(
        'id' => getPost('id'),
        'modified' => dateToDb(),
    );
    
    if (getPost('mail_notif')) {
        $user['mail_notif'] = (getPost('mail_notif') == "null" ? 0 : 1);
    }
    
    // check if the password changed and append the new 
    // password in the user array to be hashed and send
    // to the user by mail
    if (getPost('password') && getPost('password') != "" && getPost('new_password') && getPost('new_password') != "") {       
        $user['password'] = getPost('new_password');
    }

    // save the user edit mode
    if (User::editMyProfile($user)){
        
        $response = array(
            'status' => 'OK',
            'msg' => 'Mon profil modifié avec succès',
            'type' => 'success',
            'callback' => 'logout',
        );
        
        $notif = array(
            'status' => 'OK',
            'msg' => 'Mon profil modifié avec succès',
            'type' => 'success', 
        );
        setNotifCookie($notif);
        
        echo json_encode($response);
        exit();
        
    } else {
        
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        );
        echo json_encode($response);
        exit();
        
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
    echo json_encode($response);
    exit();
}

?>