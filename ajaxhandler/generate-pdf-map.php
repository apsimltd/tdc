<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc') || get('mission_id') == ""): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>

<?php

$mission_id = get('mission_id'); // mission id
$mission = Mission::getMissionDetailsById($mission_id);

require_once __DIR__ . '/../includes/functions.php';

$has_mission = $mission_id !== 0;
$candidats_par_departments = $mission_id === 0 ? false : load_candidats_par_departments($mission_id);

$bg_colors = array();
$counter = 0;

//debug($candidats_par_departments);

$grouped_regions_dept = array(75, 77, 78, 91, 92, 93, 94, 95);
$sum_grouped_dept = 0;

$greatest_value_dept_key = '';

foreach($candidats_par_departments as $key => $data) :
    
    // new modif july 2021
    // must grouped the dept 75, 77, 78, 91, 92, 93, 94 and 95 as one region and display the total num of candidats
    // must also add the region Ile de france into it as candidat number => ile de france has control id 221 in table controles but this is not a region
    // in the map
    
    /// if ($key == 75 || $key == 92 || $key == 93 || $key == 94) {
    if (in_array($key, $grouped_regions_dept)) {
        
        //$bg_colors[$key] = '#00FF40'; // must changed the color later => for now it is green
        $sum_grouped_dept += $data['nombre'];
        
//        if ($key == 75) {
//            $bg_colors[$key] = '#FF0000'; 
//        } else if ($key == 92) {
//            $bg_colors[$key] = '#36D900'; 
//        } else if ($key == 93) {
//            $bg_colors[$key] = '#FF8000'; 
//        } else if ($key == 94) {
//            $bg_colors[$key] = '#D900D9'; 
//        }
        
    } else {
        if ($counter == 0) {
            $bg_colors[$key] = '#EE417C'; // red => the highest num of candidats is always the first index
        } else {
            $bg_colors[$key] = '#2A77B4'; // orange => the rest is orange
        }
    }
    
    if ($counter == 0) {
        $greatest_value_dept_key = $key;
        $bg_colors[$key] = '#EE417C'; // red => the highest num of candidats is always the first index
    }
    
    
    $counter++;
endforeach;

// if the greatest value is in the grouped depts => color it red as the highest dept

if (in_array($greatest_value_dept_key, $grouped_regions_dept)) {
    
    foreach($grouped_regions_dept as $grouped_dept) :
        
        $bg_colors[$grouped_dept] = '#EE417C'; // red as its the highest number of candidates
        
    endforeach;
    
} 

// must add ile de france in the $sum_grouped_dept
$num_candidates_in_ile_de_france = TDC::getTotalCandidatInIleDeFrance($mission_id);

// sum $sum_grouped_dept and $num_candidates_in_ile_de_france
$sum_grouped_dept += $num_candidates_in_ile_de_france;

//debug($mission);

// config @src https://mpdf.github.io/reference/mpdf-functions/construct.html
$pdfConfig = array(
    'mode' => 'utf-8',
    // 'tempDir' => __DIR__ . 'C:/wamp/www/mybox/mpdf/' // folder should be writable
    'format' => [279, 216], // default => A4
    // 'format' => [400, 216], // default => A4
    //'orientation' => 'L', // P => portrait, L => Landscape
    'default_font_size' => 9, // in points
    'default_font' => 'Arial', // Arial, Helvetica, sans-serif
    'margin_left' => 15,
    'margin_right' => 15,
    'margin_top' => 25,
    'margin_bottom' => 16,
    'margin_header' => 5,
    'margin_footer' => 5,
);

// everything should be in millimeters for the pdf

$mpdf = new \Mpdf\Mpdf($pdfConfig);


/// problems with https images
/// https://stackoverflow.com/questions/44822433/images-with-https-in-mpdf
/// $mpdf->curlAllowUnsafeSslRequests = true;

// pdf file metadata
// metadata
$mpdf->SetTitle('TDC::'.mb_strtoupper($mission['nom_client']) . '-' . $mission['poste']);


// header
// https://mpdf.github.io/reference/mpdf-functions/sethtmlheader.html

// swap header and footer for pdf based on company chosen
if ($mission['prestataire_id'] == 1) { // opsearch
    // $header = '<div style="text-align:center; padding-bottom:10px; font-size:10px; font-weight:normal; color:#000; border-bottom:1px solid #CCC; height:20px;"><img src="'.IMAGE_URL.'/pdf/opsearch.png" style="width:300px;height:49px;"></div>';
    $header = '<div style="text-align:center; padding-bottom:10px; font-size:10px; font-weight:normal; color:#000; border-bottom:1px solid #CCC; height:20px;"><img src="../asset/images/pdf/opsearch.png" style="width:300px;height:49px;"></div>';
    $mpdf->SetAuthor('OPSEARCH');
    $mpdf->SetCreator('OPSEARCH');
} elseif ($mission['prestataire_id'] == 2) {
    $header = '<div style="text-align:center; padding-bottom:10px; font-size:10px; font-weight:normal; color:#000; border-bottom:1px solid #CCC; height:20px;"><img src="../asset/images/pdf/headhunting.png" style="width:180px;height:40px;"></div>';
    $mpdf->SetAuthor('HEADHUNTING FACTORY');
    $mpdf->SetCreator('HEADHUNTING FACTORY');
}


$mpdf->SetHTMLHeader($header);

// footer
$mpdf->SetHTMLFooter('
<table width="100%">
    <tr>
        <td width="33%">{DATE j-m-Y}</td>
        <td width="33%" align="center">{PAGENO}/{nbpg}</td>
        <td width="33%" style="text-align: right;">TDC MAP</td>
    </tr>
</table>');

//$bg_url = BASE_URL . "/asset/images/ui/bg/map-bg.svg";
////$mpdf->SetDefaultBodyCSS('background-image', "url(".$bg_url.")");
////$mpdf->SetDefaultBodyCSS('background-image-resize', 6);
//
//$mpdf->SetWatermarkImage($bg_url);
//$mpdf->showWatermarkImage = true;

// map html

$map = '';

// map legende
$map .= '<div style="text-align: center; margin-bottom:20px;">';
    $map .= '<table style="border-collapse:collapse; width:100%;text-align:center;font-weight:bold;"><tr>';
    foreach (Map::$departments as $dept_code => $dept) :
        if (array_key_exists($dept_code, $candidats_par_departments)) {
            /// if ($dept_code == 75 || $dept_code == 92 || $dept_code == 93 || $dept_code == 94):
            if (in_array($dept_code, $grouped_regions_dept)):
                if (array_key_exists($dept_code, $bg_colors)) :
                    $bgcolor = "background-color: $bg_colors[$dept_code];";
                else:
                    $bgcolor = "background-color:#80C7E3;";
                endif;
                $map .= '<td align="center" style="' .$bgcolor .';line-height: 20px; padding: 5px 10px; color: #fff; width:100px;text-align=center">';
                    $map .= $dept['name'] .' - '. $candidats_par_departments[$dept_code]['nombre'];
                $map .= '</td>';
            endif;
        }
    endforeach;
    
    if ($num_candidates_in_ile_de_france > 0) {
        $map .= '<td align="center" style="' .$bgcolor .';line-height: 20px; padding: 5px 10px; color: #fff; width:100px;text-align=center">';
            $map .= 'Ile de France - '. $num_candidates_in_ile_de_france;
        $map .= '</td>';
    }
    
    $map .= "</tr></table>";
$map .= '</div>';

$map .= '<div style=margin-left:150px;><svg xmlns:mapsvg="http://mapsvg.com" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" mapsvg:geoViewBox="-5.181900 51.089515 9.560929 41.366975" width="612.3844" height="584.36005">';


foreach (Map::$departments as $dept_code => $dept) :
    $map .= '<path';
        $map .= ' d="'. $dept['path'] .'"';
        if (array_key_exists($dept_code, $bg_colors)) :
            $map .= ' fill="'. $bg_colors[$dept_code] .'"';
        else:
            $map .= ' fill="#80C7E3"'; 
        endif;
        $map .= ' fill-opacity="0.8"';
        if (in_array($dept_code, $grouped_regions_dept)):
            $map .= ' stroke="">';
        else:
            $map .= ' stroke="black">';
        endif;
        $map .= '</path>';
endforeach;

if ($sum_grouped_dept > 0) { // must create a rectangle to display the counter of the grouped dept
    
    $map .= '<rect x="304.629000" y="138.902000" width="'. rectw($sum_grouped_dept) .'" height="11" fill="white" fill-opacity="0.5"></rect>';
    $map .= '<text x="307.629000" y="148.902000" font-size="9pt" font-family="Roboto" fill="black">'.$sum_grouped_dept.'</text>';
    
}


foreach (Map::$departments as $dept_code => $dept) :
    if (array_key_exists($dept_code, $candidats_par_departments)) {
        ///if ($dept_code == 75 || $dept_code == 92 || $dept_code == 93 || $dept_code == 94):
        if (in_array($dept_code, $grouped_regions_dept)):
            // do nothing
        else:
            $map .= '<rect x="'. $dept['rx'] . '" y="'. $dept['ry'] . '" width="'. rectw($candidats_par_departments[$dept_code]['nombre']) . '" height="11" fill="white" fill-opacity="0.5"></rect>';
        endif;
        
    }
endforeach;

foreach (Map::$departments as $dept_code => $dept) :
    if (array_key_exists($dept_code, $candidats_par_departments)) {
        ///if ($dept_code == 75 || $dept_code == 92 || $dept_code == 93 || $dept_code == 94):
        if (in_array($dept_code, $grouped_regions_dept)):
            // do nothing
        else:
            $map .= '<text x="'. $dept['x'] .'" y="'. $dept['y'] .'" font-size="9pt" font-family="Roboto" fill="black">' .$candidats_par_departments[$dept_code]['nombre'] . '</text>'; 
        endif;     
    }
endforeach;

$map .=  '</svg></div>';

// table summary
$map .= '<div style="margin-top:100px;width:100%; float:left;"><table style="border-collapse:collapse; border:1px solid #CCC;border-bottom:none;width:100%;"><thead><tr><th style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;background-color:#D2B9A1;color:#fff;">Region</th><th style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;background-color:#D2B9A1;color:#fff;">Nombre</th><th style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;font-weight:bold;background-color:#D2B9A1;color:#fff;">%</th></tr></thead><tbody>';

// must recalculate the % for each as we added ile de france manually 
$total = 0;
foreach($candidats_par_departments as $department) :

    $total += $department['nombre'];

endforeach;
// add the ile de france
$total += $num_candidates_in_ile_de_france;

foreach ($candidats_par_departments as $department) :
    $map .= '<tr><td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">'.$department['name'].'</td><td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">'.$department['nombre'].'</td><td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">'.round(100*($department['nombre'] / $total), 2).' %</td></tr>';
endforeach;

// append ile de france in table
if ($num_candidates_in_ile_de_france > 0) {
    $map .= '<tr><td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">Ile de France</td><td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">'.$num_candidates_in_ile_de_france.'</td><td style="padding:5px;border-right:1px solid #CCC;border-bottom:1px solid #CCC;">'.round(100*($num_candidates_in_ile_de_france / $total), 2).' %</td></tr>';
}

$map .= '</tbody></table></div>';

$mpdf->WriteHTML($map);


// generate the pdf
$title = 'TDC_map_' . $mission['id'] . '_' .  str_replace(' ', '-', filterChar($mission['nom_client'])) . '_' . str_replace(' ', '-', filterChar($mission['poste']));
$file_name =  $title . '.pdf';

$file = PDF_URL . '/' . $file_name;

// save the pdf file
// http://www.fpdf.org/en/doc/output.htm
$mpdf->Output($file); // save to a local file with the name given by name (may include a path).

$response = array(
    'status' => 'OK',
    'file' => $file_name,
);

echo json_encode($response);
exit();
?>