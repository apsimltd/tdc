<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isGet() || getPost()) {
    
    if (get('op') == 'select') {
        $visibilite_client = 1;
    } elseif (get('op') == 'deselect') {
        $visibilite_client = 0;
    }
    
    $data = array(
        'visibilite_client' => $visibilite_client,
        'modified' => time(),
        'mission_id' => getPost('mission_id'),
        'candidat_id' => getPost('candidat_id'),
    );
    
    if (TDC::updateVisibiliteCandidatInTDC(get('tdc_id'), $data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Candidat Visibilite Client mis à jour avec succès',
            'type' => 'success',
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>