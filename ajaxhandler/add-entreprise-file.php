<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
        
    $files = $_FILES['file']; // array
     
    Candidat::addDocumentsEntreprise(getPost('entreprise_id'), $files);
    
    $notif = array(
        'status' => 'OK',
        'msg' => 'Document ajouté avec succès',
        'type' => 'success', 
    );
    setNotifCookie($notif);
    
    if (getPost('mission_id') && getPost('mission_id') != "") {
        header('Location:' . BASE_URL . '/editer-entreprise?id='.getPost('entreprise_id') . '&tdc_id=' . getPost('mission_id'));
    } else {
        header('Location:' . BASE_URL . '/editer-entreprise?id='.getPost('entreprise_id'));
    }
    
} else {
    header('Location:' . BASE_URL . '/entreprises');
}
?>