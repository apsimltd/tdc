<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isGet()) {
    
    if (get('mission_id') && get('mission_id') != "" && get('candidat_id') && get('candidat_id') != "") {
        
        // first of all we check if the candidate was already added and removed 
        // when removed the status is 0
        if (!TDC::isCandidatExistInTDC(get('mission_id'), get('candidat_id'))) {
            // add the candidat to tdc
            $tdc = array(
                'mission_id' => get('mission_id'),
                'candidat_id' => get('candidat_id'),
                'user_id' => $me['id'],
                'created' => time(),
            );
            
            $tdc_id = TDC::addCandidat($tdc);
            
            /// if (TDC::addCandidat($tdc)) {
            if ($tdc_id > 0) {
                
                if (get('blacklist') && get('blacklist') == 1 && get('nom')) {
                    $notif = array(
                        'status' => 'NOK',
                        'msg' => '<strong>Attention :</strong> La Société ' . get('nom') . ' est blacklistée',
                        'type' => 'warning', 
                    );
                    setNotifCookie($notif);
                }

                header('Location:' . BASE_URL . '/tdc?id=' . get('mission_id') . '&tdc_id=' . $tdc_id);
            }
        } else {
            // set the existing candidate status to 1
            $tdc_id = TDC::updateCandidatStatusInTDC(get('mission_id'), get('candidat_id'));
            header('Location:' . BASE_URL . '/tdc?id=' . get('mission_id') . '&tdc_id=' . $tdc_id);
        }        
    }
    
} else {
    header('Location:' . BASE_URL . '/candidats');
}

?>