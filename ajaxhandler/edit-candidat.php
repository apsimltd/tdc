<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
//    debug($_FILES);
//    debug(getPost(), true);
       
    $candidat = array(
        'id' => getPost('id'),
        'control_civilite_id' => getPost('control_civilite_id'),
        'control_localisation_id' => (getPost('control_localisation_id') == "")? 0 : getPost('control_localisation_id'),
        'nom' => mb_strtoupper(getPost('nom')),
        'prenom' => getPost('prenom'),
        'tel_standard' => getPost('tel_standard'),
        'tel_ligne_direct' => getPost('tel_ligne_direct'),
        'tel_pro' => getPost('tel_pro'),
        'tel_mobile_perso' => getPost('tel_mobile_perso'),
        'tel_domicile' => getPost('tel_domicile'),
        'email' => getPost('email'),
        'email2' => getPost('email2'),
        'dateOfBirth' => (getPost('dateOfBirth') == "")? 0 : strtotime(getPost('dateOfBirth')),
        'societe_actuelle' => mb_strtoupper(filterCompanyName(rtrim(ltrim(getPost('societe_actuelle'))))),
        'societe_actuelle_old' => mb_strtoupper(filterCompanyName(rtrim(ltrim(getPost('societe_actuelle_old'))))),
        'poste' => getPost('poste'),
        'control_secteur_id' => (getPost('control_secteur_id') == "")? 0 : getPost('control_secteur_id'),
        'control_fonction_id' => (getPost('control_fonction_id') == "")? 0 : getPost('control_fonction_id'),
        'remuneration' => (getPost('remuneration') == "")? 0 : getPost('remuneration'),
        'detail_remuneration' => getPost('detail_remuneration'),
        'control_niveauFormation_id' => (getPost('control_niveauFormation_id') == "")? 0 : getPost('control_niveauFormation_id'),
        'diplome_specialisation' => getPost('diplome_specialisation'),
        'dateDiplome' => (getPost('dateDiplome') == "")? 0 : strtotime(getPost('dateDiplome')),
        'cv_fourni' => getPost('cv_fourni'),
        'tres_bon_candidat' => getPost('tres_bon_candidat'),
        'is_not_in_this_company' => getPost('is_not_in_this_company'),
        'control_source_id' => (getPost('control_source_id') == "")? 0 : getPost('control_source_id'),
        'modified' => time(),
//        'salaire_package' => getPost('salaire_package'),
        'salaire_fixe' => (getPost('salaire_fixe') == "")? null : getPost('salaire_fixe'),
//        'sur_combien_de_mois' => getPost('sur_combien_de_mois'),
        'avantages' => getPost('avantages'),
        'remuneration_souhaitee' => getPost('remuneration_souhaitee'),
        'mission_id' => getPost('mission_id'), // uses to send mail to client to notify that the candidat has been modified in the tdc accordingly
        'tdc_id' => getPost('tdc_id'), // uses to send mail to client to notify that the candidat has been modified in the tdc accordingly
    );
    
    if (getPost('gdpr_status')) {
        $candidat['gdpr_status'] = getPost('gdpr_status');
    }
    
    $langues = getPost('langues'); // array of control_langue_id
     
    if (Candidat::editCandidat($candidat, $langues)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Candidat modifié avec succès',
            'type' => 'success',
        );
        
//        if (getPost('mission_id') && getPost('mission_id') != "") {
//            $response['callback'] = 'gotopage';
//            $response['param'] = array(
//                'page' => 'tdc',
//                'querystring' => 'id',
//                'value' => getPost('mission_id'),
//            );
//        }
                
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>