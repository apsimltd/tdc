<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_manager_kpi')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php if (isPost()) : ?>
    
    <?php
    $filters = array(
        'start_date' => getPost('start_date'),
        'end_date' => getPost('end_date'),
        'manager_id' => getPost('manager_id'),
    );
        
    if (getPost('mission_id')) {
        $filters['mission_id'] = getPost('mission_id');
    }
    
    if (getPost('cdr_id')) {
        $filters['cdr_id'] = getPost('cdr_id');
    }
    
    $status = array();
    
    if (getPost('mission_status_232')) {
        $status[] = 232;
    }
    
    if (getPost('mission_status_230')) {
        $status[] = 230;
    }
    
    if (getPost('mission_status_231')) {
        $status[] = 231;
    }
    
    if (getPost('mission_status_233')) {
        $status[] = 233;
    }
    
    if (getPost('mission_status_235')) {
        $status[] = 235;
    }
    
    if (getPost('mission_status_250')) {
        $status[] = 250;
    }
    
    $filters['status'] = addSeperator($status, ',');

    ?>

                <div class="col col-12">
                    <div class="block nopadding">

                        <div class="block-head with-border">
                            <header><i class="os-icon os-icon-bar-chart-stats-up"></i> <span>Taux de transformation moyen de l'équipe soit les IN / (IN + ex BU)</span> <span class="glyphicon glyphicon-info-sign tooltips" title="Notez que ce KPI est composé de toutes les missions dans la base de données"></span></header>
                        </div>

                        <div class="block-body">
                                                        
                            <div class="nano" style="height:500px;">
                            	<div class="nano-content">
                                    
                                    <?php $rows = KPI::kpi_manager_new_1($filters); // debug($rows, true); ?>
                                    <?php $statut_tdc = Control::getControlListByType(8); // debug($statut_tdc); ?>
                                    
                                    <table class="table-list compact sticky-table-header">
                                        <thead>
                                            <tr>
                                                <th>Mission</th>
                                                <th>Poste - Client - Manager</th>
                                                <th style="width: 100px;">ARC</th>
                                                <th style="width: 100px;">ARCA</th>
                                                <th style="width: 100px;">BU+</th>
                                                <th style="width: 100px;">BU-</th>
                                                <th style="width: 100px;">Ex BU</th>
                                                <th style="width: 100px;">IN</th>
                                                <th style="width: 350px;">Ratio = (IN / (IN + Ex BU)) x 100</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $in_sum = 0; $arc_sum = 0; $arca_sum = 0; $bu_plus_sum = 0; $bu_minus_sum = 0; $exbu_sum = 0; $sum_percentage = 0; ?>
                                        <?php foreach($rows as $row): ?>
                                            <?php $in = 0; $arc = 0; $arca = 0; $bu_plus = 0; $bu_minus = 0; $exbu = 0; ?>
                                            <tr>
                                                <td align="center">
                                                    <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $row['id'] ?>" target="_blank">
                                                        <?php echo $row['id'] ?>
                                                    </a>
                                                </td>
                                                <td><?php echo '<strong>' . $row['poste'] . '</strong> - ' . $row['client'] . ' - ' . $row['manager_name'] ?></td>
                                                <td align="center">
                                                    <?php // ARC id = 251 ?>
                                                    <?php if (isset($row['num_candidat_by_status'][251])): ?>
                                                        <?php echo $row['num_candidat_by_status'][251] ?>
                                                        <?php $arc += $row['num_candidat_by_status'][251] ?>
                                                        <?php $arc_sum += $row['num_candidat_by_status'][251] ?>
                                                    <?php else: ?>
                                                        <?php echo '0'; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td align="center">
                                                    <?php // ARCA id = 252 ?>
                                                    <?php if (isset($row['num_candidat_by_status'][252])): ?>
                                                        <?php echo $row['num_candidat_by_status'][252] ?>
                                                        <?php $arca += $row['num_candidat_by_status'][252] ?>
                                                        <?php $arca_sum += $row['num_candidat_by_status'][252] ?>
                                                    <?php else: ?>
                                                        <?php echo '0'; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td align="center">
                                                    <?php // BU+ id = 228 ?>
                                                    <?php if (isset($row['num_candidat_by_status'][228])): ?>
                                                        <?php echo $row['num_candidat_by_status'][228] ?>
                                                        <?php $bu_plus += $row['num_candidat_by_status'][228] ?>
                                                        <?php $bu_plus_sum += $row['num_candidat_by_status'][228] ?>
                                                    <?php else: ?>
                                                        <?php echo '0'; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td align="center">
                                                    <?php // BU- id = 227 ?>
                                                    <?php if (isset($row['num_candidat_by_status'][227])): ?>
                                                        <?php echo $row['num_candidat_by_status'][227] ?>
                                                        <?php $bu_minus += $row['num_candidat_by_status'][227] ?>
                                                        <?php $bu_minus_sum += $row['num_candidat_by_status'][227] ?>
                                                    <?php else: ?>
                                                        <?php echo '0'; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td align="center">
                                                    <?php // Ex BU id = 259 ?>
                                                    <?php if (isset($row['num_candidat_by_status'][259])): ?>
                                                        <?php echo $row['num_candidat_by_status'][259] ?>
                                                        <?php $exbu += $row['num_candidat_by_status'][259] ?>
                                                        <?php $exbu_sum += $row['num_candidat_by_status'][259] ?>
                                                    <?php else: ?>
                                                        <?php echo '0'; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td align="center">
                                                    <?php // IN id = 229 ?>
                                                    <?php if (isset($row['num_candidat_by_status'][229])): ?>
                                                        <?php echo $row['num_candidat_by_status'][229] ?>
                                                        <?php $in += $row['num_candidat_by_status'][229] ?>
                                                        <?php $in_sum += $row['num_candidat_by_status'][229] ?>
                                                    <?php else: ?>
                                                        <?php echo '0'; ?>
                                                    <?php endif; ?>
                                                </td>
                                                
                                                <td align="center">
                                                    <?php 
                                                    // Ratio = (IN / (IN + Ex BU)) x 100
                                                    echo '(' . $in . ' / (' . $in . ' + ' . $exbu . ')) x 100 = ';
                                                    $sum = $in + $exbu;
                                                    
                                                    echo "<strong>";
                                                    if ($in > 0 && $sum > 0) {
                                                        
                                                        echo (round(($in/$sum), 3))*100 . ' %';
                                                        $sum_percentage += (round(($in/$sum), 3))*100;
                                                    } else {
                                                        echo '0 %';
                                                        $sum_percentage += 0;
                                                    }
                                                    echo "</strong>";
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                            <tr class="total" style="background-color: #2196f3; color: #fff;">
                                                <td align="center"><strong>Total</strong></td>
                                                <td><strong>Missions = <?php echo count($rows); ?></strong></td>
                                                <td align="center"><strong>ARC = <?php echo $arc_sum ?></strong></td>
                                                <td align="center"><strong>ARCA = <?php echo $arca_sum ?></strong></td>
                                                <td align="center"><strong>BU+ = <?php echo $bu_plus_sum ?></strong></td>
                                                <td align="center"><strong>BU- = <?php echo $bu_minus_sum ?></strong></td>
                                                <td align="center"><strong>Ex BU = <?php echo $exbu_sum ?></strong></td>
                                                <td align="center"><strong>In = <?php echo $in_sum ?></strong></td>
                                                <td align="center"><strong>Ratio Total = <?php echo $sum_percentage . ' %'; ?></strong></td>
                                            </tr>
                                            <tr class="total" style="background-color: #92D050;color: #fff;">
                                                <td align="right" colspan="8"><strong>Ratio Total Moyenne = ( Ratio Total / Total Missions ) = </strong></td>
                                                <td align="center">
                                                    <strong>
                                                        <?php
                                                        if ($sum_percentage > 0 && count($rows) > 0) {
                                                            echo '(' . $sum_percentage . ' / ' . count($rows) . ') = ';
                                                            echo round($sum_percentage/count($rows)) . ' %';
                                                        } else {
                                                            echo '0';
                                                        }
                                                        ?> 
                                                    </strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                </div><!-- /nano-content --> 
                            </div><!-- /nano -->
                                
                        </div><!-- / block-body -->

                    </div>
                </div><!-- /col -->

                <div class="col col-12">
                    <div class="block nopadding">

                        <div class="block-head with-border">
                            <header><i class="os-icon os-icon-bar-chart-stats-up"></i><span> Le temps nécessaire pour la finalisation d'une SL de l'équipe </span></header>
                        </div>

                        <div class="block-body">
                                                                                    
                            <div class="nano" style="height:500px; margin-bottom: 30px;">
                            	<div class="nano-content">
                                    
                                    <?php $SLs = KPI::kpi_manager_SL($filters); // debug($SLs); ?>
                                    
                                    <table class="table-list compact sticky-table-header">
                                        <thead>
                                            <tr>
                                                <th align="center">#</th>
                                                <th style="width: 135px;">Mission</th>
                                                <th>Poste</th>
                                                <th>Manager</th>
                                                <th align="center">Date Début</th>
                                                <th align="center">Date Fin</th>
                                                <th align="center" style="width: 200px;">Nombre Candidats</th>
                                                <th align="center" style="width: 200px;">Date SL <span class="glyphicon glyphicon-info-sign tooltips" title="Date de la short list (date ou le 3ème candidat est short listé)"></span></th>
                                                <th align="center">Temps SL <span class="glyphicon glyphicon-info-sign tooltips" title="Temps pour réaliser la SL depuis date de début"></span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $sum_candidates = 0; $sum_time_sl = 0; $total_sl_in_time = 0; $total_sl_not_in_time = 0; $total_sl_presque_in_time = 0; ?>
                                        <?php $count = 1; $mission_count_status = 0; ?>
                                        <?php foreach($SLs as $row): ?>
                                            <tr>
                                                <td align="center"><?php echo $count; ?></td>
                                                <td align="center">
                                                    <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $row['mission_id'] ?>" target="_blank">
                                                        <?php echo $row['mission_id'] ?>
                                                    </a>
                                                </td>
                                                <td><?php echo $row['poste'] ?></td>
                                                <td><?php echo $row['manager_name'] ?></td>
                                                <td align="center"><?php echo date("Y-m-d", strtotime($row['mission_date_debut'])) ?></td>
                                                <td align="center"><?php echo date("Y-m-d", strtotime($row['mission_date_fin'])) ?></td>
                                                <td align="center"><?php echo $row['num_shortliste'] ?></td>
                                                                                                
                                                <?php 
                                                // calculate all backgrounds and state
                                                // if shortliste_added <= mission date fin => green
                                                // if shortliste_added > mission date fin and shortliste_added < mission date fin + 30 days => orange
                                                // if shortliste_added > misison date fin + 30 days => red
                                                $mission_date_fin = strtotime($row['mission_date_fin']);
                                                $mission_date_debut = strtotime($row['mission_date_debut']);
                                                $mission_date_fin_30_days = strtotime($row['mission_date_fin']) + (60*60*24*30);
                                                $date_SL_added = strtotime($row['candidats'][2]['shortliste_added']);
                                                $text = "";
                                                if ($date_SL_added <= $mission_date_fin) {
                                                    $total_sl_in_time++;
                                                    $bg = "#92D050";
                                                    $sum_candidates += $row['num_shortliste'];
                                                    $sum_time_sl += $row['candidats'][2]['t_shortliste_from_start_mission'];
                                                    // handle anomalies if date SL is before date start mission in case mission start date has changed
                                                    if ($date_SL_added < $mission_date_debut) {
                                                        $bg = "#2196f3";
                                                        $text = "Erreur Date Mission";
                                                        $mission_count_status++;
                                                        $total_sl_in_time--;
                                                        $sum_candidates -= $row['num_shortliste'];
                                                        $sum_time_sl -= $row['candidats'][2]['t_shortliste_from_start_mission'];
                                                    } 
                                                } elseif ($date_SL_added > $mission_date_fin && $date_SL_added < $mission_date_fin_30_days) {
                                                    $total_sl_presque_in_time++;
                                                    $bg = "#FFC000";
                                                    $sum_candidates += $row['num_shortliste'];
                                                    $sum_time_sl += $row['candidats'][2]['t_shortliste_from_start_mission'];
                                                    // handle anomalies if date SL is before date start mission in case mission start date has changed
                                                    if ($date_SL_added < $mission_date_debut) {
                                                        $bg = "#FFC000";
                                                        $text = "Erreur Date Mission";
                                                        $mission_count_status++;
                                                        $total_sl_presque_in_time--;
                                                        $sum_candidates -= $row['num_shortliste'];
                                                        $sum_time_sl -= $row['candidats'][2]['t_shortliste_from_start_mission'];
                                                    } 
                                                } elseif ($date_SL_added > $mission_date_fin && $date_SL_added > $mission_date_fin_30_days) {
                                                    $total_sl_not_in_time++;
                                                    $bg = "#FF0000";
                                                    $sum_candidates += $row['num_shortliste'];
                                                    $sum_time_sl += $row['candidats'][2]['t_shortliste_from_start_mission'];
                                                    // handle anomalies if date SL is before date start mission in case mission start date has changed
                                                    if ($date_SL_added < $mission_date_debut) {
                                                        $bg = "#FF0000";
                                                        $text = "Erreur Date Mission";
                                                        $mission_count_status++;
                                                        $total_sl_not_in_time--;
                                                        $sum_candidates -= $row['num_shortliste'];
                                                        $sum_time_sl -= $row['candidats'][2]['t_shortliste_from_start_mission'];
                                                    } 
                                                }

                                                ?>
                                                
                                                <td align="center"><?php echo $row['candidats'][2]['shortliste_added'] ?></td>
                                                <td align="center" style="background-color: <?php echo $bg ?>; color: #fff;">
                                                    <strong>
                                                        <?php if ($text <> ""): ?>
                                                            <?php echo $text ?>
                                                        <?php else: ?>
                                                            <?php echo seconds_to_days($row['candidats'][2]['t_shortliste_from_start_mission']) ?>
                                                        <?php endif; ?>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr style="border-bottom: 2px solid #000;">
                                                <td colspan="6"></td>
                                                <td colspan="2" style="padding: 0px;">
                                                    <table class="inner-table">
                                                        <thead>
                                                            <th style="width: 200px;">Candidat</th>
                                                            <th align="center" style="border-right: none;width: 200px;">Date SL</th>
                                                        </thead>
                                                        <tbody>
                                                            <?php $candidat_count = 1; ?>
                                                            <?php foreach($row['candidats'] as $cand): ?>
                                                            <tr>
                                                                <td>
                                                                    <a class="link_to_tdc" href="<?php echo BASE_URL ?>/tdc?id=<?php echo $row['mission_id'] ?>&tdc_id=<?php echo $cand['tdcsuivi_id'] ?>" target="_blank">
                                                                        <?php echo mb_strtoupper($cand['nom']) . ' ' . mb_ucfirst($cand['prenom']) ?>
                                                                    </a>
                                                                </td>
                                                                <td align="center" style="border-right: none;<?php if ($candidat_count == 3): ?>background-color:<?php echo $bg ?> ;font-weight:bold;color:#fff;<?php endif; ?>">
                                                                    <?php echo $cand['shortliste_added'] ?>
                                                                </td>
                                                            </tr>
                                                            <?php $candidat_count++; ?>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <?php $count++; ?>
                                        <?php endforeach; ?>
                                            <tr class="total" style="background-color: #2196f3; color: #fff;">
                                                <td colspan="6" align="right"><strong>Moyennes</strong></td>
                                                <td align="center" style="width: 200px;">
                                                    <strong>
                                                        (<?php echo $sum_candidates ?> / (<?php echo count($SLs) ?> - <?php echo $mission_count_status ?>)) = <?php if (count($SLs) > 0): echo round($sum_candidates/(count($SLs) - $mission_count_status)); else: echo '0'; endif; ?>
                                                    </strong>
                                                </td>
                                                <td align="center" style="width: 200px;"></td>
                                                <td align="center">
                                                    <strong>
                                                        (<?php echo seconds_to_days($sum_time_sl) ?> / (<?php echo count($SLs) ?> - <?php echo $mission_count_status ?>) =  <?php if (count($SLs) > 0 && $sum_time_sl > 0): echo seconds_to_days(round($sum_time_sl/(count($SLs) - $mission_count_status))); else: echo '0'; endif; ?>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr class="total" style="background-color: #ff9800;color: #fff;">
                                                <td colspan="6" align="right"><strong>Totaux</strong></td>
                                                <td align="center" style="width: 200px;"><strong>Nombres Mission = <?php echo (count($SLs) - $mission_count_status) ?><strong></td>
                                                <td align="center" style="width: 200px;"><strong>Ratios<strong></td>
                                                <td align="center"></td>
                                            </tr>
                                            <tr class="total" style="background-color: #92D050;color: #fff;">
                                                <td colspan="6" align="right"><strong>Total SL dans les temps</strong></td>
                                                <td align="center" style="width: 200px;"><strong><?php echo $total_sl_in_time ?><strong></td>
                                                <td align="center" style="width: 200px;"><strong>(<?php echo $total_sl_in_time ?> / (<?php echo count($SLs) ?> - <?php echo $mission_count_status ?>)) x 100 = <?php if (count($SLs) > 0 && $total_sl_in_time > 0): echo round((($total_sl_in_time/(count($SLs) - $mission_count_status))*100), 2); else: echo '0'; endif; ?> %<strong></td>
                                                <td align="center"></td>
                                            </tr>
                                            <tr class="total" style="background-color: #FFC000;color: #fff;">
                                                <td colspan="6" align="right"><strong>Total SL presque dans les temps</strong></td>
                                                <td align="center" style="width: 200px;"><strong><?php echo $total_sl_presque_in_time ?><strong></td>
                                                <td align="center" style="width: 200px;"><strong>(<?php echo $total_sl_presque_in_time ?> / (<?php echo count($SLs) ?> - <?php echo $mission_count_status ?>)) x 100 = <?php if (count($SLs) > 0 && $total_sl_presque_in_time > 0): echo round((($total_sl_presque_in_time/(count($SLs) - $mission_count_status))*100), 2); else: echo '0'; endif; ?> %<strong></td>
                                                <td align="center"></td>
                                            </tr>
                                            <tr class="total" style="background-color: #FF0000;color: #fff;">
                                                <td colspan="6" align="right"><strong>Total SL pas dans les temps</strong></td>
                                                <td align="center" style="width: 200px;"><strong><?php echo $total_sl_not_in_time ?><strong></td>
                                                <td align="center" style="width: 200px;"><strong>(<?php echo $total_sl_not_in_time ?> / (<?php echo count($SLs) ?> - <?php echo $mission_count_status ?>)) x 100 = <?php if (count($SLs) > 0 && $total_sl_not_in_time > 0): echo round((($total_sl_not_in_time/(count($SLs) - $mission_count_status))*100), 2); else : echo '0'; endif; ?> %<strong></td>
                                                <td align="center"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                </div><!-- /nano-content --> 
                            </div><!-- /nano -->
                            
                        </div><!-- / block-body -->

                    </div>
                </div><!-- /col -->
                                                                
                <div class="col col-12">
                    <div class="block nopadding">

                        <div class="block-head with-border">
                            <header><i class="os-icon os-icon-bar-chart-stats-up"></i><span>Durée moyenne des missions de l'équipe</span></header>
                        </div>

                        <div class="block-body">
                            
                            <div class="nano" style="height:500px;">
                            	<div class="nano-content">
                                    
                                    <?php $rows = KPI::kpi_manager_4_new($filters); // debug($rows); ?>
                                    
                                    <table class="table-list compact sticky-table-header">
                                        <thead>
                                            <tr>
                                                <th>Mission</th>
                                                <th>Date Début - Date Fin</th>
                                                <th>Poste - Manager </th>
                                                <th>Utilisateur - Role</th>
                                                <th>Statut Avant</th>
                                                <th>Statut Après</th>
                                                <th>Date Changement Statut</th>
                                                <th align="center">Durée mission (Date changement statut - Date Debut Mission)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $sum_duree_mission = 0; ?>
                                        <?php foreach($rows as $row): ?>
                                            <tr>
                                                <td align="center">
                                                    <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $row['mission_id'] ?>" target="_blank">
                                                        <?php echo $row['mission_id'] ?>
                                                    </a>
                                                </td>
                                                <td><?php echo date('Y-m-d', $row['date_debut']) . ' - ' . date('Y-m-d', $row['date_fin']) ?></td>
                                                <td><?php echo '<strong>' . $row['poste'] . '</strong> - ' . $row['manager_name'] ?></td>
                                                <td><?php echo $row['user_name'] . ' - ' . $row['user_role_name'] ?></td>
                                                <td><?php echo $row['status_before'] ?></td>
                                                <td><?php echo $row['status_after'] ?></td>
                                                <td><?php echo $row['date_changement_statut_mission'] ?></td>
                                                <td align="center">
                                                    <?php $sum_duree_mission += (strtotime($row['date_changement_statut_mission']) - $row['date_debut']); ?>
                                                    <strong><?php echo seconds_to_hours_minutes_f(strtotime($row['date_changement_statut_mission']) - $row['date_debut']) ?></strong>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                            <tr class="total" style="background-color: #2196f3; color: #fff;">
                                                <td colspan="2"><strong>Total</strong></td>
                                                <td><strong>Total Missions = <?php echo count($rows) ?></strong></td>
                                                <td colspan="4" align="right"><strong>Total Durée Missions</strong></td>
                                                <td align="center"><strong><?php echo seconds_to_hours_minutes_f($sum_duree_mission) ?></strong></td>
                                            </tr>
                                            <tr class="total" style="background-color: #92D050;color: #fff;">
                                                <td colspan="7" align="right">
                                                    <strong>
                                                        La durée moyenne des missions effectuées = (Total Durée Missions / Total Missions) =
                                                    </strong>
                                                </td>
                                                <td align="center">
                                                    <strong>
                                                        <?php if (count($rows) > 0 && $sum_duree_mission > 0): ?>
                                                            <?php echo seconds_to_hours_minutes_f(round($sum_duree_mission/count($rows), 0)) ?>
                                                        <?php else: ?>
                                                            0
                                                        <?php endif; ?>
                                                    </strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                </div><!-- /nano-content --> 
                            </div><!-- /nano -->
                            
                        </div><!-- / block-body -->

                    </div>
                </div><!-- /col -->
                
                
                
                <div class="col col-12">
                    <div class="block nopadding">

                        <div class="block-head with-border sticky-table-header">
                            <header><i class="os-icon os-icon-bar-chart-stats-up"></i><span>Pourcentage de chasse réelle effectuée sur les misions</span></header>
                        </div>

                        <div class="block-body">
                                                        
                            <?php $source_status = Control::getControlListByType(3) ?>
                            <?php $sources =  KPI::kpi_manager_6_new($filters); ?>
                            
                            <table class="table-list compact">
                                <thead>
                                    <tr>
                                        <th>Source</th>
                                        <th>Nombre Candidats</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $sum = 0; $sum_no_chasse = 0; $total_chasse = 0;?>
                                <?php foreach($sources as $source): ?>
                                    <tr>
                                        <td>
                                            <?php 
                                            if ($source['control_source_id'] == 0) {
                                                echo "Pas de Source";
                                            } else {
                                                echo ucfirst($source_status[$source['control_source_id']]);
                                            }
                                            ?>
                                        </td>
                                        <td align="center">
                                            <?php 
                                            echo $source['num_candidats'];
                                            $sum += $source['num_candidats'];
                                            if ($source['control_source_id'] <> 97) { // chasse = 97
                                                $sum_no_chasse += $source['num_candidats'];
                                            } else {
                                                $total_chasse = $source['num_candidats'];
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                    <tr class="total" style="background-color: #2196f3; color: #fff;">
                                        <td align="right"><strong>Total</strong></td>
                                        <td align="center"><strong><?php echo $sum; ?></strong></td>
                                    </tr>
                                    <tr class="total" style="background-color: #ff9800; color: #fff;">
                                        <td align="right"><strong>Total Sans Chasse</strong></td>
                                        <td align="center"><strong><?php echo $sum_no_chasse; ?></strong></td>
                                    </tr>
                                    <tr class="total" style="background-color: #FFC000; color: #fff;">
                                        <td align="right"><strong>Total Chasse</strong></td>
                                        <td align="center"><strong><?php echo $total_chasse; ?></strong></td>
                                    </tr>
                                    <tr class="total" style="background-color: #92D050; color: #fff;">
                                        <td align="right"><strong>Ratio = Total Chasse / Total = </strong></td>
                                        <td align="center">
                                            <strong>
                                                <?php 
                                                if ($total_chasse > 0 && $sum > 0) {
                                                    echo '( ' . $total_chasse . ' / ' . $sum . ' ) x 100 = ' . round((($total_chasse/$sum) * 100), 2) . ' %';
                                                } else {
                                                    echo '( ' . $total_chasse . ' / ' . $sum . ' ) x 100 = 0 %';
                                                }
                                                ?>
                                            </strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                                
                        </div><!-- / block-body -->

                    </div>
                </div><!-- /col -->
                
                <?php ## START OF RINGOVER ?>
                <div class="col col-12">
                    <div class="block nopadding">

                        <div class="block-head with-border">
                            <header><i class="os-icon os-icon-bar-chart-stats-up"></i><span>Nombre d'appel moyen du Cdr (RINGOVER)</span></header>
                        </div>

                        <div class="block-body">
                                                        
                            <h3 class="kpi_h3">Total appels depuis le 2021-05-01 sur Ringover</h3>
                                                        
                            <?php $ringovers = Ringover::KPI_MANAGER($filters); // debug($ringovers); ?>
                            
                            <table class="table-list compact sticky-table-header">
                                <thead>
                                    <tr>
                                        <th>Statut Appel</th>
                                        <th>Type Appel</th>
                                        <th>Total Appels</th>
                                        <th>Temps Total Appels</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $sum_num_calls = 0; $sum_duree = 0; $sum_good_calls = 0; $sum_good_calls_duree = 0; ?>
                                    <?php foreach($ringovers as $row): ?>
                                    <tr>
                                        <td>
                                            <?php echo $row['last_state'] ?> - 
                                            <?php
                                            if ($row['last_state'] == "ANSWERED") {
                                                echo "Appel Abouti";
                                                $sum_good_calls += $row['num_calls'];
                                                $sum_good_calls_duree += $row['duree'];
                                            } elseif($row['last_state'] == "MISSED") {
                                                echo "Appel Manqué";
                                            } elseif($row['last_state'] == "VOICEMAIL") {
                                                echo "Appel Messagerie";
                                            } elseif($row['last_state'] == "CANCELLED") {
                                                echo "Appel Annulé";
                                            } elseif($row['last_state'] == "FAILED") {
                                                echo "Appel Non Abouti";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if ($row['direction'] == "in") {
                                                echo "Appel Entrant";
                                            } elseif($row['direction'] == "out") {
                                                echo "Appel Sortant";
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $row['num_calls']; $sum_num_calls += $row['num_calls']; ?></td>
                                        <td><?php echo seconds_to_hours_minutes_f($row['duree']); $sum_duree += $row['duree'] ?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <tr class="total" style="background-color: #2196f3; color: #fff;">
                                        <td align="right" colspan="2"><strong>Total</strong></td>
                                        <td><strong><?php echo $sum_num_calls; ?></strong></td>
                                        <td><strong><?php echo seconds_to_hours_minutes_f($sum_duree); ?></strong></td>
                                    </tr>
                                    <tr class="total" style="background-color: #ff9800; color: #fff;">
                                        <td align="right" colspan="2"><strong>Total Bons Appels (Appels ANSWERED Entrant + Appels ANSWERED Sortant) </strong></td>
                                        <td><strong><?php echo $sum_good_calls; ?></strong></td>
                                        <td><strong><?php echo seconds_to_hours_minutes_f($sum_good_calls_duree); ?></strong></td>
                                    </tr>
                                    <tr class="total" style="background-color: #FFC000; color: #fff;">
                                        <?php $num_days_ringover = round(getBusinessDays("2021-05-01", date("Y-m-d", time()))) ?>
                                        <td align="right" colspan="2"><strong>Moyenne de Total Appels excluant les weekends (Date : 2021-05-01 - <?php echo date("Y-m-d", time()) ?> = <?php echo $num_days_ringover ?> Jours) = Total / Nombres de jours</strong></td>
                                        <td colspan="2"><strong>= <?php echo $sum_num_calls ?> / <?php echo $num_days_ringover ?> = <?php echo round($sum_num_calls/$num_days_ringover) ?></strong></td>
                                    </tr>
                                    <tr class="total" style="background-color: #92D050; color: #fff;">
                                        <td align="right" colspan="2"><strong>Moyenne de Total Bons Appels excluant les weekends (Date : 2021-05-01 - <?php echo date("Y-m-d", time()) ?> = <?php echo $num_days_ringover ?> Jours) = Total Bons Appels / Nombres de jours</strong></td>
                                        <td colspan="2"><strong>= <?php echo $sum_good_calls ?> / <?php echo $num_days_ringover ?> = <?php echo round($sum_good_calls/$num_days_ringover) ?></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <?php ##start of ringover grouped by mission ?>
                            <h3 class="kpi_h3">Appels groupés par mission depuis le 2021-05-01</h3>
                            <p style="color: #090;">Notez que certains appels n'ont pas pu se qualifier pour sa mission car le numéro appelant ne correspond pas au numéro candidat</p>
                            
                            <div class="nano" style="height:500px;">
                            	<div class="nano-content">
                                    
                                    <?php $missions = Ringover::KPI_MANAGER_GROUPED_BY_MISSION($filters); ?>
                                    
                                    <?php $mission_status = Control::getControlListByType(9) ?>
                                    
                                    <table class="table-list compact sticky-table-header">
                                        <thead>
                                            <tr>
                                                <th>Mission</th>
                                                <th>Poste</th>
                                                <th>Manager</th>
                                                <th>Statut</th>
                                                <th>Nombres Appels</th>
                                                <th>Temps Total</th>
                                                <th>Appels Sortant - Temps</th>
                                                <th>Total Appels Sortant - Temps</th>
                                                <th>Appels Entrant - Temps</th>
                                                <th>Total Appels Entrant - Temps</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $sum_total_calls = 0; $sum_total_temps = 0; $sum_total_appel_sortant_answered = 0; $sum_total_appel_sortant_answered_temps = 0; $sum_total_appel_sortant_canceled = 0; $sum_total_appel_sortant_canceled_temps = 0; $sum_total_appel_sortant_failed = 0; $sum_total_appel_sortant_failed_temps = 0; $sum_total_appel_entrant_answered = 0; $sum_total_appel_entrant_answered_temps = 0; $sum_total_appel_sortant_missed = 0; $sum_total_appel_sortant_missed_temps = 0; $sum_total_appel_sortant_voicemail = 0; $sum_total_appel_sortant_voicemail_temps = 0; ?>
                                        <?php foreach($missions as $row): ?>
                                            <tr>
                                                <td align="center">
                                                    <?php if ($row['mission_id'] > 0): ?>
                                                        <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $row['mission_id'] ?>" target="_blank">
                                                            <?php echo $row['mission_id'] ?>
                                                        </a>
                                                    <?php else: ?>
                                                        N/A
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['poste'] ?>
                                                </td>
                                                <td>
                                                    <?php if ($row['manager_name'] <> ""): ?>
                                                        <?php echo $row['manager_name'] ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($row['status'] <> ""): ?>
                                                        <?php echo ucfirst($mission_status[$row['status']]) ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['in']['ANSWERED']['num_calls'] + $row['in']['MISSED']['num_calls'] + $row['in']['VOICEMAIL']['num_calls'] + $row['out']['ANSWERED']['num_calls'] + $row['out']['CANCELLED']['num_calls'] + $row['out']['FAILED']['num_calls'] ?>
                                                    <?php $sum_total_calls += $row['in']['ANSWERED']['num_calls'] + $row['in']['MISSED']['num_calls'] + $row['in']['VOICEMAIL']['num_calls'] + $row['out']['ANSWERED']['num_calls'] + $row['out']['CANCELLED']['num_calls'] + $row['out']['FAILED']['num_calls']; ?>
                                                </td>
                                                <td>
                                                    <?php echo seconds_to_hours_minutes_f($row['in']['ANSWERED']['duree'] + $row['in']['MISSED']['duree'] + $row['in']['VOICEMAIL']['duree'] + $row['out']['ANSWERED']['duree'] + $row['out']['CANCELLED']['duree'] + $row['out']['FAILED']['duree']) ?>
                                                    <?php $sum_total_temps += $row['in']['ANSWERED']['duree'] + $row['in']['MISSED']['duree'] + $row['in']['VOICEMAIL']['duree'] + $row['out']['ANSWERED']['duree'] + $row['out']['CANCELLED']['duree'] + $row['out']['FAILED']['duree']; ?>
                                                </td>
                                                <td style="width: 195px;">
                                                    ANSWERED - <?php echo $row['out']['ANSWERED']['num_calls'] ?> - <?php echo seconds_to_hours_minutes_f($row['out']['ANSWERED']['duree']) ?><br>
                                                    <?php $sum_total_appel_sortant_answered += $row['out']['ANSWERED']['num_calls']; ?>
                                                    <?php $sum_total_appel_sortant_answered_temps += $row['out']['ANSWERED']['duree']; ?>
                                                    CANCELLED - <?php echo $row['out']['CANCELLED']['num_calls'] ?> - <?php echo seconds_to_hours_minutes_f($row['out']['CANCELLED']['duree']) ?><br>
                                                    <?php $sum_total_appel_sortant_canceled += $row['out']['CANCELLED']['num_calls']; ?>
                                                    <?php $sum_total_appel_sortant_canceled_temps += $row['out']['CANCELLED']['duree']; ?>
                                                    FAILED - <?php echo $row['out']['FAILED']['num_calls'] ?> - <?php echo seconds_to_hours_minutes_f($row['out']['FAILED']['duree']) ?><br>
                                                    <?php $sum_total_appel_sortant_failed += $row['out']['FAILED']['num_calls'] ?>
                                                    <?php $sum_total_appel_sortant_failed_temps += $row['out']['FAILED']['duree'] ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['out']['ANSWERED']['num_calls'] + $row['out']['CANCELLED']['num_calls'] + + $row['out']['FAILED']['num_calls'] ?> -  <?php echo seconds_to_hours_minutes_f($row['out']['ANSWERED']['duree'] + $row['out']['CANCELLED']['duree'] + + $row['out']['FAILED']['duree']) ?>
                                                </td>
                                                <td style="width: 195px;">
                                                    ANSWERED - <?php echo $row['in']['ANSWERED']['num_calls'] ?> - <?php echo seconds_to_hours_minutes_f($row['in']['ANSWERED']['duree']) ?><br>
                                                    <?php $sum_total_appel_entrant_answered += $row['in']['ANSWERED']['num_calls'] ?>
                                                    <?php $sum_total_appel_entrant_answered_temps += $row['in']['ANSWERED']['duree'] ?>
                                                    MISSED - <?php echo $row['in']['MISSED']['num_calls'] ?> - <?php echo seconds_to_hours_minutes_f($row['in']['MISSED']['duree']) ?><br>
                                                    <?php $sum_total_appel_sortant_missed += $row['in']['MISSED']['num_calls'] ?>
                                                    <?php $sum_total_appel_sortant_missed_temps += $row['in']['MISSED']['duree'] ?>
                                                    VOICEMAIL - <?php echo $row['in']['VOICEMAIL']['num_calls'] ?> - <?php echo seconds_to_hours_minutes_f($row['in']['VOICEMAIL']['duree']) ?><br>
                                                    <?php $sum_total_appel_sortant_voicemail += $row['in']['VOICEMAIL']['num_calls'] ?>
                                                    <?php $sum_total_appel_sortant_voicemail_temps += $row['in']['VOICEMAIL']['duree'] ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['in']['ANSWERED']['num_calls'] + $row['in']['MISSED']['num_calls'] + + $row['in']['VOICEMAIL']['num_calls'] ?> -  <?php echo seconds_to_hours_minutes_f($row['in']['ANSWERED']['duree'] + $row['in']['MISSED']['duree'] + + $row['in']['VOICEMAIL']['duree']) ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    
                                </div><!-- /nano-content --> 
                            </div><!-- /nano -->
                            
                            <table class="table-list compact">
                                <tbody>
                                    <tr class="total" style="background-color: #2196f3; color: #fff;">
                                        <td align="right"><strong>Total Missions</strong></td>
                                        <td colspan="2"><strong><?php echo count($missions) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #ff9800; color: #fff;">
                                        <td align="right"><strong>Total Appels = <?php echo $sum_total_calls ?></strong></td>
                                        <td colspan="2"><strong>Total Temps = <?php echo seconds_to_hours_minutes_f($sum_total_temps) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #FFC000; color: #fff;">
                                        <td align="right"><strong>Total Appels Sortant (ANSWERED) = <?php echo $sum_total_appel_sortant_answered ?></strong></td>
                                        <td colspan="2"><strong>Total Temps Appels Sortant (ANSWERED) = <?php echo seconds_to_hours_minutes_f($sum_total_appel_sortant_answered_temps) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #92D050; color: #fff;">
                                        <td align="right"><strong>Total Appels Sortant (CANCELLED) = <?php echo $sum_total_appel_sortant_canceled ?></strong></td>
                                        <td colspan="2"><strong>Total Temps Appels Sortant (CANCELLED) = <?php echo seconds_to_hours_minutes_f($sum_total_appel_sortant_canceled_temps) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #2196f3; color: #fff;">
                                        <td align="right"><strong>Total Appels Sortant (FAILED) = <?php echo $sum_total_appel_sortant_failed ?></strong></td>
                                        <td colspan="2"><strong>Total Temps Sortant (FAILED) = <?php echo seconds_to_hours_minutes_f($sum_total_appel_sortant_failed_temps) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #FFC000; color: #fff;">
                                        <td align="right"><strong>Total Appels Entrant (ANSWERED) = <?php echo $sum_total_appel_entrant_answered ?></strong></td>
                                        <td colspan="2"><strong>Total Temps Appels Entrant (ANSWERED) = <?php echo seconds_to_hours_minutes_f($sum_total_appel_entrant_answered_temps) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #92D050; color: #fff;">
                                        <td align="right"><strong>Total Appels Sortant (MISSED) = <?php echo $sum_total_appel_sortant_missed ?></strong></td>
                                        <td colspan="2"><strong>Total Temps Appels Sortant (MISSED) = <?php echo seconds_to_hours_minutes_f($sum_total_appel_sortant_missed_temps) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #2196f3; color: #fff;">
                                        <td align="right"><strong>Total Appels Sortant (VOICEMAIL) = <?php echo $sum_total_appel_sortant_voicemail ?></strong></td>
                                        <td colspan="2"><strong>Total Temps Sortant (VOICEMAIL) = <?php echo seconds_to_hours_minutes_f($sum_total_appel_sortant_voicemail_temps) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #ff9800; color: #fff;">
                                        <td align="center" colspan="3">
                                            <strong>
                                                Moyenne globale appels par mission = Total Appels / Total Missions = <?php echo $sum_total_calls ?> / <?php echo count($missions) ?> = <?php echo round($sum_total_calls/count($missions)) ?>
                                            </strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                                
                        </div><!-- / block-body -->

                    </div>
                </div><!-- /col -->
                <?php ## END OF RINGOVER ?>
                
                <div class="col col-12">
                    <div class="block nopadding">

                        <div class="block-head with-border">
                            <header><i class="os-icon os-icon-bar-chart-stats-up"></i><span>Nombre d'appel moyen de l'équipe</span></header>
                        </div>

                        <div class="block-body">
                            
                            <?php /// $calls = KPI::kpi_manager_5(); //debug($calls); ?>
                            
                            <h3 class="kpi_h3">Total appels depuis le 2020-01-07</h3>
                            <p style="color: #090;">Notez que l'appel entrant et sortant a les valeurs de <strong>Terminé Par Destination</strong> et <strong>Terminé Par Source</strong> et le reste n'est pas valable pour le KPI.</p>
                            
                            <?php $rows = KPI::kpi_manager_3cx($filters); // debug($rows); ?>
                            
                            <table class="table-list compact sticky-table-header">
                                <thead>
                                    <tr>
                                        <th>Raison Terminée</th>
                                        <th>Total Appels Sortants</th>
                                        <th style="width: 260px;">Temps Total Appels Sortants</th>
                                        <th>Total Appels Entrants</th>
                                        <th style="width: 260px;">Temps Total Appels Entrants</th>
                                        <th>Total Appels</th>
                                        <th style="width: 260px;">Temps Total Appels</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $total_calls = 0; $total_time_calls = 0; $total_good_call = 0; $total_good_call_time = 0; $total_calls_in = 0; $total_calls_out = 0; $total_time_calls_in = 0; $total_time_calls_out = 0; $total_good_call_in = 0; $total_good_call_out = 0; $total_good_call_in_time = 0; $total_good_call_out_time = 0; ?>
                                    <?php foreach($rows['total_calls'] as $key => $row): ?>
                                    <tr>
                                        <td><?php echo KPI::$reasons_terminated[$key] ?></td>
                                        <td><?php echo $row['out']['total_calls'] ?></td>
                                        <td><?php echo $row['out']['total_time'] . ' S = ' .  seconds_to_hours_minutes_f($row['out']['total_time']) ?></td>
                                        <td><?php echo $row['in']['total_calls'] ?></td>
                                        <td><?php echo $row['in']['total_time'] . ' S = ' .  seconds_to_hours_minutes_f($row['in']['total_time']) ?></td>
                                        <td><?php echo $row['out']['total_calls'] + $row['in']['total_calls'] ?></td>
                                        <td><?php echo ($row['out']['total_time'] + $row['in']['total_time']) . ' S = ' .  seconds_to_hours_minutes_f(($row['out']['total_time'] + $row['in']['total_time'])) ?></td>
                                        <?php 
                                        $total_calls += ($row['out']['total_calls'] + $row['in']['total_calls']); 
                                        $total_time_calls += ($row['out']['total_time'] + $row['in']['total_time']); 
                                        $total_calls_in += $row['in']['total_calls'];
                                        $total_calls_out += $row['out']['total_calls'];
                                        $total_time_calls_in += $row['in']['total_time'];
                                        $total_time_calls_out += $row['out']['total_time'];
                                        ?>
                                        <?php 
                                        if ($key == "TerminatedByDst" || $key == "TerminatedBySrc") {
                                            $total_good_call += ($row['out']['total_calls'] + $row['in']['total_calls']);
                                            $total_good_call_time += ($row['out']['total_time'] + $row['in']['total_time']);
                                            $total_good_call_in += $row['in']['total_calls'];
                                            $total_good_call_out += $row['out']['total_calls'];
                                            $total_good_call_in_time += $row['in']['total_time']; 
                                            $total_good_call_out_time += $row['out']['total_time'];
                                        }
                                        ?>
                                    </tr>
                                    <?php endforeach; ?>
                                    <tr class="total" style="background-color: #2196f3; color: #fff;">
                                        <td align="right"><strong>Total</strong></td>
                                        <td><strong><?php echo $total_calls_out; ?></strong></td>
                                        <td><strong><?php echo $total_time_calls_out . ' S = ' . seconds_to_hours_minutes_f($total_time_calls_out) ; ?></strong></td>
                                        <td><strong><?php echo $total_calls_in; ?></strong></td>
                                        <td><strong><?php echo $total_time_calls_in . ' S = ' . seconds_to_hours_minutes_f($total_time_calls_in); ?></strong></td>
                                        <td><strong><?php echo $total_calls; ?></strong></td>
                                        <td><strong><?php echo $total_time_calls . ' S = ' . seconds_to_hours_minutes_f($total_time_calls) ?></strong></td>
                                    </tr>
                                    <tr class="total" style="background-color: #ff9800; color: #fff;">
                                        <td align="right"><strong>Total Bons Appels</strong></td>
                                        <td><strong><?php echo $total_good_call_out; ?></strong></td>
                                        <td><strong><?php echo $total_good_call_out_time . ' S = ' . seconds_to_hours_minutes_f($total_good_call_out_time) ?></strong></td>
                                        <td><strong><?php echo $total_good_call_in; ?></strong></td>
                                        <td><strong><?php echo $total_good_call_in_time . ' S = ' . seconds_to_hours_minutes_f($total_good_call_in_time) ?></strong></td>
                                        <td><strong><?php echo $total_good_call; ?></strong></td>
                                        <td><strong><?php echo $total_good_call_time . ' S = ' . seconds_to_hours_minutes_f($total_good_call_time) ?></strong></td>
                                    </tr>
                                    <tr class="total" style="background-color: #FFC000; color: #fff;">
                                        <?php $num_days = round(getBusinessDays("2020-01-07", date("Y-m-d", time()))) ?>
                                        <td align="right" colspan="5"><strong>Moyenne de Total Appels excluant les weekends (Date : 2020-01-07 - <?php echo date("Y-m-d", time()) ?> = <?php echo $num_days ?> Jours) = Total / Nombres de jours</strong></td>
                                        <td colspan="2"><strong>= <?php echo $total_calls ?> / <?php echo $num_days ?> = <?php echo round($total_calls/$num_days) ?></strong></td>
                                    </tr>
                                    <tr class="total" style="background-color: #92D050; color: #fff;">
                                        <td align="right" colspan="5"><strong>Moyenne de Total Bons Appels excluant les weekends (Date : 2020-01-07 - <?php echo date("Y-m-d", time()) ?> = <?php echo $num_days ?> Jours) = Total Bons Appels / Nombres de jours</strong></td>
                                        <td colspan="2"><strong>= <?php echo $total_good_call ?> / <?php echo $num_days ?> = <?php echo round($total_good_call/$num_days) ?></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <h3 class="kpi_h3">Appels groupés par mission depuis le 2020-01-07</h3>
                            <p style="color: #090;">Notez que certains appels n'ont pas pu se qualifier pour sa mission car le numéro appelant ne correspond pas au numéro candidat</p>
                            
                            <div class="nano" style="height:500px;">
                            	<div class="nano-content">
                                    
                                    <?php // debug($rows['total_calls_grouped_by_mission'], true); ?>
                                    
                                    <?php $mission_status = Control::getControlListByType(9) ?>
                                    
                                    <table class="table-list compact sticky-table-header">
                                        <thead>
                                            <tr>
                                                <th>Mission</th>
                                                <th>Poste</th>
                                                <th>Manager</th>
                                                <th>Statut</th>
                                                <th>Nombres Appels</th>
                                                <th>Temps Total</th>
                                                <th>Total Appels Sortants</th>
                                                <th>Temps Total Sortants</th>
                                                <th>Total Appels Entrants</th>
                                                <th>Temps Total Entrants</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $sum_total_appels = 0; $sum_total_appels_qualifier = 0; $sum_total_time = 0; $total_temps_qualifier = 0; ?>
                                        <?php $total_appels_sortants = 0; $total_time_sortants = 0; $total_appels_entrants = 0; $total_time_entrants = 0; ?>
                                        <?php foreach($rows['total_calls_grouped_by_mission'] as $row): ?>
                                            <tr>
                                                <td align="center">
                                                    <?php if ($row['mission_id'] > 0): ?>
                                                        <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $row['mission_id'] ?>" target="_blank">
                                                            <?php echo $row['mission_id'] ?>
                                                        </a>
                                                        <?php $sum_total_appels_qualifier += $row['total_appels']; ?>
                                                        <?php $total_temps_qualifier += $row['total_time']; ?>
                                                    <?php else: ?>
                                                        N/A
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($row['poste'] <> ""): ?>
                                                        <?php echo $row['poste'] ?>
                                                    <?php else: ?>
                                                    <strong>Appels non qualifiés</strong>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($row['manager_name'] <> ""): ?>
                                                        <?php echo $row['manager_name'] ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($row['status'] <> ""): ?>
                                                        <?php echo ucfirst($mission_status[$row['status']]) ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['total_appels'] ?>
                                                    <?php $sum_total_appels += $row['total_appels']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['total_time'] . ' S = ' . seconds_to_hours_minutes_f($row['total_time']) ?>
                                                    <?php $sum_total_time += $row['total_time'] ?>
                                                </td>
                                                <td>
                                                    <?php if ($row['mission_id'] > 0): ?>
                                                        <?php echo $row['total_calls_sortants'] ?>
                                                        <?php $total_appels_sortants += $row['total_calls_sortants'] ?>
                                                    <?php else: ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($row['mission_id'] > 0): ?>
                                                        <?php echo $row['total_time_sortants'] . ' S = ' . seconds_to_hours_minutes_f($row['total_time_sortants']) ?>
                                                        <?php $total_time_sortants += $row['total_time_sortants'] ?>
                                                    <?php else: ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($row['mission_id'] > 0): ?>
                                                        <?php echo $row['total_calls_entrants'] ?>
                                                        <?php $total_appels_entrants += $row['total_calls_entrants'] ?>
                                                    <?php else: ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($row['mission_id'] > 0): ?>
                                                        <?php echo $row['total_time_entrants'] . ' S = ' . seconds_to_hours_minutes_f($row['total_time_entrants']) ?>
                                                        <?php $total_time_entrants += $row['total_time_entrants'] ?>
                                                    <?php else: ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    
                                </div><!-- /nano-content --> 
                            </div><!-- /nano -->
                            
                            <table class="table-list compact">
                                <tbody>
                                    <tr class="total" style="background-color: #2196f3; color: #fff;">
                                        <td align="right"><strong>Total Missions</strong></td>
                                        <td colspan="2"><strong><?php echo count($rows['total_calls_grouped_by_mission']) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #ff9800; color: #fff;">
                                        <td align="right"><strong>Total Appels = <?php echo $sum_total_appels ?></strong></td>
                                        <td colspan="2"><strong>Total Temps = <?php echo $sum_total_time . ' S = ' . seconds_to_hours_minutes_f($sum_total_time) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #FFC000; color: #fff;">
                                        <td align="right"><strong>Total Appels Qualifiés = <?php echo $sum_total_appels_qualifier ?></strong></td>
                                        <td colspan="2"><strong>Total Temps Qualifiés = <?php echo $total_temps_qualifier . ' S = ' . seconds_to_hours_minutes_f($total_temps_qualifier) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #92D050; color: #fff;">
                                        <td align="right"><strong>Total Appels Sortants Qualifiés = <?php echo $total_appels_sortants ?></strong></td>
                                        <td colspan="2"><strong>Total Temps Sortants Qualifiés = <?php echo $total_time_sortants . ' S = ' .seconds_to_hours_minutes_f($total_time_sortants) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #2196f3; color: #fff;">
                                        <td align="right"><strong>Total Appels Entrants Qualifiés = <?php echo $total_appels_entrants ?></strong></td>
                                        <td colspan="2"><strong>Total Temps Entrants Qualifiés = <?php echo $total_time_entrants . ' S = '. seconds_to_hours_minutes_f($total_time_entrants) ?></td>
                                    </tr>
                                    <tr class="total" style="background-color: #ff9800; color: #fff;">
                                        <td colspan="3" align="center"><strong>Moyenne globale appels par mission = (Nombre bon d'appels / Nombre de missions) = (<?php echo $sum_total_appels ?> / <?php echo count($rows['total_calls_grouped_by_mission']) ?>) = <?php if ((count($rows['total_calls_grouped_by_mission'])) > 0): echo round($sum_total_appels/(count($rows['total_calls_grouped_by_mission']))); else : echo '0'; endif; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                                
                        </div><!-- / block-body -->

                    </div>
                </div><!-- /col -->
    
<?php endif; ?>