<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {

    if (Candidat::addComment(getPost('candidat_id'), getPost('mission_id'), getPost('commentaire'))){
        $response = array(
            'status' => 'OK',
            'msg' => 'Commentaire ajouté avec succès',
            'type' => 'success',
            'callback' => 'reloadcomment',
            'param' => getPost('candidat_id')
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        );
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>