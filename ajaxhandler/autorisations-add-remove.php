<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('assign_deassign_perm')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (get('action') && get('option')) {
    $action = get('action'); // add remove
    $option = get('option'); // group all single
    $data = getPost('data');
    //debug($data, true);
    switch($option):
        case "single":
            if ($action == "add")
                Action::assignAction($data);
            else if ($action == "remove")
                Action::unassignAction($data);
            break;
        case "group":
            if ($action == "add")
                Action::assignActionModule($data);
            else if ($action == "remove")
                Action::unassignActionModule($data);
            break;
        case "all":
            if ($action == "add")
                Action::assignActionRole($data); 
            else if ($action == "remove")
                Action::unassignActionRole($data);
            break;
    endswitch;
} else {
    return false;
}
?>
