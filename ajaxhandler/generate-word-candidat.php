<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_candidat') || get('id') == ""): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php
$societe = get('societe'); // opsearch or headhunting

$candidat = Candidat::getCandidatById(get('id'));
$comment = Candidat::getLastComment(get('id'));
$motivation = Candidat::getLastMotivation(get('id'));

// https://github.com/PHPOffice/PHPWord
// http://phpword.readthedocs.io/en/latest/
// Creating the new document...
$phpWord = new \PhpOffice\PhpWord\PhpWord();

// document properties
$properties = $phpWord->getDocInfo();

if ($societe == 'opsearch') {
    
    $properties->setCreator('OPSEARCH');
    $properties->setCompany('OPSEARCH');
    
} elseif ($societe == 'headhunting') {
    
    $properties->setCreator('HEADHUNTING FACTORY');
    $properties->setCompany('HEADHUNTING FACTORY');
    
}

$title = mb_ucfirst($candidat['prenom']) . ' ' . mb_strtoupper($candidat['nom']);
$properties->setTitle('Candidat::' . $title);

/* Note: any element you append to a document must reside inside of a Section. */

// Adding an empty Section to the document...
$section = $phpWord->addSection();

// header  http://phpword.readthedocs.io/en/latest/containers.html
// Add first page header
$header = $section->addHeader();

if ($societe == 'opsearch') {
    $header->addImage('../asset/images/word/opsearch.png', array('height' => 25, 'width' => 213, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
} elseif ($societe == 'headhunting') {
    $header->addImage('../asset/images/word/headhunting.png', array('height' => 20, 'width' => 213, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
}

// footer http://phpword.readthedocs.io/en/latest/containers.html
// Add footer
$footer = $section->addFooter();
$footer->addPreserveText('{PAGE}/{NUMPAGES}', null, array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));

#####################
$section->addText(''); // adding a blank text for space between header and content

$label_style = array('size' => 11, 'bold' => true);
$value_style = array('size' => 11);
$table = $section->addTable();
// prenom
$table->addRow(600);
$table->addCell(3000)->addText('Prénom :', $label_style);
$table->addCell(5500)->addText(mb_ucfirst($candidat['prenom']), $value_style);

// nom 
$table->addRow(600);
$table->addCell(3000)->addText('Nom :', $label_style);
$table->addCell(5500)->addText(mb_ucfirst($candidat['nom']), $value_style);

// age
if ($candidat['dateOfBirth'] != 0) {    
    $table->addRow(600);
    $table->addCell(3000)->addText('Age :', $label_style);
    $table->addCell(5500)->addText(getAge($candidat['dateOfBirth']), $value_style);
}

// coordonees
$table->addRow(600);
$table->addCell(3000)->addText('Coordonnées :', $label_style);


$innertable = $table->addCell(5500)->addTable(array('cellMargin' => 80));

if ($candidat['tel_mobile_perso'] != "") {
    $innertable->addRow(500);
    $innertable->addCell()->addText('Mobile : ', $label_style);
    $innertable->addCell()->addText($candidat['tel_mobile_perso'], $value_style);
}
if ($candidat['tel_ligne_direct'] != "") {
    $innertable->addRow(500);
    $innertable->addCell()->addText('Direct : ', $label_style);
    $innertable->addCell()->addText($candidat['tel_ligne_direct'], $value_style);
}
if ($candidat['tel_pro'] != "") {
    $innertable->addRow(500);
    $innertable->addCell()->addText('Pro : ', $label_style);
    $innertable->addCell()->addText($candidat['tel_pro'], $value_style);
}
if ($candidat['tel_domicile'] != "") {
    $innertable->addRow(500);
    $innertable->addCell()->addText('Domicile : ', $label_style);
    $innertable->addCell()->addText($candidat['tel_domicile'], $value_style);
}
if ($candidat['email'] != "") {
    $innertable->addRow(500);
    $innertable->addCell()->addText('Mail 1 : ', $label_style);
    $innertable->addCell()->addText($candidat['email'], $value_style);
}
if ($candidat['email2'] != "") {
    $innertable->addRow(500);
    $innertable->addCell()->addText('Mail 2 : ', $label_style);
    $innertable->addCell()->addText($candidat['email2'], $value_style);
}

// localisation
$dpt = Control::getControlListByType(6);
$table->addRow(600);
$table->addCell(3000)->addText('Localisation :', $label_style);
$table->addCell(5500)->addText((($candidat['control_localisation_id'] > 0) ? $dpt[$candidat['control_localisation_id']] : ""), $value_style);

// formation
$table->addRow(600);
$table->addCell(3000)->addText('Formation :', $label_style);
$table->addCell(5500)->addText(nl2br($candidat['diplome_specialisation']), $value_style);

// poste actuel
$table->addRow(600);
$table->addCell(3000)->addText('Poste actuel :', $label_style);
$table->addCell(5500)->addText($candidat['poste'], $value_style);

// entreprise actuelle
$table->addRow(600);
$table->addCell(3000)->addText('Entreprise actuelle :', $label_style);
$table->addCell(5500)->addText((($candidat['is_not_in_this_company'] == 1) ? 'EX - ': '') . $candidat['societe_actuelle'], $value_style);

// remuneration
if ($candidat['remuneration'] <> "") {
    $table->addRow(600);
    $table->addCell(3000)->addText('En Package salarial (K€) :', $label_style);
    $table->addCell(5500)->addText($candidat['remuneration'] . ' K€', $value_style);
}

// detail remuneration
if ($candidat['detail_remuneration'] <> "") {
    $table->addRow(600);
    $table->addCell(3000)->addText('Détails primes / variable :', $label_style);
    $table->addCell(5500)->addText($candidat['detail_remuneration'], $value_style);
}

//if ($candidat['salaire_package'] <> "") {    
//    $table->addRow(600);
//    $table->addCell(3000)->addText('Salaire Package (K€) :', $label_style);
//    $table->addCell(5500)->addText($candidat['salaire_package'] . ' K€', $value_style);
//}

if ($candidat['salaire_fixe'] <> "") {    
    $table->addRow(600);
    $table->addCell(3000)->addText('Salaire Fixe (K€) :', $label_style);
    $table->addCell(5500)->addText($candidat['salaire_fixe'] . ' K€', $value_style);
}

//if ($candidat['sur_combien_de_mois'] <> "") {    
//    $table->addRow(600);
//    $table->addCell(3000)->addText('Sur Combien de Mois :', $label_style);
//    $table->addCell(5500)->addText($candidat['sur_combien_de_mois'], $value_style);
//}

if ($candidat['avantages'] <> "") {    
    $table->addRow(600);
    $table->addCell(3000)->addText('Avantages en nature :', $label_style);
    $table->addCell(5500)->addText($candidat['avantages'], $value_style);
}

if ($candidat['remuneration_souhaitee'] <> "") {    
    $table->addRow(600);
    $table->addCell(3000)->addText('Rémunération Souhaitée :', $label_style);
    $table->addCell(5500)->addText($candidat['remuneration_souhaitee'], $value_style);
}

// commentaire:
if (!empty($comment)) {
    
    // move to the next page
    $section->addPageBreak();
    
    $section->addText('Commentaire :', $label_style);
    
    $section->addText($comment['comment']);
    
}

// motivation:
if (!empty($comment)) {
        
    $section->addText('Motivation :', $label_style);
    
    $section->addText($motivation['motivation']);
    
}

// generate the word doc
$file_name = filterChar(mb_ucfirst($candidat['prenom'])) . '-' . filterChar(mb_strtoupper($candidat['nom'])) .'.docx';
$file = PDF_URL . '/' . $file_name;

// save the word file
// Saving the document as OOXML file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$objWriter->save($file);

$response = array(
    'status' => 'OK',
    'file' => $file_name,
);

echo json_encode($response);
exit();
?>