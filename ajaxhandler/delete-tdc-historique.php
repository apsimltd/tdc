<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php 
if (isGet()) {
    
    if (TDC::deleteHistorique(get('historique_id'))) {
        $response = array(
            'status' => 'OK',
            'msg' => 'Historique supprimée avec succès',
            'type' => 'success',
            'callback' => 'reloadhistorique',
            'param' => array(
                'mission_id' => get('mission_id'),
                'candidat_id' => get('candidat_id'),
            ),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>
