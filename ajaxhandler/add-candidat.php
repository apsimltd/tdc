<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
//    debug($_FILES);
//    debug(getPost(), true);
       
    $candidat = array(
        'user_id' => $me['id'],
        'control_civilite_id' => getPost('control_civilite_id'),
        'control_localisation_id' => (getPost('control_localisation_id') == "")? 0 : getPost('control_localisation_id'),
        'nom' => mb_strtoupper(getPost('nom')),
        'prenom' => getPost('prenom'),
        'tel_standard' => getPost('tel_standard'),
        'tel_ligne_direct' => getPost('tel_ligne_direct'),
        'tel_pro' => getPost('tel_pro'),
        'tel_mobile_perso' => getPost('tel_mobile_perso'),
        'tel_domicile' => getPost('tel_domicile'),
        'email' => getPost('email'),
        'email2' => getPost('email2'),
        'dateOfBirth' => (getPost('dateOfBirth') == "")? 0 : strtotime(getPost('dateOfBirth')),
        'societe_actuelle' => mb_strtoupper(filterCompanyName(rtrim(ltrim(getPost('societe_actuelle'))))),
        'poste' => getPost('poste'),
        'control_secteur_id' => (getPost('control_secteur_id') == "")? 0 : getPost('control_secteur_id'),
        'control_fonction_id' => (getPost('control_fonction_id') == "")? 0 : getPost('control_fonction_id'),
        'remuneration' => (getPost('remuneration') == "")? 0 : getPost('remuneration'),
        'detail_remuneration' => getPost('detail_remuneration'),
        'control_niveauFormation_id' => (getPost('control_niveauFormation_id') == "")? 0 : getPost('control_niveauFormation_id'),
        'diplome_specialisation' => getPost('diplome_specialisation'),
        'dateDiplome' => (getPost('dateDiplome') == "")? 0 : strtotime(getPost('dateDiplome')),
        'cv_fourni' => getPost('cv_fourni'),
        'tres_bon_candidat' => getPost('tres_bon_candidat'),
        'is_not_in_this_company' => getPost('is_not_in_this_company'),
        'control_source_id' => (getPost('control_source_id') == "")? 0 : getPost('control_source_id'),
        'created' => time(),
//        'salaire_package' => getPost('salaire_package'),
        'salaire_fixe' => (getPost('salaire_fixe') == "")? null : getPost('salaire_fixe'),
//        'sur_combien_de_mois' => getPost('sur_combien_de_mois'),
        'avantages' => getPost('avantages'),
        'remuneration_souhaitee' => getPost('remuneration_souhaitee'),
    );
    
    if (getPost('gdpr_status')) {
        $candidat['gdpr_status'] = getPost('gdpr_status');
    }
    
    $langues = getPost('langues'); // array of control_langue_id
    $commentaire = getPost('commentaire');
    $reseauSociaux = getPost('reseauSociaux'); // array of link
    $files = $_FILES['file']; // array 
    
    // first of all we validate if the candidate already exist
    
    if (!Candidat::CheckExistance(getPost('email'), getPost('email2'))) {
        
        $candidat_id = Candidat::addCandidat($candidat, $langues, $commentaire, $reseauSociaux, $files);

        if (getPost('mission_id') && getPost('mission_id') != "") {
            // add the candidat to tdc
            $tdc = array(
                'mission_id' => getPost('mission_id'),
                'candidat_id' => $candidat_id,
                'user_id' => $me['id'],
                'created' => time(),
            );
            
            $tdc_id = TDC::addCandidat($tdc);
            
            if ($tdc_id > 0) {
                header('Location:' . BASE_URL . '/tdc?id=' . getPost('mission_id') . '&tdc_id=' . $tdc_id);
            }
            
        } else {
            
            $notif = array(
                'status' => 'OK',
                'msg' => 'Candidat ajouté avec succès',
                'type' => 'success', 
            );
            setNotifCookie($notif);
            
            header('Location:' . BASE_URL . '/editer-candidat?id='.$candidat_id);
            
        }

    } else {
        if (getPost('mission_id') && getPost('mission_id') != "") {
            header('Location:' . BASE_URL . '/ajoute-candidat?msg=C001&tdc_id=' . getPost('mission_id'));
        } else {
            header('Location:' . BASE_URL . '/ajoute-candidat?msg=C001');
        }
    }  
    
} else {
    header('Location:' . BASE_URL . '/candidats');
}

?>