<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
    $data = array(
        'id' => getPost('id'),
        'user_id' => $me['id'],
        'mission_id' => getPost('mission_id'),
        'entreprise_id' => getPost('entreprise_id'),
        'comment' => getPost('comment'),
        'updated' => time(),
    );
     
    if (TDC::updateHistoriqueEntreprise($data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Commentaire modifiée avec succès',
            'type' => 'success',
            'callback' => 'reloadhistoriqueentreprise',
            'param' => array(
                'mission_id' => getPost('mission_id'),
                'entreprise_id' => getPost('entreprise_id'),
            ),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>