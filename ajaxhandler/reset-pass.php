<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php
if (isPost()) {
    
    $email = filter_var(getPost('user_email'), FILTER_SANITIZE_STRING);
    $pass = filter_var(getPost('user_pass'), FILTER_SANITIZE_STRING);
    
    if (User::createNewPass($email, $pass)){
        
        $notif = array(
            'status' => 'OK',
            'msg' => 'Votre mot de passe a été changé avec succès',
            'type' => 'success', 
        );
        setNotifCookie($notif);
        
        $user = User::login(User::getUsernameByMail($email), $pass, true);
        
        $log  = array(
            'module_name' => 'Utilisateurs',
            'module_code' => 'mod_user',
            'action' => 'Changement mot de passe Réinitialisation',
            'action_code' => 'mod_user_reset_pass',
            'table_parent_name' => 'users',
            'table_parent_id' => $user['id'],
            'content_type' => 'json',
            'content_before' => json_encode(array('email' => $email, 'pass' => $pass)),
        );
        
        Watchdog::log($log);
        
        header('Location:'. BASE_URL . '/tableau-de-bord');
          
    } else {
        
        $notif = array(
            'status' => 'NOK',
            'msg' => 'Une erreur s\'est produite lors de la modification de votre mot de passe. Veuillez réessayer.',
            'type' => 'error', 
        );
        setNotifCookie($notif);
        
        header("Location:" . BASE_URL . "/connexion");            

    }
    
} else {
    header("Location:" . BASE_URL . "/connexion"); 
}

?>

