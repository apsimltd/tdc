<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('delete_role')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isGet() && get('id') != "") {
    
    $role_id = get('id');
    
    // check if there is atleast 1 user associated with this role and cannot delete
    if (User::getNumUsersByRoleId($role_id) > 0) {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Le rôle ne peut pas être supprimé car des utilisateurs sont liés à ce rôle',
            'type' => 'error',           
        ); 
        echo json_encode($response);
        exit();
    }

    if (Role::deleteRole($role_id)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Rôle supprimé avec succès',
            'type' => 'success',
            'callback' => 'reloadpage',
        ); 
        echo json_encode($response);
        exit();
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
        echo json_encode($response);
        exit();
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
    echo json_encode($response);
    exit();
}
?>