<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (Client::getClientCandidatBlacklist(get('nom_societe'))) {
    $response = array(
        'status' => 'OK',
        'msg' => '<strong>Attention :</strong> La Société est blacklistée',           
    ); 
        
} else {
    $response = array(
        'status' => 'NOK',          
    ); 
}
echo json_encode($response);
exit();
?>