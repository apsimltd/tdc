<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isGet()) {
    
    if (get('op') == 'select') {
        $blacklist = 1;
    } elseif (get('op') == 'deselect') {
        $blacklist = 0;
    }
    
    $data = array(
        'blacklist' => $blacklist,
        'modified' => dateToDb(),
    );
    
    if (Candidat::EntrepriseBlacklist(get('id'), $data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Entreprise blacklister avec succès',
            'type' => 'success',
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>