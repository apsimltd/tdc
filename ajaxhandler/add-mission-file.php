<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_mission')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
        
    $files = $_FILES['file']; // array
     
    Mission::addDocuments(getPost('mission_id'), $files);
    if (get('src') && get('src') == 'tdc') {
        header('Location:' . BASE_URL . '/tdc?id='.getPost('mission_id'));
    } else if (getPost('tdc_id') && getPost('tdc_id') != "") {
        header('Location:' . BASE_URL . '/editer-mission?id='.getPost('mission_id') . '&tdc_id=' . getPost('tdc_id'));
    } else {
        header('Location:' . BASE_URL . '/editer-mission?id='.getPost('mission_id'));
    }
        
} else {
    header('Location:' . BASE_URL . '/missions');
}
?>