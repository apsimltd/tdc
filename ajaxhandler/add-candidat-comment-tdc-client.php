<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('tdc', 'TDC_CLIENT'))): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php
if (isPost()) {
    
    $data = array(
        'user_id' => $me['id'],
        'mission_id' => getPost('mission_id'),
        'candidat_id' => getPost('candidat_id'),
        'comment' => getPost('comment'),
        'created' => dateToDb(),
    );
    
    if (TDC::addCommentTDCClient($data)) {
        $response = array(
            'status' => 'OK',
            'msg' => 'Commentaire ajouté avec succès',
            'type' => 'success',
            'callback' => 'reloadcomment',
            'param' => array(
                'mission_id' => getPost('mission_id'), 
                'candidat_id' => getPost('candidat_id')
            )
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        );
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>