<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_role')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {

    $role = array('name' => getPost('name'), 'created' => time());

    if (Role::isRoleNameExist(getPost('name'))){
        $response = array(
            'status' => 'NOK',
            'msg' => 'Ce rôle existe déjà',
            'type' => 'error',           
        ); 
    } else {
        if (Role::addRole($role)){
            $response = array(
                'status' => 'OK',
                'msg' => 'Rôle ajouté avec succès',
                'type' => 'success',
                'callback' => 'reloadpage',
            );
        } else {
            $response = array(
                'status' => 'NOK',
                'msg' => 'Erreur',
                'type' => 'error',           
            ); 
        }
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>