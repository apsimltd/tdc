<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_candidat') || get('id') == ""): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php
$societe = get('societe'); // opsearch or headhunting
$mission_id = get('mission_id');

$entreprise = Candidat::getEntrepriseByIdNew(get('id'));
if ($entreprise['commentaire'] <> "") {
    $comment = array_reverse(json_decode($entreprise['commentaire'], true));
} else {
    $comment = array();
}
$mission = Mission::getMissionDetailsById($mission_id);

// config @src https://mpdf.github.io/reference/mpdf-functions/construct.html
$pdfConfig = array(
    'mode' => 'utf-8',
    'orientation' => 'P', // P => portrait, L => Landscape
    // 'tempDir' => __DIR__ . 'C:/wamp/www/mybox/mpdf/' // folder should be writable
    'format' => 'Letter', // default => A4
    'default_font_size' => 10, // in points
    'default_font' => 'Arial', // Arial, Helvetica, sans-serif
    'margin_left' => 15,
    'margin_right' => 15,
    'margin_top' => 25,
    'margin_bottom' => 16,
    'margin_header' => 5,
    'margin_footer' => 5,
);

// everything should be in millimeters for the pdf

$mpdf = new \Mpdf\Mpdf($pdfConfig);

// pdf file metadata
// metadata
$title = mb_ucfirst($entreprise['name']);
$mpdf->SetTitle('Entreprise::' . $title);


// header
// https://mpdf.github.io/reference/mpdf-functions/sethtmlheader.html

// swap header and footer for pdf based on company chosen
if ($societe == 'opsearch') {
    $header = '<div style="text-align:center; padding-bottom:10px; font-size:10px; font-weight:normal; color:#000; border-bottom:1px solid #CCC; height:20px;"><img src="../asset/images/pdf/opsearch.png" style="width:300px;height:49px;"></div>';
    $mpdf->SetAuthor('OPSEARCH');
    $mpdf->SetCreator('OPSEARCH');
} elseif ($societe == 'headhunting') {
    $header = '<div style="text-align:center; padding-bottom:10px; font-size:10px; font-weight:normal; color:#000; border-bottom:1px solid #CCC; height:20px;"><img src="../asset/images/pdf/headhunting.png" style="width:180px;height:40px;"></div>';
    $mpdf->SetAuthor('HEADHUNTING FACTORY');
    $mpdf->SetCreator('HEADHUNTING FACTORY');
}

$mpdf->SetHTMLHeader($header);

// footer
$mpdf->SetHTMLFooter('
<table width="100%">
    <tr>
        <td width="33%">{DATE j-m-Y}</td>
        <td width="33%" align="center">{PAGENO}/{nbpg}</td>
        <td width="33%" style="text-align: right;">Entreprise</td>
    </tr>
</table>');

// entreprise field
$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Client : </div><div style="float:left;">'. mb_strtoupper($mission['nom_client']) .'</div></div>';
$mpdf->WriteHTML($fieldHtml);

$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Poste : </div><div style="float:left;">'. mb_ucfirst($mission['poste']) .'</div></div>';
$mpdf->WriteHTML($fieldHtml);

$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Nom : </div><div style="float:left;">'. mb_strtoupper($entreprise['name']) .'</div></div>';
$mpdf->WriteHTML($fieldHtml);

$fieldHtmlTel = '<div style="width:100%;line-height:50px;">
	<div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Coordonnées : </div>
    <div style="float:left;">';

if ($entreprise['telephone'] != "") {
    $fieldHtmlTel .= '<span>Téléphone : '. $entreprise['telephone'] .'</span>';
}
$fieldHtmlTel .= '</div></div>';
$mpdf->WriteHTML($fieldHtmlTel);

$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Région : </div><div style="float:left;">'. $entreprise['region'] .'</div></div>';
$mpdf->WriteHTML($fieldHtml);

$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Email : </div><div style="float:left;">'. $entreprise['email'] .'</div></div>';
$mpdf->WriteHTML($fieldHtml);

$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Secteur Activité : </div><div style="float:left;">'. $entreprise['secteur_activite'] .'</div></div>';
$mpdf->WriteHTML($fieldHtml);

$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Sous Secteur Activité : </div><div style="float:left;">'. $entreprise['sous_secteur_activite'] .'</div></div>';
$mpdf->WriteHTML($fieldHtml);


// commentaire:
// display only the last comment
if (!empty($comment)) {
    
    // move to the next page
    /// $mpdf->AddPage();
    
    $fieldHtml = '<div style="width:100%;line-height:50px;"><span style="width:200px;padding-right:20px;font-weight:bold; display:inline-block;">Commentaire  : </span></div>';
    $mpdf->WriteHTML($fieldHtml);

    $fieldHtml = '<div style="width:100%;line-height:25px;"><p>'. nl2br($comment[0]['comment']) .'</p></div>';
    $mpdf->WriteHTML($fieldHtml);
}

// generate the pdf
$date = date('dmYHis', time());
$file_name = 'TDC_' . $mission['id'] . '_' . filterChar(mb_ucfirst($entreprise['name'])) .'.pdf';
$file = PDF_URL . '/' . $file_name;

// save the pdf file
// http://www.fpdf.org/en/doc/output.htm
$mpdf->Output($file); // save to a local file with the name given by name (may include a path).

$response = array(
    'status' => 'OK',
    'file' => $file_name,
);

echo json_encode($response);
exit();
?>