<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {

    $comment = array(
        'key' => getPost('key'),
        'user_id' => $me['id'],
        'entreprise_id' => getPost('entreprise_id'),
        'comment' => getPost('commentaire'),
        'modified' => dateToDb(),
    );
     
    if (Candidat::editCommentEntreprise($comment)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Commentaire modifié avec succès',
            'type' => 'success',
            'callback' => 'reloadcomment',
            'param' => getPost('entreprise_id'),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>
