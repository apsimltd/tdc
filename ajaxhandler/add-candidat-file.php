<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
            
    $files = $_FILES['file']; // array
    
    if (getPost('tdc_id') && getPost('mission_id')) {
        if (Candidat::addDocuments(getPost('candidat_id'), $files, getPost('tdc_id'), getPost('mission_id'))) {
            $url = 
            header('Location:' . BASE_URL . '/editer-candidat?id='.getPost('candidat_id') . '&tdc_id=' . getPost('mission_id') . '&tdc_line_id=' . getPost('tdc_id'));
        }
    } else {
        if (Candidat::addDocuments(getPost('candidat_id'), $files)) {
            header('Location:' . BASE_URL . '/editer-candidat?id='.getPost('candidat_id'));
        }
    }
        
} else {
    header('Location:' . BASE_URL . '/candidats');
}
?>