<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_mission', 'tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php if (isPost()) : ?>
    
    <?php
    $missionSearch = array(
        'id' => getPost('id'),
        'user_id' => $me['id'],
        'prestataire_id' => getPost('prestataire_id'),
        'client_id' => getPost('client_id'),
        'consultant_id' => getPost('consultant_id'),
        'poste' => getPost('poste'),
        'control_secteur_id' => getPost('control_secteur_id'),
        'control_localisation_id' => getPost('control_localisation_id'),
        'control_fonction_id' => getPost('control_fonction_id'),
        'control_niveauFormation_id' => getPost('control_niveauFormation_id'),
        'status' => getPost('status'),
        'age_from' => getPost('age_from'),
        'age_to' => getPost('age_to'),
        'remuneration_from' => getPost('remuneration_from'),
        'remuneration_to' => getPost('remuneration_to'),
        'all' => getPost('all'),
    );
    
    $missions = Mission::getMissionsListSearch($missionSearch);
    ?>

    <?php if (User::can('read_mission', 'edit_mission', 'delete_mission')): ?>
        <div class="row">

            <div class="col col-12">
                <div class="block nopadding">

                    <div class="block-head with-border">
                        <header><span class="glyphicon glyphicon-tasks"></span>Liste des missions</header>
                        <?php $status = Control::getControlListByType(9); ?>
                        <span class="legende">
                            <span class="title">Légende</span>
                            <?php foreach($status as $key => $value): ?>
                                <?php if ($key != 235): //supprimer status ?>
                                <span class="status <?php echo getClassStatusMission($key) ?>"><?php echo $value ?></span>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </span><!--/ legende -->
                    </div>

                    <div class="block-body missions-listing">

                        <table class="datable stripe hover missionList">
                            <thead>
                                <tr>
                                    <th>Ref</th>
                                    <th>Prestataire</th>
                                    <th>Client</th>
                                    <th>Consultant</th>
                                    <th>Poste</th>
                                    <th>Secteur</th>
                                    <th>Fonction</th>
                                    <th>Date de début</th>
                                    <th>Statut</th>
                                    <?php if (User::can('tdc')): ?><th>TDC</th><?php endif; ?>
                                    <?php if (User::can('edit_mission', 'duplicate_mission', 'delete_mission', 'read_mission')): ?><th>Actions</th><?php endif; ?>
                                    <th><i class="os-icon os-icon-user-male-circle2 tooltips" title="Assigné à"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $fonctions = Control::getControlListByType(1); ?>
                                <?php $secteurs = Control::getControlListByType(2); ?>
                                <?php foreach($missions as $mission): ?>
                                <tr <?php if (Mission::isMissionForMe($me['id'], $mission['id'])): ?>class="mine-mission <?php echo getClassStatusMission($mission['status']) ?>"<?php endif; ?>>
                                    <td align="center"><?php echo $mission['id'] ?></td>
                                    <td><?php echo $mission['prestataire'] ?></td>
                                    <td><?php echo mb_strtoupper($mission['client']) ?></td>
                                    <td><?php echo mb_ucfirst($mission['firstName']) . ' ' . mb_strtoupper($mission['lastName']) ?></td>
                                    <td><?php echo $mission['poste'] ?></td>
                                    <td>
                                        <?php 
                                        if ($mission['control_secteur_id'] > 0){
                                            echo $secteurs[$mission['control_secteur_id']];
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $fonctions[$mission['control_fonction_id']] ?></td>
                                    <td align="center" data-order="<?php echo $mission['date_debut'] ?>">
                                        <?php if ($mission['date_debut'] > 0): ?>
                                        <span title="<?php echo $mission['date_debut'] ?>"><?php echo dateToFr($mission['date_debut']) ?></span>
                                        <?php else: ?>
                                        <span title="0"></span>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <span class="status <?php echo getClassStatusMission($mission['status']) ?>">
                                            <?php echo $status[$mission['status']] ?>
                                        </span>
                                    </td>
                                    <?php if (User::can('tdc')): ?>
                                    <td align="center">
                                        <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $mission['id'] ?>" class="link_to_tdc">
                                            <span class="glyphicon glyphicon-screenshot"></span>
                                        </a>
                                    </td>
                                    <?php endif; ?>
                                    <?php if (User::can('edit_mission', 'duplicate_mission', 'delete_mission', 'read_mission')): ?>
                                    <td class="actions_button">
                                        <?php if (User::can('edit_mission')): ?>
                                        <a href="<?php echo BASE_URL ?>/editer-mission?id=<?php echo $mission['id'] ?>" class="btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></a>
                                        <?php endif; ?>
                                        <?php if (User::can('duplicate_mission')): ?>
                                        <button type="button" data-mission_id="<?php echo $mission['id'] ?>" class="btn btn-icon btn-success pull-right duplicate_mission tooltips" title="Dupliquer"><i class="fa fa-clone"></i></button>
                                        <?php endif; ?>
                                        <?php if (User::can('delete_mission')): ?>
                                        
                                            <button type="button" data-mission_id="<?php echo $mission['id'] ?>" class="delete_mission btn btn-icon btn-danger pull-right tooltips" title="Archiver"><span class="glyphicon glyphicon-trash" style="padding-left: 0px;"></span></button>

                                            <?php // @below action delete the mission in real ?>
                                            <?php if (Mission::isMissionBlank($mission['id'])): ?>
                                            <button type="button" data-mission_id="<?php echo $mission['id'] ?>" class="delete_mission_total btn btn-icon btn-danger pull-right tooltips" title="Supprimer"><span class="glyphicon glyphicon-remove" style="padding-left: 0px;"></span></button>
                                            <?php endif; ?>
                                        
                                        <?php endif; ?>
                                        <?php if (User::can('read_mission')): ?>
<!--                                            <a href="<?php //echo AJAX_HANDLER ?>/pdf-mission?id=<?php //echo $mission['id'] ?>" class="generate_pdf_mission btn btn-icon btn-warning tooltips" title="Générer un fichier PDF"><i class="fa fa-file-pdf"></i></a>-->
                                        <button type="button" data-mission_id="<?php echo $mission['id'] ?>" class="comment_zoom btn btn-icon btn-primary tooltips" title="Historiques Commentaires"><span class="glyphicon glyphicon-comment"></span></button>
                                        <?php endif; ?>
                                    </td>
                                    <?php endif; ?>
                                    <td align="center">
                                        <i class="os-icon os-icon-user-male-circle2 assigned_users tooltips" title="<?php echo addSeperator(Mission::getAssignedUsersDetails($mission['id'])) ?>"></i>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div><!-- / block-body -->

                </div>
            </div><!-- /col -->

        </div><!-- / row -->
    <?php endif; ?>

<?php endif; ?>