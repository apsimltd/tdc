<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isGet()) {
    
    if (get('mission_id') && get('mission_id') != "" && get('entreprise_id') && get('entreprise_id') != "") {
        
        // first of all we check if the entreprise was already added and removed 
        // when removed the status is 0
        if (!TDC::isEntrepriseInTDC(get('mission_id'), get('entreprise_id'))) {
            // add the candidat to tdc
            $tdc = array(
                'mission_id' => get('mission_id'),
                'entreprise_id' => get('entreprise_id'),
                'user_id' => $me['id'],
                'created' => time(),
            );
            
            $tdc_id = TDC::addEntreprise($tdc);
            
            // if (TDC::addEntreprise($tdc)) {
            if ($tdc_id > 0) {
                
                if (get('blacklist') && get('blacklist') == 1 && get('nom')) {
                    $notif = array(
                        'status' => 'NOK',
                        'msg' => '<strong>Attention :</strong> L\'entreprise ' . get('nom') . ' est blacklistée',
                        'type' => 'warning', 
                    );
                    setNotifCookie($notif);
                }
                
                header('Location:' . BASE_URL . '/tdc?id=' . get('mission_id') . '&tdc_id=' . $tdc_id);
            }
        } else {
            // set the existing entreprise status to 1
            $tdc_id = TDC::updateEntrepriseStatusInTDC(get('mission_id'), get('entreprise_id'));
            header('Location:' . BASE_URL . '/tdc?id=' . get('mission_id') . '&tdc_id=' . $tdc_id);
        }        
    }
    
} else {
    header('Location:' . BASE_URL . '/entreprises');
}

?>