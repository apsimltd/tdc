<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_mission') || get('id') == ""): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php
$candidat = Candidat::getCandidatById(get('id'));
$civilite = Control::getControlListByType(7);
$dpt = Control::getControlListByType(6);
$secteurs = Control::getControlListByType(2);
$fonctions = Control::getControlListByType(1);

$pdf = new Pdf('P', 'mm', 'A4');
$pdf->AddPage();
$title = 'Mission';
$pdf->SetTitle($title);
$pdf->SetAuthor('OPSEARCH');
$pdf->AliasNbPages();

$pdf->SetFont('Helvetica', '', 12);

$counter = 0;
foreach($candidat as $key => $value):
    /**
     * <p>fields to display in the pdf</p>
     * Mission
        Poste
        Statut (on this specific mission)
        Localisation
        Age
        Société actuelle
        Poste actuel
        Rémunération
        Détail remuneration
        Téléphone mobile perso
        Téléphone Mobile pro
        Email
        Niveau études
        Spécialisation
        Commentaire
     */
    
    $pdf->setLabel($key, $counter);
    $pdf->setValue($candidat[$key], $counter);
    $counter++;
endforeach;

// generate the pdf
$date = date('dmYHis', time());
$file_name = mb_ucfirst($candidat['prenom']) . '.' . mb_strtoupper($candidat['nom']) .'.pdf';
$file = PDF_URL . '/' . $file_name;
$pdf->Output('F', $file, true); // does not save on the server. force download

$response = array(
    'status' => 'OK',
    'file' => $file_name,
);

echo json_encode($response);
exit();
?>