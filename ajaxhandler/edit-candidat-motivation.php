<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {

    $comment = array(
        'id' => getPost('id'),
        // 'user_id' => $me['id'], => this will overwrite the author of the comment
        'candidat_id' => getPost('candidat_id'),
        'mission_id' => getPost('mission_id'),
        'motivation' => getPost('motivation'),
        'modified' => time(),
    );
     
    if (Candidat::editMotivation($comment)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Motivation modifié avec succès',
            'type' => 'success',
            'callback' => 'reloadmotivation',
            'param' => getPost('candidat_id'),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>
