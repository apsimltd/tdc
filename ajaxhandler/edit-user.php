<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_user')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
        
    $user = array(
        'id' => getPost('id'),
        'user_id' => getPost('user_id'),
        'old_user_id' => getPost('old_user_id'),
        'role_id' => (getPost('role_id') == "") ? 0 : getPost('role_id'),
        'firstName' => getPost('firstName'),
        'lastName' => getPost('lastName'),
        'email' => getPost('email'),
        'username' => getPost('username'),
        'status' => getPost('status'),
        'voip_ext' => getPost('voip_ext'),
        'voip_sda' => getPost('voip_sda'),
        'modified' => time(),
    );
    
    if (getPost('ringover_userid') <> "") {
        $user['ringover_userid'] = getPost('ringover_userid');
    }
      
    // check if username has changed
    if (getPost('old_username') != $user['username']) { // username changes
        // check if there is duplicate new username
        if (User::isLoginExist($user['username'], $user['id'])) {
            $response = array(
                'status' => 'NOK',
                'msg' => 'Cette identifiant existe déjà. Merci d\'utiliser une autre identifiant.',
                'type' => 'error',           
            ); 
            echo json_encode($response);
            exit();
        }
    }
    
    // check if email has changed
    if (getPost('old_email') != $user['email']) { // username changes
        // check if there is duplicate new email
        if (User::isUserMailExist($user['email'], $user['id'])) {
             $response = array(
                'status' => 'NOK',
                'msg' => 'Cet email existe déjà. Merci d\'utiliser un autre email.',
                'type' => 'error',           
            ); 
            echo json_encode($response);
            exit();
        }
    }
    
    // check if ext has changed
    if (getPost('old_voip_ext') != $user['voip_ext']  && $user['voip_ext'] !== "") { // ext changes
        // check if there is duplicate new ext
        if (User::isExtExist($user['voip_ext'], $user['id'])) {
             $response = array(
                'status' => 'NOK',
                'msg' => 'Cette Extension existe déjà. Merci d\'utiliser une autre extension.',
                'type' => 'error',           
            ); 
            echo json_encode($response);
            exit();
        }
    } else if ($user['voip_ext'] == ""){
        $user['voip_ext'] = 0;
    }
    
    // check if sda has changed
    if (getPost('old_voip_sda') != $user['voip_sda']  && $user['voip_sda'] !== "") { // sda changes
        // check if there is duplicate new sda
        if (User::isSDAExist($user['voip_sda'], $user['id'])) {
             $response = array(
                'status' => 'NOK',
                'msg' => 'Ce SDA existe déjà. Merci d\'utiliser un autre SDA.',
                'type' => 'error',           
            ); 
            echo json_encode($response);
            exit();
        }
    }
    
    // check if ringover user id has changed
    if (isset($user['ringover_userid'])) {
        if (getPost('old_ringover_userid') != $user['ringover_userid']) {
            // check if the ring over userid already exist
            if (User::IsRingOverUserIdExist($user['ringover_userid'], $user['id'])) {
                 $response = array(
                    'status' => 'NOK',
                    'msg' => 'Ce Ringover User Id existe déjà. Merci d\'utiliser un autre Ringover User Id.',
                    'type' => 'error',           
                ); 
                echo json_encode($response);
                exit();
            }
        }
    }

    //var_dump($user);die;
    
    // check if the password changed and append the new 
    // password in the user array to be hashed and send
    // to the user by mail
    if (getPost('password') && getPost('password') != "") {
        $user['password'] = getPost('password');
    }

    // save the user edit mode
    if (User::EditUser($user)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Utilisateur modifié avec succès',
            'type' => 'success',
            'callback' => 'reloadpage',
        );
        echo json_encode($response);
        exit();
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        );
        echo json_encode($response);
        exit();
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
    echo json_encode($response);
    exit();
}

?>