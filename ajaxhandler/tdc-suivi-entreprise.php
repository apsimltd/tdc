<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
    $data = array(
        'tdc_id' => getPost('tdc_id'), 
        'user_id' => $me['id'],
        'mission_id' => getPost('mission_id'),
        'entreprise_id' => getPost('entreprise_id'),
        'comment' => getPost('comment'),
        'modified' => dateToDb(),
    );
     
    if (TDC::updateSuiviEntreprise($data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Suivi mise à jour avec succés',
            'type' => 'success',
            'callback' => 'reloadpagescrollto',
            'param' => array(
                'mission_id' => getPost('mission_id'),
                'tdc_id' => getPost('tdc_id'),
            ),
            ///'callback' => 'reloadpage',
//            'callback' => 'reloadstatstdc',
//            'param' => getPost('mission_id'),
        );        
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>