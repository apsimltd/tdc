<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_type_content')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
    $hex = getPost('hex');
    $id = getPost('id');
    
    if (Control::updateColorTDC($id, $hex)) {
        $response = array(
            'status' => 'OK',
            'msg' => 'Couleur enregistrée avec succès',
            'type' => 'success',           
        ); 
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>
