<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>

<?php
if (isPost()) {

    $data = array(
        getPost('field') => getPost('color_id'),
        'modified' => time(),
    );   
        
    TDC::updateEntrepriseColorsInTDC(getPost('tdc_id'), $data);
}
?>