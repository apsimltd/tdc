<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('edit_candidat')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (Client::checkCandidatCompanyExist(get('nom_societe'), get('id'))) {
    $response = array(
        'status' => 'NOK',           
    ); 
        
} else {
    $response = array(
        'status' => 'OK',          
    ); 
}
echo json_encode($response);
exit();
?>