<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can(array('create_candidat', 'edit_candidat'))): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isGet()) {
    
    if (get('op') == 'select') {
        $blacklist = 1;
    } elseif (get('op') == 'deselect') {
        $blacklist = 0;
    }
    
    $data = array(
        'blacklist' => $blacklist,
        'modified' => dateToDb(),
    );
    
    if (Candidat::blacklist(get('id'), $data)){
        
        if ($blacklist == 1) {
            $msg = 'Société Candidat blacklister avec succès';
        } else {
            $msg = 'Société Candidat deblacklister avec succès';
        }
        
        
        $response = array(
            'status' => 'OK',
            'msg' => $msg,
            'type' => 'success',
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>