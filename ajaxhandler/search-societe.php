<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::isLoggedIn()): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php if (isPost()) : ?>

    <?php
    //$name = getPost('name');
    
//    $companies = Candidat::getCompanies($name);
    
    $nom = getPost('nom');
    
    $clients = Client::getClientsBlacklisterByName($nom);
    
    ?>

    <?php if (User::isLoggedIn()): ?>
    <div class="row">

        <div class="col col-12">
            <div class="block nopadding">

                <div class="block-head with-border">
                    <header><span class="glyphicon glyphicon-home"></span>Société Blacklistés</header>
                </div>

                <div class="block-body">

                    <table class="datable stripe hover">
                        <thead>
                            <tr>
                                <th>Ref</th>
                                <th>Nom société</th>
                                <?php ///if (User::can('edit_client')): ?>
<!--                                <th>Blacklister / Deblacklister</th>-->
                                <?php ///endif; ?>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach($clients as $com): ?>
                            <tr>
                                <td align="center"><?php echo $com['id'] ?></td>
                                <td><?php echo $com['nom'] ?></td>
<!--                                <td align="center">
                                    <div data-client_id="<?php ///echo $com['id'] ?>" class="tick <?php ///if (User::can('edit_client')): ?>client_blacklist<?php ///endif; ?> <?php ///if ($com['blacklist'] == 1): ?>active<?php ///endif; ?>"></div>
                                </td>-->
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div><!-- / block-body -->

            </div>
        </div><!-- /col -->

    </div><!-- / row -->
    <?php endif; ?>

<?php endif; ?>