<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
    $data = array(
        'user_id' => getPost('user_id'),
        'mission_id' => getPost('mission_id'),
        'candidat_id' => getPost('candidat_id'),
        'shortliste_added' => getPost('shortliste_added'),
    );
    
    if (TDC::updateSLUser($data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Utilisateur Short-listé mis à jour avec succés',
            'type' => 'success',
            'callback' => 'reloadUserSL',
            'param' => array(
                'mission_id' => getPost('mission_id'),
                'candidat_id' => getPost('candidat_id'),
                'shortliste_added' => getPost('shortliste_added'),
            ),
        );        
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>