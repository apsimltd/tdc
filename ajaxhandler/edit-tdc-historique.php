<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
    $data = array(
        'id' => getPost('id'),
        'user_id' => $me['id'],
        'mission_id' => getPost('mission_id'),
        'candidat_id' => getPost('candidat_id'),
        'comment' => getPost('comment'),
        'updated' => time(),
    );
     
    if (TDC::updateHistorique($data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Historique modifiée avec succès',
            'type' => 'success',
            'callback' => 'reloadhistorique',
            'param' => array(
                'mission_id' => getPost('mission_id'),
                'candidat_id' => getPost('candidat_id'),
            ),
        );
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>