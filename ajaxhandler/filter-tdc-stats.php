<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php
if (isPost()) {
    
//    debug(getPost(), true);
    
    $data = array(
        'mission_id' => getPost('mission_id'),
        'type' => getPost('type'),
        'start_date' => getPost('start_date'),
        'end_date' => getPost('end_date'),
        'tdc_legende_color_ids' => getPost('tdc_legende_color_ids'),
        'tdc_legende_entreprise_color_ids' => getPost('tdc_legende_entreprise_color_ids'),
        'tdc_legende_out_categorie_id' => getPost('tdc_legende_out_categorie_id')
    );
        
    $response = array(
        'status' => 'OK',
        'msg' => 'Statistiques TDC filtrées avec succès',
        'type' => 'success',
        'callback' => 'filterstatstdc',
        'param' => $data,
    );
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>