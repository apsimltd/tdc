<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>
<?php
if (isPost()) {
    
//    debug(getPost());
    
    $ids = getPost('id');
    $desc = getPost('desc');
    
    $data = array();
    
    for($i = 0; $i <= (count($ids) - 1); $i++) :
        $data[$i] = array(
            'id' => $ids[$i],
            'desc' => $desc[$i],
        );
    endfor;
     
    if (TDC::updateColorsLegendEntreprise($data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Légende Couleurs mise à jour avec succés',
            'type' => 'success',
            'callback' => 'reloadpage'
        );
        $notif = array(
            'status' => 'OK',
            'msg' => 'Légende Couleurs mise à jour avec succés',
            'type' => 'success',
        );
        setNotifCookie($notif);
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>