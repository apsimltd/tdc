<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL;</script><?php endif; ?>

<?php
if (isPost()) {
    
    $data = array(
        'modified' => time(),
    );
    
    if (getPost('object') == "societe") {
        $data['color_societe_label'] = 0;
        $data['color_societe_bg'] = 0;
    } 
        
    TDC::updateEntrepriseColorsInTDC(getPost('tdc_id'), $data);
}
?>