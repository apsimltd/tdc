<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('create_user')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
        
    $user = array(
        'user_id' => getPost('user_id'),
        'role_id' => (getPost('role_id') == "") ? 0 : getPost('role_id'),
        'firstName' => getPost('firstName'),
        'lastName' => getPost('lastName'),
        'email' => getPost('email'),
        'username' => getPost('username'),
        'password' => getPost('password'),
        'status' => getPost('status'),
        'created' => time(),
        'voip_ext' => getPost('voip_ext'),
        'voip_sda' => getPost('voip_sda'),
    );
    
    if (getPost('ringover_userid') <> "") {
        $user['ringover_userid'] = getPost('ringover_userid');
    }
    
    // check if username already exist
    if (User::isLoginExist($user['username'])) {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Cette identifiant existe déjà. Merci d\'utiliser une autre identifiant.',
            'type' => 'error',           
        ); 
        echo json_encode($response);
        exit();
    }
    
    // check if user email exist
    if (User::isUserMailExist($user['email'])) {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Cet email existe déjà. Merci d\'utiliser un autre email.',
            'type' => 'error',           
        ); 
        echo json_encode($response);
        exit();
    }
    
    // check if ext exist
    if ($user['voip_ext'] != 0) {
        if (User::isExtExist($user['voip_ext']) && $user['voip_ext'] !== "") {
            $response = array(
                'status' => 'NOK',
                'msg' => 'Cette Extension existe déjà. Merci d\'utiliser une autre extension.',
                'type' => 'error',
            );
            echo json_encode($response);
            exit();
        }
    } else {

        $user['voip_ext'] = 0;
    }
    
    // check if sda exist
    if (User::isSDAExist($user['voip_sda'])  && $user['voip_sda'] !== "") {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Ce SDA existe déjà. Merci d\'utiliser un autre SDA.',
            'type' => 'error',           
        ); 
        echo json_encode($response);
        exit();
    }
    
    // check if the ring over userid already exist
    if (isset($user['ringover_userid'])) {
        if (User::IsRingOverUserIdExist($user['ringover_userid'])) {
             $response = array(
                'status' => 'NOK',
                'msg' => 'Ce Ringover User Id existe déjà. Merci d\'utiliser un autre Ringover User Id.',
                'type' => 'error',           
            ); 
            echo json_encode($response);
            exit();
        }
    }
        

    if (User::addUser($user)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Utilisateur ajouté avec succès',
            'type' => 'success',
            'callback' => 'reloadpage',
        );
        echo json_encode($response);
        exit();
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        );
        echo json_encode($response);
        exit();
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
    echo json_encode($response);
    exit();
}

?>