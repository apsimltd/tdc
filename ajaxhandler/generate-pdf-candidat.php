<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('read_candidat') || get('id') == ""): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>

<?php
$societe = get('societe'); // opsearch or headhunting


$candidat = Candidat::getCandidatById(get('id'));
$comment = Candidat::getLastComment(get('id'));
$motivation = Candidat::getLastMotivation(get('id'));

// config @src https://mpdf.github.io/reference/mpdf-functions/construct.html
$pdfConfig = array(
    'mode' => 'utf-8',
    'orientation' => 'P', // P => portrait, L => Landscape
    // 'tempDir' => __DIR__ . 'C:/wamp/www/mybox/mpdf/' // folder should be writable
    'format' => 'Letter', // default => A4
    'default_font_size' => 10, // in points
    'default_font' => 'Arial', // Arial, Helvetica, sans-serif
    'margin_left' => 15,
    'margin_right' => 15,
    'margin_top' => 25,
    'margin_bottom' => 16,
    'margin_header' => 5,
    'margin_footer' => 5,
);

// everything should be in millimeters for the pdf

$mpdf = new \Mpdf\Mpdf($pdfConfig);

// pdf file metadata
// metadata
$title = mb_ucfirst($candidat['prenom']) . ' ' . mb_strtoupper($candidat['nom']);
$mpdf->SetTitle('Candidat::' . $title);


// header
// https://mpdf.github.io/reference/mpdf-functions/sethtmlheader.html

// swap header and footer for pdf based on company chosen
if ($societe == 'opsearch') {
    $header = '<div style="text-align:center; padding-bottom:10px; font-size:10px; font-weight:normal; color:#000; border-bottom:1px solid #CCC; height:20px;"><img src="../asset/images/pdf/opsearch.png" style="width:300px;height:49px;"></div>';
    $mpdf->SetAuthor('OPSEARCH');
    $mpdf->SetCreator('OPSEARCH');
} elseif ($societe == 'headhunting') {
    $header = '<div style="text-align:center; padding-bottom:10px; font-size:10px; font-weight:normal; color:#000; border-bottom:1px solid #CCC; height:20px;"><img src="../asset/images/pdf/headhunting.png" style="width:180px;height:40px;"></div>';
    $mpdf->SetAuthor('HEADHUNTING FACTORY');
    $mpdf->SetCreator('HEADHUNTING FACTORY');
}

$mpdf->SetHTMLHeader($header);

// footer
$mpdf->SetHTMLFooter('
<table width="100%">
    <tr>
        <td width="33%">{DATE j-m-Y}</td>
        <td width="33%" align="center">{PAGENO}/{nbpg}</td>
        <td width="33%" style="text-align: right;">Candidat</td>
    </tr>
</table>');

// candidats field
$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Prénom : </div><div style="float:left;">'. mb_ucfirst($candidat['prenom']) .'</div></div>';
$mpdf->WriteHTML($fieldHtml);

$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Nom : </div><div style="float:left;">'. mb_strtoupper($candidat['nom']) .'</div></div>';
$mpdf->WriteHTML($fieldHtml);

// age
if ($candidat['dateOfBirth'] != 0) {
    $fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Age : </div><div style="float:left;">'. getAge($candidat['dateOfBirth']) .'</div></div>';
    $mpdf->WriteHTML($fieldHtml);
}

$fieldHtmlTel = '<div style="width:100%;line-height:50px;">
	<div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Coordonnées : </div>
    <div style="float:left;">';

if ($candidat['tel_mobile_perso'] != "") {
    $fieldHtmlTel .= '<span>Mobile: '. $candidat['tel_mobile_perso'] .'</span>';
}
if ($candidat['tel_ligne_direct'] != "") {
    $fieldHtmlTel .= '<br><span>Direct : '. $candidat['tel_ligne_direct'] .'</span>';
}
if ($candidat['tel_pro'] != "") {
    $fieldHtmlTel .= '<br><span>Pro : '. $candidat['tel_pro'] .'</span>';
}
if ($candidat['tel_domicile'] != "") {
    $fieldHtmlTel .= '<br><span>Domicile : '. $candidat['tel_domicile'] .'</span>';
}
if ($candidat['email'] != "") {
    $fieldHtmlTel .= '<br><span>Mail 1: '. $candidat['email'] .'</span>';
}
if ($candidat['email2'] != "") {
    $fieldHtmlTel .= '<br><span>Mail 2: '. $candidat['email2'] .'</span>';
}

$fieldHtmlTel .= '</div></div>';

$mpdf->WriteHTML($fieldHtmlTel);

// localisation
$dpt = Control::getControlListByType(6);
$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Localisation : </div><div style="float:left;">'. (($candidat['control_localisation_id'] > 0) ? $dpt[$candidat['control_localisation_id']] : "") .'</div></div>';
$mpdf->WriteHTML($fieldHtml);

$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Formation : </div><div style="float:left;line-height:25px;margin-top:15px;">'. nl2br($candidat['diplome_specialisation']) .'</div></div>';
$mpdf->WriteHTML($fieldHtml);

$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Poste actuel : </div><div style="float:left;">'. $candidat['poste'] .'</div></div>';
$mpdf->WriteHTML($fieldHtml);

$fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Entreprise actuelle : </div><div style="float:left;">'. (($candidat['is_not_in_this_company'] == 1) ? 'EX - ': '') . $candidat['societe_actuelle'] .'</div></div>';
$mpdf->WriteHTML($fieldHtml);

if ($candidat['remuneration'] <> "") {
    $fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">En Package salarial (K€): </div><div style="float:left;">'. $candidat['remuneration'] .' K€</div></div>';
    $mpdf->WriteHTML($fieldHtml);
}

if ($candidat['detail_remuneration'] <> "") {
    $fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Détails primes / variable : </div><div style="float:left;">'. $candidat['detail_remuneration'] .'</div></div>';
    $mpdf->WriteHTML($fieldHtml);
}

//if ($candidat['salaire_package'] <> "") {
//    $fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Salaire Package (K€) : </div><div style="float:left;">'. $candidat['salaire_package'] .' K€</div></div>';
//    $mpdf->WriteHTML($fieldHtml);
//}

if ($candidat['salaire_fixe'] <> "") {
    $fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Salaire Fixe (K€) : </div><div style="float:left;">'. $candidat['salaire_fixe'] .' K€</div></div>';
    $mpdf->WriteHTML($fieldHtml);
}

//if ($candidat['sur_combien_de_mois'] <> "") {
//    $fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Sur Combien de Mois : </div><div style="float:left;">'. $candidat['sur_combien_de_mois'] .'</div></div>';
//    $mpdf->WriteHTML($fieldHtml);
//}

if ($candidat['avantages'] <> "") {
    $fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Avantages en nature : </div><div style="float:left;">'. $candidat['avantages'] .'</div></div>';
    $mpdf->WriteHTML($fieldHtml);
}

if ($candidat['remuneration_souhaitee'] <> "") {
    $fieldHtml = '<div style="width:100%;line-height:50px;"><div style="width:200px;padding-right:20px;font-weight:bold;float:left;">Rémunération Souhaitée : </div><div style="float:left;">'. $candidat['remuneration_souhaitee'] .'</div></div>';
    $mpdf->WriteHTML($fieldHtml);
}



// commentaire:
if (!empty($comment)) {
    
    // move to the next page
    $mpdf->AddPage();
    
    $fieldHtml = '<div style="width:100%;line-height:50px;"><span style="width:200px;padding-right:20px;font-weight:bold; display:inline-block;">Commentaire  : </span></div>';
    $mpdf->WriteHTML($fieldHtml);

    $fieldHtml = '<div style="width:100%;line-height:25px;"><p>'. nl2br($comment['comment']) .'</p></div>';
    $mpdf->WriteHTML($fieldHtml);
}

// motivation:
if (!empty($motivation)) {
        
    $fieldHtml = '<div style="width:100%;line-height:50px;"><span style="width:200px;padding-right:20px;font-weight:bold; display:inline-block;">Motivation  : </span></div>';
    $mpdf->WriteHTML($fieldHtml);

    $fieldHtml = '<div style="width:100%;line-height:25px;"><p>'. nl2br($motivation['motivation']) .'</p></div>';
    $mpdf->WriteHTML($fieldHtml);
}



// generate the pdf
$date = date('dmYHis', time());
$file_name = filterChar(mb_ucfirst($candidat['prenom'])) . '.' . filterChar(mb_strtoupper($candidat['nom'])) .'.pdf';
$file = PDF_URL . '/' . $file_name;

// save the pdf file
// http://www.fpdf.org/en/doc/output.htm
$mpdf->Output($file); // save to a local file with the name given by name (may include a path).

$response = array(
    'status' => 'OK',
    'file' => $file_name,
);

echo json_encode($response);
exit();
?>