<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php if (!User::can('tdc')): ?><script>window.location.href = BASE_URL + '/tableau-de-bord';</script><?php endif; ?>
<?php
if (isPost()) {
    
    $data = array(
        'id' => getPost('id'), 
        'user_id' => $me['id'],
        'mission_id' => getPost('mission_id'),
        'candidat_id' => getPost('candidat_id'),
        'date_prise_de_contact' => (getPost('date_prise_de_contact') == "")? 0 : strtotime(getPost('date_prise_de_contact')),
        'date_rencontre_consultant' => (getPost('date_rencontre_consultant') == "")? 0 : strtotime(getPost('date_rencontre_consultant')),
        'date_rencontre_client' => (getPost('date_rencontre_client') == "")? 0 : strtotime(getPost('date_rencontre_client')),
        'date_presentation' => (getPost('date_presentation') == "")? 0 : strtotime(getPost('date_presentation')),
        'date_retour_apres_rencontre' => (getPost('date_retour_apres_rencontre') == "")? 0 : strtotime(getPost('date_retour_apres_rencontre')),
        'shortliste' => (getPost('shortliste') == "on")? 1 : 0,
        'shortliste_old' => getPost('shortliste_old'),
        'shortliste_old_addded' => getPost('shortliste_old_addded'),
        'place' => (getPost('place') == "on")? 1 : 0,
        'comment' => getPost('comment'),
        'modified' => time(),
        'tdc_id' => getPost('tdc_id'),
    );
    
    if (getPost('user_id_kpi_assigned')) {
        $data['user_id_kpi_assigned'] = getPost('user_id_kpi_assigned');
    }
    
//    debug($_SESSION['opsearch_user']);
//    debug($data, true);
    
    if (TDC::updateSuivi($data)){
        $response = array(
            'status' => 'OK',
            'msg' => 'Suivi mis à jour avec succés',
            'type' => 'success',
            'callback' => 'reloadpagescrollto',
            'param' => array(
                'mission_id' => getPost('mission_id'),
                'tdc_id' => getPost('id'),
            ),
            ///'callback' => 'reloadpage',
//            'callback' => 'reloadstatstdc',
//            'param' => getPost('mission_id'),
        );        
    } else {
        $response = array(
            'status' => 'NOK',
            'msg' => 'Erreur',
            'type' => 'error',           
        ); 
    }
    
} else {
    $response = array(
        'status' => 'NOK',
        'msg' => 'Erreur',
        'type' => 'error',
        'callback' => 'gotologin',
    );
}
echo json_encode($response);
exit();
?>