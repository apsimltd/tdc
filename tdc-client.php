<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Tableau De Chasse" ?>
<?php $page = "tdc"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('TDC_CLIENT')) || get('id') == "") header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php /// if (!Mission::checkMissionExist(get('id')) || !Mission::checkIfClientIsAssigned($me['id'], get('id')) || !User::isSA() || !Mission::isMissionManager(get('id'), $me['id'])) header("Location: " . BASE_URL . "/missions") ?>
<?php if (!Mission::checkMissionExist(get('id'))) header("Location: " . BASE_URL)  ?>
<?php 
// check if the user has been assigned this mission else he shall not have access to it
?>
<?php require_once './views/head.php'; ?>
<?php if (!isset($_SESSION['tdc_summary'])) { $_SESSION['tdc_summary'] = 'open'; } ?>
<?php $mission = Mission::getMissionDetailsById(get('id')); // debug($mission); ?>
<?php $documents = Mission::getDocuments(get('id')); ?>
<?php $criteres = Mission::getCriteres(get('id')); ?>
<?php $OutCategories = Mission::getOutCategory(get('id')); ?>
<?php $tdcCandidates = TDC::getCandidatesByTDC_Client(get('id'))?>
<?php $colorsTDC = Control::getColorPDF(); ?>
<?php // debug($tdcEntreprises); ?>
	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
    <?php ######### START OF STICKY LEGENDE ############ ?>
    <div class="tdc-sticky-legende-wrapper"><div class="sticky-legende"></div></div>
    <?php ######### END OF STICKY LEGENDE ############## ?>
	
    <div id="base" class="TDC" style="top: 6px;">

        <div id="contentWrap">
            
            <?php // debug($tdcCandidates) ?>
            
            <div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
						
                        <div class="block-head with-border">
                            <header>
                                <span class="glyphicon glyphicon-screenshot"></span>Tableau de Chasse : 
                                
                                <?php if ($mission['prestataire_id'] == 1): // OP ?>
                                <img src="<?php echo IMAGE_URL ?>/mail/mail-header-opsearch.jpg" style="max-height: 30px; width: auto;position: relative; top: 7px;">
                                <?php elseif($mission['prestataire_id'] == 2): // HF ?>
                                <img src="<?php echo IMAGE_URL ?>/mail/mail-header-headhunting.jpg" style="max-height: 30px; width: auto; position: relative; top: 8px;">
                                <?php endif; ?>
                                
                                <span style="color:#7cbd5a; padding-left: 10px;"><?php echo $mission['id'] . ' - ' . $mission['nom_client'] . ' - ' . $mission['poste'] . ' - ' . dateToFr($mission['date_debut']) . ' - ' . dateToFr($mission['date_fin']) ?> / <?php if ($mission['prestataire_id'] == 1): echo "OPSEARCH"; elseif($mission['prestataire_id'] == 2) : echo "HF"; endif; ?></span>
                            </header>
                        </div><!-- / block-head-->
                        
                        <div class="block-head with-border tdc-client-legende clearfix">
                            <span class="legende">
                                <span class="title">Légende</span>
                                <span class="status" style="color:#ffffff; background-color:#339900">IN SL : <span class="legende_text">Candidat reçu en entretien client</span></span> 
                                <span class="status" style="color:#ffffff; background-color:#339900">IN PLACÉ : <span class="legende_text">Candidat engagé par le client</span></span>
                                <span class="status" style="color:#ffffff; background-color:#339900">IN : <span class="legende_text">Candidat validé par le client</span></span> 
                                <span class="status" style="color:#ffffff; background-color:#db86db;">ARC : <span class="legende_text">Candidat envoyé au client</span></span> 
                                <span class="status" style="color:#ffffff; background-color:#2196f3">BU+ : <span class="legende_text">Candidat validé intéressant et intéressé</span></span> 
                                <br><span class="status" style="color:#ffffff; background-color:#ff9800">BU- : <span class="legende_text">Candidat intéressant et intéressé à qui il manque un ou plusieurs critères</span></span> 
                                <span class="status" style="color:#ffffff; background-color:#dec188;">Ex BU : <span class="legende_text">Candidat validé par le manager et envoyé au client mais qui a été écarté</span></span> 
                            </span>
                        </div><!-- / block-head-->
                                                
						<div class="block-body TDC_TABLE">
							<?php if (!empty($tdcCandidates)): ?>
                            <table class="datable stripe hover">
                                <thead>
                                    <tr>
										<th>Ref</th>
                                        <th>Candidat</th>
                                        <th>Coordonnées</th>
                                        <th>Poste</th>
                                        <th>DPT</th>
                                        <th>Âge</th>
										<th>Société</th>
										<th>Rémunération</th>
                                        <th>Suivi</th>
                                        <th>Statut</th>
                                        <th>CCC</th>
                                        <th style="width: 60px;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $civilite = Control::getControlListByType(7); ?>
                                    <?php $dpt = Control::getControlListByType(6); ?>
                                    <?php $status = Control::getControlListByType(8); ?>
                                    <?php foreach($tdcCandidates as $tdcCandidat): ?>
                                    <tr class="tdc_id_<?php echo $tdcCandidat['tdc_id'] ?>" data-tdc_id="<?php echo $tdcCandidat['tdc_id'] ?>">
                                        <td align="center"><?php echo $tdcCandidat['candidat_id'] ?></td>
                                        <td class="color_candidat_bg">
                                            
                                            <?php $cand_docs = json_decode($tdcCandidat['documents'], true); ?>
                                            
                                            <span class="color_candidat_label">
                                            <?php 
                                            if ($tdcCandidat['control_civilite_id'] == 0): 
                                                echo mb_strtoupper($tdcCandidat['nom']) . ' ' . mb_ucfirst($tdcCandidat['prenom']);
                                            else:
                                                echo $civilite[$tdcCandidat['control_civilite_id']] . ' ' . mb_strtoupper($tdcCandidat['nom']) . ' ' . mb_ucfirst($tdcCandidat['prenom']);
                                            endif;
                                            
                                            if ($tdcCandidat['cv_fourni'] == 1):
                                                echo ' (CV Fourni)';
                                            endif;
                                            ?>
                                            </span>
                                            
                                            <?php if (!empty($cand_docs)) :?>
                                            <ul class="links_candidat">
                                                <?php foreach($cand_docs as $docCan): ?>
                                                    <?php 
                                                    $imgType = array('jpg', 'jpeg', 'png');
                                                    $ext = getFileExt($docCan['file']);
                                                    if (in_array($ext, $imgType)):
                                                    ?>
                                                    <li>
                                                        <a href="<?php echo CANDIDAT_URL ?>/<?php echo $tdcCandidat['id'] ?>/<?php echo $docCan['file'] ?>" data-lightbox="<?php echo $docCan['file'] ?>"><i class="fa fa-file-pdf"></i> <?php echo $docCan['file'] ?></a>
                                                    </li>
                                                    <?php else: ?>
                                                    <li>
                                                        <a href="<?php echo BASE_URL ?>/download?type=can&id=<?php echo $docCan['id'] ?>"><i class="fa fa-file-pdf"></i> <?php echo $docCan['file'] ?></a>
                                                    </li>
                                                    <?php endif; ?>
                                                <?php endforeach;?>
                                            </ul>
                                            <?php endif; ?>
                                            
                                        </td>
                                        <td>
                                            <?php
                                            if ($tdcCandidat['tel_mobile_perso'] != "")
                                                echo '<strong>Mobile perso :</strong> ' . $tdcCandidat['tel_mobile_perso'] . '<br>';
                                            if ($tdcCandidat['tel_standard'] != "")
                                                echo '<strong>Std :</strong> ' . $tdcCandidat['tel_standard'] . '<br>';
                                            if ($tdcCandidat['tel_ligne_direct'] != "")
                                                echo '<strong>Direct :</strong> ' . $tdcCandidat['tel_ligne_direct'] . '<br>';
                                            if ($tdcCandidat['tel_pro'] != "")
                                                echo '<strong>Pro :</strong> ' . $tdcCandidat['tel_pro'] . '<br>';
                                            if ($tdcCandidat['tel_domicile'] != "")
                                                echo '<strong>Domicile :</strong> ' . $tdcCandidat['tel_domicile'] . '<br>';
                                            if ($tdcCandidat['email'] != "")
                                                echo '<strong>Email 1 :</strong> ' . $tdcCandidat['email'] . '<br>';
                                            if ($tdcCandidat['email2'] != "")
                                                echo '<strong>Email 2 :</strong> ' . $tdcCandidat['email2'];
                                            ?>
                                        </td>
                                        <td><?php echo $tdcCandidat['poste'] ?></td>
                                        <td>
                                            <?php
                                            if ($tdcCandidat['control_localisation_id'] > 0) {
                                                echo $dpt[$tdcCandidat['control_localisation_id']];
                                            }
                                            ?>
                                        </td>
                                        <td align="center" style="width: 40px;">
                                            <?php
                                            if ($tdcCandidat['dateOfBirth'] != 0) {
                                                echo getAge($tdcCandidat['dateOfBirth']);
                                            }
                                            ?>
                                        </td>
                                        <td class="color_societe_bg" data-order="<?php echo ltrim(mb_strtoupper($tdcCandidat['societe_actuelle'])) ?>">
                                            <span class="color_societe_label">
                                                <?php if ($tdcCandidat['is_not_in_this_company'] == 1): ?>EX - <?php endif; ?>
                                                <?php echo mb_strtoupper($tdcCandidat['societe_actuelle']) ?>
                                            </span>
                                            <?php if ($tdcCandidat['societe_actuelle'] != "" && Client::getClientCandidatBlacklist($tdcCandidat['societe_actuelle'])): ?>
                                                <div class="clearboth notif warning" style="background-color:#ff9800; color: #fff; line-height: 24px; padding: 0px 10px;"><strong>Attention :</strong> La Société est blacklistée</div>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            
                                            <?php
                                            $cand_remuneration = "";
                                            if ($tdcCandidat['remuneration'] > 0) {
                                                $cand_remuneration .= "<strong>En Package salarial (K€) :</strong> " . $tdcCandidat['remuneration'] . " K€";
                                            }
                                            if ($tdcCandidat['detail_remuneration'] != "") {
                                                $cand_remuneration .= "<br><strong>Détails primes / variable :</strong> " . $tdcCandidat['detail_remuneration'];
                                            }
                                            if ($tdcCandidat['salaire_fixe'] != "") {
                                                $cand_remuneration .= "<br><strong>Salaire Fixe (K€) :</strong> " . $tdcCandidat['salaire_fixe'] . " K€";
                                            }
                                            if ($tdcCandidat['avantages'] != "") {
                                                $cand_remuneration .= "<br><strong>Avantages en nature :</strong> " . $tdcCandidat['avantages'];
                                            }
                                            if ($tdcCandidat['remuneration_souhaitee'] != "") {
                                                $cand_remuneration .= "<br><strong>Rémunération Souhaitée :</strong> " . $tdcCandidat['remuneration_souhaitee'];
                                            }
                                            echo $cand_remuneration;
                                            ?>
                                            
                                        </td>
                                        <td>
                                            
                                            <?php $suivi = TDC::getSuivi($tdcCandidat['mission_id'], $tdcCandidat['candidat_id']); ?>
                                            
                                            <strong>Date de prise de contact :</strong> <?php echo ($suivi['date_prise_de_contact'] > 0) ? dateToFr($suivi['date_prise_de_contact']) : ''; ?><br>
                                            <strong>Date de présentation :</strong> <?php echo ($suivi['date_presentation'] > 0) ? dateToFr($suivi['date_presentation']) : ''; ?><br>
                                            <strong>Date de rencontre consultant :</strong> <?php echo ($suivi['date_rencontre_consultant'] > 0) ? dateToFr($suivi['date_rencontre_consultant']) : ''; ?><br>
                                            <strong>Date de rencontre client :</strong> <?php echo ($suivi['date_rencontre_client'] > 0) ? dateToFr($suivi['date_rencontre_client']) : ''; ?><br>
                                            <strong>Date retour après rencontre :</strong> <?php echo ($suivi['date_retour_apres_rencontre'] > 0) ? dateToFr($suivi['date_retour_apres_rencontre']) : ''; ?><br>
                                            <strong>Suivi du statut :</strong> <?php echo nl2br($suivi['comment']); ?>
                                            
                                        </td>
                                        <td align="center" style="width: 60px;" data-order="<?php echo $tdcCandidat['control_statut_tdc_order'] ?>">
                                            <?php if ($tdcCandidat['control_statut_tdc'] > 0): ?>
                                            
                                                <?php
                                                $canStatusColor = getStatusTDCColors($tdcCandidat['control_statut_tdc'], $colorsTDC, $suivi['place'], $suivi['shortliste']);
                                                ?>
                                            
                                                <span class="status" style="color:<?php echo $canStatusColor['fontColor'] ?>; background-color:<?php echo $canStatusColor['bgColor'] ?>">
                                                    <?php 
                                                    if ($tdcCandidat['control_statut_tdc'] == 229) { // IN
                                                        if ($suivi['place'] == 1 && $suivi['shortliste'] == 1) {
                                                            echo "IN Placé";
                                                        } else if ($suivi['place'] == 1 && $suivi['shortliste'] == 0) {
                                                            echo "IN Placé";
                                                        } else if ($suivi['place'] == 0 && $suivi['shortliste'] == 1) {
                                                            echo "IN SL";
                                                        } else if ($suivi['place'] == 0 && $suivi['shortliste'] == 0) {
                                                            echo $status[$tdcCandidat['control_statut_tdc']];
                                                        }
                                                    } else {
                                                        echo $status[$tdcCandidat['control_statut_tdc']];
                                                    }
                                                    ?>
                                                </span>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center" style="width: 40px;">
                                            <div class="tick tdc_ccc client <?php if ($tdcCandidat['ccc'] > 0): ?>active<?php endif; ?>"></div>
                                        </td>
                                        <td class="actions_button">

                                            <button type="button" data-mission_id="<?php echo $tdcCandidat['mission_id'] ?>" data-candidat_id="<?php echo $tdcCandidat['candidat_id'] ?>" data-societe="<?php if ($mission['prestataire_id'] == 1): echo "opsearch"; else : echo "headhunting"; endif; ?>" class="generate_pdf_candidat_tdc_client pull-right btn btn-icon btn-warning tooltips" title="Télécharger le PDF Candidat"><i class="fa fa-file-pdf"></i></button>
                                            
                                            <button type="button" data-candidat_id="<?php echo $tdcCandidat['candidat_id'] ?>" data-mission_id="<?php echo $tdcCandidat['mission_id'] ?>" class="tdc_client_comment btn btn-icon btn-success pull-right tooltips" title="Commentaire"><span class="glyphicon glyphicon-envelope"></span></button>
                                            
                                        </td>
                                    </tr>
									<?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php else: ?>
                            <div class="message warning">Il n'y a aucun candidat dans le TDC</div>
                            <?php endif; ?>
                            
                            
                            <?php ## start of entreprise ?>
                            
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
            
        </div><!-- /contentWrap -->
        
    </div><!-- /base -->
    
	<?php require_once './views/debug.php'; ?>
    <div class="hidden hidden-data">
        <span class="tdc_id"></span>
        <span class="mission_id"></span>
    </div><!-- / hidden-data -->
        
</body>
</html>
<script src="<?php echo JS_URL ?>/tdc-client.js"></script>
<script>
$(document).ready(function(){
    <?php if (get('tdc_id') && get('tdc_id') <> ""): ?>
        // then we force scroll by around 200 px because of fixed header and then fixed table header
        if ($('#row_tdc_'+'<?php echo get('tdc_id') ?>').length) {
            var x = ($('#row_tdc_'+'<?php echo get('tdc_id') ?>').offset().top) - 200;
            $('html,body').animate({scrollTop:x},2000);
        } 
    <?php endif; ?>

    
    <?php // if ($_SESSION['tdc_summary'] == 'close'): ?>
//    $('span.triggerSummary').css('top', '25px');
//    $('span.triggerSummary.close').fadeOut(500);
//    $('span.triggerSummary.open').fadeIn(500);
//    $('#base.TDC').css('margin-top', '55px');
//    headerOffset = 120;
    <?php // endif; ?>
    
    filterDateStats.Init();
         
});

var filterDateStats = {
    Init: function (){
        $("#start_date").on().datepicker({
            dateFormat: 'dd-mm-yy', 
            regional: 'fr',
            //minDate: '<?php //echo dateToFr($mission['date_debut']) ?>',
            maxDate: '<?php echo dateToFr($mission['date_fin']) ?>',
            showButtonPanel: true,
            onClose: function(selectedDate) {
                $("#end_date").datepicker("option", "minDate", selectedDate);
                forms.validateField($("#start_date"), 'text');
            }
        });
        $("#end_date").on().datepicker({
            dateFormat: 'dd-mm-yy', 
            regional: 'fr',
            minDate: '<?php echo dateToFr($mission['date_debut']) ?>',
            maxDate: '<?php echo dateToFr($mission['date_fin']) ?>',
            showButtonPanel: true,
            onClose: function(selectedDate) {
                $("#start_date").datepicker("option", "maxDate", selectedDate);
                forms.validateField($("#end_date"), 'text');
            }
        });    
    },
}

// used for stickey legende
var OriginalStickyLegendeHtml = $('.wrapper-legende').html();
</script>