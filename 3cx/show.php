<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php
/**
 * @TODO must remove comments on prod for checking 3CX server IP
 * @example https://www.opsearch.info/3cx/show?ContactID=245036
 */

header('Content-type: application/json');

define('ERROR', 'You are not allowed to use this!!!');
define('TroisCX_SERVER_IP', isDev()? '127.0.0.1' : '188.165.203.80');

$request_ip = $_SERVER['REMOTE_ADDR'];

///if ($request_ip === TroisCX_SERVER_IP) {
    
    if (get('ContactID')) {
        
        /// $q = mysqli_real_escape_string(get('number')); // TO get rid of SQL Enjection
        
        $q = get('ContactID');
        
        // must format the number as they are stored as 01 02 03 04 05
        // its handled in the class instead
        $result = TroisCX::showById($q);
        
        if (!empty($result)) {
            
            $response = array(
                'contact' => array(
                    'id' => $result['id'],
                    'firstname' => $result['prenom'],
                    'lastname' => $result['nom'],
                    'company' => $result['societe_actuelle'],
                    'email' => $result['email'],
                    'businessphone' => $result['tel_standard'],
                    'businessphone2' => $result['tel_mobile_perso'],
                    'url' => 'https://www.opsearch.info/3cx/show?ContactID=' . $result['id'],
                    'customvalue' => $result['poste'],
                )
            );
            
            echo json_encode($response);
            exit();
            
        } else {
            
            echo json_encode("No contact found");
            exit();
            
        }
                
        
    } else {
        
        echo json_encode("No contact defined in parameter");
        exit();
        
    }
    
    
    
///} else {
    ///echo json_encode(ERROR);
    ///exit();
///}



?>
