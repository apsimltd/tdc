<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php
// process all files transferred

$files = glob(CDR_PATH . '/*');
 
//Loop through the file list.
$counter = 0;
foreach($files as $file){
    //Make sure that this is a file and not a directory.
    if(is_file($file)){
        
        // process the file and store in table kpi_cdrs
        if (TroisCX::processCDR($file)) {
            //delete the file
            ///unlink($file);
            $counter++;
        } 
        
    }
}

echo "<br><br>Number of cdr files inserted = " . $counter;

