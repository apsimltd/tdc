<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php
// normal ftp to 3cx server
// cdr details and path in c3x server is here https://www.3cx.com/docs/cdr-call-data-records/
// on production started officially on 2020-01-08 to generate data
$host = '188.165.203.80';
$port = 21;
$user = 'TDC';
$password = 'Pass0103!@!2020';

$ftpConn = ftp_connect($host, $port);
$login = ftp_login($ftpConn, $user, $password);

if ((!$ftpConn) || (!$login)) {
    
    // send a notification to dev by mail            
    Mail::sendMail('FTP CDR 3CX OPSEARCH : Cannot Connect to FTP ' . date('Y-m-d G:i:s', time()), 
            'faardeen.madarbokas@gmail.com', 
            'FTP CDR 3CX OPSEARCH : Cannot Connect to FTP ' . date('Y-m-d G:i:s', time()));
    
    echo "<br>Error connecting to FTP CDR 3CX OPSEARCH";
    
} else {
    
    echo "<br>FTP CDR 3CX OPSEARCH CONNECTION SUCCESS";
    
} // end if verification ftp connection

echo "<br>";

// list all files in the directory
$files = ftp_nlist($ftpConn, "."); 

// debug($files);

$storage_dir = CDR_PATH;

$count = 0;
// fetch all files from FTP first
foreach($files as $file_cdr) :
    
    // check if the file is of type .log
    // cdr file name format is
    // cdr-200106173550.271073.log
    $file_info = pathinfo($file_cdr);

    if (isset($file_info['extension']) && $file_info['extension'] == "log") {
        
        // debug($file_info);
        echo "<br>". $file_info['basename'] . " import ";
        
        // transfer the file from the ftp to the server opsearch
        ftp_get($ftpConn, CDR_PATH . '/' . $file_info['basename'], $file_info['basename'], FTP_BINARY);
        
        
        
        // delete the file from the ftp 
        if (!isDev()) {
            ftp_delete($ftpConn, $file_cdr);
        }
        
        $count++;
        
    }
    
endforeach;

echo "<br><br>Number of cdr files transferred = " . $count;

// process all files transferred

$files = glob(CDR_PATH . '/*');
 
//Loop through the file list.
$counter = 0;
foreach($files as $file){
    //Make sure that this is a file and not a directory.
    if(is_file($file)){
        
        // process the file and store in table kpi_cdrs
        if (TroisCX::processCDR($file)) {
            //delete the file
            unlink($file);
            $counter++;
        } 
        
    }
}

echo "<br><br>Number of cdr files inserted = " . $counter;

$msg = "Number of cdr files inserted = " . $counter;

// mail('faardeen.madarbokas@gmail.com', 'CRON 3CX', $msg);

