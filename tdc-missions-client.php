<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::TDC MISSIONS CLIENTS" ?>
<?php $page = "tdc-missions-clients"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can('TDC_MISSIONS_CLIENT')) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>
	
    <?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base">

        <div id="contentWrap">
        
			<section class="title block">

				<h1 class="pull-left">TDC MISSIONS CLIENT</h1>
				
			</section>
			
            <?php if (User::isAdmin()): ?>
            
            <div class="row">
				
                <div class="col col-12">
                    <div class="block nopadding">

                        <div class="block-head with-border">
                            <header><i class="os-icon os-icon-ui-37"></i>Filtrer</header>
                        </div>

                        <div class="block-body">

                            <form class="frm_frm frm_horizontal" name="frm_search_mission" id="frm_search_mission" method="get" action="<?php echo BASE_URL ?>/tdc-missions-client" data-type="html">
               
                                <fieldset>
                                    <select class="frm_chosen must" name="manager_id" data-validation="val_blank">
                                        <option value="">Choisir Manager</option>
                                        <?php $managers = User::getManagers() ?>
                                        <?php foreach($managers as $manager): ?>
                                        <option value="<?php echo $manager['id'] ?>"><?php echo mb_strtoupper($manager['lastName']) . ' ' . mb_ucfirst($manager['firstName']) ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </fieldset>

                                <fieldset>
                                    <button type="button" class="btn btn-success frm_submit frm_notif" data-form="2"><i class="ico-txt os-icon os-icon-ui-37"></i> Rechercher</button>
                                </fieldset>
                            </form>

                        </div><!-- / block-body -->

                    </div>
                </div><!-- /col -->

            </div><!-- / row -->
            
            <?php endif; ?>
            
            <div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><span class="glyphicon glyphicon-screenshot"></span> Liste missions client</header>
                        </div>
						
						<div class="block-body">                   
							<?php 
                            if (get('manager_id') && get('manager_id') > 0) {
                                $missionsClients = Mission::getTDCMissionsClient(get('manager_id'));    
                            } else {
                                if ($me['role_id'] == 2) { // manager
                                    $missionsClients = Mission::getTDCMissionsClient($me['id']); 
                                } else {
                                    $missionsClients = Mission::getTDCMissionsClient(); 
                                }
                            }
                            ?>
                            <?php $status_mission = Control::getControlListByType(9); ?>
                            <table class="datable stripe hover">
                                <thead>
                                    <tr>
                                        <th>Mission</th>
                                        <th>TDC</th>
                                        <th>TDC Client</th>
										<th>Manager</th>
                                        <th>Client</th>
                                        <th>Poste</th>
                                        <th>Date Début</th>
                                        <th>Date Fin</th>
										<th>Statut</th>
                                        <th>Utilisateurs Client</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($missionsClients as $mission): ?>
                                    <tr>
                                        <td align="center">
                                            <a href="<?php echo BASE_URL ?>/editer-mission?id=<?php echo $mission['mission_id'] ?>" target="_blank"><?php echo $mission['mission_id'] ?></a>
                                        </td>
                                        <td align="center">
                                            <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $mission['mission_id'] ?>" target="_blank" class="link_to_tdc">
                                                <span class="glyphicon glyphicon-screenshot"></span>
                                            </a>
                                        </td>
                                        <td align="center">
                                            <a href="<?php echo BASE_URL ?>/tdc-client?id=<?php echo $mission['mission_id'] ?>" target="_blank" class="link_to_tdc">
                                                <span class="glyphicon glyphicon-screenshot"></span>
                                            </a>
                                        </td>
                                        <td><?php echo $mission['manager_name'] ?></td>
                                        <td><?php echo $mission['client'] ?></td>
                                        <td><?php echo $mission['poste'] ?></td>
                                        <td><?php echo dateToFr($mission['date_debut']) ?></td>
                                        <td><?php echo dateToFr($mission['date_fin']) ?></td>
										<td align="center">
                                            <span class="status <?php echo getClassStatusMission($mission['status']) ?>">
                                                <?php echo $status_mission[$mission['status']] ?>
                                            </span>
                                        </td>
                                        <td>
                                            <?php $users = Mission::getAssignedClientToMissions($mission['mission_id']); ?>
                                            <?php 
                                            foreach($users as $user): 
                                                echo ' - ' . mb_strtoupper($user['lastName']) . ' ' . mb_ucfirst($user['firstName']) . ' - ' . $user['email'] . '<br>';
                                            endforeach;
                                            ?>
                                            
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>						
                                </tbody>
                            </table>
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
        
        </div><!-- /contentWrap -->
        
    </div><!-- /base -->
    
	<?php require_once './views/debug.php'; ?>    
    
</body>
</html>