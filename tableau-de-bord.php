<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php if (!User::isLoggedIn()) header("Location: " . BASE_URL . "/connexion") ?>
<?php $page_title = "OPSEARCH::Tableau de bord" ?>
<?php $page = "dashboard"; ?>
<?php require_once './views/head.php'; ?>

    <?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
    
    <div id="base">
                
        <div id="contentWrap">

            <?php if (User::isClient()): ?>
            
            <section class="title block">
                <?php $client = Client::getClientById($me['client_id']); // debug($client); ?>
                <h1>Tableau de bord : <span class="client"><?php echo $client['nom'] ?></span></h1>
			</section>
            
            <div class="row">
                
                <div class="col col-12">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><span class="glyphicon glyphicon-tags"></span>Mes Missions</header>
                        </div>
						
						<div class="block-body">
                            
                            <?php $missions = Mission::getMissionsDetailsByAssignedClientUserId($me['id']); //debug($missions); ?>
                            
                            <?php if (empty($missions)): ?>
                            
                                <div class="message warning">Aucune mission ne vous a été assignée</div>
                                
                            <?php else: ?>
                                
                                <?php $fonctions = Control::getControlListByType(1); ?>
                                <?php $secteurs = Control::getControlListByType(2); ?>
                                <?php $status_mission = Control::getControlListByType(9); ?>

                                <table class="datable stripe hover missionList">
                                    <thead>
                                        <tr>
                                            <th>Ref</th>
                                            <th>Prestataire</th>
                                            <th>Consultant</th>
                                            <th>Manager</th>
                                            <th>Poste</th>
                                            <th>Secteur</th>
                                            <th>Fonction</th>
                                            <th>Date de début</th>
                                            <th>Date de Fin</th>
                                            <th>Statut</th>
                                            <th>TDC</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php foreach($missions as $mission): ?>
                                        <tr>
                                            <td align="center"><?php echo $mission['id'] ?></td>
                                            <td><?php echo $mission['prestataire'] ?></td>
                                            <td><?php echo mb_ucfirst($mission['consultant_fname']) . ' ' . mb_strtoupper($mission['consultant_lname']) ?></td>
                                            <td><?php echo $mission['manager_name'] ?></td>
                                            <td><?php echo $mission['poste'] ?></td>
                                            <td>
                                                <?php 
                                                if ($mission['control_secteur_id'] > 0){
                                                    echo $secteurs[$mission['control_secteur_id']];
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $fonctions[$mission['control_fonction_id']] ?></td>
                                            <td align="center" data-order="<?php echo $mission['date_debut'] ?>">
                                                <?php if ($mission['date_debut'] > 0): ?>
                                                <span title="<?php echo $mission['date_debut'] ?>"><?php echo dateToFr($mission['date_debut']) ?></span>
                                                <?php else: ?>
                                                <span title="0"></span>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" data-order="<?php echo $mission['date_fin'] ?>">
                                                <?php if ($mission['date_fin'] > 0): ?>
                                                <span title="<?php echo $mission['date_fin'] ?>"><?php echo dateToFr($mission['date_fin']) ?></span>
                                                <?php else: ?>
                                                <span title="0"></span>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center">
                                                <span class="status <?php echo getClassStatusMission($mission['status']) ?>">
                                                    <?php echo $status_mission[$mission['status']] ?>
                                                </span>
                                            </td>
                                            <td align="center">
                                                <a href="<?php echo BASE_URL ?>/tdc-client?id=<?php echo $mission['id'] ?>" class="link_to_tdc">
                                                    <span class="glyphicon glyphicon-screenshot"></span>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                
                            <?php endif; ?> 
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
            </div><!-- / row -->
            <?php else: ?>
            
            <section class="title block">
				<h1>Tableau de bord</h1>
			</section>
            
            <div class="row">
                
                <div class="col col-12">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><span class="glyphicon glyphicon-tasks"></span>Mes Missions en cours</header>
                        </div>
						
						<div class="block-body">
							<?php $missions = Mission::getMyMissionListEnCours($me['id']); ?>
                            <div class="nano" style="height:500px;">
                            	<div class="nano-content">
                                    <?php if (!empty($missions)): ?>
                                	<?php $status = Control::getControlListByType(9); ?>
                                    <table class="table-list compact sticky-table-header">
                                        <thead>
                                            <tr>
                                                <?php if (User::can('tdc')): ?><th>TDC</th><?php endif; ?>
                                                <th>Ref</th>
                                                <th>Prestataire</th>
                                                <th>Client</th>
                                                <th>Manager</th>
                                                <th>Consultant</th>
                                                <th>Poste</th>
                                                <th>Fonction</th>
                                                <th>Date Début</th>
                                                <th>Date Fin</th>
                                                <th>Statut</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $fonctions = Control::getControlListByType(1); ?>
                                            <?php foreach($missions as $mission): ?>
                                            <tr>
                                                <?php if (User::can('tdc')): ?>
                                                <td align="center">
                                                    <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $mission['id'] ?>" class="link_to_tdc">
                                                        <span class="glyphicon glyphicon-screenshot"></span>
                                                    </a>
                                                </td>
                                                <?php endif; ?>
                                                <td align="center"><?php echo $mission['id'] ?></td>
                                                <td><?php echo $mission['prestataire'] ?></td>
                                                <td><?php echo mb_strtoupper($mission['client']) ?></td>
                                                <td><?php echo $mission['manager_name'] ?></td>
                                                <td><?php echo $mission['consultant'] ?></td>
                                                <td><?php echo $mission['poste'] ?></td>
                                                <td><?php echo $fonctions[$mission['control_fonction_id']] ?></td>
                                                <td align="center">
                                                    <span title="<?php echo $mission['date_debut'] ?>"><?php echo dateToFr($mission['date_debut']) ?></span>
                                                </td>
                                                <td align="center">
                                                    <span title="<?php echo $mission['date_fin'] ?>"><?php echo dateToFr($mission['date_fin']) ?></span>
                                                </td>
                                                <td align="center">
                                                    <span class="status <?php echo getClassStatusMission($mission['status']) ?>">
                                                        <?php echo $status[$mission['status']] ?>
                                                    </span>
                                                </td>
                                                
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    
                                    <?php else: ?>
                                    <div class="message warning">
                                        Aucune mission en cours
                                    </div>
                                    <?php endif; ?>
                                    
                                </div><!-- /nano-content -->
                            </div><!-- nano -->
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
            </div><!-- / row -->
            
            <div class="row">
				
				<div class="col col-4">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><span class="glyphicon glyphicon-tags"></span>Mes Tâches</header>
                        </div>
						
						<div class="block-body">
							
                            <div class="nano" style="height:315px;">
                            	<div class="nano-content">
                                	
                                    <div class="tasks_wrapper clearfix tasksAssignor">
                                    
										<?php $createdTasks = Task::getTaskListAssignee($me['id']) ?>
                                        
                                        <?php if (!empty($createdTasks)): ?>
                                        
                                            <?php foreach($createdTasks as $task): ?>
                                            <div class="task-list-row task_rows_<?php echo $task['id'] ?> <?php if ($task['status'] == 0): ?>completed<?php endif; ?>">
                                                <div class="task-check tooltips" title="<?php if ($task['status'] == 1): ?>Terminé<?php else: ?>Incomplet<?php endif; ?>" data-task_id="<?php echo $task['id'] ?>">
                                                    <svg viewBox="0 0 32 32"><polygon points="27.672,4.786 10.901,21.557 4.328,14.984 1.5,17.812 10.901,27.214 30.5,7.615 "></polygon></svg>
                                                </div>
                                                <div class="task-title">
                                                    <?php echo $task['title'] ?>
                                                    <?php if (User::can('read_task', 'read_all_task', 'create_task')): ?>
                                                    <a href="<?php BASE_URL ?>/taches?id=<?php echo $task['id'] ?>"><span class="glyphicon glyphicon-eye-open tooltips" title="Voir Plus"></span></a>
                                                    <?php endif; ?>
                                                </div>
                                                <?php $creator = User::getUserById($task['creator_id']); ?>
                                                <div class="task-creator">
                                                    <?php echo dateToFr($task['created'], true) ?> - <strong><?php echo mb_ucfirst(substr($creator['firstName'], 0, 1)) . '. ' . mb_strtoupper($creator['lastName']) ?></strong>
                                                    
                                                    <?php if (User::can('archive_task')): ?>
                                                        <?php if ($task['status'] == 0): ?><i class="fa fa-archive archive_task tooltips" title="Archiver" data-task_id="<?php echo $task['id'] ?>"></i><?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div><!-- /task-list-row -->
                                            <?php endforeach; ?>
                                        
                                        <?php else: ?>
                                        
                                        <div class="task-list-row completed">
                                            <div class="task-title">
                                                Aucune tâche                                        
                                            </div>
                                        </div><!-- /task-list-row -->
                                        
                                        <?php endif; ?>
                                        
                                    </div><!-- /tasks_wrapper -->
                                    
                                </div><!-- /nano-content -->
                            </div><!-- nano -->
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
                <div class="col col-4">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><span class="glyphicon glyphicon-calendar"></span>Mes événements à venir</header>
                        </div>
						
						<div class="block-body">
							<div class="nano" style="height:315px;">
                            	<div class="nano-content">
                                    <?php $events = Dashboard::getEventsEnCours($me['id']); ?>
                                    <?php // debug($events); ?>

                                    <?php if (!empty($events)): ?>
                                    <table class="table-list compact">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Type d'évènements</th>
                                                <th>Titre</th>
                                                <th>Date de début</th>
                                                <th>Date de fin</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($events as $event): ?>
                                            <tr>
                                                <td align="center"><span class="type <?php echo $event['cssClass'] ?>"></span></td>
                                                <td><?php echo $event['name'] ?></td>
                                                <td><?php echo $event['title'] ?></td>
                                                <td><?php echo $event['start_date'] ?></td>
                                                <td><?php echo dateToFr((strtotime($event['end_date']) + 60), true); ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>

                                    <?php else: ?>
                                    <div class="message warning">
                                        Aucun événement en cours
                                    </div>
                                    <?php endif; ?>
                                </div><!-- /nano-content -->
                            </div><!-- /nano -->
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
                <div class="col col-4">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="os-icon os-icon-bar-chart-stats-up"></i>Mes Statistiques</header>
                        </div>
						
						<div class="block-body">
							
							<table class="table-list compact">
								<thead>
									<tr>
										<th>Statistiques</th>
										<th>Nombre</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Nombre de candidats <strong>Ajoutés</strong></td>
                                        <td align="center"><strong><?php echo Dashboard::getNoCandidateAdded($me['id']) ?></strong></td>
									</tr>
									<tr>
										<td>Nombre de candidats ajoutés ayant terminé <strong>Placé</strong> en mission</td>
                                        <td align="center"><strong><?php echo Dashboard::getNoCandidateAddedPlace($me['id']) ?></strong></td>
									</tr>
									<tr>
										<td>Nombre de candidats ajoutés ayant terminé <strong>Short-listé</strong> en mission</td>
                                        <td align="center"><strong><?php echo Dashboard::getNoCandidateAddedShortliste($me['id']) ?></strong></td>
									</tr>
									<tr>
										<td>Nombre de candidats <strong>Contactés</strong></td>
                                        <td align="center"><strong><?php echo Dashboard::getNoCandidateContacted($me['id']) ?></strong></td>
									</tr>
									<tr>
										<td>Nombre de missions <strong>Impliquées</strong></td>
                                        <td align="center"><strong><?php echo Dashboard::getNoMissionsImplicated($me['id']) ?></strong></td>
									</tr>
									<tr>
										<td>Nombre de missions terminées par un <strong>Placement</strong></td>
                                        <td align="center"><strong><?php echo Dashboard::getNoMissionsImplicatedPlacement($me['id']) ?></strong></td>
									</tr>
									<tr>
										<td>Nombre moyen de candidats <strong>ajoutés</strong> par jour</td>
                                        <td align="center"><strong><?php echo Dashboard::getAvgCandidateAddedPerDay($me['id']) ?></strong></td>
									</tr>
									<tr>
										<td>Nombre moyen de candidats <strong>contactés</strong> par jour</td>
                                        <td align="center"><strong><?php echo Dashboard::getAvgCandidateContactedPerDay($me['id']) ?></strong></td>
									</tr>
								</tbody>
							</table>
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
			</div><!-- / row -->
            <?php endif; ?>
			
        </div><!-- /contentWrap -->
        
    </div><!-- /base --> 
	
	<?php require_once './views/debug.php'; ?>
    
</body>
</html>
<script>
$(document).ready(function(){
    <?php if (User::can('read_task', 'read_all_task', 'create_task')): ?>
    // mark task as complete
	$(document).on('click', '.task-list-row .task-check', function () {
		var parent = $(this).parent('.task-list-row');
		var task_id = $(this).attr('data-task_id');            
		$.ajax({
			url: AJAX_UI+'/task-row-dashboard?id='+task_id,
			dataType : 'html',
			cache: false,
			success: function (result, status) {
				$('.task-screen').html('');
				parent.html(result);
				if (parent.hasClass('completed')) {
					parent.removeClass('completed');
					Notif.Show('La tâche a été marquée comme incomplète avec succès', 'success', true, 3000);
				} else {
					parent.addClass('completed');
					Notif.Show('La tâche a été marquée comme terminée avec succès', 'success', true, 3000);
				}
				
			}
		});
	});
    <?php endif; ?>
    <?php if (User::can('archive_task')): ?>
    // archive task
	$(document).on('click', '.archive_task', function (){
		var task_id = $(this).attr('data-task_id');
		Dialog.Show('Veuillez confirmer', 'Archiver tâche', AJAX_HANDLER + '/delete-archive-task?id='+task_id+'&op=archive');
	});
    <?php endif; ?>    
});


<?php if (User::can(array('create_task', 'read_task', 'edit_task', 'delete_task', 'archive_task'))): ?>
function reloadtaskcreator(user_id) {
    $.ajax({
        url: AJAX_UI+'/reload-task-creator-dashboard?id='+user_id,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('.tasksCreator').html(result);
        }
    });
}
<?php endif; ?>
</script>
<?php
/**
 * 1. list of tasks. [OK]
 * 2. list of missions the user is working on [OK]
 * 3. upcoming events from calendar
 * 4. STATS
 *      4.1 number of candidates added by the user
 *      4.2 number of candidates added by the user who ended "place" on a mission
 *      4.3 number of candidates added by the user who ended "shot liste" on a mission
 *      4.4 number of candidates contactes by the user => when user add a date "date de prise de contact"
 *      4.5 number of missions the user was involved who ended by a placement
 *      4.6 number of missions where he was involved not depending on the statut
 * 
 *      4.7 Average number of candidates added by day
 *      4.8 Average number of candidates contacted by day 
 */
?>