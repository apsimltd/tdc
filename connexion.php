<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Connexion" ?>
<?php $page = "page-login"; ?>
<?php require_once './views/init.php'; ?>
<?php require_once './views/head.php'; ?>
    <?php require_once './views/header.php'; ?>
            
    <div class="login">

        <div class="login-title">Connectez pour commencer votre session</div>
        
        <form class="frm_frm frm_ajax" name="frm_login" id="frm_login" data-url="<?php echo AJAX_HANDLER ?>/login" data-type="json">
        
            <fieldset>
                <input type="text" name="username" class="frm_text must" placeholder="Identifiant" autocomplete="off" data-validation="val_blank">
                <i class="os-icon os-icon-user-male-circle2"></i>
            </fieldset>
            
            <fieldset>
                <input type="password" name="password" class="frm_text pass must" placeholder="Mot de passe" autocomplete="off" data-validation="val_blank">
                <span class="glyphicon glyphicon-lock"></span>
                <i class="fas fa-eye togglePassword"></i>
            </fieldset>
            
            <fieldset>
                <button type="button" id="loginSubmit" class="btn btn-lg btn-primary frm_submit frm_notif" data-form="2">Connexion</button>
            </fieldset>
            
            <a id="forget_password" href="<?php echo BASE_URL ?>/mot-de-passe-oublie" class="tooltips" title="Mot de passe oublié!">Mot de passe oublié!</a>
            
        </form>

    </div><!-- / login -->
    
    <?php require_once './views/debug.php'; ?>
    
</body>
</html>
<script>
$(document).ready(function(){
	// login page
    Connexion.Init();
    $(window).resize(function(){
        Connexion.Init();
    });
    Connexion.SubmitOnEnter();
    Password.Toggle();
});

var Password = {
    Toggle: function () {
        $(document).on('click', '.togglePassword', function(){
            if ($(this).hasClass('fa-eye-slash')) {
                $(this).removeClass('fa-eye-slash').addClass('fa-eye').removeClass('active');
                $(this).parent('fieldset').find('input.pass').attr('type', 'password');
            } else if ($(this).hasClass('fa-eye')) {
                $(this).removeClass('fa-eye').addClass('fa-eye-slash').addClass('active');
                $(this).parent('fieldset').find('input.pass').attr('type', 'text');
            }
        });
    },
}

var Connexion = {
    Init: function (){
        if ($('body').hasClass('page-login')) {
            var height = $(window).height() - (($('#header').height()) * 2);
            $('body').height(height);
        }
    },
    SubmitOnEnter: function() {
        $(document).on('keypress', '#frm_login input.frm_text', function(event){
            var code = event.keyCode ? event.keyCode : event.which;
            if (code == 13) { // on pressing carriage return
                $('#loginSubmit').trigger('click');
            }
        });
    },
}

function frm_login_ajax_success (result, status) {
    if (status === "success" && result.status === "OK") {
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
        $('#frm_login .frm_text').removeClass('ok').addClass('error');
    }
}

function gotodashboard() {
    location.href = BASE_URL + '/tableau-de-bord';
}
</script>
