<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Maintenance</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet"  href="asset/css/maintenance.css">
</head>

<body><!-- body will get the class of the page variable -->
	
    <section>
    	
        <div class="body">
        	
            <img src="asset/images/logo.png" width="126" height="46" id="logo" alt="OPSEARCH">
            <h1>OPSEARCH</h1>
            <h2>Maintenance en cours</h2>
            <h3>Veuillez nous excuser pour ce desagrement</h3>
            
        </div><!-- /body -->
        
        
    </section><!-- /body_wrapper -->    
    
</body>
</html>


