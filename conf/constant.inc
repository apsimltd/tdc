<?php

// debug true or false based on dev env
define('DEBUG', isDev() ? true : false);

// maintenance
define('MAINTENANCE', FALSE); // TRUE OR FALSE

define('WATCH', TRUE);

define('DEMO', TRUE);

// airport
// BASE URL vhost config: SetEnv BASE_URL http://opsearch.dev
//defined('BASE_URL') || define('BASE_URL', (getenv('BASE_URL') ? getenv('BASE_URL') : 'https://www.opsearch.info'));
//defined('BASE_URL') || define('BASE_URL', 'http://localhost/opsearch');
defined('BASE_URL') || define('BASE_URL', 'http://localhost/opsearch');

// document root absolute path
//define('BASE_PATH', isDev() ? (isHome() ? 'C:/wamp64/www/opsearch' : 'C:/wamp64/www/archives/opsearch')  : '/var/www/html/opsearch.info/public_html');

define('BASE_PATH', '/var/www/html/opsearch');
define('CSS_URL', BASE_URL . '/asset/css');
define('JS_URL', BASE_URL . '/asset/js');
define('IMAGE_URL', BASE_URL . '/asset/images');
define('AJAX_HANDLER', BASE_URL . '/ajaxhandler');
define('AJAX_UI', BASE_URL . '/ajaxui');

define('SLOGAN', 'RECRUTEMENT PAR APPROCHE DIRECTE - INTELLIGENCE RH');

// files
define('ENTREPRISE_PATH', BASE_PATH . '/uploads/entreprises');
define('ENTREPRISE_URL', BASE_URL . '/uploads/entreprises');
define('CANDIDAT_PATH', BASE_PATH . '/uploads/candidats');
define('CANDIDAT_URL', BASE_URL . '/uploads/candidats');
define('MISSION_PATH', BASE_PATH . '/uploads/missions');
define('MISSION_URL', BASE_URL . '/uploads/missions');
define('PDF_URL', BASE_PATH . '/uploads/pdf');
define('CDR_PATH', BASE_PATH . '/3cx/cdrs');
define('RH_PROFILES_PATH', BASE_PATH . '/uploads/rh/profiles');
define('RH_PROFILES_URL', BASE_URL . '/uploads/rh/profiles');
define('RH_EDF_PATH', BASE_PATH . '/uploads/rh/edf');
define('RH_EDF_URL', BASE_URL . '/uploads/rh/edf');
define('RH_CV_PATH', BASE_PATH . '/uploads/rh/cv');
define('RH_CV_URL', BASE_URL . '/uploads/rh/cv');

// libraries
define('LIBRARIES', BASE_PATH . '/libraries');

// variables
define('MAIL_NOREPLY', 'noreply@opsearch.com');
define('MAIL_DEV', 'faardeen@localmail.com');
define('EVENT_START_TIME', '00:01');
define('EVENT_END_TIME', '23:59');

// error codes
$error_codes = array(
    'C001' => 'Il existe un candidat avec le même email.',
    'C002' => 'Il existe une entreprise avec le même nom.'
);

/**
 * mail: noreply@opsearch.com
 * username: noreply@opsearch.com
 * Pass: Kop10996 / Kop10996
 * 
 * POP
 * serveur: outlook.office365.com
 * Port: 995
 * TLS
 * 
 * IMAP
 * serveur: outlook.office365.com
 * Port: 993
 * TLS
 * 
 * SMTP
 * serveur: smtp.office365.com
 * Port: 587
 * STARTTLS
 */
