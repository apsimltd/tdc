<?php 

function Username($user) {
    return mb_ucfirst($user['firstName']) . ' ' .  mb_strtoupper($user['lastName']);
}

function GenerateInvoiceNumber($num, $prefix = null, $pad_length = 7) {
    if ($pad_length <= strlen($num)) {
        // error $pad_length cannot be less than or equal to length of $num
        //@todo must increase the $pad_length dynamically
    }
    
    if (is_string($prefix)) {
        return sprintf("%s%s", $prefix, str_pad($num, $pad_length, "0", STR_PAD_LEFT));
    }
    
    return str_pad($num, $pad_length, "0", STR_PAD_LEFT);
    
}

/**
 * Calculate width of a rectangle based on number of digits
 * @param $num
 * @return float|int
 */
function rectw($num) {
    return 10*(1+round(log10($num), 0, PHP_ROUND_HALF_UP));
}

function statut($status) {
    if ($status == 1) {
        return "Actif";
    } else {
        return "Inactif";
    }
}

//function isJSON($string){
//   return is_string($string) && is_array(json_decode($string, true)) ? true : false;
//}

/**
 * <p>Requires PHP 5.3 or greater</p>
 * @param type $string
 * @return type
 */
function isJSON($string){
   return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

function setNotifCookie($notif) {
    if (is_array($notif)) {
        if (isset($notif['status'])) 
            setcookie ('status', $notif['status'], 0, '/');
        if (isset($notif['msg']))
            setcookie ('msg', $notif['msg'], 0, '/');
        if (isset($notif['type']))
            setcookie ('type', $notif['type'], 0, '/');
        if (isset($notif['callback']))
            setcookie ('callback', $notif['callback'], 0, '/');
        if (isset($notif['param']))
            setcookie ('param', $notif['param'], 0, '/');
    }    
}

function seconds_from_time($time) {
    // 00:03:21
    
    $data = explode(':', $time);
    if (is_array($data)) {
        /// list($h, $m, $s) = $data;
        
        $seconds = 0;
        
        if (isset($data[0])) {
            $seconds += (intval($data[0]) * 3600);
        }
        
        if (isset($data[1])) {
            $seconds += (intval($data[1]) * 60);
        }
        
        if (isset($data[2])) {
            $seconds += $data[2];
        }
        
        return $seconds;
        
        // return ($h * 3600) + ($m * 60) + $s;
    } else {
        return '0';
    }
            
}

function time_from_seconds($seconds) {
	$h = floor($seconds / 3600);
	$m = floor(($seconds % 3600) / 60);
	$s = $seconds - ($h * 3600) - ($m * 60);
	return sprintf('%02d:%02d:%02d', $h, $m, $s);
}

function seconds_to_time($seconds) {
        
    if ($seconds == 0) {
        return 0;
    }

    $dt1 = new DateTime("@0");
    $dt2 = new DateTime("@$seconds");
    // return $dt1->diff($dt2)->format('%a jours, %h heures, %i minutes et %s secondes');
    return $dt1->diff($dt2)->format('%a j, %h h, %i m et %s s');
}
    
function seconds_to_hours_minutes ($seconds) {
    if ($seconds == 0) {
        return 0;
    }

    $dt1 = new DateTime("@0");
    $dt2 = new DateTime("@$seconds");
    return $dt1->diff($dt2)->format('%a jours, %h heures, %i minutes et %s secondes');
    //return $dt1->diff($dt2)->format('%a:%h:%i:%s');
}

function seconds_to_hours_minutes_noS ($seconds) {
    if ($seconds == 0) {
        return 0;
    }

    $dt1 = new DateTime("@0");
    $dt2 = new DateTime("@$seconds");
    return $dt1->diff($dt2)->format('%a J, %h H, %i M');
    //return $dt1->diff($dt2)->format('%a:%h:%i:%s');
}

function seconds_to_days ($seconds) {
    if ($seconds == 0) {
        return 0;
    }

    $dt1 = new DateTime("@0");
    $dt2 = new DateTime("@$seconds");
    //return $dt1->diff($dt2)->format('%a jours, %h heures, %i minutes et %s secondes');
    return $dt1->diff($dt2)->format('%a Jours');
}

function seconds_to_hours_minutes_f ($seconds) {
    if ($seconds == 0) {
        return 0;
    }

    $dt1 = new DateTime("@0");
    $dt2 = new DateTime("@$seconds");
    //return $dt1->diff($dt2)->format('%a jours, %h heures, %i minutes et %s secondes');
    return $dt1->diff($dt2)->format('%a J, %h H, %i M, %s S');
}

function dateYYMMDD($date) {
    return date('Y-m-d', strtotime($date));
}

function dateToDb ($timestamp = NULL) {
    
    $str = 'Y-m-d H:i:s';
    
    if (!$timestamp) {
        return date($str, time());
    } else {
        return date($str, $timestamp);
    } 
    
}

function AddSpaceTelFR($str) {
    $tels = str_split($str);
    $new_tel = "";
    $count = 0;
    foreach($tels as $tel) :
        if ($count%2 == 0) {
            $new_tel .= ' ' . $tel;
        } else {
            $new_tel .= $tel;
        }
        $count++;
    endforeach;
    
    return $new_tel;
}

function formatTelFR($str) {
    return preg_replace('/\s/', '', $str);
}

function debug($str, $die = false) {
    echo "<pre>";
    print_r($str);
    echo "</pre>";
    if ($die) 
        die();
}

function getUserIp() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

/**
 * <p>return true if number is even else return false means number is odd</p>
 * @author faardeen
 * @param integer $num
 * @return boolean true/false
 */
function isEven($num) {
    if ($num % 2 == 0)
        return TRUE;
    else
        return FALSE;
}

/**
 * <p>Generate Password</p>
 * @return type
 */
function generatePassword() {
    $i_taille = 8;
    $b_valid = false;
    $characts = '1234567890abcdefghijkmnopqrstuvwxyz1234567890ABCDEFGHJKLMNPQRSUVWXYZ1234567890';
    while (!$b_valid) {
        $mdp = '';
        for ($i = 0; $i < $i_taille; $i++) {
            $mdp .= substr($characts, rand(0, strlen($characts) - 1), 1);
        }
        if (!preg_match('/[A-Z]/', $mdp) && !preg_match('/[a-z]/', $mdp) && !preg_match('/[0-9]/', $mdp)) {
            $b_valid = false;
        } else {
            $pattern = "/[0-9]/";
            $mdpSansChiffre = preg_replace($pattern, '', (string) $mdp);
            if (ctype_upper($mdpSansChiffre)) {
                $b_valid = false;
            } else {
                if (ctype_lower($mdpSansChiffre)) {
                    $b_valid = false;
                } else {
                    $b_valid = true;
                }
            }
        }
    }
    return $mdp;
}

/**
 * <p>Check if parameters have been sent using the POST method</p>
 * @return boolean
 */
function isPost() {
    return (strtolower($_SERVER['REQUEST_METHOD']) === "post") ? TRUE : FALSE;
}

/**
 * <p>Check if parameters have been sent using the GET method</p>
 * @return boolean
 */
function isGet() {
    return (strtolower($_SERVER['REQUEST_METHOD']) === "get") ? TRUE : FALSE;
}

/**
 * <p>Function fetch the parameters sent using the post method. (if not sent '' is returned)</p>
 * @param String $name
 * @return String
 */
function getPost($name = '', $default = '') {
    if (isPost() && $name == '')
        return $_POST;
    return (isset($_POST[$name])) ? $_POST[$name] : $default;
}

/**
 * <p>Function fetch the parameters sent using the get method. (if not sent '' is returned)</p>
 * @param String $name
 * @return String
 */
function get($name = '', $default = '') {
    if (isGet() && $name == '')
        return $_GET;
    return isset($_GET[$name]) ? $_GET[$name] : $default;
}

/**
 * <p>Function fetch the parameters sent using the get method. (if not sent '' is returned)</p>
 * @param String $name
 * @return String
 */
function getRequest($name = '', $default = '') {
    if ($name == '')
        return $_REQUEST;
    return isset($_REQUEST[$name]) ? $_REQUEST[$name] : $default;
}

/**
 * 
 * @param String $email
 * @return boolean
 */
function isValidEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

/**
 * <p> parameter is a date in any format and return a date like dd-mm-yyyy </p>
 * @param String or int $date
 * @return String
 */
function dateToFr($timestamp = NULL, $showTime = false) { 
    
    if (validateDate($timestamp, 'Y-m-d H:i:s') || validateDate($timestamp, 'Y-m-d')) { // if the string is already in a well date format yyy-mm-dd hh:mm:ss
        return $timestamp;
    }
    
    if ($showTime) {
        $str = 'd-m-Y H:i:s';
    } else {
        // $str = 'd-m-Y';
        $str = 'Y-m-d';
    }
    
    if (!$timestamp) {
        return date($str, time());
    } else {
        return date($str, $timestamp);
    } 
}

/**
 * @src https://www.php.net/manual/en/function.checkdate.php
 * @param type $date
 * @param type $format
 * @return type
 */
function validateDate($date, $format = 'Y-m-d H:i:s') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

/**
 * <p>Check whether a url has a query string or not</p>
 * @param type $url
 * @return boolean true/false
 */
function urlHasParam($url) {
    $url_parts = parse_url($url);
    if ($url_parts['scheme'] == 'http' && array_key_exists('query', $url_parts)) {
        return true;
    } else {
        return false;
    }
}

function mb_ucfirst($str) {
    $fc = mb_strtoupper(mb_substr($str, 0, 1));
    return $fc.mb_substr($str, 1);
}

function getNearest60MinIntervalTime() {
    return date('H:i', ceil(time() / (60 * 60)) * (60 * 60));
}

/**
 * 
 * @param Int $dateofbirth unix timestamp
 */
function getAge($dateofbirth) {
    $difference = time() - $dateofbirth;
    return floor($difference / 31556926); //There are 31556926 seconds in a year.
}


function DateOfBirth($date) {
    $difference = time() - strtotime($date);
    return floor($difference / 31556926); //There are 31556926 seconds in a year.
}

/**
 * 
 * @param Int $age
 * @return int unix timestamp
 */
function getDateOfBirth($age) {
    return time() - (31556926 * $age); //There are 31556926 seconds in a year.
}

/**
 * 
 * @param String $url
 * @return boolean
 */
function checkLink($url) {
    
//    $timeout = 10;
//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, $url);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
//    $http_respond = curl_exec($ch);
//    $http_respond = trim(strip_tags($http_respond));
//    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//        
//    curl_close($ch);
//    
//    if ($http_code == "200" || $http_code == "302") {
//        return true;
//    } else {
//        return false;
//    }
    
    // @source http://urlregex.com/
    if (filter_var($url, FILTER_VALIDATE_URL) !== false) {
        return true;
    } else {
        return false;
    }
        
}

function getFileExt($filename) {
    $info = pathinfo($filename);
    if (isset($info['extension'])) {
       return $info['extension']; 
    } else {
        return "-";
    }
}

function getContentType($ext) {
    
    if ($ext == 'doc' || $ext == 'docx') {
        return "application/doc";
    } elseif ($ext == 'xls' || $ext == 'xlsx') {
        return "application/xls";
    } elseif ($ext == 'pdf') {
        return "application/pdf";
    } elseif ($ext == 'zip') {
        return "application/zip";
    } elseif ($ext == 'xml') {
        return "text/xml";
    } elseif ($ext == 'mp3' || $ext == "wave") {
        return "octet/stream";
    }
    
}

function build_query_like_OR($data, $field_name) {
    $str = " ( ";
    $numItems = count($data);
    $count = 1;
    foreach ($data as $key => $value):
        if ($count == $numItems) {
            $str .= $field_name . " LIKE '%" . $value . "%' ) ";
        } else {
            $str .= $field_name . " LIKE '%" . $value . "%' OR ";
        }
        $count++;
    endforeach;
    return $str;
}

function addSeperatorClosure($data, $sep = ';', $closure = "'") {
    $str = '';
    $numItems = count($data);
    $count = 1;
    foreach ($data as $key => $value):
        if ($count == $numItems) {
            $str .= $closure . $value . $closure;
        } else {
            $str .= $closure . $value . $closure . $sep;
        }
        $count++;
    endforeach;
    return $str;
}

function addSeperator($data, $sep = ';') {
    $str = '';
    $numItems = count($data);
    $count = 1;
    foreach ($data as $key => $value):
        if ($count == $numItems) {
            $str .= $value;
        } else {
            $str .= $value . $sep;
        }
        $count++;
    endforeach;
    return $str;
}

function addSep($data) {
    $str = '';
    $numItems = count($data);
    $count = 1;
    foreach ($data as $key => $value):
        if ($count == $numItems) {
            $str .= $value;
        } else {
            $str .= $value . '|';
        }
        $count++;
    endforeach;
    return $str;
}

function getClassStatusMission($status_id) {
    if ($status_id == 230) {
        return 'placement';
    } else if ($status_id == 231) {
        return 'cloture';
    } else if ($status_id == 232) {
        return 'en_cours';
    } else if ($status_id == 233) {
        return 'stopped';
    } else if ($status_id == 235) {
        return 'stopped';
    } else if ($status_id == 250) {
        return 'stopped';
    }
}

function getStatusTDCColorsEntreprise ($status_id, $colorsTDC) {
    if ($status_id == 257) { // OUT
        $bgStatus = '#' . $colorsTDC[17]['bg_hex'];
        $fontStatus = '#' . $colorsTDC[17]['font_hex'];
    } else if ($status_id == 258) { // FINI
        $bgStatus = '#' . $colorsTDC[16]['bg_hex'];
        $fontStatus = '#' . $colorsTDC[16]['font_hex'];
    }
    
    return array('bgColor' => $bgStatus, 'fontColor' => $fontStatus);
}

function getStatusTDCColors($status_id, $colorsTDC, $place, $sl) {
    if ($status_id == 226) { // OUT
        $bgStatus = '#' . $colorsTDC[9]['bg_hex'];
        $fontStatus = '#' . $colorsTDC[9]['font_hex'];
    } else if ($status_id == 227) { // BU-
        $bgStatus = '#' . $colorsTDC[7]['bg_hex'];
        $fontStatus = '#' . $colorsTDC[7]['font_hex'];
    } else if ($status_id == 228) { // BU+
        $bgStatus = '#' . $colorsTDC[6]['bg_hex'];
        $fontStatus = '#' . $colorsTDC[6]['font_hex'];
    } else if ($status_id == 229) { // IN
        
        if ($place == 1 && $sl == 1) {
            $bgStatus = '#' . $colorsTDC[0]['bg_hex'];
            $fontStatus = '#' . $colorsTDC[0]['font_hex']; 
        } else if ($place == 1 && $sl == 0) {
            $bgStatus = '#' . $colorsTDC[1]['bg_hex'];
            $fontStatus = '#' . $colorsTDC[1]['font_hex'];  
        } else if ($place == 0 && $sl == 1) {
            $bgStatus = '#' . $colorsTDC[2]['bg_hex'];
            $fontStatus = '#' . $colorsTDC[2]['font_hex']; 
        } else if ($place == 0 && $sl == 0) {
            $bgStatus = '#' . $colorsTDC[3]['bg_hex'];
            $fontStatus = '#' . $colorsTDC[3]['font_hex']; 
        }
    } else if ($status_id == 251) { // ARC
        $bgStatus = '#' . $colorsTDC[4]['bg_hex'];
        $fontStatus = '#' . $colorsTDC[4]['font_hex'];
    } else if ($status_id == 252) { // ARCA
        $bgStatus = '#' . $colorsTDC[5]['bg_hex'];
        $fontStatus = '#' . $colorsTDC[5]['font_hex'];
    } else if ($status_id == 259) { // Ex BU
        $bgStatus = '#' . $colorsTDC[8]['bg_hex'];
        $fontStatus = '#' . $colorsTDC[8]['font_hex'];
    }
    
    return array('bgColor' => $bgStatus, 'fontColor' => $fontStatus);
}

function getClassStatusTDC($status_id) {
    if ($status_id == 229) {
        return 'in';
    } else if ($status_id == 226) {
        return 'out';
    } else if ($status_id == 228) {
        return 'buplus';
    } else if ($status_id == 227) {
        return 'buminus';
    } else if ($status_id == 251) {
        return 'arc';
    } else if ($status_id == 252) {
        return 'arca';
    } else if ($status_id == 259) {
        return 'exbu';
    }
}

function buildSearchQueryString() {
    return '?type=' . get('type') . '&value=' . get('value') .'&filter=all';
}

/**
 * <p>Copy a folder and all its content</p>
 * @param type $src
 * @param type $dst
 */
function recurse_copy($src, $dst) { 
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
                copy($src . '/' . $file,$dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 
}

function filterCharEscape($str) {
    $invalidCharacters = array('"');
    $validCharacters = array(' ');
    return str_replace($invalidCharacters, $validCharacters, $str);
}

function filterChar($str) {
    $invalidCharacters = array('à','á','â','ã','ä', 'ç', 'è','é','ê','ë', 'ì','í',
        'î','ï', 'ñ', 'ò','ó','ô','õ','ö', 'ù','ú','û','ü', 'ý','ÿ', 'À','Á','Â','Ã',
        'Ä', 'Ç', 'È','É','Ê','Ë', 'Ì','Í','Î','Ï', 'Ñ', 'Ò','Ó','Ô','Õ','Ö', 'Ù','Ú',
        'Û','Ü', 'Ý', '’', '/', '&', '+');
    $validCharacters = array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i',
        'i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A',
        'A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U',
        'U','U', 'Y', ' ', ' ', ' ', ' ');

    return str_replace($invalidCharacters, $validCharacters, $str);
}

function filterCompanyName($str) {
    $invalidCharacters = array('à','á','â','ã','ä', 'ç', 'è','é','ê','ë', 'ì','í',
        'î','ï', 'ñ', 'ò','ó','ô','õ','ö', 'ù','ú','û','ü', 'ý','ÿ', 'À','Á','Â','Ã',
        'Ä', 'Ç', 'È','É','Ê','Ë', 'Ì','Í','Î','Ï', 'Ñ', 'Ò','Ó','Ô','Õ','Ö', 'Ù','Ú',
        'Û','Ü', 'Ý', '’', '/', '+', "\\", "(", ")");
    $validCharacters = array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i',
        'i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A',
        'A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U',
        'U','U', 'Y', ' ', ' ', ' ', ' ', ' ');

    return str_replace($invalidCharacters, $validCharacters, $str);
}

/**
 * <p>To get the number of business of days between 2 dates. It excludes Saturdays and Sundays. It can also excludes public holidays and we passes it though the 3rd param</p>
 * https://gist.github.com/quawn/8503445
 * modified by faardeen 
 * @param String $startDate yy-mm-dd e.g. 2020-08-01 
 * @param String $endDate yy-mm-dd e.g. 2020-08-31
 * @param Array $holidays e.g. $holidays = array("2008-12-25","2008-12-26","2009-01-01");
 * @return int
 */
function getBusinessDays($startDate, $endDate, $holidays = null) {
    
    // do strtotime calculations just once
    $endDate = strtotime($endDate);
    $startDate = strtotime($startDate);

    //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    //We add one to inlude both dates in the interval.
    $days = ($endDate - $startDate) / 86400 + 1;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N", $startDate);
    $the_last_day_of_week = date("N", $endDate);

    //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week <= $the_last_day_of_week) {
        
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        
    } else {
        // (edit by Tokes to fix an edge case where the start day was a Sunday
        // and the end day was NOT a Saturday)

        // the day of the week for start is later than the day of the week for end
        if ($the_first_day_of_week == 7) {
            // if the start date is a Sunday, then we definitely subtract 1 day
            $no_remaining_days--;

            if ($the_last_day_of_week == 6) {
                // if the end date is a Saturday, then we subtract another day
                $no_remaining_days--;
            }
            
        } else {
            // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
            // so we skip an entire weekend and subtract 2 days
            $no_remaining_days -= 2;
        }
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
    $workingDays = $no_full_weeks * 5;
    if ($no_remaining_days > 0 ) {
      $workingDays += $no_remaining_days;
    }

    //We subtract the holidays
    
    if ($holidays && is_array($holidays)) {
        
        foreach($holidays as $holiday){
            $time_stamp=strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                $workingDays--;
        }
        
    }
    
    return $workingDays;
}

function pdf_status_text($status) {
    if ($status == 227 || $status == 228 || $status == 252) {
        return "BU";
    } else if ($status == 229 || $status == 251) {
        return "IN";
    } else if ($status == 226 || $status == 259) {
        return "OUT";
    } else {
        return "";
    }
}

?>
