<?php
function getClass ($className) {
    $classes = array(
        'Role' => 'Role',
        'Action' => 'Action',
        'User' => 'User',
        'Mail' => 'Mail',
        'Map' => 'Map',
        'Migration' => 'Migration',
        'Control' => 'Control',
        'Client' => 'Client',
        'Event' => 'Event',
        'Task' => 'Task',
        'Candidat' => 'Candidat',
        'Mission' => 'Mission',
        'TDC' => 'TDC',
        'Dashboard' => 'Dashboard',
        'TroisCX' => 'TroisCX',
        'SMS' => 'SMS',
        'Watchdog' => 'Watchdog',
        'KPI' => 'KPI',
        'Ringover' => 'Ringover',
    );
    
    if (array_key_exists($className, $classes)) 
        return $classes[$className];
    else 
        return null;
}

spl_autoload_register(function ($class_name) {
    $class_path = getClass($class_name);
    if (is_string($class_path) && !is_null($class_path))
        require_once BASE_PATH . '/classes/'. $class_path .'.php';
    else 
        throw new Exception("Class $class_name does not exist");
});