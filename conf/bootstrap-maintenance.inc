<?php
// environment
// dev or prod
// dev defined in virtual host setting
// see README in root folder of the project
defined('ENV') || define('ENV', (getenv('ENV') ? getenv('ENV') : 'prod'));
defined('OFFICE') || define('OFFICE', (getenv('OFFICE') ? getenv('OFFICE') : 'home'));

/**
 * <p>Check whether the environment is dev or not else it is prod</p>
 * @return boolean true/false
 */
function isDev () {
    if (ENV == "dev")
        return true;
    else
        return false;
}

function isHome () {
    if (OFFICE == "home")
        return true;
    else
        return false;
}

// debug true/false
$debug = false;

// time zone
date_default_timezone_set('Europe/Paris');

require_once __DIR__ . '/constant.inc';
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/helpers.inc';
require_once __DIR__ . '/classes.autoload.php';

/**
 * <p>Check the maintenance mode</p>
 * @return boolean true/false
 */
function isMaintenance () {
    if (MAINTENANCE)
        return true;
    else
        return false;
}

/**
 * <p>Check if it is demo or prod</p>
 */
function isDemo () {
    if (DEMO) {
        return true;
    } else {
        return false;
    }
}

//if (isMaintenance()) {
//    header("Location:" . BASE_URL . "/maintenance");
//} 

// initialize database connections
$dbConfig = new \Doctrine\DBAL\Configuration();
$dbParams = array(
    'dbname'   => isDev() ? 'opsearch' : 'opsearch', 
    'user'     => isDev() ? 'root' : 'caimps', 
    'password' => isDev() ? 'caimps' : '181285cAiMpS',
    'host'     => isDev() ? (isHome() ? '192.168.14.128' : '192.168.247.129') : '151.80.21.111',
    'driver'   => isDev() ? 'pdo_mysql' : 'pdo_mysql',
    'charset'  => 'utf8',
);


$con = \Doctrine\DBAL\DriverManager::getConnection($dbParams, $dbConfig);

// intialize page variable to blank for authentication redirection
$page_title = "";
$page = "";

/**
 * start the session for user access control
 */
session_start();
if (User::isLoggedIn()) {
    $me = $_SESSION['opsearch_user'];
} else {
    $me = array('id' => 0);
}
