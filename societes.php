<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Société Candidats" ?>
<?php $page = "societe_candidats"; ?>
<?php if (!User::isLoggedIn()) header("Location: " . BASE_URL . "/connexion")?>
<?php require_once './views/head.php'; ?>
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base">

        <div id="contentWrap">
        
			<section class="title block">

				<h1 class="pull-left">Blacklistés / Deblacklistés</h1>
								
			</section>
            <?php require_once './views/societe-search.php'; ?>
            
            <div class="societes-listing">
                <?php $clients = Client::getClientsBlacklister(); ?>
                <div class="row">

                    <div class="col col-12">
                        <div class="block nopadding">

                            <div class="block-head with-border">
                                <header><span class="glyphicon glyphicon-home"></span>Sociétés Blacklistés</header>
                            </div>

                            <div class="block-body">

                                <table class="datable stripe hover">
                                    <thead>
                                        <tr>
                                            <th>Ref</th>
                                            <th>Nom société</th>
                                            <?php ///if (User::can('edit_client')): ?>
<!--                                            <th>Blacklister / Deblacklister</th>-->
                                            <?php ///endif; ?>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php foreach($clients as $com): ?>
                                        <tr>
                                            <td align="center"><?php echo $com['id'] ?></td>
                                            <td><?php echo $com['nom'] ?></td>
<!--                                            <td align="center">
                                                <div data-client_id="<?php ///echo $com['id'] ?>" class="tick <?php ///if (User::can('edit_client')): ?>client_blacklist<?php ///endif; ?> <?php ///if ($com['blacklist'] == 1): ?>active<?php ///endif; ?>"></div>
                                            </td>-->
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>

                            </div><!-- / block-body -->

                        </div>
                    </div><!-- /col -->

                </div><!-- / row -->
                
            </div><!-- /societes-listing -->
        
        </div><!-- /contentWrap -->
        
    </div><!-- /base -->
    
	<?php require_once './views/debug.php'; ?>
    
</body>
</html>
<script>
$(document).ready(function(){
    
//    $(document).on('click', '.client_blacklist', function(){
//        var client_id = $(this).data('client_id');
//        var me = $(this);
//        var op = 'select';
//        if (me.hasClass('active')) {
//            op = 'deselect';
//        } else {
//            op = 'select';
//        }
//        
//        var url = AJAX_HANDLER+'/client-blacklist?id='+client_id+'&op='+op;
//    
//        $.ajax({
//            url: url,
//            dataType : 'json',
//            cache: false,
//            success: function (result, status) {
//                if (op == 'select') {
//                    me.addClass('active');
//                } else {
//                    me.removeClass('active');
//                }
//                if (status === "success" && result.status === "OK") {
//                    Notif.Show(result.msg, result.type, true, 5000);
//                } else if (status === "success" && result.status === "NOK") {
//                    Notif.Show(result.msg, result.type, true, 5000);
//                }
//
//            }
//        });
//        
//    });
    
    
//    $(document).on('click', '.societe_blacklist', function(){
//        var company_id = $(this).data('company_id');
//        var me = $(this);
//        var op = 'select';
//        if (me.hasClass('active')) {
//            op = 'deselect';
//        } else {
//            op = 'select';
//        }
//        
//        var url = AJAX_HANDLER+'/company-blacklist?id='+company_id+'&op='+op;
//    
//        $.ajax({
//            url: url,
//            dataType : 'json',
//            cache: false,
//            success: function (result, status) {
//                if (op == 'select') {
//                    me.addClass('active');
//                } else {
//                    me.removeClass('active');
//                }
//                if (status === "success" && result.status === "OK") {
//                    Notif.Show(result.msg, result.type, true, 5000);
//                } else if (status === "success" && result.status === "NOK") {
//                    Notif.Show(result.msg, result.type, true, 5000);
//                }
//
//            }
//        });
//        
//    });
        
});

function frm_search_societe_ajax_success (result, status) {
    $('.societes-listing').html(result);
    Table.Init();
}

</script>