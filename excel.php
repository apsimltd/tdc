<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if (get('id') && get('id') != "") {
    
    // details of the mission
    $mission = Mission::getMissionDetailsById(get('id'));
    $title = 'TDC_' . $mission['id'] . '_' .  str_replace(' ', '-', $mission['nom_client']) . '_' . str_replace('/', '-', str_replace(' ', '-', $mission['poste']));
    $file_name =  filterChar($title) . '.xlsx';
    
    // headers of the file
    $headers = array('Candidat', 'Poste', 'DPT', 'Age', 'Société', 'Secteur', 'Rémunération / K€', 'Statut', 'CCC', 'Historiques'); 
    
    // Create new Spreadsheet object
    $spreadsheet = new Spreadsheet();
    
    // Set document properties
    $spreadsheet->getProperties()->setCreator('OPSEARCH')
        ->setLastModifiedBy('OPSEARCH')
        ->setTitle($title)
        ->setSubject($title)
        ->setDescription($title)
        ->setKeywords($title)
        ->setCategory($title);

    // set sheet properties
    // note that the sheet title max characters is always 31
    // so we push only the mission id
    $sheetTitle = 'TDC_' . $mission['id'] . '_candidats';
    $sheet = $spreadsheet->getActiveSheet()->setTitle($sheetTitle);
    
    // setting the default style of the workbook
    $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
    
    // set headers
    ///$sheet->setCellValue('A1', 'Hello World !');
    $sheet->fromArray($headers);
    
    // formatting the header cells
    // https://phpspreadsheet.readthedocs.io/en/develop/topics/recipes/#formatting-cells
    $spreadsheet->getActiveSheet()->getStyle('A1:J1')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('D2B9A100');
    $spreadsheet->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true);
    //$spreadsheet->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(HORIZONTAL_CENTER);
    
    
    // get the data of candidates
    $Candidats = TDC::getCandidatesByTDC(get('id'));
    $civilite = Control::getControlListByType(7);
    $dpt = Control::getControlListByType(6);
    $status = Control::getControlListByType(8);
    $secteur = Control::getControlListByType(2);
    
    // loop through the candidates and append the row in the excel sheet
    $startingRowNum = 2; // 1 is already taken by the header 
    foreach($Candidats as $candidat) :
        
        $rowData = array();
        $rowData[] = ($candidat['control_civilite_id'] == 0) ? mb_strtoupper($candidat['nom']) . ' ' . mb_ucfirst($candidat['prenom']) : $civilite[$candidat['control_civilite_id']] . ' ' . mb_strtoupper($candidat['nom']) . ' ' . mb_ucfirst($candidat['prenom']);
        $rowData[] = $candidat['poste'];
        $rowData[] = ($candidat['control_localisation_id'] > 0) ? $dpt[$candidat['control_localisation_id']] : '';
        $rowData[] = ($candidat['dateOfBirth'] != 0) ? getAge($candidat['dateOfBirth']) : '';
        $rowData[] = (($candidat['is_not_in_this_company'] == 1) ? 'EX - ': '') . mb_strtoupper($candidat['societe_actuelle']);
        $rowData[] = ($candidat['control_secteur_id'] > 0) ? $secteur[$candidat['control_secteur_id']] : '';
        
        
        $cand_remuneration = "";
        if ($candidat['remuneration'] > 0) {
            $cand_remuneration .= "En Package salarial (K€) : " . $candidat['remuneration'] . " K€";
        }
        if ($candidat['detail_remuneration'] != "") {
            $cand_remuneration .= "\nDétails primes / variable : " . $candidat['detail_remuneration'];
        }
//        if ($candidat['salaire_package'] != "") {
//            $cand_remuneration .= "\nSalaire Package (K€) : " . $candidat['salaire_package'] . " K€";
//        }
        if ($candidat['salaire_fixe'] != "") {
            $cand_remuneration .= "\nSalaire Fixe (K€) : " . $candidat['salaire_fixe'] . " K€";
        }
//        if ($candidat['sur_combien_de_mois'] != "") {
//            $cand_remuneration .= "\nSur Combien de Mois : " . $candidat['sur_combien_de_mois'];
//        }
        if ($candidat['avantages'] != "") {
            $cand_remuneration .= "\nAvantages en nature : " . $candidat['avantages'];
        }
        if ($candidat['remuneration_souhaitee'] != "") {
            $cand_remuneration .= "\nRémunération Souhaitée : " . $candidat['remuneration_souhaitee'];
        }
        
        $rowData[] = $cand_remuneration;
        
        $rowData[] = ($candidat['control_statut_tdc'] > 0) ? $status[$candidat['control_statut_tdc']] : '';
        $rowData[] = ($candidat['ccc'] == 1) ? 'OUI' : 'NON';
                
        // get the candidate historiques
        $histos = TDC::getHistoriques($mission['id'], $candidat['id']);
        $histo_all = '';
        $count = 1;
        foreach($histos as $histo):
            if ($count > 1) {
                $histo_all .= "\n\n";
            }
            $histo_all .= 'Créé par ' . mb_ucfirst($histo['firstName']) . ' ' . mb_strtoupper($histo['lastName']) . ' ' . dateToFr($histo['created'], true);
            $histo_all .= "\n";
            $histo_all .= str_replace('<br />', '', nl2br($histo['comment']));
            $count++;
        endforeach;
        
        $rowData[] = $histo_all;
        
        $sheet->fromArray($rowData, null, 'A' . $startingRowNum);
        
        // setting bg color of statut
        // according to doc its better to set all at once rather in loop but here can't bypass the loop
        if ($candidat['control_statut_tdc'] == 226) {
            $statut_color =  '7cbd5a';
            $spreadsheet->getActiveSheet()->getStyle('H'.$startingRowNum)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB($statut_color);
        } else if ($candidat['control_statut_tdc'] == 227) {
            $statut_color =  'dd4b39';
            $spreadsheet->getActiveSheet()->getStyle('H'.$startingRowNum)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB($statut_color);
        } else if ($candidat['control_statut_tdc'] == 228) {
            $statut_color =  '2196f3';
            $spreadsheet->getActiveSheet()->getStyle('H'.$startingRowNum)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB($statut_color);
        } else if ($candidat['control_statut_tdc'] == 229) {
            $statut_color =  'ff9800';
            $spreadsheet->getActiveSheet()->getStyle('H'.$startingRowNum)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB($statut_color);
        } else if ($candidat['control_statut_tdc'] == 259) {
            $statut_color =  '906C4A';
            $spreadsheet->getActiveSheet()->getStyle('H'.$startingRowNum)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB($statut_color);
        }
        
        // wrapping text in col J for comment
        $spreadsheet->getActiveSheet()->getStyle('J'.$startingRowNum)->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(50);
        
        $startingRowNum++;
    
    endforeach;   
    
    // wrap text of headers
    // $spreadsheet->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setWrapText(true);
    
    
    // start of entreprise
    
    // add new sheet
    
    $spreadsheet->createSheet();
    
    // debug($spreadsheet->getSheetNames());
    
    // set the new sheet as active
    $spreadsheet->setActiveSheetIndex(1);
    // set the new sheet title
    $sheetTitleEntreprise = 'TDC_' . $mission['id'] . '_entreprises';
    $sheet = $spreadsheet->getActiveSheet()->setTitle($sheetTitleEntreprise);
    
    // headers entreprise
    $headers_entreprise = array('Entreprise', 'Coordonnées', 'Région', 'Email', 'Secteur Activité', 'Sous Secteur Activité', 'Statut', 'Historiques'); 
    
    // set headers
    $sheet->fromArray($headers_entreprise);
    
    // formatting the header cells
    // https://phpspreadsheet.readthedocs.io/en/develop/topics/recipes/#formatting-cells
    $spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('D2B9A100');
    $spreadsheet->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);
    
    // get the data of entreprises
    $entreprises = TDC::getEntreprisesByTDC(get('id'));
    
    // loop through the entreprises and append the row in the excel sheet
    $startingRowNum = 2; // 1 is already taken by the header 
    foreach($entreprises as $entreprise) :
        
        $rowData = array();
        $rowData[] = $entreprise['name'];
        $rowData[] = $entreprise['telephone'];
        $rowData[] = $entreprise['region'];
        $rowData[] = $entreprise['email'];
        $rowData[] = $entreprise['secteur_activite'];
        $rowData[] = $entreprise['sous_secteur_activite'];
        
        $status_ent = "";
        if ($entreprise['status_entreprise'] == 257) {
            $status_ent = "OUT";
        } elseif ($entreprise['status_entreprise'] == 258) {
            $status_ent = "FINI";
        }
        
        $rowData[] = $status_ent;
        
        // get the entreprise historiques
        $histos = TDC::getHistoriquesEntreprise($mission['id'], $entreprise['entreprise_id']);
        $histo_all = '';
        $count = 1;
        foreach($histos as $histo):
            if ($count > 1) {
                $histo_all .= "\n\n";
            }
            $histo_all .= 'Créé par ' . mb_ucfirst($histo['firstName']) . ' ' . mb_strtoupper($histo['lastName']) . ' ' . dateToFr($histo['created'], true);
            $histo_all .= "\n";
            $histo_all .= str_replace('<br />', '', nl2br($histo['comment']));
            $count++;
        endforeach;
        
        $rowData[] = $histo_all;
        
        $sheet->fromArray($rowData, null, 'A' . $startingRowNum);
        
        // setting bg color of statut
        // according to doc its better to set all at once rather in loop but here can't bypass the loop
        if ($entreprise['status_entreprise'] == 257) { // OUT
            $statut_color =  '7cbd5a';
            $spreadsheet->getActiveSheet()->getStyle('G'.$startingRowNum)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB($statut_color);
        }
        
        if ($entreprise['status_entreprise'] == 258) { // FINI
            $statut_color =  'ff9800';
            $spreadsheet->getActiveSheet()->getStyle('G'.$startingRowNum)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB($statut_color);
        }
        
        // wrapping text in col H for comment
        $spreadsheet->getActiveSheet()->getStyle('H'.$startingRowNum)->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(50);
        
        $startingRowNum++;
    
    endforeach;   
    
    
    // end of entreprise
    
    
    // generate the file and save
    $fullpath = PDF_URL . '/' . $file_name;  
    $writer = new Xlsx($spreadsheet);
    $writer->save($fullpath);
       
    // make the file downloadable    
	header("Pragma: public"); // required
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false); // required for certain browsers 
	header("Content-Type: application/xls");
	// change, added quotes to allow spaces in filenames
	header("Content-Disposition: attachment; filename=\"".basename($fullpath)."\";" );
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: ".filesize($fullpath));
	readfile($fullpath);
	exit();
    
} else {
    header('Location:' . BASE_URL);
}
?>