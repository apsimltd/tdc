<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Tableau De Chasse" ?>
<?php $page = "tdc"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('create_mission', 'read_mission', 'edit_mission', 'delete_mission', 'read_all_mission', 'duplicate_mission', 'tdc')) || get('id') == "") header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php if (get('id') <> "" && !Mission::checkMissionExist(get('id'))) header("Location: " . BASE_URL . "/missions")  ?>
<?php $allowed_user_ids = array(301, 481, 1, 5, 502, 537); ?>
<?php if (get('id') == 2198 && !in_array($me['id'], $allowed_user_ids)) header("Location: " . BASE_URL . "/missions"); //this mission only accessible to bastien and sweety pursun and emeline peralta   ?>
<?php if (get('id') == 2027 && !in_array($me['id'], $allowed_user_ids)) header("Location: " . BASE_URL . "/missions"); //this mission only accessible to bastien and sweety pursun and emeline peralta   ?>
<?php require_once './views/head.php'; ?>
<?php if (!isset($_SESSION['tdc_summary'])) { $_SESSION['tdc_summary'] = 'open'; } ?>
<?php $mission = Mission::getMissionDetailsById(get('id')); // debug($mission); ?>
<?php $documents = Mission::getDocuments(get('id')); ?>
<?php $criteres = Mission::getCriteres(get('id')); ?>
<?php $OutCategories = Mission::getOutCategory(get('id')); // debug($OutCategories); ?>
<?php $tdcCandidates = TDC::getCandidatesByTDC(get('id'))?>
<?php $tdcEntreprises = TDC::getEntreprisesByTDC(get('id')); ?>
<?php $colorsTDC = Control::getColorPDF(); // debug($colorsTDC); ?>
<?php // debug($tdcEntreprises); ?>
	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
    <?php ######### START OF STICKY LEGENDE ############ ?>
    <div class="tdc-sticky-legende-wrapper"><div class="sticky-legende"></div></div>
    <?php ######### END OF STICKY LEGENDE ############## ?>

    <?php require_once './views/tdc-stats.php'; ?>
	
	<div id="base" class="TDC" <?php if ($_SESSION['tdc_summary'] == 'close'): ?>style="margin-top: -9px;"<?php endif; ?>>

        <div id="contentWrap">
            
            <?php // debug($tdcCandidates) ?>
            
            <div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
						
                        <div class="block-head with-border wrapper-legende clearfix">
                            <span class="legende">
                                <span class="title">Légende</span>
                                <span class="status" style="background-color:#486e34;">Placé</span>
                                <span class="status" style="background-color:#7cbd5a;">Short-listé</span>
                            </span><!--/ legende -->
                            
                            <?php $filtersColors = Control::getFilterColorTDC(get('id')); // debug($filtersColors) ?>
                            <?php $filtersColorsEntreprise = Control::getFilterColorTDCEntreprise(get('id')); // debug($filtersColors) ?>
                            
                            <?php if (!empty($filtersColors) || !empty($filtersColorsEntreprise) || !empty($OutCategories)): ?>
                            <div class="filter_by_colors_legende">
                                
                                <span class="title">Filtrer par Légende Couleurs ou par Out Catégorie</span>
                                
                                <?php if (!empty($filtersColors)): ?>
                                
                                    <span class="sub-title">Candidats : </span>

                                    <?php foreach($filtersColors as $filtersColor): ?>
                                    <span class="filter_color candidat" style="background-color:#<?php echo $filtersColor['hex'] ?>" data-colorstdc_id="<?php echo $filtersColor['colorstdc_id'] ?>"><?php echo $filtersColor['description'] ?></span>
                                    <?php endforeach; ?>

                                    <input type="hidden" name="filter_colors_TDC_ids" id="filter_colors_TDC_ids" value="">
                                
                                <?php endif; ?>
                                
                                <?php if (!empty($OutCategories)): ?>
                                    <select id="filter_out_categories" name="filter_out_categories" data-validation="val_blank">
                                        <option value="">Choisir Out Categorie</option>
                                        <?php foreach($OutCategories as $out_category): ?>
                                        <option value="<?php echo $out_category['id'] ?>"><?php echo $out_category['out_categorie'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                <?php endif; ?>    
                                    
                                <?php if (!empty($filtersColorsEntreprise)): ?>
                                
                                    <span class="sub-title" style="margin-left: 20px;">Entreprises : </span>

                                    <?php foreach($filtersColorsEntreprise as $filtersColor): ?>
                                    <span class="filter_color entreprise" style="background-color:#<?php echo $filtersColor['hex'] ?>" data-colorstdc_id="<?php echo $filtersColor['colorstdc_id'] ?>"><?php echo $filtersColor['description'] ?></span>
                                    <?php endforeach; ?>

                                    <input type="hidden" name="filter_colors_entreprise_TDC_ids" id="filter_colors_entreprise_TDC_ids" value="">
                                
                                <?php endif; ?>
                                  
                                <span class="filter_color_submit" id="filter_color_submit"><span class="glyphicon glyphicon-filter"></span> Filtrer</span>
                                
                            </div><!-- / filter_by_colors_legende --> 
                            <?php endif; ?>
                            
                        </div><!-- / block-head -->
                                                
						<div class="block-body TDC_TABLE">
							<?php if (!empty($tdcCandidates)): ?>
                            <table class="datable stripe hover">
                                <thead>
                                    <tr>
                                        <th style="width: 40px;" align="center"><div class="brush legend candidat-color tooltips" data-mission_id="<?php echo $mission['id'] ?>" title="Légende de couleurs"></div></th>
<!--										<th>Ref</th>-->
                                        <th>Candidat</th>
                                        <th>Coordonnées</th>
                                        <th>Poste</th>
                                        <th>DPT</th>
                                        <th>Âge</th>
										<th>Société</th>
										<th>Rémunération</th>
										<th>Compétences Spé</th>
                                        <th>Statut</th>
                                        <th>CCC</th>
                                        <th>Out Categorie</th>
                                        <?php if (User::can('tdc_visibilite_client')) :?><th></th><?php endif; ?>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $civilite = Control::getControlListByType(7); ?>
                                    <?php $dpt = Control::getControlListByType(6); ?>
                                    <?php $status = Control::getControlListByType(8); ?>
                                    <?php foreach($tdcCandidates as $tdcCandidat): ?>
                                    <?php 
                                    $css = '';
                                    if ($tdcCandidat['place'] > 0 && $tdcCandidat['shortliste'] == 0) {
                                        $css .= 'border-left:4px solid #486e34;';
                                    }
                                    if ($tdcCandidat['shortliste'] > 0 && $tdcCandidat['place'] == 0) {
                                        $css .= 'border-left:4px solid #7cbd5a;';
                                    }
                                    if ($tdcCandidat['shortliste'] > 0 && $tdcCandidat['place'] > 0) {
                                        $css .= 'border-left:4px solid #486e34;';
                                    }
                                    ?>
                                    <?php 
                                    if ($tdcCandidat['control_civilite_id'] == 0): 
                                        $candidat_name = mb_strtoupper($tdcCandidat['nom']) . ' ' . mb_ucfirst($tdcCandidat['prenom']);
                                    else:
                                        $candidat_name = $civilite[$tdcCandidat['control_civilite_id']] . ' ' . mb_strtoupper($tdcCandidat['nom']) . ' ' . mb_ucfirst($tdcCandidat['prenom']);
                                    endif;
                                    ?>
                                    <tr class="tdc_id_<?php echo $tdcCandidat['tdc_id'] ?>" data-tdc_id="<?php echo $tdcCandidat['tdc_id'] ?>">
                                        <td align="center" style="width: 40px;<?php echo $css ?>">
                                            <div class="brush candidat action" data-tdc_id="<?php echo $tdcCandidat['tdc_id'] ?>"></div>
                                            <a name="tdc_<?php echo $tdcCandidat['tdc_id'] ?>" id="row_tdc_<?php echo $tdcCandidat['tdc_id'] ?>" class="anchors"></a>
                                        </td>
<!--                                        <td align="center"><?php //echo $tdcCandidat['candidat_id'] ?></td>-->
                                        <?php 
                                        $style = '';
                                        if ($tdcCandidat['color_candidat_bg'] > 0 && $tdcCandidat['color_candidat_label'] > 0){
                                            $style = 'style="position:relative;background-color: ' . Control::getColorTDCById($tdcCandidat['color_candidat_bg']) . ';color:'. Control::getColorTDCById($tdcCandidat['color_candidat_label']).';"';
                                        } elseif ($tdcCandidat['color_candidat_bg'] > 0 && $tdcCandidat['color_candidat_label'] == 0) {
                                            $style = 'style="position:relative;background-color: ' . Control::getColorTDCById($tdcCandidat['color_candidat_bg']) . ';"';
                                        } elseif ($tdcCandidat['color_candidat_bg'] == 0 && $tdcCandidat['color_candidat_label'] > 0) {
                                            $style = 'style="position:relative;color: ' . Control::getColorTDCById($tdcCandidat['color_candidat_label']) . ';"';
                                        } else {
                                            $style = 'style="position:relative;"';
                                        }
                                        ?>
                                        <td class="color_candidat_bg" <?php echo $style ?>>
                                            
                                            <?php $cand_docs = json_decode($tdcCandidat['documents'], true); ?>
                                            <?php if (empty($cand_docs)) : $empty = "empty"; else: $empty = ""; endif; ?>
                                            
                                            <span class="color_candidat_label">
                                            <?php 
                                            if ($tdcCandidat['control_civilite_id'] == 0): 
                                                echo mb_strtoupper($tdcCandidat['nom']) . ' ' . mb_ucfirst($tdcCandidat['prenom']);
                                            else:
                                                echo $civilite[$tdcCandidat['control_civilite_id']] . ' ' . mb_strtoupper($tdcCandidat['nom']) . ' ' . mb_ucfirst($tdcCandidat['prenom']);
                                            endif;
                                            
                                            // add cv from tdc
                                            echo '<span class="glyphicon glyphicon-plus add-cv-from-tdc tooltips" data-tdc_line_id="'.$tdcCandidat['tdc_id'].'" data-candidat_id="'.$tdcCandidat['candidat_id'].'" data-mission_id="'.$tdcCandidat['mission_id'].'" title="Ajouter CV"></span>';
                                            
                                            if ($tdcCandidat['cv_fourni'] == 1):
                                                echo "<br>";
                                                echo '(CV Fourni) <span data-candidat_name="'.$candidat_name.'" class="tdc_cv_fourni '.$empty.'"><i class="fa fa-file-pdf"></i></span>';
                                            endif;
                                            ?>
                                            </span>
                                            
                                            <div class="tdc_cv_display">
                                                
                                                <h2>Documents Candidat</h2>
                                                
                                                <?php if (!empty($cand_docs)) :?>
                                                <div class="tdc_cv_wrapper">
                                                    
                                                    <ul class="links_candidat">
                                                    <?php foreach($cand_docs as $docCan): ?>
                                                        <?php 
                                                        $imgType = array('jpg', 'jpeg', 'png');
                                                        $ext = getFileExt($docCan['file']);
                                                        if (in_array($ext, $imgType)):
                                                        ?>
                                                        <li>
                                                            <a href="<?php echo CANDIDAT_URL ?>/<?php echo $tdcCandidat['id'] ?>/<?php echo $docCan['file'] ?>" data-lightbox="<?php echo $docCan['file'] ?>"><?php echo $docCan['file'] ?></a>
                                                        </li>
                                                        <?php else: ?>
                                                        <li>
                                                            <a href="<?php echo BASE_URL ?>/download?type=can&id=<?php echo $docCan['id'] ?>"><?php echo $docCan['file'] ?></a>
                                                        </li>
                                                        <?php endif; ?>
                                                    <?php endforeach;?>
                                                    </ul>
                                                    
                                                </div><!-- / tdc_cv_wrapper -->
                                                <?php else: ?>
                                                <div class="message warning">Pas des documents</div>
                                                <?php endif; ?>
                                                
                                            </div><!--/tdc_cv_display -->
                                            
                                            <?php if ($tdcCandidat['gdpr_status'] == 1): ?>
                                                <div class="clearboth notif warning" style="background-color:#ff9800; color: #fff; line-height: 16px; padding: 0px 10px;"><strong>Attention :</strong> Candidat Inactif (RGPD)</div>
                                            <?php endif; ?>
                                            
                                        </td>
                                        <td>
                                            <?php
                                            if ($tdcCandidat['tel_mobile_perso'] != "")
                                                echo 'Mobile perso : ' . $tdcCandidat['tel_mobile_perso'] . '<br>';
                                            if ($tdcCandidat['tel_standard'] != "")
                                                echo 'Std : ' . $tdcCandidat['tel_standard'] . '<br>';
                                            if ($tdcCandidat['tel_ligne_direct'] != "")
                                                echo 'Direct : ' . $tdcCandidat['tel_ligne_direct'] . '<br>';
                                            if ($tdcCandidat['tel_pro'] != "")
                                                echo 'Pro : ' . $tdcCandidat['tel_pro'] . '<br>';
                                            if ($tdcCandidat['tel_domicile'] != "")
                                                echo 'Domicile : ' . $tdcCandidat['tel_domicile'] . '<br>';
                                            if ($tdcCandidat['email'] != "")
                                                echo 'Email 1 : ' . $tdcCandidat['email'] . '<br>';
                                            if ($tdcCandidat['email2'] != "")
                                                echo 'Email 2 : ' . $tdcCandidat['email2'];
                                            ?>
                                        </td>
                                        <td><?php echo $tdcCandidat['poste'] ?></td>
                                        <td>
                                            <?php
                                            if ($tdcCandidat['control_localisation_id'] > 0) {
                                                echo $dpt[$tdcCandidat['control_localisation_id']];
                                            }
                                            ?>
                                        </td>
                                        <td align="center" style="width: 40px;">
                                            <?php
                                            if ($tdcCandidat['dateOfBirth'] != 0) {
                                                echo getAge($tdcCandidat['dateOfBirth']);
                                            }
                                            ?>
                                        </td>
                                        <?php 
                                        $style1 = '';
                                        if ($tdcCandidat['color_societe_bg'] > 0 && $tdcCandidat['color_societe_label'] > 0){
                                            $style1 = 'style="background-color: ' . Control::getColorTDCById($tdcCandidat['color_societe_bg']) . ';color:'. Control::getColorTDCById($tdcCandidat['color_societe_label']).';"';
                                        } elseif ($tdcCandidat['color_societe_bg'] > 0 && $tdcCandidat['color_societe_label'] == 0) {
                                            $style1 = 'style="background-color: ' . Control::getColorTDCById($tdcCandidat['color_societe_bg']) . ';"';
                                        } elseif ($tdcCandidat['color_societe_bg'] == 0 && $tdcCandidat['color_societe_label'] > 0) {
                                            $style1 = 'style="color: ' . Control::getColorTDCById($tdcCandidat['color_societe_label']) . ';"';
                                        }
                                        ?>
                                        <td class="color_societe_bg" <?php echo $style1 ?> data-order="<?php echo ltrim(mb_strtoupper($tdcCandidat['societe_actuelle'])) ?>">
                                            <span class="color_societe_label">
                                                <?php if ($tdcCandidat['is_not_in_this_company'] == 1): ?>EX - <?php endif; ?>
                                                <?php echo mb_strtoupper($tdcCandidat['societe_actuelle']) ?>
                                                <?php if (Candidat::checkCompanyMix($tdcCandidat['societe_actuelle'])) : ?> - [GROUPE MIX]<?php endif; ?>
                                            </span>
                                            <?php if ($tdcCandidat['societe_actuelle'] != "" && Client::getClientCandidatBlacklist($tdcCandidat['societe_actuelle'])): ?>
                                                <div class="clearboth notif warning" style="background-color:#ff9800; color: #fff; line-height: 24px; padding: 0px 10px;"><strong>Attention :</strong> La Société est blacklistée</div>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php
//                                            if ($tdcCandidat['remuneration'] > 0){
//                                                echo $tdcCandidat['remuneration'] . ' K€';
//                                            }
//                                            echo "<br>";
//                                            echo $tdcCandidat['detail_remuneration'];
                                            ?>
                                            
                                            <?php
                                            $cand_remuneration = "";
                                            if ($tdcCandidat['remuneration'] > 0) {
                                                $cand_remuneration .= "En Package salarial (K€) : " . $tdcCandidat['remuneration'] . " K€";
                                            }
                                            if ($tdcCandidat['detail_remuneration'] != "") {
                                                $cand_remuneration .= "<br>Détails primes / variable : " . $tdcCandidat['detail_remuneration'];
                                            }
//                                            if ($tdcCandidat['salaire_package'] != "") {
//                                                $cand_remuneration .= "<br>Salaire Package (K€) : " . $tdcCandidat['salaire_package'] . " K€";
//                                            }
                                            if ($tdcCandidat['salaire_fixe'] != "") {
                                                $cand_remuneration .= "<br>Salaire Fixe (K€) : " . $tdcCandidat['salaire_fixe'] . " K€";
                                            }
//                                            if ($tdcCandidat['sur_combien_de_mois'] != "") {
//                                                $cand_remuneration .= "<br>Sur Combien de Mois : " . $tdcCandidat['sur_combien_de_mois'];
//                                            }
                                            if ($tdcCandidat['avantages'] != "") {
                                                $cand_remuneration .= "<br>Avantages en nature : " . $tdcCandidat['avantages'];
                                            }
                                            if ($tdcCandidat['remuneration_souhaitee'] != "") {
                                                $cand_remuneration .= "<br>Rémunération Souhaitée : " . $tdcCandidat['remuneration_souhaitee'];
                                            }
                                            echo $cand_remuneration;
                                            ?>
                                            
                                        </td>
                                        <td align="center" style="width: 120px;" class="tdc_criteres_spec clickable" data-candidat_id="<?php echo $tdcCandidat['candidat_id'] ?>" data-mission_id="<?php echo $tdcCandidat['mission_id'] ?>" data-tdc_id="<?php echo $tdcCandidat['tdc_id'] ?>">
                                            <?php echo TDC::getCritereSpec($tdcCandidat['candidat_id'], $tdcCandidat['mission_id']) ?>
                                        </td>
                                        <td align="center" style="width: 60px;" class="change_statut_tdc clickable" data-candidat_id="<?php echo $tdcCandidat['candidat_id'] ?>" data-mission_id="<?php echo $tdcCandidat['mission_id'] ?>" data-tdc_id="<?php echo $tdcCandidat['tdc_id'] ?>" data-order="<?php echo $tdcCandidat['control_statut_tdc_order'] ?>">
                                            <?php $suivi = TDC::getSuivi($tdcCandidat['mission_id'], $tdcCandidat['candidat_id']); ?>
                                            <?php if ($tdcCandidat['control_statut_tdc'] > 0): ?>
                                            
                                                <?php $canStatusColor = getStatusTDCColors($tdcCandidat['control_statut_tdc'], $colorsTDC, $suivi['place'], $suivi['shortliste']); ?>
                                            
                                                <span class="status" style="color:<?php echo $canStatusColor['fontColor'] ?>; background-color:<?php echo $canStatusColor['bgColor'] ?>">
                                                    <?php 
                                                    if ($tdcCandidat['control_statut_tdc'] == 229) { // IN
                                                        if ($suivi['place'] == 1 && $suivi['shortliste'] == 1) {
                                                            echo "IN Placé";
                                                        } else if ($suivi['place'] == 1 && $suivi['shortliste'] == 0) {
                                                            echo "IN Placé";
                                                        } else if ($suivi['place'] == 0 && $suivi['shortliste'] == 1) {
                                                            echo "IN SL";
                                                        } else if ($suivi['place'] == 0 && $suivi['shortliste'] == 0) {
                                                            echo $status[$tdcCandidat['control_statut_tdc']];
                                                        }
                                                    } else {
                                                        echo $status[$tdcCandidat['control_statut_tdc']];
                                                    }
                                                    ?>
                                                </span>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center" style="width: 40px;">
                                            <div data-mission_id="<?php echo $tdcCandidat['mission_id'] ?>" data-candidat_id="<?php echo $tdcCandidat['candidat_id'] ?>" class="tick tdc_ccc <?php if ($tdcCandidat['ccc'] > 0): ?>active<?php endif; ?>"></div>
                                        </td>
                                        <td align="center" style="width: 100px;" class="tdc_out_category clickable" data-candidat_id="<?php echo$tdcCandidat['candidat_id'] ?>" data-mission_id="<?php echo $tdcCandidat['mission_id'] ?>" data-tdc_id="<?php echo $tdcCandidat['tdc_id'] ?>">
                                            <?php echo TDC::getOutCategory($tdcCandidat['candidat_id'], $tdcCandidat['mission_id']) ?>
                                        </td>
                                        <?php if (User::can('tdc_visibilite_client')) :?>
                                        <td align="center" style="width: 40px;">
                                            <?php $allowed_status = array(229, 251, 228, 227, 259, 262); ?>
                                            <?php if (in_array($tdcCandidat['control_statut_tdc'], $allowed_status)) { ?>
                                            <div title="Visible sur dashboard client" data-mission_id="<?php echo $tdcCandidat['mission_id'] ?>" data-candidat_id="<?php echo $tdcCandidat['candidat_id'] ?>" class="tooltips tick tdc_visibilite_client <?php if ($tdcCandidat['visibilite_client'] > 0): ?>active<?php endif; ?>"></div>
                                            <?php } ?>
                                        </td>
                                        <?php endif; ?>
                                        <td class="actions_button" style="min-width: 170px;position: relative;">
                                            
                                            <button type="button" data-candidat_id="<?php echo $tdcCandidat['candidat_id'] ?>" data-mission_id="<?php echo $tdcCandidat['mission_id'] ?>" class="tdc_delete_candidate btn btn-icon btn-danger pull-right tooltips" title="Supprimer"><span class="glyphicon glyphicon-trash" style="padding-left: 0px;"></span></button>
                                            
                                            <?php if (User::can('edit_candidat')): ?>
                                            
                                            <?php // candidat comments on hover on edit button ?>
                                            <?php $commentsCandidat = Candidat::getCommentsNew($tdcCandidat['candidat_id']); ?>
                                            
                                            <a style="height: 24.5px;" href="<?php echo BASE_URL ?>/editer-candidat?id=<?php echo $tdcCandidat['candidat_id'] ?>&tdc_id=<?php echo $tdcCandidat['mission_id'] ?>&tdc_line_id=<?php echo $tdcCandidat['tdc_id'] ?>" class="btn btn-icon pull-right btn-info tdc_candidat_comment <?php if (empty($commentsCandidat)): ?>empty<?php endif; ?> tooltips" title="Éditer" data-candidat_name="<?php echo $candidat_name ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                            
                                            
                                            <div class="tdc_candidat_comment_hover">                                                
                                                <?php if (!empty($commentsCandidat)): ?>
                                                <div class="nano">
                                                    <div class="nano-content">
                                                        <div class="historiques_comment_wrap">
                                                            <?php if (!empty($commentsCandidat)): ?>
                                                            <div class="task-threads-wrap clearfix">
                                                                <?php foreach ($commentsCandidat as $com): ?>
                                                                <div class="task-thread clearfix">
                                                                    <p><?php echo nl2br($com['comment']) ?></p>
                                                                    <div class="publish-info">
                                                                        Créé par <strong><?php echo mb_ucfirst($com['firstName']) . ' ' . mb_strtoupper($com['lastName']) ?></strong> le <?php echo dateToFr($com['created'], true) ?>
                                                                    </div>
                                                                    <?php if ($com['mission_id'] <> ""): ?>
                                                                    <div class="publish-info">
                                                                        Mission : <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $com['mission_id'] ?>" target="_blank"><span class="glyphicon glyphicon-screenshot"></span> <?php echo $com['mission'] . ' - ' . $com['client'] ?></a>
                                                                    </div>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <?php endforeach; ?>
                                                            </div><!--/ task-threads-wrap -->
                                                            <?php endif; ?>
                                                        </div><!-- /historiques_comment_wrap -->
                                                    </div><!-- /nano-content-->
                                                </div><!-- /nano -->
                                                <?php else: ?>
                                                <div class="message warning">Aucun commentaire pour ce candidat</div>
                                                <?php endif; ?>
                                                
                                            </div><!-- /tdc_candidat_comment_hover -->
                                            
                                            <?php endif; ?>
                                            
                                            <button type="button" data-mission_id="<?php echo $tdcCandidat['mission_id'] ?>" data-candidat_id="<?php echo $tdcCandidat['candidat_id'] ?>" class="generate_pdf_candidat_tdc pull-right btn btn-icon btn-warning tooltips" title="Générer un fichier PDF"><i class="fa fa-file-pdf"></i></button>
                                            <?php 
                                            // display suivi info on hover                             
                                            $date_prise_de_contact = ($suivi['date_prise_de_contact'] > 0) ? dateToFr($suivi['date_prise_de_contact']) : '';
                                            $date_presentation = ($suivi['date_presentation'] > 0) ? dateToFr($suivi['date_presentation']) : '';
                                            $date_rencontre_consultant = ($suivi['date_rencontre_consultant'] > 0) ? dateToFr($suivi['date_rencontre_consultant']) : '';
                                            $date_rencontre_client = ($suivi['date_rencontre_client'] > 0) ? dateToFr($suivi['date_rencontre_client']) : '';
                                            $date_retour_apres_rencontre = ($suivi['date_retour_apres_rencontre'] > 0) ? dateToFr($suivi['date_retour_apres_rencontre']) : '';
                                            $shortliste = ($suivi['shortliste'] > 0) ? '<span class="status active">Oui</span>' : '<span class="status inactive">Non</span>';
                                            $place = ($suivi['place'] > 0) ? '<span class="status active">Oui</span>' : '<span class="status inactive">Non</span>';
                                            $shortliste_added = ($suivi['shortliste_added'] <> "" && $suivi['shortliste'] > 0) ? $suivi['shortliste_added'] : '';
                                            if($suivi['shortliste_added'] <> "" && $suivi['shortliste'] > 0) {
                                                $shortliste_added_who = TDC::getWhoShortliste($tdcCandidat['mission_id'], $tdcCandidat['candidat_id']);
                                                $shortliste_added_who_formatted = ' - ' . mb_ucfirst($shortliste_added_who['firstName']) . ' ' . mb_strtoupper($shortliste_added_who['lastName']);
                                            } else {
                                                $shortliste_added_who_formatted = "";
                                            }
                                            
                                            $place_added = ($suivi['place_added'] <> "" && $suivi['place'] > 0) ? $suivi['place_added'] : '';
                                            
                                            $suivi_hover = '';
                                            $suivi_hover .= '<table class="dataTable">';
                                                $suivi_hover .= '<tr>';
                                                    $suivi_hover .= '<td>Date de prise de contact</td>';
                                                    $suivi_hover .=  '<td>' . $date_prise_de_contact . '</td>';
                                                $suivi_hover .= '</tr>';
                                                $suivi_hover .= '<tr>';
                                                    $suivi_hover .= '<td>Date de présentation</td>';
                                                    $suivi_hover .=  '<td>' . $date_presentation . '</td>';
                                                $suivi_hover .= '</tr>';
                                                $suivi_hover .= '<tr>';
                                                    $suivi_hover .= '<td>Date de rencontre consultant</td>';
                                                    $suivi_hover .=  '<td>' . $date_rencontre_consultant . '</td>';
                                                $suivi_hover .= '</tr>';
                                                $suivi_hover .= '<tr>';
                                                    $suivi_hover .= '<td>Date de rencontre client</td>';
                                                    $suivi_hover .=  '<td>' . $date_rencontre_client . '</td>';
                                                $suivi_hover .= '</tr>';
                                                $suivi_hover .= '<tr>';
                                                    $suivi_hover .= '<td>Date retour après rencontre</td>';
                                                    $suivi_hover .=  '<td>' . $date_retour_apres_rencontre . '</td>';
                                                $suivi_hover .= '</tr>';
                                                $suivi_hover .= '<tr>';
                                                    $suivi_hover .= '<td>Short-listé</td>';
                                                    $suivi_hover .=  '<td><div class="sl_'.$tdcCandidat['mission_id'].'_'.$tdcCandidat['candidat_id'].'">' . $shortliste .  ' ' . $shortliste_added . $shortliste_added_who_formatted . '</div></td>';
                                                $suivi_hover .= '</tr>';
                                                $suivi_hover .= '<tr>';
                                                    $suivi_hover .= '<td>Placé</td>';
                                                    $suivi_hover .=  '<td>' . $place .  ' ' . $place_added . '</td>';
                                                $suivi_hover .= '</tr>';
                                                $suivi_hover .= '<tr>';
                                                    $suivi_hover .= '<td>Suivi du statut</td>';
                                                    $suivi_hover .=  '<td class="suivi_statut">' . $suivi['comment'] . '</td>';
                                                $suivi_hover .= '</tr>';
                                            $suivi_hover .= '</table>';
                                            ?>
                                            <div class="tdc_suivis_wrap_wrap" style="display:none;">
                                                <div class="tdc_suivis_tooltip_wrap">
                                                    <?php echo $suivi_hover ?>
                                                </div><!-- tdc_suivis_tooltip -->
                                            </div>
                                            <button type="button" data-tdc_id="<?php echo $tdcCandidat['tdc_id'] ?>" data-candidat_id="<?php echo $tdcCandidat['candidat_id'] ?>" data-candidat_name="<?php echo $candidat_name ?>" data-mission_id="<?php echo $tdcCandidat['mission_id'] ?>" data-prestataire_id="<?php echo $mission['prestataire_id'] ?>" class="tdc_suivi_candidate btn btn-icon btn-success pull-right tooltips" title="Suivis"><span class="glyphicon glyphicon-send"></span></button>
                                            
                                            <?php
                                            // historiques hover
                                            $comments = TDC::getHistoriques($tdcCandidat['mission_id'], $tdcCandidat['candidat_id'])
                                            ?>
                                            <div class="comment_tdc_histo">
                                                <div class="historiques_comment_wrap">
                                                    <div class="task-threads-wrap clearfix">
                                                        <?php if (!empty($comments)): ?>
                                                        <?php foreach ($comments as $comment): ?>
                                                        <div class="task-thread clearfix">
                                                            <p><?php echo nl2br($comment['comment']) ?></p>
                                                            <div class="publish-info">
                                                                Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo dateToFr($comment['created'], true) ?>
                                                            </div>
                                                        </div>
                                                        <?php endforeach; ?>
                                                        <?php else: ?>
                                                        <div class="message warning">
                                                            Aucun Historique
                                                        </div>
                                                        <?php endif; ?>
                                                    </div><!--/ task-threads-wrap -->
                                                </div><!-- /historiques_comment_wrap -->    
                                            </div><!-- / comment_tdc_histo -->
                                            
                                            <button type="button" data-candidat_id="<?php echo $tdcCandidat['candidat_id'] ?>" data-mission_id="<?php echo $tdcCandidat['mission_id'] ?>" data-tdc_id="<?php echo $tdcCandidat['tdc_id'] ?>" data-candidat_name="<?php echo $candidat_name ?>" class="tdc_history_candidate btn btn-icon btn-primary pull-right tooltips" title="Historiques"><span class="glyphicon glyphicon-comment"></span></button>
                                            
                                            <button type="button" data-candidat_id="<?php echo $tdcCandidat['candidat_id'] ?>" data-mission_id="<?php echo $tdcCandidat['mission_id'] ?>" class="tdc_sms btn btn-icon btn-success pull-right tooltips" title="SMS"><span class="glyphicon glyphicon-envelope"></span></button>
                                            
                                        </td>
                                    </tr>
									<?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php else: ?>
                            <div class="message warning">Il n'y a aucun candidat dans le TDC</div>
                            <?php endif; ?>
                            
                            
                            <?php ## start of entreprise ?>
                            
                            <div class="tdc_sep clearfix"></div><!-- /tdc_sep -->
                            
                            <?php if (!empty($tdcEntreprises)): ?>
                            <table class="datable stripe hover">
                                <thead>
                                    <tr>
                                        <th style="width: 40px;" align="center"><div class="brush legend entreprise-color tooltips" data-mission_id="<?php echo $mission['id'] ?>" title="Légende de couleurs"></div></th>
                                        <th>Entreprise</th>
                                        <th>Coordonnées</th>
                                        <th>DPT / Précision adresse</th>
                                        <th>Email</th>
                                        <th>Secteur Activité</th>
										<th>Sous Secteur Activité</th>
                                        <th>Statut</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <?php foreach($tdcEntreprises as $tdcEntreprise): ?>
                                    <tr class="tdc_id_<?php echo $tdcEntreprise['tdc_id'] ?>" data-tdc_id="<?php echo $tdcEntreprise['tdc_id'] ?>">
                                        <td align="center" style="width: 40px;">
                                            <div class="brush entreprise action" data-tdc_id="<?php echo $tdcEntreprise['tdc_id'] ?>"></div>
                                            <a name="tdc_<?php echo $tdcEntreprise['tdc_id'] ?>" id="row_tdc_<?php echo $tdcEntreprise['tdc_id'] ?>" class="anchors"></a>
                                        </td>
                                                                                
                                        <?php 
                                        $style3 = '';
                                        if ($tdcEntreprise['color_societe_bg'] > 0 && $tdcEntreprise['color_societe_label'] > 0){
                                            $style3 = 'style="background-color: ' . Control::getColorTDCByIdEntreprise($tdcEntreprise['color_societe_bg']) . ';color:'. Control::getColorTDCById($tdcEntreprise['color_societe_label']).';"';
                                        } elseif ($tdcEntreprise['color_societe_bg'] > 0 && $tdcEntreprise['color_societe_label'] == 0) {
                                            $style3 = 'style="background-color: ' . Control::getColorTDCByIdEntreprise($tdcEntreprise['color_societe_bg']) . ';"';
                                        } elseif ($tdcEntreprise['color_societe_bg'] == 0 && $tdcEntreprise['color_societe_label'] > 0) {
                                            $style3 = 'style="color: ' . Control::getColorTDCByIdEntreprise($tdcEntreprise['color_societe_label']) . ';"';
                                        }
                                        ?>
                                        
                                        <td class="color_societe_bg" <?php echo $style3 ?>>
                                            
                                            <span class="color_societe_label">
                                            
                                                <?php echo $tdcEntreprise['name'] ?>

                                                <span class="glyphicon glyphicon-plus add-doc-entreprise-from-tdc tooltips" data-tdc_line_id="<?php echo $tdcEntreprise['tdc_id'] ?>" data-entreprise_id="<?php echo $tdcEntreprise['entreprise_id'] ?>" data-mission_id="<?php echo $tdcEntreprise['mission_id'] ?>" title="Ajouter Document"></span>

                                                <a name="tdc_<?php echo $tdcEntreprise['tdc_id'] ?>" id="row_tdc_<?php echo $tdcEntreprise['tdc_id'] ?>" class="anchors"></a>

                                                <?php $ent_docs = json_decode($tdcEntreprise['files'], true); ?>
                                                <?php if (empty($ent_docs)) : $empty = "empty"; else: $empty = ""; endif; ?>

                                                <?php 
                                                if (!empty($ent_docs)) :
                                                    echo "<br>";
                                                    echo '(Documents) <span data-entreprise_name="'.$tdcEntreprise['name'].'" class="tdc_doc_fourni '.$empty.'"><i class="fa fa-file-pdf"></i></span>';
                                                endif;
                                                ?>
                                                
                                            </span>
                                            
                                            <div class="tdc_doc_display">
                                                
                                                <h2>Documents Entreprise : <?php echo $tdcEntreprise['name'] ?></h2>
                                                
                                                <?php if (!empty($ent_docs)) :?>
                                                <div class="tdc_doc_wrapper">
                                                    
                                                    <ul class="links_candidat">
                                                    <?php foreach($ent_docs as $key => $ent_doc): ?>
                                                        <?php 
                                                        $imgType = array('jpg', 'jpeg', 'png');
                                                        $ext = getFileExt($ent_doc['file']);
                                                        if (in_array($ext, $imgType)):
                                                        ?>
                                                        <li>
                                                            <a href="<?php echo ENTREPRISE_URL ?>/<?php echo $tdcEntreprise['entreprise_id'] ?>/<?php echo $ent_doc['file'] ?>" data-lightbox="<?php echo $ent_doc['file'] ?>"><?php echo $ent_doc['file'] ?></a>
                                                        </li>
                                                        <?php else: ?>
                                                        <li>
                                                            <a href="<?php echo BASE_URL ?>/download?type=ent&key=<?php echo $key ?>&id=<?php echo $tdcEntreprise['entreprise_id'] ?>"><?php echo $ent_doc['file'] ?></a>
                                                        </li>
                                                        <?php endif; ?>
                                                    <?php endforeach;?>
                                                    </ul>
                                                    
                                                </div><!-- / tdc_cv_wrapper -->
                                                <?php else: ?>
                                                <div class="message warning">Pas des documents</div>
                                                <?php endif; ?>
                                                
                                            </div><!--/tdc_doc_display -->
                                            
                                            <?php if ($tdcEntreprise['name'] != "" && Client::getClientCandidatBlacklist($tdcEntreprise['name'])): ?>
                                                <div class="clearboth notif warning" style="background-color:#ff9800; color: #fff; line-height: 24px; padding: 0px 10px;"><strong>Attention :</strong> L'entreprise est blacklistée</div>
                                            <?php endif; ?>
                                            
                                        </td>
                                        <td><?php echo $tdcEntreprise['telephone'] ?></td>
                                        <td>
                                            <?php 
                                            if ($tdcEntreprise['control_localisation'] <> "") {
                                                echo "<strong>Dpt :</strong> " . $tdcEntreprise['control_localisation'];
                                            }
                                            if ($tdcEntreprise['addresse'] <> "") {
                                                echo "<br><strong>Précision adresse :</strong> " . $tdcEntreprise['addresse'];
                                            }
                                            if ($tdcEntreprise['control_localisation'] == "" && $tdcEntreprise['addresse'] == "" && $tdcEntreprise['region'] <> "") {
                                                echo $tdcEntreprise['region'];
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $tdcEntreprise['email'] ?></td>
                                        <td><?php echo $tdcEntreprise['secteur_activite'] ?></td>
                                        <td><?php echo $tdcEntreprise['sous_secteur_activite'] ?></td>
                                        
                                        <td align="center" style="width: 50px;" class="change_statut_entreprise_tdc clickable" data-entreprise_id="<?php echo $tdcEntreprise['entreprise_id'] ?>" data-mission_id="<?php echo $tdcEntreprise['mission_id'] ?>" data-tdc_id="<?php echo $tdcEntreprise['tdc_id'] ?>" data-order="<?php echo $tdcEntreprise['status_entreprise_order'] ?>">
                                            <?php if ($tdcEntreprise['status_entreprise'] == "" || $tdcEntreprise['status_entreprise'] == 0): ?>
                                                <?php // do nothing ?>
                                            <?php elseif ($tdcEntreprise['status_entreprise'] == 257): ?>
                                                <?php $statusEnt = getStatusTDCColorsEntreprise($tdcEntreprise['status_entreprise'], $colorsTDC); ?>
                                                <span class="status" style="color:<?php echo $statusEnt['fontColor'] ?>; background-color:<?php echo $statusEnt['bgColor'] ?>">
                                                    OUT
                                                </span>
                                            <?php elseif ($tdcEntreprise['status_entreprise'] == 258): ?>
                                                <?php $statusEnt = getStatusTDCColorsEntreprise($tdcEntreprise['status_entreprise'], $colorsTDC); ?>
                                                <span class="status" style="color:<?php echo $statusEnt['fontColor'] ?>; background-color:<?php echo $statusEnt['bgColor'] ?>">
                                                    FINI
                                                </span>
                                            <?php endif; ?>
                                        </td>

                                        <td class="actions_button" style="position: relative; width: 150px;">

                                            <button type="button" data-entreprise_id="<?php echo $tdcEntreprise['entreprise_id'] ?>" data-mission_id="<?php echo $tdcEntreprise['mission_id'] ?>" class="tdc_delete_entreprise btn btn-icon btn-danger pull-right tooltips" title="Supprimer"><span class="glyphicon glyphicon-trash" style="padding-left: 0px;"></span></button>
                                            
                                            <?php if (User::can('edit_candidat')): ?>                                            
                                            <a style="height: 24.5px;" href="<?php echo BASE_URL ?>/editer-entreprise?id=<?php echo $tdcEntreprise['entreprise_id'] ?>&tdc_id=<?php echo $tdcEntreprise['mission_id'] ?>&tdc_line_id=<?php echo $tdcEntreprise['tdc_id'] ?>" class="btn btn-icon pull-right btn-info tooltips tdc_entreprise_comment" data-entreprise_name="<?php echo $tdcEntreprise['name'] ?>" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></a>
                                            <?php endif; ?>
                                            
                                            <?php if ($tdcEntreprise['commentaire'] <> ""): ?>
                                                <?php $comment_entreprise = array_reverse(json_decode($tdcEntreprise['commentaire'], true)); ?>
                                            <?php else: ?>
                                                <?php $comment_entreprise = array(); ?>
                                            <?php endif; ?>
                                            
                                            <div class="tdc_entreprise_comment_hover">                                                
                                                <?php if (!empty($comment_entreprise)): ?>
                                                <div class="nano">
                                                    <div class="nano-content">
                                                        <div class="historiques_comment_wrap">
                                                            <?php if (!empty($comment_entreprise)): ?>
                                                            <div class="task-threads-wrap clearfix">
                                                                <?php foreach ($comment_entreprise as $com): ?>
                                                                <div class="task-thread clearfix">
                                                                    <p><?php echo nl2br($com['comment']) ?></p>
                                                                    <div class="publish-info">
                                                                        Créé par <strong><?php echo mb_ucfirst($com['firstName']) . ' ' . mb_strtoupper($com['lastName']) ?></strong> le <?php echo $com['created'] ?>
                                                                    </div>
                                                                </div>
                                                                <?php endforeach; ?>
                                                            </div><!--/ task-threads-wrap -->
                                                            <?php endif; ?>

                                                        </div><!-- /historiques_comment_wrap -->

                                                    </div><!-- /nano-content-->
                                                </div><!-- /nano -->
                                                <?php else: ?>
                                                <div class="message warning">Aucun commentaire pour cette entreprise</div>
                                                <?php endif; ?>
                                                
                                            </div><!-- /tdc_candidat_comment_hover -->
                                            
                                            <button type="button" data-mission_id="<?php echo $tdcEntreprise['mission_id'] ?>" data-entreprise_id="<?php echo $tdcEntreprise['entreprise_id'] ?>" class="generate_pdf_entreprise_tdc pull-right btn btn-icon btn-warning tooltips" title="Générer un fichier PDF"><i class="fa fa-file-pdf"></i></button>
                                            
                                            <button type="button" data-tdc_id="<?php echo $tdcEntreprise['tdc_id'] ?>" data-entreprise_id="<?php echo $tdcEntreprise['entreprise_id'] ?>" data-mission_id="<?php echo $tdcEntreprise['mission_id'] ?>" class="tdc_suivi_entreprise btn btn-icon btn-success pull-right tooltips" title="<?php $suivi_ent = json_decode($tdcEntreprise['suivis_entreprise'], true); if ($suivi_ent['comment'] == ""): ?>Suivis<?php else: ?><?php echo nl2br($suivi_ent['comment']); ?><?php endif; ?>"><span class="glyphicon glyphicon-send"></span></button>
                                            
                                            <button type="button" data-entreprise_id="<?php echo $tdcEntreprise['entreprise_id'] ?>" data-mission_id="<?php echo $tdcEntreprise['mission_id'] ?>" data-entreprise_name="<?php echo $tdcEntreprise['name'] ?>" data-tdc_id="<?php echo $tdcEntreprise['tdc_id'] ?>" class="tdc_history_entreprise btn btn-icon btn-primary pull-right tooltips" title="Historiques"><span class="glyphicon glyphicon-comment"></span></button>
                                            
                                            <?php
                                            // historiques hover
                                            $comments = TDC::getHistoriquesEntreprise($tdcEntreprise['mission_id'], $tdcEntreprise['entreprise_id'])
                                            ?>
                                            
                                            <div class="comment_tdc_histo_ent">
                                                <div class="historiques_comment_wrap">
                                                    <?php if (!empty($comments)): ?>
                                                    <div class="task-threads-wrap clearfix">
                                                        <?php foreach ($comments as $comment): ?>
                                                        <div class="task-thread clearfix">
                                                            <p><?php echo nl2br($comment['comment']) ?></p>
                                                            <div class="publish-info">
                                                                Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo dateToFr($comment['created'], true) ?>
                                                            </div>
                                                        </div>
                                                        <?php endforeach; ?>
                                                    </div><!--/ task-threads-wrap -->
                                                    <?php else: ?>
                                                    <div class="message warning">Aucun Historiques</div>
                                                    <?php endif; ?>
                                                </div><!-- /historiques_comment_wrap -->
                                            </div>
                                            
                                            
                                        </td>
                                    </tr>
									<?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php else: ?>
                            <div class="message warning">Il n'y a aucun entreprise dans le TDC</div>
                            <?php endif; ?>
                            <?php ## end of entreprise ?>
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
            
        </div><!-- /contentWrap -->
        
    </div><!-- /base -->
    
	<?php require_once './views/debug.php'; ?>
    <div class="hidden hidden-data">
        <span class="tdc_id"></span>
        <span class="mission_id"></span>
    </div><!-- / hidden-data -->
    
    <?php ### POP UP ### ?>
<!--    <div class="chat_box">
        <div class="chat_header clearfix">
            <span class="name"></span>
            <span class="chat_title"></span>
            <button class="close tooltips" title="Fermer" type="button">×</button>
        </div>
        <div class="chat_messages_history clearfix">
            <div class="nano">
                <div class="nano-content">
                    
                     CONTENT HERE 
                    
                     CONTENT HERE 
                    
                </div> / nano-content 
            </div> / nano 
        </div>
    </div>-->
    <?php ### END OF POP UP ### ?>
    
</body>
</html>
<script src="<?php echo JS_URL ?>/tdc.js"></script>
<script>
$(document).ready(function(){
    <?php if (get('tdc_id') && get('tdc_id') <> ""): ?>
        // then we force scroll by around 200 px because of fixed header and then fixed table header
        if ($('#row_tdc_'+'<?php echo get('tdc_id') ?>').length) {
            var x = ($('#row_tdc_'+'<?php echo get('tdc_id') ?>').offset().top) - 200;
            $('html,body').animate({scrollTop:x},2000);
        } 
    <?php endif; ?>

    
    <?php // if ($_SESSION['tdc_summary'] == 'close'): ?>
//    $('span.triggerSummary').css('top', '25px');
//    $('span.triggerSummary.close').fadeOut(500);
//    $('span.triggerSummary.open').fadeIn(500);
//    $('#base.TDC').css('margin-top', '55px');
//    headerOffset = 120;
    <?php // endif; ?>
    
    filterDateStats.Init();
         
});

var filterDateStats = {
    Init: function (){
        $("#start_date").on().datepicker({
            dateFormat: 'dd-mm-yy', 
            regional: 'fr',
            //minDate: '<?php //echo dateToFr($mission['date_debut']) ?>',
            maxDate: '<?php echo dateToFr($mission['date_fin']) ?>',
            showButtonPanel: true,
            onClose: function(selectedDate) {
                $("#end_date").datepicker("option", "minDate", selectedDate);
                forms.validateField($("#start_date"), 'text');
            }
        });
        $("#end_date").on().datepicker({
            dateFormat: 'dd-mm-yy', 
            regional: 'fr',
            minDate: '<?php echo dateToFr($mission['date_debut']) ?>',
            maxDate: '<?php echo dateToFr($mission['date_fin']) ?>',
            showButtonPanel: true,
            onClose: function(selectedDate) {
                $("#start_date").datepicker("option", "maxDate", selectedDate);
                forms.validateField($("#end_date"), 'text');
            }
        });    
    },
}

// used for stickey legende
var OriginalStickyLegendeHtml = $('.wrapper-legende').html();
var OutcategorieSelected = "";
</script>