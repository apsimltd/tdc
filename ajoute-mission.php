<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Ajouter une mission" ?>
<?php $page = "add-mission"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('create_mission'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>

	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base" class="AddCandidat AddMission">

        <div id="contentWrap">					
            
			<section class="title block">

				<h1 class="pull-left"><span class="glyphicon glyphicon-tasks"></span>Ajouter une mission</h1>
				
				<div class="btn-box pull-right">                       
					<button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page précédente" onClick="window.history.back();"><span class="glyphicon glyphicon-menu-left"></span></button>
				</div>
				
			</section>
			
            <div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
												
						<div class="block-body">
							
                            <form class="frm_frm" name="frm_create_mission" id="frm_create_mission" method="POST" action="<?php echo AJAX_HANDLER ?>/add-mission" enctype="multipart/form-data">
							
								<div class="row">
				
									<div class="col col-4">
										<div class="block nopadding">
																	
											<div class="block-body">
												
                                                <?php if ($me['id'] == "301" || User::isSA()): // only for user Bastien ?>
                                                <fieldset class="mission-checkbox">
                                                    <input type="checkbox" class="frm_checkbox" name="is_private_check" id="is_private_check">
                                                    <label class="pointer" for="is_private_check">Est Privée a moi</label>
                                                </fieldset>
                                                <input type="hidden" name="is_private" id="is_private" value="null">
                                                <?php endif; ?>
                                                
                                                <?php if (User::can('kpi_mission_include')): ?>
                                                <fieldset class="mission-checkbox">
                                                    <input type="checkbox" class="frm_checkbox" name="include_in_kpi_check" id="include_in_kpi_check" checked="checked">
                                                    <label class="pointer" for="include_in_kpi_check">Inclure dans KPI</label>
                                                </fieldset>
                                                <input type="hidden" name="include_in_kpi" id="include_in_kpi" value="1">
                                                <?php endif; ?>
                                                
                                                <fieldset class="mission-checkbox">
                                                    <input type="checkbox" class="frm_checkbox" name="mail_notif_check" id="mail_notif_check" checked="checked">
                                                    <label class="pointer" for="mail_notif_check">Notification Email <span class="glyphicon glyphicon-info-sign tooltips" title="Le Manager recevra une notification par courrier électronique du commentaire client"></span></label>
                                                </fieldset>
                                                <input type="hidden" name="mail_notif" id="mail_notif" value="1">
                                                
                                                <fieldset>
													<label>Manager</label>
													<select class="frm_chosen must" name="manager_id" data-validation="val_blank">
														<option value="">Choisir Manager</option>
														<?php $managers = User::getManagers() ?>
                                                        <?php foreach($managers as $manager): ?>
                                                        <option value="<?php echo $manager['id'] ?>"><?php echo mb_strtoupper($manager['lastName']) . ' ' . mb_ucfirst($manager['firstName']) ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
                                                
												<fieldset>
													<label>Client</label>
													<select class="frm_chosen must" name="client_id" data-validation="val_blank">
														<option value="">Choisir Client</option>
														<?php $clients = Client::getClients('nom') ?>
                                                        <?php foreach($clients as $client): ?>
                                                        <option value="<?php echo $client['id'] ?>"><?php echo $client['id'] . ' - ' . mb_strtoupper($client['nom']) ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
                                                <fieldset>
													<label>Consultant</label>
													<select class="frm_chosen must" name="consultant_id" data-validation="val_blank">
														<option value="">Choisir Consultant</option>
														<?php $consultants = User::getUsersByRole('Consultant') ?>
                                                        <?php foreach($consultants as $consultant): ?>
                                                        <option value="<?php echo $consultant['id'] ?>">
                                                            <?php echo mb_ucfirst($consultant['firstName']) . ' ' . mb_strtoupper($consultant['lastName']) ?>
                                                        </option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
                                                <fieldset>
													<label>Poste</label>
													<input class="frm_text must" name="poste" placeholder="Poste" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
                                                <fieldset>
													<label>Secteur</label>
													<select class="frm_chosen must" name="control_secteur_id" data-validation="val_blank">
														<option value="">Choisir Secteur</option>
														<?php $controls = Control::getControlByType(2, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
												<fieldset>
													<label>Localisation</label>
													<select class="frm_chosen must" name="control_localisation_id" data-validation="val_blank">
														<option value="">Choisir Localisation</option>
														<?php $controls = Control::getControlByType(6, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
                                                <fieldset>
													<label>Fonction</label>
													<select class="frm_chosen must" name="control_fonction_id" data-validation="val_blank">
														<option value="">Choisir Fonction</option>
                                                        <?php $controls = Control::getControlByType(1, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
                                                <fieldset>
													<label>Niveau Formation</label>
													<select class="frm_chosen must" name="control_niveauFormation_id" data-validation="val_blank">
														<option value="">Choisir Niveau Formation</option>
														<?php $controls = Control::getControlByType(4, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>"><?php echo $control['name'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
                                                <fieldset>
													<label>Statut</label>
													<select class="frm_chosen must ok" name="status" data-validation="val_blank">
														<option value="">Choisir Statut</option>
														<?php $controls = Control::getControlListByType(9) ?>
                                                        <?php foreach($controls as $key => $value): ?>
                                                        <option value="<?php echo $key ?>" <?php if ($key == 232): ?>selected="selected"<?php endif; ?>><?php echo $value ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
												<fieldset class="range">
                                                    <label>Rémunération / K€</label>
                                                    <input class="frm_text xsmall min" name="remuneration_min" placeholder="Entre" type="text" autocomplete="off" data-validation="val_num_range_min">
                                                    <span class="interval"> - </span>
                                                    <input class="frm_text xsmall max" name="remuneration_max" placeholder="Et" type="text" autocomplete="off" data-validation="val_num_range_max">
                                                </fieldset>
												<fieldset class="range">
                                                    <label>Âge / ans</label>
                                                    <input class="frm_text xsmall min" name="age_min" placeholder="Entre" type="text" autocomplete="off" data-validation="val_num_range_min">
                                                    <span class="interval"> - </span>
                                                    <input class="frm_text xsmall max" name="age_max" placeholder="Et" type="text" autocomplete="off" data-validation="val_num_range_max">
                                                </fieldset>
                                                <fieldset class="range">
                                                    <label>Expérience / ans</label>
                                                    <input class="frm_text xsmall min" name="experience_min" placeholder="Entre" type="text" autocomplete="off" data-validation="val_num_range_min">
                                                    <span class="interval"> - </span>
                                                    <input class="frm_text xsmall max" name="experience_max" placeholder="Et" type="text" autocomplete="off" data-validation="val_num_range_max">
                                                </fieldset>
                                                <fieldset class="single-row">
                                                    <label>Date de début - fin</label>
                                                    <input class="frm_text xsmall must" name="date_debut" id="date_debut" placeholder="De" type="text" autocomplete="off" data-validation="val_blank">
                                                    <span class="interval"> - </span>
                                                    <input class="frm_text xsmall must" name="date_fin" id="date_fin" placeholder="À" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>												
											</div><!-- / block-body -->
											
										</div>
									</div><!-- /col -->
									
									<div class="col col-4">
										<div class="block nopadding">
																	
											<div class="block-body">
												
                                                <fieldset>
                                                    <label>Out Categorie</label>
                                                    <div class="add_field_row pull-left" style="width: 60%;">
                                                        <input class="frm_text critere_specific" placeholder="Out Categorie" type="text" autocomplete="off" data-validation="val_blank" style="width: 90%;">
                                                        <input type="hidden" name="out_category" id="out_category">
                                                        <button type="button" class="add_out_category add-sortable btn btn-icon btn-success pull-right mt5 tooltips" title="Ajouter Out Categorie"><span class="glyphicon glyphicon-plus"></span></button>
                                                    </div><!-- /add_field_row -->
                                                </fieldset>
                                                
                                                <fieldset class="out_category">
                                                    <p class="note">Out Categorie</p>
                                                    <div class="sortable-list">
                                                        <div class="nano">
                                                            <div class="nano-content">
                                                                <ul class="sortable">
                                                                    
                                                                </ul>
                                                            </div>
                                                        </div><!-- / nano -->
                                                    </div><!-- /sortable-list -->
                                                </fieldset>
                                                
                                                <fieldset>
                                                    <label>Critères spécifiques</label>
                                                    <div class="add_field_row pull-left" style="width: 60%;">
                                                        <input class="frm_text critere_specific" placeholder="Critères spécifiques" type="text" autocomplete="off" data-validation="val_blank" style="width: 90%;">
                                                        <input type="hidden" name="critere_specific" id="critere_specific">
                                                        <button type="button" class="add_critere_specific add-sortable btn btn-icon btn-success pull-right mt5 tooltips" title="Ajouter un Critère Spécifique"><span class="glyphicon glyphicon-plus"></span></button>
                                                    </div><!-- /add_field_row -->
                                                </fieldset>
                                                
                                                <fieldset class="critere_specific">
                                                    <p class="note">Critères spécifiques <span class="pull-right note">* Faites glisser pour réorganiser</span></p>
                                                    <div class="sortable-list">
                                                        <div class="nano">
                                                            <div class="nano-content">
                                                                <ul class="sortable">
                                                                    
                                                                </ul>
                                                            </div>
                                                        </div><!-- / nano -->
                                                    </div><!-- /sortable-list -->
                                                </fieldset>
                                                
											</div><!-- / block-body -->
											
										</div>
									</div><!-- /col -->
									
									<div class="col col-4">
										<div class="block nopadding">
																	
											<div class="block-body">
												
                                                <fieldset>
													<label>Prestataire</label>
													<select class="frm_chosen must ok" name="prestataire_id" data-validation="val_blank">
														<option value="">Choisir Prestataire</option>
														<?php $prestataires = Mission::getPrestataires() ?>
                                                        <?php foreach($prestataires as $presta): ?>
                                                        <option value="<?php echo $presta['id'] ?>" <?php if ($presta['id'] == 1): ?>selected="selected"<?php endif; ?>><?php echo $presta['nom'] ?></option>
                                                        <?php endforeach; ?>
													</select>
												</fieldset>
                                                
                                                <fieldset class="selectOption largemultiselect">
                                                    <label><span class="pull-left">Non-Affectés à la mission</span><span class="pull-right">Affectés à la mission</span></label>
                                                    <div class="frm_select_multi_wrapper">
                                                        <select class="frm_select_multi must" data-validation="val_blank" multiple='multiple'>
                                                            <?php $users = User::getUsers(); ?>
                                                            <?php foreach($users as $user): ?>
                                                                <?php if ($user['status'] == 1): ?>
                                                                    <option value="<?php echo $user['id'] ?>">
                                                                        <?php echo mb_ucfirst($user['firstName']) . ' ' . mb_strtoupper($user['lastName']) . ' ['. $user['role_name'] .'] ' ?>
                                                                    </option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <input type="hidden" name="users_id" class="frm_select_multi_helper must" data-validation="val_blank">
                                                    </div>
                                                </fieldset>
                                                
												<fieldset>
                                                    <label style="text-align: left;">Commentaire</label>
                                                    <textarea name="commentaire" class="frm_textarea" style="width: 100%; height: 157px;" placeholder="Commentaire" data-validation="val_blank"></textarea>                                
												</fieldset>
                                                
                                                <fieldset class="uploadMultiple">
                                                    <div class="clearfix">
                                                        <label class="pull-left">Documents <span class="glyphicon glyphicon-info-sign tooltips" title="Les fichiers autorisés sont : .doc, .docx, .xls, .xlsx, .pdf, .jpg, .jpeg, .png ; Taille max : 10 Mo"></span></label>
                                                        <input type="hidden" name="uploadFlag" id="uploadFlag" value="1">
                                                        <div class="clearfix add_field_wrap_documents pull-left" style="width: 60%;">
                                                            <div class="add_field_row pull-left">
                                                                <input type="file" name="file[]" id="file_1" data-number="1" class="inputfile inputfile-1">
                                                                <label id="uploadLabel_1" for="file_1" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span></label>
                                                                <button type="button" class="clear_field_upload btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt6 clearfix">
                                                        <button type="button" class="add_field_upload btn btn-icon btn-success pull-right tooltips" title="Ajouter un nouveau fichier"><span class="glyphicon glyphicon-plus"></span></button>
                                                    </div>                              
												</fieldset>
                                                
												<fieldset>
													<button type="button" class="btn btn-primary frm_submit frm_before_submit frm_notif pull-right" data-form="6"><i class="ico-txt fa fa-plus"></i> Ajouter Mission</button>
												</fieldset>
												
											</div><!-- / block-body -->
											
										</div>
									</div><!-- /col -->
									
								</div><!-- / row -->	

							</form>
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
        
        </div><!-- /contentWrap -->
        
    </div><!-- /base --> 
	
	<?php require_once './views/debug.php'; ?>  
    
</body>
</html>
<script src="<?php echo JS_URL ?>/jquery-sortable.js"></script>
<script>
$(document).ready(function(){
    
    // checked and unchecked include mission in kpi
    $(document).on('ifUnchecked', '#is_private_check', function(){
        $('#is_private').val('null');
    });
    $(document).on('ifChecked', '#is_private_check', function(){
        $('#is_private').val('1');
    });
    
    // checked and unchecked include mission in kpi
    $(document).on('ifUnchecked', '#include_in_kpi_check', function(){
        $('#include_in_kpi').val('null');
    });
    $(document).on('ifChecked', '#include_in_kpi_check', function(){
        $('#include_in_kpi').val('1');
    });
    
    $(document).on('ifUnchecked', '#mail_notif_check', function(){
        $('#mail_notif').val('null');
    });
    $(document).on('ifChecked', '#mail_notif_check', function(){
        $('#mail_notif').val('1');
    });
    
    
    
    // sort list
    // https://johnny.github.io/jquery-sortable/#home
    $('ul.sortable').sortable();
    
    
    // add sortable item
    $(document).on('click', '.add-sortable', function (){
        var val = $(this).prev().prev('input.frm_text').val();
        if (val != "") {
            var html = '<li><div class="sort-wrapping"><span class="value">'+val+'</span><input type="text" class="frm_text ok must sortable_text" data-value="'+val+'" value="'+val+'" data-validation="val_blank"><span class="sortable-actions pull-right"><span class="glyphicon delete_sortable glyphicon-trash pull-right tooltips" title="Supprimer"></span><span class="glyphicon edit_sortable  glyphicon-edit pull-right tooltips" title="Éditer"></span><span class="glyphicon save_sortable glyphicon-ok pull-right tooltips" title="Sauvegarder"></span><span class="glyphicon cancel_sortable glyphicon-remove pull-right tooltips" title="Annuler"></span></span></div></li>';
            $(this).parent().parent('fieldset').next('fieldset').find('ul.sortable').append(html);
            $(this).prev().prev('input.frm_text').val('').removeClass('error').removeClass('ok');
            Scroller.Init();
            Tooltip.Init();
        }
    });
    
    //delete sortable item
    $(document).on('click', '.delete_sortable', function (){
        $(this).parent().parent().parent('li').remove();
        Scroller.Init();
    });
    
    // edit sortable item
    $(document).on('click', '.edit_sortable', function (){
        var parentLi = $(this).parent().parent().parent('li');
        parentLi.find('.edit_sortable').fadeOut(1);
        parentLi.find('.delete_sortable').fadeOut(1);
        parentLi.find('span.value').css('visibility','hidden');
        parentLi.find('.save_sortable').fadeIn(1);
        parentLi.find('.cancel_sortable').fadeIn(1);
        parentLi.find('.frm_text').css('visibility','visible');
    });
    
    // cancel sortable item
    $(document).on('click', '.cancel_sortable', function (){
        var parentLi = $(this).parent().parent().parent('li');
        parentLi.find('.save_sortable').fadeOut(1);
        parentLi.find('.cancel_sortable').fadeOut(1);
        var val = parentLi.find('.frm_text').attr('data-value');
        parentLi.find('.frm_text').css('visibility', 'hidden').removeClass('error').val(val);
        parentLi.find('.edit_sortable').fadeIn(1);
        parentLi.find('.delete_sortable').fadeIn(1);
        parentLi.find('span.value').css('visibility','visible');
    });
    
    // save sortable items
    $(document).on('click', '.save_sortable', function (){
        var parentLi = $(this).parent().parent().parent('li');
        var val = parentLi.find('.frm_text').val();
        if (val == "") {
            Notif.Show('You must enter a value or cancel it and then delete', 'error', true, 2000);
            parentLi.find('.frm_text').addClass('error');
            return false;
        }
        
        parentLi.find('.save_sortable').fadeOut(1);
        parentLi.find('.cancel_sortable').fadeOut(1);
        parentLi.find('.frm_text').css('visibility', 'hidden').removeClass('error').val(val);
        parentLi.find('.frm_text').attr('data-value', val);
        parentLi.find('.edit_sortable').fadeIn(1);
        parentLi.find('.delete_sortable').fadeIn(1);
        parentLi.find('span.value').css('visibility','visible').text(val);
    });
    
    $("#date_debut").on().datepicker({
        dateFormat: 'dd-mm-yy', 
        regional: 'fr',
        //minDate: '0',
        showButtonPanel: true,
        onClose: function(selectedDate) {
            $("#date_fin").datepicker("option", "minDate", selectedDate);
        }
    });

    $("#date_fin").on().datepicker({
        dateFormat: 'dd-mm-yy', 
        regional: 'fr',
        showButtonPanel: true,
        onClose: function(selectedDate) {
            $("#date_debut").datepicker("option", "maxDate", selectedDate);
        }
    });
    
    Upload.Init();
    Upload.Validate();
    
        
    // add a new field for upload
    $(document).on('click', 'button.add_field_upload', function(){
        
        // get the last input number
        var number = parseInt($('.add_field_wrap_documents .add_field_row:last input.inputfile').attr('data-number')) + 1;
        
        var inputHtml = '<input type="file" name="file[]" id="file_'+number+'" data-number="'+number+'" class="inputfile inputfile-1"><label id="uploadLabel_'+number+'" for="file_'+number+'" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span></label><button type="button" class="clear_field_upload btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>';
        var newFieldHtml = '<div class="add_field_row clearfix">'+inputHtml+'</div>';
        $('.add_field_wrap_documents').append(newFieldHtml);
        
        var flagNumber = parseInt($('#uploadFlag').val()) + 1;
        $('#uploadFlag').val(flagNumber);
        
        Upload.Init();
    });
    
    $(document).on('click', '.clear_field_upload', function(){
        
        var numRows = parseInt($('fieldset.uploadMultiple .add_field_wrap_documents .add_field_row:last').index()) + 1;
        
        if (numRows >= 2) {
            $(this).parent('.add_field_row').remove();
        } else if (numRows == 1) {
            $(this).parent('.add_field_row').find('input.inputfile').val('');
            $(this).parent('.add_field_row').find('label').html('<span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span>');
        }
        
    });    
    
}); // end document ready function

var Upload = {
    Init: function (){
        $('.inputfile').each(function(){
		
            var $input	 = $( this ),
                $label	 = $input.next( 'label' ),
                labelVal = $label.html();

            $input.on( 'change', function( e )
            {
                var fileName = '';

                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else if( e.target.value )
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    $label.find( 'span.filename' ).html( fileName );
                else
                    $label.html( labelVal );
            });

            // Firefox bug fix
            $input
            .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
            .on( 'blur', function(){ 
                $input.removeClass( 'has-focus' ); 
            });

        });
    },
    Validate: function (){
        $(document).on('change', '.inputfile', function(){
              Upload.ValidateUpload($(this));            
        });
    },
    ValidateUpload: function (input) {
        var file_data = input.prop("files")[0];
        // start validate file extension
        var validExtensions = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx', 'xls', 'xlsx']; //array of valid extensions
        var filename = file_data.name;
        var ext = filename.substr(filename.lastIndexOf('.') + 1);
        if ($.inArray(ext, validExtensions) == -1){
            Notif.Show('Fichier non valide', 'error', true, 4000);
            input.val("");
            input.next('label').removeClass('ok').addClass('error');
            return false;
        } else {
            // check file size
            // 4MB allowed
            var filesize = file_data.size;
            if (filesize > 10485760) {
                Notif.Show('La taille du fichier est supérieure à celle autorisée. Télécharger moins de 10 Mo.', 'error', true, 4000);
                input.val("");
                input.next('label').removeClass('ok').addClass('error');
                return false;
            } else {
                input.next('label').removeClass('error').addClass('ok');
                return true;
            }
        }
        // end validation file extension

    },
}

function frm_create_mission_before_submit(obj) {  
    
    // processing out_category and critere specific field
    var out_category = "";
    var numItems = parseInt($('fieldset.out_category ul li:last').index()) + 1;
    $('fieldset.out_category ul li').each(function(index){
        var itemPos = index + 1;
        if (itemPos == numItems) {
            out_category += $(this).find('span.value').text();
        } else {
            out_category += $(this).find('span.value').text() + '|';
        }
    });
    $('#out_category').val(out_category);
    
    var critere = "";
    var numItems = parseInt($('fieldset.critere_specific ul li:last').index()) + 1;
    $('fieldset.critere_specific ul li').each(function(index){
        var itemPos = index + 1;
        if (itemPos == numItems) {
            critere += $(this).find('span.value').text();
        } else {
            critere += $(this).find('span.value').text() + '|';
        }
    });
    $('#critere_specific').val(critere);
    
    
    // verify upload
    var uploadFlag = true;
    var numLabel = parseInt($('#uploadFlag').val());
    
    for(var i = 1; i <= numLabel; i++) {
        if ($('#uploadLabel_'+numLabel).length) {
            if ($('#uploadLabel_'+numLabel).hasClass('error')) {
                uploadFlag = false;
            }
        }
    }
    
    return uploadFlag;
    
}
</script>