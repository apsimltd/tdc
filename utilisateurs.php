<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Utilisateurs" ?>
<?php $page = "users"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('create_user', 'read_user', 'edit_user'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>
	
    <?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base">

        <div id="contentWrap">
        
			<section class="title block">

				<h1 class="pull-left">Utilisateurs</h1>
				
                <?php if (User::can('create_user')): ?>
				<div class="btn-box pull-right">                        
					<button type="button" class="add_user btn btn-icon btn-primary pull-right tooltips" title="Ajouter Utilisateur"><span class="glyphicon glyphicon-plus"></span></button>
				</div>
                <?php endif; ?>
				
			</section>
			
            <?php if (User::can('read_user', 'edit_user')): ?>
            <div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="os-icon os-icon-user-male-circle2"></i>Liste des utilisateurs</header>
                        </div>
						
						<div class="block-body">
							<?php $users = User::getUsers(); //debug($users) ?>
                            <table class="datable stripe hover">
                                <thead>
                                    <tr>
                                        <th>Ref</th>
										<th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Identifiant</th>
                                        <th>Email</th>
                                        <th>Rôle</th>
										<th>Chef d'équipe</th>
										<th>Date de création</th>
										<th>Dernière connexion</th>
										<th>Statut</th>
                                        <?php if (User::can('edit_user')): ?>
                                        <th>Actions</th>
                                        <?php endif; ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($users as $user): ?>
                                    <tr>
                                        <td><?php echo $user['id'] ?></td>
                                        <td><?php echo mb_strtoupper($user['lastName']) ?></td>
                                        <td><?php echo mb_ucfirst($user['firstName']) ?></td>
                                        <td><?php echo $user['username'] ?></td>
										<td><?php echo $user['email'] ?></td>
                                        <td><?php echo $user['role_name'] ?></td>
                                        <td>
                                            <?php if (isset($user['teamleader'])): ?>
                                                <?php echo mb_ucfirst($user['teamleader']['firstName']) . ' ' . mb_strtoupper($user['teamleader']['lastName']) ?>
                                            <?php else: ?>
                                                N / A
                                            <?php endif; ?>
                                        </td>
										<td data-order="<?php echo $user['created'] ?>">
                                            <span title="<?php echo $user['created'] ?>"><?php echo dateToFr($user['created'], true) ?></span>
                                        </td>
										<td data-order="<?php echo $user['lastLogin'] ?>">
                                            <span title="<?php echo $user['lastLogin'] ?>"><?php echo dateToFr($user['lastLogin'], true) ?></span>
                                        </td>
										<td align="center">
                                            <span class="glyphicon <?php if (intval($user['status']) == 1): ?>table-active glyphicon-ok<?php else: ?>table-inactive glyphicon-remove<?php endif; ?>"></span>
                                        </td>
                                        <?php if (User::can('edit_user')): ?>
                                        <td class="actions_button">
                                            <button type="button" data-user_id="<?php echo $user['id'] ?>" class="edit_user btn btn-icon btn-primary tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>                                        
                                        </td>
                                        <?php endif; ?>
                                    </tr>
                                    <?php endforeach; ?>						
                                </tbody>
                            </table>
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
            <?php endif; ?>
        
        </div><!-- /contentWrap -->
        
    </div><!-- /base -->
    
	<?php require_once './views/debug.php'; ?>    
    
</body>
</html>
<script>
$(document).ready(function(){
    <?php if (User::can('create_user')): ?>User.AddUser();<?php endif; ?>
    <?php if (User::can('edit_user')): ?>User.EditUser();<?php endif; ?>
});

var User = {
    <?php if (User::can('create_user')): ?>
    AddUser: function (){
        $(document).on('click', '.add_user', function (){
            Modal.Show(AJAX_UI + '/add-user', null, null, '<i class="os-icon os-icon-user-male-circle"></i>', 50);
        });
    },
    <?php endif; ?>
    <?php if (User::can('edit_user')): ?>
    EditUser: function (){
        $(document).on('click', '.edit_user', function (){
            var user_id = $(this).attr('data-user_id');
            Modal.Show(AJAX_UI + '/edit-user?id='+user_id, null, null, '<i class="os-icon os-icon-user-male-circle"></i>', 50);
        });
    },
    <?php endif; ?>
}

<?php if (User::can('create_user')): ?>
function frm_create_user_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}
<?php endif; ?>
    
<?php if (User::can('edit_user')): ?>
function frm_edit_user_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}
<?php endif; ?>
</script>