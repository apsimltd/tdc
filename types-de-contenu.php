<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Types de contenu" ?>
<?php $page = "contentType"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('create_type_content', 'read_type_content', 'edit_type_content'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>


	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base" class="TypeContenu">

        <div id="contentWrap">
        
			<section class="title block">
				<h1>Types de contenu</h1>	
			</section>
			
            <div class="row">
				
				<div class="col col-4">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Fonctions</header>
                            <?php if (User::can('create_type_content')): ?>
                            <div class="block-head-btns pull-right">
                            	<button type="button" data-type_id="1" class="add_type_content btn btn-icon btn-primary pull-right tooltips" title="Ajouter"><span class="glyphicon glyphicon-plus"></span></button>
                            </div>
                            <?php endif; ?>
                        </div>
						
						<div class="block-body">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_1">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $controls = Control::getControlByType(1) ?>
                                                <?php foreach($controls as $control): ?>
                                                <tr <?php if ($control['status'] == 0): ?>class="inactive"<?php endif; ?>>
                                                    <td><?php echo $control['name'] ?></td>
                                                    <?php if (User::can('edit_type_content')): ?>
                                                    <td class="actions_button">
                                                        <button type="button" data-type_id="1" data_control_id="<?php echo $control['id'] ?>" class="edit_type_contenu btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>
                                                    </td>
                                                    <?php endif; ?>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
                <div class="col col-4">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Secteurs</header>
                            <?php if (User::can('create_type_content')): ?>
                            <div class="block-head-btns pull-right">
                            	<button type="button" data-type_id="2" class="add_type_content btn btn-icon btn-primary pull-right tooltips" title="Ajouter"><span class="glyphicon glyphicon-plus"></span></button>
                            </div>
                            <?php endif; ?>
                        </div>
						
						<div class="block-body">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_2">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $controls = Control::getControlByType(2) ?>
                                                <?php foreach($controls as $control): ?>
                                                <tr <?php if ($control['status'] == 0): ?>class="inactive"<?php endif; ?>>
                                                    <td><?php echo $control['name'] ?></td>
                                                    <?php if (User::can('edit_type_content')): ?>
                                                    <td class="actions_button">
                                                        <button type="button" data-type_id="2" data_control_id="<?php echo $control['id'] ?>" class="edit_type_contenu btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>        
                                                    </td>
                                                    <?php endif; ?>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
                <div class="col col-4">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Sources</header>
                            <?php if (User::can('create_type_content')): ?>
                            <div class="block-head-btns pull-right">
                            	<button type="button" data-type_id="3" class="add_type_content btn btn-icon btn-primary pull-right tooltips" title="Ajouter"><span class="glyphicon glyphicon-plus"></span></button>
                            </div>
                            <?php endif; ?>
                        </div>
						
						<div class="block-body">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_3">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $controls = Control::getControlByType(3) ?>
                                                <?php foreach($controls as $control): ?>
                                                <tr <?php if ($control['status'] == 0): ?>class="inactive"<?php endif; ?>>
                                                    <td><?php echo $control['name'] ?></td>
                                                    <?php if (User::can('edit_type_content')): ?>
                                                    <td class="actions_button">
                                                        <button type="button" data-type_id="3" data_control_id="<?php echo $control['id'] ?>" class="edit_type_contenu btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>
                                                    </td>
                                                    <?php endif; ?>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
            
            <div class="row">
				
				<div class="col col-4">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Niveau Formations</header>
                            <?php if (User::can('create_type_content')): ?>
                            <div class="block-head-btns pull-right">
                            	<button type="button" data-type_id="4" class="add_type_content btn btn-icon btn-primary pull-right tooltips" title="Ajouter"><span class="glyphicon glyphicon-plus"></span></button>
                            </div>
                            <?php endif; ?>
                        </div>
						
						<div class="block-body">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_4">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $controls = Control::getControlByType(4) ?>
                                                <?php foreach($controls as $control): ?>
                                                <tr <?php if ($control['status'] == 0): ?>class="inactive"<?php endif; ?>>
                                                    <td><?php echo $control['name'] ?></td>
                                                    <?php if (User::can('edit_type_content')): ?>
                                                    <td class="actions_button">
                                                        <button type="button" data-type_id="4" data_control_id="<?php echo $control['id'] ?>" class="edit_type_contenu btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>
                                                    </td>
                                                    <?php endif; ?>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
                <div class="col col-4">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Langues</header>
                            <?php if (User::can('create_type_content')): ?>
                            <div class="block-head-btns pull-right">
                            	<button type="button" data-type_id="5" class="add_type_content btn btn-icon btn-primary pull-right tooltips" title="Ajouter"><span class="glyphicon glyphicon-plus"></span></button>
                            </div>
                            <?php endif; ?>
                        </div>
						
						<div class="block-body">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_5">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $controls = Control::getControlByType(5) ?>
                                                <?php foreach($controls as $control): ?>
                                                <tr <?php if ($control['status'] == 0): ?>class="inactive"<?php endif; ?>>
                                                    <td><?php echo $control['name'] ?></td>
                                                    <?php if (User::can('edit_type_content')): ?>
                                                    <td class="actions_button">
                                                        <button type="button" data-type_id="5" data_control_id="<?php echo $control['id'] ?>" class="edit_type_contenu btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>
                                                    </td>
                                                    <?php endif; ?>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
                <div class="col col-4">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Zone géographiques</header>
                            <?php if (User::can('create_type_content')): ?>
                            <div class="block-head-btns pull-right">
                            	<button type="button" data-type_id="6" class="add_type_content btn btn-icon btn-primary pull-right tooltips" title="Ajouter"><span class="glyphicon glyphicon-plus"></span></button>
                            </div>
                            <?php endif; ?>
                        </div>
						
						<div class="block-body">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_6">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $controls = Control::getControlByType(6) ?>
                                                <?php foreach($controls as $control): ?>
                                                <tr <?php if ($control['status'] == 0): ?>class="inactive"<?php endif; ?>>
                                                    <td><?php echo $control['name'] ?></td>
                                                    <?php if (User::can('edit_type_content')): ?>
                                                    <td class="actions_button">
                                                        <button type="button" data-type_id="6" data_control_id="<?php echo $control['id'] ?>" class="edit_type_contenu btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>
                                                    </td>
                                                    <?php endif; ?>
                                                </tr>
                                                <?php endforeach; ?>                                               
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
            
            <div class="row">
				
				<div class="col col-3">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Civilité</header>
                            <?php if (User::can('create_type_content')): ?>
                            <div class="block-head-btns pull-right">
                            	<button type="button" data-type_id="7" class="add_type_content btn btn-icon btn-primary pull-right tooltips" title="Ajouter"><span class="glyphicon glyphicon-plus"></span></button>
                            </div>
                            <?php endif; ?>
                        </div>
						
						<div class="block-body small">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_7">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $controls = Control::getControlByType(7) ?>
                                                <?php foreach($controls as $control): ?>
                                                <tr <?php if ($control['status'] == 0): ?>class="inactive"<?php endif; ?>>
                                                    <td><?php echo $control['name'] ?></td>
                                                    <?php if (User::can('edit_type_content')): ?>
                                                    <td class="actions_button">
                                                        <button type="button" data-type_id="7" data_control_id="<?php echo $control['id'] ?>" class="edit_type_contenu btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>
                                                    </td>
                                                    <?php endif; ?>
                                                </tr>
                                                <?php endforeach; ?>                                               
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
                <div class="col col-3">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Statut Candidat TDC</header>
                            <?php if (User::can('create_type_content')): ?>
                            <div class="block-head-btns pull-right">
                            	<button type="button" data-type_id="8" class="add_type_content btn btn-icon btn-primary pull-right tooltips" title="Ajouter"><span class="glyphicon glyphicon-plus"></span></button>
                            </div>
                            <?php endif; ?>
                        </div>
						
						<div class="block-body small">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_8">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $controls = Control::getControlByType(8) ?>
                                                <?php foreach($controls as $control): ?>
                                                <tr <?php if ($control['status'] == 0): ?>class="inactive"<?php endif; ?>>
                                                    <td><?php echo $control['name'] ?></td>
                                                    <?php if (User::can('edit_type_content')): ?>
                                                    <td class="actions_button">
                                                        <button type="button" data-type_id="8" data_control_id="<?php echo $control['id'] ?>" class="edit_type_contenu btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>
                                                    </td>
                                                    <?php endif; ?>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
                <div class="col col-3">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Statut Entreprise TDC</header>
                            <?php if (User::can('create_type_content')): ?>
                            <div class="block-head-btns pull-right">
                            	<button type="button" data-type_id="10" class="add_type_content btn btn-icon btn-primary pull-right tooltips" title="Ajouter"><span class="glyphicon glyphicon-plus"></span></button>
                            </div>
                            <?php endif; ?>
                        </div>
						
						<div class="block-body small">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_10">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $controls = Control::getControlByType(10) ?>
                                                <?php foreach($controls as $control): ?>
                                                <tr <?php if ($control['status'] == 0): ?>class="inactive"<?php endif; ?>>
                                                    <td><?php echo $control['name'] ?></td>
                                                    <?php if (User::can('edit_type_content')): ?>
                                                    <td class="actions_button">
                                                        <button type="button" data-type_id="9" data_control_id="<?php echo $control['id'] ?>" class="edit_type_contenu btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>
                                                    </td>
                                                    <?php endif; ?>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
                <div class="col col-3">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Statut Mission</header>
                            <?php if (User::can('create_type_content')): ?>
                            <div class="block-head-btns pull-right">
                            	<button type="button" data-type_id="9" class="add_type_content btn btn-icon btn-primary pull-right tooltips" title="Ajouter"><span class="glyphicon glyphicon-plus"></span></button>
                            </div>
                            <?php endif; ?>
                        </div>
						
						<div class="block-body small">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_9">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $controls = Control::getControlByType(9) ?>
                                                <?php foreach($controls as $control): ?>
                                                <tr <?php if ($control['status'] == 0): ?>class="inactive"<?php endif; ?>>
                                                    <td><?php echo $control['name'] ?></td>
                                                    <?php if (User::can('edit_type_content')): ?>
                                                    <td class="actions_button">
                                                        <button type="button" data-type_id="9" data_control_id="<?php echo $control['id'] ?>" class="edit_type_contenu btn btn-icon btn-info tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>
                                                    </td>
                                                    <?php endif; ?>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
				
			</div><!-- / row -->
            
            <div class="row">
				
				<div class="col col-6">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Couleurs Statut TDC et PDF</header>
                        </div>
						
						<div class="block-body">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_zone">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $pdfColors = Control::getColorPDF(); ?>
                                                <?php foreach($pdfColors as $pdfColor): ?>
                                                <tr data-id="">
                                                    <td align="center"><?php echo $pdfColor['id'] ?></td>
                                                    <td class="pdf_element"><?php echo $pdfColor['bg_label'] ?></td>
                                                    <td class="pdf_color_selected"><span class="colors_pdf" style="background-color:#<?php echo $pdfColor['bg_hex'] ?>;"></span></td>
                                                    <td class="pdf_color_selector sep">
                                                        <select class="frm_select_colors" data-id="<?php echo $pdfColor['id'] ?>" data-field="bg_hex">
                                                            <?php $colors = Control::$pdf_colors ?>
                                                            <?php foreach($colors as $color): ?>
                                                            <option 
                                                                value="<?php echo $color ?>"
                                                                style="background-color:#<?php echo $color ?>"
                                                                <?php if ($pdfColor['bg_hex'] == $color): ?>selected="selected"<?php endif; ?>
                                                                >
                                                                <?php echo $color ?>
                                                            </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                    <td class="pdf_element"><?php echo $pdfColor['font_label'] ?></td>
                                                    <td class="pdf_color_selected"><span class="colors_pdf" style="background-color:#<?php echo $pdfColor['font_hex'] ?>;"></span></td>
                                                    <td class="pdf_color_selector">
                                                        <select class="frm_select_colors" data-id="<?php echo $pdfColor['id'] ?>" data-field="font_hex">
                                                            <?php $colors = Control::$pdf_colors ?>
                                                            <?php foreach($colors as $color): ?>
                                                            <option 
                                                                value="<?php echo $color ?>" 
                                                                style="background-color:#<?php echo $color ?>"
                                                                <?php if ($pdfColor['font_hex'] == $color): ?>selected="selected"<?php endif; ?>
                                                                >
                                                                <?php echo $color ?>
                                                            </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
                
                <?php if (User::can('read_type_content')): ?>
                <div class="col col-3">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Couleurs TDC Candidats</header>
                        </div>
						
                        <div class="block-body">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_color_tdc">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $colorstdc = Control::getColorTDC(); ?>
                                                <?php $count = 1; ?>
                                                <?php foreach($colorstdc as $colortdc): ?>
                                                <tr>
                                                    <td align="center"><?php echo $colortdc['id'] ?></td>
                                                    <td align="center">
                                                        <input type="text" value="<?php echo $colortdc['hex'] ?>" id="colorPick_<?php echo $count ?>" class="colorPick" style="background-color:#<?php echo $colortdc['hex'] ?>;">
                                                    </td>
                                                </tr>
                                                <?php $count++; ?>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
                        						
					</div>
				</div><!-- /col -->
                <?php endif; ?>
                
                <?php if (User::can('read_type_content')): ?>
                <div class="col col-3">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-th"></i>Couleurs TDC Entreprises</header>
                        </div>
						
                        <div class="block-body">
							<div class="nano">
                            	<div class="nano-content">
                                	<div class="type_contenu_color_tdc">
                                        <table class="table-list compact inside-nano no-head fixed-actions">
                                            <tbody>
                                                <?php $colorstdc = Control::getColorTDCEntreprise(); ?>
                                                <?php $count = 1; ?>
                                                <?php foreach($colorstdc as $colortdc): ?>
                                                <tr>
                                                    <td align="center"><?php echo $colortdc['id'] ?></td>
                                                    <td align="center">
                                                        <input type="text" value="<?php echo $colortdc['hex'] ?>" id="colorPickEntreprise_<?php echo $count ?>" class="colorPickEntreprise" style="background-color:#<?php echo $colortdc['hex'] ?>;">
                                                    </td>
                                                </tr>
                                                <?php $count++; ?>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                	</div>
                            	</div>
                        	</div>
						</div><!-- / block-body -->
                        						
					</div>
				</div><!-- /col -->
                <?php endif; ?>
				
			</div><!-- / row -->
            
        
        </div><!-- /contentWrap -->
        
    </div><!-- /base --> 
	
	<?php require_once './views/debug.php'; ?>   
    
</body>
</html>
<script>
$(document).ready(function(){
	<?php if (User::can('edit_type_content')): ?>		
	$('.colorPick').each(function(index){
		var id = '#colorPick_'+ (index+1);
        var color_id = index+1;
		$(id).ColorPicker({
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			},
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				// save the value by ajax here
                // alert($(id).val());
                
                $.ajax({
                    url: AJAX_HANDLER + '/colortdc',
                    type: 'POST',
                    dataType : 'json',
                    data: {hex:$(id).val(), id:color_id},
                    cache: false,
                    success: function (result) { 
                        Notif.Show(result.msg, result.type, true, 3000);
                    }
                });
                
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				$(id).css({'background-color':'#' + hex});
				$(id).val(hex);
			}
		});
	});
    
    $('.colorPickEntreprise').each(function(index){
		var id = '#colorPickEntreprise_'+ (index+1);
        var color_id = index+1;
		$(id).ColorPicker({
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			},
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				// save the value by ajax here
                // alert($(id).val());
                
                $.ajax({
                    url: AJAX_HANDLER + '/colortdcentreprise',
                    type: 'POST',
                    dataType : 'json',
                    data: {hex:$(id).val(), id:color_id},
                    cache: false,
                    success: function (result) { 
                        Notif.Show(result.msg, result.type, true, 3000);
                    }
                });
                
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				$(id).css({'background-color':'#' + hex});
				$(id).val(hex);
			}
		});
	});
    
    // color pdf
    $(document).on('change', 'select.frm_select_colors', function(){
        var element = $(this);
        var hex_value = $(this).val();
        var id = $(this).attr('data-id');
        var field = $(this).attr('data-field');
        $.ajax({
            url: AJAX_HANDLER + '/colorpdf',
            type: 'POST',
            dataType : 'json',
            data: {id:id, field:field, hex_value:hex_value},
            cache: false,
            success: function (result) { 
                Notif.Show(result.msg, result.type, true, 3000);
                element.parent('td.pdf_color_selector').prev('td.pdf_color_selected').find('span.colors_pdf').css('background-color', '#'+hex_value);
            }
        });
    });
    
	<?php endif; ?>
        
    <?php if (User::can('create_type_content')): ?>TypeContent.Add();<?php endif; ?> 
    <?php if (User::can('edit_type_content')): ?>TypeContent.Edit();<?php endif; ?>  

});  

var TypeContent = {
    <?php if (User::can('create_type_content')): ?>
    Add: function (){
        $(document).on('click', '.add_type_content', function(){
            var type_id = $(this).attr('data-type_id');
            Modal.Show(AJAX_UI + '/add-type-content?type_id='+type_id, null, null, '<span class="glyphicon glyphicon-cog"></span>', 50);
        });
    },
    <?php endif; ?>
    <?php if (User::can('edit_type_content')): ?>
    Edit: function (){
        $(document).on('click', '.edit_type_contenu', function(){
            var type_id = $(this).attr('data-type_id');
            var control_id = $(this).attr('data_control_id');
            Modal.Show(AJAX_UI + '/add-type-content?type_id='+type_id+'&control_id='+control_id, null, null, '<span class="glyphicon glyphicon-cog"></span>', 50);
        });
    },
    <?php endif; ?>  
}

<?php if (User::can('create_type_content', 'edit_type_content')): ?>
function frm_create_type_content_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function reloadtypecontent(type_id) {
    var url = AJAX_UI+'/reload-type-content?type_id='+type_id;
    
    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('.type_contenu_'+type_id).html(result);
        }
    });
}
<?php endif; ?>
</script>