/**
 * var regMail: reg expression to validate email
 * @return boolean true or false
 * if true email address is valid else false
 * @usage regMail.test(value) == false
 */
var regMail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

/**
 * to validate aplhanumeric
 * allowed values : 0123456789abcdefghijklmnopqrstuvwxyz
 * @usage regAlpha.test(value) 
 * @return true or false
 */
var regAlphaNumeric = /^[0-9a-zA-Z]+$/;

/**
 * to validate aplhanumeric with spaces and other characters like single quote etc
 * allowed values : 0123456789abcdefghijklmnopqrstuvwxyz
 * @usage regAlpha.test(value) 
 * @return true or false
 */
var regAddress = /^[0-9a-zA-Z\s\'\-]+$/;

var regCodeVoie = /^[0-9A-Z]+$/;

var regNumVoie = /^[0-9A-Z]+$/;

/**
 * @source http://urlregex.com/
 * @type RegExp
 */
var regLink = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/;
        
/**
 * function to check for numeric values
 * @return true if numeric, false if not numeric
 */
function IsNumeric(sText){
   var ValidChars = "0123456789";
   var IsNumber = true;
   var Char;
 
   for (i = 0; i < sText.length && IsNumber == true; i++) { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) {
         IsNumber = false;
      }
   }
   return IsNumber;
}

function addslashes(str) {
  //  discuss at: http://phpjs.org/functions/addslashes/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Ates Goral (http://magnetiq.com)
  // improved by: marrtins
  // improved by: Nate
  // improved by: Onno Marsman
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
  //    input by: Denny Wardhana
  //   example 1: addslashes("kevin's birthday");
  //   returns 1: "kevin\\'s birthday"

  return (str + '')
    .replace(/[\\"']/g, '\\$&')
    .replace(/\u0000/g, '\\0');
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function hasWhiteSpace(str) {
    if (str.indexOf(' ') == -1) {
            return false;
    } else {
            return true;
    }
}

/**
 * <p>Validate Ipv4 Address</p>
 * @param String value
 * @returns Boolean True/False
 */
function validate_ipv4(value) {
    var split = value.split('.');
    if (split.length != 4) 
        return false;
            
    for (var i=0; i < split.length; i++) {
        var s = split[i];
        if (s.length == 0 || isNaN(s) || s < 0 || s > 255)
            return false;
    }
    return true;
}

/**
 * <p>Validate Ipv4 plage Address. It ends with .0 ex 12.45.23.0</p>
 * @param String value
 * @returns Boolean True/False
 */
function validate_ipv4_plage(value) {
    var split = value.split('.');
    if (split.length != 4) 
        return false;
            
    for (var i=0; i < split.length; i++) {
        var s = split[i];
        if (i <= 2) {
            if (s.length == 0 || isNaN(s) || s < 0 || s > 255)
                return false;
        } else {
            if (s.length == 0 || isNaN(s) || s != 0)
                return false;
        }
        
    }
    return true;
}