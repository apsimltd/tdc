$(document).ready(function(){
	
    $(document).on('click', '.delete_comment', function (){
        var comment_id = $(this).attr('data-comment_id');
        var candidat_id = $(this).attr('data-candidat_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer commentaire', AJAX_HANDLER + '/delete-candidat-comment?comment_id='+comment_id+'&candidat_id='+candidat_id);
    });
    
    $(document).on('click', '.edit_comment', function (){
        var comment_id = $(this).data('comment_id');
        var mission_id = $(this).data('mission_id');
        Modal.Show(AJAX_UI + '/edit-candidat-comment?comment_id='+comment_id+'&mission_id='+mission_id, null, null, '<span class="glyphicon glyphicon-user"></span>', 50);
    });
    
    $(document).on('click', '.delete_motivation', function (){
        var motivation_id = $(this).attr('data-motivation_id');
        var candidat_id = $(this).attr('data-candidat_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer motivation', AJAX_HANDLER + '/delete-candidat-motivation?motivation_id='+motivation_id+'&candidat_id='+candidat_id);
    });
    
    $(document).on('click', '.edit_motivation', function (){
        var motivation_id = $(this).data('motivation_id');
        var mission_id = $(this).data('mission_id');
        Modal.Show(AJAX_UI + '/edit-candidat-motivation?motivation_id='+motivation_id+'&mission_id='+mission_id, null, null, '<span class="glyphicon glyphicon-user"></span>', 50);
    });
    
    $(document).on('click', '.comment_zoom', function(){
        var candidat_id = $(this).data('candidat_id');
        var mission_id = $(this).data('mission_id');
        Modal.Show(AJAX_UI + '/zoom-candidat-comment?candidat_id='+candidat_id+'&mission_id='+mission_id, null, null, '<span class="glyphicon glyphicon-user"></span>', 50);
    });
    
    $(document).on('click', '.candidat_mission_history', function(){
        var candidat_id = $(this).attr('data-candidat_id');
        Modal.Show(AJAX_UI + '/zoom-candidat-mission-history?candidat_id='+candidat_id, null, null, '<span class="glyphicon glyphicon-user"></span>', 70);
    });
    
    $(document).on('click', '.generate_pdf_candidat', function(){
        var candidat_id = $(this).attr('data-candidat_id');
        Modal.Show(AJAX_UI + '/candidat-pdf-selection?candidat_id='+candidat_id, null, null, '<i class="fa fa-file-pdf"></i>', 50, 'modal-candidate-pdf');
    });
    
    $(document).on('click', '.pdf-candidat', function (){
        
        var file_format = $('#file_format').val();
        var candidat_id = $(this).attr('data-candidat_id');
        var societe = $(this).attr('data-societe');
                
        if (file_format === "PDF") {
            var url = AJAX_HANDLER+'/generate-pdf-candidat?id='+candidat_id+'&societe='+societe;
            $.ajax({
                url: url,
                dataType : 'json',
                cache: false,
                success: function (result, status) {
                    if (status === "success" && result.status === "OK") {
                        $('#modal-candidate-pdf button.close').trigger('click');
                        location.href = BASE_URL + '/download-pdf?file='+result.file;
                    }
                }
            });
        } else if (file_format === "WORD") {
            var url = AJAX_HANDLER+'/generate-word-candidat?id='+candidat_id+'&societe='+societe;
            $.ajax({
                url: url,
                dataType : 'json',
                cache: false,
                success: function (result, status) {
                    if (status === "success" && result.status === "OK") {
                        $('#modal-candidate-pdf button.close').trigger('click');
                        location.href = BASE_URL + '/download-word?file='+result.file;
                    }
                }
            });
        }
            
    });
       
}); // end document ready function

function frm_edit_candidat_add_comment_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        if ($('.commentZoom').length) {
            $('.commentZoom').removeClass('ok').removeClass('error').val('');
        }
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}



function frm_edit_candidat_add_motivation_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_edit_candidat_edit_motivation_ajax_success (result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_search_candidat_before_submit (obj) {
    
    var field_status = [];
    var valid = false;
    var form = $('#frm_search_candidat');
    
    // textbox
    form.find('input.frm_text').each(function() { 
        if ($(this).val() == "") {
            field_status.push(false);
        } else {
            field_status.push(true);
        }
    });

    // chosen select
    form.find('select.frm_chosen').each(function() {
        if ($(this).val() == "") {
            field_status.push(false);
        } else {
            field_status.push(true);
        }
    });
    
    // checkbox
    form.find('.icheckbox_square-blue').each(function(){
        if ($(this).hasClass('checked')) {
            field_status.push(true);
        } else {
            field_status.push(false);
        }
    });

    // checking if there is a false value
    // atleast 1 false value must return false to prevent form subsmission
    if (field_status.length > 0) {
        $.each(field_status, function(index, value){
            if (value === true) {
                valid = true;
            }
        });
    }
    
    if (!valid) {
        alert('Au moins un champ doit être sélectionné pour rechercher un candidat');
        return false;
    } else {
        return true;
    }

}

function frm_search_candidat_ajax_success (result, status) {
    $('.candidats-listing').html(result);
    Table.Init();
}

function reloadcomment(candidat_id) {
    
    if ($('.historiques_comment_wrap').length) {
    
        // ajax call to reload comment
        var url = AJAX_UI+'/reload-candidat-comment?id='+candidat_id;

        $.ajax({
            url: url,
            dataType : 'html',
            cache: false,
            success: function (result, status) {
                $('.add_new_comment').val('').removeClass('ok').removeClass('error');
                $('.historiques_comment_wrap').html(result);
            }
        });
        
    }
}

function reloadmotivation(candidat_id) {
    
    if ($('.historiques_motivation_wrap').length) {
    
        // ajax call to reload motivation
        var url = AJAX_UI+'/reload-candidat-motivation?id='+candidat_id;

        $.ajax({
            url: url,
            dataType : 'html',
            cache: false,
            success: function (result, status) {
                $('.add_new_motivation').val('').removeClass('ok').removeClass('error');
                $('.historiques_motivation_wrap').html(result);
            }
        });
        
    }
}