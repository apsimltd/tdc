$(document).ready(function(){
    
    $(document).on('click', '.see_child_companies', function(){
        
        if ($(this).hasClass('glyphicon-plus')) {
            
            $(this).removeClass('glyphicon-plus').addClass('glyphicon-minus');
            
            $('tr.ajaxRowL').remove();

            var tr = $(this).parent('td').parent('tr');
            var id = $(this).data('entreprise_id');
            var url = AJAX_HANDLER + '/get-child-companies';

            $.ajax({
                url: url,
                dataType : 'html',
                type: 'POST',
                data: {id:id},
                cache: false,
                success: function (result) {
                    tr.after(result);
                }
            });
        
        } else {
            
            $(this).removeClass('glyphicon-minus').addClass('glyphicon-plus');
            $('tr.ajaxRowL').remove();
            
        }
        
    });
    
    $(document).on('click', '.entreprise_blacklist', function(){
        var entreprise_id = $(this).data('entreprise_id');
        var me = $(this);
        var op = 'select';
        if (me.hasClass('active')) {
            op = 'deselect';
        } else {
            op = 'select';
        }
        
        var url = AJAX_HANDLER+'/entreprise-blacklist?id='+entreprise_id+'&op='+op;
    
        $.ajax({
            url: url,
            dataType : 'json',
            cache: false,
            success: function (result, status) {
                if (op == 'select') {
                    me.addClass('active');
                } else {
                    me.removeClass('active');
                }
                if (status === "success" && result.status === "OK") {
                    Notif.Show(result.msg, result.type, true, 5000);
                } else if (status === "success" && result.status === "NOK") {
                    Notif.Show(result.msg, result.type, true, 5000);
                }

            }
        });
        
    });
    
    $(document).on('click', '.entreprise_mission_history', function(){
        var entreprise_id = $(this).attr('data-entreprise_id');
        Modal.Show(AJAX_UI + '/zoom-entreprise-mission-history?entreprise_id='+entreprise_id, null, null, '<span class="glyphicon glyphicon-user"></span>', 70);
    });
    
    // delete document
    $(document).on('click', '.delete_document', function (){
        var id = $(this).attr('data-document_id');
        var entreprise_id = $(this).attr('data-entreprise_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer document', AJAX_HANDLER + '/delete-entreprise-file?id='+id+'&entreprise_id='+entreprise_id);
    });
    
    $(document).on('click', '.delete_comment', function (){
        var comment_id = $(this).attr('data-comment_id');
        var entreprise_id = $(this).attr('data-entreprise_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer commentaire', AJAX_HANDLER + '/delete-entreprise-comment?comment_id='+comment_id+'&entreprise_id='+entreprise_id);
    });
    
    $(document).on('click', '.edit_comment', function (){
        var comment_id = $(this).attr('data-comment_id');
        var entreprise_id = $(this).attr('data-entreprise_id');
        Modal.Show(AJAX_UI + '/edit-entreprise-comment?comment_id='+comment_id+'&entreprise_id='+entreprise_id, null, null, '<span class="glyphicon glyphicon-user"></span>', 50);
    });
    
    $(document).on('click', '.comment_zoom', function(){
        var entreprise_id = $(this).attr('data-entreprise_id');
        Modal.Show(AJAX_UI + '/zoom-entreprise-comment?entreprise_id='+entreprise_id, null, null, '<span class="glyphicon glyphicon-user"></span>', 50);
    });
        
    $(document).on('click', '.generate_pdf_entreprise', function(){
        var entreprise_id = $(this).attr('data-entreprise_id');
        Modal.Show(AJAX_UI + '/entreprise-pdf-selection?entreprise_id='+entreprise_id, null, null, '<i class="fa fa-file-pdf"></i>', 50, 'modal-entreprise-pdf');
    });
    
    $(document).on('click', '.pdf-entreprise', function (){
        
        var file_format = $('#file_format').val();
        var entreprise_id = $(this).attr('data-entreprise_id');
        var societe = $(this).attr('data-societe');
                
        if (file_format === "PDF") {
            var url = AJAX_HANDLER+'/generate-pdf-entreprise?id='+entreprise_id+'&societe='+societe;
            $.ajax({
                url: url,
                dataType : 'json',
                cache: false,
                success: function (result, status) {
                    if (status === "success" && result.status === "OK") {
                        $('#modal-entreprise-pdf button.close').trigger('click');
                        location.href = BASE_URL + '/download-pdf?file='+result.file;
                    }
                }
            });
        } else if (file_format === "WORD") {
            var url = AJAX_HANDLER+'/generate-word-entreprise?id='+entreprise_id+'&societe='+societe;
            $.ajax({
                url: url,
                dataType : 'json',
                cache: false,
                success: function (result, status) {
                    if (status === "success" && result.status === "OK") {
                        $('#modal-entreprise-pdf button.close').trigger('click');
                        location.href = BASE_URL + '/download-word?file='+result.file;
                    }
                }
            });
        }
            
    });
       
}); // end document ready function

function frm_edit_entreprise_add_comment_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        if ($('.commentZoom').length) {
            $('.commentZoom').removeClass('ok').removeClass('error').val('');
        }
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_edit_entreprise_edit_comment_ajax_success (result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_search_entreprise_before_submit (obj) {
    
    var field_status = [];
    var valid = false;
    var form = $('#frm_search_entreprise');
    
    // textbox
    form.find('input.frm_text').each(function() { 
        if ($(this).val() == "") {
            field_status.push(false);
        } else {
            field_status.push(true);
        }
    });

    // chosen select
    form.find('select.frm_chosen').each(function() {
        if ($(this).val() == "") {
            field_status.push(false);
        } else {
            field_status.push(true);
        }
    });
    
    // checkbox
//    form.find('.icheckbox_square-blue').each(function(){
//        if ($(this).hasClass('checked')) {
//            field_status.push(true);
//        } else {
//            field_status.push(false);
//        }
//    });

    // checking if there is a false value
    // atleast 1 false value must return false to prevent form subsmission
    if (field_status.length > 0) {
        $.each(field_status, function(index, value){
            if (value === true) {
                valid = true;
            }
        });
    }
    
    if (!valid) {
        alert('Au moins un champ doit être sélectionné pour rechercher une entreprise');
        return false;
    } else {
        return true;
    }

}

function frm_search_entreprise_ajax_success (result, status) {
    $('.entreprises-listing').html(result);
    Table.Init();
}

function reloadcomment(entreprise_id) {
    
    if ($('.historiques_comment_wrap').length) {
    
        // ajax call to reload comment
        var url = AJAX_UI+'/reload-entreprise-comment?id='+entreprise_id;

        $.ajax({
            url: url,
            dataType : 'html',
            cache: false,
            success: function (result, status) {
                $('.add_new_comment').val('').removeClass('ok').removeClass('error');
                $('.historiques_comment_wrap').html(result);
            }
        });
        
    }
}

function frm_edit_entreprise_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        if (result.callback != undefined && result.param != undefined) {
            Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
        } else {
            Notif.Show(result.msg, result.type, true, 5000);
        }
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }    
}

function frm_edit_entreprise_add_files_before_submit(obj) {
    
    var uploadFlag = true;
    var numLabel = parseInt($('#uploadFlag').val());
    
    for(var i = 1; i <= numLabel; i++) {
        
        if ($('#file_'+numLabel).val() == ""){
            $('#uploadLabel_'+numLabel).addClass('error');
            uploadFlag = false;
        }
        
        if ($('#uploadLabel_'+numLabel).length) {
            if ($('#uploadLabel_'+numLabel).hasClass('error')) {
                uploadFlag = false;
            }
        }
    }
    
    return uploadFlag;
    
}