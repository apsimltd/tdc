$(document).ready(function() {

    "use strict";

    var options = {
        // events_source: 'events.json.php',
        events_source: AJAX_HANDLER + '/events.json.php',
//        events_source: function () { 
//            return []; 
//        },
        width: '88%',
        view: 'month',
        tmpl_path: BASE_URL + '/tmpls/',
        tmpl_cache: false,
        day: 'now',
        first_day: 1,
        language: 'fr-FR',
        weekbox: true,
        time_start: '00:00',
        time_end: '24:00',
        time_split: '60',
        onAfterEventsLoad: function(events) {
            if(!events) {
                return;
            }
        },
        onAfterViewLoad: function(view) {
            $('.MonthName').text(this.getTitle());
            $('.btn-group button').removeClass('active');
            $('button[data-calendar-view="' + view + '"]').addClass('active');
        },
        classes: {
            months: {
                general: 'label'
            }
        }
    };

    var calendar = $('#calendar').calendar(options);

    $('.CalendarNav button[data-calendar-nav]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.navigate($this.data('calendar-nav'));
            Tooltip.Init();
        });
    });

    $('.CalendarViews button[data-calendar-view]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.view($this.data('calendar-view'));
            Tooltip.Init();
        });
    }); 
    
    $('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
        //e.preventDefault();
        //e.stopPropagation();
    });

//    calendar.setOptions({modal: '#events-modal'});
//    calendar.view();
         
});