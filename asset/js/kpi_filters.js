$(document).ready(function(){
    
    KPI_CLIENTS.SelectManager();
    KPI_CLIENTS.SelectCDR();
    KPI_CLIENTS.StartEndPicker();
        
}); // end document ready function

var KPI_CLIENTS = {
    SelectManager: function () {
        $(document).on('change', '#manager_id', function(){
            if ($(this).val() != "") {
                
                var start_date = $('#start_date').val();
                var end_date = $('#end_date').val();
                
                var url = AJAX_UI + '/kpi-load-missions-by-manager-id';
                $.ajax({
                    url: url,
                    dataType : 'html',
                    type: 'POST',
                    cache: false,
                    data:{manager_id:$(this).val(), start_date:start_date, end_date:end_date},
                    success: function (result) {                
                        $('.kpi_filter_mission').html(result);
                    }
                });
                
                var url = AJAX_UI + '/kpi-load-cdr-by-manager-id';
                $.ajax({
                    url: url,
                    dataType : 'html',
                    type: 'POST',
                    cache: false,
                    data:{manager_id:$(this).val()},
                    success: function (result) {                
                        $('.kpi_filter_cdr').html(result);
                    }
                });

            } else {
                $('.kpi_filter_cdr').html("");
                $('.kpi_filter_mission').html("");
            }
        });
    },
    SelectCDR: function () {
        $(document).on('change', '#mission_id', function(){
            if ($(this).val() != "") {
            
                var url = AJAX_UI + '/kpi-load-cdr-by-mission-id';
                $.ajax({
                    url: url,
                    dataType : 'html',
                    type: 'POST',
                    cache: false,
                    data:{mission_id:$(this).val()},
                    success: function (result) {                
                        $('.kpi_filter_cdr').html(result);
                    }
                });

            } else {
                
                var manager_id = $('#manager_id').val();
                var url = AJAX_UI + '/kpi-load-cdr-by-manager-id';
                $.ajax({
                    url: url,
                    dataType : 'html',
                    type: 'POST',
                    cache: false,
                    data:{manager_id:manager_id},
                    success: function (result) {                
                        $('.kpi_filter_cdr').html(result);
                    }
                });
                
            }
        });
    },
    StartEndPicker: function (){
        $("#start_date").on().datepicker({
            dateFormat: 'yy-mm-dd',
            regional: 'fr',
            // minDate: '-7y', 
            minDate: "2020-02-01",
            maxDate: '+1d',
            showButtonPanel: true,
            onClose: function(selectedDate) {
                $("#end_date").datepicker("option", "minDate", selectedDate); 
            },
        });
        $("#end_date").on().datepicker({
            dateFormat: 'yy-mm-dd',
            regional: 'fr',
            minDate: "2020-02-01", 
            maxDate: '+1d',
            showButtonPanel: true,
            onClose: function(selectedDate) {
                $("#start_date").datepicker("option", "maxDate", selectedDate);
            }
        });
    },
}