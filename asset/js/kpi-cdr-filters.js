$(document).ready(function(){
    
    KPI_CDR.SelectCDR();
    KPI_CDR.StartEndPicker();
        
}); // end document ready function

var KPI_CDR = {
    SelectCDR: function () {
        $(document).on('change', '#cdr_id', function(){
            if ($(this).val() != "") {
                
                var start_date = $('#start_date').val();
                var end_date = $('#end_date').val();
                
                var url = AJAX_UI + '/kpi-load-missions-by-cdr-id';
                $.ajax({
                    url: url,
                    dataType : 'html',
                    type: 'POST',
                    cache: false,
                    data:{cdr_id:$(this).val(), start_date:start_date, end_date:end_date},
                    success: function (result) {                
                        $('.kpi_filter_mission').html(result);
                    }
                });
            } else {
                $('.kpi_filter_mission').html("");
            }
        });
    },
    StartEndPicker: function (){
        $("#start_date").on().datepicker({
            dateFormat: 'yy-mm-dd',
            regional: 'fr',
            // minDate: '-7y', 
            minDate: "2020-02-01",
            maxDate: '+1d',
            showButtonPanel: true,
            onClose: function(selectedDate) {
                $("#end_date").datepicker("option", "minDate", selectedDate); 
            },
        });
        $("#end_date").on().datepicker({
            dateFormat: 'yy-mm-dd',
            regional: 'fr',
            minDate: "2020-02-01", 
            maxDate: '+1d',
            showButtonPanel: true,
            onClose: function(selectedDate) {
                $("#start_date").datepicker("option", "maxDate", selectedDate);
            }
        });
    },
}