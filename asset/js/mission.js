$(document).ready(function(){
	
    $(document).on('click', '.delete_comment', function (){
        var comment_id = $(this).attr('data-comment_id');
        var mission_id = $(this).attr('data-mission_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer commentaire', AJAX_HANDLER + '/delete-mission-comment?comment_id='+comment_id+'&mission_id='+mission_id);
    });
    
    $(document).on('click', '.delete_mission', function (){ // this one only change status
        var mission_id = $(this).attr('data-mission_id');
        Dialog.Show('Veuillez confirmer', 'Archiver mission', AJAX_HANDLER + '/delete-mission?mission_id='+mission_id);
    });
    
    $(document).on('click', '.delete_mission_total', function (){ // this removes the mission completely
        var mission_id = $(this).attr('data-mission_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer mission', AJAX_HANDLER + '/delete-mission-real?mission_id='+mission_id);
    });
    
    $(document).on('click', '.edit_comment', function (){
        var comment_id = $(this).attr('data-comment_id');
        Modal.Show(AJAX_UI + '/edit-mission-comment?comment_id='+comment_id, null, null, '<span class="glyphicon glyphicon-tasks"></span>', 50);
    });
    
    $(document).on('click', '.comment_zoom', function(){
        var mission_id = $(this).attr('data-mission_id');
        Modal.Show(AJAX_UI + '/zoom-mission-comment?mission_id='+mission_id, null, null, '<span class="glyphicon glyphicon-tasks"></span>', 50);
    });
    
    $(document).on('click', '.generate_pdf_mission', function(){
        var mission_id = $(this).attr('data-mission_id');
        var url = AJAX_HANDLER+'/generate-pdf-mission?id='+mission_id;
        $.ajax({
            url: url,
            dataType : 'json',
            cache: false,
            success: function (result, status) {
                if (status === "success" && result.status === "OK") {
                    location.href = BASE_URL + '/download-pdf?file='+result.file;
                }
            }
        });
    });
    
    // duplicate mission
    $(document).on('click', '.duplicate_mission', function (){
        var mission_id = $(this).attr('data-mission_id');
        var url = AJAX_HANDLER+'/duplicate-mission?id='+mission_id;
        $.ajax({
            url: url,
            dataType : 'json',
            cache: false,
            success: function (result, status) {
                if (status === "success" && result.status === "OK") {
                    location.href = result.url;
                }
            }
        });
    });
    
}); // end document ready function

function frm_edit_mission_add_comment_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        if ($('.commentZoom').length) {
            $('.commentZoom').removeClass('ok').removeClass('error').val('');
        }
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_edit_mission_edit_comment_ajax_success (result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function reloadcomment(mission_id) {
    
    if ($('.historiques_comment_wrap').length) {
    
        // ajax call to reload comment
        var url = AJAX_UI+'/reload-mission-comment?id='+mission_id;

        $.ajax({
            url: url,
            dataType : 'html',
            cache: false,
            success: function (result, status) {
                $('.add_new_comment').val('').removeClass('ok').removeClass('error');
                $('.historiques_comment_wrap').html(result);
            }
        });
        
    }
}

function frm_search_mission_before_submit (obj) {
    
    var field_status = [];
    var valid = false;
    var form = $('#frm_search_mission');
    
    // textbox
    form.find('input.frm_text').each(function() { 
        if ($(this).val() == "") {
            field_status.push(false);
        } else {
            field_status.push(true);
        }
    });

    // chosen select
    form.find('select.frm_chosen').each(function() {
        if ($(this).val() == "") {
            field_status.push(false);
        } else {
            field_status.push(true);
        }
    });

    // checking if there is a false value
    // atleast 1 false value must return false to prevent form subsmission
    if (field_status.length > 0) {
        $.each(field_status, function(index, value){
            if (value === true) {
                valid = true;
            }
        });
    }
    
    if (!valid) {
        alert('Au moins un champ doit être sélectionné pour rechercher une mission');
        return false;
    } else {
        return true;
    }

}

function frm_search_mission_ajax_success (result, status) {
    $('.missions-listing').html(result);
    Table.Init();
}