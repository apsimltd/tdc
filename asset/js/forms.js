/**
 * <p>Another level of form subsmission to bypass any core form behaviour 
 * to have a better web 3.0 rendering and faster and lighter for validations through 
 * ajax and in front end instead of server side validation</p>
 * @author Faardeen Madarbokas
 * @date 13/12/2016
 * @mail fmadarbokas@labf3.com
 * @issue contact faardeen
 * @usage
 * <p>Must follow the markup standards for this to work</p>
 * <p>if ajax subsmission add the class frm_ajax and attribute data-url having the url 
 * of the ajax post and data-type: json or html or text</p>
 * @template
 * <form class="frm_frm frm_ajax" name="frm_template_ajax" id="frm_template_ajax" data-url="ajax/form_template_ajax.html" data-type="html">
        <legend>This is the legende of a form</legend>
        <fieldset>
            <label>Optional field</label>
            <input type="text" class="frm_text" name="optional_field" autocomplete="off" data-validation="val_num">
        </fieldset>
        <fieldset>
            <label>Numeric</label>
            <input type="text" class="frm_text must" name="numeric" data-validation="val_num" autocomplete="off">
        </fieldset>
        <fieldset>
            <label>Checkbox</label>
            <input type="checkbox" name="lienType[]" value="C2E" class="frm_checkbox">
        </fieldset>
        <fieldset>
            <label>Radio Button</label>
            <input type="radio" name="radioName" value="Mr" class="frm_radio">Mr
            <input type="radio" name="radioName" value="Mrs" class="frm_radio">Mrs 
        </fieldset>
        <fieldset class="selectOption">
            <label>Select Box must</label>
            <select class="frm_select must" data-validation="val_blank" name="typeLien">
                    <option value="">A choisir</option>
                <option value="C2E_val">C2E</option>
                <option value="DSLE_val">DSLE</option>
                <option value="OPTIQUE_val">OPTIQUE</option>
            </select>
        </fieldset>
        <fieldset>
            <input type="button" value="Send" class="frm_button frm_submit" data-form="2"><!-- how much depth must traverse the DOM -->
        </fieldset>   
    </form>
 */

var forms = {
    radioCheckbox: function () {
        $('input.frm_checkbox').on().iCheck({
            handle: 'checkbox',
            checkboxClass: 'icheckbox_square-blue',
            cursor: true,
        });
        $('input.frm_radio').on().iCheck({
            handle: 'radio',
            radioClass: 'iradio_square-blue',
            cursor: true,
        });
    },
    selectChosen: function (){
        $('select.frm_chosen').each(function(){
            var element = $(this);
            if ($(this).hasClass('must') && $(this).hasClass('ok')) {
                var className = "must ok";
            } else if ($(this).hasClass('must')) {
                var className = "must";
            } else if ($(this).hasClass('ok')) {
                var className = "ok";
            } else {
                var className = "";
            }
            
            var width = 200;
            
            if ($(this).hasClass('large')) {
                width = 300;
            } else if ($(this).hasClass('filter')) {
                width = '100%';
            }
            
            /*
             * @source https://harvesthq.github.io/chosen/options.html
             */
            $(this).chosen({
                'no_results_text': 'Pas de Résultat',
                'width':width,
            });
            
            $(this).next('.chosen-container-single').find('.chosen-single').addClass(className);
            
        });
    },
    // multiselect option
    selectOptionMulti: function (){
        // http://loudev.com/#home
        // mutilselect.js plugin
        $('select.frm_select_multi').each(function() {
            var element = $(this);
            if ($(this).hasClass('must') && $(this).hasClass('ok')) {
                var className = "must ok";
            } else if ($(this).hasClass('must')) {
                var className = "must";
            } else if ($(this).hasClass('ok')) {
                var className = "ok";
            } else {
                var className = "";
            }

            $(this).multiSelect({
                cssClass: className,
                afterInit: function (container) { // Function to call after the multiSelect initilization.
                    // do nothing
                },
                afterSelect: function(value) { // Function to call after one item is selected.
                    forms.selectOptionMultiHelper(element, value);
                },
                afterDeselect: function(value) { // Function to call after one item is deselected.
                    forms.selectOptionMultiHelper(element, value);
                },
            });		
        });
    },
	
    selectOptionMultiHelper: function (obj, val){ // op = add or remove
        var helper = obj.parent('.frm_select_multi_wrapper').find('input.frm_select_multi_helper');
        var helperActualValue = helper.val();
        var helperNewValue = '';

        if (helperActualValue == "") { // if helper is blank and its the first value to be appended
            helperNewValue = val;
            helper.val(helperNewValue);
            forms.validateField(helper, 'multiselect');
            return true;
        } else { // second or more values selected
            // get all selected values and find its value
            var selectedValuesArray = [];
            obj.parent('.frm_select_multi_wrapper').find('.ms-container .ms-selection ul.ms-list li').each(function(index, element){
                if ($(this).css('display') != "none") {
                    var val = obj.find('option:eq('+index+')').attr('value');
                    selectedValuesArray.push(val);
                }
            });
            if (selectedValuesArray.length > 0) {
                $.each(selectedValuesArray, function(index, value){
                    helperNewValue += value;
                    if (index != (selectedValuesArray.length-1)) {
                        helperNewValue += '|';
                    }
                });
                helper.val(helperNewValue);
                forms.validateField(helper, 'multiselect');
                return true;
            } else {
                helper.val('');
                forms.validateField(helper, 'multiselect');
                return true;
            }
        }
    },
    addError: function (element) {
        $(element).removeClass('ok').addClass('error');
    },

    removeError: function (element) {
        $(element).removeClass('error').addClass('ok');
    },
	
    validateField: function (element, type) { // element:: the object e.g. textbox, type:: the type of element e.g textbox, select, radio ...
          
        if (type == "text" || type == "pass" || type == "textarea" || type == "select" || type == "multiselect" || type == "select_chosen") { // textbox & textarea
            var val = element.val();
        } else if (type == "file") {
            var val = element.find('input').val();
        }
		        
        // check if the field is compulsory
        var must = false;
        if (element.hasClass('must')) {
            must = true;
        }
        
        // select option
        if (type == "select" && must) {
            var validation = element.attr('data-validation');
            if (validation == "val_blank" && val == "") {
                forms.addError(element);
                return false;
            } else if (validation == "val_blank" && val != "") {
                forms.removeError(element, type);
                return true;
            }			
        }
        // end select box
        
        // select chosen
        if (type == "select_chosen" && must) {
            var validation = element.attr('data-validation');
            if (validation == "val_blank" && val == "") {
                element.next('.chosen-container-single').find('.chosen-single').removeClass('ok').addClass('error');
                return false;
            } else if (validation == "val_blank" && val != "") {
                element.next('.chosen-container-single').find('.chosen-single').removeClass('error').addClass('ok');
                return true;
            }
        }
        
        // multiselectbox
        if (type == "multiselect" && must) {
            var validation = element.attr('data-validation');
            if (validation == "val_blank" && val == "") {
                element.parent('.frm_select_multi_wrapper').find('.ms-container').removeClass('ok').addClass('error');
                return false;
            } else if (validation == "val_blank" && val != "") {
                element.parent('.frm_select_multi_wrapper').find('.ms-container').removeClass('error').addClass('ok');
                return true;
            }
        }
        // end multiselectbox
        
        // check if the field is optional and value is blank 
        // then no need of validation and return true
        if (!must && val == "") { forms.removeError(element, type); return true; }

        // start of validating field on validation
        // here all compulsory fields will come to execute
        var validation = element.attr('data-validation');
                
        // other forms elements
        switch(validation) {
			               
            case 'val_blank':
                if (val == "") {
                    forms.addError(element);
                    return false;
                } else { 
                    forms.removeError(element, type);
                    return true;
                }
                break;
			
            case 'val_blank_no_space': // value with no space
                if (val == "" || hasWhiteSpace(val) === true) {
                    forms.addError(element);
                    return false;
                } else { 
                    forms.removeError(element, type);
                    return true;
                }
                break;

            case 'val_email':
                if (val == "" || val.length < 7 || regMail.test(val) == false) {
                    forms.addError(element);
                    return false;
                } else { 
                    forms.removeError(element, type);
                    return true;
                }
                break;
                
            case 'val_num': //numeric field
                if (val == "" || !IsNumeric(val)) {
                    forms.addError(element);
                    return false;
                } else { 
                    forms.removeError(element, type);
                    return true;
                }
                break;

            case 'val_3cx': //numeric field
                if (val !== "" && !IsNumeric(val)) {
                    forms.addError(element);
                    return false;
                } else {
                    forms.removeError(element, type);
                    return true;
                }
                break;
                
            case 'val_sda': //numeric field
                if (val !== "" && val.length < 10) {
                    forms.addError(element);
                    return false;
                } else { 
                    forms.removeError(element, type);
                    return true;
                }
                break;
            
            case 'val_float': //numeric field
                if (val == "" || !$.isNumeric(val)) {
                    forms.addError(element);
                    return false;
                } else { 
                    forms.removeError(element, type);
                    return true;
                }
                break;
            
            case 'val_num_range_min': // range of values
                var max = parseInt(element.next().next('.frm_text.max').val());
                if (val == "" || !IsNumeric(val) || val > max) { 
                    forms.addError(element);
                    forms.addError(element.next().next('.frm_text.max'));
                    return false;   
                } else { 
                    forms.removeError(element, type);
                    forms.removeError(element.next().next('.frm_text.max'), type);
                    return true;
                }
                break;
            
            case 'val_num_range_max': // range of values
                var min = parseInt(element.prev().prev('.frm_text.min').val());
                if (val == "" || !IsNumeric(val) || val < min) { 
                    forms.addError(element);
                    forms.addError(element.prev().prev('.frm_text.min'));
                    return false;   
                } else { 
                    forms.removeError(element, type);
                    forms.removeError(element.prev().prev('.frm_text.min'), type);
                    return true;
                }
                break;
            
            case 'val_name':
            case 'val_alpha_num': // alpha numeric
                if (val == "" || regAlphaNumeric.test(val) == false) {
                    forms.addError(element);
                    return false;
                } else {
                    forms.removeError(element, type);
                    return true;
                }
                break;
			
            case 'val_alpha': // alpha only
                if (val == "" || IsNumeric(val)) {
                    forms.addError(element);
                    return false;
                } else {
                    forms.removeError(element, type);
                    return true;
                }
                break;
            
            // http://urlregex.com/
            case 'val_url': // alpha only
                if (val == "" || regLink.test(val) == false) {
                    forms.addError(element);
                    return false;
                } else {
                    forms.removeError(element, type);
                    return true;
                }
                break;
            
            case 'val_link': // validate link through ajax
                if (val == "") {
                    forms.addError(element);
                    return false;
                } else {	
                    ajaxBusy = true;
                    $.ajax({
                        url: AJAX_HANDLER+'/validate-link',
                        dataType : 'json',
                        // async: false, // dangerous => can freeze browser
                        type: 'POST',
                        data: {link:val},
                        cache: false,
                        success: function (result, status) {
                            if (status === "success" && result.status === "OK") {
                                forms.removeError(element, type);
                                return true;
                            } else if (status === "success" && result.status === "NOK"){
                                forms.addError(element);
                                return false;
                            }
                        }
                    });
                }
                break;
            
        } // end switch case
		
    },
	
    validateOnBlur: function () {
        // textbox // on chnage for number type fields
        $(document).on('blur', '.frm_text', function() { 
            forms.validateField($(this), 'text');
        });
        
        // password
        $(document).on('blur', '.frm_pass', function(){
            forms.validateField($(this), 'pass');
        });
        
        // select of type chosen
        $(document).on('change', 'select.frm_chosen', function(){
            forms.validateField($(this), 'select_chosen');
        });
        
        // textarea
        $(document).on('blur', '.frm_textarea', function(){
            forms.validateField($(this), 'textarea');
        });
        		
    },
    
    validateOnChange: function () {
        $(document).on('change', '.frm_select', function(){
            forms.validateField($(this), 'select');
        });
    },
    validateOnEnter: function (){
//        $(window).keydown(function(event){
//            if(event.target.tagName != 'textarea') {
//                if(event.keyCode == 13) {
//                    event.preventDefault();
//                    return false;
//                }
//            }
//        });
//        $(document).on('keypress', 'form', function(event){
//            var code = event.keyCode ? event.keyCode : event.which;
//            if (code == 13) { // on pressing carriage return
//                event.preventDefault();
//            }
//        });
    },
    validateOnSubmit: function (){
		
        $(document).on('click', '.frm_submit', function(){
                        
            var field_status = [];
            var valid = true;

            // check the number of parents to traverse to get the form element
            var numberParents = parseInt($(this).attr('data-form'));
            if (isNaN(numberParents)) {
                var form = $(this).parent().parent('.frm_frm');
            } else {
                var parent = "";
                for (var i = 1; i <= numberParents; i++) {
                    parent = parent + ".parent()"; 
                }
                var form = eval("$(this)" + parent);
            }
            
            var formName = form.attr('name');
            var formType =  "";
            if (form.hasClass('frm_ajax')) {
                formType = "ajax";
            } else {
                formType = "submit";
            }
			
            // check if the form has some processing before submit
            // the before submit function must return true or false
            if ($(this).hasClass('frm_before_submit')) {
                var before_submit_status = window[formName + '_before_submit']($(this));
                field_status.push(before_submit_status);
            }

            // loop through each element of the forms
            // textbox
            form.find('input.frm_text').each(function() {
                field_status.push(forms.validateField($(this), 'text'));
            });

            // textarea
            form.find('.frm_textarea').each(function() {
                field_status.push(forms.validateField($(this), 'textarea'));
            });
			
            // password 
            form.find('input.frm_pass').each(function() {
                field_status.push(forms.validateField($(this), 'pass'));
            });
            
            // select 
            form.find('select.frm_select').each(function() {
                field_status.push(forms.validateField($(this), 'select'));
            });
            
            // chosen select
            form.find('select.frm_chosen').each(function() {
                field_status.push(forms.validateField($(this), 'select_chosen'));
            });
            
            // multiselect 
            // using mutiselect jquery plugin and a helper see view ips/ip_vrf_ratacher_liaisons.ctp for more details
            form.find('input.frm_select_multi_helper').each(function(){
                field_status.push(forms.validateField($(this), 'multiselect'));
            });
            
            // checking if there is a false value
            // atleast 1 false value must return false to prevent form subsmission
            if (field_status.length > 0) {
                $.each(field_status, function(index, value){
                    if (value === false) {
                        valid = false;
                    }
                });
            }
            
            // display error notification/or anything for blank fields
            if (!valid) {
                // code here
                if ($(this).hasClass('frm_notif')) {
                    if (typeof Notif.Show === 'function') {
                        var msg = "Merci de remplir tous les champs obligatoires et respecter le type de données";
                        Notif.Show(msg, 'error', true, 3000);
                    }
                }
                return false;
            }

            // forms submissions if valid = true
            if (valid) {
                if (formType == "submit") {
                    form.submit();
                } else if (formType == "ajax") {
                    var url = form.attr('data-url');
                    var dataType = form.attr('data-type');
                    
                    ajaxBusy = true;
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: form.serialize(),
                        dataType: dataType,
                        timeout: 0, // unlimited as there may be long requests
                        success: function (result, status) {
                            // call the custom function for the ajax success
                            // each function for the ajax must be on the same page or by another ajax call must be in a common script file
                            // convention used: "form attribute name value" . "_" . ajax_success(result, status) { // code here }												
                            // call the function
                            window[formName + '_ajax_success'](result, status);
                        }
                    });
                }
            } // end valid
				
			
        });

    },
    
}