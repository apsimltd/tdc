var ajaxBusy = true;
var headerOffset = 64;
var pageLength = 25;
$(document).ready(function(){
//     alert($(window).width());
    
    Debug.Close();
    Debug.Open();
    Scroller.Init();
    Menu.Show();
    Menu.OpenSubmenu();
    Tooltip.Init();
    Tooltip.InitClick();
    Table.Init();
    Table.ZeroFilter(); 
    AjaxLoader.Init();
    Modal.Close();
    Notif.Close();
    Notif.Cookie();
    Dialog.Confirm();
    Dialog.Close();
    Quickup.Init();

    // initializing forms styling
    forms.radioCheckbox();
    forms.selectChosen();
    forms.selectOptionMulti();
    forms.validateOnBlur();
    forms.validateOnChange();
    forms.validateOnSubmit();
    forms.validateOnEnter();
    DatePicker.Init();
    DatePicker.DateOfBirth();
    DatePicker.DatePick();
    DatePicker.FR();
        
    // Search
    Search.ShowParam();
    Search.Execute();
    Search.OnEnter();
        
    /// Alert.RDVP(); // commented as it is not being used correctly
    Hide.Me(); 
    
    PhoneFR.Init();
    PhoneFR.Display();
    
    LABf3.RadioSquare();
    
}); // end document ready

var LABf3 = {
    RadioSquare: function (){
        $(document).on('click', '.labf3-radio-square', function(){
            
            if ($(this).find('span.ticks').hasClass('active')) {
                $(this).find('span.ticks').removeClass('active');
                $(this).parent('.labf3-radio-square-wrapper').find('input.frm_labf3_radio_square').val('');
            } else {
                var data = $(this).data('value');
                // assign the new value to the hidden form element
                $(this).parent('.labf3-radio-square-wrapper').find('input.frm_labf3_radio_square').val(data);
                // assign css class for styling
                $(this).parent('.labf3-radio-square-wrapper').find('span.ticks').removeClass('active');
                $(this).find('span.ticks').addClass('active');
            }   
        });
    }
}

var PhoneFR = {
    Init: function (){
        $(document).on('keyup', '.tel_fr', function(){
            var tel = $(this).val().split(" ").join("");
            if (tel.length > 0) {
                var new_tel = tel.replace(/(\d{2})/g, '$1 ').replace(/(^\s+|\s+$)/,'');
                $(this).val(new_tel);
            }
        });
    },
    Display: function () {
        $('.tel_fr').each(function(){
            var tel = $(this).val().split(" ").join("");
            var new_tel = tel.replace(/(\d{2})/g, '$1 ').replace(/(^\s+|\s+$)/,'');
            $(this).val(new_tel);
        });
    },
}

var Hide = {
    Me: function (){
        $(document).on('click', '.hidemeafterclick', function(){
            $(this).hide();
        });
    },
}

var Alert = {
    RDVP: function (){
        $.notify.defaults({autoHide: false, position: 'bottom right'});    
        setInterval(function(){

            ajaxBusy = false;
            $.ajax({
                url: BASE_URL + '/cron/events-alert',
                type: 'POST',
                dataType : 'json',
                cache: false,
                success: function (result) {
                    if (result.status === "OK") {
                        $.notify('<i class="fa fa-exclamation-triangle"></i>' + result.msg, 'info');
                    }
                }
            });

        }, 60000); // in milliseconds (ms) => 1000 ms = 1 second => execute every 1 minute
    },
}
var Debug = {
    Close: function (){
        $(document).on('click', '#debug .os-icon-close', function (){
            $('#debug').remove();
        });
    },
    Open: function () {
        $(document).on('click', '#debug .debug-head h1', function (){
            /// $('.debug-body').slideDown({duration:50, easing:"easeInQuart"});
            $('.debug-body').slideToggle({duration:50, easing:"easeInQuart"});
        });
    },
}

var Search = {
    ShowParam: function (){
        $(document).on('focus', '#TopSearchInput', function (){
            $('.SearchSelector').fadeIn(100);
        });
        $(document).on('focusout', '#TopSearchInput', function (){
            $('.SearchSelector').fadeOut(100);
        });
    },
    Execute: function () {        
        $(document).on('click', '.SearchSelector ul li', function(){
            var type = $(this).attr('data-search');
            $('#typeSearch').val(type);
            $('#frm_main_search').submit();
        });
    },
    OnEnter: function() {
        $(document).on('keypress', '#TopSearchInput', function(event){
            var code = event.keyCode ? event.keyCode : event.which;
            if (code == 13) { // on pressing carriage return
                $('#typeSearch').val('all');
                $('#frm_main_search').submit();
            }
        });
    },
}

var DatePicker = {
    Init: function (){
        if ($('.datepicker').length) {
            $('.datepicker').on().datepicker({
                dateFormat: 'dd-mm-yy', 
                regional: 'fr',
                // minDate: 0, 
                showButtonPanel: false,
                onClose: function(date, instance) {
                    forms.validateField(instance.input, 'text');
                }
            });
        }    
    },
    DatePick: function (){
        if ($('.datepick').length) {
            $('.datepick').on().datepicker({
                dateFormat: 'dd-mm-yy', 
                regional: 'fr',
                maxDate: 0, 
                showButtonPanel: false,
                onClose: function(date, instance) {
                    forms.validateField(instance.input, 'text');
                }
            });
        }    
    },
    DateOfBirth: function (){
        if ($('.dateOfBirth').length) {
            $('.dateOfBirth').on().datepicker({
                dateFormat: 'dd-mm-yy', 
                regional: 'fr',
                maxDate: 0,
                // yearRange: '1950:2013',
                yearRange: '1930:+0',
                changeMonth: true,
                changeYear: true,
                showButtonPanel: false,
                onClose: function(date, instance) {
                    forms.validateField(instance.input, 'text');
                }
            });
        }    
    }, 
    FR: function (){
        if ($('.datepicker_yyyy_mm_dd').length) {
            $('.datepicker_yyyy_mm_dd').on().datepicker({
                dateFormat: 'yy-mm-dd', 
                regional: 'fr',
                // minDate: 0, 
                showButtonPanel: false,
                onClose: function(date, instance) {
                    forms.validateField(instance.input, 'text');
                }
            });
        }    
    },
}

var Menu = {
    Show: function (){
        $(document).on('click', '.menu-trigger', function(){
            $('#base').toggleClass('shownav');
            if ($('#tdc_summary').length) {
                $('#tdc_summary').toggleClass('shownav');
            }
            $('#menuWrapper').toggleClass('shownav');
        });
        $(document).on('mouseleave', '#menuWrapper', function(){
            $('#base').toggleClass('shownav');
            if ($('#tdc_summary').length) {
                $('#tdc_summary').toggleClass('shownav');
            }
            $('#menuWrapper').toggleClass('shownav');
        });
    },
    OpenSubmenu: function () {
        $(document).on('click', 'ul.main-menu a.main', function() {
            
            if ($(this).hasClass('active')) {
                
                $(this).next('ul').find('li.expanded a:first').removeClass('active');
                $(this).next('ul').find('li.expanded ul').slideUp('fast');
                $(this).next('ul').slideUp('fast');
                $(this).removeClass('active');
                
            } else {
                
                $('ul.main-menu li.level_1').each(function() {
                    if ($(this).find('a.main').hasClass('active')) {
                        $(this).find('li.expanded a:first').removeClass('active');
                        $(this).find('li.expanded ul').slideUp('fast');
                        $(this).find('ul').slideUp('fast');
                        $(this).find('a.main').removeClass('active');
                    }
                });
                
                $(this).next('ul').slideDown('fast');
                $(this).addClass('active');
                
            }
                        
        });      
    },
}

var Table = {
    Init: function (){
        if ($('.datable').length) {
            var table = $('table.datable').DataTable();
            table.destroy();
            table = $('table.datable').DataTable({
                pageLength : pageLength,
                language: Table.LangFr(),
                order: [], // to disable sorting on the first column when loading https://datatables.net/reference/option/order
                fixedHeader: Table.FixedHeader(),
                responsive: true,
//                columnDefs: [{ type: 'alt-string', targets: 0 }], // need sorting plugins for date
            });
        }
    },
    ZeroFilter: function (){
        if ($('.datableZeroFilter').length) {
            var table = $('.datableZeroFilter').DataTable({
                paging:   false,
                ordering: false,
                info:     false,
                searching: false,
                language: Table.LangFr(),
                order: [], // to disable sorting on the first column when loading https://datatables.net/reference/option/order
                fixedHeader: Table.FixedHeader(),
//                columnDefs: [{ type: 'alt-string', targets: 0 }], // need sorting plugins for date
            });
            table.destroy();
            table = $('.datableZeroFilter').DataTable({
                paging:   false,
                ordering: false,
                info:     false,
                searching: false,
                language: Table.LangFr(),
                order: [], // to disable sorting on the first column when loading https://datatables.net/reference/option/order
                fixedHeader: Table.FixedHeader(),
//                columnDefs: [{ type: 'alt-string', targets: 0 }], // need sorting plugins for date
            });
        }
    },
    FixedHeader: function (){
        var header = {
            header: true,
            headerOffset: headerOffset,
            footer: false,
            footerOffset: 0,
        };
        return header;
    },
    LangFr: function (){
        var language = { 
            processing: "Traitement en cours...",        
            search:        "Rechercher&nbsp;:",
            searchPlaceholder: "Rechercher",
            lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",        
            info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",        
            infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",        
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",        
            infoPostFix:    "",        
            loadingRecords: "Chargement en cours...",        
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",        
            emptyTable:     "Aucune donn&eacute;e disponible dans le tableau",        
            paginate:{            
                first:      "Premier",            
                previous:   "Pr&eacute;c&eacute;dent",            
                next:       "Suivant",            
                last:       "Dernier"        
            },        
            aria:{      
                sortAscending:  ": activer pour trier la colonne par ordre croissant",            
                sortDescending: ": activer pour trier la colonne par ordre d&eacute;croissant" 
            }
        }; // end language settings
        return language;
    }
}

var Quickup = {
    Init: function (){
        /**
         * @src http://www.jqueryscript.net/animation/Back-To-Top-Link-jQuery-quick-up.html
         */
        $.quickup({
            quId: 'quickup',
            quScrollElement: undefined,
            quScrollText: 'Scroll Up',
            quScrollLength: 100,
            quScrollSpeed: 250,
            quRightAlign: '10px',
            quBottomAlign: '10px',
            quDispAnimationSpeed: 250,
            quScrollTopTo: 0,
        });
    }
}

var Loader = {
    Show: function (){
        var windowHeight = $(window).height();
        var windowWidth = $(window).width();
        var LoaderHtml = '<div class="labf3-shadowbox"><div class="labf3-shadowbox-overlay"></div><div class="labf3-shadowbox-container"></div></div>';
        $('.labf3-shadowbox').remove();
        $('body').append(LoaderHtml);
        $('.labf3-shadowbox-container').css({'width':windowWidth,'height':windowHeight});
        $('body').addClass('busy');
        $('.labf3-shadowbox').fadeIn(100);
    },
    Hide: function (){
        $('.labf3-shadowbox').fadeOut(100).remove();
        $('body').removeClass('busy');
    },
}

var Dialog = {
    Show: function (title, message, url){ // url, callback, param
        
        if ($('.dialog').length) {
            $('.dialog').remove();
        }
        
        var dialog_html = '<div class="dialog"><div class="dialog-main"><div class="dialog-content"><div class="dialog-header clearfix"><button type="button" class="close tooltips" title="Fermer"><span aria-hidden="true">&times;</span></button><h4 class="dialog-title">'+title+'</h4></div><div class="dialog-body clearfix">'+message+'</div><div class="dialog-footer clearfix"><button type="button" class="btn btn-info btn-s pull-right close">Non</button><button type="button" class="btn btn-warning btn-s pull-right mr5 dialog-confirm" data-url="'+url+'">Oui</button></div></div></div></div>';
        
        $('body').append(dialog_html);
        
        // $('.dialog').slideDown(500, "easeInQuart");
        $('.dialog').fadeIn(100);
        
    },
    Confirm: function (){
        $(document).on('click', '.dialog-confirm', function(){
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                dataType : 'json',
                cache: false,
                success: function (result, status) {   
                    $('.dialog').fadeOut(100);
                    
                    if (result.callback != undefined && result.param == undefined) {
                        Notif.Show(result.msg, result.type, true, 5000, result.callback);
                    } else if (result.callback != undefined && result.param != undefined) {
                        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
                    } else {
                        Notif.Show(result.msg, result.type, true, 5000);
                    } 
                }
            });
        });
    },
    Close: function (){
        $(document).on('click', '.dialog-header button.close, .dialog-footer .close', function(){
            // $('.dialog').slideUp(500, "easeOutQuart");
            $('.dialog').fadeOut(100);
        });
    },
}

var Modal = {
    Show: function (url, callback, param, icon, size, id){
        
        $.ajax({
            url: url,
            dataType : 'html',
            cache: false,
            success: function (result) {
                
                var icon_html = "";
                if (icon != undefined) {
                    var icon_html = icon; 
                }
                
                var size_class = 'modal-80';
                if (size != undefined) {
                    size_class = 'modal-'+size;
                }
                
                var unique_id = '';
                if (id != undefined) {
                    var unique_id = 'id="'+id+'"';
                }
                
                var html_header = '<div '+unique_id+' class="modal-v2" style="display:block;"><div class="onboarding-modal"><div class="modal-dialog '+size_class+' animUp"><div class="modal-content text-center"><button class="close tooltips" type="button" title="Fermer"><span class="os-icon os-icon-close"></span></button><div class="onboarding-slide"><div class="onboarding-side-by-side"><div class="onboarding-media">'+icon_html+'</div><div class="onboarding-content with-gradient">';
                                
                var html_body = result;
                
                var html_footer = '</div></div></div></div></div></div></div>';
                
                var HTML = html_header + html_body + html_footer;
                
                if (!$('body').hasClass('modal-open')) {
                    $('body').addClass('modal-open');
                }
                
                $('body').append(HTML);
                
                /// $('.modal').slideDown(500, "easeInQuart");
                $('.modal-v2').fadeIn(100);
                
                // call the callback function with param
                if (callback != undefined && param == undefined) {
                    window[callback]();
                } else if (callback != undefined && param != undefined) {
                    window[result.callback](result.param);
                }
                
            }
        });
    },
    Close: function (){
        $(document).on('click', '.modal-v2 button.close', function(){
            /// $('.modal').slideUp(500, "easeOutQuart");
            /// $('.modal-dialog').removeClass('animUp').addClass('slideDown');
            var parent = $(this).parent().parent().parent().parent('.modal-v2');
            // parent.addClass('nobg').slideUp(500, "easeOutQuart").remove();
            parent.slideUp(500, "easeOutQuart").remove();
            
            // remove if being used in another app
            if ($('.ui-timepicker-container').length){
                $('.ui-timepicker-container').fadeOut(10);
            }
            
            if ($('.modal-v2').length) {
                // do nothing
            } else {
                $('body').removeClass('modal-open');
            }           
        });
    },
}

var AjaxLoader = {
    Init: function(){
        $.ajaxSetup({cache: false}); // disable ajax caching
        $(document).ajaxStart(function(){
            if (ajaxBusy) {
                Loader.Show();
            }
        }).ajaxStop(function(){
            Loader.Hide();
        }).ajaxSuccess(function(){
            Tooltip.Init();
            Tooltip.Hide();
            Tooltip.InitClick();
            Scroller.Init();
            Dialog.Close();
            Modal.Close();
//            Table.Init();
//            Table.ZeroFilter();
            forms.radioCheckbox();
            forms.selectChosen();
            forms.selectOptionMulti();
            DatePicker.Init();
            DatePicker.FR();
        });
    },
}

var Tooltip = {
    Init: function () {
        //http://iamceege.github.io/tooltipster/
        if ($('.tooltips').length) {
            $('.tooltips').tooltipster({ // need to add s in tooltip because of bootstrap default .tooltip class in case using bootstrap
                animation: 'swing', // grow
                contentAsHTML: true,
                maxWidth: 300,
                multiple: true,
            });
        }
    },
    InitClick: function () {
        //http://iamceege.github.io/tooltipster/
        if ($('.tooltips-click').length) {
            $('.tooltips-click').tooltipster({ // need to add s in tooltip because of bootstrap default .tooltip class in case using bootstrap
                animation: 'swing', // grow
                contentAsHTML: true,
                maxWidth: 300,
                multiple: true,
                trigger: 'click',
            });
        }
    },
    Hide: function () {
        if ($('.tooltipster-swing-show').length) {
            $('.tooltipster-swing-show').css('opacity', 0);
        }
    },
}

var Notif = { 
    Close : function () {
        $(document).on('click', '.message_close', function(){
            $('.notification').slideUp({duration:50, easing:"easeOutQuart"});
        });
    },
    Hide : function () {
        $('.notification').slideUp({duration:50, easing:"easeOutQuart"});
    },
    Cookie: function () {
        var cookies_data = Cookies.get();
        if (cookies_data.status !== undefined) {
            // var msg = (cookies_data.msg).replace(new RegExp('+', 'g'), ' ');
            var msg = (cookies_data.msg).split("+").join(' ');
            var type = cookies_data.type;
            if (cookies_data.callback === undefined || cookies_data.param === undefined) {
                Notif.Show(msg, type, true, 6000);
                Cookies.remove('status');
                Cookies.remove('msg');
                Cookies.remove('type');
            } else if (cookies_data.callback != undefined && cookies_data.param === undefined) {
                var callback = cookies_data.callback;
                Notif.Show(msg, type, true, 6000, callback);
                Cookies.remove('status');
                Cookies.remove('msg');
                Cookies.remove('type');
                Cookies.remove('callback');
            } else if (cookies_data.param !== undefined && cookies_data.param !== undefined) {
                var callback = cookies_data.callback;
                var param = cookies_data.param;
                Notif.Show(msg, type, true, 6000, callback, param);
                Cookies.remove('status');
                Cookies.remove('msg');
                Cookies.remove('type');
                Cookies.remove('callback');
                Cookies.remove('param');
            }
        }
    },
    Show : function (message, type, remove, timeout, callback, param) { // type :: error, success, warning | remove :: true or false | timeout in milliseconds

        if ($('.notification').length) {
            $('.notification').remove();
        }

        var notificationWrapper = '<div class="notification animUp"><div class="message"></div></div>';
        $('body').append(notificationWrapper);
		
        var icon = '';
        if (type == 'error') {
            icon = '<span class="glyphicon glyphicon-remove"></span>';
        } else if (type == 'success') {
            icon = '<span class="glyphicon glyphicon-ok"></span>';
        } else if (type == 'warning') {
            icon = '<i class="fa fa-exclamation-triangle"></i>';
        } else if (type == 'info') {
            icon = '<span class="glyphicon glyphicon-info-sign"></span>';
        }
	
        // we do not put the close icon if it will be closed automatically
        var close_icon = '<span class="glyphicon glyphicon-remove message_close tooltips" title="Fermer"></span>';
        if (remove === true) {
            close_icon = '';
        }
        
        $('.notification').find('.message').html(icon + message + close_icon).addClass(type);
        
        // we keep 40% of the width for the notification so as the lightview/Modal also can be close with ease
        // if mobile version we keep 100%
        var ratioWidth = 40;
        if ($.browser.mobile) {
            var ratioWidth = 100;
        }
        
        var windowWidth = $(window).width();
        var notificationWidth = (windowWidth/100)*ratioWidth;
        var marginLeft = notificationWidth/2;
        $('.notification').css({'width':notificationWidth+'px', 'margin-left':'-'+marginLeft+'px'});
        
        if ($.browser.mobile) {
            $('.notification').css({'top':'40px'});
        }

        // automatic fadeOut or by close up
        if (remove === undefined || remove === false) {
            $('.notification').slideDown({duration:50, easing:"easeInQuart"});
        } else if (remove === true && timeout === undefined) {
            $('.notification').slideDown({duration:50, easing:"easeInQuart"}).delay(5000).slideUp({duration:50, easing:"easeOutQuart"});
        } else if (remove === true && timeout) {
            $('.notification').slideDown({duration:50, easing:"easeInQuart"}).delay(timeout).slideUp({duration:50, easing:"easeOutQuart"});
        }
        
        Tooltip.Init();
        Tooltip.Hide();
        
        if (callback != undefined) {
            // call the callback function
            if (param != undefined) {
                window[callback](param);
            } else {
                window[callback]();
            }
        }

    },
}

var Scroller = {
    Init: function (){
        if ($('.nano').length) {
            $(".nano").nanoScroller({
                preventPageScrolling: true,
                alwaysVisible: true,
            });
        }
    },
}

/**
 * @TODO pass parameter param with key:value in the url of reload
 * @returns {undefined}
 */
function reloadpage() {
    location.reload();
}

function logout() {
    window.location = 'logout';
}

function reloadpagenoparam() {
    
    setInterval(function(){
        window.location = window.location.href.split("?")[0];
    }, 3000); // in milliseconds (ms) => 1000 ms = 1 second => execute every 1 minute
}

function reloadpageparam(param) {
    var url = window.location.href.split("?")[0];
    var querystring = '?' + param.querystring + '=' + param.value;
    var redirect = url + querystring;
    setInterval(function(){
        window.location = redirect;
    }, 3000);
}

function gotopage(param) {
    setInterval(function(){
        window.location = BASE_URL + '/' + param.page + '?' + param.querystring + '=' + param.value;
    }, 3000); // in milliseconds (ms) => 1000 ms = 1 second => execute every 1 minute
}

function gotologin() {
    location.href = BASE_URL + "/connexion";
}

function goto_tdc(param) {
    window.location = BASE_URL + '/tdc?id=' + param;
}

function goto_tdc_client(param) {
    window.location = BASE_URL + '/tdc-client?id=' + param;
}

function reloadpagescrollto(param) {
    window.location = BASE_URL + '/tdc?id=' + param.mission_id + '&tdc_id='+param.tdc_id;
}