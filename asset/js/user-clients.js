$(document).ready(function(){
   
    // profile
    Profil.Myself();
    Profil.ChangePass();
    
    
});

var Profil = {
    Myself: function (){
        $(document).on('click', 'li.username.my-profile', function(){
            var user_id = $(this).data('user_id');
            Modal.Show(AJAX_UI + '/mon-profile?user_id='+user_id, null, null, '<i class="os-icon os-icon-user-male-circle"></i>', 50);
        });
    },
    ChangePass: function () {
        $(document).on('focusout', '#frm_mon_profile #password', function(){
            
            if ($(this).val() != "") {
                
                // check the password if good
                var url = AJAX_HANDLER + '/check-pass';
                $.ajax({
                    url: url,
                    dataType : 'json',
                    type: 'POST',
                    cache: false,
                    data: {pass:$(this).val()},
                    success: function (result, status) {                
                        if (status === "success" && result.status === "OK") {
                            $('#frm_mon_profile #password_tracker').removeClass('must').removeClass('error').addClass('ok').val('');
                            $('#frm_mon_profile #password').removeClass('error').removeClass('ok').addClass('ok');
                            $('#frm_mon_profile #new_password').removeClass('must').addClass('must');
                            $('#frm_mon_profile .message').fadeOut(100);
                        } else if (status === "success" && result.status === "NOK") {
                            Notif.Show(result.msg, result.type, true, 5000);
                            $('#frm_mon_profile #password_tracker').removeClass('must').removeClass('error').addClass('must').addClass('error').val('');
                            $('#frm_mon_profile #password').removeClass('error').removeClass('ok').addClass('error');
                            $('#frm_mon_profile #new_password').removeClass('must').removeClass('error').removeClass('ok').addClass('must');
                            $('#frm_mon_profile .message').fadeIn(100);
                        }
                    }
                });   
                
            } else {
                
                $('#frm_mon_profile #password_tracker').removeClass('must').removeClass('error').addClass('ok').val('');
                $('#frm_mon_profile #password').removeClass('error').removeClass('ok');
                $('#frm_mon_profile #new_password').removeClass('must').removeClass('error').removeClass('ok');
                
            }
 
        });
        
    },
}

function frm_mon_profile_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_mon_profile_before_submit (formObj) {
    var pass = $('#frm_mon_profile #password').val();
    var new_pass = $('#frm_mon_profile #new_password').val();
    
    if (new_pass != "" && pass == "") {
        $('#frm_mon_profile #password').addClass('must').addClass('error').val('');
        $('#frm_mon_profile #new_password').val('').removeClass('ok');
        return false;
    }
    
    if ($('#frm_mon_profile #password_tracker').hasClass('must') && $('#frm_mon_profile #password_tracker').val() == "") {
        $('#frm_mon_profile .message').fadeIn(100);
        return false;
    } else {
        $('#frm_mon_profile .message').fadeOut(100);
        return true;
    }
    
    return true;
}