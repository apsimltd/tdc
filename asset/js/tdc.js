// headerOffset = 272;

headerOffset = headerOffset + 40;

pageLength = 2000;
// pageLength = 2;

// chatbox => used to replace all popups on tdc
var ChatBox = {
    PARAMS: {
        title: "",
        name: "",
        content: "",
    },
    CLOSE: function (){
        $(document).on('click', '.chat_box .close', function(){
            $(this).parent('.chat_header').parent('.chat_box').slideDown({duration:500, easing:"easeInQuart"});
            $('.chat_box').remove();
        });
    },
    REMOVE: function () {
        $('.chat_box').slideDown({duration:500, easing:"easeOutQuart"});
        $('.chat_box').remove();
    },
    SHOW: function (){
        
        var HTML = '<div class="chat_box"><div class="chat_header clearfix"><span class="name">'+ChatBox.PARAMS.name+'</span><span class="chat_title">'+ChatBox.PARAMS.title+'</span><button class="close tooltips" title="Fermer" type="button">×</button></div><div class="chat_messages_history clearfix"><div class="nano"><div class="nano-content">'+ChatBox.PARAMS.content+'</div></div></div></div>';
        
        if ($('.chat_box').length) {
            $('.chat_box').remove();
        }
        
        $('body').append(HTML);
        
        // position it in the middle as sometimes some actions cannot be done on the tdc as it hides them
        var windowWidth = $(window).width();
        var chat_box_position = parseInt(((windowWidth - 600) / 2));
        $('.chat_box').css('left', chat_box_position+'px');
        
        $('.chat_box').slideDown({duration:500, easing:"easeInQuart"});
        
    }
}

$(document).ready(function(){
    
    ChatBox.CLOSE();
    
    // sticky legende
    StickyLegende.Init();
    
    // https://www.datatables.net/examples/advanced_init/dt_events.html
    // call a function on clicking next page // not working must find a way
    // $('table.datable').on('page.dt', function() {Suivi.Hover();}).DataTable();
        
    SummaryUI.Open();
    SummaryUI.Close();
    
    Upload.Init();
    Upload.Validate();
    
    // start of tdc map
    $(document).on('click', '.tdc_map', function(){
        var mission_id = $(this).data('mission_id');
        Modal.Show(AJAX_UI + '/tdc-map?mission_id='+mission_id);
    });
    
    $(document).on('click', '.tdc-map-pdf', function (){
        var mission_id = $(this).data('mission_id');
        var url = AJAX_HANDLER+'/generate-pdf-map?mission_id='+mission_id;
        ajaxBusy = true;
        $.ajax({
            url: url,
            dataType : 'json',
            cache: false,
            success: function (result, status) {
                if (status === "success" && result.status === "OK") {
                    location.href = BASE_URL + '/download-pdf?file='+result.file;
                }
            }
        });
    });
    
    // end of tdc map
    
    // delete document
    $(document).on('click', '.delete_document', function (){
        var id = $(this).attr('data-document_id');
        var mission_id = $(this).attr('data-mission_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer document', AJAX_HANDLER + '/delete-mission-file?id='+id+'&mission_id='+mission_id+'&src=tdc');
    });
    
    // delete candidate from tdc
    $(document).on('click', '.tdc_delete_candidate', function(){
        var mission_id = $(this).attr('data-mission_id');
        var candidat_id = $(this).attr('data-candidat_id');
        var tdc_id = $(this).data('tdc_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer candidat', AJAX_HANDLER + '/delete-candidat-tdc?mission_id='+mission_id+'&candidat_id='+candidat_id+'&tdc_id='+tdc_id);
    });
    
    // colors in tdc view
    $(document).on('click', '.brush.candidat.action', function(){
        var tdc_id = $(this).attr('data-tdc_id');
        Modal.Show(AJAX_UI + '/tdc-colors?tdc_id='+tdc_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 40);
    });
    
    // legende colors in tdc
    $(document).on('click', '.brush.legend.candidat-color', function(){
        var mission_id = $(this).attr('data-mission_id');
        Modal.Show(AJAX_UI + '/tdc-colors-legend?mission_id='+mission_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 40);
    });
    
    // save colors tdc
    $(document).on('click', '.colorSelector.triggerColorCandidat', function(){
        var className = $(this).attr('data-class');
        var tdc_id = $(this).attr('data-tdc_id');
        var color_id = $(this).attr('data-color_id');
        var color = $(this).attr('data-color');
        // save the colors in tdc with ajax and on success update the interface colors
        var url = AJAX_HANDLER + '/tdc-colors';
        $.ajax({
            url: url,
            dataType : 'html',
            type: 'POST',
            cache: false,
            data:{field:className,tdc_id:tdc_id, color_id:color_id},
            success: function (result, status) {                
                Notif.Show('Couleurs mises à jour avec succès.', 'success', true, 3000);
                $('.colorSelector.'+className).css('background-color', color);
                if (className == 'color_societe_bg') {
                    $('tr.tdc_id_'+tdc_id).find('td.'+className).css('background-color', color);
                } else if (className == 'color_candidat_bg') {
                    $('tr.tdc_id_'+tdc_id).find('td.'+className).css('background-color', color);
                } else if (className == 'color_societe_label'){
                    $('tr.tdc_id_'+tdc_id).find('td.color_societe_bg span.color_societe_label').css('color', color);
                } else if (className == 'color_candidat_label'){
                    $('tr.tdc_id_'+tdc_id).find('td.color_candidat_bg span.color_candidat_label').css('color', color);
                }
            }
        });
    });
    
    // remove the colors tdc 
    $(document).on('click', '.remove-colors.candidat', function(){
        var tdc_id = $(this).attr('data-tdc_id');
        var object = $(this).attr('data-object');
        var url = AJAX_HANDLER + '/tdc-colors-remove';
        $.ajax({
            url: url,
            dataType : 'html',
            type: 'POST',
            cache: false,
            data:{tdc_id:tdc_id, object:object},
            success: function (result, status) {
                
                Notif.Show('Couleurs mises à jour avec succès.', 'success', true, 3000);
                
                if (object == 'societe') {
                    $('table.societe .color_societe_label').css('background-color', '#fff');
                    $('table.societe .color_societe_bg').css('background-color', '#fff');
                    $('tr.tdc_id_'+tdc_id).find('td.color_societe_bg').css('background-color', 'transparent');
                    $('tr.tdc_id_'+tdc_id).find('td.color_societe_bg span.color_societe_label').css('color', '#313534');
                } else if (object == 'candidat') {
                    $('table.candidat .color_candidat_label').css('background-color', '#fff');
                    $('table.candidat .color_candidat_bg').css('background-color', '#fff');
                    $('tr.tdc_id_'+tdc_id).find('td.color_candidat_bg').css('background-color', 'transparent');
                    $('tr.tdc_id_'+tdc_id).find('td.color_candidat_bg span.color_candidat_label').css('color', '#313534');
                } 
                
            }
        });
    });
    
    // colors in tdc entreprise view
    $(document).on('click', '.brush.entreprise.action', function(){
        var tdc_id = $(this).attr('data-tdc_id');
        Modal.Show(AJAX_UI + '/tdc-colors-entreprise?tdc_id='+tdc_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 40);
    });
    
    // legende colors in tdc entreprise
    $(document).on('click', '.brush.legend.entreprise-color', function(){
        var mission_id = $(this).attr('data-mission_id');
        Modal.Show(AJAX_UI + '/tdc-colors-legend-entreprise?mission_id='+mission_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 40);
    });
    
    // save colors tdc entreprise
    $(document).on('click', '.colorSelector.triggerColorEntreprise', function(){
        var className = $(this).attr('data-class');
        var tdc_id = $(this).attr('data-tdc_id');
        var color_id = $(this).attr('data-color_id');
        var color = $(this).attr('data-color');
        // save the colors in tdc with ajax and on success update the interface colors
        var url = AJAX_HANDLER + '/tdc-colors-entreprise';
        $.ajax({
            url: url,
            dataType : 'html',
            type: 'POST',
            cache: false,
            data:{field:className,tdc_id:tdc_id, color_id:color_id},
            success: function (result, status) {                
                Notif.Show('Couleurs mises à jour avec succès.', 'success', true, 3000);
                $('.colorSelector.'+className).css('background-color', color);
                if (className == 'color_societe_bg') {
                    $('tr.tdc_id_'+tdc_id).find('td.'+className).css('background-color', color);
                } else if (className == 'color_societe_label'){
                    $('tr.tdc_id_'+tdc_id).find('td.color_societe_bg span.color_societe_label').css('color', color);
                }
            }
        });
    });
    
    // remove the colors tdc entreprise
    $(document).on('click', '.remove-colors.entreprise', function(){
        var tdc_id = $(this).attr('data-tdc_id');
        var object = $(this).attr('data-object');
        var url = AJAX_HANDLER + '/tdc-colors-remove-entreprise';
        $.ajax({
            url: url,
            dataType : 'html',
            type: 'POST',
            cache: false,
            data:{tdc_id:tdc_id, object:object},
            success: function (result, status) {
                
                Notif.Show('Couleurs mises à jour avec succès.', 'success', true, 3000);
                
                if (object == 'societe') {
                    $('table.societe .color_societe_label').css('background-color', '#fff');
                    $('table.societe .color_societe_bg').css('background-color', '#fff');
                    $('tr.tdc_id_'+tdc_id).find('td.color_societe_bg').css('background-color', 'transparent');
                    $('tr.tdc_id_'+tdc_id).find('td.color_societe_bg span.color_societe_label').css('color', '#313534');
                } 
                
            }
        });
    });
    
    // criteres specs
    $(document).on('click', 'td.tdc_criteres_spec', function(){
        var candidat_id = $(this).attr('data-candidat_id');
        var mission_id = $(this).attr('data-mission_id');
        var tdc_id = $(this).attr('data-tdc_id');
        Modal.Show(AJAX_UI + '/tdc-criteres-specs?tdc_id='+tdc_id+'&mission_id='+mission_id+'&candidat_id='+candidat_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 50);
    });
    
    // tdc_out_category
    $(document).on('click', 'td.tdc_out_category', function(){
        var candidat_id = $(this).attr('data-candidat_id');
        var mission_id = $(this).attr('data-mission_id');
        var tdc_id = $(this).attr('data-tdc_id');
        Modal.Show(AJAX_UI + '/tdc-out-cateories?tdc_id='+tdc_id+'&mission_id='+mission_id+'&candidat_id='+candidat_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 50);
    });
    
    //tdc_ccc
    $(document).on('click', '.tdc_ccc', function(){
        var tdc_id = $(this).parent('td').parent('tr').attr('data-tdc_id');
        var mission_id = $(this).attr('data-mission_id');
        var candidat_id = $(this).data('candidat_id');
        var CCC = $(this);
        var op = 'select';
        if (CCC.hasClass('active')) {
            op = 'deselect';
        } else {
            op = 'select';
        }
        
        var url = AJAX_HANDLER+'/tdc-ccc?tdc_id='+tdc_id+'&op='+op;
    
        $.ajax({
            url: url,
            dataType : 'json',
            type: 'POST',
            data:{mission_id:mission_id, candidat_id:candidat_id},
            cache: false,
            success: function (result, status) {
                if (op == 'select') {
                    CCC.addClass('active');
                } else {
                    CCC.removeClass('active');
                }
                if (status === "success" && result.status === "OK") {
                    Notif.Show(result.msg, result.type, true, 5000);
                } else if (status === "success" && result.status === "NOK") {
                    Notif.Show(result.msg, result.type, true, 5000);
                }
                // reload stats tdc
                reloadStatsOrFilter(mission_id);
            }
        });
        
    });
    
    // change_statut_tdc
    $(document).on('click', '.change_statut_tdc', function (){
        var mission_id = $(this).attr('data-mission_id');
        var candidat_id = $(this).attr('data-candidat_id');
        var tdc_id = $(this).attr('data-tdc_id');
        Modal.Show(AJAX_UI + '/tdc-status?tdc_id='+tdc_id+'&mission_id='+mission_id+'&candidat_id='+candidat_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 50);
    });
    
    // tdc_suivi_candidate
    $(document).on('click', '.tdc_suivi_candidate', function(){
        var candidat_id = $(this).attr('data-candidat_id');
        var mission_id = $(this).attr('data-mission_id');
        var tdc_id = $(this).attr('data-tdc_id');
        var prestataire_id = $(this).data('prestataire_id');
        Modal.Show(AJAX_UI + '/tdc-suivi?candidat_id='+candidat_id+'&mission_id='+mission_id+'&tdc_id='+tdc_id+'&prestataire_id='+prestataire_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 50, 'suivis_modal');
    });
    
    // hover on suivi button
    Suivi.Hover();
    
    // tdc_history_candidate
    $(document).on('click', '.tdc_history_candidate', function(){
        var candidat_id = $(this).attr('data-candidat_id');
        var mission_id = $(this).attr('data-mission_id');
        Modal.Show(AJAX_UI + '/tdc-historique?candidat_id='+candidat_id+'&mission_id='+mission_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 50, 'histo_model');
        // used for get back the user to the same row when reloading
        var tdc_id = $(this).data('tdc_id');
        $('.hidden-data span.tdc_id').text(tdc_id);
        $('.hidden-data span.mission_id').text(mission_id);
    });
    
    $(document).on('click', '#histo_model button.close', function(){
        var tdc_id =  $('.hidden-data span.tdc_id').text();
        var mission_id =  $('.hidden-data span.mission_id').text();
        window.location = BASE_URL + '/tdc?id=' + mission_id + '&tdc_id='+tdc_id;
    });
    
    // hover on historique comment
    Histo.Hover();
    
    // delete comment historique
    $(document).on('click', '.delete_comment_histo', function (){
        var historique_id = $(this).attr('data-historique_id');
        var mission_id = $(this).attr('data-mission_id');
        var candidat_id = $(this).attr('data-candidat_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer Historique', AJAX_HANDLER + '/delete-tdc-historique?historique_id='+historique_id+'&mission_id='+mission_id+'&candidat_id='+candidat_id);
    });
    
    // edit comment historique
    $(document).on('click', '.edit_comment_histo', function (){
        var historique_id = $(this).attr('data-historique_id');
        Modal.Show(AJAX_UI + '/edit-tdc-historique?historique_id='+historique_id, null, null, '<span class="glyphicon glyphicon-tasks"></span>', 50, 'historique_edit_comment');
    });
    
    // clear field upload file
    $(document).on('click', '.clear_field_upload', function(){
        
        var numRows = parseInt($('fieldset.uploadMultiple .add_field_wrap_documents .add_field_row:last').index()) + 1;
        
        if (numRows >= 2) {
            $(this).parent('.add_field_row').remove();
        } else if (numRows == 1) {
            $(this).parent('.add_field_row').find('input.inputfile').val('');
            $(this).parent('.add_field_row').find('label').html('<span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span>');
        }
        
    });
    
    // filter from start
    // we just need to refresh the page
    $(document).on('click', '.filter_stats_debut', function(){
        var mission_id = $(this).data('mission_id');
        goto_tdc(mission_id);
    });
    
    // PDF TDC
    $(document).on('click', '.tdc_pdf_anonyme', function(){
        var mission_id = $(this).attr('data-mission_id');
        Modal.Show(AJAX_UI + '/candidat-pdf-tdc-anonyme?mission_id='+mission_id, null, null, '<i class="fa fa-file-pdf"></i>', 50, 'modal-tdc-pdf-anonyme');
    });
    
    $(document).on('click', '.pdf-tdc-anonyme', function (){
        var mission_id = $(this).attr('data-mission_id');
        var societe = $(this).attr('data-societe');
        var url = AJAX_HANDLER+'/generate-pdf-tdc-anonyme?id='+mission_id+'&societe='+societe;
        ajaxBusy = true;
        $.ajax({
            url: url,
            dataType : 'json',
            cache: false,
            success: function (result, status) {
                if (status === "success" && result.status === "OK") {
                    $('#modal-tdc-pdf-anonyme button.close').trigger('click');
                    location.href = BASE_URL + '/download-pdf?file='+result.file;
                }
            }
        });
    });
    
    $(document).on('click', '.tdc_pdf_client', function(){
        var mission_id = $(this).attr('data-mission_id');
        Modal.Show(AJAX_UI + '/candidat-pdf-tdc-client?mission_id='+mission_id, null, null, '<i class="fa fa-file-pdf"></i>', 50, 'modal-tdc-pdf-client');
    });
    
    $(document).on('click', '.pdf-tdc-client', function (){
        var mission_id = $(this).attr('data-mission_id');
        var societe = $(this).attr('data-societe');
        // var url = AJAX_HANDLER+'/generate-pdf-tdc-client?id='+mission_id+'&societe='+societe;
        var url = '../../ajaxhandler/generate-pdf-tdc-client.php?id='+mission_id+'&societe='+societe;
        ajaxBusy = true;
        $.ajax({
            url: url,
            dataType : 'json',
            cache: false,
            success: function (result, status) {
                if (status === "success" && result.status === "OK") {
                    $('#modal-tdc-pdf-client button.close').trigger('click');
                    location.href = BASE_URL + '/download-pdf?file='+result.file;
                }
            }
        });
    });
    
    // generate pdf candidat
    $(document).on('click', '.generate_pdf_candidat_tdc', function(){
        var candidat_id = $(this).attr('data-candidat_id');
        var mission_id = $(this).attr('data-mission_id');
        Modal.Show(AJAX_UI + '/candidat-pdf-selection-tdc?candidat_id='+candidat_id+'&mission_id='+mission_id, null, null, '<i class="fa fa-file-pdf"></i>', 50, 'modal-candidate-pdf-tdc');
    });
        
    $(document).on('click', '.pdf-candidat-tdc', function (){
        var file_format = $('#file_format').val();
        var candidat_id = $(this).attr('data-candidat_id');
        var mission_id = $(this).attr('data-mission_id');
        var societe = $(this).attr('data-societe');
        
        if (file_format === "PDF") {
            var url = AJAX_HANDLER+'/generate-pdf-candidat-tdc?id='+candidat_id+'&mission_id='+mission_id+'&societe='+societe;
            $.ajax({
                url: url,
                dataType : 'json',
                cache: false,
                success: function (result, status) {
                    if (status === "success" && result.status === "OK") {
                        $('#modal-candidate-pdf-tdc button.close').trigger('click');
                        location.href = BASE_URL + '/download-pdf?file='+result.file;
                    }
                }
            });
        } else if (file_format === "WORD") {
            var url = AJAX_HANDLER+'/generate-word-candidat-tdc?id='+candidat_id+'&mission_id='+mission_id+'&societe='+societe;
            $.ajax({
                url: url,
                dataType : 'json',
                cache: false,
                success: function (result, status) {
                    if (status === "success" && result.status === "OK") {
                        $('#modal-candidate-pdf-tdc button.close').trigger('click');
                        location.href = BASE_URL + '/download-word?file='+result.file;
                    }
                }
            });
        }
        
    });
    
    // place and shortliste
    // http://icheck.fronteed.com/#callbacks
    $(document).on('ifChecked', '#shortliste', function(event){
        // alert(event.type + ' callback');
        var tdc_id = $('#tdc_id_value').val();
        $('tr.tdc_id_'+tdc_id).find('td:first').css('border-left', '4px solid #7cbd5a');
    });
    $(document).on('ifUnchecked', '#shortliste', function(event){
        // alert(event.type + ' callback');
        var tdc_id = $('#tdc_id_value').val();
        $('tr.tdc_id_'+tdc_id).find('td:first').css('border-left', 'none');
        $('.changeSL').fadeOut(100);
    });
    
    $(document).on('ifChecked', '#place', function(event){
        // alert(event.type + ' callback');
        var tdc_id = $('#tdc_id_value').val();
        $('tr.tdc_id_'+tdc_id).find('td:first').css('border-left', '4px solid #486e34');
    });
    $(document).on('ifUnchecked', '#place', function(event){
        // alert(event.type + ' callback');
        var tdc_id = $('#tdc_id_value').val();
        $('tr.tdc_id_'+tdc_id).find('td:first').css('border-left', 'none');
    });
    
    // change SL 
    $(document).on('click', '.changeSLTrigger', function(){
        var mission_id = $(this).data('mission_id');
        var candidat_id = $(this).data('candidat_id');
        Modal.Show(AJAX_UI + '/tdc-change-sl?mission_id='+mission_id+'&candidat_id='+candidat_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 50, 'change_sl_model');
    });
    
    // tdc_sms
    $(document).on('click', '.tdc_sms', function(){
        var candidat_id = $(this).attr('data-candidat_id');
        var mission_id = $(this).attr('data-mission_id');
        Modal.Show(AJAX_UI + '/tdc-sms?candidat_id='+candidat_id+'&mission_id='+mission_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 50, 'sms_model');
    });
    
    CandidatComment.Hover();
    CandidatComment.Hide();
    
    CandidatCV.Hover();
    CandidatCV.Hide();
    
    // filter by out category
    $(document).on('change', '#filter_out_categories', function(){
        
        $('#tdc_legende_out_categorie_id').val('');
        
        OutcategorieSelected = $(this).val();
                
        if ($('.wrapper-legende').html() == "") {
            OriginalStickyLegendeHtml = $('.tdc-sticky-legende-wrapper .sticky-legende').html();
        } else if ($('.tdc-sticky-legende-wrapper .sticky-legende').html() == "") {
            OriginalStickyLegendeHtml = $('.wrapper-legende').html();
        }
        
    });
    
    // filter by legende colors
    $(document).on('click', '.filter_by_colors_legende .filter_color.candidat', function(){
        
        $('#tdc_legende_color_ids').val('');
        
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            $(this).addClass('selected');
        }
        
        if ($('.wrapper-legende').html() == "") {
            OriginalStickyLegendeHtml = $('.tdc-sticky-legende-wrapper .sticky-legende').html();
        } else if ($('.tdc-sticky-legende-wrapper .sticky-legende').html() == "") {
            OriginalStickyLegendeHtml = $('.wrapper-legende').html();
        }
        
    });
    
    // filter by legende colors entreprise
    $(document).on('click', '.filter_by_colors_legende .filter_color.entreprise', function(){
        
        $('#filter_colors_entreprise_TDC_ids').val('');
        
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            $(this).addClass('selected');
        }
        
        if ($('.wrapper-legende').html() == "") {
            OriginalStickyLegendeHtml = $('.tdc-sticky-legende-wrapper .sticky-legende').html();
        } else if ($('.tdc-sticky-legende-wrapper .sticky-legende').html() == "") {
            OriginalStickyLegendeHtml = $('.wrapper-legende').html();
        }
        
    });
    
    // filter by legende colors submit button
    $(document).on('click', '#filter_color_submit', function(){
       
        var filters = "";
        var filter_ids = "";
        var outCategory_id = "";
        
        var count_num_selected = 0;
        $('.filter_by_colors_legende .filter_color.candidat.selected').each(function(){
            count_num_selected++;
        });
        
        var counter = 1;
        $('.filter_by_colors_legende .filter_color.candidat.selected').each(function(){
            filter_ids += $(this).data('colorstdc_id');
            filters += $(this).data('colorstdc_id');
            
            if (counter != count_num_selected) {
                filter_ids += ";";
                filters += ";";
            }
            
            counter++;
        });
        
        $('#filter_colors_TDC_ids').val(filter_ids);
        
        var filter_entreprise_ids = "";
        
        var count_num_selectedEntreprise = 0;
        $('.filter_by_colors_legende .filter_color.entreprise.selected').each(function(){
            count_num_selectedEntreprise++;
        });
        
        var counterEntreprise = 1;
        $('.filter_by_colors_legende .filter_color.entreprise.selected').each(function(){
            filter_entreprise_ids += $(this).data('colorstdc_id');
            filters += $(this).data('colorstdc_id');
            
            if (counterEntreprise != count_num_selectedEntreprise) {
                filter_entreprise_ids += ";";
                filters += ";";
            }
            
            counterEntreprise++;
        });
        
        if ($('#filter_out_categories').val() !== "") {
            outCategory_id = $('#filter_out_categories').val();
            filters += ";";
        }
        
        $('#filter_colors_entreprise_TDC_ids').val(filter_entreprise_ids);
        
        if (filters == "") {
            Notif.Show("Sélectionnez une légende couleur ou une Out Catégorie", 'error', true, 5000);
        } else {
            $('#tdc_legende_color_ids').val(filter_ids);
            $('#tdc_legende_entreprise_color_ids').val(filter_entreprise_ids); 
            $('#tdc_legende_out_categorie_id').val(outCategory_id);
            $('#start_date').removeClass('must');
            $('#end_date').removeClass('must');
            $('#tdc_stats_type').removeClass('must');
            $('#tdc_stats_filter_submit').trigger('click');
        }
        
    });
    
    // start of adding entreprise in tdc
    
    // change_statut_tdc
    $(document).on('click', '.change_statut_entreprise_tdc', function (){
        var mission_id = $(this).attr('data-mission_id');
        var entreprise_id = $(this).attr('data-entreprise_id');
        var tdc_id = $(this).attr('data-tdc_id');
        Modal.Show(AJAX_UI + '/tdc-status-entreprise?tdc_id='+tdc_id+'&mission_id='+mission_id+'&entreprise_id='+entreprise_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 50);
    });
    
    // delete entreprise from tdc
    $(document).on('click', '.tdc_delete_entreprise', function(){
        var mission_id = $(this).attr('data-mission_id');
        var entreprise_id = $(this).attr('data-entreprise_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer Entreprise', AJAX_HANDLER + '/delete-entreprise-tdc?mission_id='+mission_id+'&entreprise_id='+entreprise_id);
    });
        
    // tdc_history_entreprise
    $(document).on('click', '.tdc_history_entreprise', function(){
        var entreprise_id = $(this).attr('data-entreprise_id');
        var mission_id = $(this).attr('data-mission_id');
        Modal.Show(AJAX_UI + '/tdc-historique-entreprise?entreprise_id='+entreprise_id+'&mission_id='+mission_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 50, 'histo_model');
        // used for get back the user to the same row when reloading
        var tdc_id = $(this).data('tdc_id');
        $('.hidden-data span.tdc_id').text(tdc_id);
        $('.hidden-data span.mission_id').text(mission_id);
    });
    
    HistoEntreprise.Hover();
    
    // edit comment entreprise
    $(document).on('click', '.edit_comment_histo_entreprise', function (){
        var historique_id = $(this).attr('data-historique_id');
        Modal.Show(AJAX_UI + '/edit-tdc-historique-entreprise?historique_id='+historique_id, null, null, '<span class="glyphicon glyphicon-tasks"></span>', 50, 'historique_edit_comment_entreprise');
    });
    
    // delete comment historique
    $(document).on('click', '.delete_comment_histo_entreprise', function (){
        var historique_id = $(this).attr('data-historique_id');
        var mission_id = $(this).attr('data-mission_id');
        var entreprise_id = $(this).attr('data-entreprise_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer Commentaire', AJAX_HANDLER + '/delete-tdc-historique-entreprise?historique_id='+historique_id+'&mission_id='+mission_id+'&entreprise_id='+entreprise_id);
    });
    
    // tdc_suivi_entreprise
    $(document).on('click', '.tdc_suivi_entreprise', function(){
        var entreprise_id = $(this).attr('data-entreprise_id');
        var mission_id = $(this).attr('data-mission_id');
        var tdc_id = $(this).attr('data-tdc_id');
        Modal.Show(AJAX_UI + '/tdc-suivi-entreprise?entreprise_id='+entreprise_id+'&mission_id='+mission_id+'&tdc_id='+tdc_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 50, 'suivis_modal_entreprise');
    });
    
    // add document from tdc entreprise
    $(document).on('click', 'span.add-doc-entreprise-from-tdc', function(){
        var entreprise_id = $(this).data('entreprise_id');
        var tdc_line_id = $(this).data('tdc_line_id');
        var mission_id = $(this).data('mission_id');
        Modal.Show(AJAX_UI + '/tdc-add-entreprise-doc?&entreprise_id='+entreprise_id+'&tdc_line_id='+tdc_line_id+'&mission_id='+mission_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 50, 'add-candidat-from-tdc');
    });
    
    EntrepriseDoc.Hover();
    EntrepriseDoc.Hide();
    
    // generate pdf candidat
    $(document).on('click', '.generate_pdf_entreprise_tdc', function(){
        var entreprise_id = $(this).attr('data-entreprise_id');
        var mission_id = $(this).attr('data-mission_id');
        Modal.Show(AJAX_UI + '/entreprise-pdf-selection-tdc?entreprise_id='+entreprise_id+'&mission_id='+mission_id, null, null, '<i class="fa fa-file-pdf"></i>', 50, 'modal-entreprise-pdf-tdc');
    });
    
    $(document).on('click', '.pdf-entreprise-tdc', function (){
        var file_format = $('#file_format').val();
        var entreprise_id = $(this).attr('data-entreprise_id');
        var mission_id = $(this).attr('data-mission_id');
        var societe = $(this).attr('data-societe');
        
        if (file_format === "PDF") {
            var url = AJAX_HANDLER+'/generate-pdf-entreprise-tdc?id='+entreprise_id+'&mission_id='+mission_id+'&societe='+societe;
            $.ajax({
                url: url,
                dataType : 'json',
                cache: false,
                success: function (result, status) {
                    if (status === "success" && result.status === "OK") {
                        $('#modal-entreprise-pdf-tdc button.close').trigger('click');
                        location.href = BASE_URL + '/download-pdf?file='+result.file;
                    }
                }
            });
        } else if (file_format === "WORD") {
            var url = AJAX_HANDLER+'/generate-word-entreprise-tdc?id='+entreprise_id+'&mission_id='+mission_id+'&societe='+societe;
            $.ajax({
                url: url,
                dataType : 'json',
                cache: false,
                success: function (result, status) {
                    if (status === "success" && result.status === "OK") {
                        $('#modal-entreprise-pdf-tdc button.close').trigger('click');
                        location.href = BASE_URL + '/download-word?file='+result.file;
                    }
                }
            });
        }
        
    });
    
    
    EntrepriseComment.Hover();
    EntrepriseComment.Hide();
    // end of adding entreprise in tdc
    
    // add cv candidat from TDC
    $(document).on('click', 'span.add-cv-from-tdc', function(){
        var candidat_id = $(this).data('candidat_id');
        var tdc_line_id = $(this).data('tdc_line_id');
        var mission_id = $(this).data('mission_id');
        Modal.Show(AJAX_UI + '/tdc-add-candidat-cv?&candidat_id='+candidat_id+'&tdc_line_id='+tdc_line_id+'&mission_id='+mission_id, null, null, '<span class="glyphicon glyphicon-screenshot"></span>', 50, 'add-candidat-from-tdc');
    });
    
    // visibilite candidat on tdc client
    $(document).on('click', '.tdc_visibilite_client', function(){
        var tdc_id = $(this).parent('td').parent('tr').attr('data-tdc_id');
        var mission_id = $(this).data('mission_id');
        var candidat_id = $(this).data('candidat_id');
        var visibilite = $(this);
        var op = 'select';
        if (visibilite.hasClass('active')) {
            op = 'deselect';
        } else {
            op = 'select';
        }
        
        var url = AJAX_HANDLER+'/tdc-visibilite-client?tdc_id='+tdc_id+'&op='+op;
    
        $.ajax({
            url: url,
            dataType : 'json',
            type: 'POST',
            data:{mission_id:mission_id, candidat_id:candidat_id},
            cache: false,
            success: function (result, status) {
                if (op == 'select') {
                    visibilite.addClass('active');
                } else {
                    visibilite.removeClass('active');
                }
                if (status === "success" && result.status === "OK") {
                    Notif.Show(result.msg, result.type, true, 5000);
                } else if (status === "success" && result.status === "NOK") {
                    Notif.Show(result.msg, result.type, true, 5000);
                }
            }
        });
        
    });
    
}); // end document ready function

var StickyLegende = {
    Stick: function(){

        var window_top = $(window).scrollTop();
        
        if (window_top > 160) {
            $('.wrapper-legende').html("");
            $('.tdc-sticky-legende-wrapper .sticky-legende').html(OriginalStickyLegendeHtml);
            $('.tdc-sticky-legende-wrapper').fadeIn(100);
            
        } else {
            var htmlFromSticky = $('.tdc-sticky-legende-wrapper .sticky-legende').html();
            if (htmlFromSticky != "") {
                $('.wrapper-legende').html(htmlFromSticky);
                $('.tdc-sticky-legende-wrapper .sticky-legende').html("");
                $('.tdc-sticky-legende-wrapper').fadeOut(100);
            } 
        }
        
        // outcategory selected value
        $('#filter_out_categories').val(OutcategorieSelected);
        
    },
    Init: function (){
        if ($('.tdc-sticky-legende-wrapper').length) {
            $(window).scroll(StickyLegende.Stick);
            StickyLegende.Stick();
        }
    },
}

var HistoEntreprise = {
    Hover: function (){
        $('.tdc_history_entreprise').hoverIntent(
            function () {            
                //$(this).next('.comment_tdc_histo_ent').fadeIn(100);
                
                ChatBox.PARAMS.name = $(this).data('entreprise_name');
                ChatBox.PARAMS.title = "Historiques";
                ChatBox.PARAMS.content = $(this).next('.comment_tdc_histo_ent').find('.historiques_comment_wrap').html();
                ChatBox.SHOW();
                Scroller.Init();
                Tooltip.Init();
                
            },
            function () {
                //$(this).next('.comment_tdc_histo_ent').fadeOut(100);
            }
        );
    }
}


var EntrepriseDoc = {
    Hover: function (){
        $('.tdc_doc_fourni').hoverIntent(
            function () {
                // $(this).parent('span.color_societe_label').parent('td.color_societe_bg').find('.tdc_doc_display').fadeIn(100);
                
                ChatBox.PARAMS.name = $(this).data('entreprise_name');
                ChatBox.PARAMS.title = "Documents Entreprise";
                ChatBox.PARAMS.content = $(this).parent('span.color_societe_label').parent('td.color_societe_bg').find('.tdc_doc_display').find('.tdc_doc_wrapper').html();
                ChatBox.SHOW();
                Scroller.Init();
                Tooltip.Init();
                
            },
            function () {
//                if ($(this).hasClass('empty')) {
//                    $(this).parent('span.color_societe_label').parent('td.color_societe_bg').find('.tdc_doc_display').fadeOut(100);
//                }
            }
        );
    },
    Hide: function () {
        $('.tdc_doc_display').mouseleave(function(){
            $(this).fadeOut(100);
        });
    },
}

var SummaryUI = {
    Open: function (){
        $(document).on('click', 'span.triggerSummary.open', function (){
            ajaxBusy = false;
            var url = AJAX_HANDLER + '/tdc-show-hide-summary?op=open';
            $.ajax({
                url: url,
                dataType : 'html',
                cache: false,
                success: function (result, status) {
                    $('.block-body.tdc').slideDown({duration:500, easing:"easeInQuart"});
                    $('span.triggerSummary.open').fadeOut(500);
                    $('span.triggerSummary.close').fadeIn(500);
                    ///$('#base.TDC').css('margin-top', '215px');
                    //headerOffset = 272;
                }
            });
        });
    },
    Close: function (){
        $(document).on('click', 'span.triggerSummary.close', function (){
            ajaxBusy = false;
            var url = AJAX_HANDLER + '/tdc-show-hide-summary?op=close';
            $.ajax({
                url: url,
                dataType : 'html',
                cache: false,
                success: function (result, status) {
                    $('.block-body.tdc').slideUp(500, "easeOutQuart");
                    $('span.triggerSummary.close').fadeOut(500);
                    $('span.triggerSummary.open').fadeIn(500);
                    ///$('#base.TDC').css('margin-top', '55px');
                    //headerOffset = 120;
                }
            });
        });
    },
}

var CandidatCV = {
    Hover: function (){
        $('.tdc_cv_fourni').hoverIntent(
            function () {
//                $(this).next('.tdc_cv_display').fadeIn(100);
//                $(this).parent('span.color_candidat_label').parent('td.color_candidat_bg').find('.tdc_cv_display').fadeIn(100);
                
                ChatBox.PARAMS.name = $(this).data('candidat_name');
                ChatBox.PARAMS.title = "Documents Candidat";
                ChatBox.PARAMS.content = $(this).parent('span.color_candidat_label').parent('td.color_candidat_bg').find('.tdc_cv_display').find('.tdc_cv_wrapper').html();
                ChatBox.SHOW();
                Scroller.Init();
                Tooltip.Init();
                
            },
            function () {
//                if ($(this).hasClass('empty')) {
//                    $(this).parent('span.color_candidat_label').parent('td.color_candidat_bg').find('.tdc_cv_display').fadeOut(100);
//                }
            }
        );
    },
    Hide: function () {
        $('.tdc_cv_display').mouseleave(function(){
            $(this).fadeOut(100);
        });
    },
}

var EntrepriseComment = {
    Hover: function (){
        $('.tdc_entreprise_comment').hoverIntent(
            function () {
                
                ChatBox.PARAMS.name = $(this).data('entreprise_name');
                ChatBox.PARAMS.title = "Commentaires Entreprise";
                ChatBox.PARAMS.content = $(this).next('.tdc_entreprise_comment_hover').html();
                ChatBox.SHOW();
                Scroller.Init();
                Tooltip.Init();
                
//                $(this).next('.tdc_entreprise_comment_hover').fadeIn(100);
//                Scroller.Init();
            },
            function () {
//                if ($(this).hasClass('empty')) {
//                    $(this).next('.tdc_entreprise_comment_hover').fadeOut(100);
//                }
            }
        );
    },
    Hide: function () {
        $('.tdc_entreprise_comment_hover').mouseleave(function(){
            $(this).fadeOut(100);
        });
    },
}

var CandidatComment = {
    Hover: function (){
        $('.tdc_candidat_comment').hoverIntent(
            function () {
                
                ChatBox.PARAMS.name = $(this).data('candidat_name');
                ChatBox.PARAMS.title = "Commentaires Candidat";
                ChatBox.PARAMS.content = $(this).next('.tdc_candidat_comment_hover').html();
                ChatBox.SHOW();
                Scroller.Init();
                Tooltip.Init();
                
                //$(this).next('.tdc_candidat_comment_hover').fadeIn(100);
                //Scroller.Init();
            },
            function () {
                if ($(this).hasClass('empty')) {
                    //$(this).next('.tdc_candidat_comment_hover').fadeOut(100);
                }
            }
        );
    },
    Hide: function () {
        $('.tdc_candidat_comment_hover').mouseleave(function(){
            $(this).fadeOut(100);
        });
    },
}

var Suivi = {
    Hover: function (){
        $('.tdc_suivi_candidate').hoverIntent(
            function () {            
                // $(this).prev('.tdc_suivis_tooltip_wrap').fadeIn(100);
                
                ChatBox.PARAMS.name = $(this).data('candidat_name');
                ChatBox.PARAMS.title = "Suivis";
                ChatBox.PARAMS.content = $(this).prev('.tdc_suivis_wrap_wrap').html();
                ChatBox.SHOW();
                Scroller.Init();
                Tooltip.Init();
                
            },
            function () {
                /// $(this).prev('.tdc_suivis_tooltip_wrap').fadeOut(100);
            }
        );
    },
}

var Histo = {
    Hover: function (){
        $('.tdc_history_candidate').hoverIntent(
            function () {
                
                ChatBox.PARAMS.name = $(this).data('candidat_name');
                ChatBox.PARAMS.title = "Historiques";
                ChatBox.PARAMS.content = $(this).prev('.comment_tdc_histo').html();
                ChatBox.SHOW();
                Scroller.Init();
                Tooltip.Init();
                
//                $(this).prev('.comment_tdc_histo').fadeIn(100);
//                Scroller.Init();
            },
            function () {
                //$(this).prev('.comment_tdc_histo').fadeOut(100);
                // ChatBox.REMOVE(); must close
            }
        );
//        $(document).on('mouseleave', '.comment_tdc_histo', function(){
//            $(this).fadeOut(100);
//        });
    }
}

function frm_tdc_stats_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_criteres_ajax_success(result, status) { // must reload stats
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_out_categories_ajax_success(result, status) { // must reload stats
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_status_ajax_success(result, status) { // must reload stats
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_status_entreprise_ajax_success(result, status) { // must reload stats
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_sms_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('#sms_model.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_historique_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
//        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param); // reload comment
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_historique_edit_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('#historique_edit_comment button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param); // reload edited comment
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_historique_entreprise_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
//        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param); // reload comment
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_historique_edit_entreprise_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('#historique_edit_comment_entreprise button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param); // reload edited comment
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}


function frm_tdc_suivi_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('#suivis_modal button.close').trigger('click');
        // Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param); // reload stats
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param); // reload page callback as we need to refresh the page for suivi hover and scroll to the row
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_suivi_entreprise_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('#suivis_modal_entreprise button.close').trigger('click');
        // Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param); // reload stats
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param); // reload page callback as we need to refresh the page for suivi hover and scroll to the row
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_stats_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param); // reload stats
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_colors_legende_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        ///$('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_colors_legende_entreprise_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        ///$('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_tdc_change_sl_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param); // reload stats
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function reloadhistorique(param) {
    var url = AJAX_UI+'/reload-tdc-historique?mission_id='+param.mission_id+'&candidat_id='+param.candidat_id;
    
    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('.historiques_comment_wrap').html(result);
            $('#comment_histo').val('').removeClass('ok').removeClass('error');
        }
    });
}

function reloadhistoriqueentreprise(param) {
    var url = AJAX_UI+'/reload-tdc-historique-entreprise?mission_id='+param.mission_id+'&entreprise_id='+param.entreprise_id;
    
    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('.historiques_comment_wrap').html(result);
            $('#comment_histo').val('').removeClass('ok').removeClass('error');
        }
    });
}

function reloadstatus(param) {
    var url = AJAX_UI+'/reload-tdc-status?tdc_id='+param.tdc_id;
    
    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('tr.tdc_id_'+param.tdc_id).find('td.change_statut_tdc').html(result);
            // reload stats tdc or filter
            reloadStatsOrFilter(param.mission_id);
            reloadpagescrollto({mission_id:param.mission_id, tdc_id:param.tdc_id});
        }
    });
}

function reloadstatusentreprise(param) {
    var url = AJAX_UI+'/reload-tdc-status-entreprise?tdc_id='+param.tdc_id;
    
    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('tr.tdc_id_'+param.tdc_id).find('td.change_statut_entreprise_tdc').html(result);
            // reload stats tdc or filter
            reloadStatsOrFilter(param.mission_id);
            reloadpagescrollto({mission_id:param.mission_id, tdc_id:param.tdc_id});
        }
    });
}

function reloadStatsOrFilter(mission_id) {
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var tdc_legende_color_ids = $('#tdc_legende_color_ids').val();
    var tdc_legende_entreprise_color_ids = $('#tdc_legende_entreprise_color_ids').val();
    var tdc_legende_out_categorie_id = $('#tdc_legende_out_categorie_id').val();
    if (start_date != "" && end_date != "") {
        var data = {mission_id:mission_id,start_date:start_date,end_date:end_date,tdc_legende_color_ids:tdc_legende_color_ids,tdc_legende_entreprise_color_ids:tdc_legende_entreprise_color_ids, tdc_legende_out_categorie_id:tdc_legende_out_categorie_id};
        filterstatstdc(data);
    } else {
        reloadstatstdc(mission_id);
    }
}

/**
 * 
 * @param Array param{candidat_id, mission_id} 
 */
function reloadoutcategories(param) {
    
    var url = AJAX_UI+'/reload-out-categories?candidat_id='+param.candidat_id+'&mission_id='+param.mission_id;
    
    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('tr.tdc_id_'+param.tdc_id).find('td.tdc_out_category').html(result);
            // reload stats tdc
            reloadStatsOrFilter(param.mission_id);
        }
    });
}

/**
 * 
 * @param Array param{candidat_id, mission_id} 
 */
function reloadcriteres(param) {
    
    var url = AJAX_UI+'/reload-tdc-criteres?candidat_id='+param.candidat_id+'&mission_id='+param.mission_id;
    
    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('tr.tdc_id_'+param.tdc_id).find('td.tdc_criteres_spec').html(result);
            // reload stats tdc
            reloadStatsOrFilter(param.mission_id);
        }
    });
}

function reloadstatstdc(mission_id) {
    var url = AJAX_UI+'/reload-tdc-stats?mission_id='+mission_id;

    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('#stats-tdc-wrapper').html(result);
            filterDateStats.Init();
        }
    });
}

/**
 * 
 * @param Array data = {mission_id,from_start}
 * @returns {undefined}
 */
function filterstatstdc(data) {
    var url = AJAX_UI+'/filter-tdc-stats';

    $.ajax({
        url: url,
        type: 'POST',
        dataType : 'html',
        data: data,
        cache: false,
        success: function (result, status) {
            $('#stats-tdc-wrapper').html(result);
            filterDateStats.Init();
            $('.TDC_TABLE').html("");
            $.ajax({
                url: AJAX_UI+'/filter-tdc-results',
                type: 'POST',
                dataType : 'html',
                data: data,
                cache: false,
                success: function (result, status) {
                    $('.TDC_TABLE').html(result);
                    Table.Init();
                    Suivi.Hover();
                    Histo.Hover();
                    CandidatComment.Hover();
                    CandidatComment.Hide();
                    CandidatCV.Hover();
                    CandidatCV.Hide();
                    EntrepriseComment.Hover();
                    EntrepriseComment.Hide();
                    HistoEntreprise.Hover();
                    EntrepriseDoc.Hover();
                    EntrepriseDoc.Hide();
                }
            });
        }
    });
}

function reloaddocumenttdc(mission_id) {
    // ajax call to reload link
    var url = AJAX_UI+'/reload-mission-document-tdc?id='+mission_id;

    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('.files-list .nano-content').html(result);
        }
    });
}

var Upload = {
    Init: function (){
        $('.inputfile').each(function(){
            
            var $input	 = $( this ),
                $label	 = $input.next( 'label' ),
                labelVal = $label.html();

            $input.on( 'change', function( e )
            {
                var fileName = '';

                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else if( e.target.value )
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    $label.find( 'span.filename' ).html( fileName );
                else
                    $label.html( labelVal );
            });

            // Firefox bug fix
            $input
            .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
            .on( 'blur', function(){ 
                $input.removeClass( 'has-focus' ); 
            });

        });
    },
    Validate: function (){
        $(document).on('change', '.inputfile', function(){
              Upload.ValidateUpload($(this));            
        });
    },
    ValidateUpload: function (input) {
        var file_data = input.prop("files")[0];
        // start validate file extension
        var validExtensions = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx', 'xls', 'xlsx']; //array of valid extensions
        var filename = file_data.name;
        var ext = filename.substr(filename.lastIndexOf('.') + 1);
        if ($.inArray(ext, validExtensions) == -1){
            Notif.Show('Fichier non valide', 'error', true, 4000);
            input.val("");
            input.next('label').removeClass('ok').addClass('error');
            return false;
        } else {
            // check file size
            // 4MB allowed = 4194304
            // 10 MB allowed now = 10485760
            var filesize = file_data.size;
            if (filesize > 10485760) {
                Notif.Show('La taille du fichier est supérieure à celle autorisée. Télécharger moins de 10 Mo.', 'error', true, 4000);
                input.val("");
                input.next('label').removeClass('ok').addClass('error');
                return false;
            } else {
                input.next('label').removeClass('error').addClass('ok');
                return true;
            }
        }
        // end validation file extension

    },
}

function frm_edit_mission_add_files_before_submit(obj) {
    
    var uploadFlag = true;
    var numLabel = parseInt($('#uploadFlag').val());
    
    for(var i = 1; i <= numLabel; i++) {
        
        if ($('#file_'+numLabel).val() == ""){
            $('#uploadLabel_'+numLabel).addClass('error');
            uploadFlag = false;
        }
        
        if ($('#uploadLabel_'+numLabel).length) {
            if ($('#uploadLabel_'+numLabel).hasClass('error')) {
                uploadFlag = false;
            }
        }
    }
    
    return uploadFlag;
    
}

function reloadUserSL(param) {
    
    // append the hidden form tdc suivi value shortliste_old_addded to new date
    $('#shortliste_old_addded').val(param.shortliste_added);
    
    var url = AJAX_UI+'/reload-tdc-user-sl?mission_id='+param.mission_id+'&candidat_id='+param.candidat_id;
    
    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('#change_sl_model').remove();
            $('.changeSL').html(result);
        }
    });
    
    var url = AJAX_UI+'/reload-tdc-user-sl-tdc?mission_id='+param.mission_id+'&candidat_id='+param.candidat_id;
    
    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('.sl_'+param.mission_id+'_'+param.candidat_id).html(result);
        }
    });
    
}