
pageLength = 2000;

$(document).ready(function(){
    
    /*start of comment client*/
    $(document).on('click', '.tdc_client_comment', function(){
        var candidat_id = $(this).data('candidat_id');
        var mission_id = $(this).data('mission_id');
        Modal.Show(AJAX_UI + '/tdc-client-comment?mission_id='+mission_id+'&candidat_id='+candidat_id, null, null, '<span class="glyphicon glyphicon-envelope"></span>', 50);
    });
    /*end of comment client*/
    
    // filter from start
    // we just need to refresh the page
    $(document).on('click', '.filter_stats_debut', function(){
        var mission_id = $(this).data('mission_id');
        goto_tdc_client(mission_id);
    });

    
    // generate pdf candidat
    $(document).on('click', '.generate_pdf_candidat_tdc_client', function(){
        var candidat_id = $(this).data('candidat_id');
        var mission_id = $(this).data('mission_id');
        var societe = $(this).data('societe');
        
        var url = AJAX_HANDLER+'/generate-pdf-candidat-tdc?id='+candidat_id+'&mission_id='+mission_id+'&societe='+societe;
        $.ajax({
            url: url,
            dataType : 'json',
            cache: false,
            success: function (result, status) {
                if (status === "success" && result.status === "OK") {
                    location.href = BASE_URL + '/download-pdf?file='+result.file;
                }
            }
        });
        
        // download candidat files
//        var url = AJAX_HANDLER+'/tdc-client-download-candidat-files?candidat_id='+candidat_id;
//        $.ajax({
//            url: url,
//            dataType : 'json',
//            cache: false,
//            success: function (response) {
//                
//                if ($.isEmptyObject(response)) {
//
//                    return false;
//
//                } else {
//                    
//                    $.each(response, function(index, doc) {
//                        
//                        location.href = BASE_URL + '/download?type=can&id='+doc.id;
//                        
//                    });
//                    
//                }
//
//            }
//        });

    });
    
}); // end document ready function

function frm_tdc_client_comment_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        if ($('#tdc-candidat-client-comment').length) {
            $('#tdc-candidat-client-comment').removeClass('ok').removeClass('error').val('');
        }
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function reloadcomment(param) {
    
    if ($('.historiques_comment_wrap').length) {
    
        // ajax call to reload comment
        var url = AJAX_UI+'/reload-candidat-comment-tdc-client?mission_id='+param.mission_id+'&candidat_id='+param.candidat_id;

        $.ajax({
            url: url,
            dataType : 'html',
            cache: false,
            success: function (result, status) {
                $('#tdc-candidat-client-comment').val('').removeClass('ok').removeClass('error');
                $('.historiques_comment_wrap').html(result);
            }
        });
        
    }
}