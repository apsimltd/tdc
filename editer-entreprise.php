<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Éditer Entreprise" ?>
<?php $page = "add-entreprise"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('edit_candidat', 'create_candidat')) || get('id') == "") header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>
<?php $entreprise = Candidat::getEntrepriseByIdNewNew(get('id')); // debug($entreprise); ?>
<?php if (empty($entreprise)){ header("Location: " . BASE_URL . "/entreprises"); } ?>
<?php $comments = json_decode($entreprise['commentaire'], true) ?>
<?php $documents = json_decode($entreprise['files'], true); ?>
	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base" class="AddEntreprise">

        <div id="contentWrap">					
            
			<section class="title block">

				<h1 class="pull-left"><span class="glyphicon glyphicon-user"></span>Éditer entreprise</h1>
				
                <span style="line-height: 24px; font-size: 12px; font-style: italic; display: inline-block; padding-left: 30px; color: #7cbd5a;">
                    <strong>Date de création :</strong> <?php echo $entreprise['created'] ?>
                </span>
                
                <span style="line-height: 24px; font-size: 12px; font-style: italic; display: inline-block; padding-left: 30px; color: #7cbd5a;">
                    <strong>Date de modification :</strong> <?php echo $entreprise['modified'] ?>
                </span>
                
				<div class="btn-box pull-right">                   
                    
                    <?php if (get('tdc_id') && !get('tdc_line_id')): ?>
                    <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo get('tdc_id') ?>">
                        <button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page TDC"><span class="glyphicon glyphicon-menu-left"></span></button>
                    </a>
                    <?php elseif (get('tdc_id') && get('tdc_line_id')): ?>
                    <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo get('tdc_id') ?>&tdc_id=<?php echo get('tdc_line_id') ?>">
                        <button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page TDC"><span class="glyphicon glyphicon-menu-left"></span></button>
                    </a>
                    <?php else: ?>
                    <button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page précédente" onClick="window.history.back();"><span class="glyphicon glyphicon-menu-left"></span></button>
                    <?php endif; ?>
                    
                    <button type="button" data-entreprise_id="<?php echo $entreprise['id'] ?>" class="generate_pdf_entreprise pull-right btn btn-icon btn-warning tooltips" title="Générer un fichier PDF"><i class="fa fa-file-pdf"></i></button>
                    
                    <?php if (User::can('create_candidat')): ?>
                    <a href="ajoute-entreprise"><button type="button" class="btn btn-icon btn-primary pull-right tooltips" title="Ajouter une nouvelle Entreprise"><span class="glyphicon glyphicon-plus"></span></button></a>
                    <?php endif; ?>
                    
                    <?php if (User::can('tdc') && get('tdc_id') && get('tdc_id') != "" && !TDC::isEntrepriseInTDC(get('tdc_id'), $entreprise['id'])) : ?>                    
                    <a href="<?php echo AJAX_HANDLER ?>/add-entreprise-tdc?entreprise_id=<?php echo $entreprise['id'] ?>&mission_id=<?php echo get('tdc_id') ?>">
                        <button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Ajouter Entreprise au TDC Ref <?php echo get('tdc_id') ?>">
                            <span class="glyphicon glyphicon-screenshot"></span>
                        </button>
                    </a>
                    <?php endif; ?>
				</div>
				
			</section>
			
                <div class="row">

                    <div class="col col-12">
                        <div class="block nopadding">

                            <div class="block-body">

                                <div class="row">

                                    <div class="col col-6">
                                        <div class="block nopadding">

                                            <div class="block-body">
                                                
                                                <form class="frm_frm frm_ajax" name="frm_edit_entreprise" id="frm_edit_entreprise" data-url="<?php echo AJAX_HANDLER ?>/edit-entreprise" data-type="json">
			
                                                    <input type="hidden" name="id" id="company_id" value="<?php echo $entreprise['id'] ?>">

                                                    <?php if (get('tdc_id') && get('tdc_id') != ""): ?>
                                                    <input type="hidden" name="mission_id" value="<?php echo get('tdc_id') ?>">
                                                    <?php endif; ?>
                                                    
                                                    <fieldset class="mission-checkbox">
                                                        <input type="checkbox" class="frm_checkbox" name="is_mother_company" id="is_mother_company" <?php if ($entreprise['is_mother'] == 1): ?>checked="checked"<?php endif; ?>>
                                                        <label class="pointer" for="is_mother_company">Est Une Entreprise Mère</label>
                                                    </fieldset>
                                                    <input type="hidden" name="is_mother" id="is_mother" value="<?php echo $entreprise['is_mother'] ?>">
                                                    
                                                    <fieldset>
                                                        <label>Nom</label>
                                                        <input class="frm_text caps must checkSocieteBlackliste <?php if ($entreprise['name'] != ""): ?>ok<?php endif; ?>" name="name" placeholder="Nom" type="text" autocomplete="off" data-validation="val_blank" value="<?php echo $entreprise['name'] ?>">
                                                        
                                                        <input type="hidden" id="name_actual" value="<?php echo $entreprise['name'] ?>">
                                                        
                                                        <?php $style = 'style="display:none;"'; ?>
                                                        <?php if ($entreprise['name'] != "" && Client::getClientCandidatBlacklist($entreprise['name'])): ?>
                                                            <?php $style = 'style="display:block;"'; ?>
                                                        <?php endif; ?>
                                                        <div class="msgBlackliste" <?php echo $style ?>>
                                                            <div class="clearboth notif warning pull-right" style="background-color:#ff9800; color: #fff; width: 70%;line-height: 24px; padding: 0px 10px;"><strong>Attention :</strong> La Société est blacklistée</div>
                                                        </div>
                                                        
                                                    </fieldset>
                                                    
<!--                                                    <fieldset class="mother_company">
                                                        <label>Entreprise Mère</label>
                                                        <input class="frm_text caps  <?php if ($entreprise['is_mother'] == 0): ?>ok must<?php endif; ?>" id="mother_autocomplete" placeholder="Entreprise Mère" type="text" data-validation="val_blank"  <?php if ($entreprise['is_mother'] == 1): ?>disabled="disabled"<?php else: ?>value="<?php echo $entreprise['mother'] ?>"<?php endif; ?>>
                                                        <input type="hidden" name="parent_id" id="mother_autocomplete_id"  <?php if ($entreprise['is_mother'] == 0): ?>value="<?php echo $entreprise['parent_id'] ?>"<?php endif; ?>>
                                                    </fieldset>-->

                                                    <fieldset class="mother_company" <?php if ($entreprise['is_mother'] == 1): ?>style="display:none;"<?php endif; ?>>
                                                        <label>Entreprise Mère</label>
                                                        <select class="frm_chosen <?php if ($entreprise['is_mother'] == 0): ?>ok<?php endif; ?>" name="parent_id" id="parent_id" data-validation="val_blank">
                                                            <option value="">Choisir Entreprise Mère</option>
                                                            <?php $meres = Candidat::getEntrepriseMeres() ?>
                                                            <?php foreach($meres as $mere): ?>
                                                            <option value="<?php echo $mere['id'] ?>" <?php if ($entreprise['parent_id'] == $mere['id']): ?>selected="selected"<?php endif; ?>>
                                                                <?php echo $mere['name'] ?>
                                                                <?php if ($mere['blacklist'] == 1) :?>
                                                                 - [Blacklistée]
                                                                <?php endif;?>
                                                            </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </fieldset>

                                                    <fieldset>
                                                        <label>Localisation</label>
                                                        <select class="frm_chosen <?php if ($entreprise['control_localisation_id'] > 0): ?>ok<?php endif; ?>" name="control_localisation_id" data-validation="val_blank">
                                                            <option value="">Choisir Localisation</option>
                                                            <?php $controls = Control::getControlByType(6, 1) ?>
                                                            <?php foreach($controls as $control): ?>
                                                            <option value="<?php echo $control['id'] ?>" <?php if ($entreprise['control_localisation_id'] == $control['id']): ?>selected="selected"<?php endif; ?>>
                                                                <?php echo $control['name'] ?>
                                                            </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </fieldset>
                                                    
                                                    <fieldset>
                                                        <label>Précision adresse</label>
                                                        <textarea name="addresse" class="frm_textarea <?php if ($entreprise['addresse'] <> ""): ?>ok<?php endif; ?>" placeholder="Précision adresse" data-validation="val_blank" maxlength="255"><?php echo $entreprise['addresse'] ?></textarea>                                
                                                    </fieldset>

                                                    <fieldset>
                                                        <label>Secteur</label>
                                                        <select class="frm_chosen <?php if ($entreprise['secteur_activite_id'] > 0): ?>ok<?php endif; ?>" name="secteur_activite_id" data-validation="val_blank">
                                                            <option value="">Choisir Secteur</option>
                                                            <?php $controls = Control::getControlByType(2, 1) ?>
                                                            <?php foreach($controls as $control): ?>
                                                            <option value="<?php echo $control['id'] ?>"  <?php if ($entreprise['secteur_activite_id'] == $control['id']): ?>selected="selected"<?php endif; ?>>
                                                                <?php echo $control['name'] ?>
                                                            </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </fieldset>
                                                                                                
                                                    <fieldset>
                                                        <label>Sous Secteur Activité</label>
                                                        <input class="frm_text <?php if ($entreprise['sous_secteur_activite'] != ""): ?>ok<?php endif; ?>" name="sous_secteur_activite" placeholder="Sous Secteur Activité" type="text" autocomplete="off" data-validation="val_blank" value="<?php echo $entreprise['sous_secteur_activite'] ?>">
                                                    </fieldset>
                                                
                                                    <fieldset>
                                                        <label>Téléphone</label>
                                                        <input class="frm_text tel_fr <?php if ($entreprise['telephone'] != ""): ?>ok<?php endif; ?>" name="telephone" placeholder="Téléphone" type="text" autocomplete="off" data-validation="val_blank" value="<?php echo $entreprise['telephone'] ?>">
                                                    </fieldset>
                                                    
                                                    <fieldset>
                                                        <label>Email</label>
                                                        <input class="frm_text <?php if ($entreprise['email'] != ""): ?>ok<?php endif; ?>" name="email" placeholder="Email" type="text" autocomplete="off" data-validation="val_blank" value="<?php echo $entreprise['email'] ?>">
                                                    </fieldset>

                                                    <fieldset>
                                                        <label>Région</label>
                                                        <input class="frm_text <?php if ($entreprise['region'] != ""): ?>ok<?php endif; ?>" name="region" placeholder="Région" type="text" disabled="disabled" autocomplete="off" data-validation="val_blank" value="<?php echo $entreprise['region'] ?>">
                                                    </fieldset>

                                                    <fieldset class="client-checkbox">
                                                        <input type="checkbox" class="frm_checkbox" id="blacklist" <?php if ($entreprise['blacklist'] == 1): ?>checked="checked"<?php endif; ?> name="blacklist">
                                                        <label class="pointer" for="blacklist">Blacklister</label>
                                                    </fieldset>
                                                    
                                                    <fieldset>
                                                        <button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><span class="glyphicon glyphicon-pencil"></span> Éditer Entreprise</button>
                                                    </fieldset>
                                                
                                                </form>

                                                
                                            </div><!-- / block-body -->

                                        </div>
                                    </div><!-- /col -->
                                    
                                    <div class="col col-6">
                                        <div class="block nopadding">

                                            <div class="block-body nopadding">

                                                <div class="block-head with-border with-top-border">
                                                    <header><span class="glyphicon glyphicon-folder-open"></span>Documents</header>
                                                </div>

                                                <div class="documents_wrapper padding-10">

                                                    <?php if (!empty($documents)) :?>
                                                        <ul class="links_candidat">
                                                        <?php foreach($documents as $key => $document): ?>
                                                            <?php 
                                                            $imgType = array('jpg', 'jpeg', 'png');
                                                            $ext = getFileExt($document['file']);
                                                            if (in_array($ext, $imgType)):
                                                            ?>
                                                            <li>
                                                                <a href="<?php echo ENTREPRISE_URL ?>/<?php echo $entreprise['id'] ?>/<?php echo $document['file'] ?>" data-lightbox="<?php echo $document['file'] ?>"><?php echo $document['file'] ?></a>
                                                                <span class="glyphicon glyphicon-trash delete_document tooltips" title="Supprimer" data-document_id="<?php echo $key ?>" data-entreprise_id="<?php echo $entreprise['id'] ?>"></span>
                                                            </li>
                                                            <?php else: ?>
                                                            <li>
                                                                <a href="<?php echo BASE_URL ?>/download?type=ent&key=<?php echo $key ?>&id=<?php echo $entreprise['id'] ?>"><?php echo $document['file'] ?></a>
                                                                <span class="glyphicon glyphicon-trash delete_document tooltips" title="Supprimer" data-document_id="<?php echo $key ?>" data-entreprise_id="<?php echo $entreprise['id'] ?>"></span>
                                                            </li>
                                                            <?php endif; ?>
                                                        <?php endforeach;?>
                                                        </ul>
                                                    <?php endif; ?>

                                                    <form class="frm_frm" name="frm_edit_entreprise_add_files" id="frm_edit_entreprise_add_files" action="<?php echo AJAX_HANDLER ?>/add-entreprise-file" method="POST" enctype="multipart/form-data">
                                                        
                                                        <input type="hidden" name="entreprise_id" value="<?php echo $entreprise['id'] ?>">

                                                        <?php if (get('tdc_id') && get('tdc_id') != ""): ?>
                                                        <input type="hidden" name="mission_id" value="<?php echo get('tdc_id') ?>">
                                                        <?php endif; ?>

                                                        <fieldset class="uploadMultiple">
                                                            <div class="clearfix">
                                                                <label class="pull-left">Documents <span class="glyphicon glyphicon-info-sign tooltips" title="Les fichiers autorisés sont : .doc, .docx, .xls, .xlsx, .pdf, .jpg, .jpeg, .png ; Taille max : 10 Mo"></span></label>
                                                                <input type="hidden" name="uploadFlag" id="uploadFlag" value="1">
                                                                <div class="clearfix add_field_wrap_documents pull-left" style="width: 60%;">
                                                                    <div class="add_field_row pull-left">
                                                                        <input type="file" name="file[]" id="file_1" data-number="1" class="inputfile inputfile-1">
                                                                        <label id="uploadLabel_1" for="file_1" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span></label>
                                                                        <button type="button" class="clear_field_upload btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="mt6 clearfix">
                                                                <button type="button" class="add_field_upload btn btn-icon btn-success pull-right tooltips" title="Ajouter un nouveau fichier"><span class="glyphicon glyphicon-plus"></span></button>
                                                            </div>                              
                                                        </fieldset>

                                                        <fieldset>
                                                            <button type="button" class="btn btn-primary frm_submit frm_before_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter Document/s</button>
                                                        </fieldset>

                                                    </form>

                                                </div><!-- /documents_wrapper -->

                                            </div><!-- / block-body -->

                                        </div>
                                    </div><!-- /col -->

                                </div><!-- / row -->	

                            </div><!-- / block-body -->

                        </div>
                    </div><!-- /col -->

                </div><!-- / row -->
			
			<div class="row edit_candidat_nano">
				
				<div class="col col-12">
					<div class="block nopadding">
												
						<div class="block-body">
							
							<div class="row">
			
								<div class="col col-6">
									<div class="block nopadding">
										
										<div class="block-head with-border">
											<header><span class="glyphicon glyphicon-comment"></span>Commentaire Entreprise</header>
										</div>
																
										<div class="block-body">
											<form class="frm_frm frm_ajax" name="frm_edit_entreprise_add_comment" id="frm_edit_entreprise_add_comment" data-url="<?php echo AJAX_HANDLER ?>/add-entreprise-comment" data-type="json">
                                                <input type="hidden" name="entreprise_id" value="<?php echo $entreprise['id'] ?>">
												<fieldset class="full">
                                                    <textarea name="commentaire" class="frm_textarea must add_new_comment" placeholder="Ajouter un nouveau commentaire" data-validation="val_blank" style="width: 100%;"></textarea>                                
												</fieldset>
												
												<fieldset>
													<button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter commentaire</button>
												</fieldset>
												
											</form>
											
											<div class="nano">
                            					<div class="nano-content">
												
													<div class="historiques_comment_wrap">
													
														<?php if (!empty($comments)): ?>
                                                        <div class="task-threads-wrap clearfix">
                                                                                                                        
                                                            <?php foreach (array_reverse($comments, true) as $key => $comment): ?>
                                                            <div class="task-thread clearfix">

                                                                <p><?php echo nl2br($comment['comment']) ?></p>

                                                                <div class="publish-info">
                                                                    Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo $comment['created'] ?>
                                                                    <span class="glyphicon glyphicon-edit edit_comment tooltips" title="Éditer" data-comment_id="<?php echo $key ?>" data-entreprise_id="<?php echo $entreprise['id'] ?>"></span>
                                                                    <span class="glyphicon glyphicon-trash delete_comment tooltips" title="Supprimer" data-comment_id="<?php echo $key ?>" data-entreprise_id="<?php echo $entreprise['id'] ?>"></span>
                                                                </div>

                                                            </div>
                                                            <?php endforeach; ?>

                                                        </div><!--/ task-threads-wrap -->
                                                        <?php endif; ?>
														
													</div><!-- /historiques_comment_wrap -->
													
												</div><!-- /nano-content-->
											</div><!-- /nano -->

										</div><!-- / block-body -->
										
									</div>
								</div><!-- /col -->
								
								<div class="col col-6">
									<div class="block nopadding">
										
										<div class="block-head with-border">
											<header><span class="glyphicon glyphicon-tasks"></span>Historiques Missions</header>
										</div>
																
                                        <div class="block-body">
                                            
                                            <?php $suivis = Candidat::getSuivisEntreprise(get('id')); ///debug($suivis); ?> 
                                            <?php if (!empty($suivis)): ?>
                                            <div class="nano" style="height: 450px;">
                                                <div class="nano-content">
                                                    <table class="table-list compact inside-nano">
                                                        <thead>
                                                            <tr>
                                                                <th>Mission</th>
                                                                <th>Statut</th>
                                                                <th>Client</th>
                                                                <th>Poste</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach($suivis as $suivi): ?>
                                                            <tr>
                                                                <td align="center"><a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $suivi['mission_id'] ?>"><?php echo $suivi['mission_id'] ?></a></td>
                                                                <td align="center">
                                                                   <?php if ($suivi['status_entreprise'] == "" || $suivi['status_entreprise'] == 0): ?>
                                                                        <?php // do nothing ?>
                                                                    <?php elseif ($suivi['status_entreprise'] == 257): ?>
                                                                    <span class="status out">
                                                                        OUT
                                                                    </span>
                                                                    <?php elseif ($suivi['status_entreprise'] == 258): ?>
                                                                    <span class="status buminus">
                                                                        FINI
                                                                    </span>
                                                                    <?php endif; ?>
                                                                    
                                                                </td>
                                                                <td><?php echo $suivi['client'] ?></td>
                                                                <td><?php echo $suivi['poste'] ?></td>
                                                                
                                                            </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>   
                                                </div>
                                            </div><!-- /nano -->
                                            <?php else: ?>
                                            <div class="message warning">Cette entreprise n'a pas des suivis dans aucun mission.</div>
                                            <?php endif; ?>
											
										</div><!-- / block-body -->
										                                        
									</div>
								</div><!-- /col -->
								
							</div><!-- / row -->
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
        	
            <?php if (WATCH): ?>
			<div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                            <header><span class="glyphicon glyphicon-tasks"></span>Historiques Actions <span class="glyphicon tooltips refresh-watchdog-entreprise glyphicon-refresh" title="Actualiser Les Historiques"></span></header>
                        </div>
						
						<div class="block-body">
							
                            <?php $audits = Watchdog::WatchEntreprise($entreprise['id']); // debug($audits) ?>
                            <?php if (!empty($audits)): ?>
                            <div class="nano" style="min-height: 300px;max-height: 500px;">
                                <div class="nano-content">
                                    <table class="table-list compact inside-nano">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Utilisateur</th>
                                                <th>Rôle</th>
                                                <th>Action</th>
                                                <th>Voir Plus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($audits as $audit): ?>
                                            <tr>
                                                <td><?php echo $audit['created'] ?></td>
                                                <td><?php echo $audit['user_name'] ?></td>
                                                <td><?php echo $audit['user_role_name'] ?></td>
                                                <td><?php echo $audit['action'] ?></td>
                                                <td class="actions_button">
                                                    <button type="button" data-watchdog_id="<?php echo $audit['id'] ?>" class="see_more_entreprise_watchdog btn btn-icon btn-success tooltips" title="Voir Plus"><span class="glyphicon glyphicon-eye-open"></span></button> 
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>   
                                </div>
                            </div><!-- /nano -->
                            <?php else: ?>
                            <div class="message warning">Cette entreprise n'a pas des historiques actions</div>
                            <?php endif; ?>
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
            <?php endif; ?>
			
        </div><!-- /contentWrap -->
        
    </div><!-- /base --> 
	
	<?php require_once './views/debug.php'; ?>  
	
</body>
</html>
<script src="<?php echo JS_URL ?>/entreprise.js"></script>
<script>
$(document).ready(function(){
    
    $(document).on('ifUnchecked', '#is_mother_company', function(){
        $('#is_mother').val('null');
        //$('#mother_autocomplete').addClass('must').removeAttr('disabled');
        $('#parent_id').addClass('must');
        $('fieldset.mother_company').fadeIn(100);
        
    });
    $(document).on('ifChecked', '#is_mother_company', function(){
        $('#is_mother').val('1');
        //$('#mother_autocomplete').removeClass('must').attr('disabled', 'disabled');
        $('#parent_id').removeClass('must');
        $('fieldset.mother_company').fadeOut(100);
    });
    
//    $("#mother_autocomplete").autocomplete({
//        minLength: 3,
//        source: function(request, response) {
//            // Fetch data
//            $.ajax({
//                url: AJAX_HANDLER + "/search-candidat-companies-mother",
//                type: 'post',
//                dataType: "json",
//                data: {
//                    search: request.term
//                },
//                // response must be {value: id of the data, label: name to display}
//                success: function( data ) {
//                    response( data );
//                }
//            });
//        },
//        select: function (event, ui) {
//            // Set selection
//            $('#mother_autocomplete').val(ui.item.label); // display the selected text
//            $('#mother_autocomplete_id').val(ui.item.value); // save selected id to input
//            return false;
//        },
//        focus: function(event, ui){
//            $('#mother_autocomplete').val(ui.item.label); // display the selected text
//            $('#mother_autocomplete_id').val(ui.item.value); // save selected id to input
//            return false;
//        },
//    });
    
    
    // check candidat societe blacklister
    $(document).on('blur', '.checkSocieteBlackliste', function(){
        if ($(this).val() != "") {
            
            ajaxBusy = true;
            var nom_societe = $(this).val();
            
            // check if the company already exist and add
            var company_id = $('#company_id').val();
            $.ajax({
                url: AJAX_HANDLER+'/check-societe-candidat-if-exist-edit?nom_societe='+nom_societe+'&id='+company_id,
                dataType : 'json',
                cache: false,
                success: function (result) {
                    if (result.status === "NOK") {
                        $('.frm_text.checkSocieteBlackliste').val($('#name_actual').val());
                        Notif.Show('Entreprise ' + nom_societe + ' existe déjà', 'error', false);
                    } else {
                        $('.frm_text.checkSocieteBlackliste').removeClass('error').addClass('ok');
                        var url = AJAX_HANDLER+'/check-societe-candidat-blackliste?nom_societe='+nom_societe;
                        $.ajax({
                            url: url,
                            dataType : 'json',
                            cache: false,
                            success: function (result, status) {
                                if (status === "success" && result.status === "OK") {
                                    $('.msgBlackliste').find('.notif').html(result.msg);
                                    $('.msgBlackliste').fadeIn();
                                } else if (status === "success" && result.status === "NOK") {
                                    $('.msgBlackliste').fadeOut();
                                }
                            }
                        });
                        
                    }
                }
            });
            
                        
            
        } else {
            $('.msgBlackliste').fadeOut();
        }
            
    });
    
    Upload.Init();
    Upload.Validate();
    
    // refresh suivi watchdog 
    // reload the page
    $(document).on('click', 'span.refresh-watchdog-entreprise', function(){
        location.reload();
    });
    
    // see more details for watchog
    $(document).on('click', '.see_more_entreprise_watchdog', function(){
        var watchdog_id = $(this).data('watchdog_id');
        ajaxBusy = true;
        Modal.Show(AJAX_UI + '/watchdog-entreprise?id='+watchdog_id, null, null, '<span class="glyphicon glyphicon-tasks">', 80, 'watchdog_candidat');
    });
        
    // add a new field for upload
    $(document).on('click', 'button.add_field_upload', function(){
        
        // get the last input number
        var number = parseInt($('.add_field_wrap_documents .add_field_row:last input.inputfile').attr('data-number')) + 1;
        
        var inputHtml = '<input type="file" name="file[]" id="file_'+number+'" data-number="'+number+'" class="inputfile inputfile-1"><label id="uploadLabel_'+number+'" for="file_'+number+'" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span></label><button type="button" class="clear_field_upload btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>';
        var newFieldHtml = '<div class="add_field_row clearfix">'+inputHtml+'</div>';
        $('.add_field_wrap_documents').append(newFieldHtml);
        
        var flagNumber = parseInt($('#uploadFlag').val()) + 1;
        $('#uploadFlag').val(flagNumber);
        
        Upload.Init();
    });
    
    $(document).on('click', '.clear_field_upload', function(){
        
        var numRows = parseInt($('fieldset.uploadMultiple .add_field_wrap_documents .add_field_row:last').index()) + 1;
        
        if (numRows >= 2) {
            $(this).parent('.add_field_row').remove();
        } else if (numRows == 1) {
            $(this).parent('.add_field_row').find('input.inputfile').val('');
            $(this).parent('.add_field_row').find('label').html('<span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span>');
        }
        
    });
         
}); // end document ready function

var Upload = {
    Init: function (){
        $('.inputfile').each(function(){
		
            var $input	 = $( this ),
                $label	 = $input.next( 'label' ),
                labelVal = $label.html();

            $input.on( 'change', function( e )
            {
                var fileName = '';

                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else if( e.target.value )
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    $label.find( 'span.filename' ).html( fileName );
                else
                    $label.html( labelVal );
            });

            // Firefox bug fix
            $input
            .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
            .on( 'blur', function(){ 
                $input.removeClass( 'has-focus' ); 
            });

        });
    },
    Validate: function (){
        $(document).on('change', '.inputfile', function(){
              Upload.ValidateUpload($(this));            
        });
    },
    ValidateUpload: function (input) {
        var file_data = input.prop("files")[0];
        // start validate file extension
        var validExtensions = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx', 'xls', 'xlsx']; //array of valid extensions
        var filename = file_data.name;
        var ext = filename.substr(filename.lastIndexOf('.') + 1);
        if ($.inArray(ext, validExtensions) == -1){
            Notif.Show('Fichier non valide', 'error', true, 4000);
            input.val("");
            input.next('label').removeClass('ok').addClass('error');
            return false;
        } else {
            // check file size
            // 4MB allowed
            var filesize = file_data.size;
            if (filesize > 10485760) {
                Notif.Show('La taille du fichier est supérieure à celle autorisée. Télécharger moins de 10 Mo.', 'error', true, 4000);
                input.val("");
                input.next('label').removeClass('ok').addClass('error');
                return false;
            } else {
                input.next('label').removeClass('error').addClass('ok');
                return true;
            }
        }
        // end validation file extension

    },
}


</script>