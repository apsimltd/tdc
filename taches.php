<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Tâches" ?>
<?php $page = "tasks"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('create_task', 'read_task', 'read_all_task', 'edit_task', 'delete_task', 'archive_task'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>

	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base" class="tasks">

        <div id="contentWrap">
        	
            <section class="title block">

				<h1 class="pull-left">Tâches</h1>
				
				<div class="btn-box pull-right">
                    <button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page précédente" onClick="window.history.back();"><span class="glyphicon glyphicon-menu-left"></span></button>
                    <?php if (User::can('create_task')): ?>
                    <button type="button" class="create_task btn btn-icon btn-primary pull-right tooltips" title="Ajouter Tâche"><span class="glyphicon glyphicon-plus"></span></button>
                    <?php endif; ?>
				</div>

			</section>
            					
            <div class="task-row">
            
                <div class="row">
                    
                    <div class="col col-12">
                        <div class="block nopadding">
                            
                            <div class="block-head with-border">
                                <header><span class="glyphicon glyphicon-tags"></span>Mes tâches</header>
                            </div>
                            
                            <div class="block-body nopadding">
                                
                                <div class="tasks_wrapper clearfix tasksAssignor">
                                    
                                    <?php $createdTasks = Task::getTaskListAssignee($me['id']) ?>
                                                                        
                                    <?php if (!empty($createdTasks)): ?>
                                    
                                        <?php foreach($createdTasks as $task): ?>
                                        <div class="task-list-row task_rows_<?php echo $task['id'] ?> <?php if ($task['status'] == 0): ?>completed<?php endif; ?>">
                                            <div class="task-check tooltips" title="<?php if ($task['status'] == 1): ?>Terminé<?php else: ?>Incomplet<?php endif; ?>" data-task_id="<?php echo $task['id'] ?>">
                                                <svg viewBox="0 0 32 32"><polygon points="27.672,4.786 10.901,21.557 4.328,14.984 1.5,17.812 10.901,27.214 30.5,7.615 "></polygon></svg>
                                            </div>
                                            <div class="task-title">
                                                <?php echo $task['title'] ?>
                                                <?php if (User::can('read_task', 'read_all_task', 'create_task')): ?>
                                                <span class="glyphicon glyphicon-eye-open tooltips view_task" data-task_id="<?php echo $task['id'] ?>" title="Voir Plus"></span>
                                                <?php endif; ?>
                                            </div>
                                            <?php $creator = User::getUserById($task['creator_id']); ?>
                                            <div class="task-creator">
                                                <?php echo dateToFr($task['created'], true) ?> - <strong><?php echo mb_ucfirst(substr($creator['firstName'], 0, 1)) . '. ' . mb_strtoupper($creator['lastName']) ?></strong>
                                                
                                                <?php if (User::can('archive_task')): ?>
                                                    <?php if ($task['status'] == 0): ?><i class="fa fa-archive archive_task tooltips" title="Archiver" data-task_id="<?php echo $task['id'] ?>"></i><?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div><!-- /task-list-row -->
                                        <?php endforeach; ?>
                                    
                                    <?php else: ?>
                                    
                                    <div class="task-list-row completed">
                                        <div class="task-title">
                                            Aucune tâche                                        
                                        </div>
                                    </div><!-- /task-list-row -->
                                    
                                    <?php endif; ?>
                                    
                                </div><!-- /tasks_wrapper -->
                                
                            </div><!-- / block-body -->
                            
                        </div>
                    </div><!-- /col -->
                                    
                </div><!-- / row -->
                
                <?php if (User::can(array('create_task', 'read_task', 'edit_task', 'delete_task', 'archive_task'))): ?>
                
                <div class="row nextLine">
                    
                    <div class="col col-12">
                        <div class="block nopadding">
                            
                            <div class="block-head with-border">
                                <header><span class="glyphicon glyphicon-tags"></span>Les tâches que j'ai créées</header>
                            </div>
                            
                            <div class="block-body nopadding">
                                
                                <div class="tasks_wrapper clearfix tasksCreator">
                                    
                                    <?php $createdTasks = Task::getTaskListCreator($me['id']) ?>
                                    
                                    <?php if (!empty($createdTasks)): ?>
                                    
                                        <?php foreach($createdTasks as $task): ?>
                                        <div class="task-list-row task_rows_<?php echo $task['id'] ?> <?php if ($task['status'] == 0): ?>completed<?php endif; ?>">
                                            <div class="task-check tooltips" title="<?php if ($task['status'] == 1): ?>Terminé<?php else: ?>Incomplet<?php endif; ?>" data-task_id="<?php echo $task['id'] ?>">
                                                <svg viewBox="0 0 32 32"><polygon points="27.672,4.786 10.901,21.557 4.328,14.984 1.5,17.812 10.901,27.214 30.5,7.615 "></polygon></svg>
                                            </div>
                                            <div class="task-title">
                                                <?php echo $task['title'] ?>
                                                <?php if (User::can('read_task', 'read_all_task', 'create_task')): ?>
                                                <span class="glyphicon glyphicon-eye-open tooltips view_task" data-task_id="<?php echo $task['id'] ?>" title="Voir Plus"></span>
                                                <?php endif; ?>
                                            </div>
                                            <?php $assignee = User::getUserById($task['assignee_id']); ?>
                                            <div class="task-creator">
                                                <?php echo dateToFr($task['created'], true) ?> - <strong><?php echo mb_ucfirst(substr($assignee['firstName'], 0, 1)) . '. ' . mb_strtoupper($assignee['lastName']) ?></strong>
                                                <?php if (User::can('edit_task')): ?>
                                                    <?php if ($task['status'] == 1): ?><span class="glyphicon glyphicon-edit edit_task tooltips" title="Éditer" data-task_id="<?php echo $task['id'] ?>"></span><?php endif; ?>
                                                <?php endif; ?>
                                                <?php if (User::can('archive_task')): ?>
                                                    <?php if ($task['status'] == 0): ?><i class="fa fa-archive archive_task tooltips" title="Archiver" data-task_id="<?php echo $task['id'] ?>"></i><?php endif; ?>
                                                <?php endif; ?>
                                                <?php if (User::can('delete_task')): ?>
                                                    <?php if ($task['status'] == 1): ?><span class="glyphicon glyphicon-trash delete_task tooltips" title="Supprimer" data-task_id="<?php echo $task['id'] ?>"></span><?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div><!-- /task-list-row -->
                                        <?php endforeach; ?>
                                    
                                    <?php else: ?>
                                    
                                    <div class="task-list-row completed">
                                        <div class="task-title">
                                            Aucune tâche                                        
                                        </div>
                                    </div><!-- /task-list-row -->
                                    
                                    <?php endif; ?>
                                    
                                </div><!-- /tasks_wrapper -->
                                
                            </div><!-- / block-body -->
                            
                        </div>
                    </div><!-- /col -->
                    
                </div><!-- / row -->
                
                <?php endif; ?>
                
            </div><!-- /task-row -->
        	
            <?php if (User::can('read_task', 'read_all_task', 'create_task')): ?>
            <div class="task-screen">
            	<?php #load by ajax ?>
            </div><!-- /task-screen -->
            <?php endif; ?>
            
        </div><!-- /contentWrap -->
        
    </div><!-- /base --> 
	
	<?php require_once './views/debug.php'; ?>
    
</body>
</html>
<script>
$(document).ready(function(){
    // task
    <?php if (User::can('read_task', 'read_all_task', 'create_task')): ?>
        Task.Show();
        Task.Close();
        Task.CompleteLarge();
    <?php endif; ?>
    <?php if (User::can('read_task')): ?>Task.CompleteSmall();<?php endif; ?>
    <?php if (User::can('create_task')): ?>Task.Add();<?php endif; ?>
    <?php if (User::can('edit_task')): ?>Task.Edit();<?php endif; ?>
    <?php if (User::can('delete_task')): ?>Task.Delete();<?php endif; ?>
    <?php if (User::can('archive_task')): ?>Task.Archive();<?php endif; ?>
        
    <?php if (isGet() && get('id') != ""): ?>
    $('.task-screen').load(AJAX_UI+'/view-task?id='+<?php echo get('id') ?>);
    <?php endif; ?>
});

<?php if (User::can('create_task')): ?>
function frm_create_task_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}
<?php endif; ?>
    
<?php if (User::can('edit_task')): ?>
function frm_edit_task_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}
<?php endif; ?>
    
<?php if (User::can('read_task', 'read_all_task', 'create_task')): ?>
function frm_add_task_thread_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function reloadtaskview(task_id) {
    $.ajax({
        url: AJAX_UI+'/view-task?id='+task_id,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('.task-screen').html(result);
        }
    });
}

<?php endif; ?>

<?php if (User::can(array('create_task', 'read_task', 'edit_task', 'delete_task', 'archive_task'))): ?>
function reloadtaskcreator(user_id) {
    $.ajax({
        url: AJAX_UI+'/reload-task-creator?id='+user_id,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('.tasksCreator').html(result);
        }
    });
}
<?php endif; ?>

var Task = {
    <?php if (User::can('read_task', 'read_all_task', 'create_task')): ?>
    Show: function (){
        $(document).on('click', '.view_task', function (){
            var task_id = $(this).attr('data-task_id');
            $.ajax({
                url: AJAX_UI+'/view-task?id='+task_id,
                dataType : 'html',
                cache: false,
                success: function (result, status) {
                    $('.task-screen').html(result);
                }
            });
        });
    },
    Close: function () {
        $(document).on('click', '.close_task', function (){
            $('.task-screen .row').fadeOut(100).remove();
        });
    },
    CompleteLarge: function (){
        $(document).on('click', '.task-screen .task-check', function () {            
            var task_id = $(this).attr('data-task_id'); 
            parent = $(this);
            $.ajax({
                url: AJAX_UI+'/task-row?id='+task_id,
                dataType : 'html',
                cache: false,
                success: function (result, status) {
                    $('.task-list-row.task_rows_'+task_id).html(result);
                    if (parent.hasClass('completed')) {
                        $('.task-list-row.task_rows_'+task_id).removeClass('completed');
                        parent.removeClass('completed');
                        Notif.Show('La tâche a été marquée comme incomplète avec succès', 'success', true, 3000);
                        $('#frm_add_task_thread').show(100);
                    } else {
                        parent.addClass('completed');
                        $('.task-list-row.task_rows_'+task_id).addClass('completed');
                        $('#frm_add_task_thread').hide(100);
                        Notif.Show('La tâche a été marquée comme terminée avec succès', 'success', true, 3000);
                    } 
                }
            });
        });
    },
    <?php endif; ?>
    <?php if (User::can('read_task')): ?>
    CompleteSmall: function (){
        $(document).on('click', '.task-list-row .task-check', function () {
            var parent = $(this).parent('.task-list-row');
            var task_id = $(this).attr('data-task_id');            
            $.ajax({
                url: AJAX_UI+'/task-row?id='+task_id,
                dataType : 'html',
                cache: false,
                success: function (result, status) {
                    $('.task-screen').html('');
                    parent.html(result);
                    if (parent.hasClass('completed')) {
                        parent.removeClass('completed');
                        Notif.Show('La tâche a été marquée comme incomplète avec succès', 'success', true, 3000);
                        reloadpagenoparam();
                    } else {
                        parent.addClass('completed');
                        Notif.Show('La tâche a été marquée comme terminée avec succès', 'success', true, 3000);
                        reloadpagenoparam();
                    }
                    
                }
            });
        });
    },
    <?php endif; ?>
    <?php if (User::can('create_task')): ?>
    Add: function (){
        $(document).on('click', '.create_task', function (){
            Modal.Show(AJAX_UI + '/add-task', null, null, '<span class="glyphicon glyphicon-tags"></span>', 50);
        });
    },
    <?php endif; ?>
    <?php if (User::can('edit_task')): ?>
    Edit: function (){
        $(document).on('click', '.edit_task', function (){
            var task_id = $(this).attr('data-task_id');
            Modal.Show(AJAX_UI + '/edit-task?id='+task_id, null, null, '<span class="glyphicon glyphicon-tags"></span>', 50);
        });
    },
    <?php endif; ?>
    <?php if (User::can('delete_task')): ?>
    Delete: function (){
        $(document).on('click', '.delete_task', function (){
            var task_id = $(this).attr('data-task_id');
            Dialog.Show('Veuillez confirmer', 'Supprimer tâche', AJAX_HANDLER + '/delete-archive-task?id='+task_id+'&op=delete');
        });
    },
    <?php endif; ?>
    <?php if (User::can('archive_task')): ?>
    Archive: function (){
        $(document).on('click', '.archive_task', function (){
            var task_id = $(this).attr('data-task_id');
            Dialog.Show('Veuillez confirmer', 'Archiver tâche', AJAX_HANDLER + '/delete-archive-task?id='+task_id+'&op=archive');
        });
    },
    <?php endif; ?>
}
</script>