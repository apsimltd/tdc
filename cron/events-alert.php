<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php
if (!User::isLoggedIn()) {
    $response = array(
        'status' => 'NOK',           
    ); 
    echo json_encode($response);
    exit();
}

// cron job to check if a user has a rdvp
// cron job will be run every 1 min
$event = Event::getRdvp($me['id']);

if (!empty($event)) {
    
    if (isset($_SESSION['opsearch_rdvp_1']) && $_SESSION['opsearch_rdvp_1'] == $event['id'] 
        && isset($_SESSION['opsearch_rdvp_2']) && $_SESSION['opsearch_rdvp_2'] == $event['id']) {
        unset($_SESSION['opsearch_rdvp_1']);
        unset($_SESSION['opsearch_rdvp_2']);
    }
    
    $difference = strtotime($event['start_date']) - time();
    
    // display first alert at range 30 min - 20 min
    if ($difference > (20*60) && $difference < (30*60) && !isset($_SESSION['opsearch_rdvp_1'])) {
        $_SESSION['opsearch_rdvp_1'] = $event['id'];
        
        // send an email to the user
        $mail_body = 'Bonjour ' . mb_ucfirst($me['firstName']) . ' ' . mb_strtoupper($me['lastName'])
            . '<br/>Vous avez un RDVP "<strong>' . $event['title'] . '</strong>"  aujourd\'hui à ' . dateToFr(strtotime($event['start_date']));

        Mail::sendMail('Rappel RDVP ' . $event['title'], $me['email'], $mail_body);
        
        // alert the user
        $response = array(
            'status' => 'OK',
            'msg' => 'Vous avez un RDVP "<strong>'. $event['title'] .'</strong>"  aujourd\'hui à ' . dateToFr(strtotime($event['start_date']), true),   
        );
        
        echo json_encode($response);
        exit();
    }
    
    // display second alert at range 15 min - 5 min 
    if ($difference > (5*60) && $difference < (15*60) && isset($_SESSION['opsearch_rdvp_1']) && !isset($_SESSION['opsearch_rdvp_2'])) {
        $_SESSION['opsearch_rdvp_2'] = $event['id'];
        
        // send an email to the user
        $mail_body = 'Bonjour ' . mb_ucfirst($me['firstName']) . ' ' . mb_strtoupper($me['lastName'])
            . '<br/>Vous avez un RDVP "<strong>' . $event['title'] . '</strong>"  aujourd\'hui à ' . dateToFr(strtotime($event['start_date']));

        Mail::sendMail('Rappel RDVP ' . $event['title'], $me['email'], $mail_body);
        
        // alert the user
        $response = array(
            'status' => 'OK',
            'msg' => 'Vous avez un RDVP "<strong>'. $event['title'] .'</strong>"  aujourd\'hui à ' . dateToFr(strtotime($event['start_date']), true),   
        );
        echo json_encode($response);
        exit();
    }
    
} else {
    
    $response = array(
        'status' => 'NOK',           
    ); 
    echo json_encode($response);
    exit();

}
?>