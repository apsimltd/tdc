<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>

<?php
// run every hr

$missions_candidats = Watchdog::getGroupedMissionCandidat();

foreach($missions_candidats as $mission_candidat):
    
    $rows = Watchdog::getMissionCandidat($mission_candidat['mission_id'], $mission_candidat['candidat_id']);

    // status candidat in tdc
    // 226 => OUT
    // 227 => BU-
    // 228 => BU+
    // 229 => IN
    // 251 => ARC (Attente Retour Client)
    // 252 => ARCA
    // we must calculate time in seconds for only candidat remains in status 251 => ARC
    
    // number of rows 
    $num_rows = count($rows);
    
    // if only one record
    if ($num_rows == 1) {
        
        if ($rows[0]['status_after_id'] == 251) {
            $time = time() - strtotime($rows[0]['created']);
            Watchdog::updateTimerARC($rows[0]['id'], $time);
        }
    
    // if several records    
    } else {
        
        $counter = 0;
        
        foreach($rows as $row):
            
            if ($counter == 0) {
                $previous_row_index = null;
            } else {
                $previous_row_index = $counter-1;
            }
            
            if ($row['status_after_id'] == 251) {
                
                // check if there is a next row
                if (isset($rows[$counter+1])) {
                    
                    if ($rows[$counter+1]['status_after_id'] <> 251) {
                        
                        $time = strtotime($rows[$counter+1]['created']) - strtotime($row['created']);
                        Watchdog::updateTimerARC($row['id'], $time);
                        
                    }
                        
                    
                } else { // if no next row
                    
                    $time = time() - strtotime($row['created']);
                    Watchdog::updateTimerARC($row['id'], $time);
                    
                }
                
            }
            
            $counter++;
            
        endforeach;
        
    }
    

endforeach;

// mail('faardeen.madarbokas@gmail.com', 'Cron Calcul Time ARC started', 'Cron Calcul Time ARC started');

?>