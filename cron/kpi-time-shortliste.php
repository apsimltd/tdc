<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php
$sql = "SELECT * FROM kpi_missions_shortlist WHERE shortliste = 1 AND shortliste_added IS NOT NULL AND t_shortliste_from_start_mission = 0";

$results = $con->fetchAll($sql);

foreach($results as $row) :
    
    if ($row['shortliste_added'] > $row['mission_date_debut']) {
        $time = strtotime($row['shortliste_added']) - strtotime($row['mission_date_debut']);
        $con->update('kpi_missions_shortlist', array('shortliste_in_time' => 1, 't_shortliste_from_start_mission' => $time), array('id' => $row['id']));
        echo "\n<br>". $row['id'] . " - Data Shortliste updated OK";
    }
    
endforeach;