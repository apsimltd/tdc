<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>
<?php
// OP prestataire id = 1 
$sql = "SELECT * "
    . "FROM kpi_clients "
    . "WHERE prestataire_id = 1 AND (t_reactivite_client = 0 OR t_presentation_rencontre = 0 OR t_rencontre_consultant_rencontre_client = 0 OR date_modified_status = 0) "
    . "ORDER BY id ASC";

$results = $con->fetchAll($sql);

foreach($results as $row) :
    
    $data = array();
    
    // calculate time
    if (strtotime($row['date_retour_apres_rencontre']) <> "" && strtotime($row['date_presentation']) <> "") {
        $data['t_reactivite_client'] = strtotime($row['date_retour_apres_rencontre']) - strtotime($row['date_presentation']);
    } else {
        $data['t_reactivite_client'] = 0;
    }

    if (strtotime($row['date_rencontre_client']) <> "" && strtotime($row['date_presentation']) <> "") {
        $data['t_presentation_rencontre'] = strtotime($row['date_rencontre_client']) - strtotime($row['date_presentation']);
    } else {
        $data['t_presentation_rencontre'] = 0;
    }

    if (strtotime($row['date_rencontre_client']) <> "" && strtotime($row['date_rencontre_consultant']) <> "") {
        $data['t_rencontre_consultant_rencontre_client'] = strtotime($row['date_rencontre_client']) - strtotime($row['date_rencontre_consultant']);
    } else {
        $data['t_rencontre_consultant_rencontre_client'] = 0;
    }
    
    if (!empty($data)) {
        
        $data['modified'] = dateToDb();
        $data['date_modified_status'] = 0;
        
        $con->update('kpi_clients', $data, array('id' => $row['id'])); 
        echo "\n<br>". $row['id'] . " - Data OP updated OK";
        
    }

endforeach;

// HF prestataire id = 2 
$sql = "SELECT * "
    . "FROM kpi_clients "
    . "WHERE prestataire_id = 2 AND (t_reactivite_client = 0 OR t_presentation_rencontre = 0 OR date_modified_status = 0) "
    . "ORDER BY id ASC";

$results = $con->fetchAll($sql);

foreach($results as $row) :
    
    $data = array();
    
    // calculate time
    if (strtotime($row['date_retour_apres_rencontre']) <> "" && strtotime($row['date_presentation']) <> "") {
        $data['t_reactivite_client'] = strtotime($row['date_retour_apres_rencontre']) - strtotime($row['date_presentation']);
    } else {
        $data['t_reactivite_client'] = 0;
    }

    if (strtotime($row['date_rencontre_client']) <> "" && strtotime($row['date_presentation']) <> "") {
        $data['t_presentation_rencontre'] = strtotime($row['date_rencontre_client']) - strtotime($row['date_presentation']);
    } else {
        $data['t_presentation_rencontre'] = 0;
    }
    
    if (!empty($data)) {
        
        $data['modified'] = dateToDb();
        $data['date_modified_status'] = 0;
        
        $con->update('kpi_clients', $data, array('id' => $row['id'])); 
        echo "\n<br>". $row['id'] . " - Data HF updated OK";
        
    }

endforeach;