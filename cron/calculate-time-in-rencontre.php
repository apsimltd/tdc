<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>

<?php
// check last row added from watchdog
$last_record_id = Watchdog::getLastRecordTimerIn();

if ($last_record_id <> "") {
    $results = Watchdog::getTDCRencontreIN($last_record_id);
} else {
    $results = Watchdog::getTDCRencontreIN();
}

foreach($results as $row):
    
    if ($row['date_rencontre_consultant'] == 0) {
        $row['date_rencontre_consultant'] = null;
    } else {
        $row['date_rencontre_consultant'] = dateToDb($row['date_rencontre_consultant']);
    }
    
    if ($row['date_rencontre_client'] == 0) {
        $row['date_rencontre_client'] = null;
    } else {
        $row['date_rencontre_client'] = dateToDb($row['date_rencontre_client']);
    }
    
    Watchdog::timer_in($row);
    
endforeach;



$rows = Watchdog::getList_KPI_IN_TIME();

foreach($rows as $row):
    
    /**
     *  Time between IN to a date consultant => OPSEARCH
        Time between IN to a date rencontre client => HF
        Time between date consultant to date rencontre client => OPSEARCH
     * 
        prestataire_id => 1 = OPSEARCH, 2 => HF
     * 
     */
        
    $time_in = strtotime($row['created_in']);

    if ($row['prestataire_id'] == 2) { // HF
        
        if ($row['date_rencontre_client'] <> "" && $row['date_rencontre_client_added'] <> "") {

            $row['t_in_date_rencontre_client'] = strtotime($row['date_rencontre_client_added']) - $time_in;

        }
        
        Watchdog::updateTimerIn($row);
        
    } elseif ($row['prestataire_id'] == 1) { // OPSEARCH
        
        if ($row['date_rencontre_consultant'] <> "" && $row['date_rencontre_consultant_added'] <> "") {

            $row['t_in_date_consultant'] = strtotime($row['date_rencontre_consultant_added']) - $time_in;

        }
        
        if ($row['date_rencontre_consultant'] <> "" && $row['date_rencontre_consultant_added'] <> "" && $row['date_rencontre_client'] <> "" && $row['date_rencontre_client_added'] <> "") {

            $row['t_in_date_consultant_date_rencontre_client'] = strtotime($row['date_rencontre_consultant_added']) - strtotime($row['date_rencontre_client_added']);

        }
        
        Watchdog::updateTimerIn($row);
        
    }
    
    // NEW KPI
    // 1. temps reactivite du client une fois les candidats presentes = date rencontre client - date de presentation
    // 2.1 temps reactivite du client vs les candidats rencontres = date rencontre client - date presentation
    // 2.2 temps reactivite du client vs les candidats rencontres = date rencontre client - date rencontre consultant
    // 2.3 temps reactivite du client vs les candidats rencontres = date retour apres rencontre - date date presentation
    // note must take care of prestataire as up
    
    


endforeach;

// mail('faardeen.madarbokas@gmail.com', 'Cron Calcul Time In Rencontre Client started', 'Cron Calcul Time In Rencontre Client started');

?>