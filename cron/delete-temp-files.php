<?php require_once __DIR__ . '/../conf/bootstrap.inc'; ?>

<?php
/**
 * https://www.codexworld.com/delete-all-files-from-folder-using-php/
 * http://thisinterestsme.com/php-delete-all-files-from-a-folder/
 */

echo PDF_URL;

$files = glob(PDF_URL . '/*');
 
//Loop through the file list.
foreach($files as $file){
    //Make sure that this is a file and not a directory.
    if(is_file($file)){
        //Use the unlink function to delete the file.
        unlink($file);
    }
}

// delete error log file from folder cron
// file name is error_log
// file folder is /home/bastien2018apsim/public_html/cron/error_log
$cron_file = BASE_PATH . '/cron/error_log';
if(is_file($cron_file)){
    //Use the unlink function to delete the file.
    unlink($cron_file);
    echo 'CRON file deleted';
}

// send an email to faardeen.madarbokas@gmail.com when it executes
//if (!isDev()) {
//    mail('faardeen.madarbokas@gmail.com', 'CRON DELETE FILE OPSEARCH SUCCESS', 'CRON DELETE FILE OPSEARCH SUCCESS');
//}