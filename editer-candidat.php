<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Éditer un candidat" ?>
<?php $page = "add-candidate"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('edit_candidat', 'create_candidat')) || get('id') == "") header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>
<?php $candidat = Candidat::getCandidatById(get('id')); ?>
<?php if (empty($candidat)){ header("Location: " . BASE_URL . "/candidats"); } ?>
<?php $langues = Candidat::getLangues(get('id')); ?>
<?php /// $comments = Candidat::getComments(get('id'));  ?>
<?php $comments = Candidat::getCommentsNew(get('id'));  ?>
<?php $motivations = Candidat::getMotivation(get('id')) ?>
<?php $links = Candidat::getLinks(get('id')); ?>
<?php // $documents = Candidat::getDocuments(get('id')); ?>
<?php $documents = json_decode($candidat['documents'], true); ?>
<?php // debug($documents) ?>
	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base" class="AddCandidat">

        <div id="contentWrap">					
            
			<section class="title block">

				<h1 class="pull-left"><span class="glyphicon glyphicon-user"></span>Éditer candidat</h1>
				
                <span style="line-height: 24px; font-size: 12px; font-style: italic; display: inline-block; padding-left: 30px; color: #7cbd5a;">
                    <strong>Date de création :</strong> <?php echo dateToFr($candidat['created'], true) ?>
                </span>
                
                <span style="line-height: 24px; font-size: 12px; font-style: italic; display: inline-block; padding-left: 30px; color: #7cbd5a;">
                    <strong>Date de modification :</strong> <?php echo dateToFr($candidat['modified'], true) ?>
                </span>
                
				<div class="btn-box pull-right">                   
                    
                    <?php if (get('tdc_id') && !get('tdc_line_id')): ?>
                    <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo get('tdc_id') ?>">
                        <button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page TDC"><span class="glyphicon glyphicon-menu-left"></span></button>
                    </a>
                    <?php elseif (get('tdc_id') && get('tdc_line_id')): ?>
                    <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo get('tdc_id') ?>&tdc_id=<?php echo get('tdc_line_id') ?>">
                        <button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page TDC"><span class="glyphicon glyphicon-menu-left"></span></button>
                    </a>
                    <?php else: ?>
                    <button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Retour à la page précédente" onClick="window.history.back();"><span class="glyphicon glyphicon-menu-left"></span></button>
                    <?php endif; ?>
                    
                    <button type="button" data-candidat_id="<?php echo $candidat['id'] ?>" class="generate_pdf_candidat pull-right btn btn-icon btn-warning tooltips" title="Générer un fichier PDF"><i class="fa fa-file-pdf"></i></button>
                    <?php if (User::can('create_candidat')): ?>
                    <a href="ajoute-candidat"><button type="button" class="btn btn-icon btn-primary pull-right tooltips" title="Ajouter un nouveau Candidat"><span class="glyphicon glyphicon-plus"></span></button></a>
                    <?php endif; ?>
                    <?php if (User::can('tdc') && get('tdc_id') && get('tdc_id') != "" && !TDC::isCandidatInTDC($candidat['id'], get('tdc_id'))) : ?>                    
                    <a href="<?php echo AJAX_HANDLER ?>/add-candidat-tdc?candidat_id=<?php echo $candidat['id'] ?>&mission_id=<?php echo get('tdc_id') ?>">
                        <button type="button" class="btn btn-icon btn-info pull-right tooltips" title="Ajouter Candidat au TDC Ref <?php echo get('tdc_id') ?>">
                            <span class="glyphicon glyphicon-screenshot"></span>
                        </button>
                    </a>
                    <?php endif; ?>
				</div>
				
			</section>
			
			<form class="frm_frm frm_ajax" name="frm_edit_candidat" id="frm_edit_candidat" data-url="<?php echo AJAX_HANDLER ?>/edit-candidat" data-type="json">
			
                <input type="hidden" name="id" value="<?php echo $candidat['id'] ?>">
                
                <?php if (get('tdc_id') && get('tdc_id') != ""): ?>
                <input type="hidden" name="mission_id" value="<?php echo get('tdc_id') ?>">
                <?php endif; ?>
                
                <?php if (get('tdc_line_id') && get('tdc_line_id') != ""): ?>
                <input type="hidden" name="tdc_id" value="<?php echo get('tdc_line_id') ?>">
                <?php endif; ?>
                
                <div class="row">

                    <div class="col col-12">
                        <div class="block nopadding">

                            <div class="block-body">

                                <div class="row">

                                    <div class="col col-4">
                                        <div class="block nopadding">

                                            <div class="block-body">
                                                
                                                <fieldset>
                                                    <label>Civilité</label>
                                                    <div class="labf3-radio-square-wrapper">
                                                        <?php $controls = Control::getControlByType(7, 1); $counter = 1; ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <div class="labf3-radio-square" data-value="<?php echo $control['id'] ?>">
                                                            <span class="ticks <?php if ($candidat['control_civilite_id'] == $control['id']): ?>active<?php endif; ?>"></span>
                                                            <span class="libele"><?php echo $control['name'] ?></span>
                                                        </div><!-- /labf3-radio-square -->
                                                        <?php $counter++; ?>
                                                        <?php endforeach; ?>
                                                        <input type="hidden" name="control_civilite_id" class="frm_text frm_labf3_radio_square" data-validation="val_blank" <?php if ($candidat['control_civilite_id'] > 0): ?>value="<?php echo $candidat['control_civilite_id'] ?>"<?php endif; ?>>
                                                    </div><!-- /labf3-radio-square-wrapper -->   
                                                </fieldset>
                                                <fieldset>
                                                    <label>Localisation</label>
                                                    <select class="frm_chosen <?php if ($candidat['control_localisation_id'] > 0): ?>ok<?php endif; ?>" name="control_localisation_id" data-validation="val_blank">
                                                        <option value="">Choisir Localisation</option>
                                                        <?php $controls = Control::getControlByType(6, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>" <?php if ($candidat['control_localisation_id'] == $control['id']): ?>selected="selected"<?php endif; ?>>
                                                            <?php echo $control['name'] ?>
                                                        </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </fieldset>
                                                <fieldset>
                                                    <label>Nom</label>
                                                    <input class="frm_text ok caps" name="nom" placeholder="Nom" value="<?php echo $candidat['nom'] ?>" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>
                                                <fieldset>
                                                    <label>Prénom</label>
                                                    <input class="frm_text ok" name="prenom" placeholder="Prénom" value="<?php echo $candidat['prenom'] ?>" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>
                                                <fieldset>
                                                    <label>Téléphone standard</label>
                                                    <input class="frm_text tel_fr <?php if ($candidat['tel_standard'] != ""): ?>ok<?php endif; ?>" name="tel_standard" value="<?php echo $candidat['tel_standard'] ?>" placeholder="Téléphone standard" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>
                                                <fieldset>
                                                    <label>Téléphone ligne directe</label>
                                                    <input class="frm_text <?php if ($candidat['tel_ligne_direct'] != ""): ?>ok<?php endif; ?>" name="tel_ligne_direct" value="<?php echo $candidat['tel_ligne_direct'] ?>" placeholder="Téléphone ligne directe" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>
                                                <fieldset>
                                                    <label>Téléphone mobile pro</label>
                                                    <input class="frm_text <?php if ($candidat['tel_pro'] != ""): ?>ok<?php endif; ?>" name="tel_pro" value="<?php echo $candidat['tel_pro'] ?>" placeholder="Téléphone mobile professionnel" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>
                                                <fieldset>
                                                    <label>Téléphone mobile perso</label>
                                                    <input class="frm_text <?php if ($candidat['tel_mobile_perso'] != ""): ?>ok<?php endif; ?>" name="tel_mobile_perso" value="<?php echo $candidat['tel_mobile_perso'] ?>" placeholder="Téléphone mobile perso" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>
                                                <fieldset>
                                                    <label>Téléphone domicile</label>
                                                    <input class="frm_text <?php if ($candidat['tel_domicile'] != ""): ?>ok<?php endif; ?>" name="tel_domicile" value="<?php echo $candidat['tel_domicile'] ?>" placeholder="Téléphone domicile" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>
                                                    
                                                <fieldset>
                                                    <label>Email 1</label>
                                                    <input class="frm_text <?php if ($candidat['email'] != ""): ?>ok<?php endif; ?>" name="email" value="<?php echo $candidat['email'] ?>" placeholder="Email 1" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>
                                                
                                                <fieldset>
													<label>Email 2</label>
													<input class="frm_text <?php if ($candidat['email2'] != ""): ?>ok<?php endif; ?>" name="email2" value="<?php echo $candidat['email2'] ?>" placeholder="Email 2" type="text" autocomplete="off" data-validation="val_blank">
												</fieldset>
                                                
                                                <fieldset>
                                                    <label>Date de naissance</label>
                                                    <input class="frm_text dateOfBirth <?php if ($candidat['dateOfBirth'] != 0): ?>ok<?php endif; ?>" value="<?php if ($candidat['dateOfBirth'] != 0): echo dateToFr($candidat['dateOfBirth']); endif; ?>" name="dateOfBirth" placeholder="Date de naissance" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>
                                                
                                                
                                            </div><!-- / block-body -->

                                        </div>
                                    </div><!-- /col -->

                                    <div class="col col-4">
                                        <div class="block nopadding">

                                            <div class="block-body">

                                                <fieldset>
                                                    <label>Société actuelle</label>
                                                    <input id="societe_autocomplete" class="frm_text caps checkSocieteBlacklisteCandidat <?php if ($candidat['societe_actuelle'] != ""): ?>ok<?php endif; ?>" name="societe_actuelle" value="<?php echo $candidat['societe_actuelle'] ?>" placeholder="Société actuelle" type="text" autocomplete="off" data-validation="val_blank">
                                                    <input type="hidden" name="societe_actuelle_old" value="<?php echo $candidat['societe_actuelle'] ?>">
                                                    <input type="hidden" name="company_id" id="societe_autocomplete_id">
                                                    <?php $style = 'style="display:none;"'; ?>
                                                    <?php if ($candidat['societe_actuelle'] != "" && Client::getClientCandidatBlacklist($candidat['societe_actuelle'])): ?>
                                                        <?php $style = 'style="display:block;"'; ?>
                                                    <?php endif; ?>
                                                    <div class="msgBlackliste" <?php echo $style ?>>
                                                        <div class="clearboth notif warning pull-right" style="background-color:#ff9800; color: #fff; width: 60%;line-height: 24px; padding: 0px 10px;"><strong>Attention :</strong> La Société est blacklistée</div>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="horizontal_checkboxes">
                                                    <label>&nbsp;</label>
													<div class="radio_wrap">
														<div class="radio-item">
															<label class="long" for="is_not_in_this_company">Candidat plus en poste</label>
                                                            <input type="checkbox" name="is_not_in_this_company"  <?php if ($candidat['is_not_in_this_company'] == 1): ?>checked="checked"<?php endif; ?> class="frm_checkbox" id="is_not_in_this_company">
														</div>
													</div>													
												</fieldset>
                                                <fieldset>
                                                    <label>Poste</label>
                                                    <input class="frm_text <?php if ($candidat['poste'] != ""): ?>ok<?php endif; ?>" name="poste" value="<?php echo $candidat['poste'] ?>" placeholder="Poste" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>
                                                <fieldset>
                                                    <label>Secteur</label>
                                                    <select class="frm_chosen <?php if ($candidat['control_secteur_id'] > 0): ?>ok<?php endif; ?>" name="control_secteur_id" data-validation="val_blank">
                                                        <option value="">Choisir Secteur</option>
                                                        <?php $controls = Control::getControlByType(2, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>" <?php if ($candidat['control_secteur_id'] == $control['id']): ?>selected="selected"<?php endif; ?>>
                                                            <?php echo $control['name'] ?>
                                                        </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </fieldset>
                                                <fieldset>
                                                    <label>Fonction</label>
                                                    <select class="frm_chosen <?php if ($candidat['control_fonction_id'] > 0): ?>ok<?php endif; ?>" name="control_fonction_id" data-validation="val_blank">
                                                        <option value="">Choisir Fonction</option>
                                                        <?php $controls = Control::getControlByType(1, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>" <?php if ($candidat['control_fonction_id'] == $control['id']): ?>selected="selected"<?php endif; ?>>
                                                            <?php echo $control['name'] ?>
                                                        </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </fieldset>
                                                <fieldset>
                                                    <label>En Package salarial (K€)</label>
                                                    <input class="frm_text <?php if ($candidat['remuneration'] != 0): ?>ok<?php endif; ?>" name="remuneration" value="<?php if ($candidat['remuneration'] != 0): echo $candidat['remuneration']; endif; ?>" placeholder="En Package salarial (K€)" type="text" autocomplete="off" data-validation="val_float">
                                                </fieldset>
                                                <fieldset>
                                                    <label>Détails primes / variable</label>
                                                    <input class="frm_text <?php if ($candidat['detail_remuneration'] != ""): ?>ok<?php endif; ?>" name="detail_remuneration" value="<?php echo $candidat['detail_remuneration'] ?>" placeholder="Détails primes / variable" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>									
                                                
<!--                                                <fieldset>
													<label>Salaire Package (K€)</label>
													<input class="frm_text <?php ///if ($candidat['salaire_package'] != ""): ?>ok<?php ///endif; ?>" name="salaire_package" placeholder="Salaire Package (K€)" value="<?php ///echo $candidat['salaire_package'] ?>" type="text" autocomplete="off" data-validation="val_float">
												</fieldset>-->
                                                
                                                <fieldset>
													<label>Salaire Fixe (K€)</label>
													<input class="frm_text <?php if ($candidat['salaire_fixe'] != ""): ?>ok<?php endif; ?>" name="salaire_fixe" placeholder="Salaire Fixe (K€)" value="<?php echo $candidat['salaire_fixe'] ?>" type="text" autocomplete="off" data-validation="val_float">
												</fieldset>
                                                
<!--                                                <fieldset>
													<label>Prime ou Variable</label>
													<input class="frm_text <?php ///if ($candidat['sur_combien_de_mois'] != ""): ?>ok<?php ///endif; ?>" name="sur_combien_de_mois" placeholder="Prime ou Variable" value="<?php ///echo $candidat['sur_combien_de_mois'] ?>" type="text" autocomplete="off" data-validation="val_num">
												</fieldset>-->
                                                
                                                <fieldset>
													<label>Avantages en nature</label>
                                                    <textarea name="avantages" class="frm_textarea <?php if ($candidat['avantages'] != ""): ?>ok<?php endif; ?>" placeholder="Avantages en nature" data-validation="val_blank" maxlength="300"><?php echo $candidat['avantages'] ?></textarea>                                
												</fieldset>
                                                
                                                <fieldset>
													<label>Rémunération Souhaitée</label>
                                                    <textarea name="remuneration_souhaitee" class="frm_textarea <?php if ($candidat['remuneration_souhaitee'] != ""): ?>ok<?php endif; ?>" placeholder="Rémunération Souhaitée" data-validation="val_blank" maxlength="300"><?php echo $candidat['remuneration_souhaitee'] ?></textarea>                                
												</fieldset>
                                                
                                            </div><!-- / block-body -->

                                        </div>
                                    </div><!-- /col -->

                                    <div class="col col-4">
                                        <div class="block nopadding">

                                            <div class="block-body">
                                                
                                                <fieldset class="horizontal_checkboxes">
                                                    <div class="radio_wrap">
                                                        <div class="radio-item">
                                                            <label class="long" for="cvFourni">CV fourni</label>
                                                            <input type="checkbox" name="cv_fourni" class="frm_checkbox" <?php if ($candidat['cv_fourni'] == 1): ?>checked="checked"<?php endif; ?> id="cvFourni">
                                                        </div>
                                                        <div class="radio-item">
                                                            <label class="long" for="tresBonCandidat">Très bon candidat</label>
                                                            <input type="checkbox" name="tres_bon_candidat" class="frm_checkbox" <?php if ($candidat['tres_bon_candidat'] == 1): ?>checked="checked"<?php endif; ?> id="tresBonCandidat">
                                                        </div>
                                                    </div>													
                                                </fieldset>
                                                
                                                <fieldset>
                                                    <label>Niveau</label>
                                                    <select class="frm_chosen <?php if ($candidat['control_niveauFormation_id'] > 0): ?>ok<?php endif; ?>" name="control_niveauFormation_id" data-validation="val_blank">
                                                        <option value="">Choisir Niveau</option>
                                                        <?php $controls = Control::getControlByType(4, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>" <?php if ($candidat['control_niveauFormation_id'] == $control['id']): ?>selected="selected"<?php endif; ?>>
                                                            <?php echo $control['name'] ?>
                                                        </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </fieldset>

                                                <fieldset>
                                                    <label>Date d'obtention du diplôme</label>
                                                    <input class="frm_text dateOfBirth <?php if ($candidat['dateDiplome'] != 0): ?>ok<?php endif; ?>" name="dateDiplome" value="<?php if ($candidat['dateDiplome'] != 0): echo dateToFr($candidat['dateDiplome']); endif; ?>" placeholder="Date d'obtention du diplôme" type="text" autocomplete="off" data-validation="val_blank">
                                                </fieldset>

                                                <fieldset>
                                                    <label>Diplôme / spécialisation</label>
                                                    <textarea name="diplome_specialisation" class="frm_textarea <?php if ($candidat['diplome_specialisation'] != ""): ?>ok<?php endif; ?>" placeholder="Diplôme / spécialisation" data-validation="val_blank" style="height:114px;"><?php echo $candidat['diplome_specialisation'] ?></textarea>                                
                                                </fieldset>

                                                <fieldset>
                                                    <label>Source</label>
                                                    <select class="frm_chosen <?php if ($candidat['control_source_id'] > 0): ?>ok<?php endif; ?>" name="control_source_id" data-validation="val_blank">
                                                        <option value="">Choisir Source</option>
                                                        <?php $controls = Control::getControlByType(3, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>" <?php if ($candidat['control_source_id'] == $control['id']): ?>selected="selected"<?php endif; ?>>
                                                            <?php echo $control['name'] ?>
                                                        </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </fieldset>

                                                <fieldset>
                                                    <label>Langues</label>
                                                    <select class="frm_chosen" name="langues[]" data-validation="val_blank" data-placeholder="Choisissez une ou plusieurs langues" multiple="multiple">
                                                        <?php $controls = Control::getControlByType(5, 1) ?>
                                                        <?php foreach($controls as $control): ?>
                                                        <option value="<?php echo $control['id'] ?>" <?php if (in_array($control['id'], $langues)): ?>selected="selected"<?php endif; ?>>
                                                            <?php echo $control['name'] ?>
                                                        </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </fieldset>
                                                <?php if (User::can('rgpd_candidat')): ?>
                                                <fieldset class="mission-checkbox">
                                                    <input type="checkbox" class="frm_checkbox" name="gdpr_status" id="gdpr_status" <?php if ($candidat['gdpr_status'] == 1): ?>checked="checked"<?php endif; ?>>
                                                    <label class="pointer" for="gdpr_status" style="color: #FF0000">Candidat Inactif (RGPD)</label>
                                                </fieldset>
                                                <?php endif; ?>
                                                <fieldset>
                                                    <button type="button" id="editerCandidatSubmit" class="btn btn-primary frm_submit frm_notif pull-right" data-form="10"><span class="glyphicon glyphicon-pencil"></span> Éditer Candidat</button>
                                                </fieldset>

                                            </div><!-- / block-body -->

                                        </div>
                                    </div><!-- /col -->

                                </div><!-- / row -->	

                            </div><!-- / block-body -->

                        </div>
                    </div><!-- /col -->

                </div><!-- / row -->
			</form>
			
			<div class="row edit_candidat_nano">
				
				<div class="col col-12">
					<div class="block nopadding">
												
						<div class="block-body">
							
							<div class="row">
			
								<div class="col col-4">
									<div class="block nopadding">
										
										<div class="block-head with-border">
											<header><span class="glyphicon glyphicon-comment"></span>PARCOURS PROFESSIONNEL</header>
                                            <?php if (get('tdc_id') && get('tdc_id') <> "" && get('tdc_line_id') && get('tdc_line_id') <> ""): ?>
                                            <div class="block-head-btns pull-right">
                                                <button type="button" data-candidat_id="<?php echo $candidat['id'] ?>" data-mission_id="<?php echo get('tdc_id') ?>" class="comment_zoom btn btn-icon btn-primary pull-right tooltips" title="Voir en grand"><span class="glyphicon glyphicon-zoom-in"></span></button>
                                            </div>
                                            <?php endif; ?>
										</div>
																
										<div class="block-body">
                                            <?php if (get('tdc_id') && get('tdc_id') <> "" && get('tdc_line_id') && get('tdc_line_id') <> ""): ?>
											<form class="frm_frm frm_ajax" name="frm_edit_candidat_add_comment" id="frm_edit_candidat_add_comment" data-url="<?php echo AJAX_HANDLER ?>/add-candidat-comment" data-type="json">
                                                <input type="hidden" name="candidat_id" value="<?php echo $candidat['id'] ?>">
                                                <input type="hidden" name="mission_id" value="<?php echo get('tdc_id') ?>">
												<fieldset class="full">
													<textarea name="commentaire" class="frm_textarea must add_new_comment" placeholder="Ajouter un nouveau commentaire" data-validation="val_blank"></textarea>                                
												</fieldset>
												
												<fieldset>
													<button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter commentaire</button>
												</fieldset>
												
											</form>
                                            <?php endif; ?>
											
                                            <div class="nano" style="height: <?php if (get('tdc_id') && get('tdc_id') <> "" && get('tdc_line_id') && get('tdc_line_id') <> ""): ?>300px;<?php else: ?>450px;<?php endif; ?>">
                            					<div class="nano-content">
												
													<div class="historiques_comment_wrap">
													
														<?php if (!empty($comments)): ?>
                                                        <?php // debug($comments) ?>
                                                        <div class="task-threads-wrap clearfix">

                                                            <?php foreach ($comments as $comment): ?>
                                                            <div class="task-thread clearfix">

                                                                <p><?php echo nl2br($comment['comment']) ?></p>

                                                                <div class="publish-info">
                                                                    Créé par <strong><?php echo mb_ucfirst($comment['firstName']) . ' ' . mb_strtoupper($comment['lastName']) ?></strong> le <?php echo dateToFr($comment['created'], true) ?>
                                                                    <span class="glyphicon glyphicon-edit edit_comment tooltips" title="Éditer" data-comment_id="<?php echo $comment['id'] ?>" data-candidat_id="<?php echo $candidat['id'] ?>" data-mission_id="<?php echo ($comment['mission_id'] == 0) ? get('tdc_id') : $comment['mission_id']; ?>"></span>
                                                                    <span class="glyphicon glyphicon-trash delete_comment tooltips" title="Supprimer" data-comment_id="<?php echo $comment['id'] ?>" data-candidat_id="<?php echo $candidat['id'] ?>"></span>
                                                                </div>
                                                                
                                                                <?php if ($comment['mission_id'] <> ""): ?>
                                                                <div class="publish-info">
                                                                    Mission : <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $comment['mission_id'] ?>" target="_blank"><span class="glyphicon glyphicon-screenshot"></span> <?php echo $comment['mission'] . ' - ' . $comment['client'] ?></a>
                                                                </div>
                                                                <?php endif; ?>

                                                            </div>
                                                            <?php endforeach; ?>

                                                        </div><!--/ task-threads-wrap -->
                                                        <?php endif; ?>
														
													</div><!-- /historiques_comment_wrap -->
													
												</div><!-- /nano-content-->
											</div><!-- /nano -->

										</div><!-- / block-body -->
										
                                        <?php ######### start of motivation ?>
                                        
                                        <div class="block-head with-border">
											<header><span class="glyphicon glyphicon-comment"></span>MOTIVATION</header>
										</div>
                                        
                                        <div class="block-body">
                                            <?php if (get('tdc_id') && get('tdc_id') <> "" && get('tdc_line_id') && get('tdc_line_id') <> ""): ?>
											<form class="frm_frm frm_ajax" name="frm_edit_candidat_add_motivation" id="frm_edit_candidat_add_motivation" data-url="<?php echo AJAX_HANDLER ?>/add-candidat-motivation" data-type="json">
                                                <input type="hidden" name="candidat_id" value="<?php echo $candidat['id'] ?>">
                                                <input type="hidden" name="mission_id" value="<?php echo get('tdc_id') ?>">
												<fieldset class="full">
													<textarea name="motivation" class="frm_textarea must add_new_motivation" placeholder="Ajouter un nouveau motivation" data-validation="val_blank"></textarea>                                
												</fieldset>
												
												<fieldset>
													<button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter motivation</button>
												</fieldset>
												
											</form>
                                            <?php endif; ?>
											
                                            <div class="nano" style="height: <?php if (get('tdc_id') && get('tdc_id') <> "" && get('tdc_line_id') && get('tdc_line_id') <> ""): ?>300px;<?php else: ?>450px;<?php endif; ?>">
                            					<div class="nano-content">
												
													<div class="historiques_motivation_wrap">
													
														<?php if (!empty($motivations)): ?>
                                                        <?php // debug($comments) ?>
                                                        <div class="task-threads-wrap clearfix">

                                                            <?php foreach ($motivations as $motivation): ?>
                                                            <div class="task-thread clearfix">

                                                                <p><?php echo nl2br($motivation['motivation']) ?></p>

                                                                <div class="publish-info">
                                                                    Créé par <strong><?php echo mb_ucfirst($motivation['firstName']) . ' ' . mb_strtoupper($motivation['lastName']) ?></strong> le <?php echo $motivation['created'] ?>
                                                                    <span class="glyphicon glyphicon-edit edit_motivation tooltips" title="Éditer" data-motivation_id="<?php echo $motivation['id'] ?>" data-candidat_id="<?php echo $candidat['id'] ?>" data-mission_id="<?php echo ($motivation['mission_id'] == 0) ? get('tdc_id') : $motivation['mission_id']; ?>"></span>
                                                                    <span class="glyphicon glyphicon-trash delete_motivation tooltips" title="Supprimer" data-motivation_id="<?php echo $motivation['id'] ?>" data-candidat_id="<?php echo $candidat['id'] ?>"></span>
                                                                </div>
                                                                
                                                                <?php if ($motivation['mission_id'] <> ""): ?>
                                                                <div class="publish-info">
                                                                    Mission : <a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $motivation['mission_id'] ?>" target="_blank"><span class="glyphicon glyphicon-screenshot"></span> <?php echo $motivation['mission'] . ' - ' . $motivation['client'] ?></a>
                                                                </div>
                                                                <?php endif; ?>

                                                            </div>
                                                            <?php endforeach; ?>

                                                        </div><!--/ task-threads-wrap -->
                                                        <?php endif; ?>
														
													</div><!-- /historiques_comment_wrap -->
													
												</div><!-- /nano-content-->
											</div><!-- /nano -->

										</div><!-- / block-body -->
                                        
                                        <?php #### end of motivation ?>
                                        
									</div><!-- / block -->
								</div><!-- /col -->
								
								<div class="col col-4">
									<div class="block nopadding">
															
										<div class="block-body nopadding">
                                            
                                            <div class="block-head with-border">
                                                <header><span class="glyphicon glyphicon-link"></span>Réseaux sociaux</header>
                                            </div>
                                            
                                            <div class="social_media_links_wrap padding-10">
                                                
                                                <?php if (!empty($links)) :?>
                                                    <ul class="links_candidat">
                                                    <?php foreach($links as $link): ?>
                                                        <li>
                                                            <a href="<?php echo $link['link'] ?>" target="_blank"><?php echo $link['link'] ?></a>
                                                            <span class="glyphicon glyphicon-trash delete_link tooltips" title="Supprimer" data-link_id="<?php echo $link['id'] ?>" data-candidat_id="<?php echo $candidat['id'] ?>"></span>
                                                        </li>
                                                    <?php endforeach;?>
                                                    </ul>
                                                <?php endif; ?>
                                                
                                                <form class="frm_frm frm_ajax" name="frm_edit_candidat_add_link" id="frm_edit_candidat_add_link" data-url="<?php echo AJAX_HANDLER ?>/add-candidat-link" data-type="json">
                                                    <input type="hidden" name="candidat_id" value="<?php echo $candidat['id'] ?>">

                                                    <fieldset class="socialMedia">
                                                        <div class="clearfix">
                                                            <label class="pull-left">Réseaux sociaux <span class="glyphicon glyphicon-info-sign tooltips" title="Un lien ex. www.linkedin.com/bastien"></span></label>
                                                            <div class="clearfix add_field_wrap_social pull-left" style="width: 60%;">
                                                                <div class="add_field_row clearfix">
                                                                    <input class="frm_text must socialmedia_link" name="reseauSociaux[]" placeholder="Entrez le lien" type="text" autocomplete="off" data-validation="val_url">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt6 clearfix">
                                                            <button type="button" class="add_field_social btn btn-icon btn-success pull-right tooltips" title="Ajouter un nouveau lien"><span class="glyphicon glyphicon-plus"></span></button>
                                                        </div>
                                                    </fieldset>

                                                    <fieldset>
                                                        <button type="button" class="btn btn-primary frm_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter lien/s</button>
                                                    </fieldset>

                                                </form>
                                                
                                            </div><!-- social_media_links_wrap -->
                                            
                                            <div class="block-head with-border with-top-border">
                                                <header><span class="glyphicon glyphicon-folder-open"></span>Documents</header>
                                            </div>
                                            
                                            <div class="documents_wrapper padding-10">
                                                
                                                <?php if (!empty($documents)) :?>
                                                    <ul class="links_candidat">
                                                    <?php foreach($documents as $document): ?>
                                                        <?php 
                                                        $imgType = array('jpg', 'jpeg', 'png');
                                                        $ext = getFileExt($document['file']);
                                                        if (in_array($ext, $imgType)):
                                                        ?>
                                                        <li>
                                                            <a href="<?php echo CANDIDAT_URL ?>/<?php echo $candidat['id'] ?>/<?php echo $document['file'] ?>" data-lightbox="<?php echo $document['file'] ?>"><?php echo $document['file'] ?></a>
                                                            <span class="glyphicon glyphicon-trash delete_document tooltips" title="Supprimer" data-document_id="<?php echo $document['id'] ?>" data-candidat_id="<?php echo $candidat['id'] ?>"></span>
                                                        </li>
                                                        <?php else: ?>
                                                        <li>
                                                            <a href="<?php echo BASE_URL ?>/download?type=can&id=<?php echo $document['id'] ?>"><?php echo $document['file'] ?></a>
                                                            <span class="glyphicon glyphicon-trash delete_document tooltips" title="Supprimer" data-document_id="<?php echo $document['id'] ?>" data-candidat_id="<?php echo $candidat['id'] ?>"></span>
                                                        </li>
                                                        <?php endif; ?>
                                                    <?php endforeach;?>
                                                    </ul>
                                                <?php endif; ?>
                                                
                                                <form class="frm_frm" name="frm_edit_candidat_add_files" id="frm_edit_candidat_add_files" action="<?php echo AJAX_HANDLER ?>/add-candidat-file" method="POST" enctype="multipart/form-data">
                                                    <input type="hidden" name="candidat_id" value="<?php echo $candidat['id'] ?>">
                                                    
                                                    <?php if (get('tdc_id') && get('tdc_id') != ""): ?>
                                                    <input type="hidden" name="mission_id" value="<?php echo get('tdc_id') ?>">
                                                    <?php endif; ?>
                                                    
                                                    <?php if (get('tdc_line_id') && get('tdc_line_id') != ""): ?>
                                                    <input type="hidden" name="tdc_id" value="<?php echo get('tdc_line_id') ?>">
                                                    <?php endif; ?>
                                                    
                                                    <fieldset class="uploadMultiple">
                                                        <div class="clearfix">
                                                            <label class="pull-left">Documents <span class="glyphicon glyphicon-info-sign tooltips" title="Les fichiers autorisés sont : .doc, .docx, .xls, .xlsx, .pdf, .jpg, .jpeg, .png ; Taille max : 10 Mo"></span></label>
                                                            <input type="hidden" name="uploadFlag" id="uploadFlag" value="1">
                                                            <div class="clearfix add_field_wrap_documents pull-left" style="width: 60%;">
                                                                <div class="add_field_row pull-left">
                                                                    <input type="file" name="file[]" id="file_1" data-number="1" class="inputfile inputfile-1">
                                                                    <label id="uploadLabel_1" for="file_1" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span></label>
                                                                    <button type="button" class="clear_field_upload btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt6 clearfix">
                                                            <button type="button" class="add_field_upload btn btn-icon btn-success pull-right tooltips" title="Ajouter un nouveau fichier"><span class="glyphicon glyphicon-plus"></span></button>
                                                        </div>                              
                                                    </fieldset>
                                                    
                                                    <fieldset>
                                                        <button type="button" class="btn btn-primary frm_submit frm_before_submit frm_notif pull-right" data-form="2"><i class="ico-txt fa fa-plus"></i> Ajouter Document/s</button>
                                                    </fieldset>
                                                
                                                </form>
                                                
                                            </div><!-- /documents_wrapper -->
                                            
										</div><!-- / block-body -->
										
									</div>
								</div><!-- /col -->
								
								<div class="col col-4">
									<div class="block nopadding">
										
										<div class="block-head with-border">
											<header><span class="glyphicon glyphicon-tasks"></span>Historiques Missions</header>
                                            <div class="block-head-btns pull-right">
                                                <button type="button" data-candidat_id="<?php echo $candidat['id'] ?>" class="candidat_mission_history btn btn-icon btn-primary pull-right tooltips" title="Voir en grand"><span class="glyphicon glyphicon-zoom-in"></span></button>
                                            </div>
										</div>
																
                                        <div class="block-body">
                                            <?php $suivis = Candidat::getSuivis($candidat['id']); ?>
                                            <?php $status = Control::getControlListByType(8); ?>
                                            <?php //debug($status) ?>
                                            <?php //debug($suivis); ?> 
                                            <?php if (!empty($suivis)): ?>
                                            <div class="nano" style="height: 450px;">
                                                <div class="nano-content">
                                                    <table class="table-list compact inside-nano">
                                                        <thead>
                                                            <tr>
                                                                <th>Mission</th>
                                                                <th>Statut</th>
                                                                <th>Client</th>
                                                                <th>Poste</th>
                                                                <th>Prise de contact</th>
                                                                <th>Short-listé</th>
                                                                <th>Placé</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach($suivis as $suivi): ?>
                                                            <tr>
                                                                <td align="center"><a href="<?php echo BASE_URL ?>/tdc?id=<?php echo $suivi['mission_id'] ?>"><?php echo $suivi['mission_id'] ?></a></td>
                                                                <td>
                                                                    <?php $tdcData = Candidat::getCandidatTDC($suivi['mission_id'], $candidat['id']); ?>
                                                                    
                                                                    <?php if ($tdcData['control_statut_tdc'] > 0): ?>
                                                                    <span class="status <?php echo getClassStatusTDC($tdcData['control_statut_tdc']) ?>">
                                                                        <?php echo $status[$tdcData['control_statut_tdc']] ?>
                                                                    </span>
                                                                    <?php endif; ?>
                                                                    
                                                                </td>
                                                                <td><?php echo $suivi['client'] ?></td>
                                                                <td><?php echo $suivi['poste'] ?></td>
                                                                <td><?php echo dateToFr($suivi['date_prise_de_contact']) ?></td>
                                                                <td align="center">
                                                                    <span class="status <?php if($suivi['shortliste'] == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
                                                                        <?php if($suivi['shortliste'] == 1): ?>Oui<?php else: ?>Non<?php endif; ?>
                                                                    </span>
                                                                </td>
                                                                <td align="center">
                                                                    <span class="status <?php if($suivi['place'] == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
                                                                        <?php if($suivi['place'] == 1): ?>Oui<?php else: ?>Non<?php endif; ?>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>   
                                                </div>
                                            </div><!-- /nano -->
                                            <?php else: ?>
                                            <div class="message warning">Ce candidat n'a pas des suivis dans aucun mission.</div>
                                            <?php endif; ?>
											
										</div><!-- / block-body -->
										                                        
									</div>
								</div><!-- /col -->
								
							</div><!-- / row -->
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
        	
            <?php if (WATCH): ?>
			<div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                            <header><span class="glyphicon glyphicon-tasks"></span>Historiques Actions <span class="glyphicon tooltips refresh-watchdog-candidat glyphicon-refresh" title="Actualiser Les Historiques"></span></header>
                        </div>
						
						<div class="block-body">
							
                            <?php $audits = Watchdog::WatchCandidat($candidat['id']); // debug($audits) ?>
                            <?php if (!empty($audits)): ?>
                            <div class="nano" style="min-height: 300px;max-height: 500px;">
                                <div class="nano-content">
                                    <table class="table-list compact inside-nano">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Utilisateur</th>
                                                <th>Rôle</th>
                                                <th>Action</th>
                                                <th>Voir Plus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($audits as $audit): ?>
                                            <tr>
                                                <td><?php echo $audit['created'] ?></td>
                                                <td><?php echo $audit['user_name'] ?></td>
                                                <td><?php echo $audit['user_role_name'] ?></td>
                                                <td><?php echo $audit['action'] ?></td>
                                                <td class="actions_button">
                                                    <button type="button" data-watchdog_id="<?php echo $audit['id'] ?>" class="see_more_candidat_watchdog btn btn-icon btn-success tooltips" title="Voir Plus"><span class="glyphicon glyphicon-eye-open"></span></button> 
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>   
                                </div>
                            </div><!-- /nano -->
                            <?php else: ?>
                            <div class="message warning">Ce candidat n'a pas des historiques actions</div>
                            <?php endif; ?>
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
            <?php endif; ?>
			
        </div><!-- /contentWrap -->
        
    </div><!-- /base --> 
	
	<?php require_once './views/debug.php'; ?>  
	
</body>
</html>
<script src="<?php echo JS_URL ?>/candidat.js"></script>
<script>
$(document).ready(function(){
    
    Upload.Init();
    Upload.Validate();
    
    // refresh suivi watchdog 
    // reload the page
    $(document).on('click', 'span.refresh-watchdog-candidat', function(){
        location.reload();
    });
    
    // see more details for watchog
    $(document).on('click', '.see_more_candidat_watchdog', function(){
        var watchdog_id = $(this).data('watchdog_id');
        ajaxBusy = true;
        Modal.Show(AJAX_UI + '/watchdog-candidat?id='+watchdog_id, null, null, '<span class="glyphicon glyphicon-tasks">', 80, 'watchdog_candidat');
    });
    
    // add a new field for reseau socieau link
    $(document).on('click', 'button.add_field_social', function(){
        var inputHtml = '<input class="frm_text must socialmedia_link" name="reseauSociaux[]" placeholder="Entrez le lien" type="text" autocomplete="off" data-validation="val_url">';
        var newFieldHtml = '<div class="add_field_row clearfix">'+inputHtml+'</div>';
        $('.add_field_wrap_social').append(newFieldHtml);
    });
    
    $(document).on('blur', 'input.socialmedia_link', function(){
        var numRows = parseInt($('fieldset.socialMedia .add_field_wrap_social .add_field_row:last').index()) + 1;
        
        if (numRows >= 2 && $(this).val() == "") {
            $(this).parent('.add_field_row').remove();
        }       
        
    });
    
    // add a new field for upload
    $(document).on('click', 'button.add_field_upload', function(){
        
        // get the last input number
        var number = parseInt($('.add_field_wrap_documents .add_field_row:last input.inputfile').attr('data-number')) + 1;
        
        var inputHtml = '<input type="file" name="file[]" id="file_'+number+'" data-number="'+number+'" class="inputfile inputfile-1"><label id="uploadLabel_'+number+'" for="file_'+number+'" class="fileUpload"><span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span></label><button type="button" class="clear_field_upload btn btn-icon btn-danger pull-right mt5 tooltips" title="Annuler"><span class="glyphicon glyphicon-remove"></span></button>';
        var newFieldHtml = '<div class="add_field_row clearfix">'+inputHtml+'</div>';
        $('.add_field_wrap_documents').append(newFieldHtml);
        
        var flagNumber = parseInt($('#uploadFlag').val()) + 1;
        $('#uploadFlag').val(flagNumber);
        
        Upload.Init();
    });
    
    $(document).on('click', '.clear_field_upload', function(){
        
        var numRows = parseInt($('fieldset.uploadMultiple .add_field_wrap_documents .add_field_row:last').index()) + 1;
        
        if (numRows >= 2) {
            $(this).parent('.add_field_row').remove();
        } else if (numRows == 1) {
            $(this).parent('.add_field_row').find('input.inputfile').val('');
            $(this).parent('.add_field_row').find('label').html('<span class="glyphicon glyphicon-open"></span> <span class="filename">Choisir un fichier</span>');
        }
        
    });
    
    // delete social media link
    $(document).on('click', '.delete_link', function (){
        var id = $(this).attr('data-link_id');
        var candidat_id = $(this).attr('data-candidat_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer lien', AJAX_HANDLER + '/delete-candidat-link?id='+id+'&candidat_id='+candidat_id);
    });
    
    // delete document
    $(document).on('click', '.delete_document', function (){
        var id = $(this).attr('data-document_id');
        var candidat_id = $(this).attr('data-candidat_id');
        Dialog.Show('Veuillez confirmer', 'Supprimer document', AJAX_HANDLER + '/delete-candidat-file?id='+id+'&candidat_id='+candidat_id);
    });
    
    // autocomplete on societe actuelle field
    // https://makitweb.com/jquery-ui-autocomplete-with-php-and-ajax/
            
    $("#societe_autocomplete").autocomplete({
        minLength: 3,
        source: function(request, response) {
            // Fetch data
            $.ajax({
                url: AJAX_HANDLER + "/search-candidat-companies",
                type: 'post',
                dataType: "json",
                data: {
                    search: request.term
                },
                // response must be {value: id of the data, label: name to display}
                success: function( data ) {
                    response( data );
                }
            });
        },
        select: function (event, ui) {
            // Set selection
            $('#societe_autocomplete').val(ui.item.label); // display the selected text
            $('#societe_autocomplete_id').val(ui.item.value); // save selected id to input
            return false;
        },
        focus: function(event, ui){
            $('#societe_autocomplete').val(ui.item.label); // display the selected text
            $('#societe_autocomplete_id').val(ui.item.value); // save selected id to input
            return false;
        },
    });
    
    // check candidat societe blacklister
    $(document).on('blur', '.checkSocieteBlacklisteCandidat', function(){
        if ($(this).val() != "") {
            
            ajaxBusy = true;
            var nom_societe = $(this).val();
            // we hide the submit button and after success we display back
            $('#editerCandidatSubmit').fadeOut();
            var url = AJAX_HANDLER+'/check-societe-candidat-blackliste?nom_societe='+nom_societe;
            $.ajax({
                url: url,
                dataType : 'json',
                cache: false,
                success: function (result, status) {
                    if (status === "success" && result.status === "OK") {
                        $('.msgBlackliste').find('.notif').html(result.msg);
                        $('.msgBlackliste').fadeIn();
                    } else if (status === "success" && result.status === "NOK") {
                        $('.msgBlackliste').fadeOut();
                    }
                    $('#editerCandidatSubmit').fadeIn();
                }
            });
            
        } else {
            $('.msgBlackliste').fadeOut();
        }
            
    });
     
}); // end document ready function

function reloadlink(candidat_id) {
    // ajax call to reload link
    var url = AJAX_UI+'/reload-candidat-link?id='+candidat_id;

    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('.social_media_links_wrap').html(result);
        }
    });

}

function reloaddocument(candidat_id) {
    // ajax call to reload link
    var url = AJAX_UI+'/reload-candidat-document?id='+candidat_id;

    $.ajax({
        url: url,
        dataType : 'html',
        cache: false,
        success: function (result, status) {
            $('.documents_wrapper').html(result);
        }
    });

}

var Upload = {
    Init: function (){
        $('.inputfile').each(function(){
		
            var $input	 = $( this ),
                $label	 = $input.next( 'label' ),
                labelVal = $label.html();

            $input.on( 'change', function( e )
            {
                var fileName = '';

                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else if( e.target.value )
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    $label.find( 'span.filename' ).html( fileName );
                else
                    $label.html( labelVal );
            });

            // Firefox bug fix
            $input
            .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
            .on( 'blur', function(){ 
                $input.removeClass( 'has-focus' ); 
            });

        });
    },
    Validate: function (){
        $(document).on('change', '.inputfile', function(){
              Upload.ValidateUpload($(this));            
        });
    },
    ValidateUpload: function (input) {
        var file_data = input.prop("files")[0];
        // start validate file extension
        var validExtensions = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx', 'xls', 'xlsx']; //array of valid extensions
        var filename = file_data.name;
        var ext = filename.substr(filename.lastIndexOf('.') + 1);
        if ($.inArray(ext, validExtensions) == -1){
            Notif.Show('Fichier non valide', 'error', true, 4000);
            input.val("");
            input.next('label').removeClass('ok').addClass('error');
            return false;
        } else {
            // check file size
            // 4MB allowed
            var filesize = file_data.size;
            if (filesize > 10485760) {
                Notif.Show('La taille du fichier est supérieure à celle autorisée. Télécharger moins de 10 Mo.', 'error', true, 4000);
                input.val("");
                input.next('label').removeClass('ok').addClass('error');
                return false;
            } else {
                input.next('label').removeClass('error').addClass('ok');
                return true;
            }
        }
        // end validation file extension

    },
}

function frm_edit_candidat_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        if (result.callback != undefined && result.param != undefined) {
            Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
        } else {
            Notif.Show(result.msg, result.type, true, 5000);
        }
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }    
}

function frm_edit_candidat_add_link_ajax_success (result, status) {
    if (status === "success" && result.status === "OK") {
        Notif.Show(result.msg, result.type, true, 5000, result.callback, result.param);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}

function frm_edit_candidat_add_files_before_submit(obj) {
    
    var uploadFlag = true;
    var numLabel = parseInt($('#uploadFlag').val());
    
    for(var i = 1; i <= numLabel; i++) {
        
        if ($('#file_'+numLabel).val() == ""){
            $('#uploadLabel_'+numLabel).addClass('error');
            uploadFlag = false;
        }
        
        if ($('#uploadLabel_'+numLabel).length) {
            if ($('#uploadLabel_'+numLabel).hasClass('error')) {
                uploadFlag = false;
            }
        }
    }
    
    return uploadFlag;
    
}
</script>