<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Autorisations" ?>
<?php $page = "permissions"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('assign_deassign_perm', 'create_role', 'delete_role'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>
<?php $permissions = Action::getActionGroupByModule() ?>
<?php $roles = Role::getRoles() ?>

    <?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base">

            <div id="contentWrap">
        
                <section class="title block">

                    <h1 class="pull-left">Autorisations</h1>
                    
                    <?php if (User::can('create_role')): ?>
                    <div class="btn-box pull-right">                        
                        <button type="button" class="add_role btn btn-icon btn-primary pull-right tooltips" title="Ajouter Rôle"><span class="glyphicon glyphicon-plus"></span></button>
                    </div>
                    <?php endif; ?>
                    
                </section>	
            
            <div class="row">
				
                <div class="col col-12">
                    <div class="block nopadding">
						
                        <div class="block-head with-border">
                            <header><span class="glyphicon glyphicon-lock"></span>Autorisations utilisateurs</header>
                        </div>
						
                        <div class="block-body">
							
                            <div class="sticky">    	
                                <div class="permissions_wrapper clearfix">
                                    <div class="permissions_lists_wrapper">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td class="heading">Rôles Utilisateurs</td>
                                                </tr>
                                                <tr class="subheading">
                                                    <td>Liste Autorisations</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> 
                                    <div class="permissions_roles_wrapper">
                                        <?php foreach($roles as $role): ?>
                                        <table class="<?php print $role['name'] ?>">
                                            <tbody>
                                                <tr>
                                                    <td class="heading">
                                                        <?php echo mb_ucfirst($role['name']) ?>
                                                        <?php if (User::can('delete_role') && $role['id'] > 4 && $role['id'] <> 13): ?>
                                                        <span class="glyphicon glyphicon-trash tooltips delete_role" data-role_id="<?php echo $role['id'] ?>" data-role_name="<?php echo mb_ucfirst($role['name']) ?>" title="Supprimer Rôle Utilisateur <?php echo mb_ucfirst($role['name']) ?>"></span>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr> 
                                                <tr>
                                                    <td align="center" class="selectAllPerm">
                                                        <div data-role_id="<?php print $role['id'] ?>" class="tick tooltips <?php if (Action::checkAssignedActionByRole($role['id'])): ?>active<?php endif; ?> selectAll" title="Sélectionnez Tous / Désélectionner toutes pour rôle d'utilisateur <strong><?php print mb_ucfirst($role['name']) ?></strong>."></div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <?php endforeach;?>
                                    </div>    
                                </div>         
                            </div><!-- /stickyHeader -->
                            
                            <div class="permissions_wrapper clearfix" id="perm">
        	
                                <div class="permissions_lists_wrapper">
        
                                    <table>
                                            <tbody>
                                                <tr>
                                                    <td class="heading">Rôles Utilisateurs</td>
                                                </tr>
                                                <tr class="subheading">
                                                    <td>Liste Autorisations</td>
                                                </tr>
                                                <?php foreach($permissions as $element => $actions): ?>
                                                    <?php $elementArray = Action::getModuleDescByName($element) ?>
                                                    <tr>
                                                        <td class="category_title" align="center">
                                                            <?php print mb_ucfirst($elementArray['name']) ?>
                                                            <div class="trigger tooltips" title="Ouvrir / Fermer <strong><?php print mb_ucfirst($elementArray['name']) ?></strong>" data-element_id="<?php print $elementArray['id'] ?>"></div>
                                                        </td>
                                                    </tr>
                                                    <tr class="element_id_<?php print $elementArray['id'] ?>">
                                                        <td class="nopadding perm_lists">
                                                            <table>
                                                                <?php foreach($actions as $action): ?>
                                                                <tr>
                                                                    <td <?php if (User::isSA()) { print 'class="tooltips"'; print 'title="'. $action['name'] .'"'; } ?>>
                                                                        <?php print mb_ucfirst($action['name']) ?>
                                                                        <?php if (User::isSA()): ?>
                                                                        <span><?php echo $action['code'] ?></span>
                                                                        <?php endif; ?>
                                                                    </td>
                                                                </tr>
                                                                <?php endforeach; ?>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                    </table>

                                </div><!--/ permissions_lists_wrapper -->
                                        
                                <div class="permissions_roles_wrapper" id="roles">
        
                                    <?php foreach($roles as $role): ?>
                                    <table class="<?php print $role['name'] ?>">
                                        <tbody>
                                            <tr>
                                                <td class="heading" data-role_id="<?php print $role['id'] ?>">
                                                    <?php echo mb_ucfirst($role['name']) ?>
                                                    <?php if (User::can('delete_role') && $role['id'] > 4 && $role['id'] <> 13): ?>
                                                    <span class="glyphicon glyphicon-trash tooltips delete_role" data-role_id="<?php echo $role['id'] ?>" data-role_name="<?php echo mb_ucfirst($role['name']) ?>" title="Supprimer Rôle Utilisateur <?php echo mb_ucfirst($role['name']) ?>"></span>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="selectAllPerm">
                                                    <div data-role_id="<?php print $role['id'] ?>" class="tick tooltips <?php if (Action::checkAssignedActionByRole($role['id'])): ?>active<?php endif; ?> selectAll" title="Sélectionnez Tous / Désélectionner toutes pour rôle d'utilisateur <strong><?php print ucfirst($role['name']) ?></strong>."></div>
                                                </td>
                                            </tr>
                                            <?php foreach($permissions as $element => $actions): ?>
                                                <?php $elementArray = Action::getModuleDescByName($element) ?>
                                                <tr>
                                                    <td align="center" class="category_title">
                                                        <div class="tick tooltips <?php if (Action::checkAssignedActionByModule($role['id'], $elementArray['id'])): ?>active<?php endif; ?> selectCat element_id_<?php print $elementArray['id'] ?>" title="<?php print mb_ucfirst($role['name']) . ' <strong>:</strong> ' . mb_ucfirst($elementArray['name']) ?>" data-role_id="<?php print $role['id'] ?>" data-element_id="<?php print $elementArray['id'] ?>"></div>
                                                    </td>
                                                </tr>
                                                <?php $counter = 1; ?>
                                                <?php foreach($actions as $action): ?>
                                                    <?php $class = isEven($counter) ? 'even' : 'odd' ?>
                                                    <tr class="<?php print $class ?> element element_id_<?php print $elementArray['id'] ?>">
                                                        <td align="center">
                                                            <div class="tick tooltips <?php if (Action::checkAssignedAction($role['id'], $action['id'])): ?>active<?php endif; ?>" title="<?php print mb_ucfirst($role['name']) . ' <strong>:</strong> ' . mb_ucfirst($elementArray['name']) . ' <strong>:</strong> ' . mb_ucfirst($action['name']) ?>" data-role_id="<?php print $role['id'] ?>" data-permission_id="<?php print $action['id'] ?>" data-element_id="<?php print $elementArray['id'] ?>"></div>
                                                        </td>
                                                    </tr>
                                                    <?php $counter++; ?>
                                                <?php endforeach;?>

                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                    <?php endforeach;?>

                                </div><!-- /permissions_roles_wrapper -->
                                        
                            </div><!-- /permissions_wrapper -->
                            
                        </div><!-- / block-body -->
						
                    </div>
                </div><!-- /col -->
				
            </div><!-- / row -->
        
        </div><!-- /contentWrap -->
        
    </div><!-- /base -->    
    
    <?php require_once './views/debug.php'; ?>
	
</body>
</html>
<?php //if (User::can('edit_permission')): ?>
<script>
$(document).ready(function(){	
	
    Sticky.Init();
	<?php if (User::can('create_role')): ?>Perm.AddRole();<?php endif; ?>
    <?php if (User::can('delete_role')): ?>Perm.DeleteRole();<?php endif; ?>
        
	// trigger show/hide grouped elements
	$('.trigger').on('click', function(){
        var element_id = $(this).attr('data-element_id');
        var className = "element_id_" + element_id;
        $('tr.'+className).slideToggle(100, 'easeInQuart', function(){
            // stickyheader width manipulation
            var width = $('#perm .permissions_lists_wrapper td.heading').outerWidth();
            $('.sticky .permissions_lists_wrapper td').css('width', width);
            $(this).addClass('open');
        });	
	});
	
	// stickyheader width manipulation        
    var wid = $('#perm .permissions_lists_wrapper td.heading').outerWidth();
    $('.sticky .permissions_lists_wrapper td').css('width', wid);
	
	// ticks
    <?php if (User::can('assign_deassign_perm')): ?>
	$('.tick').on('click', function(){
		
        var obj = $(this);
        var roleTable = obj.parent().parent().parent().parent('table').prop('class');

        // verify if it is checked/unchecked
        var action = "";
        if ($(this).hasClass('active')) {
            action = "remove";
        } else {
            action = "add";
        }

        // verify if single/selectCat/selectAll
        var option = "";
        if ($(this).hasClass('selectCat')) {
            option = "group";
            var data = {role_id: $(this).attr('data-role_id'), module_id: $(this).attr('data-element_id')};
        } else if ($(this).hasClass('selectAll')) {
            option = "all";
            var data = {role_id: $(this).attr('data-role_id')};
        } else {
            option = "single";
            var data = {role_id: $(this).attr('data-role_id'), action_id: $(this).attr('data-permission_id'), module_id: $(this).attr('data-element_id')};
        }
				
        $.ajax({
            type: 'POST',
            url: AJAX_HANDLER + '/autorisations-add-remove?action='+ action +'&option='+option,
            data: {data:data},
            success: function(res) {},
        }).done(function() {
            if (option == "single") {
                if (action == "add") {
                    obj.addClass('active');
                } else if (action == "remove") {
                    obj.removeClass('active');
                }

                // check uncheck group
                var className = "element_id_" + obj.attr('data-element_id');
                var total_element_counter = 0;
                var total_element_checked = 0;
                $('.'+roleTable).find('tr.'+className).each(function() {
                    total_element_counter++;
                    if ($(this).find('.tick').hasClass('active')) {
                        total_element_checked++;
                    }
                });
                if (total_element_counter != total_element_checked) {
                    $('.'+roleTable).find('.selectCat.'+className).removeClass('active');
                } else if (total_element_counter == total_element_checked) {
                    $('.'+roleTable).find('.selectCat.'+className).addClass('active');
                }

                // check uncheck select All
                var total_counter = 0;
                var total_checked = 0;
                $('.'+roleTable).find('tr.element').each(function() {
                    total_counter++;
                    if ($(this).find('.tick').hasClass('active')) {
                        total_checked++;
                    }
                });
                if (total_counter != total_checked) {
                    $('.'+roleTable).find('.tick.selectAll').removeClass('active');
                } else if (total_counter == total_checked) {
                    $('.'+roleTable).find('.tick.selectAll').addClass('active');
                }

            } else if (option == "group") {
                var element_id = parseInt(obj.attr('data-element_id'));
                $('.'+roleTable).find('tr').each(function() {
                    var thisElementId = parseInt($(this).find('.tick').attr('data-element_id'));
                        if (element_id == thisElementId) {
                            if (action == "add") {
                                $(this).find('.tick').removeClass('active').addClass('active');
                            } else if (action == "remove") {
                                $(this).find('.tick').removeClass('active');
                            }
                        }
                    });

                    // check uncheck select All
                    var total_counter = 0;
                    var total_checked = 0;
                    $('.'+roleTable).find('tr.element').each(function() {
                        total_counter++;
                        if ($(this).find('.tick').hasClass('active')) {
                            total_checked++;
                        }
                    });
                    if (total_counter != total_checked) {
                        $('.'+roleTable).find('.tick.selectAll').removeClass('active');
                    } else if (total_counter == total_checked) {
                        $('.'+roleTable).find('.tick.selectAll').addClass('active');
                    }

            } else if (option == "all") {
                $('.'+roleTable).find('tr').each(function() {
                    if (action == "add") {
                        $(this).find('.tick').removeClass('active').addClass('active');
                    } else if (action == "remove") {
                        $(this).find('.tick').removeClass('active');
                    }
                });
            }	

        });
		
	});
    <?php endif; ?>
	
});

var Perm = {
    <?php if (User::can('create_role')): ?>
    AddRole: function (){
        $(document).on('click', '.add_role', function (){
            Modal.Show(AJAX_UI + '/add-role', null, null, '<i class="os-icon os-icon-user-male-circle"></i>', 50);
        });
    },
    <?php endif; ?>
    <?php if (User::can('delete_role')): ?>
    DeleteRole: function () {
        $(document).on('click', '.delete_role', function (){
            var role_id = $(this).attr('data-role_id');
            var role_name = $(this).attr('data-role_name');
            Dialog.Show('Veuillez confirmer', 'Supprimer le rôle '+ role_name, AJAX_HANDLER + '/delete-role?id='+role_id);
        });
    },
    <?php endif; ?>
}

var Sticky = {
    Stick: function(){

        var window_top = $(window).scrollTop();
        if (window_top > 160) {
            $('.sticky').css('visibility','visible');
        } else {
            $('.sticky').css('visibility','hidden');
        }

    },
    Init: function (){
        if ($('.sticky').length) {
            $(window).scroll(Sticky.Stick);
            Sticky.Stick();
        }
    },
}

<?php if (User::can('create_role')): ?>
function frm_create_role_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
        $('#frm_create_role .frm_text').removeClass('ok').addClass('error');
    }
}
<?php endif; ?>
</script>
<?php //endif; ?>