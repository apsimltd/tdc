<?php require_once __DIR__ . '/conf/bootstrap.inc'; ?>
<?php $page_title = "OPSEARCH::Clients" ?>
<?php $page = "clients"; ?>
<?php require_once './views/init.php'; ?>
<?php if (!User::can(array('create_client', 'read_client', 'edit_client'))) header("Location: " . BASE_URL . "/tableau-de-bord") ?>
<?php require_once './views/head.php'; ?>

	
	<?php require_once './views/menu.php'; ?>
    <?php require_once './views/header.php'; ?>
	
	<div id="base">

        <div id="contentWrap">
        
			<section class="title block">

				<h1 class="pull-left">Clients</h1>
				
                <?php if (User::can('create_client')): ?>
				<div class="btn-box pull-right">                        
					<button type="button" class="add_client btn btn-icon btn-primary pull-right tooltips" title="Ajouter Client"><span class="glyphicon glyphicon-plus"></span></button>
				</div>
                <?php endif; ?>
				
			</section>
			
            <?php if (User::can('read_client', 'edit_client')): ?>
            <div class="row">
				
				<div class="col col-12">
					<div class="block nopadding">
						
						<div class="block-head with-border">
                        	<header><i class="fa fa-users"></i>Liste des clients</header>
                        </div>
						
						<div class="block-body">
							
                            <table class="datable stripe hover">
                                <thead>
                                    <tr>
                                        <th>Ref</th>
										<th>Nom société</th>
                                        <th>Adresse société</th>
                                        <th>Téléphone société</th>
                                        <th>Email</th>
										<th>Date de création</th>
										<th>Nom Contact</th>
										<th>Prénom Contact</th>
                                        <th>Poste Contact</th>
                                        <th>Téléphone Contact</th>
                                        <th>Email Contact</th>
                                        <th>Blacklist</th>
                                        <?php if (User::can('edit_client')): ?>
                                        <th>Actions</th>
                                        <?php endif; ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $clients = Client::getClients(); ?>
                                    <?php foreach($clients as $client): ?>
                                    <tr>
                                        <td><?php echo $client['id'] ?></td>
										<td><?php echo $client['nom'] ?></td>
										<td><?php echo $client['address'] ?></td>
										<td><?php echo $client['telephone'] ?></td>
                                        <td><?php echo $client['email'] ?></td>
                                        <td data-order="<?php echo $client['created'] ?>">
                                            <span title="<?php echo $client['created'] ?>"><?php echo dateToFr($client['created'], true) ?></span>
                                        </td>
                                        <td><?php echo mb_strtoupper($client['contact_lastName']) ?></td>
                                        <td><?php echo mb_ucfirst($client['contact_firstName']) ?></td>
                                        <td><?php echo $client['contact_post'] ?></td>
                                        <td><?php echo $client['contact_telephone'] ?></td>
                                        <td><?php echo $client['contact_email'] ?></td>
                                        <td align="center" style="width: 40px;" data-order="<?php echo $client['blacklist'] ?>">
                                            <div data-client_id="<?php echo $client['id'] ?>" class="tick <?php if (User::can('edit_client')): ?>client_blacklist<?php endif; ?> <?php if ($client['blacklist'] == 1): ?>active<?php endif; ?>"></div>
                                        </td>
                                        <?php if (User::can('edit_client')): ?>
                                        <td class="actions_button">
                                            <button type="button" data-client_id="<?php echo $client['id'] ?>" class="edit_client btn btn-icon btn-primary tooltips" title="Éditer"><span class="glyphicon glyphicon-pencil"></span></button>
                                        </td>
                                        <?php endif; ?>
                                    </tr>
									<?php endforeach; ?>
                                </tbody>
                            </table>
                            
						</div><!-- / block-body -->
						
					</div>
				</div><!-- /col -->
				
			</div><!-- / row -->
            <?php endif; ?>
        
        </div><!-- /contentWrap -->
        
    </div><!-- /base -->
    
	<?php require_once './views/debug.php'; ?>
    
</body>
</html>
<script>
$(document).ready(function(){
    <?php if (User::can('create_client')): ?>Client.Add();<?php endif; ?>
    <?php if (User::can('edit_client')): ?>Client.Edit();<?php endif; ?>
        
    $(document).on('click', '.client_blacklist', function(){
        var client_id = $(this).data('client_id');
        var me = $(this);
        var op = 'select';
        if (me.hasClass('active')) {
            op = 'deselect';
        } else {
            op = 'select';
        }
        
        var url = AJAX_HANDLER+'/client-blacklist?id='+client_id+'&op='+op;
    
        $.ajax({
            url: url,
            dataType : 'json',
            cache: false,
            success: function (result, status) {
                if (op == 'select') {
                    me.addClass('active');
                    me.parent('td').attr('data-order', "1");
                } else {
                    me.removeClass('active');
                    me.parent('td').attr('data-order', "0");
                }
                if (status === "success" && result.status === "OK") {
                    Notif.Show(result.msg, result.type, true, 5000);
                } else if (status === "success" && result.status === "NOK") {
                    Notif.Show(result.msg, result.type, true, 5000);
                }

            }
        });
        
    });
        
});

var Client = {
    <?php if (User::can('create_client')): ?>
    Add: function (){
        $(document).on('click', '.add_client', function (){
            Modal.Show(AJAX_UI + '/add-client', null, null, '<i class="fa fa-users"></i>', 50);
        });
    },
    <?php endif; ?>
    <?php if (User::can('edit_client')): ?>
    Edit: function (){
        $(document).on('click', '.edit_client', function (){
            var client_id = $(this).attr('data-client_id');
            Modal.Show(AJAX_UI + '/edit-client?id='+client_id, null, null, '<i class="fa fa-users"></i>', 50);
        });
    },
    <?php endif; ?>
}

<?php if (User::can('create_client')): ?>
function frm_create_client_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}
<?php endif; ?>
    
<?php if (User::can('edit_client')): ?>
function frm_edit_client_ajax_success(result, status) {
    if (status === "success" && result.status === "OK") {
        $('.modal-v2 button.close').trigger('click');
        Notif.Show(result.msg, result.type, true, 5000, result.callback);
    } else if (status === "success" && result.status === "NOK") {
        Notif.Show(result.msg, result.type, true, 5000);
    }
}
<?php endif; ?>
</script>